/**
 * AcuseAceptaRechaza.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package views.core.soap.services.apps;

public class AcuseAceptaRechaza  implements java.io.Serializable {
    private views.core.soap.services.apps.Rechaza[] rechazo;

    private views.core.soap.services.apps.Acepta[] aceptacion;

    private java.lang.String error;

    public AcuseAceptaRechaza() {
    }

    public AcuseAceptaRechaza(
           views.core.soap.services.apps.Rechaza[] rechazo,
           views.core.soap.services.apps.Acepta[] aceptacion,
           java.lang.String error) {
           this.rechazo = rechazo;
           this.aceptacion = aceptacion;
           this.error = error;
    }


    /**
     * Gets the rechazo value for this AcuseAceptaRechaza.
     * 
     * @return rechazo
     */
    public views.core.soap.services.apps.Rechaza[] getRechazo() {
        return rechazo;
    }


    /**
     * Sets the rechazo value for this AcuseAceptaRechaza.
     * 
     * @param rechazo
     */
    public void setRechazo(views.core.soap.services.apps.Rechaza[] rechazo) {
        this.rechazo = rechazo;
    }


    /**
     * Gets the aceptacion value for this AcuseAceptaRechaza.
     * 
     * @return aceptacion
     */
    public views.core.soap.services.apps.Acepta[] getAceptacion() {
        return aceptacion;
    }


    /**
     * Sets the aceptacion value for this AcuseAceptaRechaza.
     * 
     * @param aceptacion
     */
    public void setAceptacion(views.core.soap.services.apps.Acepta[] aceptacion) {
        this.aceptacion = aceptacion;
    }


    /**
     * Gets the error value for this AcuseAceptaRechaza.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this AcuseAceptaRechaza.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AcuseAceptaRechaza)) return false;
        AcuseAceptaRechaza other = (AcuseAceptaRechaza) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rechazo==null && other.getRechazo()==null) || 
             (this.rechazo!=null &&
              java.util.Arrays.equals(this.rechazo, other.getRechazo()))) &&
            ((this.aceptacion==null && other.getAceptacion()==null) || 
             (this.aceptacion!=null &&
              java.util.Arrays.equals(this.aceptacion, other.getAceptacion()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRechazo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRechazo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRechazo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAceptacion() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAceptacion());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAceptacion(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AcuseAceptaRechaza.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("apps.services.soap.core.views", "AcuseAceptaRechaza"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rechazo");
        elemField.setXmlName(new javax.xml.namespace.QName("apps.services.soap.core.views", "rechazo"));
        elemField.setXmlType(new javax.xml.namespace.QName("apps.services.soap.core.views", "Rechaza"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("apps.services.soap.core.views", "Rechaza"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aceptacion");
        elemField.setXmlName(new javax.xml.namespace.QName("apps.services.soap.core.views", "aceptacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("apps.services.soap.core.views", "Acepta"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("apps.services.soap.core.views", "Acepta"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("apps.services.soap.core.views", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
