/**
 * UUIDS_AR.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package views.core.soap.services.apps;

public class UUIDS_AR  implements java.io.Serializable {
    private views.core.soap.services.apps.UUID_AR[] uuids_ar;

    public UUIDS_AR() {
    }

    public UUIDS_AR(
           views.core.soap.services.apps.UUID_AR[] uuids_ar) {
           this.uuids_ar = uuids_ar;
    }


    /**
     * Gets the uuids_ar value for this UUIDS_AR.
     * 
     * @return uuids_ar
     */
    public views.core.soap.services.apps.UUID_AR[] getUuids_ar() {
        return uuids_ar;
    }


    /**
     * Sets the uuids_ar value for this UUIDS_AR.
     * 
     * @param uuids_ar
     */
    public void setUuids_ar(views.core.soap.services.apps.UUID_AR[] uuids_ar) {
        this.uuids_ar = uuids_ar;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UUIDS_AR)) return false;
        UUIDS_AR other = (UUIDS_AR) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uuids_ar==null && other.getUuids_ar()==null) || 
             (this.uuids_ar!=null &&
              java.util.Arrays.equals(this.uuids_ar, other.getUuids_ar())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUuids_ar() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUuids_ar());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUuids_ar(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UUIDS_AR.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("apps.services.soap.core.views", "UUIDS_AR"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uuids_ar");
        elemField.setXmlName(new javax.xml.namespace.QName("apps.services.soap.core.views", "uuids_ar"));
        elemField.setXmlType(new javax.xml.namespace.QName("apps.services.soap.core.views", "UUID_AR"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("apps.services.soap.core.views", "UUID_AR"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
