/**
 * AcuseRelacionados.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package views.core.soap.services.apps;

public class AcuseRelacionados  implements java.io.Serializable {
    private views.core.soap.services.apps.Padre[] padres;

    private java.lang.String error;

    private views.core.soap.services.apps.Hijo[] hijos;

    public AcuseRelacionados() {
    }

    public AcuseRelacionados(
           views.core.soap.services.apps.Padre[] padres,
           java.lang.String error,
           views.core.soap.services.apps.Hijo[] hijos) {
           this.padres = padres;
           this.error = error;
           this.hijos = hijos;
    }


    /**
     * Gets the padres value for this AcuseRelacionados.
     * 
     * @return padres
     */
    public views.core.soap.services.apps.Padre[] getPadres() {
        return padres;
    }


    /**
     * Sets the padres value for this AcuseRelacionados.
     * 
     * @param padres
     */
    public void setPadres(views.core.soap.services.apps.Padre[] padres) {
        this.padres = padres;
    }


    /**
     * Gets the error value for this AcuseRelacionados.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this AcuseRelacionados.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the hijos value for this AcuseRelacionados.
     * 
     * @return hijos
     */
    public views.core.soap.services.apps.Hijo[] getHijos() {
        return hijos;
    }


    /**
     * Sets the hijos value for this AcuseRelacionados.
     * 
     * @param hijos
     */
    public void setHijos(views.core.soap.services.apps.Hijo[] hijos) {
        this.hijos = hijos;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AcuseRelacionados)) return false;
        AcuseRelacionados other = (AcuseRelacionados) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.padres==null && other.getPadres()==null) || 
             (this.padres!=null &&
              java.util.Arrays.equals(this.padres, other.getPadres()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.hijos==null && other.getHijos()==null) || 
             (this.hijos!=null &&
              java.util.Arrays.equals(this.hijos, other.getHijos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPadres() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPadres());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPadres(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getHijos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHijos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHijos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AcuseRelacionados.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("apps.services.soap.core.views", "AcuseRelacionados"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("padres");
        elemField.setXmlName(new javax.xml.namespace.QName("apps.services.soap.core.views", "Padres"));
        elemField.setXmlType(new javax.xml.namespace.QName("apps.services.soap.core.views", "Padre"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("apps.services.soap.core.views", "Padre"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("apps.services.soap.core.views", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hijos");
        elemField.setXmlName(new javax.xml.namespace.QName("apps.services.soap.core.views", "Hijos"));
        elemField.setXmlType(new javax.xml.namespace.QName("apps.services.soap.core.views", "Hijo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("apps.services.soap.core.views", "Hijo"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
