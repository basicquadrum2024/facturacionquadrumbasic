package com.mx.quadrum.basic.excepcion;

public class ValidacionCertificadosExcepcion extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6986552325327747301L;

	public ValidacionCertificadosExcepcion(String message) {
		super(message);
	}
}
