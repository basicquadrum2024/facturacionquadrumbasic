package com.mx.quadrum.basic.dao;

import com.mx.quadrum.basic.entity.TwUsuario;

/**
 * Interface que contiene la definición de métodos de acceso a la base de datos a clase/objeto TwUsuario
 * @author 
 *
 */
public interface IUsuarioDao extends GenericDao<TwUsuario> {

    public TwUsuario getFindUsuarioLogin(String email) throws Exception;

    public int getUpdateIntentos(String rfcUsr, int cont) throws Exception;

}
