package com.mx.quadrum.basic.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcAcciones;
import com.mx.quadrum.basic.entity.TwUsuario;

/**
 * Clase que contiene los m�todos de acceso a la base de datos a clase/objeto TwUsuario
 * @author Manu
 *
 */
@Repository
public class UsuarioDaoImp extends AbstractGenericDao<TwUsuario> implements IUsuarioDao {
    public static Logger log = Logger.getLogger(UsuarioDaoImp.class);

    /**
     * Servicio de encripci�n.
     */

    @Autowired
    private AesServices aesServices;

    /**
     * M�todo busca usuario en la base de datos por rfc
     */
    @Override
    public TwUsuario getFindUsuarioLogin(String username) {
	TwUsuario tmp = null;
	try {
	    Criteria crit = getSession().createCriteria(TwUsuario.class).add(Restrictions.eq("rfce", username));
	    crit.createAlias("tcPerfil", "perfil", Criteria.LEFT_JOIN);
	    crit.createAlias("perfil.tcAccioneses", "acciones", Criteria.LEFT_JOIN);
	    crit.addOrder(Order.asc("acciones.nombre"));
	    tmp = (TwUsuario) crit.uniqueResult();
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return tmp;
    }

    /**
     * M�todo que convierte una lista de TcAcciones en un mapa
     * @param list
     * @return
     */
    private Set<TcAcciones> ConvertedMap(List<TcAcciones> list) {
	Set<TcAcciones> resultsMap = new HashSet<TcAcciones>(0);
	if (list != null && !list.isEmpty()) {
	    for (TcAcciones obj : list) {
		resultsMap.add(obj);
	    }
	}
	return resultsMap;
    }

    /**
     * M�todo para actualizar en n�mero de intentos
     */
//    @Override
//    public int getUpdateIntentos(String rfcUsr, int cont) throws Exception {
//	Query query = getSession().createQuery(
//		"update TwUsuario set ultimaconexion = :ultimaconexion, sesion = :contador where rfce = :rfcUsr");
//	query.setParameter("ultimaconexion", new Date());
//	query.setParameter("contador", aesServices.encriptaCadena(String.valueOf(cont + 1)));
//	query.setParameter("rfcUsr", rfcUsr);
//	return query.executeUpdate();
//
//    }
    
    /**
     * M�todo para actualizar en n�mero de intentos
     */
    @Override
    public int getUpdateIntentos(String rfcUsr, int cont) throws Exception {
	Query query = getSession().createQuery("update TwUsuario set fechaIntentos = :fechaIntentos, sesion = :contador where rfce = :rfcUsr");
	query.setParameter("fechaIntentos", new Date());
	query.setParameter("contador", aesServices.encriptaCadena(String.valueOf(cont + 1)));
	query.setParameter("rfcUsr", rfcUsr);
	return query.executeUpdate();
    }

}
