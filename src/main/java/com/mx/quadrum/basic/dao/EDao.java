package com.mx.quadrum.basic.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

import com.mx.quadrum.basic.util.CriteriaEstrategia;

/**
 * Interface para acceso a la base de datos contiene la definicion de todos los m�todos CRUD
 * @author 
 *
 */
public interface EDao {
    <E> void saveOrupdate(E entity) throws Exception;

    <E> void delete(E entity) throws Exception;

    <E> E find(Class klass, Serializable key) throws Exception;

    <E> List<E> findAll(Class klass) throws Exception;

    <E> Serializable save(E entity) throws Exception;

    <E> E merge(E entity) throws Exception;

    <E> E find(Class klass, Serializable id, Criterion... criterions) throws Exception;

    <E> E findFetch(Class klass, Serializable id, String... fetch) throws Exception;

    <E> List<E> findAll(Class klass, Criterion... criterions) throws Exception;

    <E> List<E> examples(Class klass, Order order, String... excludes) throws Exception;

    <E> List<E> list(Class klass, String hql) throws Exception;

    <E> List<E> exactExamples(Class klass) throws Exception;

    <E> List<E> findRange(Class klass, int[] range, Order... orders) throws Exception;

    <E> int count(Class klass) throws Exception;

    <E> int countComprobante(Class klass, CriteriaEstrategia interceptor) throws Exception;

    <E> double countBig(Class klass, CriteriaEstrategia interceptor) throws Exception;

    <E> int excuteQueryHql(String hql) throws Exception;

    <E> List<E> getQuerySqlList(String sql, Class class1) throws Exception;

    int executeForSql(String sql) throws Exception;

    <E> int excuteQueryHqlParam(String hql, HashMap<String, Object> param) throws Exception;

    <E> List<E> excuteQueryHqlList(String hql) throws Exception;

    <E> List<E> find(Class klass, CriteriaEstrategia interceptor) throws Exception;

    <E> E findUnique(Class klass, CriteriaEstrategia interceptor) throws Exception;

    <E> List<E> findAcciones(Class klass, CriteriaEstrategia interceptor) throws Exception;

    <E> E findFetchList(Class<? extends Object> class1, Serializable id, List<String> innerJoin) throws Exception;

    <T> T findById(Integer valueId, Class klass, String... fetch) throws Exception;

    <E> Serializable getFolio(Object serie) throws Exception;

    <E> void update(E entity) throws Exception;

    <E> Integer count(Class<?> klass, CriteriaEstrategia interceptor) throws Exception;
    
    <T> List<T> selectQueryHqlParamPag(Class<?> clase, String hql, HashMap<String, Object> param, int pagina,
    	    Integer numRegistros) throws Exception;
    
    <T> Number countQueryHqlParam(String hql, HashMap<String, Object> param) throws Exception;
    
    <T> List<T> selectQueryHqlParam(Class<?> clase, String hql, HashMap<String, Object> param) throws Exception;

}
