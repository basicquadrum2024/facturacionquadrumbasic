package com.mx.quadrum.basic.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mx.quadrum.basic.entity.TcPerfil;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

/**
 * Clase que contiene el comportamiento de todos los m�todos CRUD
 * @author 
 *
 */
@Repository
public class EDaoImpl implements EDao {

    @Autowired
    private SessionFactory sessionFactory;

    public static final Logger LOGGER = Logger.getLogger(EDaoImpl.class);

    @SuppressWarnings("unchecked")
    public EDaoImpl() {

    }

    @Override
    public <E> void saveOrupdate(E entity) throws Exception {
	sessionFactory.getCurrentSession().saveOrUpdate(entity);
	sessionFactory.getCurrentSession().flush();
    }

    @Override
    public <E> void delete(E entity) throws Exception {
	sessionFactory.getCurrentSession().delete(entity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E> E find(Class klass, Serializable key) throws Exception {
	return (E) sessionFactory.getCurrentSession().get(klass, key);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E> List<E> findAll(Class klass) throws Exception {
	List<E> lista = sessionFactory.getCurrentSession().createCriteria(klass).list();
	return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E> Serializable save(E entity) throws Exception {
	Serializable id = sessionFactory.getCurrentSession().save(entity);
	sessionFactory.getCurrentSession().flush();
	return id;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E> E merge(E entity) throws Exception {
	return (E) sessionFactory.getCurrentSession().merge(entity);
    }

    @Override
    public <E> E find(Class klass, Serializable id, Criterion... criterions) throws Exception {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	criteria.add(Restrictions.idEq(id));
	for (Criterion criterion : criterions) {
	    criteria.add(criterion);
	}
	return (E) criteria.uniqueResult();
    }

    @Override
    public <E> E findFetch(Class klass, Serializable id, String... fetch) throws Exception {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	criteria.add(Restrictions.idEq(id));
	for (String fet : fetch) {
	    criteria.setFetchMode(fet, FetchMode.JOIN);
	}
	return (E) criteria.uniqueResult();
    }

    @Override
    public <E> List<E> findAll(Class klass, Criterion... criterions) throws Exception {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	for (Criterion criterion : criterions) {
	    criteria.add(criterion);
	}
	List<E> lista = criteria.list();
	return lista;
    }

    @Override
    public <E> List<E> examples(Class klass, Order order, String... excludes) throws Exception {
	Example example = Example.create(this).ignoreCase().enableLike(MatchMode.ANYWHERE).excludeZeroes();
	for (String exlud : excludes) {
	    example.excludeProperty(exlud);
	}
	return examples(klass, order, example);
    }

    private <E> List<E> examples(Class klass, Order order, Example example) {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass).add(example);
	if (order != null) {
	    criteria.addOrder(order);
	}
	return criteria.list();
    }

    @Override
    public <E> List<E> list(Class klass, String hql) throws Exception {
	Query query = sessionFactory.getCurrentSession()
		.createQuery("from " + klass + " E " + (hql == null ? "" : hql));
	return query.list();
    }

    @Override
    public <E> List<E> exactExamples(Class klass) throws Exception {
	Example example = Example.create(this).ignoreCase().excludeZeroes();
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass).add(example);
	return criteria.list();
    }

    public <E> List<E> findRange(Class klass, int[] range, Order... orders) throws Exception {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	for (Order order : orders) {
	    criteria.addOrder(order);
	}
	criteria.setMaxResults(range[1] - range[0]);
	criteria.setFirstResult(range[0]);
	return criteria.list();
    }

    @Override
    public int count(Class klass) throws Exception {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	criteria.setProjection(Projections.rowCount());
	return ((Number) criteria.uniqueResult()).intValue();
    }

    @Override
    public int excuteQueryHql(String hql) throws Exception {
	return sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
    }

    @Override
    public int executeForSql(String sql) throws Exception {
	return sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
    }

    @Override
    public <E> List<E> excuteQueryHqlList(String hql) throws Exception {
	return sessionFactory.getCurrentSession().createQuery(hql).list();
    }

    @Override
    public <E> List<E> getQuerySqlList(String sql, Class klas) throws Exception {
	return sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(klas).list();
    }

    public SessionFactory getSessionFactory() {
	return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
    }

    protected Session getSession() {
	return sessionFactory.getCurrentSession();
    }

    @Override
    public <E> List<E> find(Class klass, CriteriaEstrategia interceptor) throws Exception {
	List<E> objects = null;
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	if (interceptor != null) {
	    interceptor.estrategia(criteria);
	}
	objects = criteria.list();
	return objects;
    }

    @Override
    public <E> Serializable getFolio(Object folio) throws Exception {
	Serializable IdObjeto = sessionFactory.getCurrentSession().save(folio);
	sessionFactory.getCurrentSession().flush();
	return IdObjeto;
    }

    @Override
    public <E> E findUnique(Class klass, CriteriaEstrategia interceptor) throws Exception {
	E objet = null;
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	if (interceptor != null) {
	    interceptor.estrategia(criteria);
	}
	return (E) criteria.uniqueResult();
    }

    @Override
    public <E> List<E> findAcciones(Class klass, CriteriaEstrategia interceptor) throws Exception {

	List<E> objects = null;
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	if (interceptor != null) {
	    interceptor.estrategia(criteria);
	}
	objects = criteria.list();

	if (objects != null && objects.size() > 0) {
	    return objects;// Retorno la lista encriptada aun no se desencripta
	}

	return null;
    }

    @Override
    public <E> E findFetchList(Class<? extends Object> class1, Serializable id, List<String> innerJoin) {
	Session session = null;
	Object listaUsurio = null;
	try {
	    session = sessionFactory.openSession();
	    session.beginTransaction();
	    Criteria criteria = session.createCriteria(class1).add(Restrictions.idEq(id));
	    if (class1.equals(TcPerfil.class)) {
		criteria.setFetchMode("accioneses", FetchMode.JOIN);
	    }
	    for (String string : innerJoin) {
		criteria.setFetchMode(string, FetchMode.JOIN);
	    }
	    listaUsurio = criteria.uniqueResult();
	} catch (Exception e) {
	    if (session != null && session.isConnected()) {
		session.getTransaction().rollback();
	    }

	} finally {
	    if (session != null && session.isConnected()) {
		session.close();
	    }
	}
	return (E) listaUsurio;

    }

    @Override
    public <E> int countComprobante(Class klass, CriteriaEstrategia interceptor) throws Exception {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	criteria.setProjection(Projections.rowCount());
	if (interceptor != null) {
	    interceptor.estrategia(criteria);
	}

	return ((Number) criteria.uniqueResult()).intValue();
    }

    @Override
    public <E> double countBig(Class klass, CriteriaEstrategia interceptor) throws Exception {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	criteria.setProjection(Projections.rowCount());
	if (interceptor != null) {
	    interceptor.estrategia(criteria);
	}
	if (criteria.uniqueResult() != null) {
	    return ((Number) criteria.uniqueResult()).doubleValue();
	}
	return 0.0;
    }

    @Override
    public <T> T findById(Integer valueId, Class klass, String... fetch) throws Exception {
	Criteria cri = sessionFactory.getCurrentSession().createCriteria(klass);
	cri.add(Restrictions.idEq(valueId));
	for (String string : fetch) {
	    cri.setFetchMode(string, FetchMode.JOIN);
	}
	return (T) cri.uniqueResult();
    }

    @Override
    public <E> int excuteQueryHqlParam(String hql, HashMap<String, Object> param) throws Exception {
	Query query = sessionFactory.getCurrentSession().createQuery(hql);
	for (Map.Entry<String, Object> map : param.entrySet()) {
	    query.setParameter(map.getKey(), map.getValue());
	}
	return query.executeUpdate();
    }

    @Override
    public <E> void update(E entity) throws Exception {
	Session session = sessionFactory.getCurrentSession();
	session.update(entity);
    }

    @Override
    public <E> Integer count(Class<?> klass, CriteriaEstrategia interceptor) throws Exception {
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
	criteria.setProjection(Projections.rowCount());
	if (interceptor != null) {
	    interceptor.estrategia(criteria);
	}

	return ((Number) criteria.uniqueResult()).intValue();

    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> List<T> selectQueryHqlParamPag(Class<?> clase, String hql, HashMap<String, Object> param, int pagina,
	    Integer numRegistros) throws Exception {
	Query query = getSessionFactory().getCurrentSession().createSQLQuery(hql);
	for (Map.Entry<String, Object> map : param.entrySet()) {
	    query.setParameter(map.getKey(), map.getValue());
	}
	query.setFirstResult(pagina);
	query.setMaxResults(numRegistros);
	query.setResultTransformer(Transformers.aliasToBean(clase));
	return query.list();
    }

    @Override
    public <T> Number countQueryHqlParam(String hql, HashMap<String, Object> param) throws Exception {
	Query query = getSessionFactory().getCurrentSession().createSQLQuery(hql);
	for (Map.Entry<String, Object> map : param.entrySet()) {
	    query.setParameter(map.getKey(), map.getValue());
	}
	return ((Number) query.uniqueResult());
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> List<T> selectQueryHqlParam(Class<?> clase, String hql, HashMap<String, Object> param) throws Exception {
	Query query = getSessionFactory().getCurrentSession().createSQLQuery(hql);
	for (Map.Entry<String, Object> map : param.entrySet()) {
	    query.setParameter(map.getKey(), map.getValue());
	}
	query.setResultTransformer(Transformers.aliasToBean(clase));
	return query.list();
    }
}
