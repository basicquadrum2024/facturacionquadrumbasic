package com.mx.quadrum.basic.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

/**
 * Interface genérica que contiene la definición de todos los métodos CRUD
 * @author 
 *
 * @param <E>
 */
public interface GenericDao<E> {
	
	  void saveOrupdate(E entity) throws Exception;

	  void delete(E entity)  throws Exception;

	  E find(Serializable key) throws Exception;
	
	  List<E> findAll()  throws Exception;
	
	  Serializable save(E entity) throws Exception;
	
	  E merge(E entity) throws Exception;
	  
	  E find(Serializable id, Criterion... criterions) throws Exception ;
	  
	  E findFetch(Serializable id, String... fetch) throws Exception ;
	  
	  E findFetchJoin(E entity, String... fetch) throws Exception;
	  
	  List<E> findAll(Criterion... criterions) throws Exception ;
	  
	  List<E> examples(Order order, String... excludes) throws Exception ;
	  
	  List<E> list(String hql) throws Exception ;
	  
	  List<E> exactExamples()  throws Exception ;
	  
	  List<E> findRange(int[] range, Order... orders) throws Exception ;
	  
	  int count() throws Exception;
	  
	  int excuteQueryHql(String hql) throws Exception ;
	  
	  List<E> excuteQueryHqlList(String hql) throws Exception;
	  	

}
