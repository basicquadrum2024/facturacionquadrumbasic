package com.mx.quadrum.basic.dao;

import java.util.List;

import com.mx.quadrum.basic.util.CriteriaEstrategia;

/**
 * Interface que contiene la definicion de los metodos principales de b�squeda
 * @author 
 *
 */
public interface EDaoPatrones {

    <E> E findUnique(Class klass, CriteriaEstrategia interceptor) throws Exception;

    <E> List<E> find(Class klass, CriteriaEstrategia interceptor) throws Exception;

    <E> List<E> findAll(Class klass) throws Exception;
}