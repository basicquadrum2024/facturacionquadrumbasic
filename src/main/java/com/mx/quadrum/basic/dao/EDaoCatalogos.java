package com.mx.quadrum.basic.dao;

import java.util.List;

import com.mx.quadrum.basic.util.CriteriaEstrategia;
/**
 * Interface contiene la definici�n de los metodos de b�squeda 
 * @author 
 *
 */
public interface EDaoCatalogos {

	<E> E findUnique(Class klass, CriteriaEstrategia interceptor)throws Exception;

	<E> List<E> find(Class klass,CriteriaEstrategia interceptor)throws Exception;

}
