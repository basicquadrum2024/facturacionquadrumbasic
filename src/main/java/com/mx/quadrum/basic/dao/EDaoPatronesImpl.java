package com.mx.quadrum.basic.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.mx.quadrum.basic.util.CriteriaEstrategia;

/**
 * Clase que contiene el comportamiento de los metodos principales de b�squeda
 * @author Manu
 *
 */
@Repository
public class EDaoPatronesImpl implements EDaoPatrones {

    @Autowired
    @Qualifier(value = "sessionFactoryPatrones")
    private SessionFactory sessionFactoryPatrones;

    public SessionFactory getSessionFactoryGeneral() {
	return sessionFactoryPatrones;
    }

    public void setSessionFactoryGeneral(SessionFactory sessionFactoryGeneral) {
	this.sessionFactoryPatrones = sessionFactoryGeneral;
    }

    @Override
    public <E> E findUnique(Class klass, CriteriaEstrategia interceptor) throws Exception {
	Criteria criteria = sessionFactoryPatrones.getCurrentSession().createCriteria(klass);
	if (interceptor != null) {
	    interceptor.estrategia(criteria);
	}
	return (E) criteria.uniqueResult();
    }

    @Override
    public <E> List<E> find(Class klass, CriteriaEstrategia interceptor) throws Exception {
	List<E> objects = null;
	Criteria criteria = sessionFactoryPatrones.getCurrentSession().createCriteria(klass);
	if (interceptor != null) {
	    interceptor.estrategia(criteria);
	}
	objects = criteria.list();
	if (objects != null && objects.size() > 0) {
	    objects = objects;
	}

	return objects;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E> List<E> findAll(Class klass) throws Exception {
	List<E> lista = sessionFactoryPatrones.getCurrentSession().createCriteria(klass).list();
	return lista;
    }

}