package com.mx.quadrum.basic.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

/**
 * Clase que contiene el comportamiento de los metodos de b�squeda 
 * @author 
 *
 */
@Repository
public class EDaoCatalogosImpl implements EDaoCatalogos{
		
		private final static Logger LOGGER=Logger.getLogger(EDaoCatalogosImpl.class);
		
		@Autowired
		@Qualifier(value = "sessionFactoryCatalogos")
		private SessionFactory sessionFactoryCatalogos;
		
		public SessionFactory getSessionFactoryGeneral() {
			return sessionFactoryCatalogos;
		}

		public void setSessionFactoryGeneral(SessionFactory sessionFactoryGeneral) {
			this.sessionFactoryCatalogos = sessionFactoryGeneral;
		}

		@Override
		public <E> E findUnique(Class klass, CriteriaEstrategia interceptor) throws Exception {
			Criteria criteria=sessionFactoryCatalogos.getCurrentSession().createCriteria(klass);
			if(interceptor!=null){
				interceptor.estrategia(criteria);
			}
			return (E) criteria.uniqueResult();
		}

		@Override
		public <E> List<E> find(Class klass, CriteriaEstrategia interceptor) throws Exception {
			List<E> objects = null;
			Criteria criteria = sessionFactoryCatalogos.getCurrentSession().createCriteria(
					klass);
			if (interceptor != null) {
				interceptor.estrategia(criteria);
			}
			objects = criteria.list();
			if (objects != null && objects.size() > 0) {
				objects =objects;
			}

			return objects;
		}
}
