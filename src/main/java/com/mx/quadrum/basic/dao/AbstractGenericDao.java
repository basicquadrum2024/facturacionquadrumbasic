
package com.mx.quadrum.basic.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.mx.quadrum.basic.aes.AesServices;

/**
 * Clase para acceso a base de datos, <br>
 * que contiene la definición de método principales.
 * 
 * @author Pedro Romero Martinez.
 * 
 * @param <T>
 *            clase/objeto a realizar alguna operación.
 */
public abstract class AbstractGenericDao<E> implements GenericDao<E> {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private AesServices aesServices;

	protected Class<? extends E> klass;

	@SuppressWarnings("unchecked")
	public AbstractGenericDao() {
		klass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass())
		        .getActualTypeArguments()[0];
	}

	@Override
	public void saveOrupdate(E entity) throws Exception {
//		sessionFactory.getCurrentSession().saveOrUpdate(aesServices.encriptaObject(entity));
		sessionFactory.getCurrentSession().saveOrUpdate(entity);
	}

	@Override
	public void delete(E entity) throws Exception {
		sessionFactory.getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public E find(Serializable key) throws Exception {
		return (E) sessionFactory.getCurrentSession().get(klass, key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> findAll() throws Exception {
		return sessionFactory.getCurrentSession().createCriteria(klass).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Serializable save(E entity) throws Exception {
		return sessionFactory.getCurrentSession().save(klass);
	}

	@SuppressWarnings("unchecked")
	@Override
	public E merge(E entity) throws Exception {
		return (E) sessionFactory.getCurrentSession().merge(entity);
	}

	@Override
	public E find(Serializable id, Criterion... criterions) throws Exception {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
		criteria.add(Restrictions.idEq(id));
		for (Criterion criterion : criterions) {
			criteria.add(criterion);
		}
		return (E) aesServices.desEncritaObject(criteria.uniqueResult());
	}

	@Override
	public E findFetch(Serializable id, String... fetch) throws Exception {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
		criteria.add(Restrictions.idEq(id));
		for (String fet : fetch) {
			criteria.setFetchMode(fet, FetchMode.JOIN);
		}
		return (E) (List<E>) criteria.uniqueResult();
//		return (E) aesServices.desEncriptaListObject((List<E>) criteria.uniqueResult());
	}

	@Override
	public E findFetchJoin(E entity, String... fetch) throws Exception {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
		Example example = Example.create(entity).excludeZeroes();
		criteria.add(example);
		for (String fet : fetch) {
			criteria.setFetchMode(fet, FetchMode.JOIN);
		}
		return (E) aesServices.desEncritaObject(criteria.uniqueResult());
	}
	@Override
	public List<E> findAll(Criterion... criterions) throws Exception {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
		for (Criterion criterion : criterions) {
			criteria.add(criterion);
		}
		return aesServices.desEncriptaListObject(criteria.list());
	}

	@Override
	public List<E> examples(Order order, String... excludes) throws Exception {
		Example example = Example.create(this).ignoreCase().enableLike(MatchMode.ANYWHERE)
		        .excludeZeroes();
		for (String exlud : excludes) {
			example.excludeProperty(exlud);
		}
		return examples(order, example);
	}

	private List<E> examples(Order order, Example example) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass).add(example);
		if (order != null) {
			criteria.addOrder(order);
		}
		return criteria.list();
//		return aesServices.desEncriptaListObject(criteria.list());
	}

	@Override
	public List<E> list(String hql) throws Exception {
		Query query = sessionFactory.getCurrentSession().createQuery(
		        "from " + klass + " E " + (hql == null ? "" : hql));
		return query.list();
//		return aesServices.desEncriptaListObject(query.list());
	}

	@Override
	public List<E> exactExamples() throws Exception {
		Example example = Example.create(this).ignoreCase().excludeZeroes();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass).add(example);
		return criteria.list();
//		return aesServices.desEncriptaListObject(criteria.list());
	}

	public List<E> findRange(int[] range, Order... orders) throws Exception {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
		for (Order order : orders) {
			criteria.addOrder(order);
		}
		criteria.setMaxResults(range[1] - range[0]);
		criteria.setFirstResult(range[0]);
		return criteria.list();
//		return aesServices.desEncriptaListObject(criteria.list());
	}

	@Override
	public int count() throws Exception {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(klass);
		criteria.setProjection(Projections.rowCount());
		return ((Number) criteria.uniqueResult()).intValue();
	}

	@Override
	public int excuteQueryHql(String hql) throws Exception {
		return sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
	}

	@Override
	public List<E> excuteQueryHqlList(String hql) throws Exception {
		return sessionFactory.getCurrentSession().createQuery(hql).list();
//		return aesServices.desEncriptaListObject(sessionFactory.getCurrentSession().createQuery(hql).list());
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

}
