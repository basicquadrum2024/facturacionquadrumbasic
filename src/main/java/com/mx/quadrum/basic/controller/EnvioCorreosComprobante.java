//package com.mx.quadrum.basic.controller;
//
//import java.io.File;
//import java.io.Serializable;
//import java.nio.file.Files;
//import java.util.List;
//
//import javax.servlet.http.HttpServletRequest;
//
//import mx.bigdata.sat.cfdi.v32.schema.TimbreFiscalDigital;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.mx.quadrum.basic.adenda.CartaP;
//import com.mx.quadrum.basic.aes.AesServices;
//import com.mx.quadrum.basic.entity.Comprobante;
//import com.mx.quadrum.basic.services.ComprobanteServices;
//import com.mx.quadrum.basic.services.HistorialServices;
//import com.mx.quadrum.basic.util.ParametroApp;
//import com.mx.quadrum.basic.util.Utilerias;
//import com.mx.quadrum.basic.wrapper.CorreoComprobante;
//import com.mx.quadrum.basic.wrapper.agregarNota;
//import com.mx.quadrum.sat.util.Utils;
//
//@RestController
//@RequestMapping(value = "/envioCorreosComprobante")
//public class EnvioCorreosComprobante implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//	private static final Logger LOGGER = Logger.getLogger(ComprobanteController.class);
//
//	@Autowired
//	private ComprobanteServices comprobanteServices;
//	
//	@Autowired
//	private AesServices aesServices;
//	
//	
//	
//	@Autowired
//	private HistorialServices historialServices;
//
//	@RequestMapping(value = "/cancelarComprobante.json", method = RequestMethod.POST)
//	private List<Comprobante> cancelarComprobante(@RequestBody List<Comprobante> comprobante) throws Exception {
//		for (Comprobante c : comprobante) {
//
//		}
//
//		return null;
//
//	}
//
//	@RequestMapping(value = "/enviarCorreos.json", method = RequestMethod.POST)
//	private String enviarCorreos(@RequestBody CorreoComprobante comprobante,HttpServletRequest request) throws Exception {
//
//		String respuesta = null;
//		for (Comprobante comp : comprobante.getComprobante()) {
//			for (String coreo : comprobante.getCorreos()) {
//				String ruta = Utilerias.direccionFile(ParametroApp.getRaizHistEnviados(), comp.getFechacreacion(),
//						comp.getContribuyente().getRfce(), comp.getUuid());
//				mx.bigdata.sat.cfdi.v32.schema.Comprobante comprobantebig = DocumentosController.convertirStringAComprobanteCFDv32(Files.readAllBytes(new File(ruta).toPath()),mx.bigdata.sat.cfdi.v32.schema.Comprobante.class,CartaP.class);
//				
//				TimbreFiscalDigital timbreFiscalDigital = (TimbreFiscalDigital) Utils
//						.getObjectoComplemento(comprobantebig.getComplemento().getAny(), TimbreFiscalDigital.class);
//				com.mx.quadrum.basic.entity.Comprobante comprobanteBD = comprobanteServices
//				        .findUniquebyUuid(aesServices.encriptaCadena(timbreFiscalDigital.getUUID()));
//
//				String pdf = DocumentosController.imprimeRecibo(comprobantebig, "crearPdf", comp.getUuid() + ".pdf", null,comprobanteBD,timbreFiscalDigital);
//
//				if (ruta != null) {
//					File file = new File(ruta);
//					if (file.exists()) {
//
//						Utilerias.enviarCorreoSinAcuse(coreo, "envio de archivos", "envio de archivos", ruta, pdf);
//						respuesta = "se envio correo";
//						Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//						
//						Utilerias.guardarHistorial("El usuario oprimi� enviar correo electronico",
//						        Utilerias.getUserSession(auth), "El usuario "
//						                + Utilerias.getUserSession(auth).getIdUsuario()
//						                + " envio correo electronico", historialServices, request,
//						        null, null);
//
//					} else {
//
//						respuesta = "No existe el XML";
//
//					}
//				}
//			}
//		}
//		
//
//		return respuesta;
//	}
//
//	@RequestMapping(value = "/agregarNota.json", method = RequestMethod.POST)
//	private String agregarNotas(@RequestBody agregarNota nota) throws Exception {
//		String respuesta = null;
//		for (Comprobante com : nota.getComprobante()) {
//			for (String notas : nota.getNotas()) {
//
//				com.setNota(notas);
//
//				comprobanteServices.actualizarComprobante(com);
//
//			}
//
//		}
//		return respuesta;
//	}
//
//}
