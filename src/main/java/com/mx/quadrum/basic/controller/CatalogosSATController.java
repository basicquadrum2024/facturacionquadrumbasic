package com.mx.quadrum.basic.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.catalogosSAT.ClaveProdServ;
import com.mx.quadrum.basic.entity.catalogosSAT.ClaveUnidad;
import com.mx.quadrum.basic.entity.catalogosSAT.CodigoPostal;
import com.mx.quadrum.basic.entity.catalogosSAT.Exportacion;
import com.mx.quadrum.basic.entity.catalogosSAT.FormaPago;
import com.mx.quadrum.basic.entity.catalogosSAT.Impuesto;
import com.mx.quadrum.basic.entity.catalogosSAT.MetodoPago;
import com.mx.quadrum.basic.entity.catalogosSAT.Moneda;
import com.mx.quadrum.basic.entity.catalogosSAT.ObjetoImpuesto;
import com.mx.quadrum.basic.entity.catalogosSAT.Pais;
import com.mx.quadrum.basic.entity.catalogosSAT.RegimenFiscal;
import com.mx.quadrum.basic.entity.catalogosSAT.TasaOcuota;
import com.mx.quadrum.basic.entity.catalogosSAT.TipoComprobante;
import com.mx.quadrum.basic.entity.catalogosSAT.TipoFactor;
import com.mx.quadrum.basic.entity.catalogosSAT.TipoRelacion;
import com.mx.quadrum.basic.entity.catalogosSAT.UsoCfdi;
import com.mx.quadrum.basic.services.CatalogosSATServices;
import com.mx.quadrum.basic.services.ClaveProdServServices;
import com.mx.quadrum.basic.services.ClaveUnidadServices;
import com.mx.quadrum.basic.services.FormaPagoServices;
import com.mx.quadrum.basic.services.ImpuestoServices;
import com.mx.quadrum.basic.services.MetodoPagoServices;
import com.mx.quadrum.basic.services.MonedaServices;
import com.mx.quadrum.basic.services.TasaOcuotaServices;
import com.mx.quadrum.basic.services.TipoComprobanteServices;
import com.mx.quadrum.basic.services.TipoFactorServices;
import com.mx.quadrum.basic.services.TipoRelacionServices;
import com.mx.quadrum.basic.services.catalogos.CPaisServices;
import com.mx.quadrum.basic.services.catalogos.CPostalServices;
import com.mx.quadrum.basic.services.catalogos.CUsoCfdiServices;
import com.mx.quadrum.basic.util.Anexo20;

/**
 * Clase controlador REST contiene metodos para adminsitrar catalogosSAT
 * 
 * @author
 *
 */
@RestController
@RequestMapping(value = "/catalogosSATController")
public class CatalogosSATController {

	@Autowired
	CatalogosSATServices catalogosServices;

	@Autowired
	private CPaisServices cPaisServices;

	@Autowired
	private CUsoCfdiServices cUsoCfdiServices;

	@Autowired
	private ImpuestoServices impuestoServices;

	@Autowired
	private TipoFactorServices tipoFactorServices;

	@Autowired
	private FormaPagoServices formaPagoServices;

	@Autowired
	private MonedaServices monedaServices;

	@Autowired
	private MetodoPagoServices metodoPagoServices;

	@Autowired
	private TipoComprobanteServices tipoComprobanteServices;

	@Autowired
	private ClaveUnidadServices claveUnidadServices;

	@Autowired
	private ClaveProdServServices claveProdServServices;

	@Autowired
	private TasaOcuotaServices tasaOcuotaServices;

	@Autowired
	private TipoRelacionServices tipoRelacionServices;

	@Autowired
	private CPostalServices codigoPostalServices;

	/**
	 * Metodo que devuelve una bandera correspondiente si existe el lugar de
	 * expedici�n devuelve true si no existe devuelve false
	 * 
	 * @param tcContribuyente
	 * @return retorna variable booleana: true si existe el lugar de expedicion,
	 *         false si no existe el lugar de expedicion dentro del catalogo
	 *         codigoPostal
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscaLugarExpedicion.json", method = RequestMethod.POST)
	public boolean consultarCodigoPostal(@RequestBody TcContribuyente tcContribuyente) throws Exception {
		boolean respuesta = true;
		CodigoPostal codigoPostal = codigoPostalServices.buscarCodigoPostal(tcContribuyente.getLugarExpedicion());
		if (null == codigoPostal)
			respuesta = false;
		return respuesta;
	}

	/**
	 * M�todo que devuelve una lista de regimen fiscal correspondiente al RFC seg�n
	 * su longitud
	 * 
	 * @param rfc
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscaRegimenFiscal/{rfc}.json", method = RequestMethod.POST)
	public List<RegimenFiscal> buscaRegimenFiscal(@PathVariable("rfc") String rfc) throws Exception {
		String tipo = "";
		List<RegimenFiscal> lista = null;
		if (rfc.length() == 13) {
			tipo = "T,F";
			lista = catalogosServices.buscarRegimenFiscal(tipo.split(","));
		} else if (rfc.length() == 12) {
			tipo = "T,M";
			lista = catalogosServices.buscarRegimenFiscal(tipo.split(","));
		}
		return lista;
	}
	
	

	/**
	 * M�todo que obtiene el cat�logo de paises
	 * 
	 * @return cat�logo de paises
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarTodosPaises.json", method = RequestMethod.POST)
	public List<Pais> buscarTodosPaises() throws Exception {
		return cPaisServices.buscarTodosPaises();
	}

	/**
	 * M�todo obtiene la lista de usos de CFDI, seg�n corresponde a la longitud del
	 * RFC
	 * 
	 * @param rfc
	 * @return lista de usoCFDI
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarUsoCfdiPorTipoPersona/{rfc}/.json", method = RequestMethod.POST)
	public List<UsoCfdi> buscarUsoCfdiPorTipoPersona(@PathVariable("rfc") String rfc) throws Exception {
		String tipo = "T,M";
		if (rfc.length() == 13) {
			tipo = "T,F";
		}
		return cUsoCfdiServices.buscarUsoCfdiPorTipoPersona(tipo.split(","));
	}

	/**
	 * M�todo que obtiene el tipo de impuesto que se puede aplicar a traslado
	 * 
	 * @return lista de c_impuesto (tipo de impuesto)
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarImpuestosTrasladados.json", method = RequestMethod.POST)
	public List<Impuesto> buscarImpuestosTrasladados() throws Exception {
		return impuestoServices.buscarImpuestosTrasladados();
	}
	
	
	@RequestMapping(value = "/buscarExportacion.json", method = RequestMethod.POST)
	public List<Exportacion> buscarExportacion() throws Exception {
		return catalogosServices.buscarExportacion();
	}
	
	@RequestMapping(value = "/buscarImpustoObjeto.json", method = RequestMethod.POST)
	public List<ObjetoImpuesto> buscarObjeto() throws Exception {
		return catalogosServices.buscarObjetoImpuesto();
	}

	/**
	 * M�todo que obtiene el tipo de impuesto que se puede aplicar a retenido
	 * 
	 * @return lista de c_impuesto (tipo de impuesto)
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarImpuestosRetenidos.json", method = RequestMethod.POST)
	public List<Impuesto> buscarImpuestosRetenidos() throws Exception {
		return impuestoServices.buscarImpuestosRetenidos();
	}

	/**
	 * M�todo que busca en el cat�logo TipoFactor seg�n corresponda al impuesto
	 * 
	 * @param impuesto
	 * @return lista de tipoFactor
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarTiposFactor/{impuesto}/.json", method = RequestMethod.POST)
	public List<TipoFactor> buscarTiposFactor(@PathVariable("impuesto") String impuesto) throws Exception {
		List<String> impuestos = new ArrayList<>();
		impuestos.add("");
		if (impuesto.equals("R")) {
			impuestos.add("Exento");
		}
		String[] claves = new String[impuestos.size()];
		claves = impuestos.toArray(claves);
		return tipoFactorServices.buscarTiposFactor(claves);
	}

	/**
	 * M�todo que devuelve el cat�logo formas de pago
	 * 
	 * @return lista tipo FormaPago
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarFormasPago.json", method = RequestMethod.POST)
	public List<FormaPago> buscarFormasPago() throws Exception {
		return formaPagoServices.buscarFormasPago();
	}

	/**
	 * M�todo que devuelve el cat�logo de moneda
	 * 
	 * @return lista tipo Moneda
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarMonedas.json", method = RequestMethod.POST)
	public List<Moneda> buscarMonedas() throws Exception {
		return monedaServices.buscarMonedas();
	}

	/**
	 * M�todo que devuelve el cat�logo de tipo de comprobante
	 * 
	 * @return lista de TipoComprobante
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarTiposComprobante.json", method = RequestMethod.POST)
	public List<TipoComprobante> buscarTiposComprobante() throws Exception {
		return tipoComprobanteServices.buscarTiposComprobante("I", "E", "T");
	}

	/**
	 * M�todo que devuelve el cat�logo Metodo de pago
	 * 
	 * @return lista de MetodoPago
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarMetodosPago.json", method = RequestMethod.POST)
	public List<MetodoPago> buscarMetodosPago() throws Exception {
		return metodoPagoServices.buscarMetodosPago();
	}

	/**
	 * M�todo que devuelve el cat�logo tipo de relaci�n
	 * 
	 * @return lista TipoRelacion
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarTipoRelacion.json", method = RequestMethod.POST)
	public List<TipoRelacion> buscarTipoRelacion() throws Exception {
		return tipoRelacionServices.buscarTipoRelacion();
	}

	/**
	 * M�todo que devuelve los estados de la replublica Mexicona
	 * 
	 * @return lista de tipo String con el nombre de los estados de la replublica
	 *         Mexicona
	 */
	@RequestMapping(value = "/buscarEstados.json", method = RequestMethod.POST)
	public List<String> buscarEstados() {
		return Arrays.asList(Anexo20.listaEstados());
	}

	/**
	 * M�todo que devuelve lista de ClaveUnidad, filtradas por nombre (clave)
	 * 
	 * @param nombre correspondiente a la Unidad del cat�logo de ClaveUnidad
	 * @return lista de ClaveUnidad
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarClavesUnidadLikeNombre/{nombre}/.json", method = RequestMethod.POST)
	public List<ClaveUnidad> buscarClavesUnidadLikeNombre(@PathVariable("nombre") String nombre) throws Exception {
		return claveUnidadServices.buscarClavesUnidadLikeNombre(nombre);
	}

	/**
	 * M�todo que devuelve lista de ClaveUnidad, filtradas por descripci�n (nombre
	 * de la clave)
	 * 
	 * @param descripcion correspondiente a la Unidad del cat�logo de ClaveUnidad
	 * @return lista de ClaveUnidad
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarClaveProdServLikeNombre/{descripcion}/.json", method = RequestMethod.POST)
	public List<ClaveProdServ> buscarClaveProdServLikeNombre(@PathVariable("descripcion") String descripcion)
			throws Exception {
		return claveProdServServices.buscarClaveProdServLikeNombre(descripcion);
	}

	/**
	 * M�todo que devuelve lista de tasa o cuota, tomando en cuenta el impuesto y el
	 * factor
	 * 
	 * @param impuesto
	 * @param factor
	 * @param tipoImpuesto
	 * @return lista de TasaOcuota
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarTasasOCuotas/{impuesto}/{factor}/.json", method = RequestMethod.POST)
	public List<TasaOcuota> buscarTasasOCuotas(@PathVariable("impuesto") String impuesto,
			@PathVariable("factor") String factor, @RequestBody String[] tipoImpuesto) throws Exception {
		return tasaOcuotaServices.buscarTasasOCuotas(elegirTipoImpuesto(impuesto), primerLetraMayuscula(factor),
				tipoImpuesto);
	}

	/**
	 * M�todo valida la existencia de la clave de unidad
	 * 
	 * @param clave
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/validarExistenciaClaveUnidad/{clave}/.json", method = RequestMethod.POST)
	public boolean validarExistenciaClaveUnidad(@PathVariable("clave") String clave) throws Exception {
		return claveUnidadServices.buscarClavePorClave(clave) == null ? false : true;
	}

	/**
	 * M�todo que valida la existencia del proveedor de servicio
	 * 
	 * @param clave
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/validarExistenciaProvedorServicio/{clave}/.json", method = RequestMethod.POST)
	public boolean validarExistenciaProvedorServicio(@PathVariable("clave") String clave) throws Exception {
		return claveProdServServices.buscarClaveProdServClave(clave) == null ? false : true;
	}

	/**
	 * M�todo devuelve el String correspondiente a la clave del tipo impuesto
	 * 
	 * @param impuesto
	 * @return
	 */
	private String elegirTipoImpuesto(final String impuesto) {

		String respuesta = null;
		switch (impuesto) {
		case "001":
			respuesta = "ISR";
			break;

		case "002":
			respuesta = "IVA";
			break;
		case "003":
			respuesta = "IEPS";
			break;
		default:
			break;
		}
		return respuesta;
	}

	/**
	 * M�todo que devuelve la primer letra de un String en may�scula
	 * 
	 * @param factor String a procesar
	 * @return
	 */
	private String primerLetraMayuscula(final String factor) {
		String respuesta = null;
		if (factor != null && !factor.isEmpty()) {
			char[] caracteres = factor.toCharArray();
			caracteres[0] = Character.toUpperCase(caracteres[0]);
			respuesta = new String(caracteres);
		}
		return respuesta;
	};

	/**
	 * M�todo que devuelve una lista con los tipoFactor correspondiente al impuesto
	 * 
	 * @param impuesto
	 * @param tipoImpuesto
	 * @return lista String
	 * @throws Exception
	 */
	@RequestMapping(value = "/buscarTiposFactorTipoImpuesto/{impuesto}/.json", method = RequestMethod.POST)
	public List<String> buscarTiposFactorPorImpuesto(@PathVariable("impuesto") String impuesto,
			@RequestBody String[] tipoImpuesto) throws Exception {
		Impuesto impuestoObj = impuestoServices.buscarImpuestosClave(impuesto);
		List<String> tipoFactor = new ArrayList<>();
		tipoFactor = tasaOcuotaServices.buscarTasasOCuotasImpuesto(impuestoObj.getDescripcion(), tipoImpuesto);

		if (tipoImpuesto[0].equals("T")) {
			tipoFactor.add("Exento");
		}
		return tipoFactor;
	}

}
