package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcConceptos;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.TcConceptosServices;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante.Conceptos.Concepto;

@RestController
public class ConceptosV40Controller implements Serializable {
	
	public final static Logger LOGGER = Logger.getLogger(ConceptosV40Controller.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private EmisorServices emisorServices;

	@Autowired
	private TcConceptosServices tcConceptosServices;

	@Autowired
	private AesServices aesServices;

	@Autowired
	private PistasServices pistasServices;

	@RequestMapping(value = "/conceptos_v40/crearConceptoCfdiv40.json", method = RequestMethod.POST)
	public Concepto crearConceoCfdiv40() throws Exception {
		Concepto concepto = new Concepto();
		Concepto.Impuestos impuestos = new Concepto.Impuestos();
		Concepto.Impuestos.Traslados traslados = new Concepto.Impuestos.Traslados();
		Concepto.Impuestos.Retenciones retenciones = new Concepto.Impuestos.Retenciones();
		impuestos.setTraslados(traslados);
		impuestos.setRetenciones(retenciones);
		concepto.setImpuestos(impuestos);
		return concepto;

	}

	@RequestMapping(value = "/conceptos_v40/buscarConceptosPorContribuyente.json", method = RequestMethod.POST)
	public List<TcConceptos> buscarConceptosPorContribuyente(HttpServletRequest request) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		LOGGER.error(twUsuario.getRfce());
		TcContribuyente tcContribuyente = emisorServices
				.buscarPorRfcEmisor(aesServices.encriptaCadena(twUsuario.getRfce()));
		LOGGER.error(tcContribuyente.getRfc());

		return tcConceptosServices.buscarConceptosPorContribuyente(tcContribuyente);
	}

	/**
	 * M�todo guarda en base de datos el objeto concepto, valida que no exista un
	 * concepto con la misma descripci�n
	 * 
	 * @param concepto
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "/conceptos_v40/guardarConcepto.json", method = RequestMethod.POST)
	public void guardarConcepto(@RequestBody Concepto concepto, HttpServletRequest request) throws Exception {
		System.out.println(ToStringBuilder.reflectionToString(concepto));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		TcContribuyente tcContribuyente = emisorServices
				.buscarPorRfcEmisor(aesServices.encriptaCadena(twUsuario.getRfce()));
		TcConceptos tcConceptos = tcConceptosServices
				.buscarConceptoPorDescripcionContribuyente(concepto.getDescripcion(), tcContribuyente);
		if (null == tcConceptos) {
			tcConceptos = new TcConceptos();
			tcConceptos.setDescripcion(concepto.getDescripcion());
			tcConceptos.setTcContribuyente(tcContribuyente);
			Utilerias.eventosAdministrador(Utilerias.CONCEPTO,
					"CONCEPTO " + concepto.getDescripcion() + " GENERADO CORRECTAMENTE.", twUsuario, "INSERT",
					pistasServices, request);
			tcConceptosServices.guardarTcConcepto(tcConceptos);

		} else {
			Utilerias.eventosAdministrador(Utilerias.CONCEPTO,
					"EL CONCEPTO " + concepto.getDescripcion() + " YA ESTA DADO DE ALTA.", twUsuario, "INSERT",
					pistasServices, request);
		}
	}

	@RequestMapping(value = "/conceptos_v40/crearTraslado.json", method = RequestMethod.POST)
	public Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado crearTraslado() {
		return new Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado();
	}

	@RequestMapping(value = "/conceptos_v40/crearRetencion.json")
	public Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion crearRetencion() {
		return new Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion();

	}

	@RequestMapping(value = "/conceptos_v40/crearParte.json")
	public Comprobante.Conceptos.Concepto.Parte crearParte() {
		return new Comprobante.Conceptos.Concepto.Parte();
	}

	@RequestMapping(value = "/conceptos_v40/crearParteInformacionAduanera.json", method = RequestMethod.POST)
	public Comprobante.Conceptos.Concepto.Parte.InformacionAduanera crearParteInformacionAduanera(){
	return new Comprobante.Conceptos.Concepto.Parte.InformacionAduanera();

}
}