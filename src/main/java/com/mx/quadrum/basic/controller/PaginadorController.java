//package com.mx.quadrum.basic.controller;
//
//import java.util.Date;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.validation.Valid;
//
//import org.apache.commons.lang.builder.ToStringBuilder;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Controller;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.mx.quadrum.basic.entity.Historial;
//import com.mx.quadrum.basic.services.HistorialServices;
//import com.mx.quadrum.basic.services.IUsuarioServices;
//import com.mx.quadrum.basic.util.Utilerias;
//import com.mx.quadrum.basic.wrapper.Paginacion;
//
//@Controller
//@RequestMapping(value = "/paginacion")
//public class PaginadorController extends AbstractControllerHandler {
//
//	@Autowired
//	private IUsuarioServices iServices;
//	
//	@Autowired
//	private HistorialServices historialServices;
//
//	private static final Logger logger = Logger.getLogger(PaginadorController.class);
//
//	@RequestMapping(value = "/getPaginationObjetBusca", method = RequestMethod.POST)
//	@ResponseBody
//	public Paginacion getPaginationObjetBusca(@Valid @RequestBody Paginacion pagina, BindingResult results, HttpServletRequest request) {
//		try {
//			String campoBetween = null;
//			Date date1 = null;
//			Date date2 = null;
//			String valor = "";
//
//			if (pagina.getBetween() != null && !pagina.getBetween().isEmpty()) {
//				String[] dateString1 = pagina.getBetween().get(0);
//				String[] dateString2 = pagina.getBetween().get(1);
//				campoBetween = dateString1[0];
//				date1 = Utilerias.formatDateTime.get().parse(dateString1[1].toString().replace("/", "-"));
//				date2 = Utilerias.formatDateTime.get().parse(dateString2[1].toString().replace("/", "-"));
//			}
//			pagina = iServices.getPaginacionBus(pagina, campoBetween, date1, date2, false);
//			if (pagina.getListBus() == null || pagina.getListBus().isEmpty()) {
//				pagina.setError("No hay registros con los criterios selecccioandos!.");
//			}
//			if (pagina.getConjuncion() != null && !pagina.getConjuncion().isEmpty()) {
//				for (String conjuncion : pagina.getConjuncion()) {
//					String[] campo = conjuncion.split("[|]");
//					for (int i = 0; i < campo.length; i++) {
//						valor = valor + campo[i] + " ";
//					}
//				}
//				logger.error("valor"+valor);
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		Utilerias.guardarHistorial("El usuario oprimi� el bot�n buscar comprobante", Utilerias.getUserSession(auth), "El usuario"+Utilerias.getUserSession(auth).getLogin()+" busco por "+valor,historialServices ,request,null,null);
////			     Utilerias.guardarHistorial("El usuario oprimi� el bot�n buscar comprobante", Utilerias.getUserSession(auth), "El usuario:" + Utilerias.getUserSession(auth).getLogin() + " busco por: " + valor, historialServices, request);
//			}
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			logger.error("Ocurrio un error al realizar la consulta del paginador causa:--> " + ex.getCause()
//					+ Utilerias.imprimeStackError(ex.getStackTrace()));
//			pagina.setError("Ocurrio un error al realizar la consulta del paginador ");
//		}
//
//		return pagina;
//	}
//
//	@RequestMapping(value = "/getPaginationBuscaLike", method = RequestMethod.POST)
//	@ResponseBody
//	public Paginacion getPaginationObjetLike(@Valid @RequestBody Paginacion pagina, BindingResult results) {
//		try {
//			String campoBetween = null;
//			Date date1 = null;
//			Date date2 = null;
//			if (pagina.getBetween() != null && !pagina.getBetween().isEmpty()) {
//				logger.debug(ToStringBuilder.reflectionToString(pagina.getBetween().get(0)));
//				logger.debug(ToStringBuilder.reflectionToString(pagina.getBetween().get(1)));
//				String[] dateString1 = pagina.getBetween().get(0);
//				String[] dateString2 = pagina.getBetween().get(1);
//				campoBetween = dateString1[0];
//				date1 = Utilerias.formatDateTime.get().parse(dateString1[1].toString());
//				date2 = Utilerias.formatDateTime.get().parse(dateString2[1].toString());
//			}
//			pagina = iServices.getPaginacionBus(pagina, campoBetween, date1, date2, true);
//			if (pagina.getListBus() == null || pagina.getListBus().isEmpty()) {
//				// pagina.setError("No hay registros con los criterios
//				// selecccioandos!.");
//			}
//
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			logger.error("Ocurrio un error al realizar la consulta del paginador causa:--> " + ex.getCause()
//					+ Utilerias.imprimeStackError(ex.getStackTrace()));
//			pagina.setError("Ocurrio un error al realizar la consulta del paginador ");
//		}
//
//		return pagina;
//	}
//}
