package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.entity.Paginacion;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.IUsuarioServices;
/**
 * Esta clase es un controlador REST contiene m�todos para el modulo de administrador
 *  herada de AbstractControllerHandler e implementa Serializable
 * @author
 *
 */
@RestController
@RequestMapping(value = "/adminController")
public class AdministradorController extends AbstractControllerHandler implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    final static Logger LOGGER = Logger.getLogger(UsuarioController.class);

    @Autowired
    private IUsuarioServices usuarioServices;

    /**
     * M�todo para buscar usuarios
     * @param currentPage p�gina actual 
     * @param pageSize tama�o de la p�gina
     * @return Paginacion, con los resultados de la busqueda
     * @throws Exception
     */
    @RequestMapping(value = "/findUsuario/{currentPage}/{pageSize}/.json", method = RequestMethod.POST)
    public Paginacion<TwUsuario> findRol(@PathVariable("currentPage") Integer currentPage,
	    @PathVariable("pageSize") Integer pageSize) throws Exception {

	Paginacion<TwUsuario> pagination = new Paginacion<TwUsuario>();
	int start = (currentPage - 1) * pageSize;
	List<TwUsuario> userDesencriptado = new ArrayList<TwUsuario>();
	List<TwUsuario> lista = usuarioServices.findUsuarioPagination(start, pageSize);

	pagination.setList(lista);
	pagination.setTotalResults(usuarioServices.findUsuarioPaginationCount());
	return pagination;
    }

    /**
     * M�todo guarda el objeto TwUsuario
     * @param usuario
     * @throws Exception
     */
    @RequestMapping(value = "/save.json", method = RequestMethod.POST)
    public void guardaCorreo(@RequestBody TwUsuario usuario) throws Exception {
	usuarioServices.saveorUpdate(usuario);
    }

    /**
     * M�todo que elimina TwUsuario
     * @param usuario
     * @throws Exception
     */
    @RequestMapping(value = "/delete.json", method = RequestMethod.POST)
    public void eliminarUser(@RequestBody TwUsuario usuario) throws Exception {
	usuarioServices.delete(usuario);
    }

    // @RequestMapping(value = "/perfiles.json", method = RequestMethod.POST)
    // public List<TcPerfil> listaPrivilegios() throws Exception {
    // return usuarioServices.buscarPerfiles();
    // }

}
