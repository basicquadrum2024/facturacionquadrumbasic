package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.utileria2021.cfdi.clientehttp.wrapper.wscatalogoscfdi40.ReceptorDto;
import com.mx.quadrum.utileria2021.cfdi.clientehttp.wrapper.wscatalogoscfdi40.RegimenFiscalDto;
import com.mx.quadrum.utileria2021.cfdi.utileria.UtileriaValidacionRegimenFiscal40;


@Controller
@RequestMapping(value = "/regimen")
public class RegimenFiscalController extends AbstractControllerHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6428928619270469972L;

	@RequestMapping(value = "/usocfdi_rfc.json", method = RequestMethod.POST)
	public @ResponseBody List<RegimenFiscalDto> obtenerListaRegimenesSinRegistro(@RequestBody final ReceptorDto receptorDto)
			throws JsonProcessingException {
		String url = ParametroEnum.URL_REGIMENFISCAL_40.getValue();
		int timeOut = 0;
		String username = ParametroEnum.USUARIO_LRFC_40.getValue();
		String password = ParametroEnum.PASSWORD_LRFC_40.getValue();
		return UtileriaValidacionRegimenFiscal40.obtenerRegimenFiscalesPorUsoCfdiRfc(url, timeOut, username, password, receptorDto);
	}
}