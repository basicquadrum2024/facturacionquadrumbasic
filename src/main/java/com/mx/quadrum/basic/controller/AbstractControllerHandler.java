package com.mx.quadrum.basic.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mx.quadrum.basic.util.ExcepcionesFacturacionBasica;
import com.mx.quadrum.basic.util.MensajeExcepcion;
import com.mx.quadrum.basic.util.UtileriaException;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

/**
 * Clase para el manejo de excepciones
 * @author 
 *
 */
@ControllerAdvice
public abstract class AbstractControllerHandler {

	private static final Logger logger = LoggerFactory.getLogger(AbstractControllerHandler.class);

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "IOException occured")
	@ExceptionHandler(Exception.class)
	public void handleIOException(Exception ioException) {
		logger.error("Internal Server Error");
		logger.error(UtileriasCfdi.imprimeStackError(ioException));
	}

	/**
	 * ExceptionHandler para los errores de negocio
	 *
	 * @param ex Exception que desencadena
	 * @return MensajeException
	 */
	@ExceptionHandler(ExcepcionesFacturacionBasica.class)
	public ResponseEntity<MensajeExcepcion> exceptionEmployeeHandler(Exception ex) {
		logger.error("Exception Handler Retenciones");
		logger.error("Causa->" + ex.getCause());
		logger.error("Mensaje->" + ex.getMessage());
		logger.error("Localiza Mensaje->" + ex.getLocalizedMessage());
		logger.error(UtileriaException.imprimeStackError(ex));
		MensajeExcepcion error = new MensajeExcepcion();
		error.setElemento(HttpStatus.PRECONDITION_FAILED.value());
		error.setMensaje(ex.getMessage());
		return new ResponseEntity<MensajeExcepcion>(error, HttpStatus.OK);
	}
}
