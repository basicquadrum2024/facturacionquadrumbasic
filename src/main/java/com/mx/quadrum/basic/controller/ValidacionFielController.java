package com.mx.quadrum.basic.controller;

import java.io.File;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.excepcion.ValidacionCertificadosExcepcion;
import com.mx.quadrum.basic.util.ValidacionCertificadosContribuyente;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

@RestController
public class ValidacionFielController implements Serializable {

	private static final Logger LOGGER = Logger.getLogger(ValidacionFielController.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 2404500569185526558L;

	@RequestMapping(value = "/validacion_fiel/validacion.json", method = RequestMethod.POST)
	public String validarFiel(@RequestParam(value = "fileCerFiel", required = false) MultipartFile fileCerFiel,
			@RequestParam(value = "fileKeyFiel", required = false) MultipartFile fileKeyFiel,
			@RequestParam("objeto") Object objecto) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			TcContribuyente emisor = mapper.readValue(objecto.toString(), TcContribuyente.class);
			byte[] bytesCertificado = fileCerFiel.getBytes();
			byte[] bytesLLave = fileKeyFiel.getBytes();
			ValidacionCertificadosContribuyente documentosFiel = new ValidacionCertificadosContribuyente(bytesCertificado, bytesLLave,
					emisor.getTipoCertificado().equals("CSD") ? emisor.getClave() : emisor.getClaveFiel(),
					emisor.getRfc(), "Cadena", emisor.getTipoCertificado());
			String firma = documentosFiel.generarFirma();
			if (null != firma && !firma.isEmpty())
				return "valida";
		} catch (ValidacionCertificadosExcepcion e) {
			return e.getMessage();
		} catch (Exception e) {
			LOGGER.error(String.format("Error en el metodo validarFiel: %s", UtileriasCfdi.imprimeStackError(e)));
			return "Error al validar la FIEL";
		}
		return "Error al validar la FIEL";
	}

	@RequestMapping(value = "/validacion_fiel/descargarArchivos.json", method = RequestMethod.POST)
	public void descargaArchivos(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws Exception {
		File archivo = new File("/home/marco/Escritorio/SOLICITUD DE VACACIONES_MACC.pdf");
		byte[] fileContent = Files.readAllBytes(archivo.toPath());
		httpServletResponse.setContentType("application/pdf");
		httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + archivo.getName());
		OutputStream outStream = httpServletResponse.getOutputStream();
		outStream.write(fileContent, 0, fileContent.length);
		outStream.close();
	}

}
