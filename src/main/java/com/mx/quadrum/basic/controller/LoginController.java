package com.mx.quadrum.basic.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.entity.UsuarioPrincipal;

/**
 * Esta clase se utiliza para realizar operaciones al accesar al sistema<br/>
 * Utiliza @Controller
 * 
 * @author Pedro Romero Martinez
 * @version 1.0.0
 *
 */
@Controller
public class LoginController extends AbstractControllerHandler {

	final static Logger logger = Logger.getLogger(LoginController.class);
	
	@Autowired
    private SessionRegistry sessionRegistry;

	@RequestMapping("/login.html")
	public String getAdminInfo(HttpSession session) {
		return "view/home/template";
	}
	
	/**
	 * M�todo para listar el usuario en sesi�n
	 * @return
	 */
	@RequestMapping(value="/app/usuariosApp.fue",method=RequestMethod.GET)
	@ResponseBody
	 public List<TwUsuario> listLoggedInUsers() {
	        final List<Object> allPrincipals = sessionRegistry.getAllPrincipals();
	        List<TwUsuario> listaUsr=null;
	        for(final Object principal : allPrincipals) {
	            if(principal instanceof UsuarioPrincipal) {
	                final UsuarioPrincipal user = (UsuarioPrincipal) principal;
	                listaUsr.add(user.getUsuario());
	            }
	        }
	        return listaUsr;
	    }
	
	/**
	 * M�todo para contar usuarios
	 * @return
	 */
	@RequestMapping(value="/app/usuariosCountApp.fue",method=RequestMethod.GET)
	@ResponseBody
	 public Integer listLoggedInUsersCount() {
	        final List<Object> allPrincipals = sessionRegistry.getAllPrincipals();	    
	        return allPrincipals.size();
	    }

}
