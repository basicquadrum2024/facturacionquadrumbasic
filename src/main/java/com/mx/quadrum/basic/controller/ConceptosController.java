package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcConceptos;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.TcConceptosServices;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.cfdi.v33.schema.Comprobante;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Conceptos.Concepto;

/**
 * Clase controlador REST contiene metodos relacionados al modulo de conceptos
 * 
 * @author
 *
 */
@RestController
public class ConceptosController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7301055326475893758L;

	@Autowired
	private EmisorServices emisorServices;

	@Autowired
	private TcConceptosServices tcConceptosServices;

	@Autowired
	private AesServices aesServices;

	@Autowired
	private PistasServices pistasServices;

	/**
	 * M�todo inicializa el objeto Concepto
	 * 
	 * @param request
	 * @return objeto Concepto
	 * @throws Exception
	 */
	@RequestMapping(value = "/conceptos/crearConceptoCfdiv33.json", method = RequestMethod.POST)
	public Concepto crearConceptoCfdiv33(HttpServletRequest request) throws Exception {
		Concepto concepto = new Concepto();
		Comprobante.Conceptos.Concepto.Impuestos impuestos = new Comprobante.Conceptos.Concepto.Impuestos();
		Comprobante.Conceptos.Concepto.Impuestos.Traslados traslados = new Comprobante.Conceptos.Concepto.Impuestos.Traslados();
		Comprobante.Conceptos.Concepto.Impuestos.Retenciones retenciones = new Comprobante.Conceptos.Concepto.Impuestos.Retenciones();
		impuestos.setTraslados(traslados);
		impuestos.setRetenciones(retenciones);
		concepto.setImpuestos(impuestos);
		return concepto;
	}

	

	/**
	 * M�todo que devuelve la lista de conceptos que corresponden al contribuyente
	 * 
	 * @param request
	 * @return lista de TcConceptos
	 * @throws Exception
	 */
	@RequestMapping(value = "/conceptos/buscarConceptosPorContribuyente.json", method = RequestMethod.POST)
	public List<TcConceptos> buscarConceptosPorContribuyente(HttpServletRequest request) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		TcContribuyente tcContribuyente = emisorServices
				.buscarPorRfcEmisor(aesServices.encriptaCadena(twUsuario.getRfce()));

		return tcConceptosServices.buscarConceptosPorContribuyente(tcContribuyente);
	}

	/**
	 * M�todo guarda en base de datos el objeto concepto, valida que no exista un
	 * concepto con la misma descripci�n
	 * 
	 * @param concepto
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "/conceptos/guardarConcepto.json", method = RequestMethod.POST)
	public void guardarConcepto(@RequestBody Concepto concepto, HttpServletRequest request) throws Exception {
		System.out.println(ToStringBuilder.reflectionToString(concepto));
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		TcContribuyente tcContribuyente = emisorServices
				.buscarPorRfcEmisor(aesServices.encriptaCadena(twUsuario.getRfce()));
		TcConceptos tcConceptos = tcConceptosServices
				.buscarConceptoPorDescripcionContribuyente(concepto.getDescripcion(), tcContribuyente);
		if (null == tcConceptos) {
			tcConceptos = new TcConceptos();
			tcConceptos.setDescripcion(concepto.getDescripcion());
			tcConceptos.setTcContribuyente(tcContribuyente);
			Utilerias.eventosAdministrador(Utilerias.CONCEPTO,
					"CONCEPTO " + concepto.getDescripcion() + " GENERADO CORRECTAMENTE.", twUsuario, "INSERT",
					pistasServices, request);
			tcConceptosServices.guardarTcConcepto(tcConceptos);

		} else {
			Utilerias.eventosAdministrador(Utilerias.CONCEPTO,
					"EL CONCEPTO " + concepto.getDescripcion() + " YA ESTA DADO DE ALTA.", twUsuario, "INSERT",
					pistasServices, request);
		}
	}

	/**
	 * M�todo que crea Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado
	 * 
	 * @return Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado
	 */
	@RequestMapping(value = "/conceptos/crearTraslado.json", method = RequestMethod.POST)
	public Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado crearTraslado() {
		return new Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado();
	}

	/**
	 * M�todo que crea
	 * Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion
	 * 
	 * @return Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion
	 */
	@RequestMapping(value = "/conceptos/crearRetencion.json", method = RequestMethod.POST)
	public Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion crearRetencion() {
		return new Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion();
	}

	/**
	 * M�todo que crea Comprobante.Conceptos.Concepto.Parte
	 * 
	 * @return Comprobante.Conceptos.Concepto.Parte
	 */
	@RequestMapping(value = "/conceptos/crearParte.json", method = RequestMethod.POST)
	public Comprobante.Conceptos.Concepto.Parte crearParte() {
		return new Comprobante.Conceptos.Concepto.Parte();
	}

	/**
	 * M�todo que crea Comprobante.Conceptos.Concepto.Parte.InformacionAduanera
	 * 
	 * @return Comprobante.Conceptos.Concepto.Parte.InformacionAduanera
	 */
	@RequestMapping(value = "/conceptos/crearParteInformacionAduanera.json", method = RequestMethod.POST)
	public Comprobante.Conceptos.Concepto.Parte.InformacionAduanera crearParteInformacionAduanera() {
		return new Comprobante.Conceptos.Concepto.Parte.InformacionAduanera();
	}

}
