package com.mx.quadrum.basic.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.quadrum.basic.adenda.Adenda33;
import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.Paginacion;
import com.mx.quadrum.basic.entity.Reporte;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TrGeneral;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.entity.UsuarioPrincipal;
import com.mx.quadrum.basic.services.CfdiServices;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.EnvioCorreoXmlServices;
import com.mx.quadrum.basic.services.MailService;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.TablaGeneralServices;
import com.mx.quadrum.basic.services.TcReceptorServices;
import com.mx.quadrum.basic.util.GenerarPdf;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.ResultadoTimbradoWrapper;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v11.schema.TimbreFiscalDigital;
import com.mx.quadrum.cfdi.utilerias.BytesArchivo;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasFecha;
import com.mx.quadrum.cfdi.v33.schema.Comprobante;

/**
 * Clase controlador REST contiene metodos para la consulta de CFDIs
 * @author 
 *
 */
@RestController
public class ConsultasCfdiController {

    private static final Logger LOGGER = Logger.getLogger(ConsultasCfdiController.class);

    @Autowired
    private TablaGeneralServices tablaGralServices;

    @Autowired
    private CfdiServices cfdiServices;

    @Autowired
    private AesServices aesServices;

    @Autowired
    private MailService mailService;

    @Autowired
    private PistasServices pistasServices;

    @Autowired
    private EnvioCorreoXmlServices envioCorreoXmlServices;
    
    @Autowired
    private EmisorServices emisorServices;
   
    @Autowired
    private TcReceptorServices tcReceptorServices;

    /**
     * M�todo busca comprobantes filtrados por UUID (folio fiscal)
     * @param currentPage
     * @param pageSize
     * @param folio
     * @param request
     * @return objeto Paginacion 
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/buscarComprobantesPorUuid/{currentPage}/{pageSize}/{folio}/.json", method = RequestMethod.POST)
    public Paginacion<TrGeneral> buscarComprobantesPorUuid(@PathVariable("currentPage") int currentPage,
	    @PathVariable("pageSize") int pageSize, @PathVariable("folio") String folio, HttpServletRequest request)
	    throws Exception {
	Paginacion<TrGeneral> paginacion = new Paginacion<TrGeneral>();
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	int start = (currentPage - 1) * pageSize;
	Date fechaActual=new Date();
	Date fechaInicial3Meses=UtileriasFecha.sumarRestarMesesFecha(fechaActual, -3);
	
	paginacion.setTotalResults(tablaGralServices.buscarPorFolioCount(aesServices.encriptaCadena(folio),
		aesServices.encriptaCadena(Utilerias.getUserSession(authentication).getRfce()),fechaInicial3Meses,fechaActual));
	paginacion.setList(tablaGralServices.buscarPorFolio(aesServices.encriptaCadena(folio),
		aesServices.encriptaCadena(Utilerias.getUserSession(authentication).getRfce()), start, pageSize,fechaInicial3Meses,fechaActual));
	return paginacion;

    }

    /**
     * M�todo busca comprobantes filtrados por RFC de Receptor
     * @param currentPage
     * @param pageSize
     * @param rfcReceptor
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/buscarComprobantesPorReceptor/{currentPage}/{pageSize}/{rfcReceptor}/.json", method = RequestMethod.POST)
    public Paginacion<TrGeneral> buscarComprobantesPorReceptor(@PathVariable("currentPage") int currentPage,
	    @PathVariable("pageSize") int pageSize, @PathVariable("rfcReceptor") String rfcReceptor,
	    HttpServletRequest request) throws Exception {
	Paginacion<TrGeneral> paginacion = new Paginacion<TrGeneral>();
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	int start = (currentPage - 1) * pageSize;
	Date fechaActual=new Date();
	Date fechaInicial3Meses=UtileriasFecha.sumarRestarMesesFecha(fechaActual, -3);

	paginacion.setTotalResults(tablaGralServices.buscarRfcReceptorCount(aesServices.encriptaCadena(rfcReceptor),
		aesServices.encriptaCadena(Utilerias.getUserSession(authentication).getRfce()),fechaInicial3Meses,fechaActual));
	paginacion.setList(tablaGralServices.buscarRfcReceptor(aesServices.encriptaCadena(rfcReceptor),
		aesServices.encriptaCadena(Utilerias.getUserSession(authentication).getRfce()), start, pageSize,fechaInicial3Meses,fechaActual));
	return paginacion;

    }

    /**
     * M�todo busca comprobantes filtrados por estatus
     * @param currentPage
     * @param pageSize
     * @param estatus
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/buscarComprobantesPorEstatus/{currentPage}/{pageSize}/{estatus}/.json", method = RequestMethod.POST)
    public Paginacion<TrGeneral> buscarComprobantesPorEstatus(@PathVariable("currentPage") int currentPage,
	    @PathVariable("pageSize") int pageSize, @PathVariable("estatus") Long estatus, HttpServletRequest request)
	    throws Exception {
	Paginacion<TrGeneral> paginacion = new Paginacion<TrGeneral>();
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	Date fechaActual=new Date();
	Date fechaInicial3Meses=UtileriasFecha.sumarRestarMesesFecha(fechaActual, -3);
	
	int start = (currentPage - 1) * pageSize;
	paginacion.setTotalResults(tablaGralServices.buscarPorEstatusCount(estatus,
		aesServices.encriptaCadena(Utilerias.getUserSession(authentication).getRfce()),fechaInicial3Meses,fechaActual));
	paginacion.setList(tablaGralServices.buscarPorEstatus(estatus,
		aesServices.encriptaCadena(Utilerias.getUserSession(authentication).getRfce()), start, pageSize,fechaInicial3Meses,fechaActual));
	return paginacion;

    }

    /**
     * M�todo busca comprobantes filtrados por rango de fechas
     * @param currentPage
     * @param pageSize
     * @param fechaInicial
     * @param fechaFinal
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/buscarComprobantesPorFechas/{currentPage}/{pageSize}/{fechaInicial}/{fechaFinal}.json", method = RequestMethod.POST)
    public Paginacion<TrGeneral> buscarComprobantes(@PathVariable("currentPage") int currentPage,
	    @PathVariable("pageSize") int pageSize, @PathVariable("fechaInicial") String fechaInicial,
	    @PathVariable("fechaFinal") String fechaFinal, HttpServletRequest request) throws Exception {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date fechaIni = null;
	Date fechaFi = null;
	fechaIni = formatter.parse(fechaInicial);
	fechaFi = formatter.parse(fechaFinal);
	
	Date fechaInicial3Meses=UtileriasFecha.sumarRestarMesesFecha(fechaIni, -3);
	
	Paginacion<TrGeneral> paginacion = new Paginacion<TrGeneral>();
	int start = (currentPage - 1) * pageSize;
	paginacion.setTotalResults(tablaGralServices.buscarPorFechaTimbradoCountEmisor(formatter.parse(fechaInicial),
		formatter.parse(fechaFinal), aesServices.encriptaCadena(Utilerias.getUserSession(auth).getRfce())));
	paginacion.setList(tablaGralServices.buscarPorFechaTimbradoEmisor(fechaInicial3Meses, fechaFi,
		aesServices.encriptaCadena(Utilerias.getUserSession(auth).getRfce()), start, pageSize));
	return paginacion;

    }

    /**
     * M�todo busca comprobantes filtrados por clave de confirmaci�n
     * @param currentPage
     * @param pageSize
     * @param claveConfirmacion
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/buscarComprobantesPorConfirmacion/{currentPage}/{pageSize}/{claveConfirmacion}/.json", method = RequestMethod.POST)
    public Paginacion<TrGeneral> buscarComprobantesPorConfirmacion(@PathVariable("currentPage") int currentPage,
	    @PathVariable("pageSize") int pageSize, @PathVariable("claveConfirmacion") String claveConfirmacion,
	    HttpServletRequest request) throws Exception {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	Paginacion<TrGeneral> paginacion = new Paginacion<TrGeneral>();
	int start = (currentPage - 1) * pageSize;
	Date fechaActual=new Date();
	Date fechaInicial3Meses=UtileriasFecha.sumarRestarMesesFecha(fechaActual, -3);
	
	paginacion.setTotalResults(tablaGralServices.buscarPorClaveConfirmacionCountEmisor(
		aesServices.encriptaCadena(Utilerias.getUserSession(auth).getRfce()),
		aesServices.encriptaCadena(claveConfirmacion),fechaInicial3Meses,fechaActual));
	paginacion.setList(tablaGralServices.buscarPorEmisorClaveConfirmacion(
		aesServices.encriptaCadena(Utilerias.getUserSession(auth).getRfce()),
		aesServices.encriptaCadena(claveConfirmacion), start, pageSize,fechaInicial3Meses,fechaActual));
	return paginacion;

    }

    /**
     * M�todo que envia la lsita de comprobante por correo
     * @param lista
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/mandarComprobantePorCorreo.json", method = RequestMethod.POST)
    public List<ResultadoTimbradoWrapper> mandarComprobantePorCorreo(@RequestBody List<Long> lista,
	    HttpServletRequest request) throws Exception {
	List<ResultadoTimbradoWrapper> listaRespuesta = new ArrayList<>();
    byte[] arrayFiles = null;
	 if (lista != null && !lista.isEmpty()) {
	    for (Long id : lista) {
		ResultadoTimbradoWrapper resultadoTimbradoWrapper = new ResultadoTimbradoWrapper();
		TrGeneral trgeneral = tablaGralServices.buscarCfdiPorId(id);
		if (trgeneral != null) {
		    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		    UsuarioPrincipal customUser = (UsuarioPrincipal) authentication.getPrincipal();
		    List<BytesArchivo> listabyte = new ArrayList<>();
            Path path = Paths.get(trgeneral.getTwCfdi().getDireccionXml());
		    BytesArchivo byteXMl = new BytesArchivo();
		    byteXMl.setNombreArchivo(trgeneral.getTwCfdi().getUuid() + ".xml");
		    byteXMl.setBytesArchivo(Files.readAllBytes(path));
		    listabyte.add(byteXMl);
		    arrayFiles = Utilerias.crearPdf(ParametroEnum.JASPER_V4.getValue(),trgeneral.getTwCfdi().getDireccionXml(),
		    		trgeneral.getTwCfdi().getTotal().setScale(2));
		    BytesArchivo bytepdf = new BytesArchivo();
		    bytepdf.setNombreArchivo(trgeneral.getTwCfdi().getUuid() + ".pdf");
		    bytepdf.setBytesArchivo(arrayFiles);
		    listabyte.add(bytepdf);
		    mandarFacturaPorCorreo(trgeneral.getTwCfdi().getUuid(), customUser.getUsuario().getCorreo(),
			    listabyte);
		    resultadoTimbradoWrapper.setError(2);
		    resultadoTimbradoWrapper.setOpcion("El envio de correo se ha ejecutado con �xito:  "
			    + trgeneral.getTwCfdi().getUuid());
		    listaRespuesta.add(resultadoTimbradoWrapper);
		}

	    }
	}
	return listaRespuesta;

    }

    /**
     * <p>
     * Metodo para mandar por correo los XML
     * <p>
     * 
     * @param xml
     * @param comprobante
     */
    public void mandarFacturaPorCorreo(String uuid, String correo, List<BytesArchivo> listabyte) {
	try {
	    byte[] zip = UtileriasCfdi.crearZipListaBytesArchivo(listabyte);
	    mailService.enviarZip(correo, correo, "Representaci\u00f3n impresa",
		    envioCorreoXmlServices.buscarNombreCfdis(uuid, ParametroEnum.DIRRECCION_APP.getValue()), uuid
			    + ".zip", zip);
	} catch (Exception e) {
	    LOGGER.error("ERROR al mandar correo cargar excel-> " + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
	}

    }

    /**
     * M�todo que manda un comprobante por correo
     * @param objeto
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/mandarComprobantePorCorreos.json", method = RequestMethod.POST)
    public List<ResultadoTimbradoWrapper> mandarComprobantePorCorreos(@RequestBody Reporte objeto,
	    HttpServletRequest request) throws Exception {
	ObjectMapper mapper = new ObjectMapper();
	Object[] objetos = objeto.getObjetos();
	String listaInicial = "" + objetos[0];
	String correos = "" + objetos[1];
	List<String> list = mapper.readValue(listaInicial, new TypeReference<List<String>>() {
	});
	List<ResultadoTimbradoWrapper> listaRespuesta = new ArrayList<>();
	byte[] arrayFiles = null;
	if (list != null && !list.isEmpty()) {
	    for (String id : list) {
		List<String> listaCorreos = mapper.readValue(correos, new TypeReference<List<String>>() {
		});
		for (String correo : listaCorreos) {
		    ResultadoTimbradoWrapper respuesta = new ResultadoTimbradoWrapper();
		    TrGeneral trgeneral = tablaGralServices.buscarCfdiPorId(Long.parseLong(id));
		    if (trgeneral != null) {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			UsuarioPrincipal customUser = (UsuarioPrincipal) authentication.getPrincipal();
			
//			Adenda33 adenda = (Adenda33) UtileriasCfdi.getObjectoComplemento(comprobante.getAddenda()
//				.getAny(), Adenda33.class);
			// arrayFiles =
			// GenerarPdf33.creaPDFByteArray(comprobante,
			// fiscalDigital, adenda);
		

			List<BytesArchivo> listabyte = new ArrayList<>();

			Path path = Paths.get(trgeneral.getTwCfdi().getDireccionXml());
			BytesArchivo byteXMl = new BytesArchivo();
			byteXMl.setNombreArchivo(trgeneral.getTwCfdi().getUuid() + ".xml");
			byteXMl.setBytesArchivo(Files.readAllBytes(path));
			listabyte.add(byteXMl);
			// arrayFiles =
			// GenerarPdf33.creaPDFByteArray(comprobante,
			// fiscalDigital, adenda);
			 arrayFiles = Utilerias.crearPdf(ParametroEnum.JASPER_V4.getValue(),trgeneral.getTwCfdi().getDireccionXml(),
			    		trgeneral.getTwCfdi().getTotal().setScale(2));
			
			BytesArchivo bytepdf = new BytesArchivo();
			bytepdf.setNombreArchivo(trgeneral.getTwCfdi().getUuid() + ".pdf");
			bytepdf.setBytesArchivo(arrayFiles);
			listabyte.add(bytepdf);
			mandarFacturaPorCorreo(trgeneral.getTwCfdi().getUuid(), correo, listabyte);
			respuesta.setError(2);
			respuesta.setOpcion("El comprobante con UUID " + trgeneral.getTwCfdi().getUuid()
				+ " se ha enviado al siguiente correo: " + correo);
			listaRespuesta.add(respuesta);
		    }

		}
	    }
	}
	return listaRespuesta;

    }

    /**
     * M�todo que devuelve los pagos cancelados que corresponden a un CFDI
     * @param idCfdiPadre
     * @return lista de TwCFDI
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/buscarPagosCancelados/{idCfdiPadre}/.json", method = RequestMethod.POST)
    public List<TwCfdi> buscarPagosCancelados(@PathVariable("idCfdiPadre") Long idCfdiPadre) throws Exception {
	return cfdiServices.buscarPagosCancelados(idCfdiPadre);
    }

    /**
     * M�doto que devuelve una lista de TwCfdi relacionado a un comprobante padre
     * @param twCfdiExample
     * @param idCfdiPadre
     * @return lista de TwCfdi
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/buscarPagoExample/{idCfdiPadre}/.json", method = RequestMethod.POST)
    public List<TwCfdi> buscarPagosExample(@RequestBody TwCfdi twCfdiExample,
	    @PathVariable("idCfdiPadre") Long idCfdiPadre) throws Exception {
	aesServices.encriptaObject(twCfdiExample);
	return cfdiServices.buscarPagosExample(twCfdiExample, idCfdiPadre);
    }

    /**
     * M�todo que realiza b�squeda de comprobante hijo por fecha d craci�n 
     * 
     * @param twCfdiExample
     *            Entidad TwCfdi
     * @param idCfdiPadre
     *            Id cfdi padre
     * @param fechaCreacion
     *            Fecha creacion o certificacion
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/comprobante/buscarPagoExample/{idCfdiPadre}/{fechaCreacion}/.json", method = RequestMethod.POST)
    public List<TwCfdi> buscarPagosExample(@RequestBody TwCfdi twCfdiExample,
	    @PathVariable("idCfdiPadre") Long idCfdiPadre, @PathVariable("fechaCreacion") String fechaCreacion,
	    HttpServletRequest request) throws Exception {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario twUsuario = Utilerias.getUserSession(auth);
	Date fechaInicio = null;
	Date fechaFin = null;
	if (fechaCreacion != null) {
	    if (!fechaCreacion.contains("undefined") && !fechaCreacion.contains("null")) {
		fechaInicio = UtileriasCfdi.YYYYMMDD_HHMMSS.parse(fechaCreacion + " 00:00:00");
		fechaFin = UtileriasCfdi.YYYYMMDD_HHMMSS.parse(fechaCreacion + " 23:59:59");
	    }
	}
	twCfdiExample.setFechaCreacion(null);
	Utilerias.eventosAdministrador("TWCFDI", "OBTENER COMPROBANTES POR FECHA DE CREACION", twUsuario, "SELECT",
		pistasServices, request);
	aesServices.encriptaObject(twCfdiExample);
	return cfdiServices.buscarPagosExample(twCfdiExample, idCfdiPadre, fechaInicio, fechaFin);
    }

}
