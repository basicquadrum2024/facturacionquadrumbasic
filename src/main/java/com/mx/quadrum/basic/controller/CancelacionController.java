package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TrGeneral;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.entity.UsuarioPrincipal;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.EnvioCorreoXmlServices;
import com.mx.quadrum.basic.services.TablaGeneralServices;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.UtileriaCancelacion;
import com.mx.quadrum.basic.wrapper.ObjetoCancelacion;
import com.mx.quadrum.basic.wrapper.ResultadoTimbradoWrapper;
import com.mx.quadrum.basic.wrapper.WrapperCancelacion;
import com.mx.quadrum.cfdi.utilerias.UtileriasArchivosPem;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasGenerales;

import views.core.soap.services.apps.Cancela;
import views.core.soap.services.apps.Folio;
import views.core.soap.services.apps.UUID;

/**
 * Clase controlador REST contiene los metodos para el modulo cancelaci�n del
 * comprobante
 * 
 * @author
 *
 */
@RestController
public class CancelacionController extends AbstractControllerHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1537016633708558976L;

	@Autowired
	TablaGeneralServices comprobanteServices;

	@Autowired
	EmisorServices emisorService;

	@Autowired
	AesServices aesServices;

	@Autowired
	private EnvioCorreoXmlServices envioCorreoXmlServices;

	public static final Logger LOGGER = Logger.getLogger(CancelacionController.class);

	/**
	 * M�todo busca los CFDI a cancelar, valida si existe en la base de datos del
	 * sistema
	 * 
	 * @param lista
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cancelar/cancelarComprobantes.json", method = RequestMethod.POST)
	public List<ResultadoTimbradoWrapper> buscarComprobantes(@RequestBody WrapperCancelacion wrapperCancelacion)
			throws Exception {
		List<ResultadoTimbradoWrapper> listaRespuesta = new ArrayList<>();
		ResultadoTimbradoWrapper resultadoTimbrado = new ResultadoTimbradoWrapper();

		for (Long id : wrapperCancelacion.getLista()) {
			TrGeneral comprobante = comprobanteServices.buscarCfdiPorCancelacionId(id);
			if (comprobante != null) {
				cancelarComprobante(comprobante, resultadoTimbrado, listaRespuesta,
						wrapperCancelacion.getCancelacion());
			} else {
				resultadoTimbrado.setTimbrado(false);
				resultadoTimbrado.setError(new Integer(2));
				resultadoTimbrado.setOpcion("El comprobante no existe contacte al administrador");
				resultadoTimbrado.setUuid("No encontrado");
			}
		}
		return listaRespuesta;

	}

	/**
	 * M�todo para cancelar un CFDI
	 * 
	 * @param comprobante
	 * @param resultadoTimbrado
	 * @throws Exception
	 */
	public void cancelarComprobante(TrGeneral cfdi, ResultadoTimbradoWrapper resultadoTimbrado,
			List<ResultadoTimbradoWrapper> listaRespuesta, ObjetoCancelacion objetoCancelacion) throws Exception {
		TcContribuyente emisor = emisorService
				.buscarPorRfcEmisor(aesServices.encriptaCadena(cfdi.getTcContribuyente().getRfc()));
		UUID uuid = new UUID();
		uuid.setFolioSustitucion(StringUtils.stripToEmpty(objetoCancelacion.getUuidSustituto()));
		uuid.setMotivo(StringUtils.stripToEmpty(objetoCancelacion.getMotivoCancelacion()));
		uuid.setUUID(cfdi.getTwCfdi().getUuid());
		UUID[] uuids = new UUID[1];
		uuids[0] = uuid;

		PrivateKey privateKey = UtileriasArchivosPem
				.obtenerLlavePrivada(UtileriasGenerales.decodificarBase64(emisor.getLlaveKey()), emisor.getClave());
		X509Certificate x509Certificate = UtileriasArchivosPem
				.obtenerCertificado(UtileriasGenerales.decodificarBase64(emisor.getLlaveCer()));
		byte[] certificadoPem = UtileriasArchivosPem.convertirX509CertificadoAPem(x509Certificate);
		byte[] llave3DesCancelar = UtileriasArchivosPem.crearLlaveRsaDes3(privateKey,
				ParametroEnum.PASSWORDWS.getValue());
		Cancela cancela = UtileriaCancelacion.cancelacion(ParametroEnum.DIRECCIONWSCANCELACION.getValue(), uuids,
				ParametroEnum.USUARIOWS.getValue(), ParametroEnum.PASSWORDWS.getValue(), emisor.getRfc(),
				certificadoPem, llave3DesCancelar, true);

		if (null != cancela) {
			if (cancela.getCodEstatus() != null && cancela.getCodEstatus().equals("301")) {
				resultadoTimbrado.setTimbrado(false);
				resultadoTimbrado.setError(new Integer(2));
				resultadoTimbrado.setOpcion("XML mal formado");
				listaRespuesta.add(resultadoTimbrado);
			}
			if (cancela.getCodEstatus() != null && cancela.getCodEstatus().equals("302")) {
				resultadoTimbrado.setTimbrado(false);
				resultadoTimbrado.setError(new Integer(2));
				resultadoTimbrado.setOpcion("Sello mal formado o inv�lido");
				listaRespuesta.add(resultadoTimbrado);
			}
			if (cancela.getCodEstatus() != null && cancela.getCodEstatus().equals("303")) {
				resultadoTimbrado.setTimbrado(false);
				resultadoTimbrado.setError(new Integer(2));
				resultadoTimbrado.setOpcion("Sello no corresponde a emisor");
				listaRespuesta.add(resultadoTimbrado);
			}
			if (cancela.getCodEstatus() != null && cancela.getCodEstatus().equals("304")) {
				resultadoTimbrado.setTimbrado(false);
				resultadoTimbrado.setError(new Integer(2));
				resultadoTimbrado.setOpcion("Certificado revocado o caduco");
				listaRespuesta.add(resultadoTimbrado);
			}
			if (cancela.getCodEstatus() != null && cancela.getCodEstatus().equals("305")) {
				resultadoTimbrado.setTimbrado(false);
				resultadoTimbrado.setError(new Integer(2));
				resultadoTimbrado.setOpcion("La fecha de emisi�n no esta dentro de la vigencia del CSD del Emisor");
				listaRespuesta.add(resultadoTimbrado);
			}

			if (cancela.getCodEstatus() != null && cancela.getCodEstatus().equals("306")) {
				resultadoTimbrado.setTimbrado(false);
				resultadoTimbrado.setError(new Integer(2));
				resultadoTimbrado.setOpcion("EL certificado no es de tipo CSD");
				listaRespuesta.add(resultadoTimbrado);
			}
			if (cancela.getCodEstatus() != null && cancela.getCodEstatus().equals("308")) {
				resultadoTimbrado.setTimbrado(false);
				resultadoTimbrado.setError(new Integer(2));
				resultadoTimbrado.setOpcion("Certificado no expedido por el SAT");
				listaRespuesta.add(resultadoTimbrado);
			}
			if (null != cancela.getFolios()) {
				for (Folio acuseFolios : cancela.getFolios()) {

					if (null != acuseFolios.getEstatusUUID() && (acuseFolios.getEstatusUUID().equals("201")
							|| acuseFolios.getEstatusUUID().equals("202"))) {

						String ruta = UtileriasCfdi.crearDireccionGuardadoXml(
								ParametroEnum.DIRECCION_CANCELACION_XML.getValue(), new Date(), cancela.getRfcEmisor(),
								acuseFolios.getUUID());
						UtileriasCfdi.guardarArchivoXml(ruta, cancela.getAcuse());

						resultadoTimbrado.setTimbrado(true);
						resultadoTimbrado.setOpcion("UUID Cancelado.");
						resultadoTimbrado.setError(new Integer(2));
						resultadoTimbrado.setUuid(cfdi.getTwCfdi().getUuid());
						resultadoTimbrado.setId(cfdi.getId());
						listaRespuesta.add(resultadoTimbrado);
						actualizaComprobante(cfdi.getTwCfdi(), ruta);
						Authentication authe = SecurityContextHolder.getContext().getAuthentication();
						UsuarioPrincipal principal = (UsuarioPrincipal) authe.getPrincipal();

						envioCorreoXmlServices.enviarXmlCanceladoPorCorreo(ruta, acuseFolios.getUUID(),
								principal.getUsuario().getCorreo());
					}
					if (null != acuseFolios.getEstatusUUID() && acuseFolios.getEstatusUUID().equals("203")) {
						resultadoTimbrado.setTimbrado(false);
						resultadoTimbrado.setError(new Integer(2));
						resultadoTimbrado.setOpcion("UUID No encontrado o no corresponde en el emisor.");
						listaRespuesta.add(resultadoTimbrado);

					}
					if (null != acuseFolios.getEstatusUUID() && acuseFolios.getEstatusUUID().equals("204")) {
						resultadoTimbrado.setError(new Integer(2));
						resultadoTimbrado.setOpcion("UUID No aplicable para cancelaci�n");
						listaRespuesta.add(resultadoTimbrado);

					}
					if (null != acuseFolios.getEstatusUUID() && acuseFolios.getEstatusUUID().equals("205")) {
						resultadoTimbrado.setTimbrado(false);
						resultadoTimbrado.setError(new Integer(2));
						resultadoTimbrado.setOpcion("UUID No existe.");
						listaRespuesta.add(resultadoTimbrado);

					}
					if (null != acuseFolios.getEstatusUUID() && acuseFolios.getEstatusUUID().equals("206")) {
						resultadoTimbrado.setTimbrado(false);
						resultadoTimbrado.setError(new Integer(2));
						resultadoTimbrado.setOpcion("UUID no corresponde a un CFDI del Sector Primario.");
						listaRespuesta.add(resultadoTimbrado);

					}

					if (null != acuseFolios.getEstatusUUID() && acuseFolios.getEstatusUUID().equals("704")) {
						resultadoTimbrado.setTimbrado(false);
						resultadoTimbrado.setError(new Integer(2));
						resultadoTimbrado.setOpcion("Xml no encontrado ante el SAT, favor de intentarlo en 20 minutos");
						listaRespuesta.add(resultadoTimbrado);
					}
				}
			}
		} else {
			LOGGER.error("Xml no encontrado ante el SAT, favor de intentarlo en 20 minutos");
			resultadoTimbrado.setTimbrado(false);
			resultadoTimbrado.setError(new Integer(2));
			resultadoTimbrado.setOpcion("Xml no encontrado ante el SAT, favor de intentarlo en 20 minutos");
			listaRespuesta.add(resultadoTimbrado);

		}
	}

	/**
	 * M�todo que actualiza el estatus a 0 del CFDIs
	 * 
	 * @param comprobante
	 * @param ruta        direcci�n del CFDI
	 * @throws Exception
	 * 
	 */
	public void actualizaComprobante(TwCfdi comprobante, String ruta) throws Exception {
		try {
			comprobante.setEstatus(Long.parseLong("0"));
			comprobante.setFechaCancelacion(new Date());
			comprobante.setDireccionXml(ruta);
			comprobanteServices.saveOrUpdateGeneral(comprobante);
		} catch (Exception e) {
			LOGGER.error("Error al actualizar el entity tw_cfdi" + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
	}

}
