package com.mx.quadrum.basic.controller;

import java.io.File;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcPerfil;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.IUsuarioServices;
import com.mx.quadrum.basic.services.MailService;
import com.mx.quadrum.basic.services.PerfilServices;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.ServicioParametros;
import com.mx.quadrum.basic.util.Anexo20;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.SessionApp;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.CodigoVerificacionWrapper;

/**
 * Clase @RestController contiene metodos para administrar Usuario
 * @author Manu
 *
 */
@RestController
@RequestMapping(value = "/usuarioController")
public class UsuarioController extends AbstractControllerHandler implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    final static Logger LOGGER = Logger.getLogger(UsuarioController.class);
    public static final int REGISTROSPORPAGINA = 10000;

    @Autowired
    private IUsuarioServices usuarioServices;

    @Autowired
    private EmisorServices emisorServices;

    @Autowired
    private AesServices aesServices;

    @Autowired
    private PistasServices pistasServices;

    @Autowired
    private PerfilServices perfilServices;

    @Autowired
    private MailService enMailService;
    
    @Autowired
    private ServicioParametros servicioParametros;

    /**
     * M�todo que guarda en eventos administrador el acceso al sistema
     * @param request
     * @throws Exception
     */
    @RequestMapping(value = "/guardaBitacora.json", method = RequestMethod.POST)
    public void guardaBitacora(HttpServletRequest request) throws Exception {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario userEntity = Utilerias.getUserSession(auth);
	Utilerias.eventosAdministrador(Utilerias.USERS, "ACCESO AL SISTEMA", userEntity, "SELECT", pistasServices,
		request);

    }

    /**
     * M�todo para registrar los usuarios v�a json.
     * 
     * @param usuario
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/registro.fue", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String registro(@RequestBody TwUsuario usuario, HttpServletRequest request) throws Exception {
	String respuesta = null;
	TwUsuario user = new TwUsuario();
	String pass = Anexo20.generaPassword();

	SessionApp sessionApp = new SessionApp();
	sessionApp.setRfc(usuario.getRfce());

	// aesServices.setClaveKey(sessionApp.getRfc());
	aesServices.encriptaCadena(sessionApp.getRfc());
	TcPerfil perfil = new TcPerfil();
	perfil.setId(2); // perfil para usuarios

	if (usuarioServices.validarRfcTemporal(aesServices.encriptaCadena(usuario.getRfce().trim()))) {

	    user.setRfce(usuario.getRfce().trim());
	    user.setFechacreacion(new Date());
	    user.setUltimaconexion(new Date());
	    user.setPrimerasession(String.valueOf(0));

	    user.setNombre(usuario.getNombre().trim());
	    if (usuario.getApPaterno() != null) {
		user.setApPaterno(usuario.getApPaterno().trim());
	    }
	    if (usuario.getApMaterno() != null) {
		user.setApMaterno(usuario.getApMaterno().trim());
	    }
	    user.setTelFijo(usuario.getTelFijo());
	    if (usuario.getTelCel() != null) {
		user.setTelCel(usuario.getTelCel());
	    }
	    user.setCorreo(usuario.getCorreo().trim());
	    user.setPassword(pass);
	    user.setVersioncomercial(null);
	    user.setHorasession(null);
	    user.setStatuscuenta(String.valueOf(1));
	    user.setStatussession(null);
	    user.setFechacambiopassword(null);
	    user.setFechamensajeaviso(null);
	    user.setSesion(null);
	    user.setAlgoritmo(usuario.getRfce());
	    user.setTcPerfil(perfil);
	    user.setCfdiMes(3);
	    Utilerias.eventosAdministrador(Utilerias.CONTACTO, "CONTACTO " + user.getRfce() + " CREADO CORRECTAMENTE.",
		    user, "INSERT", pistasServices, request);
	    usuarioServices.saveorUpdate(user);
	    enviaCorreoUsuarioNuevo(user);
	    respuesta = "success";

	}
	return respuesta;
    }

    /**
     * M�todo para recuperar contrase�a via json.
     * 
     * @param usuario
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/recuperarContrasenia.fue", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String recuperarContrasasenia(@RequestBody TwUsuario usuario, HttpServletRequest request)
	    throws Exception {
	String respuesta = null;
	TwUsuario user = null;
	String pass = Anexo20.generaPassword();
	Date date = new Date();
	String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
	SessionApp sessionApp = new SessionApp();
	sessionApp.setRfc(usuario.getRfce());

	// aesServices.setClaveKey(sessionApp.getRfc());
	aesServices.encriptaCadena(sessionApp.getRfc());

	user = usuarioServices.findbyRfcTemporal(aesServices.encriptaCadena(usuario.getRfce().trim()));
	if (user != null) {
	    user.setRfce(user.getRfce());
	    user.setFechacambiopassword(modifiedDate);
	    user.setCorreo(user.getCorreo());
	    user.setPassword(pass);
	    user.setPrimerasession(String.valueOf(0));

	    Utilerias.eventosAdministrador(Utilerias.CONTACTO, "CONTACTO " + "CONTRASE�A RECUPERADA CORRECTAMENTE.",
		    user, "UPDATE", pistasServices, request);
	    usuarioServices.saveorUpdate(user);
	    enviaCorreoUsuarioRecuperarContrase�a(user);

	    respuesta = "success";
	} else {
	    respuesta = "error";
	}

	return respuesta;
    }

    /**
     * Metodo encargado de enviar correos al crear usuario.
     * 
     * @param archivo
     * @return boolean
     * @throws Exception
     */
    public void enviaCorreoUsuarioNuevo(TwUsuario usuario) throws MessagingException {
	try {
	    if (usuario.getCorreo() != null) {
		enMailService.send(
			aesServices.desEncriptaCadena(usuario.getCorreo()),
			"Nueva Cuenta Facturaci�n Basic.".toUpperCase(),
			textoNuevoUsuario(aesServices.desEncriptaCadena(usuario.getPassword()),
				aesServices.desEncriptaCadena(usuario.getRfce())), null, null);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Metodo encargado enviar correos al Recuperar Contrase�a.
     * 
     * @param archivo
     * @return boolean
     * @throws Exception
     */
    public void enviaCorreoUsuarioRecuperarContrase�a(TwUsuario usuario) throws MessagingException {
	try {
	    if (usuario.getCorreo() != null) {
		enMailService.send(
			aesServices.desEncriptaCadena(usuario.getCorreo()),
			"Restablecer Contrase�a.".toUpperCase(),
			textoRecuperarContrase�a(aesServices.desEncriptaCadena(usuario.getPassword()),
				aesServices.desEncriptaCadena(usuario.getRfce())), null, null);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Texto para el contenido del correo electr�nico al registrar.
     * 
     * @param correo
     * @param password
     * @return
     * @throws UnknownHostException
     */
    public static String textoNuevoUsuario(String password, String rfc) {
	StringBuilder mensaje = new StringBuilder();

	mensaje.append("CENTRO DE VALIDAC�N DIGITAL CVDSA S.A. DE C.V.\n");
	mensaje.append("Le da la Bienvenida a Quadrum, SISTEMA DE FACTURACI�N ELECTR�NICA, agradeciendo su preferencia al elegirnos como su proveedor autorizado de facturaci�n electr�nica.");
	mensaje.append("\n A continuaci�n se le indican los datos personales que deber� proporcionar para ingresar al sitio web de Quadrum.\n");
	mensaje.append("\n\n\t\t RFC:      " + rfc);
	mensaje.append("\n\n\t\t CONTRASE�A:      " + password);
	mensaje.append("\n\n\n Para ingresar al sitio web de Quadrum-cfdi copie y pegue el siguiente enlace en su explorador.");
	mensaje.append("\n\t\t " + ParametroEnum.DIRRECCION_APP.getValue() + "");
	mensaje.append("\n\n INSTRUCCIONES:");
	mensaje.append("\n\t 1. Una vez ingresado al sitio web, por propia seguridad el sistema le guiar� a cambiar la contrase�a, y despu�s continuara con el llenado de sus datos fiscales como deber�n aparecer en los comprobantes.");
	mensaje.append("\n\t 2. El Sistema le indicar� cuales son los pasos que debe completar antes de comenzar a facturar.");
	mensaje.append("\n\n Le estaremos muy agradecidos si nos env�a sus sugerencias, comentarios o aspectos relacionados con el servicio y nuestra empresa.");
	mensaje.append("\n\n Saludos Cordiales.");
	mensaje.append("\n\n Soporte T�cnico Quadrum-cfdi");
	mensaje.append("\n (01) 4421612565, (01) 5512091825, 01(55) 5512091825, 01(722)5075558, 01(722)5075559 y 01(722)2122754");
	mensaje.append("\n " + ParametroEnum.CORREO_SOPORTE.getValue() + "");
	mensaje.append("\n\n\n" + "Aviso de Privacidad\n");
	mensaje.append("\n");
	mensaje.append("\n");
	mensaje.append("\n");
	mensaje.append("Quadrum como Proveedor Autorizado Certificado por el SAT con domicilio ubicado en:");
	mensaje.append("Montecito n�mero 38 Piso 25 Despacho 22 Colonia N�poles Delegaci�n Benito Ju�rez");
	mensaje.append(" M�xico Distrito Federal C.P 03810 utilizar� sus datos personales aqu� recabados ");
	mensaje.append("para proveerle de los servicios en la emisi�n y generaci�n de CFDI�s que usted ha contratado.");
	mensaje.append("Para mayor informaci�n acerca del tratamiento y de los derechos que puede hacer valer, ");
	mensaje.append("usted puede acceder al aviso de privacidad completo a trav�s de "
		+ ParametroEnum.PRIVACIDAD.getValue() + "\n");
	return mensaje.toString();
    }

    /**
     * Texto para contenido del correo electr�nico al restaurar.
     * 
     * @param usuario
     * @return
     * @throws UnknownHostException
     */
    public static String textoRecuperarContrase�a(String password, String rfc) {
	StringBuilder mensaje = new StringBuilder();

	mensaje.append("CENTRO DE VALIDAC�N DIGITAL CVDSA S.A. DE C.V.\n");
	mensaje.append("\n\n\t\t Se ha generado una nueva contrase�a.");
	mensaje.append("\n A continuac�n se le indican los datos personales que deber� proporcionar para ingresar al sitio web de Quadrum.\n");
	mensaje.append("\n\n\t\t RFC:      " + rfc);
	mensaje.append("\n\n\t\t CONTRASE�A:      " + password);
	mensaje.append("\n\n\n Para ingresar al sitio web de Quadrum-cfdi copie y pegue el siguiente enlace en su explorador.");
	mensaje.append("\n\t\t " + ParametroEnum.DIRRECCION_APP.getValue() + "");
	mensaje.append("\n\n INSTRUCCIONES:");
	mensaje.append("\n\t 1. Una vez ingresado al sitio web, por propia seguridad el sistema le guiar� a cambiar la contrase�a, y despu�s continuara con el llenado de sus datos fiscales como deber�n aparecer en los comprobantes.");
	mensaje.append("\n\t 2. El Sistema le indicar� cuales son los pasos que debe completar antes de comenzar a facturar.");
	mensaje.append("\n\n Le estaremos muy agradecidos si nos env�a sus sugerencias, comentarios o aspectos relacionados con el servicio y nuestra empresa.");
	mensaje.append("\n\n Saludos Cordiales.");
	mensaje.append("\n\n Soporte T�cnico Quadrum-cfdi");
	mensaje.append("\n(55)  12091825");
	mensaje.append("\nCallCenter: " + ParametroEnum.CORREO_SOPORTE.getValue() + "");
	mensaje.append("\n\n\n" + "Aviso de Privacidad\n");
	mensaje.append("\n");
	mensaje.append("\n");
	mensaje.append("\n");
	mensaje.append("Quadrum como Proveedor Autorizado Certificado por el SAT con domicilio ubicado en:");
	mensaje.append("Montecito n�mero 38 Piso 25 Despacho 22 Colonia N�poles Delegaci�n Benito Ju�rez");
	mensaje.append(" M�xico Distrito Federal C.P 03810 utilizar� sus datos personales aqu� recabados ");
	mensaje.append("para proveerle de los servicios en la emisi�n y generaci�n de CFDI�s que usted ha contratado.");
	mensaje.append("Para mayor informaci�n acerca del tratamiento y de los derechos que puede hacer valer, ");
	mensaje.append("usted puede acceder al aviso de privacidad completo a trav�s de "
		+ ParametroEnum.PRIVACIDAD.getValue() + "\n");

	return mensaje.toString();
    }

    /**
     * M�todo para descargar los clientes registrados
     * @param request
     * @param response
     */
    @RequestMapping(value = "descargaClientesRegistrados.json", method = RequestMethod.GET)
    public void doDowloadComprobantePorFecha(HttpServletRequest request, HttpServletResponse response) {
	List<TcContribuyente> contribuyentes = null;
	try {
	    Workbook libro = null;
	    libro = new SXSSFWorkbook(20);
	    Sheet hoja = libro.createSheet();
	    Integer contador = 0;
	    creaEncabezado(hoja, Utilerias.TITULOSUSUARIO);

	    String nombre = "Clientes_Registrados";

	    Long totalRegistros = emisorServices.buscaClientesRegistradosCount();

	    Integer paginas = Double.valueOf(Math.ceil((double) totalRegistros / (double) REGISTROSPORPAGINA))
		    .intValue();

	    for (int i = 1; i <= paginas; i++) {
		final Integer paginaInicial = (i * REGISTROSPORPAGINA) - REGISTROSPORPAGINA;
		final Integer maxResult = REGISTROSPORPAGINA;
		contribuyentes = emisorServices.clientesRegistrados(paginaInicial, maxResult);
		creaCuerpo(hoja, contribuyentes, contador, contador);
		contador += REGISTROSPORPAGINA;
	    }

	    escribeLibroResponse(libro, response, nombre, request);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * M�todo crea encabezados del reporte
     * @param hoja
     * @param titulos
     */
    private void creaEncabezado(Sheet hoja, String[] titulos) {
	Row prefila = hoja.createRow(0);
	for (int a = 0; a < titulos.length; a++) {
	    Cell cell = prefila.createCell(a);
	    cell.setCellValue(titulos[a].toUpperCase());
	    hoja.autoSizeColumn(a);
	}
    }

    /**
     * M�todo para crear cuerpo del reporte
     * @param hoja
     * @param contribuyentes
     * @param contador
     * @param consulta
     */
    private void creaCuerpo(Sheet hoja, List<TcContribuyente> contribuyentes, int contador, int consulta) {
	try {
	    for (TcContribuyente pista : contribuyentes) {

		hoja.autoSizeColumn(contador + 1);
		Row row = hoja.createRow(contador + 1);
		creaCeldaHoja(pista.getRfc(), 0, row);
		creaCeldaHoja(pista.getNombre(), 1, row);
		creaCeldaHoja(pista.getRegimenFiscal(), 2, row);
//		creaCeldaHoja(pista.getTcDomicilio().getCalle(), 3, row);
//		creaCeldaHoja(pista.getTcDomicilio().getNoInterior(), 4, row);
//		creaCeldaHoja(pista.getTcDomicilio().getNoExterior(), 5, row);
//		creaCeldaHoja(pista.getTcDomicilio().getColonia(), 6, row);
//		creaCeldaHoja(pista.getTcDomicilio().getLocalidad(), 7, row);
//		creaCeldaHoja(pista.getTcDomicilio().getReferencia(), 8, row);
//		creaCeldaHoja(pista.getTcDomicilio().getMunicipio(), 9, row);
//		creaCeldaHoja(pista.getTcDomicilio().getEstado(), 10, row);
//		creaCeldaHoja(pista.getTcDomicilio().getPais(), 11, row);
//		creaCeldaHoja(pista.getTcDomicilio().getCodigoPostal(), 12, row);
		contador++;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * crea celda de la consulta
     * 
     * @param valor
     * @param celdaX
     * @param row
     */
    private void creaCeldaHoja(String valor, int celdaX, Row row) {
	Cell celda = row.createCell(celdaX);
	HSSFRichTextString texto = new HSSFRichTextString(valor);
	celda.getSheet().autoSizeColumn(celdaX);
	celda.setCellValue(texto);
    }

    /**
     * M�todo duelve el archivo (.xlsx) 
     * @param libro
     * @param response
     * @param nombre
     * @param request
     * @throws Exception
     */
    private void escribeLibroResponse(Workbook libro, HttpServletResponse response, String nombre,
	    HttpServletRequest request) throws Exception {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario userEntity = Utilerias.getUserSession(auth);
	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-Disposition", "attachment; filename=" + nombre + ".xlsx");
	libro.write(response.getOutputStream());

	response.getOutputStream().close();

	Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "DESCARGA EXCEL CON NOMBRE " + nombre, userEntity,
		"SELECT", pistasServices, request);
    }

    /**
     * M�todo que envia por correo c�digo de verificaci�n
     * @param codigoVerificacion
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/varificacion.fue", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String varificacion(@RequestBody CodigoVerificacionWrapper codigoVerificacion,
	    HttpServletRequest request) throws Exception {
	String respuesta = null;
	
	try {
	    if (codigoVerificacion.getCorreo() != null && codigoVerificacion.getCodigo() != null) {
		enMailService.send(codigoVerificacion.getCorreo(),
			"C�digo de verificaci�n Facturaci�n Basic.".toUpperCase(),
			textoCodigoVerificacion(codigoVerificacion.getCodigo()), null, null);
		respuesta = "success";
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}

	return respuesta;
    }

    /**
     * M�todo que devuelve String del cuerpo del correo correspondiente al envio de c�digo de verificaci�n
     * @param codigo
     * @return
     */
    public static String textoCodigoVerificacion(String codigo) {
	StringBuilder mensaje = new StringBuilder();

	mensaje.append("CENTRO DE VALIDAC�N DIGITAL CVDSA S.A. DE C.V.\n");
	mensaje.append("\n\n\t\t Ha solicitado un c�digo de verifaci�n de correo elect�nico, para crear una cuenta en nuestro sitio");
	mensaje.append("\n A continuac�n se le muestra el c�digo que deber� proporcionar para continuar con su registro en el sitio web de Quadrum.\n");
	mensaje.append("\n\n\t\t C�digo:      " + codigo);
	mensaje.append("\n\n Le estaremos muy agradecidos si nos env�a sus sugerencias, comentarios o aspectos relacionados con el servicio y nuestra empresa.");
	mensaje.append("\n\n Saludos Cordiales.");
	mensaje.append("\n\n Soporte T�cnico Quadrum-cfdi");
	mensaje.append("\n(55)  5512091825");
	mensaje.append("\nCallCenter: " + ParametroEnum.CORREO_SOPORTE.getValue() + "");
	mensaje.append("\n\n\n" + "Aviso de Privacidad\n");
	mensaje.append("\n");
	mensaje.append("\n");
	mensaje.append("\n");
	mensaje.append("Quadrum como Proveedor Autorizado Certificado por el SAT con domicilio ubicado en:");
	mensaje.append("Montecito n�mero 38 Piso 25 Despacho 22 Colonia N�poles Delegaci�n Benito Ju�rez");
	mensaje.append(" M�xico Distrito Federal C.P 03810 utilizar� sus datos personales aqu� recabados ");
	mensaje.append("para proveerle de los servicios en la emisi�n y generaci�n de CFDI�s que usted ha contratado.");
	mensaje.append("Para mayor informaci�n acerca del tratamiento y de los derechos que puede hacer valer, ");
	mensaje.append("usted puede acceder al aviso de privacidad completo a trav�s de "
		+ ParametroEnum.PRIVACIDAD.getValue() + "\n");

	return mensaje.toString();
    }
    @RequestMapping(value = "/buscarUsuarioPorRfc/{rfc}.json", method = RequestMethod.POST)
	 public TwUsuario buscarDatosUsuario(@PathVariable("rfc") String rfc)throws Exception{
    	TwUsuario twUsuarioBase = null;
    	twUsuarioBase=usuarioServices.usuarioByRFC(aesServices.encriptaCadena(rfc));
    	return twUsuarioBase;
		 
	 }
    
	
	@RequestMapping(value="/manuales.fue",method = RequestMethod.POST)
	public void descargaArchivos(@RequestParam("ruta") String ruta, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws Exception {
		File archivo = new File(ruta);
		byte[] fileContent = Files.readAllBytes(archivo.toPath());
		httpServletResponse.setContentType("application/pdf");
		httpServletResponse.setHeader("Content-Disposition", "inline; filename=" + archivo.getName());
		OutputStream outStream = httpServletResponse.getOutputStream();
		outStream.write(fileContent, 0, fileContent.length);
		outStream.close();
	}
	
	
	@RequestMapping(value="/terminos.fue",method = RequestMethod.POST)
	public void descargaTerminosCondiciones(HttpServletResponse httpServletResponse)
			throws Exception {
		
		String rutaTerminos=servicioParametros.buscarParametros("BasicaTerminosCondiciones").getValor();
		
		Path archivo=Paths.get(rutaTerminos);
		byte[] fileContent = Files.readAllBytes(archivo);
		httpServletResponse.setContentType("application/pdf");
		httpServletResponse.setHeader("Content-Disposition", "inline; filename=" + archivo.getFileName().toString());
		OutputStream outStream = httpServletResponse.getOutputStream();
		outStream.write(fileContent, 0, fileContent.length);
		outStream.close();
	}
}