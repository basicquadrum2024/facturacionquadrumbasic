package com.mx.quadrum.basic.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.services.AcuerdosServices;

@RestController
@RequestMapping(value = "/acuerdo")
public class AcuerdosController {

	@Autowired
	private AcuerdosServices servicioAcuerdo;

	@Autowired
	private AesServices aesServices;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/listar.json", method = RequestMethod.POST)
	public <T> List<T> obtenerLista(@RequestBody Map<String, Object> datos) throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		List<TcContribuyente> listaContribuyente = servicioAcuerdo.obtenerLista(
				mapper.convertValue(datos.get("inicio"), Integer.class),
				mapper.convertValue(datos.get("totalMostrar"), Integer.class));

		List<Map<String, Object>> lista = new ArrayList<>();

		if(listaContribuyente!=null && !listaContribuyente.isEmpty()) {

			for (TcContribuyente tcContribuyente : listaContribuyente) {
				Map<String, Object> mapa = new HashMap<>();
				mapa.put("id", tcContribuyente.getId());
				mapa.put("rfc",   (tcContribuyente.getRfc()!=null ? aesServices.descryAES(tcContribuyente.getRfc()) :null ) );
				mapa.put("nombre", (tcContribuyente.getNombre()!=null ? aesServices.descryAES(tcContribuyente.getNombre()) :null )  );
				mapa.put("rutaManifiesto",  (tcContribuyente.getRutaManifiesto()!=null ? aesServices.descryAES(tcContribuyente.getRutaManifiesto()) :null ) );
				mapa.put("rutaContratoServicios",  (tcContribuyente.getRutaContratoServicios()!=null ? aesServices.descryAES(tcContribuyente.getRutaContratoServicios()) :null ) );
				mapa.put("rutaConvenioConfidencialidad",  (tcContribuyente.getRutaConvenioConfidencialidad()!=null ? aesServices.descryAES(tcContribuyente.getRutaConvenioConfidencialidad()) :null ));
				mapa.put("estatusArchivos", tcContribuyente.getEstatusArchivos());
				lista.add(mapa);
			}
		}

		return (List<T>) lista;
	}

	@RequestMapping(value = "/totalRegistros.json", method = RequestMethod.POST)
	public Number obtenerTotal() throws Exception {
		Number total = servicioAcuerdo.obtenerTotalRegistros();
		return total = (total == null ? 0 : total);
	}
	
	@RequestMapping(value="/pdf.json", method= RequestMethod.GET ) 
	public void descarPdf(@RequestParam("ruta") String ruta,HttpServletResponse response ) throws IOException {
		Path path = Paths.get(ruta);
		byte[] arrayFiles = Files.readAllBytes(path);
		String headerValue = String.format("inline; filename=\"%s\"", "Acuerdo.pdf");
		response.setHeader("Content-Disposition", headerValue);
		ServletOutputStream output = response.getOutputStream();
		output.write(arrayFiles);
		output.close();
		
	}
	
	@RequestMapping(value="/cambiaEstatus.json",method=RequestMethod.POST)
	public void editaEstatus(@RequestBody TcContribuyente tcContribuyente ) throws Exception {
		tcContribuyente.setEstatusArchivos((tcContribuyente.getEstatusArchivos()==0 ? 1 : 0));
		servicioAcuerdo.cambiaEstatus(tcContribuyente);
	}
	

}
