package com.mx.quadrum.basic.controller;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.RmiServices;
import com.mx.quadrum.basic.services.TcDomicilioServices;
import com.mx.quadrum.basic.services.TcReceptorServices;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.ReceptorCfdiv33Wrapper;
import com.mx.quadrum.basic.wrapper.ReceptorCfdiv40Wrapper;
import com.mx.quadrum.basic.wrapper.TcLRfc;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante.Receptor;

/**
 * Clase @RestController contiene metodos para adminsitrar receptor
 * 
 * @author
 *
 */
@RestController
public class TcReceptorController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4437438705443012162L;
	private static final Logger LOGGER=Logger.getLogger(TcReceptorController.class);

	@Autowired
	private TcReceptorServices tcReceptorServices;

	@Autowired
	private TcDomicilioServices domicilioServices;

	@Autowired
	private AesServices aesServices;

	@Autowired
	private PistasServices pistasServices;

	@Autowired
	private RmiServices rmiServices;

	/**
	 * Encargado de guardar ub objeto TcReceptor, pero retoran un objeto
	 * ReceptorCfdiv33Wrapper para actualizar el formulario del recetor
	 * 
	 * @param cfdiv33Wrapper
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tcreceptor/guardarTcReceptor.json", method = RequestMethod.POST)
	public ReceptorCfdiv33Wrapper guardarTcReceptor(@RequestBody ReceptorCfdiv33Wrapper cfdiv33Wrapper,
			HttpServletRequest request) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(authentication);
		ReceptorCfdiv33Wrapper cfdiv33WrapperResponse = new ReceptorCfdiv33Wrapper();
		TcReceptor tcReceptor = convertirReceptorCfdiv33WrapperATcRecepor(cfdiv33Wrapper);
		Utilerias.eventosAdministrador(Utilerias.RECEPTOR,
				"RECEPTOR CON RFC " + tcReceptor.getRfc() + " AGREGADO A LA BASE DE DATOS.", twUsuario, "INSERT",
				pistasServices, request);
		// TcDomicilio tcDomicilio = tcReceptor.getTcDomicilio();
		// domicilioServices.guardarTcDomicilio(tcDomicilio);
		// tcReceptor.setTcDomicilio(tcDomicilio);
		tcReceptorServices.guardarActualizarTcReceptor(tcReceptor);
		tcReceptor = tcReceptorServices.buscaTcReceptorPorRFC(cfdiv33Wrapper.getRfc());
		asignarValoresTcReceporAReceptorCfdiv33Wrapper(cfdiv33WrapperResponse, tcReceptor);
		return cfdiv33WrapperResponse;
	}
	
	
	@RequestMapping(value = "/tcreceptor/guardarTcReceptor_v4.json", method = RequestMethod.POST)
	public void guardarTcReceptorV40(@RequestBody TcReceptor receptor,
			HttpServletRequest request) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(authentication);
		Utilerias.eventosAdministrador(Utilerias.RECEPTOR,
				"RECEPTOR CON RFC " + receptor.getRfc() + " AGREGADO A LA BASE DE DATOS.", twUsuario, "INSERT",
				pistasServices, request);
		tcReceptorServices.guardarActualizarTcReceptor(receptor);
	}
	
	/**
	 * Metodo encargado de buscar un objeto TcReceptor y convertirlo en
	 * ReceptorCfdiv33Wrapper
	 * 
	 * @param wrapper
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tcreceptor/buscarReceptorPorRfc_v4.json", method = RequestMethod.POST)
	public ReceptorCfdiv40Wrapper buscarReceptorPorRfcV40(@RequestBody ReceptorCfdiv40Wrapper wrapper,
			HttpServletRequest request) throws Exception {
		ReceptorCfdiv40Wrapper receptorCfdiv40Wrapper = new ReceptorCfdiv40Wrapper();
		TcLRfc tcLRfc = rmiServices.obtenerObjetoV40(TcLRfc.class, "consultaTcLRfcPorRfc", wrapper.getRfc());
		
		if(null==tcLRfc)
			tcLRfc=validarRfcPublicoEngeral(wrapper.getRfc());
		
		if (null == tcLRfc) {
			receptorCfdiv40Wrapper.setExisteLrfc(false);
			receptorCfdiv40Wrapper.setRfc("Este RFC del receptor no existe en la lista de RFC inscritos no cancelados del SAT.");
			return receptorCfdiv40Wrapper;
		}
		receptorCfdiv40Wrapper.setRfc(tcLRfc.getRfc());
		receptorCfdiv40Wrapper.setNombre(tcLRfc.getNombre());
		receptorCfdiv40Wrapper.setDomicilioFiscalReceptor(String.valueOf(tcLRfc.getCodigoPostal()));
		TcReceptor tcReceptor = tcReceptorServices.buscaTcReceptorPorRFC(wrapper.getRfc());
		if (tcReceptor != null) {
			receptorCfdiv40Wrapper.setId(tcReceptor.getId());
		//	receptorCfdiv40Wrapper.setResidenciaFiscal(tcReceptor.getResidenciaFiscal());
		//	receptorCfdiv40Wrapper.setNumRegIdTrib(tcReceptor.getNumRegIdTrib());
		//	receptorCfdiv40Wrapper.setUsoCfdi(tcReceptor.getUsoCfdi());
		//	receptorCfdiv40Wrapper.setRegimenFiscalReceptor(tcReceptor.getRegimenFiscalReceptor());
		//	receptorCfdiv40Wrapper.setDomicilio(tcReceptor.getDomicilio());
		}
		return receptorCfdiv40Wrapper;
	}

	/**
	 * Metodo encargado de buscar un objeto TcReceptor y convertirlo en
	 * ReceptorCfdiv33Wrapper
	 * 
	 * @param wrapper
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tcreceptor/buscarReceptorPorRfc.json", method = RequestMethod.POST)
	public ReceptorCfdiv33Wrapper buscarReceptorPorRfc(@RequestBody ReceptorCfdiv33Wrapper wrapper,
			HttpServletRequest request) throws Exception {
		ReceptorCfdiv33Wrapper cfdiv33WrapperResponse = new ReceptorCfdiv33Wrapper();
		TcLRfc tcLRfc = rmiServices.obtenerObjeto(TcLRfc.class, "buscarTcLRfcPorRfc", wrapper.getRfc());
		
		if(null==tcLRfc)
			tcLRfc=validarRfcPublicoEngeral(wrapper.getRfc());
		
		if (null == tcLRfc) {
			cfdiv33WrapperResponse.setExisteLrfc(false);
			cfdiv33WrapperResponse.setRfc("Este RFC del receptor no existe en la lista de RFC inscritos no cancelados del SAT.");
			return cfdiv33WrapperResponse;
		}
		cfdiv33WrapperResponse.setRfc(wrapper.getRfc());
		TcReceptor tcReceptor = tcReceptorServices.buscaTcReceptorPorRFC(wrapper.getRfc());
		if (tcReceptor != null) {
			asignarValoresTcReceporAReceptorCfdiv33Wrapper(cfdiv33WrapperResponse, tcReceptor);
		}
		return cfdiv33WrapperResponse;
	}
	
	/**
	 * 
	 * @param rfc
	 * @return
	 */
	public TcLRfc validarRfcPublicoEngeral(String rfc) {
		TcLRfc tcLRfc=null;
		if(rfc.equals("XAXX010101000")) 
			tcLRfc=new TcLRfc(rfc);
		return tcLRfc;
	}

	/**
	 * Metodo encargado de convertir un objeto TcReceptor a un objeto
	 * ReceptorCfdiv33Wrapper
	 * 
	 * @param cfdiv33WrapperResponse
	 * @param tcReceptor
	 */
	private void asignarValoresTcReceporAReceptorCfdiv33Wrapper(ReceptorCfdiv33Wrapper cfdiv33WrapperResponse,
			TcReceptor tcReceptor) {
		cfdiv33WrapperResponse.setId(tcReceptor.getId());
		cfdiv33WrapperResponse.setRfc(tcReceptor.getRfc());
		cfdiv33WrapperResponse.setNombre(tcReceptor.getNombre());

		cfdiv33WrapperResponse.setResidenciaFiscal(tcReceptor.getResidenciaFiscal());
		cfdiv33WrapperResponse.setNumRegIdTrib(tcReceptor.getNumRegIdTrib());
		cfdiv33WrapperResponse.setUsoCFDI(tcReceptor.getUsoCfdi());
		cfdiv33WrapperResponse.setDomicilio(tcReceptor.getDomicilio());
//	if (null != tcReceptor.getTcDomicilio()) {
//	    TcDomicilio tcDomicilio = tcReceptor.getTcDomicilio();
//	    cfdiv33WrapperResponse.setIdDomicilio(tcDomicilio.getId());
//	    cfdiv33WrapperResponse.setCalle(tcDomicilio.getCalle());
//	    cfdiv33WrapperResponse.setNoExterior(tcDomicilio.getNoExterior());
//	    cfdiv33WrapperResponse.setNoInterior(tcDomicilio.getNoInterior());
//	    cfdiv33WrapperResponse.setColonia(tcDomicilio.getColonia());
//	    cfdiv33WrapperResponse.setLocalidad(tcDomicilio.getLocalidad());
//	    cfdiv33WrapperResponse.setReferencia(tcDomicilio.getReferencia());
//	    cfdiv33WrapperResponse.setMunicipio(tcDomicilio.getMunicipio());
//	    cfdiv33WrapperResponse.setEstado(tcDomicilio.getEstado());
//	    cfdiv33WrapperResponse.setPais(tcDomicilio.getPais());
//	    cfdiv33WrapperResponse.setCodigoPostal(tcDomicilio.getCodigoPostal());
//	}

	}

	/**
	 * Metodo encargado de convertir un objeto ReceptorCfdiv33Wrapper a un objeto
	 * TcReceptor
	 * 
	 * @param cfdiv33WrapperResponse
	 * @return
	 */
	private TcReceptor convertirReceptorCfdiv33WrapperATcRecepor(final ReceptorCfdiv33Wrapper cfdiv33WrapperResponse) {
		TcReceptor tcReceptor = new TcReceptor();
		tcReceptor.setId(cfdiv33WrapperResponse.getId());
		tcReceptor.setRfc(StringUtils.trimToNull(cfdiv33WrapperResponse.getRfc()));
		tcReceptor.setNombre(StringUtils.trimToNull(cfdiv33WrapperResponse.getNombre()));
		tcReceptor.setDomicilio(StringUtils.trimToNull(cfdiv33WrapperResponse.getDomicilio()));
		if (null != cfdiv33WrapperResponse.getResidenciaFiscal())
			tcReceptor.setResidenciaFiscal(StringUtils.trimToNull(cfdiv33WrapperResponse.getResidenciaFiscal()));
		tcReceptor.setNumRegIdTrib(StringUtils.trimToNull(cfdiv33WrapperResponse.getNumRegIdTrib()));
		if (null != cfdiv33WrapperResponse.getUsoCFDI())
			tcReceptor.setUsoCfdi(StringUtils.trimToNull(cfdiv33WrapperResponse.getUsoCFDI()));
//	TcDomicilio tcDomicilio = new TcDomicilio();
//	tcDomicilio.setId(cfdiv33WrapperResponse.getIdDomicilio());
//	tcDomicilio.setCalle(StringUtils.trimToNull(cfdiv33WrapperResponse.getCalle()));
//	tcDomicilio.setNoExterior(StringUtils.trimToNull(cfdiv33WrapperResponse.getNoExterior()));
//	tcDomicilio.setNoInterior(StringUtils.trimToNull(cfdiv33WrapperResponse.getNoInterior()));
//	tcDomicilio.setColonia(StringUtils.trimToNull(cfdiv33WrapperResponse.getColonia()));
//	tcDomicilio.setLocalidad(StringUtils.trimToNull(cfdiv33WrapperResponse.getLocalidad()));
//	tcDomicilio.setReferencia(StringUtils.trimToNull(cfdiv33WrapperResponse.getReferencia()));
//	tcDomicilio.setMunicipio(StringUtils.trimToNull(cfdiv33WrapperResponse.getMunicipio()));
//	tcDomicilio.setEstado(StringUtils.trimToNull(cfdiv33WrapperResponse.getEstado()));
//	tcDomicilio.setPais(StringUtils.trimToNull(cfdiv33WrapperResponse.getPais()));
//	tcDomicilio.setCodigoPostal(StringUtils.trimToNull(cfdiv33WrapperResponse.getCodigoPostal()));
		// tcReceptor.setTcDomicilio(tcDomicilio);
		return tcReceptor;
	}
}
