package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.IUsuarioServices;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.util.Utilerias;

/**
 * Clase controlador REST administra cuentas de usuario
 * @author 
 *
 */
@RestController
@RequestMapping(value = "/administradorCuentasController")
public class AdministradorCuentasController implements Serializable {

    @Autowired
    private IUsuarioServices usuarioServices;

    @Autowired
    private AesServices aesServices;

    @Autowired
    private PistasServices pistasServices;

    private static final Logger logger = Logger.getLogger(AdministradorCuentasController.class);

    /**
     * M�todo que obtiene el usuario para saber estatus de 30 dias de inactividad
     * 
     * @param rfcEmisor
     * @return TwUsuario
     * @throws Exception
     */
    @RequestMapping(value = "/obtenerContacto/{rfc}/{bandera}.json", method = RequestMethod.POST)
    public TwUsuario obtenerEstatusCuentaEmisor(@PathVariable("rfc") String rfcEmisor,
	    @PathVariable("bandera") String bandera, HttpServletRequest request) throws Exception {
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario usuario = Utilerias.getUserSession(authentication);
	TwUsuario twUsuario = null;

	twUsuario = usuarioServices.usuarioByRFC(aesServices.encriptaCadena(rfcEmisor));

	if (twUsuario != null) {
	    if (bandera.equals("inactividad")) {
		Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "CONSULTA DE INACTIVIDAD DEL USUARIO "
			+ rfcEmisor, usuario, "SELECT", pistasServices, request);
	    }
	    if (bandera.equals("bloqueo")) {
		Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "CONSULTA DE BLOQUEO DEL USUARIO " + rfcEmisor,
			usuario, "SELECT", pistasServices, request);
	    }
	    if (bandera.equals("consulta")) {
		Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR,
			"CONSULTA DE DATOS DE CONTACTO DEL CONTRIBUYENTE " + rfcEmisor, usuario, "SELECT",
			pistasServices, request);
	    }
	} else {

	    if (bandera.equals("inactividad")) {
		Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "CONSULTA DE INACTIVIDAD DEL USUARIO "
			+ rfcEmisor + ", PERO NO EXISTE EN EL SISTEMA", usuario, "SELECT", pistasServices, request);
	    }
	    if (bandera.equals("bloqueo")) {
		Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "CONSULTA DE BLOQUEO DEL USUARIO " + rfcEmisor
			+ ", PERO NO EXISTE EN EL SISTEMA", usuario, "SELECT", pistasServices, request);
	    }
	    if (bandera.equals("consulta")) {
		Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR,
			"CONSULTA DE DATOS DE CONTACTO DEL CONTRIBUYENTE " + rfcEmisor
				+ ", PERO NO EXISTE EN EL SISTEMA", usuario, "SELECT", pistasServices, request);
	    }
	}
	return twUsuario;
    }

    /**
     * M�todo para Activar la cuenta del usuario, coloca statusSession(1)
     * 
     * @param estatus
     */
    @RequestMapping(value = "/guardarContacto.json", method = RequestMethod.POST)
    public void guardarContacto(@RequestBody TwUsuario usuario, HttpServletRequest request) throws Exception {
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario twUsuario = Utilerias.getUserSession(authentication);
	Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "SE ACTUALIZARON DATOS DE CONTACTO ", twUsuario,
		"INSERT", pistasServices, request);
	usuarioServices.saveorUpdate(usuario);
    }

    /**
     * M�todo modifica el n�mero de intentos de inicio de session 
     * @param usuario
     * @param request
     * @throws Exception
     */
    @RequestMapping(value = "/activarCuenta.json", method = RequestMethod.POST)
    public void activarCuenta(@RequestBody TwUsuario usuario, HttpServletRequest request) throws Exception {
	usuario.setUltimaconexion(new Date());
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario twUsuario = Utilerias.getUserSession(authentication);
	Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "SE ACTIVO CUENTA DEL USUARIO ", twUsuario, "UPDATE",
		pistasServices, request);
	usuarioServices.getUpdateIntentos(aesServices.encriptaCadena(usuario.getRfce()), 0);
    }

}
