package com.mx.quadrum.basic.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Date;
import java.util.List;

import javax.security.cert.CertificateException;
import javax.security.cert.CertificateExpiredException;
import javax.security.cert.X509Certificate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.ssl.PKCS8Key;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.Foliador;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.DocumentosLegalesServices;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.FoliadorService;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.RmiServices;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.RegistrarCliente;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.util.ValidacionCertificadosContribuyente;
import com.mx.quadrum.basic.wrapper.TcLco;
import com.mx.quadrum.cfdi.utilerias.UtileriasArchivosPem;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

/**
 * Clase controlador REST contiene metodos para la administraci�n de emisor
 * (contribuyente)
 * 
 * @author
 *
 */
@RestController
@RequestMapping(value = "/emisorController")
public class EmisorController extends AbstractControllerHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4904550211544053502L;

	final static Logger logger = Logger.getLogger(EmisorServices.class);

	@Autowired
	private EmisorServices emisorServices;

	@Autowired
	private AesServices aesServices;

	@Autowired
	private FoliadorService foliadorService;

	@Autowired
	private PistasServices pistasServices;

	@Autowired
	private DocumentosLegalesServices documentosLegalesServices;

	@Autowired
	private RmiServices rmiServices;

	@RequestMapping(value = "/validarContribuyenteLco.json", method = RequestMethod.POST)
	public boolean validarContribuyenteLco(@RequestBody TcContribuyente tcContribuyente) throws Exception {
		List<TcLco> listaLco = rmiServices.obtenerObjetosV40(TcLco[].class, "consultaTcLcoPorRfc",
				tcContribuyente.getRfc());
		return null == listaLco || listaLco.isEmpty() ? false : true;
	}

	/**
	 * M�todo que v�lida el RFC de emisor
	 * 
	 * @param rfc
	 * @param request
	 * @return String con respuesta error si el RFC existe en nuestros registro o
	 *         success si no lo est�
	 * @throws Exception
	 */
	@RequestMapping(value = "/validarRfcEmisor/{rfc}/.json", method = RequestMethod.POST)
	public String validaRfcEmisor(@PathVariable("rfc") String rfc, HttpServletRequest request) throws Exception {
		String respuesta = "";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		try {
			if (emisorServices.buscarPorRfcE(aesServices.encriptaCadena(rfc))) {
				Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
						"EL CONTRIBUYENTE " + rfc + " YA ESTA DADO DE ALTA EN EL SISTEMA.", twUsuario, "ERROR",
						pistasServices, request);
				respuesta = "error";
			} else {
				respuesta = "success";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return respuesta;

	}

	/**
	 * M�todo actualiza emisor, se validan los certificados, se guardan en base de
	 * datos y se registra en el WS
	 * 
	 * @param fileCer  archivo .cer
	 * @param fileKey  archivo .key
	 * @param objecto  emisor
	 * @param response
	 * @param request
	 * @return respuesta a las validaciones correspondientes
	 * @throws Exception
	 */
	@RequestMapping(value = "/actualizaEmisor.json", method = RequestMethod.POST)
	public @ResponseBody String[] actualizaEmisor(
			@RequestParam(value = "fileCer", required = false) MultipartFile fileCer,
			@RequestParam(value = "fileKey", required = false) MultipartFile fileKey,
			@RequestParam(value = "fileCerFiel", required = false) MultipartFile fileCerFiel,
			@RequestParam(value = "fileKeyFiel", required = false) MultipartFile fileKeyFiel,
			@RequestParam("objeto") Object objecto, HttpServletResponse response, HttpServletRequest request)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		TcContribuyente emisor = mapper.readValue(objecto.toString(), TcContribuyente.class);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(authentication);
		InputStream auxcer;
		InputStream auxkey;
		String[] respuesta = new String[2];
		try {
			if (fileCer != null && fileKey != null) {
				byte[] bytesCertificadoEvaluar = fileCer.getBytes();
				java.security.cert.X509Certificate certificadoEmisor = UtileriasArchivosPem
						.obtenerCertificado(bytesCertificadoEvaluar);
				BigInteger byteArray = certificadoEmisor.getSerialNumber();
				String noCertificado = new String(byteArray.toByteArray());
				TcLco tcLco = rmiServices.obtenerObjeto(TcLco.class, "buscarUnicoTcLcoPorRfcCertificado",
						emisor.getRfc(), noCertificado);
				if (null == tcLco) {
					respuesta[0] = "Error";
					respuesta[1] = String.format(
							"El rfc %s no se encuentra en la LCO o Lista de Contribuyentes Obligados", emisor.getRfc());
				}

				auxcer = fileCer.getInputStream();
				auxkey = fileKey.getInputStream();
				respuesta = validaArchivos(auxcer, auxkey, emisor.getClave(), emisor.getRfc(), request);
				if (respuesta[1].equals("CertificadosOK")) {
					RegistrarCliente.registra(ParametroEnum.URLREGISTRO.getValue(), ParametroEnum.USUARIOWS.getValue(),
							ParametroEnum.PASSWORDWS.getValue(), emisor.getRfc());
					String byCer = Base64.encodeBase64String(fileCer.getBytes());
					String byKey = Base64.encodeBase64String(fileKey.getBytes());

					emisor.setLlaveCer(byCer);
					emisor.setLlaveKey(byKey);

					String cadenaOriginalSelloContrato = documentosLegalesServices
							.obtenerCadenaOriginalSelloContrato("Contrato");
					String cadenaOriginalSelloConvenio = documentosLegalesServices
							.obtenerCadenaOriginalSelloContrato("Convenio");
					String cadenaOriginalSelloManifiesto = documentosLegalesServices
							.obtenerCadenaOriginalSelloContrato("Manifiesto");

					byte[] bytesCertificadoRepLegal = Files
							.readAllBytes(new File(ParametroEnum.RUTA_CER_REP_LEG.getValue()).toPath());
					byte[] bytesLLaveRepLegal = Files
							.readAllBytes(new File(ParametroEnum.RUTA_KEY_REP_LEG.getValue()).toPath());
					ValidacionCertificadosContribuyente documentosFielSelloContrato = new ValidacionCertificadosContribuyente(
							bytesCertificadoRepLegal, bytesLLaveRepLegal,
							aesServices.descryAES(ParametroEnum.VALOR_PASWW_LLAVE_RL.getValue()), "PESL821028G87",
							cadenaOriginalSelloContrato, "FIEL");
					String selloDigitalContrato = documentosFielSelloContrato.generarFirma();
					ValidacionCertificadosContribuyente documentosFielSelloConvenio = new ValidacionCertificadosContribuyente(
							bytesCertificadoRepLegal, bytesLLaveRepLegal,
							aesServices.descryAES(ParametroEnum.VALOR_PASWW_LLAVE_RL.getValue()), "PESL821028G87",
							cadenaOriginalSelloConvenio, "FIEL");
					String selloDigitalConvenio = documentosFielSelloConvenio.generarFirma();
					ValidacionCertificadosContribuyente documentosFielSelloManifiesto = new ValidacionCertificadosContribuyente(
							bytesCertificadoRepLegal, bytesLLaveRepLegal,
							aesServices.descryAES(ParametroEnum.VALOR_PASWW_LLAVE_RL.getValue()), "PESL821028G87",
							cadenaOriginalSelloManifiesto, "FIEL");
					String selloDigitalManifiesto = documentosFielSelloManifiesto.generarFirma();

					String cadenaOriginalContrato = documentosLegalesServices.obtenerCadenaOriginal(emisor,
							selloDigitalContrato, "Contrato");
					String cadenaOriginalConvenio = documentosLegalesServices.obtenerCadenaOriginal(emisor,
							selloDigitalConvenio, "Convenio");
					String cadenaOriginalManifiesto = documentosLegalesServices.obtenerCadenaOriginal(emisor,
							selloDigitalManifiesto, "Manifiesto");

					byte[] bytesCertificado = fileCerFiel.getBytes();
					byte[] bytesLLave = fileKeyFiel.getBytes();
					ValidacionCertificadosContribuyente documentosFielContrato = new ValidacionCertificadosContribuyente(
							bytesCertificado, bytesLLave, emisor.getClaveFiel(), emisor.getRfc(),
							cadenaOriginalContrato, "FIEL");
					String firmaElectronicaContrato = documentosFielContrato.generarFirma();

					ValidacionCertificadosContribuyente documentosFielConvenio = new ValidacionCertificadosContribuyente(
							bytesCertificado, bytesLLave, emisor.getClaveFiel(), emisor.getRfc(),
							cadenaOriginalConvenio, "FIEL");
					String firmaElectronicaConvenio = documentosFielConvenio.generarFirma();

					ValidacionCertificadosContribuyente documentosFielManifiesto = new ValidacionCertificadosContribuyente(
							bytesCertificado, bytesLLave, emisor.getClaveFiel(), emisor.getRfc(),
							cadenaOriginalConvenio, "FIEL");
					String firmaElectronicaManifiesto = documentosFielManifiesto.generarFirma();

					String rutaGuardadoContrato = UtileriasCfdi.crearDireccionGuardadoArchivo(
							ParametroEnum.RUTACONTRATOS.getValue(), new Date(), emisor.getRfc(),
							String.format("%s_CONTRATO_DE_PRESTACION_DE_SERVICIOS.pdf", emisor.getRfc()));
					String rutaGuardadoConvenio = UtileriasCfdi.crearDireccionGuardadoArchivo(
							ParametroEnum.RUTACONTRATOS.getValue(), new Date(), emisor.getRfc(),
							String.format("%s_CONVENIO_DE_CONFIDENCIALIDAD.pdf", emisor.getRfc()));
					String rutaGuardadoManifiesto = UtileriasCfdi.crearDireccionGuardadoArchivo(
							ParametroEnum.RUTACONTRATOS.getValue(), new Date(), emisor.getRfc(),
							String.format("%s_MANIFIESTO.pdf", emisor.getRfc()));

					try {
						byte[] bytesContrato = documentosLegalesServices.obtenerContrato(emisor,
								Utilerias.dividirCadena(cadenaOriginalContrato, 106),
								Utilerias.dividirCadena(firmaElectronicaContrato, 106),
								Utilerias.dividirCadena(selloDigitalContrato, 106));
						UtileriasCfdi.escribirBytesEnArchivos(bytesContrato, rutaGuardadoContrato, ".pdf");

						byte[] bytesConvenio = documentosLegalesServices.obtenerConvenio(emisor,
								Utilerias.dividirCadena(cadenaOriginalConvenio, 106),
								Utilerias.dividirCadena(firmaElectronicaConvenio, 106),
								Utilerias.dividirCadena(selloDigitalConvenio, 106));
						UtileriasCfdi.escribirBytesEnArchivos(bytesConvenio, rutaGuardadoConvenio, ".pdf");

						byte[] bytesManifiesto = documentosLegalesServices.obtenerManifiesto(emisor,
								Utilerias.dividirCadena(cadenaOriginalManifiesto, 106),
								Utilerias.dividirCadena(firmaElectronicaManifiesto, 106),
								Utilerias.dividirCadena(selloDigitalManifiesto, 106));
						UtileriasCfdi.escribirBytesEnArchivos(bytesManifiesto, rutaGuardadoManifiesto, ".pdf");
					} catch (Exception e) {
						logger.error(String.format("Error en metodo guardar archivos: %s",
								UtileriasCfdi.imprimeStackError(e)));
					}

					emisor.setRutaContratoServicios(rutaGuardadoContrato);
					emisor.setRutaConvenioConfidencialidad(rutaGuardadoConvenio);
					emisor.setRutaManifiesto(rutaGuardadoManifiesto);
					Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
							"EL CONTRIBUYENTE " + emisor.getRfc() + " SE AGREGO AL SISTEMA.", twUsuario, "INSERT",
							pistasServices, request);

					respuesta = emisorServices.actualizaEmisor(emisor);

					TcContribuyente contribuyente = emisorServices
							.buscarPorRfcEmisor(aesServices.encriptaCadena(respuesta[2]));

					Foliador foliador = foliadorService.findFoliadorByContribuyente(contribuyente.getId());
					if (foliador == null) {
						foliador = new Foliador();
						foliador.setSerie("Factura");
						foliador.setFolio(0L);
						foliador.setTcContribuyente(contribuyente);
						foliadorService.guardarFoliador(foliador);
					}

				}
			} else {
				respuesta[0] = "Error";
				respuesta[1] = "Los certificados son requeridos";
			}

		} catch (Exception e) {
			respuesta[0] = "error";
			respuesta[1] = "No se puede actualizar al Contribuyente";
			logger.error(String.format("Error en metodo actualizaEmisor: %s", UtileriasCfdi.imprimeStackError(e)));
		}
		return respuesta;
	}

	/**
	 * M�todo que busca el emisor por RFC
	 * 
	 * @return objeto Emisor
	 * @throws Exception
	 */
	@RequestMapping(value = "/datosEmisor.json", method = RequestMethod.POST)
	public @ResponseBody TcContribuyente datosEmisor() throws Exception {
		TcContribuyente emisor = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario usuario = Utilerias.getUserSession(auth);
		emisor = emisorServices.buscarPorRfcEmisor(aesServices.encriptaCadena(usuario.getRfce()));
		return emisor;
	}

	/**
	 * M�todoq ue devuelve todos los emisores registrados
	 * 
	 * @return lista de TcContribuyente
	 * @throws Exception
	 */
	@RequestMapping(value = "/todosEmisores.json", method = RequestMethod.POST)
	public List<TcContribuyente> todosEmisores() throws Exception {
		return emisorServices.clientesRegistrados();
	}

	/**
	 * M�todo que actualiza emisor cuando no se modifican los certificados
	 * 
	 * @param objecto
	 * @param response
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/actualizaEmisorSinCer.json", method = RequestMethod.POST)
	public @ResponseBody String[] actualizaEmisorSinCer(@RequestParam("objeto") Object objecto,
			HttpServletResponse response, HttpServletRequest request) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		TcContribuyente emisor = mapper.readValue(objecto.toString(), TcContribuyente.class);
		String[] respuesta = new String[2];
		try {
			respuesta = emisorServices.actualizaEmisor(emisor);
		} catch (Exception e) {
			respuesta[0] = "error";
			respuesta[1] = "No se puede actualizar al Contribuyente";
			e.printStackTrace();
		}
		return respuesta;
	}

	/**
	 * M�toso que v�lida los certificados del emisor
	 * 
	 * @param cer
	 * @param key
	 * @param clave
	 * @param rfc
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String[] validaArchivos(InputStream cer, InputStream key, String clave, String rfc,
			HttpServletRequest request) throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		String[] respuesta = new String[2];
		ByteArrayOutputStream inputStreamOutCer = null;
		ByteArrayOutputStream inputStreamOutKey = null;
		byte[] clavePrivada = null;
		byte[] clavePublica = null;
		String password;
		InputStream archivoClavePublica;
		InputStream archivoClavePrivada;
		X509Certificate certificado = null;
		try {
			if (cer != null && key != null) {

				inputStreamOutCer = new ByteArrayOutputStream();
				inputStreamOutKey = new ByteArrayOutputStream();
				IOUtils.copy(cer, inputStreamOutCer);
				IOUtils.copy(key, inputStreamOutKey);
				archivoClavePrivada = new ByteArrayInputStream(inputStreamOutKey.toByteArray());
				archivoClavePublica = new ByteArrayInputStream(inputStreamOutCer.toByteArray());
				clavePrivada = IOUtils.toByteArray(archivoClavePrivada);
				clavePublica = IOUtils.toByteArray(archivoClavePublica);
				password = clave;

				String[] validaCorrespondencias = validaCorrespondencias(clavePrivada, clavePublica, password, request);
				if (validaCorrespondencias[1].equals("ok")) {
					certificado = validacionesCertificado(clavePublica);
					String[] validaFechas = validaFecha(certificado, request, twUsuario);
					if (validaFechas[1].equals("ok")) {

						if (certificado != null) {
							if (validaCSD(certificado)) {
								if (correspondenciaEmisor(certificado, rfc)) {

									respuesta[0] = "Succces";
									respuesta[1] = "CertificadosOK";
									Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
											"LOS CERTIFICADOS DEL CONTRIBUTYENTE  " + rfc
													+ " FUERON VALIDADOS CORRECTAMENTE.",
											twUsuario, "INSERT", pistasServices, request);
								} else {
									respuesta[0] = "Error";
									respuesta[1] = "Los certificados no corresponden al Emisor.";
									Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
											"LOS CERTIFICADOS NO CORRESPONDEN AL CONTRIBUTYENTE  " + rfc, twUsuario,
											"ERROR", pistasServices, request);
								}
							} else {
								respuesta[0] = "Error";
								respuesta[1] = "Los archivos deben ser de tipo Certificado de Sello Digital";
								Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
										"LOS CERTIFICADOS DEBEN SER TIPO 'CERTIFICADO DE SELLO DIGITAL'", twUsuario,
										"ERROR", pistasServices, request);
							}
						}
					} else {
						respuesta = validaFechas;
					}

				} else {
					respuesta = validaCorrespondencias;
				}
			}

		} catch (Exception f) {
			respuesta[0] = "Error";
			respuesta[1] = "Los certificados no pueden ser validados correctamente, favor de verificar";
			Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
					"LOS CERTIFICADOS NO PUEDEN SER VALIDADOS CORRECTAMENTE.", twUsuario, "ERROR", pistasServices,
					request);
		}
		return respuesta;
	}

	/**
	 * M�todo que valida el password y que la llave privada corresponda a la llave
	 * publica
	 * 
	 * @return true si el password y llave privada corresponden, en otro caso false
	 * @throws Exception
	 */
	public String[] validaCorrespondencias(byte[] clavePrivada, byte[] clavePublica, String clave,
			HttpServletRequest request) throws Exception {
		String[] respuesta = new String[2];
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(authentication);
		try {

			PKCS8Key pkcs8 = new PKCS8Key(clavePrivada, clave.toCharArray());
			PrivateKey pk = pkcs8.getPrivateKey();
			X509Certificate cert = X509Certificate.getInstance(clavePublica);
			Signature firma = Signature.getInstance("SHA1withRSA");
			firma.initSign(pk);
			byte[] firmado = firma.sign();
			firma.initVerify(cert.getPublicKey());
			if (firma.verify(firmado)) {
				respuesta[0] = "Succces";
				respuesta[1] = "ok";
				Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE, "LA CLAVE CORRESPONDE A LOS CERTIFICADOS.",
						twUsuario, "INSERT", pistasServices, request);
			} else {
				respuesta[0] = "Error";
				respuesta[1] = "Ocurrio un error al ingresar los Certificados, favor de Verificar.";
				Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
						"OCURRIO UN ERROR AL INGRESAR LOS CERTIFICADOS, FAVOR DE VERIFICAR.", twUsuario, "ERROR",
						pistasServices, request);
			}
		} catch (GeneralSecurityException e) {
			respuesta[0] = "Error";
			respuesta[1] = "La Clave no corresponde a los archivos ingresdos, favor de Verificar.";
			Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
					"LA CLAVE NO CORRESPONDE A LOS ARCHIVOS INGRESDOS, FAVOR DE VERIFICAR.", twUsuario, "ERROR",
					pistasServices, request);

		} catch (CertificateException e) {
			respuesta[0] = "Error";
			respuesta[1] = "CertificateException";

		}
		return respuesta;
	}

	/**
	 * M�todo que valida certificados con su clavepublica
	 * 
	 * @param clavePublica
	 * @return
	 */
	public X509Certificate validacionesCertificado(byte[] clavePublica) {
		X509Certificate certificado = null;
		try {
			certificado = X509Certificate.getInstance(clavePublica);
		} catch (CertificateException e) {
			e.printStackTrace();

		}
		return certificado;
	}

	/**
	 * M�todo que v�lida que le certificado corresponda al RFC
	 * 
	 * @param cert
	 * @param rfc
	 * @return
	 */
	public boolean correspondenciaEmisor(X509Certificate cert, String rfc) {
		boolean value = true;
		String Issuer = cert.getSubjectDN().getName();
		if (!Issuer.contains(rfc)) {
			value = false;
		}
		return value;
	}

	/**
	 * M�todo que revisa la valides del certificado (fecha y hora)
	 * 
	 * @param cert Certificado correspondiente al emisor
	 * @return caduco, fechaErronea si el certificado esta mal, en otro caso cadena
	 *         vacia
	 */

	public String[] validaFecha(X509Certificate cert, HttpServletRequest request, TwUsuario twUsuario)
			throws Exception {
		String[] respuesta = new String[2];

		respuesta[0] = "Succces";
		respuesta[1] = "ok";
		try {
			cert.checkValidity();
		} catch (CertificateExpiredException e) {
			logger.error(e.getMessage());
			respuesta[0] = "Error";
			respuesta[1] = "El certificado del emisor a caducado, favor de verificar.";
			Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE,
					"LA CLAVE NO CORRESPONDE A LOS ARCHIVOS INGRESDOS, FAVOR DE VERIFICAR.", twUsuario, "ERROR",
					pistasServices, request);

		} catch (CertificateException e) {
			logger.error(e.getMessage());
			respuesta[0] = "Error";
			respuesta[1] = "La fecha del certificado del emisor es erronea, favor de verificar";

		}
		return respuesta;
	}

	@RequestMapping(value = "/encriptarCertificadosEmisor.json", method = RequestMethod.GET)
	public void encriptarCertificadosEmisor() throws Exception {
		List<TcContribuyente> listaEmisores = emisorServices.clientesRegistrados();
		if (null != emisorServices) {
			for (TcContribuyente tcContribuyente : listaEmisores) {
				emisorServices.actualizarDatosContribuyente(tcContribuyente.getId(),
						aesServices.encriptaCadena(tcContribuyente.getLlaveCer()),
						aesServices.encriptaCadena(tcContribuyente.getLlaveKey()));
			}
		}
	}

	/**
	 * M�todo que revisa que sea un archivo CSD
	 * 
	 * @param cert Certificado correspondiente al emisor
	 * @return true o false
	 */
	public boolean validaCSD(X509Certificate cert) {
		boolean value = false;
		if (cert.getSubjectDN().toString().startsWith("OU")) {
			value = true;
		}
		return value;
	}

	@RequestMapping(value = "/validarLongitudRfc/{rfc}.json", method = RequestMethod.POST)
	public String validaLongitudRfc(@PathVariable("rfc") String rfc) throws Exception {
		String tipo = "";
		if (rfc.length() == 12) {
			tipo = "M";
		}
		if (rfc.length() == 13) {
			tipo = "F";
		}
		return tipo;
	}
}
