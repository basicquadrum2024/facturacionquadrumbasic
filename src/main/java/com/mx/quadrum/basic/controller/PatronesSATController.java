package com.mx.quadrum.basic.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.entity.patronesSAT.PatronesCfdi;
import com.mx.quadrum.basic.services.PatronesCfdiServices;

/**
 * Clase  @RestController contiene metodos para administrar patronesSAT
 * @author 
 *
 */
@RestController
@RequestMapping(value = "/patronesSATController")
public class PatronesSATController extends AbstractControllerHandler {

    @Autowired
    private PatronesCfdiServices patronesCfdiServices;

    /**
     * M�todo devuelve un mapa de patrones 
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/listarPatrones.json", method = RequestMethod.POST)
    public Map<String, String> listarPatrones() throws Exception {
	List<PatronesCfdi> lista = new ArrayList<PatronesCfdi>();
	lista = patronesCfdiServices.listaPatrones();
	Map<String, String> mapaPatrones = new HashMap<String, String>();
	for (PatronesCfdi patron : lista) {
	    mapaPatrones.put(patron.getNombre(), patron.getPatron());
	}
	return mapaPatrones;
    }

}