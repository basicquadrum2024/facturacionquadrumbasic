package com.mx.quadrum.basic.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.Buzon;
import com.mx.quadrum.basic.entity.Paginacion;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.BuzonServices;
import com.mx.quadrum.basic.services.MailService;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.util.ExcepcionesFacturacionBasica;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.BusquedasWrapper;
import com.mx.quadrum.basic.wrapper.BuzonWrapper;

/**
 * Clase rest controler contiene los metodos del modulo de buzon de sugerencias y quejas
 * @author 
 *
 */
@RestController
@RequestMapping(value = "/buzonController")
public class BuzonController {
	
	@Autowired
    private PistasServices pistasServices;
	
	@Autowired
	private BuzonServices buzonServices;
	
	@Autowired
    private MailService enMailService;
	
	@Autowired
	private AesServices aesServices;
	
	private static final Logger LOGGER = Logger.getLogger(BuzonController.class);
	
	/**
	 * M�todo envia correo con el mensaje agregado en buz�n de sugerencias y quejas
	 * guarda en buzon
	 * @param buzon objeto que contiene datos del mensaje de sugerencias y quejas 
	 * @param request
	 * @return objeto buzon
	 * @throws Exception
	 */
	@RequestMapping(value = "/guardarEnBuzon.json", method = RequestMethod.POST)
    public Buzon guardarEnBuzon(@RequestBody Buzon buzon, HttpServletRequest request)
	    throws Exception{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario usuario = Utilerias.getUserSession(authentication);
		
			buzon.setEstatus(new Long("0"));
			buzon.setFecha(new Date());
			buzon.setTwUsuario(usuario);
			
			enMailService.sendConCopia(ParametroEnum.CORREO_BUZON.getValue(), null,
					"Mensaje del buz�n de sugerencias y quejas Facturaci�n Basic.".toUpperCase(),
					textoMensajeBuzon(buzon, usuario), null, null);
			
			buzonServices.guardaEnBuzon(buzon);
		
			 Utilerias.eventosAdministrador(Utilerias.CONTRIBUYENTE, "EL CONTRIBUYENTE " + usuario.getRfce()
					    + " ESCRIBIO EN EL BUZON DE SUGERENCIAS Y QUEJAS", usuario, "INSERT", pistasServices, request);
		
		return buzon;
	}
	
	/**
	 * M�todo que modifica el estatus del objeto buzon
	 * @param buzon objeto a modificar
	 * @param request
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value = "/modificarBuzon.json", method = RequestMethod.POST)
    public Buzon modificaBuzon(@RequestBody Buzon buzon, HttpServletRequest request)
	    throws Exception{
		
			buzon.setEstatus(new Long("1"));
			
			buzonServices.guardaEnBuzon(buzon);
			
			
		return buzon;
	}
	
	/**
	 * M�todo crea la estructura del contenido del correo 
	 * @param buzon
	 * @param usuario
	 * @return
	 */
	public static String textoMensajeBuzon(Buzon buzon, TwUsuario usuario ) {
		StringBuilder mensaje = new StringBuilder();

		String tipo ="";
		if(buzon.getTipo()==1){
			tipo="SUGERENCIA";
		}else{
			tipo="QUEJA";
		}
		String nombreUsuario=usuario.getNombre();
		if(usuario.getApPaterno()!=null){
			nombreUsuario=" "+ usuario.getApPaterno();
		}
		if(usuario.getApMaterno()!=null){
			nombreUsuario=" "+ usuario.getApMaterno();
		}
		
		mensaje.append("\t\t\t\t\t\t\t\t\t\t\t\tQUADRUM BASIC\n");
		mensaje.append("\n\n\t El usuario con RFC " + usuario.getRfce()+ "  y nombre " + nombreUsuario + " a escrito en el buz�n de sugerencias y quejas una " + tipo+".");
		
		mensaje.append("\n\n\t\t Asunto:      " + buzon.getAsunto());
		mensaje.append("\n\t\t Mensaje:      " + buzon.getMensaje());
		
		if(buzon.getCorreo()!=null || buzon.getTelefono()!=null){
			mensaje.append("\n\n\t\t Indica que nos comuniquemos por medio del:" );
			if(buzon.getTelefono()!=null){
			mensaje.append("\n\t\t Tel�fono:      " + buzon.getTelefono());
			}
			if(buzon.getCorreo()!=null){
			mensaje.append("\n\t\t Correo:      " + buzon.getCorreo());
			}
		}
		mensaje.append("\n\n\t\t Esperando brindar un mejor servicio, agredecemos se atienda lo antes posible.");
		mensaje.append("\n\n Saludos Cordiales.");
		mensaje.append("\n\n Soporte T�cnico Quadrum-cfdi");
		mensaje.append("\n(01) 5512091825");
		mensaje.append("\nCallCenter: " + ParametroEnum.CORREO_SOPORTE.getValue() + "");
		mensaje.append("\n\n\n" + "Aviso de Privacidad\n");
		mensaje.append("\n");
		mensaje.append("\n");
		mensaje.append("\n");
		mensaje.append("Quadrum como Proveedor Autorizado Certificado por el SAT con domicilio ubicado en:");
		mensaje.append("Montecito n�mero 38 Piso 25 Despacho 22 Colonia N�poles Delegaci�n Benito Ju�rez");
		mensaje.append(" M�xico Distrito Federal C.P 03810 utilizar� sus datos personales aqu� recabados ");
		mensaje.append("para proveerle de los servicios en la emisi�n y generaci�n de CFDI�s que usted ha contratado.");
		mensaje.append("Para mayor informaci�n acerca del tratamiento y de los derechos que puede hacer valer, ");
		mensaje.append("usted puede acceder al aviso de privacidad completo a trav�s de "
			+ ParametroEnum.PRIVACIDAD.getValue() + "\n");

		return mensaje.toString();
	    }

	/**
	 * M�todo que busca todos los mensajes del buz�n, usando paginaci�n
	 * @param wrapper
	 * @param request
	 * @return
	 * @throws ExcepcionesFacturacionBasica
	 */
	  @RequestMapping(value = "/bandejaBuzon.json", method = RequestMethod.POST)
	  public Paginacion<BuzonWrapper> obtenerReporte(@RequestBody BusquedasWrapper wrapper,
			    HttpServletRequest request) throws ExcepcionesFacturacionBasica{

		  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			TwUsuario usuario = Utilerias.getUserSession(authentication);
			Paginacion<BuzonWrapper> pagination = new Paginacion<BuzonWrapper>();
			String hql = "";
			String hqlCount = "";
			HashMap<String, Object> parametros = new HashMap<>();
			int inicio = (wrapper.getNumPaguina() - 1) * wrapper.getTamanioPaguina();
			
			
			    hql = "select b.id as id, b.estatus as estatus, b.tipo as tipo, b.asunto as asunto, b.mensaje as mensaje, b.fecha as fecha, b.telefono as telefono, b.correo as correo, u.rfce as rfcUsuario, u.nombre as nombreUsuario, u.ap_pat as usuarioApPaterno, u.ap_mat as usuarioApMaterno, u.id as idUsuario from buzon as b LEFT JOIN tw_usuario as u on u.id=b.tw_usuario_id where b.fecha  BETWEEN '"
		    + wrapper.getValor1() + " 00:00:00' AND '" + wrapper.getValor2() + " 23:59:59' group by b.id";
			    hqlCount = "select count(id) from buzon where b.fecha  BETWEEN '"
		    + wrapper.getValor1() + " 00:00:00' AND '" + wrapper.getValor2() + " 23:59:59'";

			try {

			    pagination.setList(aesServices.desEncriptaListObject(buzonServices.listaReporteBuzon(
				    BuzonWrapper.class, hql, parametros, inicio, wrapper.getTamanioPaguina())));
			    pagination.setTotalResults((buzonServices.contarDatosReporte(hqlCount, parametros).intValue()));
			    
			    Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "ADMINISTRADOR CONSULTO EL BUZON DE SUGERENCIAS Y QUEJAS"
					    , usuario, "SELECT", pistasServices, request);

			} catch (Exception e) {

			    e.printStackTrace();
			}
			return pagination;
		    }
	  
	  
	  @RequestMapping(value = "/obtenerReporteExcel.json", method = RequestMethod.POST)
	  public List<BuzonWrapper> obtenerReporteExcel(@RequestBody BusquedasWrapper wrapper,
			    HttpServletRequest request) throws ExcepcionesFacturacionBasica{

		  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			TwUsuario usuario = Utilerias.getUserSession(authentication);
			List<BuzonWrapper> listaResultados = new ArrayList<BuzonWrapper>();
			String hql = "";
			HashMap<String, Object> parametros = new HashMap<>();
			
			    hql = "select b.id as id, b.estatus as estatus, b.tipo as tipo, b.asunto as asunto, b.mensaje as mensaje, b.fecha as fecha, b.telefono as telefono, b.correo as correo, u.rfce as rfcUsuario, u.nombre as nombreUsuario, u.ap_pat as usuarioApPaterno, u.ap_mat as usuarioApMaterno, u.id as idUsuario from buzon as b LEFT JOIN tw_usuario as u on u.id=b.tw_usuario_id where b.fecha  BETWEEN '"
		    + wrapper.getValor1() + " 00:00:00' AND '" + wrapper.getValor2() + " 23:59:59' group by b.id";
			    
			try {

				listaResultados= (aesServices.desEncriptaListObject(buzonServices.listaReporteBuzonExcel(
				    BuzonWrapper.class, hql, parametros)));
			    
			    Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "ADMINISTRADOR DESCARGO ARCHIVO EXCEL DE BUZON DE SUGERENCIAS Y QUEJAS"
					    , usuario, "SELECT", pistasServices, request);

			} catch (Exception e) {

			    e.printStackTrace();
			}
			return listaResultados;
		    }
	
}
