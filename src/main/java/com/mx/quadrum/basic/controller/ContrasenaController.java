package com.mx.quadrum.basic.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.dao.IUsuarioDao;
import com.mx.quadrum.basic.entity.TwHistorialPasword;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.EService;

import com.mx.quadrum.basic.services.HistorialServices;
import com.mx.quadrum.basic.services.IUsuarioServices;
import com.mx.quadrum.basic.services.ServicioTwUsuario;
import com.mx.quadrum.basic.util.ExcepcionesFacturacionBasica;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.ContraseniaWrapper;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

/**
 * Clase controlador REST contiene metodos para administrar contrase�a
 * 
 * @author
 *
 */
@RestController
@RequestMapping(value = "/contrasenaController")
public class ContrasenaController extends AbstractControllerHandler {

	@Autowired
	private IUsuarioServices usuarioServices;
	
	@Autowired
	private ServicioTwUsuario servicioTwUsuario;

	@Autowired
	private AesServices aesServices;

	@Autowired
	private EService service;

	@Autowired
	private HistorialServices historialServices;

	@Autowired
	private HistorialServices historialContrasenasServices;

	final static Logger LOGGER = Logger.getLogger(LoginController.class);

	/**
	 * M�todo para cambiar contrase�a
	 * 
	 * @param contraseniaWrapper
	 * @throws ExcepcionesFacturacionBasica
	 * @throws Exception
	 */
	@RequestMapping(value = "/cambioContrasenia.json", method = RequestMethod.POST)
	public @ResponseBody void cambioContrasena(@RequestBody ContraseniaWrapper contraseniaWrapper)
			throws ExcepcionesFacturacionBasica, Exception {

		try {
			Pattern pat = Pattern.compile(
					"((?=.*[^a-zA-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%*&(){},/=.?+])(?=.*[A-Z])(?!.*\\s).{8,16})");
			Matcher mat = pat.matcher(contraseniaWrapper.getNuevaContrasenia());
			if (mat.matches()) {
				Date date = new Date();
				String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				TwUsuario usuario = usuarioServices
						.findbyRfcTemporal(aesServices.encriptaCadena(Utilerias.getUserSession(auth).getRfce()));
				List<TwHistorialPasword> listaContrasenas = historialContrasenasServices
						.buscarHistorialContrasena(aesServices.encriptaCadena(usuario.getRfce()));
				List<String> contrasenasAnteriores = new ArrayList<>();

				for (TwHistorialPasword pass : listaContrasenas) {
					contrasenasAnteriores.add(pass.getPassword());

				}

				if (!contraseniaWrapper.getContraseniaAnterior().equals(usuario.getPassword()))
					throw new ExcepcionesFacturacionBasica("La contrase\u00F1a anterior no es valida");

				if (contrasenasAnteriores.size() >= 3)

				{

					if (contrasenasAnteriores.get(contrasenasAnteriores.size() - 1)
							.equals(contraseniaWrapper.getNuevaContrasenia()))
						throw new ExcepcionesFacturacionBasica("La contrase\u00F1a ya se ha registrado anteriormente");
					if (contrasenasAnteriores.get(contrasenasAnteriores.size() - 2)
							.equals(contraseniaWrapper.getNuevaContrasenia()))
						throw new ExcepcionesFacturacionBasica("La contrase\u00F1a ya se ha registrado anteriormente");
					if (contrasenasAnteriores.get(contrasenasAnteriores.size() - 3)
							.equals(contraseniaWrapper.getNuevaContrasenia()))
						throw new ExcepcionesFacturacionBasica("La contrase\u00F1a ya se ha registrado anteriormente");

				}

				if (contrasenasAnteriores.size() == 2) {
					if (contrasenasAnteriores.get(contrasenasAnteriores.size() - 1)
							.equals(contraseniaWrapper.getNuevaContrasenia()))
						throw new ExcepcionesFacturacionBasica("La contrase\u00F1a ya se ha registrado anteriormente");
					if (contrasenasAnteriores.get(contrasenasAnteriores.size() - 2)
							.equals(contraseniaWrapper.getNuevaContrasenia()))
						throw new ExcepcionesFacturacionBasica("La contrase\u00F1a ya se ha registrado anteriormente");

				}

				if (contrasenasAnteriores.size() == 1) {
					if (contrasenasAnteriores.get(contrasenasAnteriores.size() - 1)
							.equals(contraseniaWrapper.getNuevaContrasenia()))
						throw new ExcepcionesFacturacionBasica("La contrase\u00F1a ya se ha registrado anteriormente");

				}

				if ((usuario.getPassword().equals(contraseniaWrapper.getNuevaContrasenia())))
					throw new ExcepcionesFacturacionBasica("La contrase\u00F1a ya se ha registrado anteriormente");

				usuario.setFechacambiopassword(modifiedDate);
				usuario.setPassword(contraseniaWrapper.getNuevaContrasenia());

				usuario.setPrimerasession(String.valueOf(contraseniaWrapper.getPrimeraSession()));
				usuarioServices.saveorUpdate(usuario);
				TwHistorialPasword historialContrasenas = new TwHistorialPasword();
				historialContrasenas.setPassword(contraseniaWrapper.getNuevaContrasenia());
				historialContrasenas.setTwUsuario(usuario);
				historialContrasenas.setFechaCreacion(new Date());
				historialContrasenasServices.guardaHistorial(historialContrasenas);
			} else {
				throw new ExcepcionesFacturacionBasica("La contrase\u00F1a no cumple con la estructura correcta");
			}
			throw new ExcepcionesFacturacionBasica("exito");
		} catch (ExcepcionesFacturacionBasica e) {
			throw new ExcepcionesFacturacionBasica(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(String.format("Error cambioContrasenia %s",UtileriasCfdi.imprimeStackError(e)));
			throw new Exception(e.getMessage());
		}

	}

	/**
	 * M�todo que obtiene el usuario en sesi�n
	 * 
	 * @return objeto TwUsuario
	 */
	@RequestMapping(value = "/obtenerUsuario", method = RequestMethod.POST)
	public TwUsuario getcontrasena() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return Utilerias.getUserSession(auth);
	}

	/**
	 * M�todo modifica los intentos por iniciar sesi�n
	 * 
	 * @throws Exception
	 */
	@RequestMapping(value = "/ultimoLogin.json", method = RequestMethod.POST)
	public void ultimaConexion() throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario usuario = usuarioServices
				.findbyRfcTemporal(aesServices.encriptaCadena(Utilerias.getUserSession(auth).getRfce()));
		servicioTwUsuario.actualizarFechaIntenos(aesServices.encriptaCadena(usuario.getRfce()));

	}

	/**
	 * M�todo que modifiac estatus de usuario
	 * 
	 * @throws Exception
	 */
	@RequestMapping(value = "/estatusInactivo.json", method = RequestMethod.POST)
	public void estatusInactivo() throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario usuario = usuarioServices
				.findbyRfcTemporal(aesServices.encriptaCadena(Utilerias.getUserSession(auth).getRfce()));
		usuarioServices.estatusCuenta(aesServices.encriptaCadena(usuario.getRfce()));

	}

}
