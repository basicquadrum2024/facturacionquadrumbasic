package com.mx.quadrum.basic.controller;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.ssl.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;

import com.mx.quadrum.basic.entity.Foliador;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TrGeneral;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.CfdiServices;
import com.mx.quadrum.basic.services.EnvioCorreoXmlServices;
import com.mx.quadrum.basic.services.FoliadorService;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.TablaGeneralServices;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.ResultadoTimbradoWrapper;
import com.mx.quadrum.basic.wrapper.Retencion;
import com.mx.quadrum.basic.wrapper.Traslado;
import com.mx.quadrum.basic.wrapper.WrapperPagosV20;
import com.mx.quadrum.cfdi.catalogos.catcfdi.CMoneda;
import com.mx.quadrum.cfdi.timbrado.AcuseTimbradoCfdi;
import com.mx.quadrum.cfdi.timbrado.ErroresAcuseTimbrado;
import com.mx.quadrum.cfdi.timbrado.TimbradoCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.utileria2021.cfdi.CFDIv40;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante.Complemento;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante.Conceptos;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante.Conceptos.Concepto;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante.Emisor;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante.Receptor;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.DoctoRelacionado;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.DoctoRelacionado.ImpuestosDR;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.DoctoRelacionado.ImpuestosDR.RetencionesDR;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.DoctoRelacionado.ImpuestosDR.RetencionesDR.RetencionDR;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.DoctoRelacionado.ImpuestosDR.TrasladosDR;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.DoctoRelacionado.ImpuestosDR.TrasladosDR.TrasladoDR;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.ImpuestosP.RetencionesP;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.ImpuestosP.RetencionesP.RetencionP;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.ImpuestosP.TrasladosP;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.ImpuestosP.TrasladosP.TrasladoP;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Totales;

@RestController
@RequestMapping(value = "/pagosControllerV20")
public class PagosV20Controller implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(PagosV20Controller.class);

	@Autowired
	private FoliadorService foliadorService;

	@Autowired
	private PistasServices pistasServices;

	@Autowired
	private CfdiServices cfdiServices;

	@Autowired
	private TablaGeneralServices tablaGeneralServices;
	
	@Autowired
	private EnvioCorreoXmlServices envioCorreoXmlServices;

	@RequestMapping(value = "/generaComprobantePago.json", method = RequestMethod.POST)
	public ResultadoTimbradoWrapper generaComplementoPago(@RequestBody WrapperPagosV20 wrapperPagos,
			HttpServletRequest request) throws Exception {

		LOGGER.error("id uiddd padre" + wrapperPagos.getCfdiPadre().getId());

		ResultadoTimbradoWrapper timbradoWrapper = new ResultadoTimbradoWrapper();
		Comprobante comprobante = crearComprobante();
		Conceptos conceptos = crearConceptos();
		comprobante.setConceptos(conceptos);

		crearDocumentoRelacionado(wrapperPagos);
		LOGGER.error("crearDocumentoRelacionado" + wrapperPagos.getPago().getDoctoRelacionado().size());
		TcContribuyente tcContribuyente = wrapperPagos.getContribuyente();
		comprobante.setLugarExpedicion(tcContribuyente.getLugarExpedicion());

		Emisor emisor = crearEmisor(tcContribuyente);
		comprobante.setEmisor(emisor);

		byte[] bytesCertificado = Base64.decodeBase64(tcContribuyente.getLlaveCer());
		byte[] bytesLLave = Base64.decodeBase64(tcContribuyente.getLlaveKey());

		TcReceptor tcReceptor = wrapperPagos.getReceptor();

		Receptor comprobanteReceptor = crearReceptor(tcReceptor);
		comprobante.setReceptor(comprobanteReceptor);

		Complemento complemento = agregarComplementoPago(wrapperPagos.getPago(), wrapperPagos.getTraslados(),
				wrapperPagos.getRetenciones(),wrapperPagos.getObjetoImpDR());
		comprobante.setComplemento(complemento);

		CFDIv40 cfdiv4 = new CFDIv40(comprobante, bytesCertificado, bytesLLave, tcContribuyente.getClave(), 0, 6);

		try {
			String xml = cfdiv4.sellar();
			LOGGER.error(xml);
			AcuseTimbradoCfdi acuseTimbradoCfdi = TimbradoCfdi.timbrarXml(xml,
					ParametroEnum.DIRECCIONWSTIMBRADO.getValue(), ParametroEnum.USUARIOWS.getValue(),
					ParametroEnum.PASSWORDWS.getValue(), true);
			if (!acuseTimbradoCfdi.getError()) {

				Foliador foliador = null;
				// Serializable folioId =
				// foliadorService.saveFoliador("Factura");
				// foliador = foliadorService.findFoliadorById(folioId);

				// obtenemos foliador del contribuyente
				foliador = foliadorService.findFoliadorByContribuyente(tcContribuyente.getId());
				if (null == foliador) {
					foliador = new Foliador(tcContribuyente, "Factura", 0L);
					foliadorService.guardarFoliador(foliador);
				}
				// simulamos actualizacion
				foliadorService.guardarFoliador(foliador);
				// obtenemos foliador del contribuyente con nuevo folio
				foliador = foliadorService.findFoliadorByContribuyente(tcContribuyente.getId());

				// String direccionXml = null;
				// direccionXml =
				// UtileriasCfdi.crearDireccionGuardadoXml(ParametroEnum.DIRECCION_GUARDADO_XML.getValue(),
				// comprobante.getFecha(), comprobante.getEmisor().getRfc(),
				// acuseTimbradoCfdi.getUuid());
				// UtileriasCfdi.guardarArchivoXml(direccionXml,
				// cfdIvConvertidor.convertirComprobanteEnString(null));

				String fecha = generarRutaFecha();
				String uuid = acuseTimbradoCfdi.getUuid();
				String direccionXml = String.format("%s%s/%s/%s.xml", ParametroEnum.DIRECCION_GUARDADO_XML.getValue(),
						comprobante.getEmisor().getRfc(), fecha, uuid);
				LOGGER.error("DIRECCION");
				LOGGER.error(direccionXml);
				guardarXmlTimbrado(direccionXml, acuseTimbradoCfdi.getXml());

				timbradoWrapper.setFolio(String.valueOf(foliador.getFolio()));
				timbradoWrapper.setSerie(foliador.getSerie());
				timbradoWrapper.setUuid(acuseTimbradoCfdi.getUuid());
				timbradoWrapper.setLugarFile(direccionXml);
				timbradoWrapper.setErrores("Cfdi creado con �xito");
				timbradoWrapper.setTimbrado(Boolean.TRUE);
				timbradoWrapper.setError(1);

				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				TwUsuario twUsuario = Utilerias.getUserSession(auth);
//
//			Utilerias.eventosAdministrador("CFDI", "CFDI GENERADO CORRECTAMENTE.", twUsuario, "INSERT",
//				pistasServices, request);
//
				guardarInformacionBaseDatos(comprobante, wrapperPagos, direccionXml, timbradoWrapper, foliador,
						twUsuario, request);
				try {
					TwCfdi twCfdi = cfdiServices.buscaTwCfdi(uuid);
					envioCorreoXmlServices.enviarXmlPorCorreo(twCfdi, direccionXml,uuid, twUsuario.getCorreo());
				} catch (Exception e) {
					LOGGER.error(String.format("Error envio de correo cuando se timbra el xml %s", UtileriasCfdi.imprimeStackError(e)));
				}

			} else {
				timbradoWrapper.setTimbrado(false);
				timbradoWrapper.setUuid(
						"No se ha podido timbrar la factura intente m\u00e1s tarde, o por favor, contacte alg\u00fan agente del centro de atenci\u00f3n al cliente");
				// timbradoWrapper.setErrores(acuseTimbradoCfdi.getErrores());
				StringBuilder builder = new StringBuilder();
				for (ErroresAcuseTimbrado erroresAcuseTimbrado : acuseTimbradoCfdi.getErrores()) {
					builder.append(String.format("C\u00F3digo de Incidencia: %s, Mensaje de Incidencia: %s \n\r",
							erroresAcuseTimbrado.getCodigoError(), erroresAcuseTimbrado.getMensaje()));

				}
				timbradoWrapper.setErrores(builder.toString());
			}
		} catch (Exception e) {
			LOGGER.error(e.getCause());
			LOGGER.error(e.getLocalizedMessage());
			LOGGER.error(UtileriasCfdi.imprimeStackError(e.getStackTrace()));
			e.printStackTrace();
			timbradoWrapper.setTimbrado(false);
			timbradoWrapper.setUuid("Error en la creaci\u00f3n del CFDI");
			timbradoWrapper.setErrores(
					"Algunos elementos que usted ha ingresado no se encuentran dentro de los cat\u00e1logos del SAT, verifique sus datos, o por favor, contacte alg\u00fan agente del centro de atenci\u00f3n al cliente");
		}

		return timbradoWrapper;

	}

	private Comprobante crearComprobante() {
		Comprobante comprobante = new Comprobante();
		comprobante.setVersion("4.0");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String strDate = dateFormat.format(new Date());
		comprobante.setFecha(strDate.replace(" ", "T"));
		comprobante.setSubTotal(BigDecimal.ZERO);
		comprobante.setMoneda("XXX");
		comprobante.setTotal(BigDecimal.ZERO);
		comprobante.setTipoDeComprobante("P");
		comprobante.setExportacion("01");
		return comprobante;
	}

	private Conceptos crearConceptos() {
		Conceptos conceptos = new Conceptos();
		Concepto concepto = new Concepto();
		concepto.setClaveProdServ("84111506");
		concepto.setCantidad(BigDecimal.ONE);
		concepto.setClaveUnidad("ACT");
		concepto.setDescripcion("Pago");
		concepto.setValorUnitario(BigDecimal.ZERO);
		concepto.setImporte(BigDecimal.ZERO);
		concepto.setObjetoImp("01");
		conceptos.getConcepto().add(concepto);
		return conceptos;
	}

	private Emisor crearEmisor(TcContribuyente tcContribuyente) {
		Emisor emisor = new Emisor();
		emisor.setRfc(tcContribuyente.getRfc());
		emisor.setNombre(tcContribuyente.getNombre());
		emisor.setRegimenFiscal(tcContribuyente.getRegimenFiscal());

		return emisor;
	}

	private Receptor crearReceptor(TcReceptor receptor) {
		Receptor comprobanteReceptor = new Receptor();
		comprobanteReceptor.setRfc(receptor.getRfc());
		comprobanteReceptor.setNombre(receptor.getNombre());
		comprobanteReceptor.setDomicilioFiscalReceptor(receptor.getDomicilioFiscalReceptor());
		comprobanteReceptor.setRegimenFiscalReceptor(receptor.getRegimenFiscalReceptor());
		comprobanteReceptor.setUsoCFDI("CP01");

		return comprobanteReceptor;
	}

	private Complemento agregarComplementoPago(Pago pago, List<Traslado> traslados, List<Retencion> retenciones,String objetoImpDR) {
		Pagos pagos = new Pagos();
		pagos.setVersion("2.0");
		pago.setTipoCambioP(BigDecimal.ONE);

		BigDecimal totalImpuestosTrasladadosP = BigDecimal.ZERO;
		BigDecimal totalImpuestosRetenidosP = BigDecimal.ZERO;
		Totales totales = new Totales();
		ImpuestosDR impuestosDR = new ImpuestosDR();
		Map<String, TrasladoP> mapTrasladoP = new HashMap<String, Pagos.Pago.ImpuestosP.TrasladosP.TrasladoP>();
		Map<String, RetencionP> mapRetencionP = new HashMap<String, Pagos.Pago.ImpuestosP.RetencionesP.RetencionP>();
		Pagos.Pago.ImpuestosP impuestosP = null;

		if (retenciones.size() > 0) {
			for (Retencion retencion : retenciones) {
				RetencionDR retencionDR = new RetencionDR();
				retencionDR.setBaseDR(pago.getMonto());
				retencionDR.setTipoFactorDR(retencion.getTipoFactor());
				retencionDR.setTasaOCuotaDR(new BigDecimal(retencion.getTasaOCuota()));
				BigDecimal iva = new BigDecimal("1.16");
				if (retencionDR.getTasaOCuotaDR().compareTo(new BigDecimal("0.080000")) == 0)
					iva = new BigDecimal("0.08");
				retencionDR.setBaseDR(pago.getMonto().divide(iva, RoundingMode.HALF_UP).setScale(2));
				totalImpuestosRetenidosP = totalImpuestosRetenidosP
						.add(retencionDR.getBaseDR().multiply(retencionDR.getTasaOCuotaDR()));
				retencionDR.setImporteDR(totalImpuestosRetenidosP.setScale(2, RoundingMode.HALF_UP));
				retencionDR.setImpuestoDR(retencion.getImpuesto());
				RetencionesDR retencionesDR = new RetencionesDR();
				retencionesDR.getRetencionDR().add(retencionDR);

				impuestosDR.setRetencionesDR(retencionesDR);
				String llaveRetencionIva = retencion.getTipoFactor();
				if (mapRetencionP.containsKey(llaveRetencionIva)) {
					BigDecimal importe = mapRetencionP.get(llaveRetencionIva).getImporteP();
					importe = importe.add(retencionDR.getImporteDR());
					mapRetencionP.get(llaveRetencionIva).setImporteP(importe);
				} else {
					Pagos.Pago.ImpuestosP.RetencionesP.RetencionP retencionP = new Pagos.Pago.ImpuestosP.RetencionesP.RetencionP();

					retencionP.setImporteP(retencionDR.getImporteDR());
					retencionP.setImpuestoP(retencionDR.getImpuestoDR());

					mapRetencionP.put(llaveRetencionIva, retencionP);

				}

			}
			RetencionesP retencionesP = new RetencionesP();
			for (Map.Entry<String, Pagos.Pago.ImpuestosP.RetencionesP.RetencionP> mapaIvasRetenciones : mapRetencionP
					.entrySet()) {

				if (mapaIvasRetenciones.getValue().getImpuestoP().equals("002")) {
					totales.setTotalRetencionesIVA(mapaIvasRetenciones.getValue().getImporteP());
				}
				if (mapaIvasRetenciones.getValue().getImpuestoP().equals("001")) {
					totales.setTotalRetencionesISR(mapaIvasRetenciones.getValue().getImporteP());
				}
				if (mapaIvasRetenciones.getValue().getImpuestoP().equals("003")) {
					totales.setTotalRetencionesIEPS(mapaIvasRetenciones.getValue().getImporteP());
				}

				retencionesP.getRetencionP().add(mapaIvasRetenciones.getValue());
			}
			if (retencionesP != null) {
				if(impuestosP==null)
					impuestosP=new Pagos.Pago.ImpuestosP();
				impuestosP.setRetencionesP(retencionesP);
			}
		}
		TrasladosP trasladosP = null;
		if (traslados != null && !traslados.isEmpty()) {
			trasladosP = new TrasladosP();
			for (Traslado traslado : traslados) {
				// if (!traslado.getTipoFactor().equals("Exento")) {
				TrasladoDR trasladoDR = new TrasladoDR();
				trasladoDR.setBaseDR(pago.getMonto());
				trasladoDR.setTipoFactorDR(traslado.getTipoFactor());
				String tasaCuota = traslado.getTasaOCuota();
				if (!StringUtils.isBlank(tasaCuota)) {
					trasladoDR.setTasaOCuotaDR(new BigDecimal(traslado.getTasaOCuota()));
					BigDecimal iva = new BigDecimal("1.16");
					if (trasladoDR.getTasaOCuotaDR().compareTo(new BigDecimal("0.080000")) == 0)
						iva = new BigDecimal("0.08");
					trasladoDR.setBaseDR(pago.getMonto().divide(iva, RoundingMode.HALF_UP).setScale(2));
					totalImpuestosTrasladadosP = totalImpuestosTrasladadosP
							.add(trasladoDR.getBaseDR().multiply(trasladoDR.getTasaOCuotaDR()));

					trasladoDR.setImporteDR(totalImpuestosTrasladadosP.setScale(2, RoundingMode.HALF_UP));
				}

				trasladoDR.setImpuestoDR(traslado.getImpuesto());

				TrasladosDR trasladosDR = new TrasladosDR();
				trasladosDR.getTrasladoDR().add(trasladoDR);

				impuestosDR.setTrasladosDR(trasladosDR);
				pago.getDoctoRelacionado().get(0).setImpuestosDR(impuestosDR);

				if (StringUtils.isBlank(tasaCuota)) {
					pago.getDoctoRelacionado().get(0).setImpuestosDR(null);
					pago.getDoctoRelacionado().get(0).setObjetoImpDR(objetoImpDR);
				}
				String llaveRetencionIva = traslado.getTipoFactor();

				if (mapTrasladoP.containsKey(llaveRetencionIva)) {
					BigDecimal importe = mapTrasladoP.get(llaveRetencionIva).getImporteP();
					importe = importe.add(trasladoDR.getImporteDR());
					mapTrasladoP.get(llaveRetencionIva).setImporteP(importe);
				} else {
					Pagos.Pago.ImpuestosP.TrasladosP.TrasladoP trasladoP = new Pagos.Pago.ImpuestosP.TrasladosP.TrasladoP();
					trasladoP.setBaseP(trasladoDR.getBaseDR());
					trasladoP.setImporteP(trasladoDR.getImporteDR());
					trasladoP.setImpuestoP(trasladoDR.getImpuestoDR());
					trasladoP.setTasaOCuotaP(trasladoDR.getTasaOCuotaDR());
					trasladoP.setTipoFactorP(trasladoDR.getTipoFactorDR());
					if (StringUtils.isBlank(tasaCuota)) {
//						trasladoP.setImporteP(BigDecimal.ZERO);
//						trasladoP.setTasaOCuotaP(BigDecimal.ZERO);
					}
					mapTrasladoP.put(llaveRetencionIva, trasladoP);
				}

			}
		}

		for (Map.Entry<String, Pagos.Pago.ImpuestosP.TrasladosP.TrasladoP> mapaIvasTraslado : mapTrasladoP.entrySet()) {
			if (mapaIvasTraslado.getValue().getTasaOCuotaP() != null) {
				if (mapaIvasTraslado.getValue().getTasaOCuotaP().compareTo(new BigDecimal("0.160000")) == 0) {
					totales.setTotalTrasladosBaseIVA16(mapaIvasTraslado.getValue().getBaseP());
					totales.setTotalTrasladosImpuestoIVA16(mapaIvasTraslado.getValue().getImporteP());
				}
				if (mapaIvasTraslado.getValue().getTasaOCuotaP().compareTo(new BigDecimal("0.080000")) == 0) {
					totales.setTotalTrasladosBaseIVA8(mapaIvasTraslado.getValue().getBaseP());
					totales.setTotalTrasladosImpuestoIVA8(mapaIvasTraslado.getValue().getImporteP());
				}
				if (mapaIvasTraslado.getValue().getTasaOCuotaP().compareTo(new BigDecimal("0.000000")) == 0) {
					totales.setTotalTrasladosBaseIVA0(mapaIvasTraslado.getValue().getBaseP());
					totales.setTotalTrasladosImpuestoIVA0(mapaIvasTraslado.getValue().getImporteP());
				}
			} else {
				totales.setTotalTrasladosBaseIVAExento(mapaIvasTraslado.getValue().getBaseP());

			}

			trasladosP.getTrasladoP().add(mapaIvasTraslado.getValue());
		}

		//// instancia una sola vez
		if (trasladosP != null) {
			if(impuestosP==null)
				impuestosP=new Pagos.Pago.ImpuestosP();
			impuestosP.setTrasladosP(trasladosP);
		}

		if (impuestosP != null)
			pago.setImpuestosP(impuestosP);

		totales.setMontoTotalPagos(pago.getMonto());

		pagos.setTotales(totales);

		pagos.getPago().add(pago);
		Complemento complemento = new Complemento();
		complemento.getAny().add(pagos);

		return complemento;
	}

	private void crearDocumentoRelacionado(WrapperPagosV20 wrapperPagos) {
		DoctoRelacionado doctoRelacionado = new DoctoRelacionado();
		wrapperPagos.getPago().getDoctoRelacionado();
		doctoRelacionado.setIdDocumento(wrapperPagos.getCfdiPadre().getUuid());
		if (wrapperPagos.getCfdiPadre().getFolio() != null) {
			doctoRelacionado.setFolio(wrapperPagos.getCfdiPadre().getFolio().toString());
		}
		doctoRelacionado.setSerie(wrapperPagos.getCfdiPadre().getSerie());

		doctoRelacionado
				.setNumParcialidad(BigInteger.valueOf(wrapperPagos.getCfdiPadre().getTotalParcialidades() + 1L));
		doctoRelacionado.setMonedaDR(CMoneda.valueOf(wrapperPagos.getCfdiPadre().getMonedaDr()).toString());

		// redondeo a decimales
		BigDecimal total = wrapperPagos.getCfdiPadre().getTotal();
		BigDecimal totalPagado = wrapperPagos.getCfdiPadre().getTotalPagado();
		BigDecimal monto = wrapperPagos.getPago().getMonto();

		BigDecimal saldoAnterior = total.subtract(totalPagado);

		BigDecimal importePagado = BigDecimal.ZERO;
		// if (doctoRelacionado.getTipoCambioDR() != null) {
		// // importePagado =
		// // monto.multiply(doctoRelacionado.getTipoCambioDR());
		// } else {
		importePagado = monto;
		// }

		BigDecimal saldoInsoluto = saldoAnterior.subtract(importePagado);

		int decimales = wrapperPagos.getDecimales();
		doctoRelacionado.setImpSaldoAnt(saldoAnterior.setScale(decimales, BigDecimal.ROUND_HALF_EVEN));
		doctoRelacionado.setImpPagado(importePagado.setScale(decimales, BigDecimal.ROUND_HALF_EVEN));
		doctoRelacionado.setImpSaldoInsoluto(saldoInsoluto.setScale(decimales, BigDecimal.ROUND_HALF_EVEN));
		doctoRelacionado.setObjetoImpDR(wrapperPagos.getObjetoImpDR());
		doctoRelacionado.setEquivalenciaDR(BigDecimal.ONE);
		wrapperPagos.getPago().getDoctoRelacionado().add(doctoRelacionado);
		LOGGER.error(wrapperPagos.getPago().getDoctoRelacionado().size());
	}

	private String generarRutaFecha() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MMMM/dd/HH", new Locale("es", "MX"));
		return simpleDateFormat.format(new Date());
	}

	private void guardarXmlTimbrado(final String pathArchivo, final String contenido) {
		try {
			Utilerias.guardarArchivo(pathArchivo, contenido, StandardCharsets.UTF_8);
		} catch (Exception e) {
			LOGGER.error(String.format("Error al guardar el archivo xml %s , %s , excepction: %s", pathArchivo,
					contenido, UtileriasCfdi.imprimeStackError(e)));
		}
	}

	private void guardarInformacionBaseDatos(final Comprobante comprobante, WrapperPagosV20 wrapperPagos,
			final String direccionXml, ResultadoTimbradoWrapper timbradoWrapper, final Foliador foliador,
			final TwUsuario twUsuario, HttpServletRequest request) throws Exception {

		try {

			// if (wrapperPagos.getFaltante()) {
			// wrapperPagos.getCfdiPadre().setEstatus(2L);
			// }

			Serializable idTwCfdi = construyeNuevoCfdi(comprobante, direccionXml, foliador,
					wrapperPagos.getCfdiPadre());
			timbradoWrapper.setId((Long) idTwCfdi);

			TrGeneral trGeneral = new TrGeneral(wrapperPagos.getContribuyente(), wrapperPagos.getReceptor(),
					new TwCfdi((Long) idTwCfdi), twUsuario);
			tablaGeneralServices.saveOrUpdateGeneral(trGeneral);
			Utilerias.eventosAdministrador("CFDI", "CFDI PAGOS GUARDADO EN BASE DE DATOS", twUsuario, "INSERT",
					pistasServices, request);
			
		} catch (Exception e) {
			Utilerias.eventosAdministrador("CFDI", "CFDI PAGOS GUARDADO EN BASE DE DATOS", twUsuario, "ERROR",
					pistasServices, request);
			LOGGER.error(e);
			LOGGER.error(e.getCause());
			LOGGER.error(e.getMessage());
			LOGGER.error("Error en metodo guardarInformacionBaseDatos -->"
					+ UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
	}

	private Serializable construyeNuevoCfdi(final Comprobante comprobante, final String direccionXml,
			final Foliador foliador, TwCfdi cfdiPadre) {

		Serializable serializable = null;
		try {
			XPath xPath = XPathFactory.newInstance().newXPath();
			Document xmlsDocument = parsearXML(Files.readAllBytes(Paths.get(direccionXml)));

			String uuid = (String) xPath.compile("/Comprobante/Complemento/TimbreFiscalDigital/@UUID")
					.evaluate(xmlsDocument, XPathConstants.STRING);
			String fechaTimbrado = (String) xPath.compile("/Comprobante/Complemento/TimbreFiscalDigital/@FechaTimbrado")
					.evaluate(xmlsDocument, XPathConstants.STRING);

			String monedaP = (String) xPath.compile("/Comprobante/Complemento/Pagos/Pago/@MonedaP")
					.evaluate(xmlsDocument, XPathConstants.STRING);
			String monto = (String) xPath.compile("/Comprobante/Complemento/Pagos/Pago/@Monto").evaluate(xmlsDocument,
					XPathConstants.STRING);

			TwCfdi cfdi = new TwCfdi();

			cfdi.setUuid(uuid);

			cfdi.setDireccionXml(direccionXml);

			cfdi.setEstatus(1L);

			cfdi.setFechaCreacion(UtileriasCfdi.YYYYMMDDTHHMMSS.parse(fechaTimbrado));

			cfdi.setFechaCertificacion(new Date());
			cfdi.setFolio(String.valueOf((foliador.getFolio())));
			cfdi.setSerie(foliador.getSerie());

			cfdi.setClaveConfirmacion(comprobante.getConfirmacion());
			cfdi.setMetodoPago(comprobante.getMetodoPago());

			cfdi.setMonedaDr(monedaP);

			cfdi.setTotal(new BigDecimal(monto));

			cfdi.setTwCfdi(cfdiPadre);

			serializable = cfdiServices.guardarCfdi(cfdi);
		} catch (Exception e) {
			LOGGER.error("Error en metodo construyeNuevoCfdi -->" + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
		return serializable;
	}

	public Document parsearXML(final byte[] xmls) throws Exception {
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		return builder.parse(new ByteArrayInputStream(xmls));
	}

}
