package com.mx.quadrum.basic.controller;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.adenda.Adenda33;
import com.mx.quadrum.basic.adenda.DomicilioEmisor;
import com.mx.quadrum.basic.adenda.DomicilioReceptor;
import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.Foliador;
import com.mx.quadrum.basic.entity.Paginacion;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TrGeneral;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.CfdiServices;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.FoliadorService;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.TablaGeneralServices;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.ResultadoTimbradoWrapper;
import com.mx.quadrum.basic.wrapper.WrapperPagos;
import com.mx.quadrum.cfdi.CFDIv33;
import com.mx.quadrum.cfdi.catalogos.catcfdi.CMetodoPago;
import com.mx.quadrum.cfdi.catalogos.catcfdi.CMoneda;
import com.mx.quadrum.cfdi.complementos.pagos.v10.schema.Pagos;
import com.mx.quadrum.cfdi.complementos.pagos.v10.schema.Pagos.Pago;
import com.mx.quadrum.cfdi.complementos.pagos.v10.schema.Pagos.Pago.DoctoRelacionado;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v11.schema.TimbreFiscalDigital;
import com.mx.quadrum.cfdi.timbrado.AcuseTimbradoCfdi;
import com.mx.quadrum.cfdi.timbrado.ErroresAcuseTimbrado;
import com.mx.quadrum.cfdi.timbrado.TimbradoCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasGenerales;
import com.mx.quadrum.cfdi.v33.schema.Comprobante;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.CfdiRelacionados;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.CfdiRelacionados.CfdiRelacionado;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Complemento;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Conceptos;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Conceptos.Concepto;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Emisor;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Receptor;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;

/**
 * Clase @RestController contiene metodos para adminsitrar complemento de pagos
 * 
 * @author
 *
 */
@RestController
@RequestMapping(value = "/pagosController")
public class PagosController extends AbstractControllerHandler implements Serializable {

	public static final Logger LOGGER = Logger.getLogger(PagosController.class);

	@Autowired
	private CfdiServices cfdiServices;

	@Autowired
	private TablaGeneralServices tablaGeneralServices;

	@Autowired
	private EmisorServices emisorServices;

	@Autowired
	private AesServices aesServices;

	@Autowired
	private FoliadorService foliadorService;

	@Autowired
	private PistasServices pistasServices;

	/**
	 * MEtodo que enlista los complementos Padre
	 * 
	 * @param currentPage
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listarComprobantesPagosPadre/{currentPage}/{pageSize}/.json", method = RequestMethod.POST)
	public Paginacion<TrGeneral> listarComprobantesPagosPadre(@PathVariable("currentPage") Integer currentPage,
			@PathVariable("pageSize") Integer pageSize) throws Exception {
		Paginacion<TrGeneral> pagination = new Paginacion<TrGeneral>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		try {
			int start = (currentPage - 1) * pageSize;
			TcContribuyente tcContribuyente = emisorServices
					.buscarPorRfcEmisor(aesServices.encriptaCadena(twUsuario.getRfce()));
			pagination.setTotalResults(tablaGeneralServices.findTrGeneralPaginationCount(tcContribuyente.getId(),
					aesServices.encriptaCadena("PPD")));
			pagination.setList(tablaGeneralServices.findTrGeneralPadre(start, pageSize, tcContribuyente.getId(),
					aesServices.encriptaCadena("PPD")));

			for (TrGeneral trGeneral : pagination.getList()) {

				BigDecimal totalPagado = cfdiServices.calculaTotalPagado(trGeneral.getTwCfdi().getId());
				Long totalParcialidades = cfdiServices.calculaTotalCfdiHijos(trGeneral.getTwCfdi().getId());
				if (totalPagado == null) {
					totalPagado = BigDecimal.ZERO;
				}
				trGeneral.getTwCfdi().setRestante(trGeneral.getTwCfdi().getTotal().subtract(totalPagado));
				trGeneral.getTwCfdi().setTotalPagado(totalPagado);
				trGeneral.getTwCfdi().setTotalParcialidades(totalParcialidades);
			}
		} catch (Exception e) {
			LOGGER.error("Error@" + e.getLocalizedMessage());
			LOGGER.error("Error@" + e.getMessage());
			LOGGER.error("Error@" + e.getCause());
			e.printStackTrace();
		}

		return pagination;
	}

	/**
	 * MEtodo que crea un obj Pago para ser llenado en formulario
	 * 
	 * @return Pago Entidad Pago
	 * @throws Exception
	 */
	@RequestMapping(value = "/crearComplementoPago.json", method = RequestMethod.POST)
	public Pago crearComplementoPago() throws Exception {
		Pago pago = new Pago();
		Pago.DoctoRelacionado doc = new Pago.DoctoRelacionado();
		pago.getDoctoRelacionado().add(doc);
		return pago;
	}

	@RequestMapping(value = "/buscarVersion/{idCfdiPadre}/.json", method = RequestMethod.POST)
	public String obtenerVersion(@PathVariable("idCfdiPadre") Long idCfdiPadre) throws Exception {
		String version = "4.0";
//		TwCfdi cfdi = cfdiServices.buscaTwCfdiId(idCfdiPadre);
//		try {
//			File file = new File(cfdi.getDireccionXml());
//			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//			DocumentBuilder dBuilder;
//			dBuilder = dbFactory.newDocumentBuilder();
//			Document doc = dBuilder.parse(file);
//			doc.getDocumentElement().normalize();
//			XPath xPath = XPathFactory.newInstance().newXPath();
//			String expression = "/Comprobante";
//			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
//			for (int i = 0; i < nodeList.getLength(); i++) {
//				Node nNode = nodeList.item(i);
//
//				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//					Element eElement = (Element) nNode;
//					version = eElement.getAttribute("Version");
//
//				}
//			}
//		} catch (ParserConfigurationException e) {
//			System.out.println(e);
//		} catch (SAXException e) {
//			System.out.println(e);
//		} catch (IOException e) {
//			System.out.println(e);
//		} catch (XPathExpressionException e) {
//			System.out.println(e);
//		}
		return version;
	}

	@RequestMapping(value = "/crearComplementoPagoV20.json", method = RequestMethod.POST)
	public com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago crearComplementoPagoV20()
			throws Exception {
		com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago pago = new com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago();
		com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.DoctoRelacionado doc = new com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago.DoctoRelacionado();
		pago.getDoctoRelacionado().add(doc);
		return pago;
	}

	/**
	 * MEtodo que crear y timbra el comprobante
	 * 
	 * @param wrapperPagos Pojo WrapperPagos
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/generaComprobantePago.json", method = RequestMethod.POST)
	public ResultadoTimbradoWrapper generaComplementoPago(@RequestBody WrapperPagos wrapperPagos,
			HttpServletRequest request) throws Exception {
		ResultadoTimbradoWrapper timbradoWrapper = new ResultadoTimbradoWrapper();
		Comprobante comprobante = crearComprobante();

		Conceptos conceptos = crearConceptos();
		comprobante.setConceptos(conceptos);
		if (wrapperPagos.getUuidCfdiRelacionado() != null) {
			CfdiRelacionados cfdiRelacionados = crearCfdiRelacionado(wrapperPagos.getUuidCfdiRelacionado());
			comprobante.setCfdiRelacionados(cfdiRelacionados);
		}

		crearDocumentoRelacionado(wrapperPagos);

		TcContribuyente tcContribuyente = wrapperPagos.getContribuyente();
		comprobante.setLugarExpedicion(tcContribuyente.getLugarExpedicion());

		Emisor emisor = crearEmisor(tcContribuyente);
		comprobante.setEmisor(emisor);

		TcReceptor tcReceptor = wrapperPagos.getReceptor();

		Receptor comprobanteReceptor = crearReceptor(tcReceptor);
		comprobante.setReceptor(comprobanteReceptor);

		Complemento complemento = agregarComplementoPago(wrapperPagos.getPago());
		comprobante.setComplemento(complemento);

		CFDIv33 cfdIv33 = new CFDIv33(comprobante, UtileriasGenerales.decodificarBase64(tcContribuyente.getLlaveCer()),
				UtileriasGenerales.decodificarBase64(tcContribuyente.getLlaveKey()), tcContribuyente.getClave(), 0, 0);
		try {
			String xml = cfdIv33.sellarObtenerComprobanteEnString();
			AcuseTimbradoCfdi acuseTimbradoCfdi = TimbradoCfdi.timbrarXml(xml,
					ParametroEnum.DIRECCIONWSTIMBRADO.getValue(), ParametroEnum.USUARIOWS.getValue(),
					ParametroEnum.PASSWORDWS.getValue(), true);
			if (!acuseTimbradoCfdi.getError()) {

				Foliador foliador = null;
				// Serializable folioId =
				// foliadorService.saveFoliador("Factura");
				// foliador = foliadorService.findFoliadorById(folioId);

				// obtenemos foliador del contribuyente
				foliador = foliadorService.findFoliadorByContribuyente(tcContribuyente.getId());
				if (null == foliador) {
					foliador = new Foliador(tcContribuyente, "Factura", 0L);
					foliadorService.guardarFoliador(foliador);
				}
				// simulamos actualizacion
				foliadorService.guardarFoliador(foliador);
				// obtenemos foliador del contribuyente con nuevo folio
				foliador = foliadorService.findFoliadorByContribuyente(tcContribuyente.getId());

				Class<?> clases[] = { Comprobante.class, Pagos.class, TimbreFiscalDigital.class };

				Comprobante comprobantePagosv33 = UtileriasCfdi.convertirAComprobante(acuseTimbradoCfdi.getXml(),
						clases);
//		Comprobante.Addenda addenda = new Comprobante.Addenda();
//		Adenda33 adenda33 = crearAsignarAddenda(tcReceptor, tcContribuyente, foliador);
//		addenda.getAny().add(adenda33);
//		comprobantePagosv33.setAddenda(addenda);

				CFDIv33 cfdIvConvertidor = new CFDIv33(comprobantePagosv33, 0, 0);
				// cfdIvConvertidor.addSchemaLocation("http://www.sat.gob.mx/Pagos",
				// "http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd");
				String direccionXml = null;
				direccionXml = UtileriasCfdi.crearDireccionGuardadoXml(ParametroEnum.DIRECCION_GUARDADO_XML.getValue(),
						comprobante.getFecha(), comprobante.getEmisor().getRfc(), acuseTimbradoCfdi.getUuid());
				UtileriasCfdi.guardarArchivoXml(direccionXml, cfdIvConvertidor.convertirComprobanteEnString(null));

				timbradoWrapper.setFolio(String.valueOf(foliador.getFolio()));
				timbradoWrapper.setSerie(foliador.getSerie());
				timbradoWrapper.setUuid(acuseTimbradoCfdi.getUuid());
				timbradoWrapper.setLugarFile(direccionXml);
				timbradoWrapper.setErrores("Cfdi creado con �xito");
				timbradoWrapper.setTimbrado(Boolean.TRUE);
				timbradoWrapper.setError(1);

				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				TwUsuario twUsuario = Utilerias.getUserSession(auth);

				Utilerias.eventosAdministrador("CFDI", "CFDI GENERADO CORRECTAMENTE.", twUsuario, "INSERT",
						pistasServices, request);

				guardarInformacionBaseDatos(comprobantePagosv33, wrapperPagos, direccionXml, timbradoWrapper, foliador,
						twUsuario, request);

			} else {
				timbradoWrapper.setTimbrado(false);
				timbradoWrapper.setUuid(
						"No se ha podido timbrar la factura intente m\u00e1s tarde, o por favor, contacte alg\u00fan agente del centro de atenci\u00f3n al cliente");
				StringBuilder builder = new StringBuilder();
				for (ErroresAcuseTimbrado erroresAcuseTimbrado : acuseTimbradoCfdi.getErrores()) {
					builder.append(String.format("C\u00F3digo de Incidencia: %s, Mensaje de Incidencia: %s \n\r",
							erroresAcuseTimbrado.getCodigoError(), erroresAcuseTimbrado.getMensaje()));

				}
				timbradoWrapper.setErrores(builder.toString());
			}
		} catch (Exception e) {
			LOGGER.error(e.getCause());
			LOGGER.error(e.getLocalizedMessage());
			LOGGER.error(UtileriasCfdi.imprimeStackError(e.getStackTrace()));
			e.printStackTrace();
			timbradoWrapper.setTimbrado(false);
			timbradoWrapper.setUuid("Error en la creaci\u00f3n del CFDI");
			timbradoWrapper.setErrores(
					"Algunos elementos que usted ha ingresado no se encuentran dentro de los cat\u00e1logos del SAT, verifique sus datos, o por favor, contacte alg\u00fan agente del centro de atenci\u00f3n al cliente");
		}
		return timbradoWrapper;
	}

	/**
	 * M�todo que agrega documento relacionados al comprobante
	 * 
	 * @param wrapperPagos
	 */
	private void crearDocumentoRelacionado(WrapperPagos wrapperPagos) {
		DoctoRelacionado doctoRelacionado =new DoctoRelacionado();

		doctoRelacionado.setIdDocumento(wrapperPagos.getCfdiPadre().getUuid());
		if (wrapperPagos.getCfdiPadre().getFolio() != null) {
			doctoRelacionado.setFolio(wrapperPagos.getCfdiPadre().getFolio().toString());
		}
		doctoRelacionado.setSerie(wrapperPagos.getCfdiPadre().getSerie());
		doctoRelacionado.setMetodoDePagoDR(CMetodoPago.valueOf(wrapperPagos.getCfdiPadre().getMetodoPago()).toString());
		doctoRelacionado
				.setNumParcialidad(BigInteger.valueOf(wrapperPagos.getCfdiPadre().getTotalParcialidades() + 1L));
		doctoRelacionado.setMonedaDR(CMoneda.valueOf(wrapperPagos.getCfdiPadre().getMonedaDr()).toString());

		// redondeo a decimales
		BigDecimal total = wrapperPagos.getCfdiPadre().getTotal();
		BigDecimal totalPagado = wrapperPagos.getCfdiPadre().getTotalPagado();
		BigDecimal monto = wrapperPagos.getPago().getMonto();

		BigDecimal saldoAnterior = total.subtract(totalPagado);

		BigDecimal importePagado = BigDecimal.ZERO;
		// if (doctoRelacionado.getTipoCambioDR() != null) {
		// // importePagado =
		// // monto.multiply(doctoRelacionado.getTipoCambioDR());
		// } else {
		importePagado = monto;
		// }

		BigDecimal saldoInsoluto = saldoAnterior.subtract(importePagado);

		int decimales = wrapperPagos.getDecimales();
		doctoRelacionado.setImpSaldoAnt(saldoAnterior.setScale(decimales, BigDecimal.ROUND_HALF_EVEN));
		doctoRelacionado.setImpPagado(importePagado.setScale(decimales, BigDecimal.ROUND_HALF_EVEN));
		doctoRelacionado.setImpSaldoInsoluto(saldoInsoluto.setScale(decimales, BigDecimal.ROUND_HALF_EVEN));
		wrapperPagos.getPago().getDoctoRelacionado().add(doctoRelacionado);

	}

	/**
	 * M�todo que devuelve objeto CfdiRelacionados, con un tipo de relaci�n (04)
	 * 
	 * @param uuidCfdiRelacionado
	 * @return
	 * @throws Exception
	 */
	private CfdiRelacionados crearCfdiRelacionado(String uuidCfdiRelacionado) throws Exception {
		CfdiRelacionados cfdiRelacionados = new CfdiRelacionados();
		cfdiRelacionados.setTipoRelacion("04");

		CfdiRelacionado cfdiRelacionado = new CfdiRelacionado();
		cfdiRelacionado.setUUID(uuidCfdiRelacionado);

		cfdiRelacionados.getCfdiRelacionado().add(cfdiRelacionado);

		return cfdiRelacionados;
	}

	/**
	 * M�todo crea objeto comprobante de tipo P (Pago)
	 * 
	 * @return objeto Comprobante
	 * @throws Exception
	 */
	private Comprobante crearComprobante() throws Exception {
		Comprobante comprobante = new Comprobante();
		comprobante.setVersion("3.3");
		comprobante.setFecha(new Date());
		comprobante.setSubTotal(BigDecimal.ZERO);
		comprobante.setMoneda("XXX");
		comprobante.setTotal(BigDecimal.ZERO);
		comprobante.setTipoDeComprobante("P");

		return comprobante;
	}

	/**
	 * M�todo crea objeto Conceptos para el comprobate de pago
	 * 
	 * @return objeto Conceptos
	 * @throws Exception
	 */
	private Conceptos crearConceptos() throws Exception {
		Conceptos conceptos = new Conceptos();
		Concepto concepto = new Concepto();

		concepto.setClaveProdServ("84111506");
		concepto.setCantidad(BigDecimal.ONE);
		concepto.setClaveUnidad("ACT");
		concepto.setDescripcion("Pago");
		concepto.setValorUnitario(BigDecimal.ZERO);
		concepto.setImporte(BigDecimal.ZERO);

		conceptos.getConcepto().add(concepto);
		return conceptos;
	}

	/**
	 * M�todo crea objeto Emisor con datos de TcContribuyente
	 * 
	 * @param tcContribuyente
	 * @return objeto Emisor
	 * @throws Exception
	 */
	private Emisor crearEmisor(TcContribuyente tcContribuyente) throws Exception {
		Emisor emisor = new Emisor();
		emisor.setRfc(tcContribuyente.getRfc());
		emisor.setNombre(tcContribuyente.getNombre());
		emisor.setRegimenFiscal(tcContribuyente.getRegimenFiscal());
		return emisor;
	}

	/**
	 * M�todo que crea objeto Receptor
	 * 
	 * @param receptor
	 * @return objeto Receptor
	 * @throws Exception
	 */
	private Receptor crearReceptor(TcReceptor receptor) throws Exception {

		Receptor comprobanteReceptor = new Receptor();
		comprobanteReceptor.setRfc(receptor.getRfc());
		comprobanteReceptor.setNombre(receptor.getNombre());
		comprobanteReceptor.setResidenciaFiscal(receptor.getResidenciaFiscal());
		comprobanteReceptor.setNumRegIdTrib(receptor.getNumRegIdTrib());
		comprobanteReceptor.setUsoCFDI("P01");

		return comprobanteReceptor;
	}

	/**
	 * M�todo agrega complemento de pagos
	 * 
	 * @param pago
	 * @return objeto complemento
	 */
	private Complemento agregarComplementoPago(Pago pago) {
		Pagos pagos = new Pagos();
		pagos.setVersion("1.0");

		pago.setFechaPago(new Date());

		pagos.getPago().add(pago);

		Complemento complemento = new Complemento();
		complemento.getAny().add(pagos);

		return complemento;

	}

	/**
	 * M�todo para asignar addenda
	 * 
	 * @param receptor
	 * @param tcContribuyente
	 * @param foliador
	 * @return objeto Adenda33
	 * @throws Exception
	 */
	private Adenda33 crearAsignarAddenda(TcReceptor receptor, TcContribuyente tcContribuyente, Foliador foliador)
			throws Exception {
		Adenda33 adenda33 = new Adenda33();
		try {
			adenda33.setFolio(String.valueOf(foliador.getFolio()));
			adenda33.setSerie(foliador.getSerie());

			DomicilioEmisor domicilioEmisor = new DomicilioEmisor();
//	    domicilioEmisor.setCalle(tcContribuyente.getTcDomicilio().getCalle());
//	    domicilioEmisor.setCodigoPostal(tcContribuyente.getTcDomicilio().getCodigoPostal());
//	    domicilioEmisor.setColonia(tcContribuyente.getTcDomicilio().getColonia());
//	    domicilioEmisor.setEstado(tcContribuyente.getTcDomicilio().getEstado());
//	    domicilioEmisor.setLocalidad(tcContribuyente.getTcDomicilio().getLocalidad());
//	    domicilioEmisor.setMunicipio(tcContribuyente.getTcDomicilio().getMunicipio());
//	    domicilioEmisor.setNumeroExterior(tcContribuyente.getTcDomicilio().getNoExterior());
//	    domicilioEmisor.setNumeroInterior(tcContribuyente.getTcDomicilio().getNoInterior());
//	    domicilioEmisor.setPais(tcContribuyente.getTcDomicilio().getPais());
//	    domicilioEmisor.setReferencia(tcContribuyente.getTcDomicilio().getReferencia());
			adenda33.setDomicilioEmisor(domicilioEmisor);

			DomicilioReceptor domicilioReceptor = new DomicilioReceptor();
//	    domicilioReceptor.setCalle(receptor.getTcDomicilio().getCalle());
//	    domicilioReceptor.setCodigoPostal(receptor.getTcDomicilio().getCodigoPostal());
//	    domicilioReceptor.setColonia(receptor.getTcDomicilio().getColonia());
//	    domicilioReceptor.setEstado(receptor.getTcDomicilio().getEstado());
//	    domicilioReceptor.setLocalidad(receptor.getTcDomicilio().getLocalidad());
//	    domicilioReceptor.setMunicipio(receptor.getTcDomicilio().getMunicipio());
//	    domicilioReceptor.setNumeroExterior(receptor.getTcDomicilio().getNoExterior());
//	    domicilioReceptor.setNumeroInterior(receptor.getTcDomicilio().getNoInterior());
//	    domicilioReceptor.setPais(receptor.getTcDomicilio().getPais());
//	    domicilioReceptor.setReferencia(receptor.getTcDomicilio().getReferencia());
			adenda33.setDomicilioReceptor(domicilioReceptor);
		} catch (Exception e) {
			LOGGER.error(e);
			LOGGER.error(e.getCause());
			LOGGER.error(e.getMessage());
			LOGGER.error(
					"Error en metodo crearAsignarAddenda--> " + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
		return adenda33;
	}

	/**
	 * M�todo que guarda informaci�n del comprobante timbrado en la base de datos
	 * 
	 * @param comprobante
	 * @param wrapperPagos
	 * @param direccionXml
	 * @param twUsuario
	 * @param tcContribuyente
	 * @param request
	 * @throws Exception
	 */
	private void guardarInformacionBaseDatos(final Comprobante comprobante, WrapperPagos wrapperPagos,
			final String direccionXml, ResultadoTimbradoWrapper timbradoWrapper, final Foliador foliador,
			final TwUsuario twUsuario, HttpServletRequest request) throws Exception {

		try {

			// if (wrapperPagos.getFaltante()) {
			// wrapperPagos.getCfdiPadre().setEstatus(2L);
			// }

			Serializable idTwCfdi = construyeNuevoCfdi(comprobante, direccionXml, foliador,
					wrapperPagos.getCfdiPadre());
			timbradoWrapper.setId((Long) idTwCfdi);

			TrGeneral trGeneral = new TrGeneral(wrapperPagos.getContribuyente(), wrapperPagos.getReceptor(),
					new TwCfdi((Long) idTwCfdi), twUsuario);
			tablaGeneralServices.saveOrUpdateGeneral(trGeneral);
			Utilerias.eventosAdministrador("CFDI", "CFDI PAGOS GUARDADO EN BASE DE DATOS", twUsuario, "INSERT",
					pistasServices, request);
		} catch (Exception e) {
			Utilerias.eventosAdministrador("CFDI", "CFDI PAGOS GUARDADO EN BASE DE DATOS", twUsuario, "ERROR",
					pistasServices, request);
			LOGGER.error(e);
			LOGGER.error(e.getCause());
			LOGGER.error(e.getMessage());
			LOGGER.error("Error en metodo guardarInformacionBaseDatos -->"
					+ UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
	}

	/**
	 * M�todo para crear objeto TwCdfi y almacenarlo en base de datos
	 * 
	 * @param comprobante
	 * @param direccionXml
	 * @param foliador
	 * @param cfdiPadre
	 * @return
	 */
	private Serializable construyeNuevoCfdi(final Comprobante comprobante, final String direccionXml,
			final Foliador foliador, TwCfdi cfdiPadre) {
		Serializable serializable = null;
		try {

			TimbreFiscalDigital timbreFiscalDigital = (TimbreFiscalDigital) UtileriasCfdi
					.getObjectoComplemento(comprobante.getComplemento().getAny(), TimbreFiscalDigital.class);
			Pagos pagos = (Pagos) UtileriasCfdi.getObjectoComplemento(comprobante.getComplemento().getAny(),
					Pagos.class);
			TwCfdi cfdi = new TwCfdi();
			cfdi.setUuid(timbreFiscalDigital.getUUID());
			cfdi.setDireccionXml(direccionXml);
			cfdi.setEstatus(1L);
			cfdi.setFechaCreacion(comprobante.getFecha());
			cfdi.setFechaCertificacion(timbreFiscalDigital.getFechaTimbrado());
			cfdi.setFolio(String.valueOf((foliador.getFolio())));
			cfdi.setSerie(foliador.getSerie());
			cfdi.setClaveConfirmacion(comprobante.getConfirmacion());
			cfdi.setMetodoPago(comprobante.getMetodoPago());
			cfdi.setMonedaDr(pagos.getPago().get(0).getMonedaP().toString());
			cfdi.setTotal(pagos.getPago().get(0).getMonto());
			cfdi.setTwCfdi(cfdiPadre);

			serializable = cfdiServices.guardarCfdi(cfdi);
		} catch (Exception e) {
			LOGGER.error("Error en metodo construyeNuevoCfdi -->" + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
		return serializable;
	}

}
