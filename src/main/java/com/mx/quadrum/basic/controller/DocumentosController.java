package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.adenda.Adenda33;
import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TrGeneral;
import com.mx.quadrum.basic.services.DocumentosLegalesServices;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.TablaGeneralServices;
import com.mx.quadrum.basic.services.TcReceptorServices;
import com.mx.quadrum.basic.util.GenerarPdf;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.cfdi.complementos.pagos.v10.schema.Pagos;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v11.schema.TimbreFiscalDigital;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.v33.schema.Comprobante;

/**
 * Clase controlador REST contiene metodos para administración de documentos
 * 
 * @author
 *
 */
@RestController
public class DocumentosController extends AbstractControllerHandler implements Serializable {

	private static final long serialVersionUID = -8306593169155070281L;

	public static Logger LOGGER = Logger.getLogger(DocumentosController.class);

	@Autowired
	private TablaGeneralServices consultaservice;

	@Autowired
	private EmisorServices emisorServices;
	@Autowired
	private AesServices aesServices;
	@Autowired
	private TcReceptorServices tcReceptorServices;

	@Autowired
	private DocumentosLegalesServices documentosLegalesServices;

	/**
	 * Método para la descarga de documentos (.pdf, .xml)
	 * 
	 * @param opcion
	 * @param id
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentos/descarga.json", method = RequestMethod.GET)
	public void doDownload(@RequestParam("opcion") String opcion, @RequestParam("id") Long id,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<String> listaDirecciones = new ArrayList<>();
		String ruta = null;
		String nameFile = null;
		TrGeneral cfdi = consultaservice.buscarCfdiPorId(id);
		ruta = cfdi.getTwCfdi().getDireccionXml();
		listaDirecciones.add(ruta);
		byte[] arrayFiles = null;

		if (opcion.equals("pdf")) {
			LOGGER.error(cfdi.getTwCfdi().getTotal());
			arrayFiles = Utilerias.crearPdf(ParametroEnum.JASPER_V4.getValue(),ruta,
					cfdi.getTwCfdi().getTotal().setScale(2));

			response.setContentType("application/pdf");
			nameFile = "FACTURA_" + cfdi.getTwCfdi().getUuid() + ".pdf";
		} else {
			nameFile = "FACTURA_" + cfdi.getTwCfdi().getUuid() + ".xml";
			response.setContentType("text/xml");
			Path path = Paths.get(ruta);
			arrayFiles = Files.readAllBytes(path);
		}

		String headerValue = String.format("attachment; filename=\"%s\"", nameFile);
		response.setHeader("Content-Disposition", headerValue);
		ServletOutputStream output = response.getOutputStream();
		output.write(arrayFiles);
		output.close();

	}

	@RequestMapping(value = "/documentos/visualizarcontrato.json", method = RequestMethod.POST)
	public void visualizarContrato(@RequestBody TcContribuyente emisor, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		byte[] bytesContrato = documentosLegalesServices.obtenerContrato(emisor, "", "", "");
		response.setContentType("application/pdf");
		String headerValue = String.format("attachment; filename=\"%s_%s\"", emisor.getRfc(),
				"CONTRATO_DE_PRESTACION_DE_SERVICIOS.pdf");
		response.setHeader("Content-Disposition", headerValue);
		ServletOutputStream output = response.getOutputStream();
		output.write(bytesContrato);
		output.close();
	}

	@RequestMapping(value = "/documentos/vizualizarConvenio.json", method = RequestMethod.POST)
	public void vizualizarConvenio(@RequestBody TcContribuyente emisor, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		byte[] bytesConvenio = documentosLegalesServices.obtenerConvenio(emisor, "", "", "");
		response.setContentType("application/pdf");
		String headerValue = String.format("attachment; filename=\"%s_%s\"", emisor.getRfc(),
				"CONVENIO_DE_CONFIDENCIALIDAD.pdf");
		response.setHeader("Content-Disposition", headerValue);
		ServletOutputStream output = response.getOutputStream();
		output.write(bytesConvenio);
		output.close();

	}

	@RequestMapping(value = "/documentos/vizualizaManifiesto.json", method = RequestMethod.POST)
	public void vizualizaManifiesto(@RequestBody TcContribuyente emisor, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		byte[] bytesManiefiesto = documentosLegalesServices.obtenerManifiesto(emisor, "", "", "");
		response.setContentType("application/pdf");
		String headerValue = String.format("attachment; filename=\"%s_%s\"", emisor.getRfc(), "MANIFIESTO.pdf");
		response.setHeader("Content-Disposition", headerValue);
		ServletOutputStream output = response.getOutputStream();
		output.write(bytesManiefiesto);
		output.close();
	}

}
