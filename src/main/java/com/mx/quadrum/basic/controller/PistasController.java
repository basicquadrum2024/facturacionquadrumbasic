package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TcPistasAdministrador;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.Paginacion;

/**
 * Clase @RestController contiene metodos para guardar las pistas de los clientes
 * @author 
 *
 */
@RestController
@RequestMapping(value = "/pistasController")
public class PistasController implements Serializable {

    @Autowired
    private PistasServices pistasServices;

    @Autowired
    private AesServices aesServices;

    private static final Logger LOGGER = Logger.getLogger(PistasController.class);

    public static final int REGISTROSPORPAGINA = 10000;
    public static final String TITULOS[] = { "USUARIO", "ACCI\u00d3N", "ENTIDAD", "DESCRIPCI\u00d3N", "FECHA",
	    "DIRECCI\u00d3N IP", "PUERTO" };

    /**
     * M�todo para generar un reporte de las pistas de los cliente
     * @param fInicial
     * @param fFinal
     * @param entidad
     * @param accion
     * @param request
     * @param response
     */
    @RequestMapping(value = "/descargaPistasCliente.json", method = RequestMethod.GET)
    public void descargaPistasCliente(@RequestParam("fechaInicio") String fInicial,
	    @RequestParam("fechaFin") String fFinal, @RequestParam("entidad") String entidad,
	    @RequestParam("accion") String accion, HttpServletRequest request, HttpServletResponse response) {
	List<TcPistasAdministrador> pistasAdmin = null;
	try {

	    Workbook libro = null;
	    libro = new SXSSFWorkbook(20);
	    Sheet hoja = libro.createSheet();
	    Integer contador = 0;
	    creaEncabezado(hoja, TITULOS);

	    String nombre = "Pistas_Cliente";

	    Date fechaInicial = null, fechaFinal = null;

	    fechaInicial = Utilerias.formatDateTime.get().parse(fInicial.toString().replace("/", "-"));
	    fechaFinal = Utilerias.formatDateTime.get().parse(fFinal.toString().replace("/", "-"));

	    Long totalRegistros = pistasServices.buscaPistasPorFechasClienteCount(fechaInicial, fechaFinal,
		    aesServices.encriptaCadena(entidad), aesServices.encriptaCadena(accion));

	    Integer paginas = Double.valueOf(Math.ceil((double) totalRegistros / (double) REGISTROSPORPAGINA))
		    .intValue();

	    for (int i = 1; i <= paginas; i++) {
		final Integer paginaInicial = (i * REGISTROSPORPAGINA) - REGISTROSPORPAGINA;
		final Integer maxResult = REGISTROSPORPAGINA;
		pistasAdmin = pistasServices.buscaPistasPorFechasCliente(fechaInicial, fechaFinal,
			aesServices.encriptaCadena(entidad), aesServices.encriptaCadena(accion), paginaInicial,
			maxResult);
		creaCuerpoAdmin(hoja, pistasAdmin, contador, i);
		contador += REGISTROSPORPAGINA;
	    }

	    escribeLibroResponse(libro, response, nombre, request);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Obtiene las pistas del Administrador en el componente ng-grid
     * 
     * @param paginacion
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/listaPistasCliente.json", method = RequestMethod.POST)
    public Paginacion listaPistasCliente(@RequestBody Paginacion paginacion, HttpServletRequest request)
	    throws Exception {
	Date fechaInicial = null;
	Date fechaFinal = null;
	String objeto = null;
	String accion = null;
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario twUsuario = Utilerias.getUserSession(authentication);

	objeto = paginacion.getConjuncion().get(1).split("[|]")[1].replace(",", "");
	accion = paginacion.getConjuncion().get(0).split("[|]")[1].replace(",", "");

	if (paginacion.getBetween() != null && !paginacion.getBetween().isEmpty()) {
	    String[] dateString1 = paginacion.getBetween().get(0);
	    String[] dateString2 = paginacion.getBetween().get(1);

	    fechaInicial = Utilerias.formatDateTime.get().parse(dateString1[1].toString().replace("/", "-"));
	    fechaFinal = Utilerias.formatDateTime.get().parse(dateString2[1].toString().replace("/", "-"));
	    paginacion = pistasServices.pistasCliente(fechaInicial, fechaFinal, aesServices.encriptaCadena(objeto),
		    aesServices.encriptaCadena(accion), paginacion);

	    Utilerias.eventosAdministrador(
		    Utilerias.ADMINISTRADOR,
		    "SE REALIZA B\u00daSQUEDA POR PARAMETROS FECHA INICIAL "
			    + dateString1[1].toString().replace("/", "-") + " Fecha Final "
			    + dateString2[1].toString().replace("/", "-") + " Entidad " + objeto, twUsuario, "SELECT",
		    pistasServices, request);

	}
	return paginacion;
    }

    /**
     * Obtiene las pistas del cliente en el componente ng-grid
     * 
     * @param paginacion
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/listaDePistasAdmin.json", method = RequestMethod.POST)
    public Paginacion listaDePistasAdmin(@RequestBody Paginacion paginacion, HttpServletRequest request)
	    throws Exception {
	Date fechaInicial = null;
	Date fechaFinal = null;
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario twUsuario = Utilerias.getUserSession(authentication);
	if (paginacion.getBetween() != null && !paginacion.getBetween().isEmpty()) {
	    String[] dateString1 = paginacion.getBetween().get(0);
	    String[] dateString2 = paginacion.getBetween().get(1);

	    fechaInicial = Utilerias.formatDateTime.get().parse(dateString1[1].toString().replace("/", "-"));
	    fechaFinal = Utilerias.formatDateTime.get().parse(dateString2[1].toString().replace("/", "-"));
	    paginacion = pistasServices.pistasAdministradores(fechaInicial, fechaFinal, paginacion);

	    Utilerias
		    .eventosAdministrador(Utilerias.ADMINISTRADOR,
			    "SE REALIZA B\u00daSQUEDA POR PARAMETROS FECHA INICIAL "
				    + dateString1[1].toString().replace("/", "-") + " Fecha Final "
				    + dateString2[1].toString().replace("/", "-"), twUsuario, "SELECT", pistasServices,
			    request);

	}
	return paginacion;
    }

    /**
     * M�todo que genera un reporte de las pistas filtradas por fecha
     * @param fInicial
     * @param fFinal
     * @param request
     * @param response
     */
    @RequestMapping(value = "/descargaPistasAdmin.json", method = RequestMethod.GET)
    public void doDowloadComprobantePorFecha(@RequestParam("fechaInicio") String fInicial,
	    @RequestParam("fechaFin") String fFinal, HttpServletRequest request, HttpServletResponse response) {
	List<TcPistasAdministrador> pistasClientes = null;
	try {
	    Workbook libro = null;
	    libro = new SXSSFWorkbook(20);
	    Sheet hoja = libro.createSheet();
	    Integer contador = 0;
	    creaEncabezado(hoja, TITULOS);

	    String nombre = "Pistas_Cliente";

	    Date fechaInicial = null, fechaFinal = null;

	    fechaInicial = Utilerias.formatDateTime.get().parse(fInicial.toString().replace("/", "-"));
	    fechaFinal = Utilerias.formatDateTime.get().parse(fFinal.toString().replace("/", "-"));

	    Long totalRegistros = pistasServices.pistasClienteCount(fechaInicial, fechaFinal);

	    Integer paginas = Double.valueOf(Math.ceil((double) totalRegistros / (double) REGISTROSPORPAGINA))
		    .intValue();

	    for (int i = 1; i <= paginas; i++) {
		final Integer paginaInicial = (i * REGISTROSPORPAGINA) - REGISTROSPORPAGINA;
		final Integer maxResult = REGISTROSPORPAGINA;
		pistasClientes = pistasServices.buscaPistasAdministrador(fechaInicial, fechaFinal, paginaInicial,
			maxResult);
		creaCuerpoAdmin(hoja, pistasClientes, contador, i);
		contador += REGISTROSPORPAGINA;
	    }

	    escribeLibroResponse(libro, response, nombre, request);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * M�todo crear el cuerpo del archivo para el reporte de pistas
     * @param hoja
     * @param pistasClientes
     * @param contador
     * @param consulta
     */
    private void creaCuerpoAdmin(Sheet hoja, List<TcPistasAdministrador> pistasClientes, int contador, int consulta) {
	try {
	    for (TcPistasAdministrador pista : pistasClientes) {

		hoja.autoSizeColumn(contador + 1);
		Row row = hoja.createRow(contador + 1);
		creaCeldaHoja(pista.getCliente(), 0, row);
		creaCeldaHoja(pista.getSentenciasql(), 1, row);
		creaCeldaHoja(pista.getObjeto(), 2, row);
		creaCeldaHoja(pista.getAccion(), 3, row);
		creaCeldaHoja(pista.getFechahora() + "", 4, row);
		creaCeldaHoja(pista.getClientaddress(), 5, row);
		creaCeldaHoja(pista.getClientport(), 6, row);
		contador++;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * crea celda de la consulta
     * 
     * @param valor
     * @param celdaX
     * @param row
     */
    private void creaCeldaHoja(String valor, int celdaX, Row row) {
	Cell celda = row.createCell(celdaX);
	HSSFRichTextString texto = new HSSFRichTextString(valor);
	celda.getSheet().autoSizeColumn(celdaX);
	celda.setCellValue(texto);
    }

    /**
     * M�todo que escribe archivo (.xlsx)
     * @param libro
     * @param response
     * @param nombre
     * @param request
     * @throws Exception
     */
    private void escribeLibroResponse(Workbook libro, HttpServletResponse response, String nombre,
	    HttpServletRequest request) throws Exception {
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	TwUsuario twUsuario = Utilerias.getUserSession(authentication);
	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-Disposition", "attachment; filename=" + nombre + ".xlsx");
	libro.write(response.getOutputStream());

	response.getOutputStream().close();

	Utilerias.eventosAdministrador(Utilerias.ADMINISTRADOR, "DESCARGA EXCEL CON NOMBRE " + nombre, twUsuario,
		"SELECT", pistasServices, request);
    }

    /**
     * M�todo para crear encabezado del reporte
     * @param hoja
     * @param titulos
     */
    private void creaEncabezado(Sheet hoja, String[] titulos) {
	Row prefila = hoja.createRow(0);
	for (int a = 0; a < titulos.length; a++) {
	    Cell cell = prefila.createCell(a);
	    cell.setCellValue(titulos[a].toUpperCase());
	    hoja.autoSizeColumn(a);
	}
    }

}