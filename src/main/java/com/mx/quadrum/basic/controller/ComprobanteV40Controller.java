package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.ssl.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.Foliador;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TrGeneral;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.CfdiServices;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.EnvioCorreoXmlServices;
import com.mx.quadrum.basic.services.FoliadorService;
import com.mx.quadrum.basic.services.MonedaServices;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.TablaGeneralServices;
import com.mx.quadrum.basic.services.TcReceptorServices;
import com.mx.quadrum.basic.services.catalogos.CPostalServices;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.wrapper.ResultadoTimbradoWrapper;
import com.mx.quadrum.cfdi.timbrado.AcuseTimbradoCfdi;
import com.mx.quadrum.cfdi.timbrado.ErroresAcuseTimbrado;
import com.mx.quadrum.cfdi.timbrado.TimbradoCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.utileria2021.cfdi.CFDIv40;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante;

@RestController
public class ComprobanteV40Controller extends AbstractControllerHandler implements Serializable {
	@Autowired
	private EmisorServices emisorServices;

	@Autowired
	private MonedaServices monedaServices;

	@Autowired
	private AesServices aesServices;

	@Autowired
	private CfdiServices cfdiServices;

	@Autowired
	private TcReceptorServices tcReceptorServices;

	@Autowired
	private TablaGeneralServices tablaGeneralServices;

	@Autowired
	private FoliadorService foliadorService;

	@Autowired
	private EnvioCorreoXmlServices envioCorreoXmlServices;

	@Autowired
	private PistasServices pistasServices;

	@Autowired
	private CPostalServices codigoPostalServices;

	private static final Logger LOGGER = Logger.getLogger(ComprobanteV40Controller.class);
	
	/**
	 * M�todo crea un nuevo objeto ComprobanteWrapper
	 * 
	 * @return objeto vac�o ComprobanteWrapper
	 * @throws Exception
	 */
	@RequestMapping(value = "/comprobante_v40/crearComprobanteCfdiv40.json", method = RequestMethod.POST)
	public Comprobante crearComprobanteCfdiv33() throws Exception {
		Comprobante comprobanteWrapper = new Comprobante();
		Comprobante.Conceptos conceptos = new Comprobante.Conceptos();
		comprobanteWrapper.setConceptos(conceptos);
		Comprobante.Impuestos impuestos = new Comprobante.Impuestos();
		comprobanteWrapper.setImpuestos(impuestos);
		return comprobanteWrapper;
	}

	@RequestMapping(value = "/comprobante_v40/generarComprobante.json", method = RequestMethod.POST)
	public ResultadoTimbradoWrapper generarComprobanteV40(@RequestBody Comprobante comprobante,
			HttpServletRequest request) throws Exception {
		comprobante.setVersion("4.0");
		ResultadoTimbradoWrapper timbradoWrapper = new ResultadoTimbradoWrapper();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		TcContribuyente tcContribuyente = emisorServices
				.buscarPorRfcEmisor(aesServices.encriptaCadena(twUsuario.getRfce()));
        comprobante.setLugarExpedicion(tcContribuyente.getLugarExpedicion());
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");  
		String strDate = dateFormat.format(new Date());  
		comprobante.setFecha(strDate.replace(" ", "T"));
		
		Comprobante.Emisor emisor = new Comprobante.Emisor();
		emisor.setNombre(tcContribuyente.getNombre());
		emisor.setRegimenFiscal(tcContribuyente.getRegimenFiscal());
		emisor.setRfc(tcContribuyente.getRfc());
		comprobante.setEmisor(emisor);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("Estad\u00edsticas creaci\u00f3n de cfdi y timbrado de cfdi\n"));
		stringBuilder.append(String.format("***** Creaci\u00f3n del cfdi *****\n"));
		stringBuilder.append(String.format("Hora de inicio creaci\u00f3n de cfdi: %s  \n",
				UtileriasCfdi.FORMATOFECHA_MILISEGUNDOS.format(new Date())));
		long tiempoIncioCreacionXml = System.currentTimeMillis();
		byte[] bytesCertificado = Base64.decodeBase64(tcContribuyente.getLlaveCer());
		byte[] bytesLLave = Base64.decodeBase64(tcContribuyente.getLlaveKey());

		CFDIv40 cfdiv4 = new CFDIv40(comprobante, bytesCertificado, bytesLLave, tcContribuyente.getClave(),2,6);
		
		try {
			String xml = cfdiv4.sellar();
			LOGGER.error(xml);
			long tiempoFinalCreacionXml = System.currentTimeMillis();
			stringBuilder.append(String.format("Hora final creaci\u00f3n de cfdi: %s \n",
					UtileriasCfdi.FORMATOFECHA_MILISEGUNDOS.format(new Date())));
			stringBuilder.append(String.format("Tiempo en crear el cfdi: %s milisegundos \n",
					(tiempoFinalCreacionXml - tiempoIncioCreacionXml)));
			stringBuilder.append(String.format("***** Timbrado del cfdi *****\n"));
			long tiempoInicioTimbradoXml = System.currentTimeMillis();
			stringBuilder.append(String.format("Hora de inicio timbrado de cfdi: %s  \n",
					UtileriasCfdi.FORMATOFECHA_MILISEGUNDOS.format(new Date())));
			AcuseTimbradoCfdi acuseTimbradoCfdi = TimbradoCfdi.timbrarXml(xml,
					ParametroEnum.DIRECCIONWSTIMBRADO.getValue(), ParametroEnum.USUARIOWS.getValue(),
					ParametroEnum.PASSWORDWS.getValue(), true);
			long tiempoFinalTimbradoXml = System.currentTimeMillis();
			stringBuilder.append(String.format("Hora final de timbrado de cfdi: %s  \n",
					UtileriasCfdi.FORMATOFECHA_MILISEGUNDOS.format(new Date())));
			stringBuilder.append(String.format("Tiempo en validar y certificar el cfdi: %s milisegundos \n",
					(tiempoFinalTimbradoXml - tiempoInicioTimbradoXml)));
			LOGGER.error(stringBuilder.toString());
			if (!acuseTimbradoCfdi.getError()) {
				Foliador foliador = null;
				foliador = foliadorService.findFoliadorByContribuyente(tcContribuyente.getId());
				if (null == foliador) {
					foliador = new Foliador(tcContribuyente, "Factura", 0L);
					foliadorService.guardarFoliador(foliador);
				}
				foliadorService.guardarFoliador(foliador);
				foliador = foliadorService.findFoliadorByContribuyente(tcContribuyente.getId());
				
				String fecha = generarRutaFecha();
				String uuid=acuseTimbradoCfdi.getUuid();
				String direccionXml = String.format("%s/%s/%s/%s.xml", ParametroEnum.DIRECCION_GUARDADO_XML.getValue(),
						comprobante.getEmisor().getRfc(), fecha, uuid);
				guardarXmlTimbrado(direccionXml, acuseTimbradoCfdi.getXml());

				timbradoWrapper.setFolio(String.valueOf(foliador.getFolio()));
				timbradoWrapper.setSerie(foliador.getSerie());
				timbradoWrapper.setUuid(uuid);
				timbradoWrapper.setLugarFile(direccionXml);
				timbradoWrapper.setErrores("Cfdi creado con �xito");
				timbradoWrapper.setTimbrado(Boolean.TRUE);
				timbradoWrapper.setError(1);
				Utilerias.eventosAdministrador("CFDI", "CFDI GENERADO CORRECTAMENTE.", twUsuario, "INSERT",
						pistasServices, request);
				
				
				
				try {
					TwCfdi cfdi = new TwCfdi();
					cfdi.setUuid(uuid);
					cfdi.setDireccionXml(direccionXml);
					cfdi.setEstatus(1L);
					cfdi.setFechaCreacion(generarFechaCreacion(comprobante.getFecha()));
					cfdi.setFechaCertificacion(acuseTimbradoCfdi.getFecha());
					cfdi.setFolio(String.valueOf((foliador.getFolio())));
					cfdi.setSerie(foliador.getSerie());
					cfdi.setClaveConfirmacion( comprobante.getConfirmacion());
					cfdi.setMetodoPago(comprobante.getMetodoPago());
					cfdi.setMonedaDr(comprobante.getMoneda());
					cfdi.setTotal(comprobante.getTotal());

					Serializable idTwCfdi = cfdiServices.guardarCfdi(cfdi);
					timbradoWrapper.setId((Long) idTwCfdi);
					TcReceptor tcReceptor = tcReceptorServices.buscaTcReceptorPorRFC(comprobante.getReceptor().getRfc());
					TrGeneral trGeneral = new TrGeneral(tcContribuyente, tcReceptor, new TwCfdi((Long) idTwCfdi), twUsuario);
					tablaGeneralServices.saveOrUpdateGeneral(trGeneral);

					Utilerias.eventosAdministrador("CFDI", "CFDI GUARDADO EN BASE DE DATOS", twUsuario, "INSERT",
							pistasServices, request);
				} catch (Exception e) {
					Utilerias.eventosAdministrador("CFDI", "CFDI GUARDADO EN BASE DE DATOS", twUsuario, "ERROR", pistasServices,
							request);
					LOGGER.error("Error en metodo guardarInformacionBaseDatos -->"
							+ UtileriasCfdi.imprimeStackError(e.getStackTrace()));
				}
				TwCfdi twCfdi = cfdiServices.buscaTwCfdi(uuid);
				envioCorreoXmlServices.enviarXmlPorCorreo(twCfdi, direccionXml,uuid, twUsuario.getCorreo());

			} else {
				StringBuilder builder = new StringBuilder();
				for (ErroresAcuseTimbrado erroresAcuseTimbrado : acuseTimbradoCfdi.getErrores()) {
					builder.append(String.format("C\u00F3digo de Incidencia: %s, Mensaje de Incidencia: %s \n\r",
							erroresAcuseTimbrado.getCodigoError(), erroresAcuseTimbrado.getMensaje()));

				}
				Utilerias.eventosAdministrador("CFDI", "NO SE PUDO GENERAR EL COMPROBANTE.", twUsuario, "ERROR",
						pistasServices, request);
				timbradoWrapper.setUuid(builder.toString());
			}
		} catch (Exception e) {
			timbradoWrapper.setUuid("Error en la creaci\u00f3n del CFDI");
			timbradoWrapper.setErrores(e.getLocalizedMessage()
					.replace("Error de  validacion contra XSD: *******   ERROR   **************", "")
					.replace("*********   ERROR   **************", "").replace("\r\n", ""));
			LOGGER.error("Error en metodo generarComprobante mensaje-->" + e.getMessage() + " statck"
					+ UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}

		return timbradoWrapper;
	}
		
	@RequestMapping(value = "/comprobante_v40/calculosComprobante.json", method = RequestMethod.POST)
	public Comprobante calculosComprobante(@RequestBody Comprobante comprobante) throws Exception {
		sumarConceptos(comprobante);
		return comprobante;
	}
	
	private void sumarConceptos(Comprobante comprobante) throws Exception {
		Integer decimalesMoneda = 2;
		if (comprobante.getMoneda() != null) {
			decimalesMoneda = monedaServices.buscarMonedasPorClave(comprobante.getMoneda());
		}
		BigDecimal subtotal = BigDecimal.ZERO;
		BigDecimal totalDescuento = BigDecimal.ZERO;
		BigDecimal totalImpuestosRetenidos = BigDecimal.ZERO;
		BigDecimal totalImpuestosTrasladados = BigDecimal.ZERO;
		Map<String, Comprobante.Impuestos.Traslados.Traslado> mapaTraslados = new TreeMap<>();
		Map<String, Comprobante.Impuestos.Retenciones.Retencion> mapaRetencion =new TreeMap<>();
		
		
		for (Comprobante.Conceptos.Concepto concepto : comprobante.getConceptos().getConcepto()) {
			if (concepto.getDescuento() != null)
				totalDescuento = totalDescuento.add(concepto.getDescuento());
			if (concepto.getImporte() != null)
				subtotal = subtotal.add(concepto.getImporte());
			if (null != concepto.getImpuestos().getRetenciones()) {
				for (Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion retencion : concepto.getImpuestos()
						.getRetenciones().getRetencion()) {
					if (retencion.getImporte() != null)
						totalImpuestosRetenidos = totalImpuestosRetenidos.add(retencion.getImporte());
					 
					String llaveRetencionIva=retencion.getTipoFactor();
					if (mapaRetencion.containsKey(llaveRetencionIva)) {
						BigDecimal importe = mapaRetencion.get(llaveRetencionIva).getImporte();
						importe = importe.add(retencion.getImporte());
						mapaRetencion.get(llaveRetencionIva).setImporte(importe);
					}else {
						Comprobante.Impuestos.Retenciones.Retencion retencionIva = new Comprobante.Impuestos.Retenciones.Retencion();
						retencionIva.setImporte(retencion.getImporte());
						retencionIva.setImpuesto(retencion.getImpuesto());
						mapaRetencion.put(llaveRetencionIva, retencionIva);
						
					}
				}
			}
			if (null != concepto.getImpuestos().getTraslados()) {
				for (Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado traslado : concepto.getImpuestos()
						.getTraslados().getTraslado()) {
					if (traslado.getImporte() != null)
						totalImpuestosTrasladados = totalImpuestosTrasladados.add(traslado.getImporte());
					 String key=traslado.getTipoFactor();
					 
					if (mapaTraslados.containsKey(key)) {
						BigDecimal importeTrasaldo = mapaTraslados.get(key).getImporte();
						importeTrasaldo = importeTrasaldo.add(traslado.getImporte());
						BigDecimal importeBase=mapaTraslados.get(key).getBase();
						importeBase=importeBase.add(traslado.getBase());
						mapaTraslados.get(key).setImporte(importeTrasaldo);
					}else {
						Comprobante.Impuestos.Traslados.Traslado trasladoImpuesto = new Comprobante.Impuestos.Traslados.Traslado();
						trasladoImpuesto.setImporte(traslado.getImporte());
						trasladoImpuesto.setImpuesto(traslado.getImpuesto());
						trasladoImpuesto.setTasaOCuota(traslado.getTasaOCuota());
						trasladoImpuesto.setTipoFactor(traslado.getTipoFactor());
						trasladoImpuesto.setBase(traslado.getBase());
						mapaTraslados.put(key, trasladoImpuesto);	
					}
				}
			}

		}
		
		Comprobante.Impuestos.Retenciones retenciones=null;
		if(totalImpuestosRetenidos.compareTo(BigDecimal.ZERO)==1) {
			comprobante.getImpuestos()
			.setTotalImpuestosRetenidos(totalImpuestosRetenidos.setScale(decimalesMoneda, RoundingMode.HALF_UP));
			retenciones=new Comprobante.Impuestos.Retenciones();	
		}
		
		for (Map.Entry<String, Comprobante.Impuestos.Retenciones.Retencion> retencionIVA : mapaRetencion.entrySet()) {
			retenciones.getRetencion().add(retencionIVA.getValue());
		}
		
		Comprobante.Impuestos.Traslados traslados=null;
		if(totalImpuestosTrasladados.compareTo(BigDecimal.ZERO)==1) {
			comprobante.getImpuestos().setTotalImpuestosTrasladados(
					totalImpuestosTrasladados.setScale(decimalesMoneda, RoundingMode.HALF_UP));
			traslados = new Comprobante.Impuestos.Traslados();
		}
		if(totalImpuestosTrasladados.compareTo(BigDecimal.ZERO)==0) {
			traslados = new Comprobante.Impuestos.Traslados();
		}
		
		for (Map.Entry<String, Comprobante.Impuestos.Traslados.Traslado> mapaIvasTraslado : mapaTraslados.entrySet()) {
			traslados.getTraslado().add(mapaIvasTraslado.getValue());
		}
		comprobante.getImpuestos().setRetenciones(retenciones);
		comprobante.getImpuestos().setTraslados(traslados);
		comprobante.setSubTotal(subtotal.setScale(decimalesMoneda, RoundingMode.HALF_UP));
		
		if(mapaTraslados.isEmpty())
			totalImpuestosTrasladados=BigDecimal.ZERO;
		
		if(mapaRetencion.isEmpty())
			totalImpuestosRetenidos=BigDecimal.ZERO;

		double totalDouble = 0;
		totalDouble = subtotal.doubleValue() - totalDescuento.doubleValue() + totalImpuestosTrasladados.doubleValue()
				- totalImpuestosRetenidos.doubleValue();
		comprobante.setDescuento(
				totalDescuento.intValue() == 0 ? null : totalDescuento.setScale(decimalesMoneda, RoundingMode.HALF_UP));
		comprobante.setTotal((new BigDecimal(totalDouble)).setScale(decimalesMoneda, RoundingMode.HALF_UP));
	}
	
	private String generarRutaFecha() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MMMM/dd/HH", new Locale("es", "MX"));
		return simpleDateFormat.format(new Date());
	}
	
	private Date generarFechaCreacion(String fecha) throws Exception{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", new Locale("es", "MX"));
		return simpleDateFormat.parse(fecha);
	}
	
	private void guardarXmlTimbrado(final String pathArchivo, final String contenido) {
		try {
			Utilerias.guardarArchivo(pathArchivo, contenido, StandardCharsets.UTF_8);
		} catch (Exception e) {
			LOGGER.error(String.format("Error al guardar el archivo xml %s , %s , excepction: %s", pathArchivo,
					contenido, UtileriasCfdi.imprimeStackError(e)));
		}
	}
	
	@RequestMapping(value = "/comprobante_v40/calcularImporte/{valor1}/{valor2}/{decimales}/.json", method = RequestMethod.POST)
	public BigDecimal calcularImporte(@PathVariable("valor1") BigDecimal valor1,
			@PathVariable("valor2") BigDecimal valor2, @PathVariable("decimales") int decimales) throws Exception {
		
     return (valor1.multiply(valor2)).setScale(decimales, RoundingMode.HALF_UP);
	}
	
	public static void main(String args[]) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");  
		String strDate = dateFormat.format(new Date());  
		
		System.out.println(strDate);
		
	}

	

}
