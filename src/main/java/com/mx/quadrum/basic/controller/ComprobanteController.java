package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.ssl.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.adenda.Adenda33;
import com.mx.quadrum.basic.adenda.DomicilioEmisor;
import com.mx.quadrum.basic.adenda.DomicilioReceptor;
import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.Foliador;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TrGeneral;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.entity.catalogosSAT.CodigoPostal;
import com.mx.quadrum.basic.excepcion.ValidacionCertificadosExcepcion;
import com.mx.quadrum.basic.services.CfdiServices;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.services.EnvioCorreoXmlServices;
import com.mx.quadrum.basic.services.FoliadorService;
import com.mx.quadrum.basic.services.MonedaServices;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.TablaGeneralServices;
import com.mx.quadrum.basic.services.TcReceptorServices;
import com.mx.quadrum.basic.services.catalogos.CPostalServices;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.basic.util.ValidacionCertificadosContribuyente;
import com.mx.quadrum.basic.wrapper.ComprobanteWrapper;
import com.mx.quadrum.basic.wrapper.ReceptorCfdiv33Wrapper;
import com.mx.quadrum.basic.wrapper.ResultadoTimbradoWrapper;
import com.mx.quadrum.cfdi.CFDIv33;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v11.schema.TimbreFiscalDigital;
import com.mx.quadrum.cfdi.timbrado.AcuseTimbradoCfdi;
import com.mx.quadrum.cfdi.timbrado.ErroresAcuseTimbrado;
import com.mx.quadrum.cfdi.timbrado.TimbradoCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasFecha;
import com.mx.quadrum.cfdi.v33.schema.Comprobante;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Conceptos.Concepto;

/**
 * Esta clase es un controlador REST, contiene los m�todos para el modulo de
 * generaci�n y timbrado del comprobante herada de AbstractControllerHandler e
 * implementa Serializable
 * 
 * @author
 *
 */
@RestController
public class ComprobanteController extends AbstractControllerHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3615724992061398651L;
	private static final Logger LOGGER = Logger.getLogger(ComprobanteController.class);

	@Autowired
	private EmisorServices emisorServices;

	@Autowired
	private MonedaServices monedaServices;

	@Autowired
	private AesServices aesServices;

	@Autowired
	private CfdiServices cfdiServices;

	@Autowired
	private TcReceptorServices tcReceptorServices;

	@Autowired
	private TablaGeneralServices tablaGeneralServices;

	@Autowired
	private FoliadorService foliadorService;

	@Autowired
	private EnvioCorreoXmlServices envioCorreoXmlServices;

	@Autowired
	private PistasServices pistasServices;

	@Autowired
	private CPostalServices codigoPostalServices;

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/comprobante/validarLugarExpedicion.json", method = RequestMethod.POST)
	public Boolean validarLugarExpedicion() throws Exception {
		Boolean validacion = Boolean.FALSE;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		TcContribuyente tcContribuyente = emisorServices
				.buscarPorRfcEmisor(aesServices.encriptaCadena(twUsuario.getRfce()));
		if (tcContribuyente.getLugarExpedicion() == null)
			validacion = Boolean.TRUE;
		return validacion;
	}

	/**
	 * M�todo crea un nuevo objeto ComprobanteWrapper
	 * 
	 * @return objeto vac�o ComprobanteWrapper
	 * @throws Exception
	 */
	@RequestMapping(value = "/comprobante/crearComprobanteCfdiv33.json", method = RequestMethod.POST)
	public ComprobanteWrapper crearComprobanteCfdiv33() throws Exception {
		ComprobanteWrapper comprobanteWrapper = new ComprobanteWrapper();
		Comprobante.Conceptos conceptos = new Comprobante.Conceptos();
		comprobanteWrapper.setConceptos(conceptos);
		Comprobante.Impuestos impuestos = new Comprobante.Impuestos();
		comprobanteWrapper.setImpuestos(impuestos);
		return comprobanteWrapper;
	}

	/**
	 * Metodo para timbrado del comprobante, devuelve el objeto
	 * ResultadoTimbradoWrapper el cual contiene la respuesta al timbrado, si es
	 * satisfactoria devolvera el CFDI timbrado en caso contrario devolvera el error
	 * correspodiente
	 * 
	 * @param         comprobante, objeto con la estructura correspondiente para
	 *                crear un objeto de CFDI
	 * @param request
	 * @return objeto ResultadoTimbradoWrapper
	 * @throws Exception
	 */
	@RequestMapping(value = "/comprobante/generarComprobante.json", method = RequestMethod.POST)
	public ResultadoTimbradoWrapper generarComprobante(@RequestBody ComprobanteWrapper comprobante,
			HttpServletRequest request) throws Exception {
		ResultadoTimbradoWrapper timbradoWrapper = new ResultadoTimbradoWrapper();
		eliminarConceptosImpuestos(comprobante);
		comprobante.setReceptor(comprobante.getReceptorCfdiv33Wrapper());

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		TcContribuyente tcContribuyente = emisorServices
				.buscarPorRfcEmisor(aesServices.encriptaCadena(twUsuario.getRfce()));

		if (null != tcContribuyente.getLugarExpedicion()) {
			comprobante.setLugarExpedicion(tcContribuyente.getLugarExpedicion());
			CodigoPostal codigoPostal = codigoPostalServices.buscarCodigoPostal(tcContribuyente.getLugarExpedicion());
			if (ParametroEnum.USO_HORARIO.getValue().equals("-5")) {
				String usoHorario = String.format("%s%s", "GMT", codigoPostal.getDiferenciaHorarioVerano());
				comprobante.setFecha(UtileriasFecha.obtenerFechaPorTimeZone(usoHorario));
			}
			if (ParametroEnum.USO_HORARIO.getValue().equals("-6")) {
				String usoHorario = String.format("%s%s", "GMT", codigoPostal.getDiferenciaHorarioInvierno());
				comprobante.setFecha(UtileriasFecha.obtenerFechaPorTimeZone(usoHorario));
			}
		}

		Comprobante.Emisor emisor = new Comprobante.Emisor();
		emisor.setRfc(tcContribuyente.getRfc());
		emisor.setNombre(tcContribuyente.getNombre());
		emisor.setRegimenFiscal(tcContribuyente.getRegimenFiscal());
		comprobante.setEmisor(emisor);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("Estad\u00edsticas creaci\u00f3n de cfdi y timbrado de cfdi\n"));
		stringBuilder.append(String.format("***** Creaci\u00f3n del cfdi *****\n"));
		stringBuilder.append(String.format("Hora de inicio creaci\u00f3n de cfdi: %s  \n",
				UtileriasCfdi.FORMATOFECHA_MILISEGUNDOS.format(new Date())));
		long tiempoIncioCreacionXml = System.currentTimeMillis();
		byte[] bytesCertificado = Base64.decodeBase64(tcContribuyente.getLlaveCer());
		byte[] bytesLLave = Base64.decodeBase64(tcContribuyente.getLlaveKey());
//		try {
//			ValidacionCertificadosContribuyente validacionCertificadosContribuyente = new ValidacionCertificadosContribuyente(
//					bytesCertificado, bytesLLave, tcContribuyente.getClave(), tcContribuyente.getRfc(), "validacion",
//					"CSD");
//			validacionCertificadosContribuyente.generarFirma();
//		} catch (ValidacionCertificadosExcepcion e) {
//			timbradoWrapper.setUuid(e.getMessage());
//			return timbradoWrapper;
//		}catch (Exception e) {
//			LOGGER.error(String.format("Error en el metodo validar certificados emisor: %s",
//					UtileriasCfdi.imprimeStackError(e)));
//			timbradoWrapper.setUuid("Los certificados CSD del contribuyente son iv\u00e1lidos");
//			return timbradoWrapper;
//		}

		CFDIv33 cfdIv33 = new CFDIv33(comprobante, bytesCertificado, bytesLLave, tcContribuyente.getClave(), 0, 6);
		try {
			String xml = cfdIv33.sellarObtenerComprobanteEnString();
			long tiempoFinalCreacionXml = System.currentTimeMillis();
			stringBuilder.append(String.format("Hora final creaci\u00f3n de cfdi: %s \n",
					UtileriasCfdi.FORMATOFECHA_MILISEGUNDOS.format(new Date())));
			stringBuilder.append(String.format("Tiempo en crear el cfdi: %s milisegundos \n",
					(tiempoFinalCreacionXml - tiempoIncioCreacionXml)));
			stringBuilder.append(String.format("***** Timbrado del cfdi *****\n"));
			long tiempoInicioTimbradoXml = System.currentTimeMillis();
			stringBuilder.append(String.format("Hora de inicio timbrado de cfdi: %s  \n",
					UtileriasCfdi.FORMATOFECHA_MILISEGUNDOS.format(new Date())));
			AcuseTimbradoCfdi acuseTimbradoCfdi = TimbradoCfdi.timbrarXml(xml,
					ParametroEnum.DIRECCIONWSTIMBRADO.getValue(), ParametroEnum.USUARIOWS.getValue(),
					ParametroEnum.PASSWORDWS.getValue(), true);
			long tiempoFinalTimbradoXml = System.currentTimeMillis();
			stringBuilder.append(String.format("Hora final de timbrado de cfdi: %s  \n",
					UtileriasCfdi.FORMATOFECHA_MILISEGUNDOS.format(new Date())));
			stringBuilder.append(String.format("Tiempo en validar y certificar el cfdi: %s milisegundos \n",
					(tiempoFinalTimbradoXml - tiempoInicioTimbradoXml)));
			LOGGER.error(stringBuilder.toString());
			if (!acuseTimbradoCfdi.getError()) {
				Foliador foliador = null;
				foliador = foliadorService.findFoliadorByContribuyente(tcContribuyente.getId());
				if (null == foliador) {
					foliador = new Foliador(tcContribuyente, "Factura", 0L);
					foliadorService.guardarFoliador(foliador);
				}
				foliadorService.guardarFoliador(foliador);
				foliador = foliadorService.findFoliadorByContribuyente(tcContribuyente.getId());

				Class<?> clases[] = { Comprobante.class, TimbreFiscalDigital.class };
				Comprobante comprobanteCdfiv33 = UtileriasCfdi.convertirAComprobante(acuseTimbradoCfdi.getXml(),
						clases);
				CFDIv33 cfdIvConvertidor = new CFDIv33(comprobanteCdfiv33, 0, 6);
				String direccionXml = null;
				direccionXml = UtileriasCfdi.crearDireccionGuardadoXml(ParametroEnum.DIRECCION_GUARDADO_XML.getValue(),
						comprobante.getFecha(), comprobante.getEmisor().getRfc(), acuseTimbradoCfdi.getUuid());
				UtileriasCfdi.guardarArchivoXml(direccionXml, cfdIvConvertidor.convertirComprobanteEnString(null));

				timbradoWrapper.setFolio(String.valueOf(foliador.getFolio()));
				timbradoWrapper.setSerie(foliador.getSerie());
				timbradoWrapper.setUuid(acuseTimbradoCfdi.getUuid());
				timbradoWrapper.setLugarFile(direccionXml);
				timbradoWrapper.setErrores("Cfdi creado con �xito");
				timbradoWrapper.setTimbrado(Boolean.TRUE);
				timbradoWrapper.setError(1);
				Utilerias.eventosAdministrador("CFDI", "CFDI GENERADO CORRECTAMENTE.", twUsuario, "INSERT",
						pistasServices, request);
				guardarInformacionBaseDatos(acuseTimbradoCfdi.getXml(), direccionXml, twUsuario, tcContribuyente,
						comprobante.getReceptor().getRfc(), foliador, comprobanteCdfiv33.getConfirmacion(), request,
						timbradoWrapper);
				TwCfdi twCfdi = cfdiServices.buscaTwCfdi(acuseTimbradoCfdi.getUuid());
				envioCorreoXmlServices.enviarXmlPorCorreo(twCfdi, direccionXml,acuseTimbradoCfdi.getUuid(), twUsuario.getCorreo());

			} else {
				
				StringBuilder builder = new StringBuilder();
				for (ErroresAcuseTimbrado erroresAcuseTimbrado : acuseTimbradoCfdi.getErrores()) {
					builder.append(String.format("C\u00F3digo de Incidencia: %s, Mensaje de Incidencia: %s \n\r",
							erroresAcuseTimbrado.getCodigoError(), erroresAcuseTimbrado.getMensaje()));

				}
				
				Utilerias.eventosAdministrador("CFDI", "NO SE PUDO GENERAR EL COMPROBANTE.", twUsuario, "ERROR",
						pistasServices, request);
				timbradoWrapper.setUuid(builder.toString());
			}

			// else {
			// Utilerias.eventosAdministrador("CFDI",
			// "NO SE PUDO GENERAR EL COMPROBANTE.", twUsuario, "ERROR",
			// pistasServices, request);
			// timbradoWrapper
			// .setUuid("No se ha podido timbrar la factura intente m\u00e1s
			// tarde, o por favor, contacte alg\u00fan agente del centro de
			// atenci\u00f3n al cliente");
			// timbradoWrapper.setErrores(acuseTimbradoCfdi.getErrores());
			// }
		} catch (Exception e) {
			timbradoWrapper.setUuid("Error en la creaci\u00f3n del CFDI");
			timbradoWrapper.setErrores(e.getLocalizedMessage()
					.replace("Error de  validacion contra XSD: *******   ERROR   **************", "")
					.replace("*********   ERROR   **************", "").replace("\r\n", ""));
			LOGGER.error("Error en metodo generarComprobante mensaje-->" + e.getMessage() + " statck"
					+ UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}

		return timbradoWrapper;
	}

	/**
	 * M�todo para invocar a sumarConceptos
	 * 
	 * @param comprobante
	 * @return devuelve el objeto ComprobanteWrapper con los nuevos datos agregados
	 * @throws Exception
	 */
	@RequestMapping(value = "/comprobante/calculosComprobante.json", method = RequestMethod.POST)
	public ComprobanteWrapper calculosComprobante(@RequestBody ComprobanteWrapper comprobante) throws Exception {
		sumarConceptos(comprobante);
		return comprobante;
	}

	/**
	 * Validar si el contribuyente puede seguir timbrado, seg�n la restricci�n
	 * n�mero de timbres al mes
	 * 
	 * @return true reguir timbrado, false no podr� timbrar
	 * @throws Exception
	 */
	@RequestMapping(value = "/comprobante/validarContinuidadTimbrado.json", method = RequestMethod.POST)
	public boolean validarContinuidadTimbrado() throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario twUsuario = Utilerias.getUserSession(auth);
		Date ahora = new Date();
		Date fechaInicial = UtileriasFecha.obtenerPrimeraHoraMes(ahora);
		Date fechaFinal = UtileriasFecha.obtenerUltimaHoraMes(ahora);
		Integer totalMes = cfdiServices.contarComprobantesTimbradosMes(twUsuario, fechaInicial, fechaFinal);
		if (totalMes == null)
			return true;
		return totalMes.intValue() <= twUsuario.getCfdiMes() - 1 ? true : false;
	}

	/**
	 * M�todo que valida la clave de comfirmaci�n, asignada al emisor
	 * 
	 * @param claveConfirmacion
	 * @return true si ya fue usada o false si la puede usar
	 * @throws Exception
	 */
	@RequestMapping(value = "/comprobante/validarClaveConfirmacion/{claveConfirmacion}/.json", method = RequestMethod.POST)
	public boolean validarClaveConfirmacion(@PathVariable("claveConfirmacion") String claveConfirmacion)
			throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Date ahora = new Date();
		Date fechaInicial = UtileriasFecha.obtenerPrimeraHoraMes(ahora);
		Date fechaFinal = UtileriasFecha.obtenerUltimaHoraMes(ahora);
		List<TrGeneral> listaClaves = tablaGeneralServices.buscarPorEmisorClaveConfirmacion(
				aesServices.encriptaCadena(Utilerias.getUserSession(auth).getRfce()),
				aesServices.encriptaCadena(claveConfirmacion), 0, 25,fechaInicial,fechaFinal);
		return listaClaves != null && !listaClaves.isEmpty() ? false : true;
	}

	/**
	 * M�todo que realiza las operaciones correspondiente para obtener los valores
	 * totales de CFDI
	 * 
	 * @param comprobante Objeto de donde obtenemos los insumos para las
	 *                    operaciones, y se le agregan los valores totales
	 * @throws Exception
	 */
	private void sumarConceptos(ComprobanteWrapper comprobante) throws Exception {
		Integer decimalesMoneda = 2;
		if (comprobante.getMoneda() != null) {
			decimalesMoneda = monedaServices.buscarMonedasPorClave(comprobante.getMoneda());
		}
		BigDecimal subtotal = new BigDecimal("0.00");
		BigDecimal totalDescuento = new BigDecimal("0.00");
		BigDecimal totalImpuestosRetenidos = new BigDecimal("0.00");
		BigDecimal totalImpuestosTrasladados = new BigDecimal("0.00");
		for (Comprobante.Conceptos.Concepto concepto : comprobante.getConceptos().getConcepto()) {
			if (concepto.getDescuento() != null)
				totalDescuento = totalDescuento.add(concepto.getDescuento());
			if (concepto.getImporte() != null)
				subtotal = subtotal.add(concepto.getImporte());
			if (null != concepto.getImpuestos().getRetenciones()) {
				for (Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion retencion : concepto.getImpuestos()
						.getRetenciones().getRetencion()) {
					if (retencion.getImporte() != null)
						totalImpuestosRetenidos = totalImpuestosRetenidos.add(retencion.getImporte());
				}
			}

			if (null != concepto.getImpuestos().getTraslados()) {
				for (Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado traslado : concepto.getImpuestos()
						.getTraslados().getTraslado()) {
					if (traslado.getImporte() != null)
						totalImpuestosTrasladados = totalImpuestosTrasladados.add(traslado.getImporte());
				}
			}

		}

		comprobante.getImpuestos()
				.setTotalImpuestosRetenidos(totalImpuestosRetenidos.setScale(decimalesMoneda, RoundingMode.HALF_UP));
		comprobante.getImpuestos().setTotalImpuestosTrasladados(
				totalImpuestosTrasladados.setScale(decimalesMoneda, RoundingMode.HALF_UP));
		comprobante.setSubTotal(subtotal.setScale(decimalesMoneda, RoundingMode.HALF_UP));

		double totalDouble = 0;
		totalDouble = subtotal.doubleValue() - totalDescuento.doubleValue() + totalImpuestosTrasladados.doubleValue()
				- totalImpuestosRetenidos.doubleValue();
		comprobante.setDescuento(
				totalDescuento.intValue() == 0 ? null : totalDescuento.setScale(decimalesMoneda, RoundingMode.HALF_UP));
		comprobante.setTotal((new BigDecimal(totalDouble)).setScale(decimalesMoneda, RoundingMode.HALF_UP));
	}

	/**
	 * M�todo para almacenar informac�n del comprobante timbrado en la base de datos
	 * 
	 * @param xml
	 * @param direccionXml
	 * @param twUsuario
	 * @param tcContribuyente
	 * @param rfcReceptor
	 * @param foliador
	 * @param claveConfirmacion
	 * @param request
	 * @throws Exception
	 */
	public void guardarInformacionBaseDatos(final String xml, final String direccionXml, final TwUsuario twUsuario,
			final TcContribuyente tcContribuyente, final String rfcReceptor, final Foliador foliador,
			final String claveConfirmacion, HttpServletRequest request, ResultadoTimbradoWrapper timbradoWrapper)
			throws Exception {

		try {
			Serializable idTwCfdi = construyeNuevoCfdi(xml, direccionXml, foliador, claveConfirmacion);
			timbradoWrapper.setId((Long) idTwCfdi);
			TcReceptor tcReceptor = tcReceptorServices.buscaTcReceptorPorRFC(rfcReceptor);
			TrGeneral trGeneral = new TrGeneral(tcContribuyente, tcReceptor, new TwCfdi((Long) idTwCfdi), twUsuario);
			tablaGeneralServices.saveOrUpdateGeneral(trGeneral);

			Utilerias.eventosAdministrador("CFDI", "CFDI GUARDADO EN BASE DE DATOS", twUsuario, "INSERT",
					pistasServices, request);
		} catch (Exception e) {
			Utilerias.eventosAdministrador("CFDI", "CFDI GUARDADO EN BASE DE DATOS", twUsuario, "ERROR", pistasServices,
					request);
			LOGGER.error("Error en metodo guardarInformacionBaseDatos -->"
					+ UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
	}

	/**
	 * M�todo que setea valores al objeto TwCdfi y lo guarda en la base de datos
	 * 
	 * @param xml
	 * @param direccionXml
	 * @param foliador
	 * @param claveConfirmacion
	 * @return
	 */
	private Serializable construyeNuevoCfdi(final String xml, final String direccionXml, final Foliador foliador,
			final String claveConfirmacion) {
		Serializable serializable = null;
		try {
			Class<?> classes[] = { Comprobante.class, TimbreFiscalDigital.class };
			Comprobante comprobante = UtileriasCfdi.convertirAComprobante(xml, classes);
			TimbreFiscalDigital timbreFiscalDigital = (TimbreFiscalDigital) UtileriasCfdi
					.getObjectoComplemento(comprobante.getComplemento().getAny(), TimbreFiscalDigital.class);
			TwCfdi cfdi = new TwCfdi();
			cfdi.setUuid(timbreFiscalDigital.getUUID());
			cfdi.setDireccionXml(direccionXml);
			cfdi.setEstatus(1L);
			cfdi.setFechaCreacion(comprobante.getFecha());
			cfdi.setFechaCertificacion(timbreFiscalDigital.getFechaTimbrado());
			cfdi.setFolio(String.valueOf((foliador.getFolio())));
			cfdi.setSerie(foliador.getSerie());
			cfdi.setClaveConfirmacion(claveConfirmacion);
			cfdi.setMetodoPago(comprobante.getMetodoPago());
			cfdi.setMonedaDr(comprobante.getMoneda());
			cfdi.setTotal(comprobante.getTotal());

			serializable = cfdiServices.guardarCfdi(cfdi);
		} catch (Exception e) {
			LOGGER.error("Error en metodo construyeNuevoCfdi -->" + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
		return serializable;
	}

	/**
	 * M�todo para agregar valores a los campos impuestos del CFDI
	 * 
	 * @param comprobante
	 */
	private void eliminarConceptosImpuestos(ComprobanteWrapper comprobante) {

		for (Concepto concepto : comprobante.getConceptos().getConcepto()) {
			if (concepto.getImpuestos() != null) {
				if (concepto.getImpuestos().getRetenciones() != null) {
					if (concepto.getImpuestos().getRetenciones().getRetencion().isEmpty()) {
						concepto.getImpuestos().setRetenciones(null);
					}
				}

				if (concepto.getImpuestos().getTraslados() != null) {
					if (concepto.getImpuestos().getTraslados().getTraslado().isEmpty()) {
						concepto.getImpuestos().setTraslados(null);
					}
				}
			}
		}

		for (Concepto concepto : comprobante.getConceptos().getConcepto()) {
			if (concepto.getImpuestos() != null) {
				if (concepto.getImpuestos().getRetenciones() == null
						&& concepto.getImpuestos().getTraslados() == null) {
					concepto.setImpuestos(null);
				}
			}
		}

		BigDecimal totalImpuestosRetenidos = new BigDecimal(0);
		BigDecimal totalImpuestosTrasladados = new BigDecimal(0);
		Map<String, Comprobante.Impuestos.Traslados.Traslado> mapaTraslados = new HashMap<String, Comprobante.Impuestos.Traslados.Traslado>();
		Map<String, Comprobante.Impuestos.Retenciones.Retencion> mapaRetenciones = new HashMap<String, Comprobante.Impuestos.Retenciones.Retencion>();
		for (Concepto concepto : comprobante.getConceptos().getConcepto()) {
			if (concepto.getImpuestos() != null) {
				if (concepto.getImpuestos().getTraslados() != null) {
					for (Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado trasladoConcepto : concepto
							.getImpuestos().getTraslados().getTraslado()) {
						if (null != trasladoConcepto.getTasaOCuota() && null != trasladoConcepto.getImporte()) {
							String llaveTraslado = trasladoConcepto.getImpuesto() + "-"
									+ trasladoConcepto.getTipoFactor() + "-"
									+ String.valueOf(trasladoConcepto.getTasaOCuota());
							if (mapaTraslados.containsKey(llaveTraslado)) {
								BigDecimal importeTrasaldo = mapaTraslados.get(llaveTraslado).getImporte();
								if (trasladoConcepto.getImporte() != null)
									importeTrasaldo = importeTrasaldo.add(trasladoConcepto.getImporte());
								mapaTraslados.get(llaveTraslado).setImporte(importeTrasaldo);
							} else {
								Comprobante.Impuestos.Traslados.Traslado trasladoImpuesto = new Comprobante.Impuestos.Traslados.Traslado();
								trasladoImpuesto.setImpuesto(trasladoConcepto.getImpuesto());
								trasladoImpuesto.setTipoFactor(trasladoConcepto.getTipoFactor());
								if (null != trasladoConcepto.getTasaOCuota())
									trasladoImpuesto.setTasaOCuota(trasladoConcepto.getTasaOCuota());
								if (null != trasladoConcepto.getImporte())
									trasladoImpuesto.setImporte(trasladoConcepto.getImporte());
								mapaTraslados.put(llaveTraslado, trasladoImpuesto);
							}
							if (trasladoConcepto.getImporte() != null)
								totalImpuestosTrasladados = totalImpuestosTrasladados
										.add(trasladoConcepto.getImporte());
						}
					}

				}

				if (concepto.getImpuestos().getRetenciones() != null) {
					for (Comprobante.Conceptos.Concepto.Impuestos.Retenciones.Retencion retencionConcepto : concepto
							.getImpuestos().getRetenciones().getRetencion()) {
						String llaveTraslado = retencionConcepto.getImpuesto();
						if (mapaRetenciones.containsKey(llaveTraslado)) {
							BigDecimal importeRetencion = mapaRetenciones.get(llaveTraslado).getImporte();
							importeRetencion = importeRetencion.add(retencionConcepto.getImporte());
							mapaRetenciones.get(llaveTraslado).setImporte(importeRetencion);
						} else {
							Comprobante.Impuestos.Retenciones.Retencion retencionImpuesto = new Comprobante.Impuestos.Retenciones.Retencion();
							retencionImpuesto.setImpuesto(retencionConcepto.getImpuesto());
							retencionImpuesto.setImporte(retencionConcepto.getImporte());
							mapaRetenciones.put(llaveTraslado, retencionImpuesto);
						}
						totalImpuestosRetenidos = totalImpuestosRetenidos.add(retencionConcepto.getImporte());
					}
				}

			}
		}

		if (!mapaTraslados.isEmpty()) {
			for (Map.Entry<String, Comprobante.Impuestos.Traslados.Traslado> elemento : mapaTraslados.entrySet()) {
				if (comprobante.getImpuestos().getTraslados() == null)
					comprobante.getImpuestos().setTraslados(new Comprobante.Impuestos.Traslados());
				comprobante.getImpuestos().getTraslados().getTraslado().add(elemento.getValue());
			}
		}

		if (!mapaRetenciones.isEmpty()) {
			for (Map.Entry<String, Comprobante.Impuestos.Retenciones.Retencion> elemento : mapaRetenciones.entrySet()) {
				if (comprobante.getImpuestos().getRetenciones() == null)
					comprobante.getImpuestos().setRetenciones(new Comprobante.Impuestos.Retenciones());
				comprobante.getImpuestos().getRetenciones().getRetencion().add(elemento.getValue());
			}
		}

		if (comprobante.getImpuestos() != null) {
			if (comprobante.getImpuestos().getTraslados() != null) {
				if (!comprobante.getImpuestos().getTraslados().getTraslado().isEmpty()) {
					if (totalImpuestosTrasladados.intValue() != 0)
						comprobante.getImpuestos().setTotalImpuestosTrasladados(totalImpuestosTrasladados);
				}
			}

			if (comprobante.getImpuestos().getRetenciones() != null) {
				if (!comprobante.getImpuestos().getRetenciones().getRetencion().isEmpty()) {
					if (totalImpuestosRetenidos.intValue() != 0)
						comprobante.getImpuestos().setTotalImpuestosRetenidos(totalImpuestosRetenidos);
				}
			}
		}
	}

	/**
	 * M�todo que genera la adenda del CFDI
	 * 
	 * @param receptorCfdiv33Wrapper
	 * @param tcContribuyente
	 * @param foliador
	 * @return
	 * @throws Exception
	 */
	private Adenda33 crearAsignarAddenda(ReceptorCfdiv33Wrapper receptorCfdiv33Wrapper, TcContribuyente tcContribuyente,
			Foliador foliador) throws Exception {
		Adenda33 adenda33 = new Adenda33();
		try {
			adenda33.setFolio(String.valueOf(foliador.getFolio()));
			adenda33.setSerie(foliador.getSerie());
			DomicilioEmisor domicilioEmisor = new DomicilioEmisor();
//	    domicilioEmisor.setCalle(tcContribuyente.getTcDomicilio().getCalle());
//	    domicilioEmisor.setCodigoPostal(tcContribuyente.getTcDomicilio().getCodigoPostal());
//	    domicilioEmisor.setColonia(tcContribuyente.getTcDomicilio().getColonia());
//	    domicilioEmisor.setEstado(tcContribuyente.getTcDomicilio().getEstado());
//	    domicilioEmisor.setLocalidad(tcContribuyente.getTcDomicilio().getLocalidad());
//	    domicilioEmisor.setMunicipio(tcContribuyente.getTcDomicilio().getMunicipio());
//	    domicilioEmisor.setNumeroExterior(tcContribuyente.getTcDomicilio().getNoExterior());
//	    domicilioEmisor.setNumeroInterior(tcContribuyente.getTcDomicilio().getNoInterior());
//	    domicilioEmisor.setPais(tcContribuyente.getTcDomicilio().getPais());
//	    domicilioEmisor.setReferencia(tcContribuyente.getTcDomicilio().getReferencia());
			adenda33.setDomicilioEmisor(domicilioEmisor);
			DomicilioReceptor domicilioReceptor = new DomicilioReceptor();
			domicilioReceptor.setCalle(receptorCfdiv33Wrapper.getCalle());
			domicilioReceptor.setCodigoPostal(receptorCfdiv33Wrapper.getCodigoPostal());
			domicilioReceptor.setColonia(receptorCfdiv33Wrapper.getColonia());
			domicilioReceptor.setEstado(receptorCfdiv33Wrapper.getEstado());
			domicilioReceptor.setLocalidad(receptorCfdiv33Wrapper.getLocalidad());
			domicilioReceptor.setMunicipio(receptorCfdiv33Wrapper.getMunicipio());
			LOGGER.error("E" + receptorCfdiv33Wrapper.getNoExterior());
			LOGGER.error("I" + receptorCfdiv33Wrapper.getNoInterior());
			domicilioReceptor.setNumeroExterior(receptorCfdiv33Wrapper.getNoExterior());
			domicilioReceptor.setNumeroInterior(receptorCfdiv33Wrapper.getNoInterior());
			domicilioReceptor.setPais(receptorCfdiv33Wrapper.getPais());
			domicilioReceptor.setReferencia(receptorCfdiv33Wrapper.getReferencia());
			LOGGER.error("CALLE:" + domicilioReceptor.getCalle());
			LOGGER.error("ADDExt:" + domicilioReceptor.getNumeroExterior());
			LOGGER.error("ADDInt:" + domicilioReceptor.getNumeroInterior());
			adenda33.setDomicilioReceptor(domicilioReceptor);
		} catch (Exception e) {
			LOGGER.error(
					"Error en metodo crearAsignarAddenda--> " + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
		return adenda33;
	}

	/**
	 * Metodo para obtener importe redondeado hacia arriba con los decimales
	 * correspondientes
	 * 
	 * @param valor1    puede tener cantidad del concepto/base de impuesto
	 * @param valor2    puede tener valorUnitario del concepto/tasaOCuota de
	 *                  impuesto
	 * @param decimales numero de decimales a aplicar
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/comprobante/calcularImporte/{valor1}/{valor2}/{decimales}/.json", method = RequestMethod.POST)
	public BigDecimal calcularImporte(@PathVariable("valor1") BigDecimal valor1,
			@PathVariable("valor2") BigDecimal valor2, @PathVariable("decimales") int decimales) throws Exception {

		return (valor1.multiply(valor2)).setScale(decimales, RoundingMode.HALF_UP);
	}
}