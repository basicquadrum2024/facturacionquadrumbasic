package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.utileria2021.cfdi.clientehttp.wrapper.wscatalogoscfdi40.ReceptorDto;
import com.mx.quadrum.utileria2021.cfdi.clientehttp.wrapper.wscatalogoscfdi40.UsoCfdiDto;
import com.mx.quadrum.utileria2021.cfdi.utileria.UtileriaValidacionUsoCfdi40;


@Controller
@RequestMapping(value = "/usocfdi40")
public class UsoCfdi40Controller implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2576718191267040117L;

	@RequestMapping(value = "/uso_cfdi_por_rfc.json", method = RequestMethod.POST)
	public @ResponseBody List<UsoCfdiDto> obtenerUsosPorRfc(@RequestBody final ReceptorDto receptorDto)
			throws JsonProcessingException {
		String url = ParametroEnum.URL_USOCFDI_LRFC_40.getValue();
		int timeOut = 0;
		String username = ParametroEnum.USUARIO_LRFC_40.getValue();
		String password = ParametroEnum.PASSWORD_LRFC_40.getValue();
		return UtileriaValidacionUsoCfdi40.obtenerUsosCfdiPorRfc(url, timeOut, username, password, receptorDto);
	}
}
