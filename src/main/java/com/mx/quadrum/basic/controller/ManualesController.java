package com.mx.quadrum.basic.controller;

import java.io.Serializable;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@RestController
@RequestMapping(value = "/manualesController")
public class ManualesController  implements Serializable {
	
	  private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.

	
	   private static final long serialVersionUID = 1L;

	@RequestMapping(value = "/manualUsuario.fue")
	    public void test(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String filePDF = "/data/facturacion/basica/manuales/Facturacion_basica_manual.pdf";
		if (filePDF == null) {
		    response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
		    return;
		}
		File file = new File(URLDecoder.decode(filePDF, "UTF-8"));

		if (!file.exists()) {
		    response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
		    return;
		}
		String contentType = request.getServletContext().getMimeType(file.getName());
		if (contentType == null) {
		    contentType = "application/octet-stream";
		}
		escribe(file, response);

	    }
	
	 public void escribe(File file, HttpServletResponse response) throws IOException {
			response.reset();
			response.setBufferSize(DEFAULT_BUFFER_SIZE);
			response.setContentType("application/pdf");
			response.setHeader("Content-Length", String.valueOf(file.length()));
			response.setHeader("Content-Disposition", "filename=\"" + file.getName() + "\"");
			BufferedInputStream input = null;
			BufferedOutputStream output = null;

			try {
			    input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
			    output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);
			    byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
			    int length;
			    while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			    }
			} finally {
			    close(output);
			    close(input);
			}
		    }

		    private static void close(Closeable resource) {
			if (resource != null) {
			    try {
				resource.close();
			    } catch (IOException e) {
				e.printStackTrace();
			    }
			}
		    }
}
