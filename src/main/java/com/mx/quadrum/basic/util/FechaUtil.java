package com.mx.quadrum.basic.util;

import java.io.File;
import java.util.List;

/**
 * 
 * Entidad FechaUtil
 *
 */
public class FechaUtil {
	private String fechainicio;
	private String fechaFIn;
	private Integer pagina;
	private Integer bloque;
	private String rfcReceptor;
	private String uuid;
	private String correo;
	private String status;
	private String tipoComprobante;
	private double subtotal = 0;
	private double totalIva = 0;
	private double total = 0;
	private File fileKey;
	private File FileCer;
	private String folio;
	private String serie;
	private Integer numeroKiosco;
	private String rfcEmisor;
	private List<String> listacorreos;

	public String getFechainicio() {
		return fechainicio;
	}

	public void setFechainicio(String fechainicio) {
		this.fechainicio = fechainicio;
	}

	public String getFechaFIn() {
		return fechaFIn;
	}

	public void setFechaFIn(String fechaFIn) {
		this.fechaFIn = fechaFIn;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public Integer getBloque() {
		return bloque;
	}

	public void setBloque(Integer bloque) {
		this.bloque = bloque;
	}

	public String getRfcReceptor() {
		return rfcReceptor;
	}

	public void setRfcReceptor(String rfcReceptor) {
		this.rfcReceptor = rfcReceptor;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotalIva() {
		return totalIva;
	}

	public void setTotalIva(double totalIva) {
		this.totalIva = totalIva;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public File getFileKey() {
		return fileKey;
	}

	public void setFileKey(File fileKey) {
		this.fileKey = fileKey;
	}

	public File getFileCer() {
		return FileCer;
	}

	public void setFileCer(File fileCer) {
		FileCer = fileCer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public List<String> getListacorreos() {
		return listacorreos;
	}

	public void setListacorreos(List<String> listacorreos) {
		this.listacorreos = listacorreos;
	}

	public Integer getNumeroKiosco() {
		return numeroKiosco;
	}

	public void setNumeroKiosco(Integer numeroKiosco) {
		this.numeroKiosco = numeroKiosco;
	}

	public String getRfcEmisor() {
		return rfcEmisor;
	}

	public void setRfcEmisor(String rfcEmisor) {
		this.rfcEmisor = rfcEmisor;
	}
	
	

}
