package com.mx.quadrum.basic.util;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.mx.quadrum.basic.adenda.Adenda33;
import com.mx.quadrum.cfdi.complementos.ine.v11.schema.INE;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v11.schema.TimbreFiscalDigital;
import com.mx.quadrum.cfdi.utilerias.ConceptoReporte;
import com.mx.quadrum.cfdi.utilerias.ConvertidorLetras;
import com.mx.quadrum.cfdi.utilerias.ListaSubReportes;
import com.mx.quadrum.cfdi.utilerias.PojoReporte;
import com.mx.quadrum.cfdi.utilerias.QrGenerator;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.v33.schema.Comprobante;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.CfdiRelacionados.CfdiRelacionado;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Conceptos.Concepto;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Impuestos.Traslados.Traslado;

public class GenerarPdf33 {

    public static final Logger LOGGER = Logger.getLogger(GenerarPdf33.class);

    /**
     * Metodo encargado de crear un archivo pdf y retornar un arreglo de bytes
     * del archivo creado
     * 
     * @param listaCfdi
     *            tipo ComprobanteWrapper33
     * @param tipo
     * @param fiscalDigital
     *            v11
     * @param adenda
     *            v33
     * @return
     */
    public static byte[] creaPDFByteArray(Comprobante comprobante, TimbreFiscalDigital fiscalDigital, Adenda33 adenda) {
	byte respuesta[] = null;
	List<PojoReporte> pam = new ArrayList<PojoReporte>();
	try {

	    Map<String, Object> map = null;
	    PojoReporte padre = null;
	    padre = new PojoReporte();
	    map = crearMapaParametros(comprobante, fiscalDigital, adenda);
	    List<ConceptoReporte> listaReporte = new ArrayList<ConceptoReporte>();
	    convertirConceptos(comprobante.getConceptos().getConcepto(), listaReporte);
	    ListaSubReportes listaSubreportes = new ListaSubReportes();
	    listaSubreportes.setConceptosList(listaReporte);
	    padre.addObjeto(listaSubreportes);
	    asignarUrlReporteSubReporte(padre, map);
	    padre.setDatos(map);
	    pam.add(padre);

	    JasperReport reporte = (JasperReport) JRLoader.loadObjectFromFile(ParametroEnum.JASPER_PADREV33.getValue());
	    JasperPrint jasperPrint = null;
	    jasperPrint = JasperFillManager.fillReport(reporte, null, new JRBeanCollectionDataSource(pam));

	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    JRExporter exporter = new JRPdfExporter();
	    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
	    exporter.exportReport();
	    respuesta = baos.toByteArray();

	} catch (Exception e) {
	    e.printStackTrace();
	}
	return respuesta;
    }

    /**
     * Metodo encargado de crear una Imagen QR .png en un arreglo de bytes
     * 
     * @param comprobante
     * @param fiscalDigital
     * @return
     * @throws Exception
     */
    private static byte[] crearQrByte(Comprobante comprobante, TimbreFiscalDigital fiscalDigital) throws Exception {
	byte respuesta[] = null;
	String cadena = null;

	if (comprobante.getEmisor().getRfc() != null && comprobante.getReceptor().getRfc() != null
		&& comprobante.getTotal() != null && fiscalDigital.getUUID() != null) {
	    cadena = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx " + "&id="
		    + fiscalDigital.getUUID() + "&re=" + comprobante.getEmisor().getRfc() + "&rr="
		    + comprobante.getReceptor().getRfc() + "&tt=" + comprobante.getTotal() + "&fe="
		    + Utilerias.ultimosCaracteresSelloDigital(fiscalDigital.getSelloCFD());

	}
	if (cadena != null) {
	    respuesta = QrGenerator.generateQrByteArray(cadena, 140, 140);
	}
	return respuesta;
    }

    /**
     * Metodo encargado de crear un mapa de objetos el cual sera enviado a una
     * plantilla .jasper, el mapa sera utilizado para facturas normales
     * 
     * @param comprobante
     *            v3.3
     * @param fiscalDigital
     *            v1.1
     * @param adenda
     *            v3.3
     * @return
     * @throws Exception
     */
    private static Map<String, Object> crearMapaParametros(Comprobante comprobante, TimbreFiscalDigital fiscalDigital,
	    Adenda33 adenda) throws Exception {
	Map<String, Object> mapa = new HashMap<String, Object>();
	StringBuilder calleReceptor = new StringBuilder();
	byte pathImgQr[] = null;
	pathImgQr = crearQrByte(comprobante, fiscalDigital);
	if (pathImgQr != null) {
	    mapa.put("QR", pathImgQr);
	}
	if (adenda != null && adenda.getFolio() != null) {
	    mapa.put("folio", adenda.getFolio());
	}
	if (adenda != null && adenda.getSerie() != null) {
	    mapa.put("serie", adenda.getSerie());
	}

	mapa.put("noCertificadoSAT", fiscalDigital.getNoCertificadoSAT());
	mapa.put("fecha", Utilerias.YYYYMMDD_HHMMSS.format(comprobante.getFecha()));
	mapa.put("FechaTimbrado", Utilerias.YYYYMMDD_HHMMSS.format(fiscalDigital.getFechaTimbrado()));
	mapa.put("UUID", fiscalDigital.getUUID());
	mapa.put(
		"regimenFiscal_emisor",
		comprobante.getEmisor().getRegimenFiscal()
			+ " "
			+ (DescripcionCatalogos.getValorRegimenFiscal(comprobante.getEmisor().getRegimenFiscal()))
				.toUpperCase());
	mapa.put("rfc_emisor", comprobante.getEmisor().getRfc());
	mapa.put("nombre_emisor", StringUtils.stripToEmpty(comprobante.getEmisor().getNombre()));
	mapa.put("domicilioEmisor", crearDomicilioEmisor(adenda));

	StringBuilder emisor_calle = new StringBuilder();
	if (adenda != null && adenda.getDomicilioEmisor() != null) {
	    emisor_calle.append(StringUtils.stripToEmpty(adenda.getDomicilioEmisor().getCalle()));
	    mapa.put("cp_emisor", StringUtils.trimToEmpty(adenda.getDomicilioEmisor().getCodigoPostal()));
	    mapa.put("colonia_emisor", StringUtils.trimToEmpty(adenda.getDomicilioEmisor().getColonia()));
	    mapa.put("municipio_emisor", StringUtils.trimToEmpty(adenda.getDomicilioEmisor().getMunicipio()));
	    mapa.put("estado_emisor", StringUtils.trimToEmpty(adenda.getDomicilioEmisor().getEstado()));
	}
	if (adenda != null && adenda.getDomicilioEmisor() != null) {
	    emisor_calle.append(" " + StringUtils.stripToEmpty(adenda.getDomicilioEmisor().getNumeroExterior()));
	}
	mapa.put("calle_emisor", StringUtils.stripToEmpty(emisor_calle.toString()));

	if (comprobante.getReceptor().getNombre() != null) {
	    mapa.put("nombre_receptor", StringUtils.stripToEmpty(comprobante.getReceptor().getNombre()));
	}

	if (adenda != null && adenda.getDomicilioReceptor() != null) {
	    calleReceptor.append(StringUtils.stripToEmpty(adenda.getDomicilioReceptor().getCalle()));
	    mapa.put("cp_receptor", StringUtils.trimToEmpty(adenda.getDomicilioReceptor().getCodigoPostal()));
	    mapa.put("colonia_receptor", StringUtils.trimToEmpty(adenda.getDomicilioReceptor().getColonia()));
	    mapa.put("municipio_receptor", StringUtils.trimToEmpty(adenda.getDomicilioReceptor().getMunicipio()));
	    mapa.put("estado_receptor", StringUtils.trimToEmpty(adenda.getDomicilioReceptor().getEstado()));
	}
	if (adenda != null && adenda.getDomicilioReceptor().getCodigoPostal() == null) {
	    String codigoPostal = " ";
	    mapa.put("cp_receptor", StringUtils.stripToEmpty(codigoPostal));
	}
	if (adenda != null && adenda.getDomicilioReceptor() != null
		&& adenda.getDomicilioReceptor().getNumeroExterior() != null) {
	    calleReceptor.append(", No. ext. "
		    + StringUtils.stripToEmpty(adenda.getDomicilioReceptor().getNumeroExterior()));
	}
	if (adenda != null && adenda.getDomicilioReceptor() != null
		&& adenda.getDomicilioReceptor().getNumeroInterior() != null) {
	    calleReceptor.append(", No. int. "
		    + StringUtils.stripToEmpty(adenda.getDomicilioReceptor().getNumeroInterior()));
	}
	mapa.put("rfc_receptor", comprobante.getReceptor().getRfc());
	mapa.put(
		"usocfdi",
		comprobante.getReceptor().getUsoCFDI() + " "
			+ (DescripcionCatalogos.getValorUsoCFdi(comprobante.getReceptor().getUsoCFDI())).toUpperCase());
	mapa.put("calle_receptor", StringUtils.stripToEmpty(calleReceptor.toString().toUpperCase()));
	mapa.put("subTotal", "$" + Utilerias.formateador.format(comprobante.getSubTotal().doubleValue()));
	if (comprobante.getDescuento() != null) {
	    mapa.put("descuentoTotal", "$" + comprobante.getDescuento());
	}
	mapa.put("total", "$" + Utilerias.formateador.format(comprobante.getTotal().doubleValue()));
	if (comprobante.getImpuestos() != null) {
	    agregarImpuestos(mapa, comprobante.getImpuestos());
	}
	mapa.put("TimbreFiscalDigital_selloCFD", fiscalDigital.getSelloCFD());
	mapa.put("TimbreFiscalDigital_selloSAT", fiscalDigital.getSelloSAT());

	mapa.put(
		"cadenaOriginalSat",
		"||" + comprobante.getVersion() + "|" + fiscalDigital.getUUID() + "|"
			+ Utilerias.YYYYMMDD_HHMMSS.format(fiscalDigital.getFechaTimbrado()) + "|"
			+ fiscalDigital.getSelloCFD() + "|" + fiscalDigital.getNoCertificadoSAT() + "||");
	mapa.put("noCertificado", fiscalDigital.getNoCertificadoSAT());
	if (comprobante.getCondicionesDePago() != null) {
	    mapa.put("condicionesDePago", comprobante.getCondicionesDePago().toUpperCase());
	} else {
	    mapa.put("condicionesDePago", "-");
	}

	if (comprobante.getFormaPago() != null) {
	    mapa.put(
		    "formaDePago",
		    comprobante.getFormaPago() + " "
			    + (DescripcionCatalogos.getValorFormaPago(comprobante.getFormaPago())).toUpperCase());
	}

	if (comprobante.getMetodoPago() != null) {
	    mapa.put(
		    "metodoPago",
		    comprobante.getMetodoPago() + " "
			    + (DescripcionCatalogos.getValorMetodoPago(comprobante.getMetodoPago())).toUpperCase());
	}

	if (comprobante.getTipoDeComprobante() != null) {
	    mapa.put("tipoComprobante",
		    (DescripcionCatalogos.getValorTipoComprobante(comprobante.getTipoDeComprobante())).toUpperCase());
	}

	mapa.put("LugarExpedicion", crearDomicilioEmisor(adenda));
	mapa.put("totalLetra", ConvertidorLetras.convertir("" + comprobante.getTotal(), true,"MXN"));
	mapa.put("NumCtaPago", "NO IDENTIFICADO");
	mapa.put("nota", "");
	if (comprobante.getCfdiRelacionados() != null) {
	    mapa.put(
		    "tipo_relacion",
		    comprobante.getCfdiRelacionados().getTipoRelacion()
			    + " "
			    + DescripcionCatalogos.getValorTipoRelacion(comprobante.getCfdiRelacionados()
				    .getTipoRelacion()));
	    CfdiRelacionado cfdiRelacionado = comprobante.getCfdiRelacionados().getCfdiRelacionado().get(0);
	    mapa.put("uuid_relacionado", cfdiRelacionado.getUUID());
	}
	mapa.put("complemento", "INE");
	mapa.put("tipo_proceso", "ORDINARIO");
	mapa.put("tipo_comite", "FERERAL");
	mapa.put("id_contabilidad", "12312");
	mapa.put("clave_entidad", "mex");
	mapa.put("ambito", "local");
	if (obtenerINE(comprobante) != null) {
	    mapaComplemento(obtenerINE(comprobante), mapa);
	}

	return mapa;
    }

    /**
     * Metedo para llenar las variables del complemento INE para crear el pdf
     * @param ine
     * @param mapa
     */
    private static void mapaComplemento(INE ine, Map<String, Object> mapa) {

	mapa.put("complemento", "INE");
	mapa.put("tipo_proceso", ine.getTipoProceso());
	mapa.put("tipo_comite", ine.getTipoComite());
	mapa.put("id_contabilidad", ine.getIdContabilidad());
	mapa.put("clave_entidad", ine.getEntidad().get(0).getClaveEntidad());
	mapa.put("ambito", ine.getEntidad().get(0).getAmbito());

    }

    /**
     * Metodo para obtener el objeto del INE
     * @param comprobante
     * @return
     * @throws Exception
     */
    public static INE obtenerINE(Comprobante comprobante) throws Exception {
	INE ine = null;

	ine = (INE) UtileriasCfdi.getObjectoComplemento(comprobante.getComplemento().getAny(), INE.class);

	return ine;
    }

    /**
     * M�todo encargado de devolver el domicilio del Emisor
     * 
     * @param adenda33
     *            v3.3
     * @return
     */
    public static String crearDomicilioEmisor(Adenda33 adenda33) {
	StringBuilder domicilio = new StringBuilder();

	if (adenda33 != null && adenda33.getDomicilioEmisor() != null) {
	    if (adenda33.getDomicilioEmisor().getCalle() != null) {
		domicilio.append(StringUtils.stripToEmpty(adenda33.getDomicilioEmisor().getCalle()) + ", ");
	    }
	    if (adenda33.getDomicilioEmisor().getNumeroInterior() != null) {
		domicilio.append("No. int. "
			+ StringUtils.stripToEmpty(adenda33.getDomicilioEmisor().getNumeroInterior()) + ", ");
	    }
	    if (adenda33.getDomicilioEmisor().getNumeroExterior() != null) {
		domicilio.append("No. ext. "
			+ StringUtils.stripToEmpty(adenda33.getDomicilioEmisor().getNumeroExterior()) + ", ");
	    }
	    if (adenda33.getDomicilioEmisor().getColonia() != null) {
		domicilio.append(StringUtils.stripToEmpty(adenda33.getDomicilioEmisor().getColonia()) + ", ");
	    }
	    if (adenda33.getDomicilioEmisor().getCodigoPostal() != null) {
		domicilio.append("C. P. " + StringUtils.stripToEmpty(adenda33.getDomicilioEmisor().getCodigoPostal())
			+ ", ");
	    }
	    if (adenda33.getDomicilioEmisor().getMunicipio() != null) {
		domicilio.append(StringUtils.stripToEmpty(adenda33.getDomicilioEmisor().getMunicipio()) + ", ");
	    }
	    if (adenda33.getDomicilioEmisor().getEstado() != null) {
		domicilio.append(StringUtils.stripToEmpty(adenda33.getDomicilioEmisor().getEstado()) + ", ");
	    }
	    if (adenda33.getDomicilioEmisor().getPais() != null) {
		domicilio.append(StringUtils.stripToEmpty(adenda33.getDomicilioEmisor().getPais()) + ".");
	    }
	}
	return domicilio.toString();
    }

    /**
     * Metodo encargado de convertir y agregar objetos del tipo
     * Comprobante.Conceptos.Concepto a ConceptoReporte
     * 
     * @param lista
     *            de conceptos v33
     * @param listaReporte
     */
    public static void convertirConceptos(List<Concepto> conceptos, List<ConceptoReporte> listaReporte) {
	for (Concepto concepto : conceptos) {
	    ConceptoReporte conceptoReporte = new ConceptoReporte();
	    if (concepto.getCantidad() != null) {
		conceptoReporte.setCantidad(String.valueOf(concepto.getCantidad()));
	    }
	    if (concepto.getUnidad() != null) {
		conceptoReporte.setUnidad(String.valueOf(concepto.getUnidad()));
	    }

	    conceptoReporte.setDescripcion(StringUtils.stripToEmpty(String.valueOf(concepto.getDescripcion())));
	    conceptoReporte.setValorUnitario("$" + concepto.getValorUnitario());
	    conceptoReporte.setImporte("$" + concepto.getImporte());
	    conceptoReporte.setAduana("");
	    if (concepto.getNoIdentificacion() != null) {
		conceptoReporte.setNoIdentificacion(String.valueOf(concepto.getNoIdentificacion()));
	    }

	    conceptoReporte.setNumero("");
	    conceptoReporte.setFecha("");
	    if (concepto.getDescuento() != null) {
		conceptoReporte.setDescuento("$" + concepto.getDescuento());
	    }
	    conceptoReporte.setClaveprodserv(concepto.getClaveProdServ());
	    conceptoReporte.setClaveUnidad(concepto.getClaveUnidad());

	    listaReporte.add(conceptoReporte);
	}
    }

    /**
     * Metodo encargado de agreagar los respectivos impuestos Traslados y
     * Retenciones al mapa de objetos
     * 
     * @param mapa
     * @param impuestos
     * @throws Exception
     */
    public static void agregarImpuestos(Map<String, Object> mapa,
	    com.mx.quadrum.cfdi.v33.schema.Comprobante.Impuestos impuestos) throws Exception {
	double ivaRetenido = 0;
	double isrRetenido = 0;
	double ivaTrasladado = 0;
	if (impuestos.getRetenciones() != null) {
	    List<com.mx.quadrum.cfdi.v33.schema.Comprobante.Impuestos.Retenciones.Retencion> retenciones = impuestos
		    .getRetenciones().getRetencion();
	    if (retenciones != null && !retenciones.isEmpty()) {
		for (com.mx.quadrum.cfdi.v33.schema.Comprobante.Impuestos.Retenciones.Retencion retencion : retenciones) {
		    if (retencion.getImpuesto() != null && retencion.getImpuesto().equalsIgnoreCase("IVA")) {
			ivaRetenido = ivaRetenido + retencion.getImporte().doubleValue();
		    } else if (retencion.getImporte() != null && retencion.getImpuesto().equalsIgnoreCase("ISR")) {
			isrRetenido = isrRetenido + retencion.getImporte().doubleValue();
		    }
		}
	    }
	}
	if (impuestos.getTraslados() != null) {
	    List<Traslado> traslados = impuestos.getTraslados().getTraslado();
	    if (traslados != null && !traslados.isEmpty()) {
		for (Traslado traslado : traslados) {
		    ivaTrasladado = ivaTrasladado + traslado.getImporte().doubleValue();
		}
	    }
	}
	mapa.put("ivatrasladado", "$" + Utilerias.formateador.format(ivaTrasladado));
	mapa.put("ivaretenido", "$" + Utilerias.formateador.format(ivaRetenido));
	mapa.put("isrRetenido", "$" + Utilerias.formateador.format(isrRetenido));

    }

    /**
     * Metodo encargado de asignar jasper
     * 
     * @param pojoReporte
     * @param map
     * @throws Exception
     */

    public static void asignarUrlReporteSubReporte(PojoReporte pojoReporte, Map<String, Object> map) throws Exception {
	pojoReporte.setUrlReporte(ParametroEnum.JASPER_REPORTEV33.getValue());
	map.put("dirSubreporte", ParametroEnum.RUTA_SUBREPORTEV33.getValue());
    }

}