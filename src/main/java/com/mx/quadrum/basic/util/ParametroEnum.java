package com.mx.quadrum.basic.util;

/**
 * 
 * Entidad de ParametroEnum
 *
 */
public enum ParametroEnum {

    PISTAS(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "Pistas")), MAILPORT(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "MailPort")), MAILUSERNAME(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "MailUsername")), MAILPASSWORD(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "MailPassword")), PROPCCER(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "PropcCer")), PROPPEM(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "PropPem")), FILEPDF(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "FilePDF")), FILEARCHIVOS(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "FileArchivos")), PATCHQR(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "PatchQR")), PATCHPDF(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "PatchPDF")), PATCHJASPERS(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "PatchJaspers")), DIRECCIONWSTIMBRADO(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "DireccionWSTimbrado")), DIRECCIONWSCANCELACION(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "DireccionWSCancelacion")), USUARIOWS(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "UsuarioWS")), PASSWORDWS(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "PasswordWS")), PANTILLA_BASIC(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "PlantillaBasic")), REPORTE_JASPER_CARTA(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "ReporteJasperCarta")), FACTURAS_QR_BASIC(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "FacturaQrBasic")), FACTURAS_PDF_BASIC(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "FacturaPdfBasic")), DIRECCION_GUARDADO_XML(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "DireccionGuardadoXml")), DIRECCION_CANCELACION_XML(
	    ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "DireccionCanceladoXml")), JASPER_PADREV33(
	    ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "ReportePadrev33")), JASPER_REPORTEV33(
	    ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "Plantillav33")), RUTA_SUBREPORTEV33(
	    ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "JaspersCfdi33")), DIRRECCION_APP(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "UrlApp")), TIEMPO_BLOQ_USR(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "MinutosBloqueoUsr")), 
    CORREO_SOPORTE(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "CorreoSoporte")),
    USO_HORARIO(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "UsoHorario")),
    CSSCONVENIOS(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "RutaConveniosCSS")),
    RUTACONTRATOS(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA + "RutaContratos")),
    PRIVACIDAD(ParametroApp
	    .getValorParametro(Utilerias.CONTEXTO_BASICA + "Privacidad")), URLREGISTRO(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"UrlRegistro")), CORREO_BUZON(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"CorreoBuzonSugerenciasQuejas")),
    RUTA_CERTIFICADOS_PADRE(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"RutaCertificadosPadre")),
    RUTA_CER_REP_LEG(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"RutaCertificadoRepQuadrum")),
    RUTA_KEY_REP_LEG(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"RutaLlaveRepQuadrum")),
    VALOR_PASWW_LLAVE_RL(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"ValorLLaveRepLeg")),
    SERVICIO_RMI_SAT(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"RmiCatalogosSAT")),
    NOMBRE_BEAN_REMOTO_SAT(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"NombreBeanRemotoSAT")),
    SERVICIO_RMI_SAT_V4(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"RmiCatalogosSATV4")),
    NOMBRE_BEAN_REMOTO_SAT_V4(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"NombreBeanRemotoSATV4")),
    JASPER_V4(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"JASPERV4")),
    URL_REGIMENFISCAL_40(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"UrlRegimenFiscal40")),
    USUARIO_LRFC_40(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"UsuarioLrfc40")),
    PASSWORD_LRFC_40(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"PasswordLrfc40")),
    URL_USOCFDI_LRFC_40(ParametroApp.getValorParametro(Utilerias.CONTEXTO_BASICA+"UrlUsosCfdi40"));

    private final String value;

    private ParametroEnum(String value) {
	this.value = value;
    }

    public String getValue() {
	return this.value;
    }
}
