package com.mx.quadrum.basic.util;

/**
 * 
 * Clase de ExcepcionesFacturacionBasica
 *
 */
public class ExcepcionesFacturacionBasica extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 6904688753014730366L;

    private String errorMessage;

    public String getErrorMessage() {
	return errorMessage;
    }

    public ExcepcionesFacturacionBasica(String errorMessage) {
	super(errorMessage);
	this.errorMessage = errorMessage;
    }

    public ExcepcionesFacturacionBasica() {
	super();
    }

}
