package com.mx.quadrum.basic.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class UtileriaException {

    /**
     * Se encarga de convertir todo el stack mandado por la exception en una
     * variable de tipo String
     * 
     * @param exception
     * @return
     */
    public static String imprimeStackError(Exception exception) {
	StringWriter builder = new StringWriter();
	exception.printStackTrace(new PrintWriter(builder));
	return builder.toString();
    }
}
