package com.mx.quadrum.basic.util;

public class DescripcionCatalogos {

	/**
	 * Metodo para obtener el valor del regimen fiscal
	 * @param clave
	 * @return String
	 */
    public static String getValorRegimenFiscal(String clave) {
	switch (clave) {
	case "601":
	    return "General de Ley Personas Morales";
	case "603":
	    return "Personas Morales con Fines no Lucrativos";
	case "605":
	    return "Sueldos y Salarios e Ingresos Asimilados a Salarios";
	case "606":
	    return "Arrendamiento";
	case "608":
	    return "Dem�s ingresos";
	case "609":
	    return "Consolidaci�n";
	case "610":
	    return "Residentes en el Extranjero sin Establecimiento Permanente en M�xico";
	case "611":
	    return "Ingresos por Dividendos (socios y accionistas)";
	case "612":
	    return "Personas F�sicas con Actividades Empresariales y Profesionales";
	case "614":
	    return "Ingresos por intereses";
	case "616":
	    return "Sin obligaciones fiscales";
	case "620":
	    return "Sociedades Cooperativas de Producci�n que optan por diferir sus ingresos";
	case "621":
	    return "Incorporaci�n Fiscal";
	case "622":
	    return "Actividades Agr�colas, Ganaderas, Silv�colas y Pesqueras";
	case "623":
	    return "Opcional para Grupos de Sociedades";
	case "624":
	    return "Coordinados";
	case "628":
	    return "Hidrocarburos";
	case "607":
	    return "R�gimen de Enajenaci�n o Adquisici�n de Bienes";
	case "629":
	    return "De los Reg�menes Fiscales Preferentes y de las Empresas Multinacionales";
	case "630":
	    return "Enajenaci�n de acciones en bolsa de valores";
	case "615":
	    return "R�gimen de los ingresos por obtenci�n de premios";
	default:
	    return "Regimen no definido";
	}
    }

    /**
     * Metodo para obtener la clave de forma de pago
     * @param clave
     * @return String
     */
    public static String getValorFormaPago(String clave) {
	switch (clave) {
	case "01":
	    return "Efectivo";
	case "02":
	    return "Cheque nominativo";
	case "03":
	    return "Transferencia electr�nica de fondos";
	case "04":
	    return "Tarjeta de cr�dito";
	case "05":
	    return "Monedero electr�nico";
	case "06":
	    return "Dinero electr�nico";
	case "08":
	    return "Vales de despensa";
	case "12":
	    return "Daci�n en pago";
	case "13":
	    return "Pago por subrogaci�n";
	case "14":
	    return "Pago por consignaci�n";
	case "15":
	    return "Condonaci�n";
	case "17":
	    return "Compensaci�n";
	case "23":
	    return "Novaci�n";
	case "24":
	    return "Confusi�n";
	case "25":
	    return "Remisi�n de deuda";
	case "26":
	    return "Prescripci�n o caducidad";
	case "27":
	    return "A satisfacci�n del acreedor";
	case "28":
	    return "Tarjeta de d�bito";
	case "29":
	    return "Tarjeta de servicios";
	case "99":
	    return "Por definir";

	default:
	    return "Forma de pago no definida";
	}
    }

    /**
     * Metodo para obtener clave de metodo de pago
     * @param clave
     * @return String
     */
    public static String getValorMetodoPago(String clave) {
	switch (clave) {
	case "PUE":
	    return "Pago en una sola exhibici�n";
	case "PIP":
	    return "Pago inicial y parcialidades";
	case "PPD":
	    return "Pago en parcialidades o diferido";
	default:
	    return "Metodo de pago no definido";
	}
    }

    /**
     * Metodo para obtener clave de tipo comprobante
     * @param clave
     * @return String
     */
    public static String getValorTipoComprobante(String clave) {
	switch (clave) {
	case "I":
	    return "Ingreso";
	case "E":
	    return "Egreso";
	case "T":
	    return "Traslado";
	case "N":
	    return "N�mina";
	case "P":
	    return "Pago";
	default:
	    return "Tipo de comproabnte no definido";
	}
    }

    /**
     * Metodo para obtener clave uso de cfdi
     * @param clave
     * @return String
     */
    public static String getValorUsoCFdi(String clave) {
	switch (clave) {
	case "G01":
	    return "Adquisici�n de mercancias";
	case "G02":
	    return "Devoluciones, descuentos o bonificaciones";
	case "G03":
	    return "Gastos en general";
	case "I01":
	    return "Construcciones";
	case "I02":
	    return "Mobilario y equipo de oficina por inversiones";
	case "I03":
	    return "Equipo de transporte";
	case "I04":
	    return "Equipo de computo y accesorios";
	case "I05":
	    return "Dados, troqueles, moldes, matrices y herramental";
	case "I06":
	    return "Comunicaciones telef�nicas";
	case "I07":
	    return "Comunicaciones satelitales";
	case "I08":
	    return "Otra maquinaria y equipo";
	case "D01":
	    return "Honorarios m�dicos, dentales y gastos hospitalarios.";
	case "D02":
	    return "Gastos m�dicos por incapacidad o discapacidad";
	case "D03":
	    return "Gastos funerales.";
	case "D04":
	    return "Donativos.";
	case "D05":
	    return "Intereses reales efectivamente pagados por cr�ditos hipotecarios (casa habitaci�n).";
	case "D06":
	    return "Aportaciones voluntarias al SAR.";
	case "D07":
	    return "Primas por seguros de gastos m�dicos.";
	case "D08":
	    return "Gastos de transportaci�n escolar obligatoria.";
	case "D09":
	    return "Dep�sitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.";
	case "D10":
	    return "Pagos por servicios educativos (colegiaturas)";
	case "P01":
	    return "Por definir";
	default:
	    return "No definido";
	}
    }

    /**
     * Metodo para obtener valor de tipo relacion
     * @param clave
     * @return String
     */
    public static String getValorTipoRelacion(String clave) {
	switch (clave) {
	case "01":
	    return "Nota de cr�dito de los documentos relacionados.";
	case "02":
	    return "Nota de d�bito de los documentos relacionados.";
	case "03":
	    return "Devoluci�n de mercanc�a sobre facturas o traslados previos.";
	case "04":
	    return "Sustituci�n de los CFDI previos.";
	case "05":
	    return "Traslados de mercancias facturados previamente.";
	case "06":
	    return "Factura generada por los traslados previos.";
	case "07":
	    return "CFDI por aplicaci�n de anticipo.";

	default:
	    return "Tipo de relaci�n no definido";
	}
    }

}
