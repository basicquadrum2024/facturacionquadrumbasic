package com.mx.quadrum.basic.util;
/**
 * 
 * Entidad MensajeExcepcion
 *
 */
public class MensajeExcepcion {
    private int elemento;
    private String mensaje;

    public int getElemento() {
	return elemento;
    }

    public void setElemento(int elemento) {
	this.elemento = elemento;
    }

    public void setMensaje(String mensaje) {
	this.mensaje = mensaje;
    }

    public String getMensaje() {
	return mensaje;
    }
}