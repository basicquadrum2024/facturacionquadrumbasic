package com.mx.quadrum.basic.util;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Clase auxiliar que permite reutilzar los metodos por todas las demas clases
 * del sistema de información
 */
public class Anexo20 {
    public static DecimalFormat formatoQr = new DecimalFormat("###########0000000000.000000");
    public static DecimalFormat formateador = new DecimalFormat("###,###.00");
    public static DecimalFormat formatoTotales = new DecimalFormat("###########################################0.00");
    public static DecimalFormat formatoImporte = new DecimalFormat("######################################0.00####");
    public static SimpleDateFormat formatoFechaTimbrado = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static DateFormat YYYYMMDD_HHMMSS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("es", "MX"));
    public static DateFormat formatoGuardarOrdenados = new SimpleDateFormat("dd-MMMM-yyyy-HH", new Locale("es", "MX"));

    public static DateFormat formatoLetraMes = new SimpleDateFormat("MMMM", new Locale("es", "MX"));
    public static DateFormat formatoLetraAnio = new SimpleDateFormat("yyyy", new Locale("es", "MX"));

    /**
     * Método que carga los estados en los formularios correspondientes
     *
     * @return Lista de SelectItem con los Estados
     */
    public static String[] listaEstados() {
	String[] estados = { "[ SELECCIONAR ESTADO ]", "AGUASCALIENTES", "BAJA CALIFORNIA SUR", "BAJA CALIFORNIA NORTE",
		"CAMPECHE", "CHIAPAS", "CHIHUAHUA", "COAHUILA", "COLIMA", "CIUDAD DE MEXICO", "DURANGO",
		"ESTADO DE MEXICO", "GUANAJUATO", "GUERRERO", "HIDALGO", "JALISCO", "MICHOACAN", "MORELOS", "NAYARIT",
		"NUEVO LEON", "OAXACA", "PUEBLA", "QUERETARO", "QUINTANA ROO", "SAN LUIS POTOSI", "SINALOA", "SONORA",
		"TABASCO", "TAMAULIPAS", "TLAXCALA", "VERACRUZ", "YUCATAN", "ZACATECAS" };
	return estados;
    }

    /**
     * Método que carga las personas morales en los formularios
     * correspondientes
     *
     * @return Lista de SelectItem con las personas morales fecha de
     *         modificacion 12/06/2013
     */
    public static String[] listaPersonaMoral() {
	String[] regimen = { "Regimen General de Personas Morales", "Regimen Simplificado de Personas Morales",
		"Regimen de Personas Morales con Fines no Lucrativos", "Asociaciones Religiosas",
		"Otro Régimen Fiscal" };
	return regimen;
    }

    public static String[] listaTrampa() {
	String[] regimen = { "Regimen General de Personas Morales", "Regimen Simplificado de Personas Morales",
		"Regimen de Personas Morales con Fines no Lucrativos", "Asociaciones Religiosas", "Otro Regimen Fiscal",
		"Actividades Empresariales y Profesionales", "Regimen de Peque�os Contribuyentes",
		"Por Salarios y Prestacion de servicios Personales Subordinados", "Arrendamiento",
		"Enajenacion de Bienes", "Adquisicion de Bienes", "Dividendos", "Intereses", "Premios",
		"De mas Ingresos", "Otro Regimen Fiscal" };
	return regimen;
    }

    /**
     * Método que carga las personas fisicas en los formularios
     * correspondientes
     *
     * @return Lista de SelectItem con las personas fisicas fecha de
     *         modificacion 12/06/2013
     */
    public static String[] listaPersonaFisica() {
	String[] regimen = { "Actividades Empresariales y Profesionales", "Regimen de Peque�os Contribuyentes",
		"Por Salarios y Prestacion de servicios Personales Subordinados", "Arrendamiento",
		"Enajenacion de Bienes", "Adquisicion de Bienes", "Dividendos", "Intereses", "Premios",
		"De mas Ingresos", "Otro Regimen Fiscal" };

	return regimen;
    }

    /**
     * Método que carga el regimen fiscal en los formularios correspondientes
     *
     * @return Lista de SelectItem con el regimen fiscal fecha de modificacion
     *         12/06/2013
     */
    public static String[] listaRegimenFiscal() {
	String[] regimenFiscal = { "REGIMEN FISCAL", "REGIMEN GENERAL DE PERSONAS MORALES",
		"PERSONAS MORALES CON FINES NO LUCRATIVOS", "SIMPLIFICADO", "OTRO REGIMEN" };
	return regimenFiscal;
    }

    public static String secuencias(String cadena) {
	String subcadena = "&amp;";
	int indice = cadena.indexOf(subcadena);
	if (!(indice != -1)) {
	    cadena = cadena.replace("&", "&");
	}
	cadena = cadena.replace(String.valueOf('"'), "&quot;");
	cadena = cadena.replace("<", "&lt;");
	cadena = cadena.replace(">", "&gt;");
	cadena = cadena.replace("'", "&apos;");
	return cadena;
    }

    /**
     * Método que permite el reemplazo de caractes acentuados nos permitidos en
     * XML
     *
     * @param input
     *            Texto digitado por usuario
     * @return Valor sin caracteres permitidos
     */
    public static String formatCadena(String input) {
	// input = input.toUpperCase();
	if (input != null) {
	    // input.toUpperCase();
	    for (int i = 0; i < input.length(); i++) {
		if (input.charAt(i) == 193) {
		    input = input.replace(input.charAt(i), 'A');
		}
		if (input.charAt(i) == 201) {
		    input = input.replace(input.charAt(i), 'E');
		}
		if (input.charAt(i) == 205) {
		    input = input.replace(input.charAt(i), 'I');
		}
		if (input.charAt(i) == 211) {
		    input = input.replace(input.charAt(i), 'O');
		}
		if (input.charAt(i) == 218) {
		    input = input.replace(input.charAt(i), 'U');
		}
		// ahora a las minusculas
		if (input.charAt(i) == 225) {
		    input = input.replace(input.charAt(i), 'a');
		}
		if (input.charAt(i) == 233) {
		    input = input.replace(input.charAt(i), 'e');
		}
		if (input.charAt(i) == 237) {
		    input = input.replace(input.charAt(i), 'i');
		}
		if (input.charAt(i) == 243) {
		    input = input.replace(input.charAt(i), 'o');
		}
		if (input.charAt(i) == 250) {
		    input = input.replace(input.charAt(i), 'u');
		}
	    }
	    return input;
	}
	return input;
    }

    /**
     * Método que reemplaza las secuencias de escape existentes para que no se
     * vea alterardo el XML
     *
     * @param cadena
     *            Texto digitado por el usuario
     * @return Texto con reemplazo de secuencias de escape en formato UTF-8
     */
    public static String escapeSecuencia(String cadena) {
	byte[] cadena1 = null;
	String aux = null;
	try {
	    cadena1 = cadena.getBytes("UTF8");
	    aux = new String(cadena1, "UTF8");
	} catch (UnsupportedEncodingException e) {
	    e.printStackTrace();
	}
	aux = aux.replace("&amp;", "&");
	aux = aux.replace("&quot;", String.valueOf('"'));
	aux = aux.replace("&lt;", "<");
	aux = aux.replace("&gt;", ">");
	aux = aux.replace("&apos;", "'");
	return aux;
    }

    /**
     * 
     * @return El un string con password temporal compuesto por la palabra
     *         temporal y un numero aleatorio
     */

    public static String generaPassword() {
	StringBuilder password = new StringBuilder();
	try {
	    password.append("Temporal");
	    password.append((int) (Math.random() * 100000));
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return password.toString();
    }

    public static String quitacerosIzquierda(String cadena) {
	char[] tmp = cadena.toCharArray();
	try {
	    for (int i = 0; i < tmp.length; i++) {
		if (tmp[i] == '0') {
		    tmp[i] = ' ';
		} else {
		    break;
		}
	    }
	    cadena = new String(tmp);
	    cadena = cadena.trim();
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return cadena;
    }

}
