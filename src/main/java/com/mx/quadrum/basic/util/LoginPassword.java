package com.mx.quadrum.basic.util;

/**
 * Clase que manipula los passwords en el sistema de información
 */

public class LoginPassword {
    public LoginPassword() {
        super();
    }

    /**
     * Genera de forma aleatoria el Login con base en el RFC y 4 caracteres adicionales aleatrios
     * 
     * @return Login    
     */

	public static String generaLogin(String rfc) {
        String login = (rfc.substring(0, 4)).toUpperCase();
        for (int i = 0; i < 4; i++) {
            int eleccion = aleatorio(1, 3, 0);
            if (eleccion == 1)
				login += (char) aleatorio(97, 122, 0); // genera una letra
				                                       // min�scula
            if (eleccion == 2)
				login += (char) aleatorio(65, 90, 0); // genera una letra
				                                      // may�scula
            if (eleccion == 3)
                login += (char)aleatorio(48, 57, 0); //genera un digito

        }
        return login;
    }


    /**
     * Genera de forma aleatoria un password y lo devulve al usuario
     * 
     * @return Password     Password actual para el usuario
     */
	public static String generaPassword() {
        String password = "";

		int numChar = aleatorio(8, 10, 0); // genera la contrase�a de 8 a 15
										   // caracteres
        while (numChar > 0) {
            int eleccion = aleatorio(1, 4, 0);
            if (eleccion == 1)
				password += (char) aleatorio(97, 122, 0); // genera una letra
				                                          // min�scula
            if (eleccion == 2)
				password += (char) aleatorio(65, 90, 0); // genera una letra
				                                         // may�scula
            if (eleccion == 3)
                password += (char)aleatorio(48, 57, 0); //genera un digito
            if (eleccion == 4) {
                password += (char)aleatorio(33, 61, 4); //genera un caracter especial

            }
            numChar--;
        }
        return password;
    }

    /**
     * Genera un nuevo entero aleatorio entre a y b donde a es menor que b
     * 
     * @param a
     * @param b
     * @return
     */

	public static int aleatorio(int a, int b, int eleccion) {
        if (eleccion == 4) {
            int caracter;
            do {
                caracter = (int)Math.floor(Math.random() * (b - a + 1) + a);
                switch (caracter) {
                    case 47:
                        return caracter;
                    case 33:
                        return caracter;
                    case 35:
                        return caracter;
                    case 36:
                        return caracter;
                    case 37:
                        return caracter;
                    case 38:
                        return caracter;
                    case 40:
                        return caracter;
                    case 41:
                        return caracter;
                    case 61:
                        return caracter;
                }
            }while (true);
        } else
            return (int)Math.floor(Math.random() * (b - a + 1) + a);
    }
}