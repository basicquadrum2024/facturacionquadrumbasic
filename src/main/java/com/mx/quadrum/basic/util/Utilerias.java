package com.mx.quadrum.basic.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;

import com.mx.quadrum.basic.entity.TcPistasAdministrador;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.entity.UsuarioPrincipal;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.cfdi.utilerias.ConvertidorLetras;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

public class Utilerias {

	public static final String CLASE_LANG_STRING = "java.lang.String";
	public static final String CAMPO_ENCRIP_TRAN = "encriptado";
	public static final String CONTEXTO_BASICA = "Basica";

	private static final Logger LOGGER = Logger.getLogger(Utilerias.class);

	public static DecimalFormat formatoQr = new DecimalFormat("###########0000000000.000000");
	public static DateFormat YYYYMMDD_HHMMSS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("es", "MX"));
	public static DecimalFormat formateador = new DecimalFormat("###,###.00");

	public static String[] TITULOSUSUARIO = { "RFC", "RAZ\u00d3N SOCIAL", "R\u00c9GIMEN FISCAL", "CALLE",
			"NO. INTERIOR", "NO EXTERIOR", "COLONIA", "LOCALIDAD", "REFERENCIA", "MUNICIPIO", "ESTADO", "PA\u00cdS",
			"CODIGO POSTAL" };

	public static final String ADMINISTRADOR = "ADMINISTRADOR";
	public static final String CLIENTE = "CLIENTE";
	public static final String CONTACTO = "CONTACTO";
	public static final String CONCEPTO = "CONCEPTO";
	public static final String USERS = "USERS";
	public static final String CONTRIBUYENTE = "CONTRIBUYENTE";
	public static final String RECEPTOR = "RECEPTOR";

	/**
	 * Metodo para obtener los ultimos caracteres del sello digital
	 * 
	 * @param selloDigital
	 * @return
	 */
	public static String ultimosCaracteresSelloDigital(String selloDigital) {
		return selloDigital.substring((selloDigital.length() - 8), selloDigital.length());

	}

	/**
	 * Metodo para obtener el usuario en session del Spring
	 * 
	 * @param auth
	 * @return
	 */
	public static TwUsuario getUserSession(Authentication auth) {
		TwUsuario usuario = null;
		UsuarioPrincipal customUser = (UsuarioPrincipal) auth.getPrincipal();
		usuario = customUser.getUsuario();
		return usuario;
	}

	/**
	 * Guarda Pistas de Administrador
	 * 
	 * @param accion
	 * @param usuario
	 * @param descripcion
	 * @param service
	 * @param resquest
	 * @param valorNuevo
	 * @param valorAnterior
	 */
	public static void eventosAdministrador(final String objeto, final String accion, final TwUsuario usuario,
			final String sql, final PistasServices pistasServices, HttpServletRequest request) throws Exception {

		TcPistasAdministrador auditoria = new TcPistasAdministrador();
		auditoria.setObjeto(objeto); // Clase que se hace la modificacion
		auditoria.setFechahora(new Date()); // hora
		if (request != null) {
			auditoria.setClientaddress(getClientIpAddress(request));
			auditoria.setClientport(String.valueOf(request.getRemotePort()));
		}
		auditoria.setUserbd(usuario.getTcPerfil().getId());
		auditoria.setAccion(accion); // Descripcion
		auditoria.setCliente(usuario.getRfce()); // usuario en sesion
		auditoria.setSentenciasql(sql); // Eliminar, borrar,
		pistasServices.guardaPistasCliente(auditoria);

	}

	private static String getClientIpAddress(HttpServletRequest request) {
		String xForwardedForHeader = request.getHeader("X-Forwarded-For");
		if (xForwardedForHeader == null) {
			return request.getRemoteAddr();
		} else {
			return new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
		}
	}

	/**
	 * Obtiene la direccion IP del cliente para el registro de la pista de auditoria
	 * 
	 * @return IP del cliente
	 * @throws UnknownHostException
	 */

	public static String obtenerDireccionIP() throws UnknownHostException {
		InetAddress addr = InetAddress.getLocalHost();
		return addr.getHostAddress();
	}

	/**
	 * Formato de fecha yyyy-MM-dd HH:mm:ss
	 */
	public final static ThreadLocal<SimpleDateFormat> formatDateTime = new ThreadLocal<SimpleDateFormat>() {
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
	};
	public static final String login = ".1n1v!9a9e#8o8a?";

	public static String dividirCadena(String cadena, int numeroCaracteres) {
		StringBuilder stringBuilder = new StringBuilder();
		String[] divisiones = cadena.split(String.format("(?<=\\G.{%s})", numeroCaracteres));
		for (String string : divisiones) {
			stringBuilder.append(String.format("%s\n", string));
		}
		return stringBuilder.toString();
	}

	/**
	 * Se encarga de guardar un archivo en el SO
	 * 
	 * @param pathArchivo
	 * @param contenido
	 * @throws IOException
	 * @throws Exception
	 */
	public static void guardarArchivo(final String pathArchivo, final String contenido, final Charset charset)
			throws IOException {
		guardarArchivo(pathArchivo, contenido.getBytes(charset));
	}

	/**
	 * Se encarga de guardar un archivo en el SO
	 * 
	 * @param pathArchivo
	 * @param contenido
	 * @throws IOException
	 * @throws Exception
	 */
	public static void guardarArchivo(final String pathArchivo, final byte[] contenido) throws IOException {
		Path path = Paths.get(pathArchivo);
		Files.createDirectories(path.getParent());
		Files.write(path, contenido);
	}

	public static byte[] crearPdf(String rutaJasper, String rutaXml, BigDecimal total) throws Exception {
		JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(rutaJasper);
		JRXmlDataSource xmlDataSource = new JRXmlDataSource(rutaXml);
		Map<String, Object> parametros = new HashMap<String, Object>();
		System.out.println(ConvertidorLetras.convertir(String.valueOf(total), true, "MXN"));
		parametros.put("TotalEnLetra", ConvertidorLetras.convertir(String.valueOf(total), true, "MXN"));
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, xmlDataSource);
		return JasperExportManager.exportReportToPdf(jasperPrint);
	}

	public static void main(String... strings) throws Exception {
		Files.write(Paths.get("/home/joseluis/Descargas/pdf"),
				crearPdf("/data/facturacion/basica/jasper/PlantillaCfdiV3yV4.jasper",
						"/home/joseluis/Descargas/xml.xml", new BigDecimal("143000.10")));
	}

}