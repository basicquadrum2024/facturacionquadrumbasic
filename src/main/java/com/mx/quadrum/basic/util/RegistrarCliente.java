package com.mx.quadrum.basic.util;

import java.rmi.RemoteException;

import mx.com.cfdiquadrum.ws.registro.ApplicationStub;

import org.apache.axis.AxisFault;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import views.core.soap.services.apps.Registro;
import views.core.soap.services.apps.RegistroLista;

public class RegistrarCliente {

	/**
	 * Metodo para registrar un usuario en el web service de timbrado
	 * @param url
	 * @param usuario
	 * @param contrasenia
	 * @param rfc
	 * @return
	 */
    public static boolean registra(final String url, final String usuario, final String contrasenia, final String rfc) {
	boolean respuesta = false;

	try {

	    ApplicationStub applicationStub = new ApplicationStub();
	    applicationStub._setProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY, url);
	    applicationStub.setTimeout(50000);
	    Registro registro = null;

	    registro = applicationStub.agregar(usuario, contrasenia, rfc);

	    respuesta = registro.getEstatus();

	} catch (AxisFault e) {
	    e.printStackTrace();
	} catch (RemoteException e) {
	    e.printStackTrace();
	}

	return respuesta;

    }

    /**
     * Metodo para obtener el usuario del web service de timbrado
     * @param url
     * @param usuario
     * @param contrasenia
     * @param rfc
     * @return
     */
    public static boolean obtener(final String url, final String usuario, final String contrasenia, final String rfc) {
    	boolean respuesta = false;

    	try {

    	    ApplicationStub applicationStub = new ApplicationStub();
    	    applicationStub._setProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY, url);
    	    applicationStub.setTimeout(50000);
    	    RegistroLista registro = null;

    	    registro = applicationStub.obtener(usuario, contrasenia, rfc);
    	    if(registro.getUsuarios().length>0){
    	    	respuesta = true;    	    	
    	    }
 
    	} catch (AxisFault e) {
    	    e.printStackTrace();
    	} catch (RemoteException e) {
    	    e.printStackTrace();
    	}

    	return respuesta;

        }
    
    public static void main(String args[]){
    	boolean registro = registra("http://dev.ws.cfdiquadrum.com.mx/registro.wsdl", "fact_basica@quadrum.com.mx", "FactB3@07.", "LAN8507268IA");
    	try {
    		boolean respuesta= obtener("http://dev.ws.cfdiquadrum.com.mx/registro.wsdl", "fact_basica@quadrum.com.mx", "FactB3@07.", "LAN8507268IA");
    		System.out.println("resgistro--> " + registro);
        	System.out.println("men de respuesta--> "+respuesta);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
	}
}
