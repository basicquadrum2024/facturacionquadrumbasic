
package com.mx.quadrum.basic.util;

import org.apache.log4j.Logger;

/**
 * 
 * Entidad SessionApp
 *
 */
public final class SessionApp {
    private static final Logger logger = Logger.getLogger(SessionApp.class);

    private static String rfc;

    public static String getRfc() {
	return rfc;
    }

    public static void setRfc(String rfc) {
	SessionApp.rfc = rfc;
    }

}
