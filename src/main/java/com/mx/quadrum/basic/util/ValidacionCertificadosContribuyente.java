package com.mx.quadrum.basic.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.ssl.PKCS8Key;

import com.mx.quadrum.basic.excepcion.ValidacionCertificadosExcepcion;
import com.mx.quadrum.cfdi.seguridad.validador.CertificateVerifier;
import com.mx.quadrum.cfdi.utilerias.UtileriasValidacionLlavesPrivadaPublica;

public class ValidacionCertificadosContribuyente {

	private static final File RAIZ_CERTIFICADOS = new File(ParametroEnum.RUTA_CERTIFICADOS_PADRE.getValue());

	private byte[] bytesCertificado;
	private byte[] bytesLLave;
	private String passowordLlave;
	private String rfcEmisor;
	private String cadenaOriginal;
	private String tipoCertificado;

	public ValidacionCertificadosContribuyente(byte[] bytesCertificado, byte[] bytesLLave, String passowordLlave,
			String rfcEmisor, String cadenaOriginal, String tipoCertificado) {
		this.bytesCertificado = bytesCertificado;
		this.bytesLLave = bytesLLave;
		this.passowordLlave = passowordLlave;
		this.rfcEmisor = rfcEmisor;
		this.cadenaOriginal = cadenaOriginal;
		this.tipoCertificado = tipoCertificado;
	}

	public String generarFirma() throws Exception {

		/**
		 * Obtenemos un obejto X509Certificate mediante un arreglo de byte, el cual lo
		 * obtenemos del archivo .cer el emisor
		 */
		X509Certificate certificado = null;
		PrivateKey llavePrivada = null;
		try {
			certificado = obtenerCertificado(bytesCertificado);
		} catch (Exception e) {
			throw new ValidacionCertificadosExcepcion(
					String.format("El Certificado %s no tiene un formato v\u00e1lido", tipoCertificado));
		}

		/**
		 * Obtenemos un objeto PrivateKey a travez del arreglo de byte obtenido del
		 * archivo .key del emisor y de la contrase�a para abrir la llave
		 */
		try {
			llavePrivada = obtenerLlavePrivada(bytesLLave, passowordLlave);
		} catch (Exception e) {
			throw new ValidacionCertificadosExcepcion(
					String.format("La contrase\u00f1a de la llave privada .key del cetificado de tipo %s es incorrecta",
							tipoCertificado));
		}

		/**
		 * Se realiza la validaci�n de correspondencias entre la llave privada y la
		 * llave publica
		 */
		if (!UtileriasValidacionLlavesPrivadaPublica.validaCorrespondenciasLLavePrivadaConPublica(llavePrivada,
				certificado))
			throw new ValidacionCertificadosExcepcion(
					String.format("La llave privada %s no corresponde a la llave p\u00fablica", tipoCertificado));

		/**
		 * Se valida que el certificado que se esta evaluando corresponda al emisor
		 * registrado en el xml
		 */
		if (!certificado.getSubjectDN().getName().contains(rfcEmisor))
			throw new ValidacionCertificadosExcepcion(
					String.format("El certificado %s que intenta validar no corresponde al emisor", tipoCertificado));

		/**
		 * Se realiza la validaci�n de la vigencia de los certificados mediante los
		 * atributos NotBefore y NotAfter
		 */
		Date ahora = new Date();
		boolean inicio = certificado.getNotBefore().before(ahora);
		boolean fin = certificado.getNotAfter().after(ahora);
//		if (!(inicio && fin))
//			throw new ValidacionCertificadosExcepcion(
//					String.format("Certificado %s revocado o caduco", tipoCertificado));

		/**
		 * Se valida que el archivo .cer sea de tipo FIEL o de tipo CSD dependiendo del
		 * tipo de archivo que se requiere validar
		 */
		String subject = certificado.getSubjectDN().toString();
		if (tipoCertificado.equals("CSD")) {
			if (!subject.contains("OU="))
				throw new ValidacionCertificadosExcepcion(
						String.format("El certificado para la emisi\u00f3n de CFDI no es de tipo %s", tipoCertificado));
		} else {
			if (subject.contains("OU="))
				throw new ValidacionCertificadosExcepcion(
						String.format("El certificado para la firma del contrato no es de tipo %s", tipoCertificado));
		}

		/**
		 * Se valida mendiante los archivos publicados en
		 * http://omawww.sat.gob.mx/tramitesyservicios/Paginas/certificado_sello_digital.htm
		 * para saber si un certificado fue o no expedido por los certificados del SAT
		 */
//		try {
//			verificarCertificadoExpedidoSAT(certificado, RAIZ_CERTIFICADOS.listFiles());
//		} catch (Exception e) {
//			throw new ValidacionCertificadosExcepcion(
//					String.format("Certificado %s no expedido por el SAT", tipoCertificado));
//		}

		Signature firma = Signature.getInstance("SHA1withRSA");
		firma.initSign(llavePrivada);
		firma.update(cadenaOriginal.getBytes("UTF-8"));
		byte[] signed = firma.sign();
		Base64 base64 = new Base64(-1);
		return base64.encodeToString(signed);
	}

	/**
	 * Obtiene una instancia del tipo java.security.cert.X509Certificate, la cual
	 * contiene la llave publica de un archivo x509 con extencion .cer generado por
	 * el SAT
	 * 
	 * @param certificadoArrayBytes
	 * @return
	 * @throws Exception
	 */
	private X509Certificate obtenerCertificado(final byte[] certificadoArrayBytes) throws Exception {
		CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
		X509Certificate x509Certificate = (X509Certificate) certificateFactory
				.generateCertificate(new ByteArrayInputStream(certificadoArrayBytes));
		return x509Certificate;
	}

	/**
	 * Obtiene la llave privada (java.security.PrivateKey) de un archivo de tipo
	 * pkcs8, en este caso sirve para los archivos .key generados por el SAT, los
	 * cuales sirven para el formado de archivos xml
	 * 
	 * <p>
	 * El parametro passwordArchivoKey, es la contrase�a o password para manipular
	 * el archivo pkcs8 con extencion .key, si no se ingresa la contrase�a correcta
	 * es practicamente imposible poder obtener la llave privada
	 * </p>
	 * 
	 * @param keyArrayBytes
	 * @param passwordArchivoKey
	 * @return
	 * @throws Exception
	 */
	private PrivateKey obtenerLlavePrivada(final byte[] keyArrayBytes, final String passwordArchivoKey)
			throws Exception {
		byte[] privateKeyByte;
		privateKeyByte = new PKCS8Key(keyArrayBytes, passwordArchivoKey.toCharArray()).getDecryptedBytes();
		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKeyByte);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(pkcs8EncodedKeySpec);
	}

	private void verificarCertificadoExpedidoSAT(X509Certificate certificadoEmisor, File[] certificadosRaiz)
			throws Exception {
		Set<X509Certificate> additionalCerts = new HashSet<X509Certificate>();
		for (File filecertificado : certificadosRaiz) {
			Path dirCer = Paths.get(filecertificado.getPath());
			byte[] fileCer = Files.readAllBytes(dirCer);
			CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
			X509Certificate certificadoRaiz = (X509Certificate) certificateFactory
					.generateCertificate(new ByteArrayInputStream(fileCer));
			additionalCerts.add(certificadoRaiz);
		}
		CertificateVerifier.verificarCertificado(certificadoEmisor, additionalCerts);
	}

	public static void main(String arg[]) {
		try {
			byte[] bytesCertificado = Files.readAllBytes(new File(
					"/home/marco/M�sica/RFC-PAC-SC/Personas Fisicas/FIEL_CACX7605101P8_20190528152826/CACX7605101P8_FIEL.cer")
							.toPath());
			byte[] bytesLLave = Files.readAllBytes(new File(
					"/home/marco/M�sica/RFC-PAC-SC/Personas Fisicas/FIEL_CACX7605101P8_20190528152826/CACX7605101P8_FIEL.key")
							.toPath());
//			FirmarDocumentosFiel documentosFiel = new FirmarDocumentosFiel(bytesCertificado, bytesLLave, "12345678a",
//					"CACX7605101P8", "Esta es la cadena");
//			String firma = documentosFiel.generarFirmaFiel();
//			System.out.println(firma);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
