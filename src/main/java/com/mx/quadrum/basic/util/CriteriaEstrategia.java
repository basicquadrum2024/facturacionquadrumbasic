package com.mx.quadrum.basic.util;

import org.hibernate.Criteria;
/**
 * 
 * Entidad abstract CriteriaEstrategia
 *
 */
public abstract class CriteriaEstrategia {
    public abstract void estrategia(Criteria cliteria) throws Exception;
}