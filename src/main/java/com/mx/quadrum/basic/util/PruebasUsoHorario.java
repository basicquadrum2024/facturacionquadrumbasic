package com.mx.quadrum.basic.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class PruebasUsoHorario {

	private static final String FORMATO_FECHA = "yyyy-MM-dd'T'HH:mm:ss";

	public static void main(String arg[]) {
		try {
			System.out.println(obtenerFechaPorTimeZone("GMT-06:00"));
			System.out.println(clientDateString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String clientDateString() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		df.setTimeZone(tz); // strip timezone
		return df.format(new Date());
	}

	public static String obtenerFechaPorTimeZone(String idTimeZone) {
		TimeZone timeZone = TimeZone.getTimeZone(idTimeZone);
		Date fechaSistema = new Date();
		DateFormat formatter = new SimpleDateFormat(FORMATO_FECHA);
		formatter.setTimeZone(timeZone);
		String sDate = formatter.format(fechaSistema);
		return sDate;
	}
}
