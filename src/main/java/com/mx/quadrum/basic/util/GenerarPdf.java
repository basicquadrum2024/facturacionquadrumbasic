package com.mx.quadrum.basic.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import org.springframework.web.bind.annotation.RestController;

import com.mx.quadrum.basic.adenda.Adenda33;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.services.EmisorServices;
import com.mx.quadrum.basic.wrapper.ConceptoPremium;
import com.mx.quadrum.basic.wrapper.PagosPremium;
import com.mx.quadrum.cfdi.complementos.pagos.v10.schema.Pagos;
import com.mx.quadrum.cfdi.complementos.pagos.v10.schema.Pagos.Pago;
import com.mx.quadrum.cfdi.complementos.pagos.v10.schema.Pagos.Pago.DoctoRelacionado;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v11.schema.TimbreFiscalDigital;
import com.mx.quadrum.cfdi.utilerias.ConvertidorLetras;
import com.mx.quadrum.cfdi.utilerias.QrGenerator;
import com.mx.quadrum.cfdi.utilerias.UtileriaSecuenciaEscape;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.v33.schema.Comprobante;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

public class GenerarPdf {

	public static final Logger LOGGER = Logger.getLogger(GenerarPdf.class);

	/**
	 * Metodo que genera una lista de reportes apartir de una lista de xml
	 * 
	 * @author salvador
	 * @param uuidRecibos
	 * @return byte[]
	 */
	public static byte[] leerXmlToPdf(Comprobante comprobante, TcContribuyente tcContribuyente, TcReceptor tcReceptor,
			TwCfdi twCfdi) {
		byte pdf[] = null;
		try {

			// Path dirXml = Paths.get(direccionXml);
			// byte[] fileXMLComprobante = Files.readAllBytes(dirXml);

			// Class[] classes = { Comprobante.class, Adenda33.class,
			// TimbreFiscalDigital.class, Pagos.class };
			// Comprobante comprobante =
			// UtileriasCfdi.convertirAComprobante(fileXMLComprobante, classes);

			JasperPrint jasperPrint = ejecutar(comprobante, tcContribuyente, tcReceptor, twCfdi);

			ByteArrayOutputStream outPdf = new ByteArrayOutputStream();
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outPdf));

			// Configuration
			// SimplePdfExporterConfiguration configuration = new
			// SimplePdfExporterConfiguration();
			// configuration.setEncrypted(true);
			// configuration.set128BitKey(true);
			// configuration.setUserPassword(comprobante.getReceptor().getRfc());
			// configuration.setOwnerPassword(comprobante.getEmisor().getRfc());
			// configuration.setPermissions(PdfWriter.ALLOW_COPY |
			// PdfWriter.ALLOW_PRINTING);

			// exporter.setConfiguration(configuration);

			exporter.setConfiguration(new SimplePdfExporterConfiguration());

			if (exporter != null) {
				exporter.exportReport();
			}

			pdf = outPdf.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pdf;
	}

	/**
	 * Metodo que genera un repote a partir de parametros y datasource
	 * 
	 * @param comprobante
	 * @param nomina
	 * @param timbreFiscal
	 * @return JasperPrint
	 */
	private static JasperPrint ejecutar(Comprobante comprobante, TcContribuyente tcContribuyente, TcReceptor tcReceptor,
			TwCfdi twCfdi) {
		JasperPrint jasperPrint = new JasperPrint();
		try {

			JasperReport jasperReport = (JasperReport) JRLoader
					.loadObjectFromFile(ParametroEnum.JASPER_PADREV33.getValue());

			TimbreFiscalDigital timbre = (TimbreFiscalDigital) UtileriasCfdi
					.getObjectoComplemento(comprobante.getComplemento().getAny(), TimbreFiscalDigital.class);

			// Comprobante.Addenda addendaAny = (Comprobante.Addenda)
			// UtileriasCfdi.getObjectoComplemento(comprobante
			// .getComplemento().getAny(), Comprobante.Addenda.class);

			// Adenda33 addenda = (Adenda33)
			// UtileriasCfdi.getObjectoComplemento(comprobante.getAddenda().getAny(),
			// Adenda33.class);

			// Obtenemos los parametros
			Map<String, Object> params = new HashMap<String, Object>();
			params = obtieneParametrosCfdiV33(comprobante, timbre, null, tcContribuyente, tcReceptor, twCfdi);

			// Obtenemos el DataSource
			JRDataSource dataSource = cargarConceptos(comprobante.getConceptos().getConcepto());

			jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}

	/**
	 * MEtodo que devuelve los parametros del reporte
	 * 
	 * @param comprobante
	 * @param nomina
	 * @param timbreFiscalDigital
	 * @return Map<String, Object>
	 * @throws JAXBException
	 */
	public static Map<String, Object> obtieneParametrosCfdiV33(Comprobante comprobante, TimbreFiscalDigital timbre,
			Adenda33 addenda, TcContribuyente tcContribuyente, TcReceptor tcReceptor, TwCfdi twCfdi) throws Exception {
		Map<String, Object> parametros = new HashMap<>();

		parametros.put("tipoComprobante", comprobante.getTipoDeComprobante()+ "-"+ DescripcionCatalogos.getValorTipoComprobante( comprobante.getTipoDeComprobante()));
		parametros.put("Lugar", UtileriaSecuenciaEscape.unEscaparCadenasSat(comprobante.getLugarExpedicion()));
		parametros.put("FechaHoraEmision", UtileriasCfdi.YYYYMMDDTHHMMSS.format(comprobante.getFecha()));

		parametros.put("TipoDocumento", "Factura Electrónica");
		parametros.put("NombreDelCliente",
				UtileriaSecuenciaEscape.unEscaparCadenasSat(comprobante.getEmisor().getNombre()));
		parametros.put("RFCcliente", UtileriaSecuenciaEscape.unEscaparCadenasSat(comprobante.getEmisor().getRfc()));
		parametros.put("razonSocialReceptor",
				UtileriaSecuenciaEscape.unEscaparCadenasSat(comprobante.getReceptor().getNombre()));
		parametros.put("RFCReceptor", comprobante.getReceptor().getRfc());
		parametros.put("TotalEnLetra", ConvertidorLetras.convertir("" + comprobante.getTotal(), true, "MXN"));
		if (comprobante.getDescuento() != null) {
			parametros.put("descuentoTotal", comprobante.getDescuento().toString());
		}
		if (comprobante.getImpuestos() != null && comprobante.getImpuestos().getTotalImpuestosRetenidos() != null)
			parametros.put("TIRetenidos", comprobante.getImpuestos().getTotalImpuestosRetenidos().toString());
		if (comprobante.getImpuestos() != null && comprobante.getImpuestos().getTotalImpuestosTrasladados() != null)
			parametros.put("TITrasladados", comprobante.getImpuestos().getTotalImpuestosTrasladados().toString());
		if (comprobante.getSubTotal() != null)
			parametros.put("Subtotal", comprobante.getSubTotal().toString());
		if (comprobante.getTotal() != null)
			parametros.put("Total", comprobante.getTotal().toString());
		if (comprobante.getMetodoPago() != null)
			parametros.put("metodoPago", comprobante.getMetodoPago().toString()+"-"+ DescripcionCatalogos.getValorMetodoPago(comprobante.getMetodoPago()));
		if (comprobante.getCondicionesDePago() != null)
			parametros.put("CondicionesDePago", comprobante.getCondicionesDePago());
		if (comprobante.getFormaPago() != null)
			parametros.put("FormaDePago", comprobante.getFormaPago().toString()+"-"+ DescripcionCatalogos.getValorFormaPago(comprobante.getFormaPago()));
		parametros.put("RegimenFiscalEmisor", comprobante.getEmisor().getRegimenFiscal()+"-"+ DescripcionCatalogos.getValorRegimenFiscal(comprobante.getEmisor().getRegimenFiscal()));

		// if (addenda != null) {
		// if (addenda.getFolio() != null)
		parametros.put("Folio", twCfdi.getFolio());
		parametros.put("Serie", twCfdi.getSerie());
		// StringBuilder emisor_calle = new StringBuilder();
		// if (addenda.getDomicilioEmisor() != null) {
		// if (addenda.getDomicilioEmisor().getCalle() != null)
		// emisor_calle.append(StringUtils.stripToEmpty(addenda.getDomicilioEmisor().getCalle())
		// + " ");
		//
		// if (addenda.getDomicilioEmisor().getNumeroInterior() != null)
		// emisor_calle.append(StringUtils
		// .trimToEmpty("No. Int. " +
		// addenda.getDomicilioEmisor().getNumeroInterior() + " "));
		//
		// if (addenda.getDomicilioEmisor().getNumeroExterior() != null)
		// emisor_calle.append(StringUtils
		// .trimToEmpty("No. Ext." +
		// addenda.getDomicilioEmisor().getNumeroExterior() + " "));
		//
		// if (addenda.getDomicilioEmisor().getCodigoPostal() != null)
		// emisor_calle.append(StringUtils.trimToEmpty(addenda.getDomicilioEmisor().getCodigoPostal())
		// + " ");
		// if (addenda.getDomicilioEmisor().getColonia() != null)
		// emisor_calle.append(StringUtils.trimToEmpty(addenda.getDomicilioEmisor().getColonia())
		// + " ");
		// if (addenda.getDomicilioEmisor().getMunicipio() != null)
		// emisor_calle.append(StringUtils.trimToEmpty((addenda.getDomicilioEmisor().getMunicipio()))
		// + " ");
		// if (addenda.getDomicilioEmisor().getEstado() != null)
		// emisor_calle.append(StringUtils.trimToEmpty((addenda.getDomicilioEmisor().getEstado())));
		//
		// }
		if (tcContribuyente.getDomicilio() != null) {
			parametros.put("calleEmisor", tcContribuyente.getDomicilio());
		}
		if (tcReceptor.getDomicilio() != null) {
			parametros.put("calleReceptor", tcReceptor.getDomicilio());
		}

		// }
		parametros.put("moneda", comprobante.getMoneda().toString());
		if(comprobante.getTipoCambio()!=null){
		parametros.put("tipoCambio", comprobante.getTipoCambio().toString());
		}
		parametros.put("usoCfdi", comprobante.getReceptor().getUsoCFDI().toString()+ "-"+ DescripcionCatalogos.getValorUsoCFdi(comprobante.getReceptor().getUsoCFDI()));
		parametros.put("FolioFiscal", timbre.getUUID());
		parametros.put("NoSerieCSD", timbre.getNoCertificadoSAT());
		parametros.put("SelloSAT", timbre.getSelloSAT());
		parametros.put("NoSerieCertificadoSAT", timbre.getNoCertificadoSAT());
		parametros.put("FechaHoraCertificacion", UtileriasCfdi.YYYYMMDDTHHMMSS.format(timbre.getFechaTimbrado()));
		parametros.put("SelloDigitalCFDI", timbre.getSelloCFD());
		parametros.put("CadenaOriginal",
				"||" + comprobante.getVersion() + "|" + timbre.getUUID() + "|"
						+ UtileriasCfdi.YYYYMMDD_HHMMSS.format(timbre.getFechaTimbrado()) + "|" + timbre.getSelloCFD()
						+ "|" + timbre.getNoCertificadoSAT() + "||");

		// Pagos
		Pagos pagos = (Pagos) UtileriasCfdi.getObjectoComplemento(comprobante.getComplemento().getAny(), Pagos.class);
		if (pagos != null) {
			parametros.put("subReporte", "ComplementoPagos");// Nombre
			parametros.put("complementoDatos", cargaComplementoPagos(pagos.getPago()));// DataSource

			parametros.put("TipoDocumento", "Factura de Recepción de Pagos");
		}

		parametros.put("QR", new ByteArrayInputStream(crearQrByte(comprobante, timbre)));

		return parametros;
	}

	/**
	 * MEtodo que carga el complemento Pagos
	 * 
	 * @param pagos
	 *            Lista de pagos
	 * @return JRBeanCollectionDataSource
	 * @throws Exception
	 */
	private static JRBeanCollectionDataSource cargaComplementoPagos(List<Pago> pagos) throws Exception {
		List<PagosPremium> pagosPremium = new ArrayList<>();

		for (Pago pago : pagos) {
			PagosPremium pagoPremium = new PagosPremium();
			pagoPremium.setMonto(pago.getMonto().toString());
			pagoPremium.setMoneda(pago.getMonedaP().toString());
			pagoPremium.setFormaPago(pago.getFormaDePagoP());
			pagoPremium.setFechaPago(UtileriasCfdi.YYYYMMDD_HHMMSS.format(pago.getFechaPago()));
			DoctoRelacionado docto = pago.getDoctoRelacionado().get(0);
			pagoPremium.setISI(docto.getImpSaldoInsoluto().toString());
			pagoPremium.setImportePagado(docto.getImpPagado().toString());
			pagoPremium.setISA(docto.getImpSaldoAnt().toString());
			pagoPremium.setNoParcialidad(docto.getNumParcialidad().toString());
			pagoPremium.setMetodoPago(docto.getMetodoDePagoDR().toString());
			pagoPremium.setIdDocumento(docto.getIdDocumento());
			pagosPremium.add(pagoPremium);
		}

		return new JRBeanCollectionDataSource(pagosPremium);
	}

	/**
	 * Metodo que carga los Conceptos del cfdi
	 * 
	 * @param conceptos
	 *            Lista de Conceptos
	 * @return JRDataSource
	 */
	public static JRDataSource cargarConceptos(List<Comprobante.Conceptos.Concepto> conceptos) {
		List<ConceptoPremium> listaReporte = new ArrayList<>();

		for (Comprobante.Conceptos.Concepto concepto : conceptos) {
			ConceptoPremium conceptoReporte = new ConceptoPremium();
			if (concepto.getCantidad() != null) {
				conceptoReporte.setCantidad(String.valueOf(concepto.getCantidad()));
			}
			if (concepto.getUnidad() != null) {
				conceptoReporte
						.setUnidad(UtileriaSecuenciaEscape.unEscaparCadenasSat(String.valueOf(concepto.getUnidad())));
			}
			conceptoReporte.setDescripcion(
					UtileriaSecuenciaEscape.unEscaparCadenasSat(String.valueOf(concepto.getDescripcion())));
			conceptoReporte.setValorUnitario(concepto.getValorUnitario().toString());
			conceptoReporte.setImporte(concepto.getImporte().toString());
			if (concepto.getNoIdentificacion() != null) {
				conceptoReporte.setNoIdentificacion(String.valueOf(concepto.getNoIdentificacion()));
			}
			if (concepto.getDescuento() != null) {
				conceptoReporte.setDescuento(concepto.getDescuento().toString());
			}
			conceptoReporte.setClaveProdServ(concepto.getClaveProdServ());
			conceptoReporte.setClaveUnidad(concepto.getClaveUnidad());
			listaReporte.add(conceptoReporte);
		}
		return new JRBeanCollectionDataSource(listaReporte);
	}

	/**
	 * Metodo encargado de crear una Imagen QR .png en un arreglo de bytes
	 * 
	 * @param comprobante
	 * @param fiscalDigital
	 * @return
	 * @throws Exception
	 */
	private static byte[] crearQrByte(Comprobante comprobante, TimbreFiscalDigital fiscalDigital) throws Exception {
		byte respuesta[] = null;
		String cadena = null;

		if (comprobante.getEmisor().getRfc() != null && comprobante.getReceptor().getRfc() != null
				&& comprobante.getTotal() != null && fiscalDigital.getUUID() != null) {
			cadena = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx " + "&id="
					+ fiscalDigital.getUUID() + "&re=" + comprobante.getEmisor().getRfc() + "&rr="
					+ comprobante.getReceptor().getRfc() + "&tt=" + comprobante.getTotal() + "&fe="
					+ Utilerias.ultimosCaracteresSelloDigital(fiscalDigital.getSelloCFD());

		}
		if (cadena != null) {
			respuesta = QrGenerator.generateQrByteArray(cadena, 140, 140);
		}
		return respuesta;
	}

}
