package com.mx.quadrum.basic.util;

import java.util.Arrays;

public class DividirCadenas {

	public static void main(String arg[]) {
		try {
			System.out.println(Arrays.toString(
				    "Thequickbrownfoxjumps".split("(?<=\\G.{4})")
				));
			System.out.println(Utilerias.dividirCadena("Thequickbrownfoxjumps", 145));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
