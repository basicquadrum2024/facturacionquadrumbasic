package com.mx.quadrum.basic.util;

import org.apache.axis.MessageContext;
import org.apache.log4j.Logger;

import com.mx.quadrum.cfdi.utilerias.UtileriaRequestResponseAxis;
import com.mx.quadrum.cfdi.utilerias.UtileriaRequestResponseAxis.TipoPeticion;

import mx.com.cfdiquadrum.ws.cancelar.ApplicationStub;
import views.core.soap.services.apps.Cancela;

public class UtileriaCancelacion {

	private UtileriaCancelacion() {

	}

	public static final Logger LOGGER = Logger.getLogger(UtileriaCancelacion.class);

	public static Cancela cancelacion(final String urlWebServiceTimbrado, views.core.soap.services.apps.UUID[] UUIDS,
			java.lang.String usuario, java.lang.String contrasena, java.lang.String rfc, byte[] cer, byte[] key,
			java.lang.Boolean reintentar) {
		ApplicationStub bindingStub = null;
		Cancela cancela = null;

		try {
			bindingStub = new ApplicationStub();
			bindingStub._setProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY, urlWebServiceTimbrado);
			bindingStub.setTimeout(50000);
			cancela = bindingStub.cancelar(UUIDS, usuario, contrasena, rfc, cer, key, reintentar);
			MessageContext messageContext = bindingStub._getCall().getMessageContext();
			String request = UtileriaRequestResponseAxis.obtenerSoapRequestResponse(messageContext.getRequestMessage(),
					true, TipoPeticion.REQUEST);
			String response = UtileriaRequestResponseAxis
					.obtenerSoapRequestResponse(messageContext.getResponseMessage(), true, TipoPeticion.RESPONSE);
			LOGGER.error(String.format("REQUEST CANCELACIÓN: %s", request));
			LOGGER.error(String.format("RESPONSE CANCELACIÓN: %s", response));
		} catch (Exception e) {
			LOGGER.error("Error en la Utilerias de cancelacion" + UtileriaException.imprimeStackError(e));
		}
		return cancela;
	}

}
