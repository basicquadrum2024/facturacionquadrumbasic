package com.mx.quadrum.basic.util;

/**
 * Clase para definir variables globales.
 * 
 * @author Pedro Romero Martinez.
 * @since 01-Sep-2015.
 *
 */

public class GlobalUtil {
	public static final int TIMEOUT_TIMBRAR = 5000;
	public static final String MSG_VALIDA_USR = "Usuario no encontrado en el sistema, consulte a su administrador!";
	public static final String CLASE_LANG_STRING = "java.lang.String";
	public static final String CAMPO_ENCRIP_TRAN = "encriptado";	
	// Estados que puede tener el folio.
	public static final Integer FOLIO_ERROR = 2;
	public static final Integer FOLIO_OK = 1;
	public static final Integer FOLIO_NUEVO = 0;
	// tipos de pago
	public static final Integer PAGO_EFECTIVO = 0;
	public static final Integer PAGO_CREDITO = 1;
	public static final Integer PAGO_DEBITO = 2;
	// tipos de pago Emergencia
	public static final String PAGO_EFECTIVOEMERGENCIA = "Efectivo";
	public static final String PAGO_CREDITOEMERGENCIA = "tarjeta de cr�dito";
	public static final String PAGO_DEBITOEMERGENCIA = "tarjeta de d�bito";
	
	public static final String FACTURA_PEAJE="Factura de Peaje";
	public static final String FACTURA_EMERGENCIA="Factura de emergencia";
	public static final String FACTURA_GENERAL="Factura General";
	public static final String FACTURA_REEXPEDIDA="Factura reexpedida";
	public static final String NOTA_CREDITO="Nota de cr�dito";
	public static final String INGRESOS_DIVERSOS="Ingresos diversos";
	public static final String INGRESOS_DIVERSOS_NOTA_CREDITO=INGRESOS_DIVERSOS+"/"+NOTA_CREDITO;
	
	public static final Integer FACTURACION_PEAJE=0;
	public static final Integer FACTURACION_NOTA_CREDITO=1;
	public static final Integer FACTURACION_EMERGENCIA=2;
	
	public static final int IDFACTURACION_RAPIDA=58706;
	/**
	 * Variable para poder hacer din�micos los par�metros.
	 */
	public static final String PARAMETRO="facturacion";
	public static final Integer APLICA_EXPEDIDO_EN=1;
	
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	
	public static final String PARCIALIDADES_PAGADAS="Parcialidades Pagadas";

}
