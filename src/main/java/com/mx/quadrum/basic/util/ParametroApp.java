
package com.mx.quadrum.basic.util;

import java.util.Map;

public final class ParametroApp {

    private static Map<String, String> mapParam;

    /**
     * Contructor de la clase ParametroApp
     * @param mapa
     */
    public ParametroApp(Map<String, String> mapa) {
	ParametroApp.mapParam = mapa;
    }

    /**
     * 
     * @param nombreParametro
     * @return
     */
    public static String getValorParametro(final String nombreParametro) {
	return mapParam.get(nombreParametro);
    }

    private static String empresa;

    /**
     * @return the empresa
     */
    public String getEmpresa() {
	return empresa;
    }

    /**
     * @param empresa
     *            the empresa to set
     */
    public void setEmpresa(String empresa) {
	ParametroApp.empresa = empresa;
    }
}
