package com.mx.quadrum.basic.aes;

import java.util.List;

public interface AesServices {

    public AESKey generaKey(String cadena);

    public String encryAES(String cadena);

    public String HexToString(byte[] arregloEncriptado);

    public byte[] StringToHex(String encriptado);

    public String descryAES(String encriptado);

    public String encriptaCadena(String plaintext);

    public String desEncriptaCadena(String encrytext);

    public Object desEncritaObject(Object object);

    public Object encriptaObject(Object object);

    public <T> List<T> desEncriptaListObject(List<T> listObjetc);

}