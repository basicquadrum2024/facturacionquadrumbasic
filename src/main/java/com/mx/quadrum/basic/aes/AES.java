package com.mx.quadrum.basic.aes;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.mx.quadrum.basic.entity.TcAcciones;
import com.mx.quadrum.basic.entity.TcPerfil;
import com.mx.quadrum.basic.entity.TwUsuario;

/**
 * Clase que realiza la encripción de texto plano utilizando el algoritmo AES
 * 256
 */
public class AES {
    // ******** Variables de instacia *********
    private String ALGORITMO = "AES"; // algoritmo
    private int LONGITUD = 256; // longitud de la llave
    private String CODIFICACION = "UTF-8"; // como se convertira a byte, esto
					   // sera mas adelante
    private AESKey aesKey;
    private EncripcionDatos encripcion = new EncripcionDatos();

    // ******** Constructores ********
    public AES() {
	super();
    }

    public AES(AESKey aesKey) {
	this.aesKey = aesKey;
    }

    // *******************************

    /**
     * Método que genera la clave con la cual sera cifrado el texto plano.
     * 
     * @param cadena
     *            Valor que se codificara para obtener la clave
     * @return Clave utilizada para cifrar
     */

    public AESKey generaKey(String cadena) {
	AESKey aesKey = new AESKey();
	aesKey.setEncoded(HexToString(cadena.getBytes()));
	return aesKey;
    }

    /**
     * Método que encripta texto plano con el algoritmo AES 256
     * 
     * @param cadena
     *            Texto plano a encriptar
     * @return Texto cifrado
     */

    public String encryAES(String cadena) {
	String salida = null;
	try {
	    byte[] raw = StringToHex(aesKey.getEncoded());
	    SecretKeySpec skeySpec = new SecretKeySpec(raw, ALGORITMO);
	    Cipher cipher = Cipher.getInstance(ALGORITMO);
	    cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
	    byte[] encrypted = cipher.doFinal(cadena.getBytes(CODIFICACION));
	    salida = HexToString(encrypted);
	} catch (UnsupportedEncodingException uee) {
	    // TODO: Add catch code
	    uee.printStackTrace();
	} catch (InvalidKeyException ike) {
	    // TODO: Add catch code
	    ike.printStackTrace();
	} catch (BadPaddingException bpe) {
	    // TODO: Add catch code
	    bpe.printStackTrace();
	} catch (IllegalBlockSizeException ibse) {
	    // TODO: Add catch code
	    ibse.printStackTrace();
	} catch (NoSuchAlgorithmException nsae) {
	    // TODO: Add catch code
	    nsae.printStackTrace();
	} catch (NoSuchPaddingException nspe) {
	    // TODO: Add catch code
	    nspe.printStackTrace();
	}
	return salida;
    }

    /**
     * Método que convirte de Hexadecimal a String
     * 
     * @param arregloEncriptado
     * @return Valor en Hexadecimal
     */

    private String HexToString(byte[] arregloEncriptado) {
	String textoEncriptado = "";
	for (int i = 0; i < arregloEncriptado.length; i++) {
	    int aux = arregloEncriptado[i] & 0xff;
	    if (aux < 16) {
		textoEncriptado = textoEncriptado.concat("0");
	    }
	    textoEncriptado = textoEncriptado.concat(Integer.toHexString(aux));
	}
	return textoEncriptado;
    }

    /**
     * Método que convierte de String a Hexadecimal
     * 
     * @param encriptado
     * @return
     */

    private byte[] StringToHex(String encriptado) {
	byte[] enBytes = new byte[encriptado.length() / 2];
	for (int i = 0; i < enBytes.length; i++) {
	    int index = i * 2;
	    String aux = encriptado.substring(index, index + 2);
	    int v = Integer.parseInt(aux, 16);
	    enBytes[i] = (byte) v;
	}
	return enBytes;
    }

    /**
     * Método que desencripta texto cifrado con algoritmo AES 256
     * 
     * @param encriptado
     *            Texto encriptado
     * @return Texto plano
     */

    public String descryAES(String encriptado) {
	String originalString = null;
	try {
	    byte[] raw = StringToHex(aesKey.getEncoded());
	    SecretKeySpec skeySpec = new SecretKeySpec(raw, ALGORITMO);
	    Cipher cipher = Cipher.getInstance(ALGORITMO);
	    cipher.init(Cipher.DECRYPT_MODE, skeySpec);
	    byte[] original = cipher.doFinal(StringToHex(new String(new BASE64Decoder().decodeBuffer(encriptado))));
	    originalString = new String(original);
	} catch (InvalidKeyException ike) {
	    // TODO: Add catch code
	    ike.printStackTrace();
	} catch (BadPaddingException bpe) {
	    // TODO: Add catch code
	    bpe.printStackTrace();
	} catch (IllegalBlockSizeException ibse) {
	    // TODO: Add catch code
	    ibse.printStackTrace();
	} catch (IOException ioe) {
	    // TODO: Add catch code
	    ioe.printStackTrace();
	} catch (NoSuchAlgorithmException nsae) {
	    // TODO: Add catch code
	    nsae.printStackTrace();
	} catch (NoSuchPaddingException nspe) {
	    // TODO: Add catch code
	    nspe.printStackTrace();
	}
	return originalString;
    }

    /**
     * Método que sera invocado por las clases para encriptar texto plano
     * 
     * @param login
     *            Valor utilizado para generar la clave de cifrado
     * @param plaintext
     *            Texto plano
     * @return Texto cifrado
     * @throws DecoderException
     */

    public String encripcion(String plaintext) throws DecoderException {
	AES tmp = new AES();
	aesKey = tmp.generaKey(encripcion.Encription());
	AES encriptar = new AES(aesKey);
	return new BASE64Encoder().encode(encriptar.encryAES(plaintext).getBytes());

    }

    /**
     * Método que sera invocado por las clases para desencriptar texto cifrado
     * 
     * @param encrytext
     *            Texto cifrado con AES 256
     * @return Texto plano
     */

    public String desencripcion(String encrytext) {
	AES encriptar = new AES(aesKey);
	return encriptar.descryAES(encrytext);
    }

    public static void main(String args[]) throws DecoderException {
	AES aes = new AES();
	String[] arrelgo = { "AAQM610917QJA", "AUAC4601138F9", "BAJF541014RB3", "BAJS721028S88", "CALJ581101M37",
		"LAN7008173R5", "LAN8507268IA", "MAG041126GT8", "MAR980114GQA", "MSE061107IA8", "PZA000413788",
		"SUL010720JN8", "TCM970625MB1", "TME960709LR2", "TUCA2107035N9", "TUCA5703119R5", "ULC051129GC0",
		"URU070122S28", "VAAM130719H60", "VOC990129I26" };

	String rfc = "VOC990129I26";

	/*******************************************************************************/
	TwUsuario user = new TwUsuario();
	user.setRfce(aes.encripcion("VOC990129I26"));
	user.setPrimerasession(aes.encripcion("0"));
	user.setPassword(aes.encripcion("Prueba123."));

	TcPerfil perfil = new TcPerfil();
	perfil.setNombre(aes.encripcion("Admin"));
	perfil.setDescripcion(aes.encripcion("perfil para administradores"));

	TcAcciones accion = new TcAcciones();
	accion.setNombre(aes.encripcion("#comprobante"));
	accion.setAccion(aes.encripcion("M�dulo para comprobantes"));

	user.setTcPerfil(perfil);
	/*******************************************************************************/
	// String rfc = "LAN7008173R5";
	// String text = "Basic123.";
	/******************************************************************************/
	System.out.println("PROCESANDO...");
	System.out.println(aes.encripcion(rfc));
	/*******************************************************************************/
	System.out.println("RFC======> " + user.getRfce());
	System.out.println("PSESSION=> " + user.getPrimerasession());
	System.out.println("PASS=====> " + user.getPassword());
	System.out.println("PASS=====> " + user.getTcPerfil().getNombre());

	System.out.println("P_NOMB===> " + perfil.getNombre());
	System.out.println("P_DESC===> " + perfil.getDescripcion());

	System.out.println("A_NOMB===> " + accion.getNombre());
	System.out.println("A_ACCT===> " + accion.getAccion());

	// System.out.println(aes.encripcion(rfc, rfc));
	// System.out.println(aes.desencripcion("Y2FiZTYyNjcyMDk5NDQ3MTAzNzdkYmQ4YzEwMWNiYTg="));
	// for (String rfc : arrelgo) {
	// System.out.println(rfc + " - " + aes.encripcion(rfc, rfc) + " - "
	// +
	// aes.desencripcion("ZWM4MmJjMmU1MDU5MDBiZGE4Yjc4NjE2NDQyMGMwNjU=")
	// + " - "
	// +
	// aes.desencripcion("YzQ0ZGI2ZTM0NzBlMmZlM2FlZDc5M2RhMWM1Yzg0MTk=")
	// + " - "
	// +
	// aes.desencripcion("ODdhNGU2M2VjYTA3NTVmZTE5NmU4YjdlZDQyYzVlNjg=")
	// );
	// }
    }
}