package com.mx.quadrum.basic.aes;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Encoder;

import com.mx.quadrum.basic.util.Utilerias;

/**
 * Clase que permite realizar la encripcion de datos utilizando SHA 512
 */

public class EncripcionDatos {
    private String cipher, sha1;
    private String strCipherText;
    private String strDecryptedText;

    public EncripcionDatos() {
	super();
    }

    public String Encription() {
	// se implementa sha1 del rfc para generar la key
	sha1 = Sha512(Utilerias.login);
	try {
	    // se genera la llave
	    SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
	    DESKeySpec kspec = new DESKeySpec(sha1.getBytes());
	    SecretKey ks = skf.generateSecret(kspec);
	    // se crea el objecto Cipher con sus respectivos parametros
	    Cipher desCipher = Cipher.getInstance("DES");
	    // se inicializa el Cipher para encriptar
	    desCipher.init(Cipher.ENCRYPT_MODE, ks);
	    // se encriptan los datos
	    byte[] byteDataToEncrypt = ".1n1v!9a9e#8o8a?".getBytes();
	    byte[] byteCipherText = desCipher.doFinal(byteDataToEncrypt);
	    strCipherText = new BASE64Encoder().encode(byteCipherText);

	} catch (Exception e) {
	    e.printStackTrace();
	}
	return strCipherText;
    }

    public String DesEncription(String encrypt) {
	// se implementa el sha1 del rfc para generar la key
	sha1 = Sha512(Utilerias.login);
	try {
	    // se genera la llave
	    SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
	    DESKeySpec kspec = new DESKeySpec(sha1.getBytes());
	    SecretKey ks = skf.generateSecret(kspec);
	    // se crea el objecto Cipher con sus respectivos parametros
	    Cipher desCipher = Cipher.getInstance("DES");
	    // se inicializa el Cipher para desencriptar
	    desCipher.init(Cipher.DECRYPT_MODE, ks, desCipher.getParameters());
	    byte[] byteDecryptedText = new sun.misc.BASE64Decoder().decodeBuffer(encrypt);
	    byte[] decryptedBytes = desCipher.doFinal(byteDecryptedText);
	    strDecryptedText = new String(decryptedBytes);

	} catch (Exception e) {
	    e.printStackTrace();
	}
	return strDecryptedText;
    }

    private String Sha512(String plaintext) {
	try {
	    MessageDigest md = MessageDigest.getInstance("SHA1");
	    md.update(plaintext.getBytes());
	    byte[] cip = md.digest();
	    cipher = bytesToHex(cip);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return cipher;
    }

    private String bytesToHex(byte[] b) {
	char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	StringBuffer buf = new StringBuffer();
	for (int j = 0; j < b.length; j++) {
	    buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
	    buf.append(hexDigit[b[j] & 0x0f]);
	}
	return buf.toString();
    }
}