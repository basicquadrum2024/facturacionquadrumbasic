package com.mx.quadrum.basic.aes;

/**
 * Clase que almacenara la clave de cifrado utlizada en el algoritmo AES 256
 */

public class AESKey {
    private String encoded;

    // ******** SETTERS Y GETTERS *********
    public String getEncoded() {
        return encoded;
    }

    public void setEncoded(String encoded) {
        this.encoded = encoded;
    }
}
