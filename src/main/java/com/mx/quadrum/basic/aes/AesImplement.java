package com.mx.quadrum.basic.aes;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.Entity;

import org.springframework.stereotype.Repository;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

//import com.mx.quadrum.basic.entity.Metodopago;
//import com.mx.quadrum.basic.util.SessionApp;
import com.mx.quadrum.basic.util.Utilerias;

@Repository
public class AesImplement implements AesServices {

    private String ALGORITMO = "AES"; // algoritmo
    private String CODIFICACION = "UTF-8"; // como se convertira a byte, esto
					   // sera mas adelante
    private AESKey aesKey;
    private EncripcionDatos encripcion = new EncripcionDatos();

    // private String claveKey;

    @Override
    public AESKey generaKey(String cadena) {
	AESKey aesKey = new AESKey();
	aesKey.setEncoded(HexToString(cadena.getBytes()));
	return aesKey;
    }

    @Override
    public String encryAES(String cadena) {
	String encriptado = null;
	try {
	    byte[] raw = StringToHex(aesKey.getEncoded());
	    SecretKeySpec skeySpec = new SecretKeySpec(raw, ALGORITMO);
	    Cipher cipher = Cipher.getInstance(ALGORITMO);
	    cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
	    byte[] encrypted = cipher.doFinal(cadena.getBytes(CODIFICACION));
	    encriptado = HexToString(encrypted);
	} catch (UnsupportedEncodingException uee) {
	    // TODO: Add catch code
	    uee.printStackTrace();
	} catch (InvalidKeyException ike) {
	    // TODO: Add catch code
	    ike.printStackTrace();
	} catch (BadPaddingException bpe) {
	    // TODO: Add catch code
	    bpe.printStackTrace();
	} catch (IllegalBlockSizeException ibse) {
	    // TODO: Add catch code
	    ibse.printStackTrace();
	} catch (NoSuchAlgorithmException nsae) {
	    // TODO: Add catch code
	    nsae.printStackTrace();
	} catch (NoSuchPaddingException nspe) {
	    // TODO: Add catch code
	    nspe.printStackTrace();
	}
	return encriptado;
    }

    @Override
    public String HexToString(byte[] arregloEncriptado) {
	String textoEncriptado = "";
	for (int i = 0; i < arregloEncriptado.length; i++) {
	    int aux = arregloEncriptado[i] & 0xff;
	    if (aux < 16) {
		textoEncriptado = textoEncriptado.concat("0");
	    }
	    textoEncriptado = textoEncriptado.concat(Integer.toHexString(aux));
	}
	return textoEncriptado;
    }

    @Override
    public byte[] StringToHex(String encriptado) {
	byte[] enBytes = new byte[encriptado.length() / 2];
	for (int i = 0; i < enBytes.length; i++) {
	    int index = i * 2;
	    String aux = encriptado.substring(index, index + 2);
	    int v = Integer.parseInt(aux, 16);
	    enBytes[i] = (byte) v;
	}
	return enBytes;
    }

    @Override
    public String descryAES(String encriptado) {
	String originalString = null;
	try {
	    byte[] raw = StringToHex(aesKey.getEncoded());
	    SecretKeySpec skeySpec = new SecretKeySpec(raw, ALGORITMO);
	    Cipher cipher = Cipher.getInstance(ALGORITMO);
	    cipher.init(Cipher.DECRYPT_MODE, skeySpec);
	    byte[] original = cipher.doFinal(StringToHex(new String(new BASE64Decoder().decodeBuffer(encriptado))));
	    originalString = new String(original, CODIFICACION);
	} catch (InvalidKeyException ike) {
	    // TODO: Add catch code
	    ike.printStackTrace();
	} catch (BadPaddingException bpe) {
	    // TODO: Add catch code
	    bpe.printStackTrace();
	} catch (IllegalBlockSizeException ibse) {
	    // TODO: Add catch code
	    ibse.printStackTrace();
	} catch (IOException ioe) {
	    // TODO: Add catch code
	    ioe.printStackTrace();
	} catch (NoSuchAlgorithmException nsae) {
	    // TODO: Add catch code
	    nsae.printStackTrace();
	} catch (NoSuchPaddingException nspe) {
	    // TODO: Add catch code
	    nspe.printStackTrace();
	}
	return originalString;
    }

    @Override
    public String encriptaCadena(String plaintext) {
	aesKey = generaKey(encripcion.Encription());
	return new BASE64Encoder().encode(encryAES(plaintext).getBytes());
    }

    @Override
    public String desEncriptaCadena(String encrytext) {
	return descryAES(encrytext);
    }

    @Override
    public Object desEncritaObject(Object object) {
	Object obj2 = null;
	try {
	    if (object != null) {
		Class classObject = object.getClass();
		Field[] objectFields = classObject.getDeclaredFields();
		Boolean enciBoolean = null;
		if (validaencri(object)) {
		    Field atributo = object.getClass().getDeclaredField(Utilerias.CAMPO_ENCRIP_TRAN);
		    atributo.setAccessible(true);
		    enciBoolean = (Boolean) atributo.get(object);
		}
		if (enciBoolean != null && !enciBoolean) {
		    Field atributo = object.getClass().getDeclaredField(Utilerias.CAMPO_ENCRIP_TRAN);
		    atributo.setAccessible(true);
		    atributo.set(object, true);
		    validateFieldObject(objectFields, object);
		} else if (enciBoolean == null) {
		    validateFieldObject(objectFields, object);
		}
		obj2 = object;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
	return obj2;
    }

    @Override
    public Object encriptaObject(Object object) {
	try {
	    if (object != null) {

		Class classObject = object.getClass();
		Field[] objectFields = classObject.getDeclaredFields();
		for (Field f : objectFields) {
		    f.setAccessible(true);
		    if (f.getType().isAssignableFrom(String.class) && !f.getName().equals("codigo")) {
			if (f.get(object) != null) {
			    String value = (String) f.get(object);
			    f.set(object, encriptaCadena(value));
			}
		    }
		}

	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
	return object;
    }

    @Override
    public <T> List<T> desEncriptaListObject(List<T> listObjetc) {
	try {

	    if (listObjetc != null && listObjetc.size() > 0) {
		for (T t : listObjetc) {

		    Class classojb = t.getClass();
		    Field[] listaFields = classojb.getDeclaredFields();
		    if (validaencri(t)) {
			Field atributo = t.getClass().getDeclaredField(Utilerias.CAMPO_ENCRIP_TRAN);
			atributo.setAccessible(true);
			atributo.set(t, true);
		    }
		    for (Field f : listaFields) {
			f.setAccessible(true);
			if (f.getType().isAssignableFrom(String.class) && !f.getName().equals("codigo")) {
			    String value = (String) f.get(t);
			    if (value != null) {
				f.set(t, desEncriptaCadena(value));
			    }
			} else if (f.getType().isAnnotationPresent(Entity.class)) {
			    Object nuevoObjeto = f.get(t);
			    if (nuevoObjeto != null && nuevoObjeto.getClass().isAnnotationPresent(Entity.class)) {
				desEncritaObject(nuevoObjeto);
			    }
			}
		    }

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return listObjetc;
    }

    private boolean validaencri(Object object) {
	boolean salida = false;
	try {
	    Field atributo = object.getClass().getDeclaredField(Utilerias.CAMPO_ENCRIP_TRAN);
	    atributo.setAccessible(true);
	    if (atributo.getType().isAssignableFrom(Boolean.class)) {
		salida = true;
	    }
	} catch (Exception e) {
	    salida = false;
	}
	return salida;
    }

    private void validateFieldObject(Field[] objectFields, Object object) throws Exception {
	for (int i = 0; i < objectFields.length; i++) {
	    objectFields[i].setAccessible(true);
	    if (objectFields[i].getType().isAssignableFrom(String.class)) {
		String value = (String) objectFields[i].get(object);
		if (value != null) {
		    objectFields[i].set(object, desEncriptaCadena(value));
		}
	    } else if (objectFields[i].getType().isAnnotationPresent(Entity.class)) {
		Object objetoTmp = objectFields[i].get(object);
		if (objetoTmp != null && objetoTmp.getClass().isAnnotationPresent(Entity.class)) {
		    desEncritaObject(objetoTmp);
		}
	    }
	}
    }

}
