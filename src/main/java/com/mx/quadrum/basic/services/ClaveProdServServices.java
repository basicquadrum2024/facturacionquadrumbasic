package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.ClaveProdServ;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ClaveProdServServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 24523501519873038L;

	@Autowired
	private EDaoCatalogos edaoCatalogos;

	/**
	 * Metodo para obtener una lista de la ClaveProdServ con la restringcion de
	 * descripcion.
	 * 
	 * @param descripcion
	 * @return Retorna una lista del objeto ClaveProdServ
	 * @throws Exception
	 */
	public List<ClaveProdServ> buscarClaveProdServLikeNombre(final String descripcion) throws Exception {
		return edaoCatalogos.find(ClaveProdServ.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				c.add(Restrictions.or(Restrictions.like("descripcion", descripcion, MatchMode.START),
						Restrictions.like("clave", descripcion, MatchMode.START)));
				c.setMaxResults(5);
			}
		});
	}

	/**
	 * Metodo para buscar ClaveProdServ con la restringcion de clave
	 * 
	 * @param clave
	 * @return retorna una lista del objeto ClaveProdServ
	 * @throws Exception
	 */
	public ClaveProdServ buscarClaveProdServClave(final String clave) throws Exception {
		ClaveProdServ claveProdServ = null;
		List<ClaveProdServ> lista = edaoCatalogos.find(ClaveProdServ.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				c.add(Restrictions.eq("clave", clave));
			}
		});

		if (lista != null && !lista.isEmpty()) {
			claveProdServ = lista.get(0);
		}

		return claveProdServ;
	}

}
