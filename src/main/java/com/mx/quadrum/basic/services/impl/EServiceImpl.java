package com.mx.quadrum.basic.services.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.services.EService;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EServiceImpl implements EService {

	@Autowired
	private EDao edao;

	@SuppressWarnings("unchecked")
	public EServiceImpl() {

	}

	public EDao getEdao() {
		return edao;
	}

	public void setEdao(EDao edao) {
		this.edao = edao;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public <E> void saveOrupdate(E entity) throws Exception {
		edao.saveOrupdate(entity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public <E> void delete(E entity) throws Exception {
		edao.delete(entity);

	}

	@Override
	public <E> E find(Class klass, Serializable key) throws Exception {

		return edao.find(klass, key);
	}

	@Override
	public <E> List<E> findAll(Class klass) throws Exception {
		return edao.findAll(klass);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public <E> Serializable save(E entity) throws Exception {
		return edao.save(entity);
	}

	@Override
	public <E> E merge(E entity) throws Exception {

		return edao.merge(entity);
	}

	@Override
	public <E> E find(Class klass, Serializable id, Criterion... criterions) throws Exception {

		return edao.find(klass, id, criterions);
	}

	@Override
	public <E> E findFetch(Class klass, Serializable id, String... fetch) throws Exception {

		return edao.findFetch(klass, id, fetch);
	}

	@Override
	public <E> List<E> findAll(Class klass, Criterion... criterions) throws Exception {

		return edao.findAll(klass);
	}

	@Override
	public <E> List<E> examples(Class klass, Order order, String... excludes) throws Exception {

		return edao.examples(klass, order, excludes);
	}

	@Override
	public <E> List<E> list(Class klass, String hql) throws Exception {

		return edao.list(klass, hql);
	}

	@Override
	public <E> List<E> exactExamples(Class klass) throws Exception {

		return edao.exactExamples(klass);
	}

	@Override
	public <E> List<E> findRange(Class klass, int[] range, Order... orders) throws Exception {

		return edao.findRange(klass, range, orders);
	}

	@Override
	public <E> int count(Class klass) throws Exception {

		return edao.count(klass);
	}

	@Override
	public <E> int excuteQueryHql(String hql) throws Exception {

		return edao.excuteQueryHql(hql);
	}

	@Override
	public <E> List<E> excuteQueryHqlList(String hql) throws Exception {

		return edao.excuteQueryHqlList(hql);
	}

	@Override
	public <E> List<E> find(Class klass, CriteriaEstrategia interceptor) throws Exception {
		return edao.find(klass, interceptor);
	}

	@Override
	public <E> E findUnique(Class klass, CriteriaEstrategia interceptor) throws Exception {

		return edao.findUnique(klass, interceptor);
	}

	@Override
	public <E> List<E> findAcciones(Class klass, CriteriaEstrategia interceptor) throws Exception {

		return edao.findAcciones(klass, interceptor);
	}

	@Override
	public <E> int countComprobante(Class klass, CriteriaEstrategia interceptor) throws Exception {

		return edao.countComprobante(klass, interceptor);
	}

	@Override
	public <T> T findById(Integer valueId, Class klass, String... fetch) throws Exception {

		return edao.findById(valueId, klass, fetch);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public <E> Serializable getFolio(Object serie) throws Exception {
		return edao.getFolio(serie);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public <E> void update(E entity) throws Exception {
		edao.update(entity);
	}

}
