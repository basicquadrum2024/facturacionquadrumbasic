package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TcReceptorServices {

    static final Logger LOGGER = Logger.getLogger(TcReceptor.class);

    @Autowired
    private EDao dao;

    @Autowired
    private AesServices aesServices;

    /**
     * Metodo encargado de hacer una busqueda de la entidad TcReceptor en la
     * base de datos, recibe el rfc del receptor
     * 
     * @param rfc
     * @return
     * @throws Exception
     */
    public TcReceptor buscaTcReceptorPorRFC(final String rfc) throws Exception {
	return dao.findUnique(TcReceptor.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		//c.createAlias("tcDomicilio", "dom", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("rfc", aesServices.encriptaCadena(rfc)));
	    }
	});
    }

    /**
     * Metodo para guardar una entidad TcReceptor
     * @param tcReceptor
     * @return Serializable
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Serializable guardarTcReceptor(final TcReceptor tcReceptor) throws Exception {
	return dao.save(tcReceptor);
    }

    /**
     * Metodo para guardar o actualizar la entidad TcReceptor
     * @param tcReceptor
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void guardarActualizarTcReceptor(final TcReceptor tcReceptor) throws Exception {
	dao.saveOrupdate(tcReceptor);
    }

    /**
     * Metodo que busca los registros de la entidad TcReceptor
     * @return
     * @throws Exception
     */
    public List<TcReceptor> buscarTodosTcReceptores() throws Exception {
	return dao.find(TcReceptor.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria cliteria) {

	    }
	});
    }
}
