package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.TipoFactor;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TipoFactorServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 969188059580336726L;

    @Autowired
    private EDaoCatalogos edaoCatalogos;

    /**
     * Metodo para buscar un listado de la entidad TipoFactor por un arreglo de claves
     * @param claves
     * @return List<TipoFactor>
     * @throws Exception
     */
    public List<TipoFactor> buscarTiposFactor(final String[] claves) throws Exception {
	return edaoCatalogos.find(TipoFactor.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.not(Restrictions.in("clave", claves)));
	    }
	});
    }
}
