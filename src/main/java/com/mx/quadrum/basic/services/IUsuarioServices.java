package com.mx.quadrum.basic.services;

import java.util.Date;
import java.util.List;

import com.mx.quadrum.basic.entity.TcPerfil;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.wrapper.Paginacion;

public interface IUsuarioServices<E> {

    List<TwUsuario> findAllUsuarios(String h) throws Exception;

    TwUsuario findUsuario() throws Exception;

    void Editar(TwUsuario u) throws Exception;

    public void saveorUpdate(TwUsuario usuario) throws Exception;

    public void delete(TwUsuario usuario) throws Exception;

    TwUsuario findbyCorreo(String correo) throws Exception;

    TwUsuario findbyRfcTemporal(String rfcTemporal) throws Exception;

    Paginacion getPaginacionBus(Paginacion pagina, final String campoBetween, final Date date1, final Date date2,
	    boolean b) throws Exception;

    public TwUsuario usuarioById(final Integer id) throws Exception;

    boolean validarRfcTemporal(String rfc) throws Exception;

    public TwUsuario usuarioByRFC(final String rfc) throws Exception;

    int getUpdateIntentos(String encriptaCadena, int i) throws Exception;

    void estatusCuenta(String encriptaCadena) throws Exception;

    List<TwUsuario> findUsuarioPagination(int startPosition, int maxResults) throws Exception;

    Integer findUsuarioPaginationCount() throws Exception;

    List<TcPerfil> buscarPerfiles() throws Exception;
}
