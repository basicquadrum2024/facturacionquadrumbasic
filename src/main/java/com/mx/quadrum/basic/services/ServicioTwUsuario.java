package com.mx.quadrum.basic.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.IUsuarioDao;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ServicioTwUsuario {

	@Autowired
	private IUsuarioDao dao;

	public void actualizarFechaIntenos(String username) throws Exception {
		dao.getUpdateIntentos(username, 0);
	}
}
