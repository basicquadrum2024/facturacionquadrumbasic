package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.TipoRelacion;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TipoRelacionServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4780333701972963387L;

    @Autowired
    private EDaoCatalogos edaoCatalogos;

    /**
     * Metodo para buscar un listado de la entidad TipoRelacion
     * @return List<TipoRelacion>
     * @throws Exception
     */
    public List<TipoRelacion> buscarTipoRelacion() throws Exception {
	return edaoCatalogos.find(TipoRelacion.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {

	    }
	});

    }

}
