package com.mx.quadrum.basic.services;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoImpl;
import com.mx.quadrum.basic.entity.Parametros;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ServicioParametros {

	@Autowired
	private EDaoImpl daoImpl;
	
	public Parametros buscarParametros(final String nombre)throws Exception {
		return daoImpl.findUnique(Parametros.class, new CriteriaEstrategia() {
			
			@Override
			public void estrategia(Criteria cliteria) throws Exception {
				cliteria.add(Restrictions.eq("nombre", nombre));
				
			}
		});
	}
}
