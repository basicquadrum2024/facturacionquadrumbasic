package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.Moneda;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class MonedaServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4845137988488130212L;

    @Autowired
    private EDaoCatalogos edaoCatalogos;

    /**
     * Metodo para buscar monedas sat
     * @return List<Moneda>
     * @throws Exception
     */
    public List<Moneda> buscarMonedas() throws Exception {
	return edaoCatalogos.find(Moneda.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {

	    }
	});
    }

    /**
     * Metodo para buscar el numero de decimales del tipo de moneda
     * @param clave
     * @return Integer
     * @throws Exception
     */
    public Integer buscarMonedasPorClave(final String clave) throws Exception {
	Moneda moneda = edaoCatalogos.findUnique(Moneda.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("clave", clave));
	    }
	});
	return moneda.getDecimales();
    }
}
