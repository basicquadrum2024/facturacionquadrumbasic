package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TcConceptos;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TcConceptosServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4106095053103288818L;

	@Autowired
	private EDao dao;

	@Autowired
	private AesServices aesServices;

	/**
	 * Metodo para buscar una lista de conceptos mediante la restriccion de la
	 * entidad contribuyente
	 * 
	 * @param tcContribuyente
	 * @return retorna una lista de la entidad TcConceptos
	 * @throws Exception
	 */
	public List<TcConceptos> buscarConceptosPorContribuyente(final TcContribuyente tcContribuyente) throws Exception {
		return dao.find(TcConceptos.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				c.add(Restrictions.eq("tcContribuyente", tcContribuyente));
			}
		});
	}

	/**
	 * Metodo para buscar concpeto mediante la restriccion de descripcion
	 * 
	 * @param descripcion
	 * @param tcContribuyente
	 * @return retorna un objeto de la entidad TcContribuyente
	 * @throws Exception
	 */
	public synchronized TcConceptos buscarConceptoPorDescripcionContribuyente(final String descripcion,
			final TcContribuyente tcContribuyente) throws Exception {
		return dao.findUnique(TcConceptos.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				c.add(Restrictions.eq("descripcion", aesServices.encriptaCadena(descripcion)));
				c.add(Restrictions.eq("tcContribuyente", tcContribuyente));
			}
		});
	}

	/**
	 * Metodo para guardar la entidad de TcConcepto
	 * 
	 * @param tcConceptos
	 * @return retorna uan entidad Serializable
	 * @throws Exception
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Serializable guardarTcConcepto(final TcConceptos tcConceptos) throws Exception {
		return dao.save(tcConceptos);
	}

}
