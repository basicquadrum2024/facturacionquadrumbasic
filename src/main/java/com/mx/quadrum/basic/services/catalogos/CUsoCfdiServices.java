package com.mx.quadrum.basic.services.catalogos;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.UsoCfdi;
import com.mx.quadrum.basic.util.CriteriaEstrategia;
/**
 * Clase @Service contiene m�todos de negocio para la clase/objeto UsoCfdi
 * @author 
 *
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CUsoCfdiServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6935034670338594269L;

    @Autowired
    private EDaoCatalogos daoCatalogos;

    public List<UsoCfdi> buscarUsoCfdiPorTipoPersona(final String[] tipoPersona) throws Exception {
	return daoCatalogos.find(UsoCfdi.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.in("tipoPersona", tipoPersona));
	    }
	});
    }
}
