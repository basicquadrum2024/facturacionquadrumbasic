package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.entity.UsuarioPrincipal;
import com.mx.quadrum.basic.util.CriteriaEstrategia;
import com.mx.quadrum.basic.util.Utilerias;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EmisorServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(EmisorServices.class);

	@Autowired
	private EDao edao;

	@Autowired
	private IUsuarioServices usuarioServices;

	/**
	 * Metodo que buscar una entidad TcContribuyente por rfc
	 * 
	 * @param rfc
	 * @return TcContribuyente
	 * @throws Exception
	 */
	public TcContribuyente buscarPorRfcEmisor(final String rfc) throws Exception {
		TcContribuyente emisor = edao.findUnique(TcContribuyente.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				// c.createAlias("tcDomicilio", "domiemisor");// inner join
				c.add(Restrictions.eq("rfc", rfc));// where
			}
		});
		return emisor;

	}

	/**
	 * Metodo que busca una entidad TcContribuyente si existe retorna true si no
	 * existe retorna falso
	 * 
	 * @param rfc
	 * @return boolean
	 * @throws Exception
	 */
	public boolean buscarPorRfcE(final String rfc) throws Exception {
		boolean respuesta = false;
		TcContribuyente emisor = edao.findUnique(TcContribuyente.class, new CriteriaEstrategia() {
			@Override
			public void estrategia(Criteria c) {
				c.add(Restrictions.eq("rfc", rfc));// where
			}
		});
		if (emisor == null) {
			respuesta = false;
		} else {
			respuesta = true;
		}
		return respuesta;

	}

	/**
	 * Metodo para actualizar entidad TcContribuyente
	 * 
	 * @param emisor
	 * @return String[]
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public String[] actualizaEmisor(TcContribuyente emisor) throws Exception {
		String[] respuesta = new String[4];
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		TwUsuario usuario = Utilerias.getUserSession(auth);

		try {
			edao.saveOrupdate(emisor);
			if (usuario.getRfce() == null) {
				usuario.setRfce(emisor.getRfc());
				edao.saveOrupdate(usuario);
				usuario = usuarioServices.usuarioByRFC(usuario.getRfce());
				UsuarioPrincipal customUser = (UsuarioPrincipal) auth.getPrincipal();
				customUser.setUsuario(usuario);
			}
			respuesta[0] = "Succces";
			respuesta[1] = "El emisor fue agregado correctamente";
			respuesta[2] = usuario.getRfce();
		} catch (Exception e) {
			e.printStackTrace();
			respuesta[0] = "Error";
			respuesta[1] = "El emisor no fue agregado, verifique porfavor";
		}

		return respuesta;
	}

	/**
	 * Metodo para buscar todos clientes registrados devolviendo una lista por
	 * bloques
	 * 
	 * @param maxResult
	 * @param paginaInicial
	 * @return lista de clientes registrados
	 * @throws Exception
	 */
	public List<TcContribuyente> clientesRegistrados(final Integer paginaInicial, final Integer maxResult)
			throws Exception {
		return edao.find(TcContribuyente.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				//c.createCriteria("tcDomicilio");
				c.setFirstResult(paginaInicial);
				c.setMaxResults(maxResult);
			}
		});
	}

	/**
	 * metodo que busca todos los clientes registrados.
	 * 
	 * @return List<TcContribuyente>
	 * @throws Exception
	 */
	public List<TcContribuyente> clientesRegistrados() throws Exception {
		return edao.find(TcContribuyente.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				c.addOrder(Order.asc("rfc"));
			}
		});
	}

	/**
	 * total de Emisores agregados
	 * 
	 * @return
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Long buscaClientesRegistradosCount() throws Exception {
		return (Long) edao.find(TcContribuyente.class, new CriteriaEstrategia() {
			@Override
			public void estrategia(Criteria c) {
				//c.createCriteria("tcDomicilio");
				c.setProjection(Projections.projectionList().add(Projections.rowCount()));
				c.uniqueResult();
			}
		}).get(0);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void actualizarDatosContribuyente(final Long id,final String llaveCer, final String llaveKey) throws Exception{
		String hql="update TcContribuyente set llaveCer=:llaveCer, llaveKey=:llaveKey where id=:id";
		HashMap<String, Object> param=new HashMap<>();
		param.put("llaveCer", llaveCer);
		param.put("llaveKey", llaveKey);
		param.put("id", id);
		edao.excuteQueryHqlParam(hql, param);
	}
}
