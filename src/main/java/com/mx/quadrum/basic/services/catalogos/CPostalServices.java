package com.mx.quadrum.basic.services.catalogos;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.CodigoPostal;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CPostalServices implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 8461321185326118889L;
	@Autowired
	    private EDaoCatalogos daoCatalogos;
	
	/**
	 * Servicio para buscar codigo postal en el catalogo de codigoPostal
	 * @param recibe una variable de tipo String que es el codigo postal a buscar
	 * @return retorna objeto de la entidad CodigoPostal
	 * @throws Exception
	 */
	  public CodigoPostal buscarCodigoPostal(final String lugarExpedicion) throws Exception {
			CodigoPostal codigoPostal = daoCatalogos.findUnique(CodigoPostal.class, new CriteriaEstrategia() {

			    @Override
			    public void estrategia(Criteria c) {
				c.add(Restrictions.eq("codigoPostal", lugarExpedicion));// where
			    }
			});
			return codigoPostal;

		    }
	
	

}
