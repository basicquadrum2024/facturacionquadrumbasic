package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoPatronesImpl;
import com.mx.quadrum.basic.entity.patronesSAT.PatronesCfdi;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PatronesCfdiServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 57733324904361223L;

    @Autowired
    private EDaoPatronesImpl daoPatronesImpl;

    /**
     * Metodo para lista de patrones de cfdi
     * @return retorna listaPatrones
     * @throws Exception
     */
    public List<PatronesCfdi> listaPatrones() throws Exception {
	return daoPatronesImpl.findAll(PatronesCfdi.class);
    }

}