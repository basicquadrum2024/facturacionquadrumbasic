package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.quadrum.basic.util.ParametroEnum;

@Service
public class RmiServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 777081515016821638L;

	@SuppressWarnings("unchecked")
	public <T> T obtenerObjeto(final Class<?> clase, final String nombreMetodoInvocar, final Object... parametros)
			throws Exception {
		JMXServiceURL url = new JMXServiceURL(ParametroEnum.SERVICIO_RMI_SAT.getValue());
		JMXConnector jmxc = JMXConnectorFactory.connect(url, null);

		MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
		ObjectName mbeanName = new ObjectName(ParametroEnum.NOMBRE_BEAN_REMOTO_SAT.getValue());
		String arregloTipoParametros[] = null;
		List<String> tipoParametros = null;
		if (null != parametros) {
			tipoParametros = new ArrayList<>();
			for (Object object : parametros) {
				tipoParametros.add(object.getClass().getName());
			}
			arregloTipoParametros = tipoParametros.toArray(new String[tipoParametros.size()]);
		}
		String consulta = (String) mbsc.invoke(mbeanName, nombreMetodoInvocar, parametros, arregloTipoParametros);
		jmxc.close();
		return (StringUtils.isNotBlank(consulta)) ? (T) new ObjectMapper().readValue(consulta, clase) : null;
	}

	public <T> List<T> obtenerObjetos(final Class<?> clase, final String nombreMetodoInvocar,
			final Object... parametros) throws Exception {
		JMXServiceURL url = new JMXServiceURL(ParametroEnum.SERVICIO_RMI_SAT.getValue());
		JMXConnector jmxc = JMXConnectorFactory.connect(url, null);

		MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
		ObjectName mbeanName = new ObjectName(ParametroEnum.NOMBRE_BEAN_REMOTO_SAT.getValue());
		String arregloTipoParametros[] = null;
		List<String> tipoParametros = null;
		if (null != parametros) {
			tipoParametros = new ArrayList<>();
			for (Object object : parametros) {
				tipoParametros.add(object.getClass().getName());
			}
			arregloTipoParametros = tipoParametros.toArray(new String[tipoParametros.size()]);
		}
		String consulta = (String) mbsc.invoke(mbeanName, nombreMetodoInvocar, parametros, arregloTipoParametros);
		jmxc.close();

		T[] arreglo = (StringUtils.isNotBlank(consulta)) ? (T[]) new ObjectMapper().readValue(consulta, clase) : null;
		return (null != arreglo) ? Arrays.asList(arreglo) : null;
	}

	@SuppressWarnings("unchecked")
	public <T> T obtenerObjetoV40(final Class<?> clase, final String nombreMetodoInvocar, final Object... parametros)
			throws Exception {
		JMXServiceURL url = new JMXServiceURL(ParametroEnum.SERVICIO_RMI_SAT_V4.getValue());
		JMXConnector jmxc = JMXConnectorFactory.connect(url, null);

		MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
		ObjectName mbeanName = new ObjectName(ParametroEnum.NOMBRE_BEAN_REMOTO_SAT_V4.getValue());
		String arregloTipoParametros[] = null;
		List<String> tipoParametros = null;
		if (null != parametros) {
			tipoParametros = new ArrayList<>();
			for (Object object : parametros) {
				tipoParametros.add(object.getClass().getName());
			}
			arregloTipoParametros = tipoParametros.toArray(new String[tipoParametros.size()]);
		}
		String consulta = (String) mbsc.invoke(mbeanName, nombreMetodoInvocar, parametros, arregloTipoParametros);
		jmxc.close();
		return (StringUtils.isNotBlank(consulta)) ? (T) new ObjectMapper().readValue(consulta, clase) : null;
	}
	
	public <T> List<T> obtenerObjetosV40(final Class<?> clase, final String nombreMetodoInvocar,
			final Object... parametros) throws Exception {
		JMXServiceURL url = new JMXServiceURL(ParametroEnum.SERVICIO_RMI_SAT_V4.getValue());
		JMXConnector jmxc = JMXConnectorFactory.connect(url, null);

		MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
		ObjectName mbeanName = new ObjectName(ParametroEnum.NOMBRE_BEAN_REMOTO_SAT_V4.getValue());
		String arregloTipoParametros[] = null;
		List<String> tipoParametros = null;
		if (null != parametros) {
			tipoParametros = new ArrayList<>();
			for (Object object : parametros) {
				tipoParametros.add(object.getClass().getName());
			}
			arregloTipoParametros = tipoParametros.toArray(new String[tipoParametros.size()]);
		}
		String consulta = (String) mbsc.invoke(mbeanName, nombreMetodoInvocar, parametros, arregloTipoParametros);
		jmxc.close();

		T[] arreglo = (StringUtils.isNotBlank(consulta)) ? (T[]) new ObjectMapper().readValue(consulta, clase) : null;
		return (null != arreglo) ? Arrays.asList(arreglo) : null;
	}
}
