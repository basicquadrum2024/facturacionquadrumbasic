package com.mx.quadrum.basic.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.Exportacion;
import com.mx.quadrum.basic.entity.catalogosSAT.ObjetoImpuesto;
import com.mx.quadrum.basic.entity.catalogosSAT.RegimenFiscal;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CatalogosSATServices {
	
	@Autowired
	private EDaoCatalogos edaoCatalogos;
	
	
	/**
	 * Metodo que retorna una lista  del objeto Regimen Fiscal
	 * @param tipo F (personla fisica) M (persona Moral)
	 * @return lista del objeto RegimenFiscal
	 * @throws Exception
	 */
	public List<RegimenFiscal> buscarRegimenFiscal(final String[] tiposPersona) throws Exception {

		List<RegimenFiscal> tipoComprobantes = edaoCatalogos.find(RegimenFiscal.class, new CriteriaEstrategia() {
			@Override
			public void estrategia(Criteria c) {
				c.add(Restrictions.in("tipoPersona", tiposPersona));
				c.addOrder(Order.asc("descripcion"));
			}
		});
		return tipoComprobantes;
	}
	
	
	public List<Exportacion> buscarExportacion() throws Exception {

		List<Exportacion> listaExportacion = edaoCatalogos.find(Exportacion.class, new CriteriaEstrategia() {
			@Override
			public void estrategia(Criteria c) {
				
			}
		});
		return listaExportacion;
	}
	
	public List<ObjetoImpuesto> buscarObjetoImpuesto() throws Exception {

		List<ObjetoImpuesto> listaObjetoImpuesto = edaoCatalogos.find(ObjetoImpuesto.class, new CriteriaEstrategia() {
			@Override
			public void estrategia(Criteria c) {
				
			}
		});
		return listaObjetoImpuesto;
	}

}
