package com.mx.quadrum.basic.services;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TcDomicilio;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TcDomicilioServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1231150351551949667L;

    @Autowired
    private EDao dao;

    /**
     * Metodo para guardar la entidad de TcDocilio
     * @param tcDomicilio
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void guardarTcDomicilio(final TcDomicilio tcDomicilio) throws Exception {
	dao.saveOrupdate(tcDomicilio);
    }
}
