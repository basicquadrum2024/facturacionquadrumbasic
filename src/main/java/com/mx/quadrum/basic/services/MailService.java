package com.mx.quadrum.basic.services;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.mx.quadrum.basic.controller.AbstractControllerHandler;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

@Service
public class MailService extends AbstractControllerHandler {

    public static final Logger LOGGER = Logger.getLogger(MailService.class);

    @Autowired
    private JavaMailSender javaMailService;
    /** correo electrónico del remitente */
    private String from;

    public void setFrom(String from) {
	this.from = from;
    }

    public String getFrom() {
	//from = "soporte@quadrum.mx";
    	from = "sistemanotificacionp@gmail.com";
	return from;
    }

    /** flag para indicar si está activo el servicio */
    public boolean active = true;

    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
	this.active = active;
    }

    private static final File[] NO_ATTACHMENTS = null;

    /**
     * envￜo de email
     * 
     * @param to
     *            correo electrￜnico del destinatario
     * @param subject
     *            asunto del mensaje
     * @param text
     *            cuerpo del mensaje
     */
    public void send(String to, String subject, String text, String archivo, String name) {
	send(to, subject, text, archivo, name, NO_ATTACHMENTS);
    }

    /**
     * envￜo de email con attachments
     * 
     * @param to
     *            correo electrￜnico del destinatario
     * @param subject
     *            asunto del mensaje
     * @param text
     *            cuerpo del mensaje
     * @param attachments
     *            ficheros que se anexarￜn al mensaje
     */
    public boolean send(String to, String subject, String text, String archivo, String name, File... attachments) {
	boolean respuesta = false;
	final MimeMessage message = javaMailService.createMimeMessage();
	// final MimeMessage message = null;

	try {
	    final MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    helper.setTo(to);
	    helper.setSubject(subject);
	    helper.setFrom(getFrom());
	    helper.setText(text);
	    String ar = null;
	    if (attachments != null) {
		for (int i = 0; i < attachments.length; i++) {
		    FileSystemResource file = new FileSystemResource(attachments[i]);
		    if (attachments[i].getName().contains(".xml")) {
			ar = archivo;
		    }
		    if (attachments[i].getName().contains(".pdf")) {
			ar = name;
		    }
		    helper.addAttachment(ar, file);
		}
	    }
	    this.javaMailService.send(message);
	    respuesta = true;
	} catch (MessagingException e) {
	    respuesta = false;
	    LOGGER.error("ERROR en envio correo send---> " + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
	}
	return respuesta;
    }

    /**
     * envￜo de email
     * 
     * @param to
     *            correo electrￜnico del destinatario
     * @param subject
     *            asunto del mensaje
     * @param text
     *            cuerpo del mensaje
     */
    public void sendLayout(String to, String subject, String text, String archivo, String name) {
	sendLayout(to, subject, text, archivo, name, NO_ATTACHMENTS);
    }

    public boolean sendLayout(String to, String subject, String text, String archivo, String name, File... attachments) {
	boolean respuesta = false;
	final MimeMessage message = javaMailService.createMimeMessage();
	// final MimeMessage message = null;

	try {
	    final MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    helper.setTo(to);
	    helper.setSubject(subject);
	    helper.setFrom(getFrom());
	    helper.setText(text);
	    String ar = null;
	    if (attachments != null) {
		for (int i = 0; i < attachments.length; i++) {
		    FileSystemResource file = new FileSystemResource(attachments[i]);
		    if (attachments[i].getName().contains(".xml")) {
			ar = archivo;
		    }
		    // if (attachments[i].getName().contains(".pdf")) {
		    // ar = name;
		    // }
		    helper.addAttachment(ar, file);
		}
	    }
	    this.javaMailService.send(message);
	    respuesta = true;
	} catch (MessagingException e) {
	    respuesta = false;
	    LOGGER.error("ERROR en envio correo send---> " + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
	}
	return respuesta;
    }

    /**
     * METODO PARA ENVIAR EL ZIP MEDIANTE CORREO ELECTR�NICO
     * 
     * @param para
     * @param copia
     * @param asunto
     * @param texto
     * @param nameArchivo
     * @param array
     */
    public void enviarZip(String para, String copia, String asunto, String texto, String nameArchivo, byte[] array)
	    throws Exception {
	final MimeMessage message = javaMailService.createMimeMessage();

	try {
	    final MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    helper.setTo(para);
	    if (copia != null) {
		helper.setCc(copia);
	    }
	    helper.setSubject(asunto);
	    helper.setFrom(getFrom());
	    helper.setText(texto);
	    helper.addAttachment(nameArchivo, new ByteArrayResource(array));
	    this.javaMailService.send(message);

	} catch (Exception e) {
	}
    }
    
    
    /**
     * envￜo de email con attachments
     * 
     * @param to
     *            correo electrￜnico del destinatario
     * @param subject
     *            asunto del mensaje
     * @param text
     *            cuerpo del mensaje
     * @param attachments
     *            ficheros que se anexarￜn al mensaje
     */
    public boolean sendConCopia(String to, String copiaPara, String subject, String text, String archivo, String name, File... attachments) {
	boolean respuesta = false;
	final MimeMessage message = javaMailService.createMimeMessage();
	// final MimeMessage message = null;

	try {
	    final MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    helper.setTo(to);
	    if(copiaPara!=null){
	    	helper.setCc(copiaPara);
	    }
	    helper.setSubject(subject);
	    helper.setFrom(getFrom());
	    helper.setText(text);
	    String ar = null;
	    if (attachments != null) {
		for (int i = 0; i < attachments.length; i++) {
		    FileSystemResource file = new FileSystemResource(attachments[i]);
		    if (attachments[i].getName().contains(".xml")) {
			ar = archivo;
		    }
		    if (attachments[i].getName().contains(".pdf")) {
			ar = name;
		    }
		    helper.addAttachment(ar, file);
		}
	    }
	    this.javaMailService.send(message);
	    respuesta = true;
	} catch (MessagingException e) {
	    respuesta = false;
	    LOGGER.error("ERROR en envio correo send---> " + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
	}
	return respuesta;
    }

}