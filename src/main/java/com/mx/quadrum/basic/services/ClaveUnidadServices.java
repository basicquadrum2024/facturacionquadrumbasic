package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.ClaveUnidad;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ClaveUnidadServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5804979452962823407L;

	@Autowired
	private EDaoCatalogos edaoCatalogos;

	/**
	 * Metodo que busca Claves de Unidad por nombre
	 * 
	 * @param nombre
	 * @return Retorna una Lista del objeto ClaveUnidad
	 * @throws Exception
	 */
	public List<ClaveUnidad> buscarClavesUnidadLikeNombre(final String nombre) throws Exception {
		return edaoCatalogos.find(ClaveUnidad.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				c.add(Restrictions.or(Restrictions.like("nombre", nombre, MatchMode.START),
						Restrictions.like("clave", nombre, MatchMode.START)));
				c.setMaxResults(5);
			}
		});
	}

	/**
	 * Metodo que busca clave por clave
	 * @param clave
	 * @return retorna una lista de objeto Clave Unidad
	 * @throws Exception
	 */
	public ClaveUnidad buscarClavePorClave(final String clave) throws Exception {
		ClaveUnidad claveUnidad = null;
		List<ClaveUnidad> lista = edaoCatalogos.find(ClaveUnidad.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) {
				c.add(Restrictions.eq("clave", clave));
			}
		});

		if (lista != null && !lista.isEmpty()) {
			claveUnidad = lista.get(0);
		}

		return claveUnidad;
	}

}
