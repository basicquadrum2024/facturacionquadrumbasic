package com.mx.quadrum.basic.services;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Service;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.mx.quadrum.basic.documentos.ConvenioConfidencialidadPersonaFisica;
import com.mx.quadrum.basic.documentos.ConvenioConfidencialidadPersonaMoral;
import com.mx.quadrum.basic.documentos.FormatoContratoServiciosBasicoPersonaFisica;
import com.mx.quadrum.basic.documentos.FormatoContratoServiciosBasicoPersonaMoral;
import com.mx.quadrum.basic.documentos.ManifiestoPersonaFisica;
import com.mx.quadrum.basic.documentos.ManifiestoPersonaMoral;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

@Service
public class DocumentosLegalesServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2353095449641280974L;

	/**
	 * Metodo que retorna la cadena original del sello digital del contrato
	 * 
	 * @param emisor
	 * @param tipo
	 * @return
	 * @throws Exception
	 */
	public String obtenerCadenaOriginalSelloContrato(String tipo) throws Exception {
		return String.format("||%s|%s|%s|%s|%s|%s||", "CVD110412TF6",
				"Centro de Validaci\u00f3n Digital CVDSA, S.A. de C.V.", "PESL821028G87", "LAURA IVETE P\u00c9REZ SERAFIN",
				UtileriasCfdi.YYYYMMDDTHHMMSS.format(new Date()), tipo);
	}

	/**
	 * Metodo que retorna la cadena original del sello del contrato entre la empresa
	 * y el contribuyente
	 * 
	 * @param emisor
	 * @param tipo
	 * @return
	 * @throws Exception
	 */
	public String obtenerCadenaOriginal(TcContribuyente emisor, String selloDigitalContrato, String tipo)
			throws Exception {
		return String.format("||%s|%s|%s|%s|%s|%s|%s||", "CVD110412TF6",
				"Centro de Validaci\u00f3n Digital CVDSA, S.A. de C.V.", emisor.getRfc(), emisor.getNombre(),
				UtileriasCfdi.YYYYMMDDTHHMMSS.format(new Date()), selloDigitalContrato, tipo);
	}

	public byte[] obtenerContrato(TcContribuyente emisor, String cadenaOriginal, String firmaElectronica,
			String selloDigitalContrato) throws Exception {
		ConverterProperties properties = new ConverterProperties();
		properties.setBaseUri(ParametroEnum.CSSCONVENIOS.getValue());
		String html = "";
		Calendar desde = Calendar.getInstance();
		String mesletra = mes(desde);
		int anioletra = ano(desde);
		int dialetra = dia(desde);
		if (emisor.getRfc().length() == 12) {
			// Persona MORAL
			String fechaescriturapublica = UtileriasCfdi.FORMATO_ANIO_MES_DIA_GUION_MEDIO
					.format(emisor.getFechaEscrituraPublica());
			String fechadondequedoinscritafoliomercantil = UtileriasCfdi.FORMATO_ANIO_MES_DIA_GUION_MEDIO
					.format(emisor.getFechaInscritoFolioMercantil());
			String fechaaceptacion = UtileriasCfdi.FORMATO_ANIO_MES_DIA_GUION_MEDIO.format(new Date());
			html = FormatoContratoServiciosBasicoPersonaMoral.crearPdfPersonaMoral(emisor.getNombre(),
					emisor.getNumeroEscrituraPublica(), fechaescriturapublica, emisor.getNombreNotario(),
					emisor.getNumeroNotario(), emisor.getEntidadFederativaNotario(),
					emisor.getEntidadFederativaConstruyoEmpresa(), emisor.getFolioMercantil(),
					fechadondequedoinscritafoliomercantil, emisor.getNombre(), emisor.getNombreRepresentanteLegal(), "",
					emisor.getRfcRepresentanteLegalApoderado(), emisor.getNumeroEscrituraPublicaFacultades(),
					emisor.getNombreNotario(), emisor.getNumeroNotario(), emisor.getDomicilio(), emisor.getTelefono(),
					emisor.getCorreo(), emisor.getNombrePersonaContacto(), emisor.getCargoPersonaContacto(),
					fechaaceptacion, cadenaOriginal, dialetra, mesletra, anioletra, firmaElectronica,
					selloDigitalContrato);
		}
		if (emisor.getRfc().length() == 13) {
			String fechaconformidad = UtileriasCfdi.FORMATO_ANIO_MES_DIA_GUION_MEDIO.format(new Date());
			html = FormatoContratoServiciosBasicoPersonaFisica.crearPdfPersonaFisica(emisor.getNombre(),
					emisor.getIneIfe(), emisor.getFolioIne(), emisor.getEdad(), emisor.getDomicilio(), emisor.getRfc(),
					emisor.getTelefono(), emisor.getCorreo(), emisor.getNombrePersonaContacto(), fechaconformidad,
					cadenaOriginal, dialetra, mesletra, anioletra, firmaElectronica, selloDigitalContrato);
		}
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		HtmlConverter.convertToPdf(html, arrayOutputStream, properties);
		return arrayOutputStream.toByteArray();
	}

	public byte[] obtenerConvenio(TcContribuyente emisor, String cadenaOriginal, String firmaElectronica,String selloDigitalConvenio)
			throws Exception {
		ConverterProperties properties = new ConverterProperties();
		properties.setBaseUri(ParametroEnum.CSSCONVENIOS.getValue());
		String html = "";
		Calendar desde = Calendar.getInstance();

		String mesletra = mes(desde);
		int anioletra = ano(desde);
		int dialetra = dia(desde);
		if (emisor.getRfc().length() == 12) {
			// Persona MORAL
			String fechaescriturapublica = UtileriasCfdi.FORMATO_ANIO_MES_DIA_GUION_MEDIO
					.format(emisor.getFechaEscrituraPublica());
			String fechadondequedoinscritafoliomercantil = UtileriasCfdi.FORMATO_ANIO_MES_DIA_GUION_MEDIO
					.format(emisor.getFechaInscritoFolioMercantil());
			String fechaaceptacion = UtileriasCfdi.FORMATO_ANIO_MES_DIA_GUION_MEDIO.format(new Date());
			html = ConvenioConfidencialidadPersonaMoral.crearAvisoPersonaMoral(emisor.getNombre(),
					emisor.getNumeroEscrituraPublica(), fechaescriturapublica, emisor.getNombreNotario(),
					emisor.getNumeroNotario(), emisor.getEntidadFederativaNotario(),
					emisor.getEntidadFederativaConstruyoEmpresa(), emisor.getFolioMercantil(),
					fechadondequedoinscritafoliomercantil, emisor.getRfc(), emisor.getNombreRepresentanteLegal(), "",
					emisor.getRfcRepresentanteLegalApoderado(), emisor.getNumeroEscrituraPublicaFacultades(),
					emisor.getNombreNotarioTestimonio(), emisor.getNumeroNotarioTestimonio(), emisor.getDomicilio(),
					emisor.getTelefono(), emisor.getCorreo(), emisor.getNombrePersonaContacto(),
					emisor.getCargoPersonaContacto(), fechaaceptacion, cadenaOriginal, dialetra, mesletra, anioletra,
					firmaElectronica,selloDigitalConvenio);
		}
		if (emisor.getRfc().length() == 13) {
			String fechaconformidad = UtileriasCfdi.FORMATO_ANIO_MES_DIA_GUION_MEDIO.format(new Date());
			html = ConvenioConfidencialidadPersonaFisica.avisopersonaFisica(emisor.getNombre(), emisor.getIneIfe(),
					emisor.getFolioIne(), emisor.getEdad(), emisor.getDomicilio(), emisor.getRfc(),
					emisor.getTelefono(), emisor.getCorreo(), emisor.getNombrePersonaContacto(), fechaconformidad,
					cadenaOriginal, emisor.getCargoPersonaContacto(), dialetra, mesletra, anioletra, firmaElectronica,selloDigitalConvenio);
		}

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		HtmlConverter.convertToPdf(html, arrayOutputStream, properties);
		return arrayOutputStream.toByteArray();
	}

	public byte[] obtenerManifiesto(TcContribuyente emisor, String cadenaOriginal, String firmaelectronica,String selloDigitalManifiesto)
			throws Exception {
		ConverterProperties properties = new ConverterProperties();
		properties.setBaseUri(ParametroEnum.CSSCONVENIOS.getValue());
		String html = "";
		Calendar desde = Calendar.getInstance();

		String mesletra = mes(desde);
		int anioletra = ano(desde);
		int dialetra = dia(desde);
		if (emisor.getRfc().length() == 12) {
			// Persona MORAL
			html = ManifiestoPersonaMoral.crearmanifiestopersonamoral(dialetra, mesletra, anioletra, emisor.getNombre(),
					emisor.getRfc(), emisor.getNombreRepresentanteLegal(), cadenaOriginal, firmaelectronica,selloDigitalManifiesto);
		}
		if (emisor.getRfc().length() == 13) {
			html = ManifiestoPersonaFisica.crearManifiestoPersonaFisica(dialetra, mesletra, anioletra, emisor.getRfc(),
					emisor.getNombre(), cadenaOriginal, firmaelectronica,selloDigitalManifiesto);
		}
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		HtmlConverter.convertToPdf(html, arrayOutputStream, properties);
		return arrayOutputStream.toByteArray();
	}

	private String mes(Calendar calendar) {
		return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("es", "ES")).toUpperCase();

	}

	private int ano(Calendar calendar) {
		return calendar.get(Calendar.YEAR);

	}

	private int dia(Calendar calendar) {
		return calendar.get(Calendar.DATE);

	}
}
