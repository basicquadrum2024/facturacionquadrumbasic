package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CfdiServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1708825126168647152L;

    private static final Logger LOGGER = Logger.getLogger(CfdiServices.class);

    @Autowired
    private EDao dao;
    @Autowired
    private AesServices aesServices;

    /**
     * Metodo para guardar la entidad TwCfdi en la base de datos
     * @param twCfdi
     * @return Objecto Serializable
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Serializable guardarCfdi(final TwCfdi twCfdi) throws Exception {
	return dao.save(twCfdi);
    }

    /**
     * Metodo para guardar o actualizar cualquier entidad que este mapeada.
     * @param twCfdi
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public <E> void guardar(final E twCfdi) throws Exception {
	dao.saveOrupdate(twCfdi);
    }

    /**
     * Metodo para contar los comprobantes timbrados por mes por rango de mes y  del usuario timbrados
     * @param twUsuario
     * @param fechaInicial
     * @param fechaFinal
     * @return Integer total de registros de la entidad TwCfdi
     * @throws Exception
     */
    public Integer contarComprobantesTimbradosMes(final TwUsuario twUsuario, final Date fechaInicial,
	    final Date fechaFinal) throws Exception {
	return dao.count(TwCfdi.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("trGenerals", "general");
		c.add(Restrictions.between("fechaCreacion", fechaInicial, fechaFinal));
		c.add(Restrictions.eq("general.twUsuario", twUsuario));
	    }
	});
    }

    /**
     * // * MEtodo que se encarga de buscar todos los comprobantes de pago por
     * // * contribuyente // * // * @param startPosition // * @param maxResults
     * // * @param idContribuyente // * @param ppd // * @return // * @throws
     * Exception //
     */
    // public List<TrGeneral> findTrGeneralPadre(final int startPosition, final
    // int maxResults,
    // final Long idContribuyente, final String ppd) throws Exception {
    // return dao.find(TrGeneral.class, new CriteriaEstrategia() {
    //
    // @Override
    // public void estrategia(Criteria criteria) {
    // criteria.createAlias("trGenerals", "trGen");
    // criteria.createAlias("trGen.tcContribuyente", "contribuyente");
    // criteria.createAlias("trGen.tcReceptor", "receptor");
    // criteria.createAlias("receptor.tcDomicilio", "domicilioReceptor");
    // criteria.add(Restrictions.eq("metodoPago", ppd));
    // criteria.add(Restrictions.isNull("twCfdi"));
    // criteria.add(Restrictions.eq("contribuyente.id", idContribuyente));
    // criteria.setFirstResult(startPosition);
    // criteria.setMaxResults(maxResults);
    // criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
    // criteria.addOrder(Order.desc("id"));
    //
    // }
    // });
    // }
    //
    // /**
    // * MEtodo que cuenta los registros de comprobante de pagos por
    // contribuyente
    // *
    // * @param idContribuyente
    // * @param ppd
    // * @return
    // * @throws Exception
    // */
    // public Integer findTrGeneralPaginationCount(final Long idContribuyente,
    // final String ppd) throws Exception {
    // return dao.count(TrGeneral.class, new CriteriaEstrategia() {
    // @Override
    // public void estrategia(Criteria criteria) {
    // criteria.createAlias("trGenerals", "trGen");
    // criteria.createAlias("trGen.tcContribuyente", "contribuyente");
    // criteria.createAlias("trGen.tcReceptor", "receptor");
    // criteria.add(Restrictions.eq("metodoPago", ppd));
    // criteria.add(Restrictions.isNull("twCfdi"));
    // criteria.add(Restrictions.eq("contribuyente.id", idContribuyente));
    // }
    // });
    // }

    /**
     * MEtodo que calcula lo que se a pagado no contando los cancelados
     * 
     * @param idCfdiPadre
     *            Id Cfdi Padre
     * @return BigDecimal del total pagado
     * @throws Exception
     */
    public BigDecimal calculaTotalPagado(final Long idCfdiPadre) throws Exception {
	return (BigDecimal) dao.findUnique(TwCfdi.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria criteria) {
		criteria.createAlias("twCfdi", "cfdi");
		criteria.add(Restrictions.eq("cfdi.id", idCfdiPadre));
		criteria.add(Restrictions.ne("estatus", 0L));
		criteria.setProjection(Projections.projectionList().add(Projections.sum("total"), "total"));
	    }
	});
    }

    /**
     * Metodo que calcula cuantos pagos se han hecho cuenta los cancelados
     * 
     * @param idCfdiPadre
     *            Id Cfdi Padre
     * @return Long con el total de cfdi hechos
     * @throws Exception
     */
    public Long calculaTotalCfdiHijos(final Long idCfdiPadre) throws Exception {
	return (Long) dao.findUnique(TwCfdi.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria criteria) {
		criteria.createAlias("twCfdi", "cfdi");
		criteria.add(Restrictions.eq("cfdi.id", idCfdiPadre));
		criteria.setProjection(Projections.projectionList().add(Projections.rowCount(), "pagos"));
	    }
	});
    }

    /**
     * Metodo que devuelve lista de cancelados por idCfdiPAdre
     * 
     * @param idCfdiPadre
     * @return
     * @throws Exception
     */
    public List<TwCfdi> buscarPagosCancelados(final Long idCfdiPadre) throws Exception {

	return dao.find(TwCfdi.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria criteria) {
		criteria.createAlias("twCfdi", "cfdi");
		criteria.add(Restrictions.eq("cfdi.id", idCfdiPadre));
		criteria.add(Restrictions.eq("estatus", 0L));
	    }
	});

    }

    /**
     * Metodo para buscar los pagos de Criterion.Example
     * @param twCfdiExample
     * @param idCfdiPadre
     * @return List<TwCfdi> listado de cfdi por entidad TwCfdi y idCfdiPadre 
     * @throws Exception
     */
    public List<TwCfdi> buscarPagosExample(final TwCfdi twCfdiExample, final Long idCfdiPadre) throws Exception {

	return dao.find(TwCfdi.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria criteria) {
		criteria.createAlias("twCfdi", "cfdi");
		// criteria.createAlias("trGenerals", "trGeneral");
		criteria.add(Restrictions.eq("cfdi.id", idCfdiPadre));
		Example example = Example.create(twCfdiExample);
		example.ignoreCase();
		example.enableLike();
		criteria.add(example);
	    }
	});

    }

    /**
     * Metodo que hace la busqueda de cfdi de pagos Criterion.Example
     * 
     * @param twCfdiExample
     * @param idCfdiPadre
     * @param fechaInicio
     * @param fechaFin
     * @return List<TwCfdi> listado de entidad TwCfdi
     * @throws Exception
     */
    public List<TwCfdi> buscarPagosExample(final TwCfdi twCfdiExample, final Long idCfdiPadre, final Date fechaInicio,
	    final Date fechaFin) throws Exception {

	return dao.find(TwCfdi.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria criteria) {
		criteria.createAlias("twCfdi", "cfdi");
		if (fechaInicio != null && fechaFin != null) {
		    criteria.add(Restrictions.between("fechaCertificacion", fechaInicio, fechaFin));
		}
		criteria.add(Restrictions.eq("cfdi.id", idCfdiPadre));
		Example example = Example.create(twCfdiExample);
		example.ignoreCase();
		example.enableLike();
		criteria.add(example);
	    }
	});
    }
    
    public TwCfdi buscaTwCfdi(final String uuid) throws Exception {
    	return dao.findUnique(TwCfdi.class, new CriteriaEstrategia() {

    	    @Override
    	    public void estrategia(Criteria c) {
    		//c.createAlias("tcDomicilio", "dom", Criteria.LEFT_JOIN);
    		c.add(Restrictions.eq("uuid", aesServices.encriptaCadena(uuid)));
    	    }
    	});
        }
    
    public TwCfdi buscaTwCfdiId(final long id) throws Exception {
    	return dao.findUnique(TwCfdi.class, new CriteriaEstrategia() {

    	    @Override
    	    public void estrategia(Criteria c) {
    		//c.createAlias("tcDomicilio", "dom", Criteria.LEFT_JOIN);
    		c.add(Restrictions.eq("id", id));
    	    }
    	});
        }

}
