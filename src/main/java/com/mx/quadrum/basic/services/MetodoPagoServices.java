package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.MetodoPago;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class MetodoPagoServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3980484259771640013L;

    @Autowired
    private EDaoCatalogos edaoCatalogos;

    /**
     * Metodo para buscar los metodos de pago sat
     * @return List<MetodoPago>
     * @throws Exception
     */
    public List<MetodoPago> buscarMetodosPago() throws Exception {
	return edaoCatalogos.find(MetodoPago.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
	    }
	});
    }

}
