package com.mx.quadrum.basic.services;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.Foliador;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class FoliadorService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1127676169320616570L;

    @Autowired
    private EDao dao;

    /**
     * Metodo para guardar en la tabla  de foliador
     * @param name
     * @return retorna el objeto foliador
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Serializable saveFoliador(String name) throws Exception {
	Foliador foliador = new Foliador();
	foliador.setSerie(name);
	foliador.setFolio(0L);
	return dao.save(foliador);
    }
    /**
     * Metodo que sirve para guardar foliador
     * @param foliador
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void guardarFoliador(Foliador foliador) throws Exception {
	dao.saveOrupdate(foliador);
    }

    public Foliador findFoliadorById(final Serializable id) throws Exception {
	return dao.findUnique(Foliador.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.idEq(id));
	    }
	});
    }
    /**
     * Metodo para la busqueda de foliador
     * @param id
     * @return retorna una lista de foliador
     * @throws Exception
     */
    public Foliador findFoliadorByContribuyente(final Long id) throws Exception {
	return dao.findUnique(Foliador.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "contribuyente");
		c.add(Restrictions.eq("contribuyente.id", id));
	    }
	});
    }

}
