package com.mx.quadrum.basic.services.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoImpl;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ServicioTwUsuarioImpl2024 implements Serializable {

	private static final long serialVersionUID = 8213612087341899129L;
	@Autowired
	private EDaoImpl eDaoImpl;

	public TwUsuario buscarPorRfcE(final String rfce) throws Exception {
		return eDaoImpl.findUnique(TwUsuario.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria cliteria) throws Exception {
				cliteria.add(Restrictions.eq("rfce", rfce));

			}
		});
	}
}
