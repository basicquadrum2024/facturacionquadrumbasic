package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.TasaOcuota;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TasaOcuotaServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3684112112174548518L;

    @Autowired
    private EDaoCatalogos edaoCatalogos;

    /**
     * Metodo para buscar una lista de tasa o cuota mediante las restricciones impuesto, factor, tipo impuesto
     * @param impuesto
     * @param factor
     * @param tipoImpuesto
     * @return retorna lista del objeto Tasa o cuota
     * @throws Exception
     */
    public List<TasaOcuota> buscarTasasOCuotas(final String impuesto, final String factor, final String[] tipoImpuesto)
	    throws Exception {
	return edaoCatalogos.find(TasaOcuota.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("impuesto", impuesto));
		c.add(Restrictions.eq("factor", factor));
		c.add(Restrictions.in("tipoImpuesto", tipoImpuesto));
	    }
	});
    }

    /**
     * Metodo para buscar tasa o cuota e impuesto mediante las restricciones impuesto, tipoImpuesto
     * @param impuesto
     * @param tipoImpuesto
     * @return retorna una lista de string 
     * @throws Exception
     */
    public List<String> buscarTasasOCuotasImpuesto(final String impuesto, final String[] tipoImpuesto)
	    throws Exception {
	return edaoCatalogos.find(TasaOcuota.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.and(Restrictions.eq("impuesto", impuesto),
			(Restrictions.or(Restrictions.eq("tipoImpuesto", tipoImpuesto[0]),
				Restrictions.eq("tipoImpuesto", tipoImpuesto[1])))));
		c.setProjection(Projections.projectionList().add(Projections.distinct(Projections.property("factor"))));
	    }
	});
    }
}
