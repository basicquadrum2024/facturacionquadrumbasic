package com.mx.quadrum.basic.services;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
public class AcuerdosServices {

	@Autowired
	private EDao dao;

	/**
	 * Metodo que consulta a la base de datos para obtener una lista de
	 * Contribuyente
	 * 
	 * @param inicio
	 * @param totalMostrar
	 * @return
	 * @throws Exception
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<TcContribuyente> obtenerLista(final int inicio, final int totalMostrar) throws Exception {
		return dao.find(TcContribuyente.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria c) throws Exception {
				c.createAlias("trGenerals", "trGenerals", JoinType.LEFT_OUTER_JOIN);
				// c.createAlias("trGenerals.twUsuario", "twUsuario",JoinType.LEFT_OUTER_JOIN);
				c.createAlias("trGenerals.tcContribuyente", "tcContribuyente", JoinType.LEFT_OUTER_JOIN);
				ProjectionList lista = Projections.projectionList();
				lista.add(Projections.distinct(Projections.property("tcContribuyente.id")));
				lista.add(Projections.property("id"), "id");
				lista.add(Projections.property("rfc"), "rfc");
				lista.add(Projections.property("nombre"), "nombre");
				// lista.add(Projections.property("usuario.nombre"),"usuario");
				lista.add(Projections.property("rutaManifiesto"), "rutaManifiesto");
				lista.add(Projections.property("rutaContratoServicios"), "rutaContratoServicios");
				lista.add(Projections.property("rutaConvenioConfidencialidad"), "rutaConvenioConfidencialidad");
				lista.add(Projections.property("estatusArchivos"), "estatusArchivos");

				c.setProjection(lista);
				c.setFirstResult(inicio);
				c.setMaxResults(totalMostrar);
				c.setResultTransformer(Transformers.aliasToBean(TcContribuyente.class));
			}
		});
	}

	/**
	 * Metodo que consulta a la base de datos parr obtener el total de registros de
	 * la entidad Contribuyente
	 * 
	 * @return
	 * @throws Exception
	 */

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Number obtenerTotalRegistros() throws Exception {
		return dao.count(TcContribuyente.class);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void cambiaEstatus(TcContribuyente tcContribuyente) throws Exception {
		String hql="update TcContribuyente set estatusArchivos=:estatusArchivos where id=:id";
		HashMap<String, Object> param=new HashMap<>();
		param.put("estatusArchivos", tcContribuyente.getEstatusArchivos());
		param.put("id", tcContribuyente.getId());
		dao.excuteQueryHqlParam(hql, param);
	}


}
