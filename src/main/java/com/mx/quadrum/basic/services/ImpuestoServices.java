package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.Impuesto;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ImpuestoServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 209028393614452176L;
    @Autowired
    private EDaoCatalogos edaoCatalogos;

    /**
     * Metodo para buscar los impuestos trasladados
     * @return List<Impuesto>
     * @throws Exception
     */
    public List<Impuesto> buscarImpuestosTrasladados() throws Exception {
	return edaoCatalogos.find(Impuesto.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("traslado", "Si"));
	    }
	});
    }

    /**
     * Metodo para buscar los impuestos retenidos
     * @return List<Impuesto>
     * @throws Exception
     */
    public List<Impuesto> buscarImpuestosRetenidos() throws Exception {
	return edaoCatalogos.find(Impuesto.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("retencion", "Si"));
	    }
	});
    }

    /**
     * Metodo para buscar clave del impuesto
     * @param clave
     * @return
     * @throws Exception
     */
    public Impuesto buscarImpuestosClave(final String clave) throws Exception {
	return edaoCatalogos.findUnique(Impuesto.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("clave", clave));
	    }
	});
    }
}
