package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TcPistasAdministrador;
import com.mx.quadrum.basic.entity.TcPistasClientes;
import com.mx.quadrum.basic.util.CriteriaEstrategia;
import com.mx.quadrum.basic.wrapper.Paginacion;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PistasServices implements Serializable {

    @Autowired
    private EDao dao;

    @Autowired
    private AesServices aesServices;

    private static final Logger LOGGER = Logger.getLogger(PistasServices.class);

    /**
     * Servicio para guardar en la tabla tc_pistas_clientes las pistas generadas
     * 
     * @param pistasClientes
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void guardaPistasCliente(TcPistasClientes pistasClientes) throws Exception {
	dao.saveOrupdate(pistasClientes);
    }

    /**
     * Servicio para guardar en la tabla tc_pistas_administrador las pistas
     * generadas
     * 
     * @param pistasClientes
     * @throws Exception
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void guardaPistasCliente(TcPistasAdministrador pistasAdministrador) throws Exception {
	dao.saveOrupdate(pistasAdministrador);
    }

    public Long buscaPistasPorFechasClienteCount(final Date fechaInicial, final Date fechaFinal, final String entidad,
	    final String accion) throws Exception {
	return (Long) dao.find(TcPistasAdministrador.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("userbd", new Long(2)));
		c.add(Restrictions.between("fechahora", fechaInicial, fechaFinal));
		if (!accion.equals("NjVjYzg1MjYxZWZjNzY3MTdlOWY0ZTY1NGE2MzMwN2Y="))
		    c.add(Restrictions.eq("sentenciasql", accion));
		if (!entidad.equals("NjVjYzg1MjYxZWZjNzY3MTdlOWY0ZTY1NGE2MzMwN2Y="))
		    c.add(Restrictions.eq("objeto", entidad));
		c.setProjection(Projections.projectionList().add(Projections.rowCount()));
		c.uniqueResult();
	    }
	}).get(0);
    }

    /**
     * Metodo para buscar pistas por fecha cliente 
     * @param fechaInicial
     * @param fechaFinal
     * @param entidad
     * @param accion
     * @param paginaInicial
     * @param maxResult
     * @return retorna una lista del objeto TcPistasAdministrador
     * @throws Exception
     */
    public List<TcPistasAdministrador> buscaPistasPorFechasCliente(final Date fechaInicial, final Date fechaFinal,
	    final String entidad, final String accion, final Integer paginaInicial, final Integer maxResult)
	    throws Exception {
	List<TcPistasAdministrador> administradors = null;
	administradors = dao.find(TcPistasAdministrador.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("userbd", new Long(2)));
		c.add(Restrictions.between("fechahora", fechaInicial, fechaFinal));
		if (!accion.equals("NjVjYzg1MjYxZWZjNzY3MTdlOWY0ZTY1NGE2MzMwN2Y="))
		    c.add(Restrictions.eq("sentenciasql", accion));
		if (!entidad.equals("NjVjYzg1MjYxZWZjNzY3MTdlOWY0ZTY1NGE2MzMwN2Y="))
		    c.add(Restrictions.eq("objeto", entidad));
		c.setFirstResult(paginaInicial);
		c.setMaxResults(maxResult);
	    }

	});
	return administradors;
    }

    /**
     * Servicio para obtener Pistas de auditoria para clientes por fechas
     * 
     * @param fechaInicial
     * @param fechaFinal
     * @return
     * @throws Exception
     */
    public Paginacion pistasAdministradores(final Date fechaInicial, final Date fechaFinal, final Paginacion paginacion)
	    throws Exception {
	final Integer paginaInicial = (paginacion.getPage() * paginacion.getPageSize()) - paginacion.getPageSize();
	final Integer maxResult = paginacion.getPageSize();

	List<Object> pistas = dao.find(TcPistasAdministrador.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.ne("userbd", new Long(2)));
		c.add(Restrictions.between("fechahora", fechaInicial, fechaFinal));
		c.setFirstResult(paginaInicial);
		c.setMaxResults(maxResult);
	    }
	});
	paginacion.setCount(pistasAdministradoresCount(fechaInicial, fechaFinal, paginacion).intValue());
	paginacion.setListBus(pistas);
	return paginacion;

    }

    /**
     * Contador Pistas de auditoria para el Servicio Admin
     * 
     * @param fechaInicial
     * @param fechaFinal
     * @param paginacion
     * @return
     */
    private Long pistasAdministradoresCount(final Date fechaInicial, final Date fechaFinal, Paginacion paginacion)
	    throws Exception {
	return (Long) dao.find(TcPistasAdministrador.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.ne("userbd", new Long(2)));
		c.add(Restrictions.between("fechahora", fechaInicial, fechaFinal));
		c.setProjection(Projections.projectionList().add(Projections.rowCount()));
		c.uniqueResult();
	    }
	}).get(0);
    }

    /**
     * Servicio para obtener Pistas de auditoria para clientes por fechas
     * 
     * @param fechaInicial
     * @param fechaFinal
     * @return
     * @throws Exception
     */
    public Paginacion pistasCliente(final Date fechaInicial, final Date fechaFinal, final String objeto,
	    final String accion, final Paginacion paginacion) throws Exception {

	final Integer paginaInicial = (paginacion.getPage() * paginacion.getPageSize()) - paginacion.getPageSize();
	final Integer maxResult = paginacion.getPageSize();

	List<Object> pistas = dao.find(TcPistasAdministrador.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("userbd", new Long(2)));
		c.add(Restrictions.between("fechahora", fechaInicial, fechaFinal));
		if (!accion.equals("NjVjYzg1MjYxZWZjNzY3MTdlOWY0ZTY1NGE2MzMwN2Y="))
		    c.add(Restrictions.eq("sentenciasql", accion));
		if (!objeto.equals("NjVjYzg1MjYxZWZjNzY3MTdlOWY0ZTY1NGE2MzMwN2Y="))
		    c.add(Restrictions.eq("objeto", objeto));
		c.setFirstResult(paginaInicial);
		c.setMaxResults(maxResult);
	    }
	});
	paginacion.setCount(
		buscaPistasPorFechasClienteCount(fechaInicial, fechaFinal, objeto, accion, paginacion).intValue());
	paginacion.setListBus(pistas);
	return paginacion;

    }

    /**
     * Contador Pistas de auditoria para el Servicio clientes
     * 
     * @param fechaInicial
     * @param fechaFinal
     * @param paginacion
     * @return
     */
    private Long buscaPistasPorFechasClienteCount(final Date fechaInicial, final Date fechaFinal, final String objeto,
	    final String accion, Paginacion paginacion) throws Exception {

	return (Long) dao.find(TcPistasAdministrador.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("userbd", new Long(2)));
		c.add(Restrictions.between("fechahora", fechaInicial, fechaFinal));
		if (!accion.equals("NjVjYzg1MjYxZWZjNzY3MTdlOWY0ZTY1NGE2MzMwN2Y="))
		    c.add(Restrictions.eq("sentenciasql", accion));
		if (!objeto.equals("NjVjYzg1MjYxZWZjNzY3MTdlOWY0ZTY1NGE2MzMwN2Y="))
		    c.add(Restrictions.eq("objeto", objeto));
		c.setProjection(Projections.projectionList().add(Projections.rowCount()));
		c.uniqueResult();
	    }
	}).get(0);
    }

    /**
     * descarga de Excel por Administrador
     * 
     * @param fechaInicial
     * @param fechaFinal
     * @param firstResult
     * @param maxResults
     * @return
     * @throws Exception
     */
    public List<TcPistasAdministrador> buscaPistasAdministrador(final Date fechaInicial, final Date fechaFinal,
	    final int firstResult, final int maxResults) throws Exception {
	List<TcPistasAdministrador> clientes = null;
	clientes = dao.find(TcPistasAdministrador.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.ne("userbd", new Long(2)));
		c.add(Restrictions.between("fechahora", fechaInicial, fechaFinal));
		c.setFirstResult(firstResult);
		c.setMaxResults(maxResults);
	    }
	});
	return clientes;
    }

    /**
     * Metodo para contar los registros de la tabla TcPistasAdministrador
     * @param fechaInicial
     * @param fechaFinal
     * @return
     * @throws Exception
     */
    public Long pistasClienteCount(final Date fechaInicial, final Date fechaFinal) throws Exception {
	return (Long) dao.find(TcPistasAdministrador.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.ne("userbd", new Long(2)));
		c.add(Restrictions.between("fechahora", fechaInicial, fechaFinal));
		c.setProjection(Projections.projectionList().add(Projections.rowCount()));
		c.uniqueResult();
	    }
	}).get(0);
    }

}