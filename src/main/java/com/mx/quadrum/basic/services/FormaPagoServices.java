package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.FormaPago;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class FormaPagoServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1006298419575289195L;

    @Autowired
    private EDaoCatalogos edaoCatalogos;

    /**
     * metodo para buscar formas de pago
     * @return List<FormaPago>
     * @throws Exception
     */
    public List<FormaPago> buscarFormasPago() throws Exception {
	return edaoCatalogos.find(FormaPago.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {

	    }
	});
    }
}
