package com.mx.quadrum.basic.services.catalogos;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.Pais;
import com.mx.quadrum.basic.util.CriteriaEstrategia;
/**
 * Clase @Service contiene m�todos de negocio para la clase/objeto Pais
 * @author 
 *
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CPaisServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5108232067377338932L;
    @Autowired
    private EDaoCatalogos daoCatalogos;

    public List<Pais> buscarTodosPaises() throws Exception {
	return daoCatalogos.find(Pais.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria cliteria) {

	    }
	});
    }
}
