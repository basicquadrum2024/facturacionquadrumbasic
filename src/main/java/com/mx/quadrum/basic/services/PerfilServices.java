package com.mx.quadrum.basic.services;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TcPerfil;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PerfilServices implements Serializable {

	static final Logger LOGGER = Logger.getLogger(PerfilServices.class);

	@Autowired
	private EDao eDao;

	/**
	 * Metodo para buscar una entidad de TcPerfil por id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public TcPerfil findbyId(Integer id)  throws Exception{
		TcPerfil perfil = eDao.findById(id, TcPerfil.class, "accioneses");
		return perfil;
	}

}
