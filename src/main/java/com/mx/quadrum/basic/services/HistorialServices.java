package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TwHistorialPasword;
import com.mx.quadrum.basic.util.CriteriaEstrategia;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class HistorialServices implements Serializable {
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(HistorialServices.class);

	@Autowired
	private EDao edao;

	/**
	 * metodo para guardar entidad de TwHistorialPasword
	 * @param historial
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void guardaHistorial(TwHistorialPasword historial) {
		try {
			edao.saveOrupdate(historial);
		} catch (Exception e) {
			e.getStackTrace();
			logger.error(UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
	}
	
	public List<TwHistorialPasword> buscarHistorialContrasena(final String usuario) throws Exception {
		return edao.find(TwHistorialPasword.class, new CriteriaEstrategia() {

			@Override
			public void estrategia(Criteria cliteria) {
				cliteria.createAlias("twUsuario", "alias_twUsuario");
				cliteria.add(Restrictions.eq("alias_twUsuario.rfce", usuario));
			}
		});
	}

}
