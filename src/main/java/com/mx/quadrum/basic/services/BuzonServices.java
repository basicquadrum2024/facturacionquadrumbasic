package com.mx.quadrum.basic.services;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.Buzon;
import com.mx.quadrum.basic.wrapper.BuzonWrapper;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class BuzonServices {
	
	 @Autowired
	    private EDao dao;
	 
	    private static final Logger LOGGER = Logger.getLogger(BuzonServices.class);
	    
	    /**
	     * Metodo para guardar una entidad de Buzon o actualizarlo
	     * @param buzon
	     * @throws Exception
	     */
	    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	    public void guardaEnBuzon(Buzon buzon) throws Exception {
		dao.saveOrupdate(buzon);
	    }
	    
	    /**
	     * Metodo que resibe un HQL con los siguientes parametros para realizar una un select de cualquier entidad mapeada, devolviendo un listado de la entidad BuzonWrapper 
	     * @param clase
	     * @param hql
	     * @param parametros
	     * @param pagina
	     * @param numRegistros
	     * @return listado de List<BuzonWrapper>
	     * @throws Exception
	     */
	    public List<BuzonWrapper> listaReporteBuzon(Class clase, String hql,
	    	    HashMap<String, Object> parametros, int pagina, Integer numRegistros) throws Exception {
	    	return dao.selectQueryHqlParamPag(clase, hql, parametros, pagina, numRegistros);
	        }

	    /**
	     * Metodo de devuelve un numero despues de realizar un count con un hql de una entidad mapeada
	     * @param hql
	     * @param parametros
	     * @return el total de registros de una entidad Number
	     * @throws Exception
	     */
	    public Number contarDatosReporte(String hql, HashMap<String, Object> parametros) throws Exception {
	    	return dao.countQueryHqlParam(hql, parametros);
	        }
	    
	    /**
	     * Metodo que devuelve la lista de reporte buzon
	     * @param clase
	     * @param hql
	     * @param parametros
	     * @return
	     * @throws Exception
	     */
	    public List<BuzonWrapper> listaReporteBuzonExcel(Class clase, String hql,
	    	    HashMap<String, Object> parametros) throws Exception {
	    	return dao.selectQueryHqlParam(clase, hql, parametros);
	        }
}
