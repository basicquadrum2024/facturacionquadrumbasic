package com.mx.quadrum.basic.services.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.dao.IUsuarioDao;
import com.mx.quadrum.basic.entity.TcPerfil;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.EService;
import com.mx.quadrum.basic.services.IUsuarioServices;
import com.mx.quadrum.basic.util.CriteriaEstrategia;
import com.mx.quadrum.basic.wrapper.Paginacion;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.utilerias.UtileriasFecha;

//esta clase
@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
// es aqui
// no
public class UsuarioServicesImp<E> implements IUsuarioServices {

    private static final Logger logger = Logger.getLogger(UsuarioServicesImp.class);

    @Autowired
    private IUsuarioDao usuarioDao;

    @Autowired
    private EService service;

    @Autowired
    private AesServices aesServices;

    @Autowired
    private EDao edao;

    public List<TwUsuario> findAllUsuarios() throws Exception {
	return usuarioDao.findAll();

    }

    public TwUsuario findUsuario() throws Exception {
	return usuarioDao.findFetch(1200);

    }

    @Override
    public List findAllUsuarios(String h) throws Exception {
	return usuarioDao.excuteQueryHqlList(h);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void Editar(TwUsuario user) throws Exception {
	usuarioDao.saveOrupdate(user);

    }

    @Override
    public boolean validarRfcTemporal(final String rfc) throws Exception {
	List<TwUsuario> user = usuarioDao.findAll(Restrictions.eq("rfce", rfc));
	if (user != null && !user.isEmpty()) {
	    return false;
	} else {
	    return true;
	}
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void saveorUpdate(TwUsuario usuario) throws Exception {
	usuarioDao.saveOrupdate(usuario);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(TwUsuario usuario) throws Exception {
	usuarioDao.delete(usuario);
    }

    @Override
    public TwUsuario findbyCorreo(String correo) throws Exception {
	TwUsuario user = usuarioDao.getFindUsuarioLogin(correo);

	return user;
    }

    public TwUsuario findByRfcUsuarioTemporal(final String rfc) throws Exception {
	TwUsuario usuario = edao.findUnique(TwUsuario.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {

		c.add(Restrictions.eq("rfce", rfc));// where
	    }
	});
	return usuario;

    }

    public Paginacion getPaginacionBus(Paginacion pagina, final String campoBetween, final Date date1, final Date date2,
	    final boolean like) throws Exception {
	pagina.setCount(getCountPaginador(pagina, campoBetween, date1, date2, like).intValue());
	pagina.setListBus(buscaPaginaBloque(pagina, campoBetween, date1, date2, like));
	return pagina;
    }

    private Long getCountPaginador(final Paginacion pagina, final String campoBetween, final Date date1,
	    final Date date2, final boolean like) throws Exception {
	return (Long) service.find(pagina.getClase(), new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria crit) throws Exception {

		if (pagina.getConjuncion() != null && !pagina.getConjuncion().isEmpty()) {
		    for (String conjuncion : pagina.getConjuncion()) {
			String[] campo = conjuncion.split("[|]");
			logger.error("campo->" + ToStringBuilder.reflectionToString(campo, ToStringStyle.SIMPLE_STYLE));
			logger.error("campo->" + aesServices.encriptaCadena(campo[1].toString()));
			logger.error(
				"folio---> " + aesServices.desEncriptaCadena("MK98oIf.u0K99u88KMun0oIuFI@KFM9H77MI"));
			if (campo[0].toString().indexOf(".") > -1) {
			    String criteraAlias[] = campo[0].split("[.]");
			    logger.error("criteraAlias->"
				    + ToStringBuilder.reflectionToString(criteraAlias, ToStringStyle.SIMPLE_STYLE));
			    crit.createAlias(criteraAlias[0], criteraAlias[0].substring(0, 3), JoinType.INNER_JOIN);

			    if (campo[1] instanceof String) {
				// 2015-06-18
				if (((String) campo[1]).indexOf("-") == 2) {
				    Date date = parseDate(campo[1]);
				    crit.add(Restrictions.between(
					    criteraAlias[0].substring(0, 3) + "." + criteraAlias[1],
					    UtileriasFecha.getPrimerHoraDia(date),
					    UtileriasFecha.getUltimaHoraDia(date)));
				} else {
				    crit.add(Restrictions.eq(criteraAlias[0].substring(0, 3) + "." + criteraAlias[1],
					    aesServices.encriptaCadena(campo[1].toString())));
				}
			    }

			} else if (campo[1] instanceof String) {
			    // 2015-06-18
			    if (((String) campo[1]).indexOf("-") == 2) {
				Date date = parseDate(campo[1]);
				crit.add(Restrictions.between(campo[0], UtileriasFecha.getPrimerHoraDia(date),
					UtileriasFecha.getUltimaHoraDia(date)));
			    } else {

				if (like) {
				    crit.add(Restrictions.ilike(campo[0],
					    aesServices.encriptaCadena(campo[1].toString()), MatchMode.ANYWHERE));
				} else {
				    crit.add(
					    Restrictions.eq(campo[0], aesServices.encriptaCadena(campo[1].toString())));
				}
			    }
			}
		    }
		}
		// para el caso de dos fechas between
		if (pagina.getBetween() != null && !pagina.getBetween().isEmpty()) {
		    crit.add(Restrictions.between(campoBetween, date1, date2));
		}

		if (pagina.getJoinn() != null && !pagina.getJoinn().isEmpty()) {
		    for (String join : pagina.getJoinn()) {
			crit.setFetchMode(join, FetchMode.JOIN);
		    }
		}
		crit.setProjection(Projections.projectionList().add(Projections.rowCount()));
		crit.uniqueResult();
	    }
	}).get(0);
    }

    private List<Object> buscaPaginaBloque(final Paginacion pagina, final String campoBetween, final Date date1,
	    final Date date2, final boolean like) throws Exception {
	final Integer paginaInicial = (pagina.getPage() * pagina.getPageSize()) - pagina.getPageSize();
	final Integer maxResult = pagina.getPageSize();
	return service.find(pagina.getClase(), new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria crit) throws Exception {
		if (pagina.getConjuncion() != null && !pagina.getConjuncion().isEmpty()) {
		    for (String conjuncion : pagina.getConjuncion()) {
			// String campo = conjuncion.getKey();
			String[] campo = conjuncion.split("[|]");
			// Object valor = conjuncion.getValue();
			if (campo[0].toString().indexOf(".") > -1) {
			    String criteraAlias[] = campo[0].split("[.]");
			    // logger.debug("criteraAlias->"+ToStringBuilder.reflectionToString(criteraAlias,ToStringStyle.SIMPLE_STYLE));
			    crit.createAlias(criteraAlias[0], criteraAlias[0].substring(0, 3), JoinType.INNER_JOIN);
			    if (campo[1] instanceof String) {
				// 2015-06-18
				if (((String) campo[1]).indexOf("-") == 2) {
				    Date date = parseDate(campo[1]);
				    crit.add(Restrictions.between(
					    criteraAlias[0].substring(0, 3) + "." + criteraAlias[1],
					    UtileriasFecha.getPrimerHoraDia(date),
					    UtileriasFecha.getUltimaHoraDia(date)));
				} else {
				    crit.add(Restrictions.eq(criteraAlias[0].substring(0, 3) + "." + criteraAlias[1],
					    aesServices.encriptaCadena(campo[1].toString())));
				}
			    }

			} else if (campo[1] instanceof String) {
			    // 2015-06-18
			    if (((String) campo[1]).indexOf("-") == 2) {
				Date date = parseDate(campo[1]);
				crit.add(Restrictions.between(campo[0], UtileriasFecha.getPrimerHoraDia(date),
					UtileriasFecha.getUltimaHoraDia(date)));
			    } else {
				if (like) {
				    crit.add(Restrictions.ilike(campo[0],
					    aesServices.encriptaCadena(campo[1].toString()), MatchMode.ANYWHERE));
				} else {
				    crit.add(
					    Restrictions.eq(campo[0], aesServices.encriptaCadena(campo[1].toString())));
				}
			    }
			}
		    }
		}
		// para el caso de dos fechas between
		if (pagina.getBetween() != null && !pagina.getBetween().isEmpty()) {
		    crit.add(Restrictions.between(campoBetween, date1, date2));
		}
		if (pagina.getOrder() != null && !pagina.getOrder().isEmpty()) {
		    for (String order : pagina.getOrder()) {
			crit.addOrder(Order.asc(order));
		    }
		}
		if (pagina.getJoinn() != null && !pagina.getJoinn().isEmpty()) {
		    for (String join : pagina.getJoinn()) {
			crit.setFetchMode(join, FetchMode.JOIN);
		    }
		}
		crit.setFirstResult(paginaInicial);
		crit.setMaxResults(maxResult);
	    }
	});
    }

    public TwUsuario usuarioById(final Integer id) throws Exception {
	TwUsuario usuario = null;
	usuario = service.findById(id, TwUsuario.class);
	return usuario;
    }

    public TwUsuario usuarioByRFC(final String rfc) throws Exception {
	return service.findUnique(TwUsuario.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createCriteria("tcPerfil");
		c.add(Restrictions.eq("rfce", rfc));

	    }
	});
    }

    protected Date parseDate(Object valor) {
	Date date = null;
	try {
	    date = UtileriasCfdi.FORMATOFECHA.parse((String) valor);
	} catch (ParseException e) {
	    logger.error("error al parse la fecha->" + valor);
	}
	logger.debug("parseDate->" + date);
	return date;
    }

    @Override
    public TwUsuario findbyRfcTemporal(final String rfc) throws Exception {
	return edao.findUnique(TwUsuario.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.eq("rfce", rfc));

	    }
	});
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int getUpdateIntentos(String encriptaCadena, int i) throws Exception {
	SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	return edao.excuteQueryHql("update TwUsuario set ultimaconexion = '" + sd.format(new Date()) + "', sesion = '"
		+ aesServices.encriptaCadena(String.valueOf(i + 1)) + "' where rfce = '" + encriptaCadena + "'");

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void estatusCuenta(String encriptaCadena) throws Exception {
	edao.excuteQueryHql("update TwUsuario set statuscuenta = '" + aesServices.encriptaCadena("0")
		+ "' where rfce = '" + encriptaCadena + "'");

    }

    @Override
    public List<TwUsuario> findUsuarioPagination(final int startPosition, final int maxResults) throws Exception {
	return edao.find(TwUsuario.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcPerfil", "perfil", Criteria.LEFT_JOIN);
		c.setFirstResult(startPosition);
		c.setMaxResults(maxResults);
		c.addOrder(Order.asc("id"));
		c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

	    }
	});
    }

    public Integer findUsuarioPaginationCount() throws Exception {
	Long count = (long) edao.countComprobante(TwUsuario.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria cliteria) {

	    }
	});
	return count.intValue();
    }

    public List<TcPerfil> buscarPerfiles() throws Exception {
	return edao.find(TcPerfil.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria cliteria) {
		// TODO Auto-generated method stub

	    }
	});
    }
}
