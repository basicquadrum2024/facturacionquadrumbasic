package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDaoCatalogos;
import com.mx.quadrum.basic.entity.catalogosSAT.TipoComprobante;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TipoComprobanteServices implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8050084465056845478L;

    @Autowired
    private EDaoCatalogos edaoCatalogos;
    
    /**
     * Metodo que busca una lista de la entidad TipoComprobante mediante la restriccion de la clave de tipo de comprobante
     * @param tiposComprobante
     * @return retorna una lista de la entidad de TipoComprobante
     * @throws Exception
     */

    public List<TipoComprobante> buscarTiposComprobante(final String... tiposComprobante) throws Exception {
	return edaoCatalogos.find(TipoComprobante.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria c) {
		c.add(Restrictions.in("clave", tiposComprobante));
	    }
	});
    }
}
