package com.mx.quadrum.basic.services;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.dao.EDao;
import com.mx.quadrum.basic.entity.TrGeneral;
//import com.mx.quadrum.basic.entity.quadrumfree.Users;
import com.mx.quadrum.basic.util.CriteriaEstrategia;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TablaGeneralServices {

    @Autowired
    private EDao dao;

    /**
     * Metodo para contar los registros de la entidad TrGeneral por uuid y rfce
     * @param uuid
     * @param rfce
     * @return int
     * @throws Exception
     */
    public int buscarPorFolioCount(final String uuid, final String rfce,final Date fechaInicial,final Date fechaFinal) throws Exception {
	return dao.countComprobante(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.eq("twCfdi.uuid", uuid));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));

	    }
	});
    }

    /**
     * Metodo para buscar un listado de la entidad TrGeneral por uuid y rfc en bloques
     * @param uuid
     * @param rfce
     * @param startPosition
     * @param maxResults
     * @return
     * @throws Exception
     */
    public List<TrGeneral> buscarPorFolio(final String uuid, final String rfce, final int startPosition,
	    final int maxResults,final Date fechaInicial,final Date fechaFinal) throws Exception {
	return dao.find(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.eq("twCfdi.uuid", uuid));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));
		c.setFirstResult(startPosition);
		c.setMaxResults(maxResults);
	    }
	});
    }

    /**
     * Metodo para contar entidad TrGeneral por rfcReceptor y rfce
     * @param rfcReceptor
     * @param rfce
     * @return
     * @throws Exception
     */
    public int buscarRfcReceptorCount(final String rfcReceptor, final String rfce,final Date fechaInicial,final Date fechaFinal) throws Exception {
	return dao.countComprobante(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.eq("tcReceptor.rfc", rfcReceptor));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));

	    }
	});
    }

    /**
     * Metodo para buscar en entidad TrGeneral por rfcReceptor y rfce devolviendo un listado
     * @param rfcReceptor
     * @param rfce
     * @param startPosition
     * @param maxResults
     * @return List<TrGeneral>
     * @throws Exception
     */
    public List<TrGeneral> buscarRfcReceptor(final String rfcReceptor, final String rfce, final int startPosition,
	    final int maxResults,final Date fechaInicial,final Date fechaFinal) throws Exception {
	return dao.find(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.eq("tcReceptor.rfc", rfcReceptor));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));
		c.setFirstResult(startPosition);
		c.setMaxResults(maxResults);
	    }
	});
    }
    
    /**
     * Metodo para contar los registros de la  entidad TrGeneral por estatus,rfce
     * @param estatus
     * @param rfce
     * @return
     * @throws Exception
     */
    public int buscarPorEstatusCount(final Long estatus, final String rfce,final Date fechaInicial,final Date fechaFinal) throws Exception {
	return dao.countComprobante(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.eq("twCfdi.estatus", estatus));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));

	    }
	});
    }

    /**
     * Metodo que devuelve un listado en bloques de la entidad TrGeneral por estatus rfce
     * @param estatus
     * @param rfce
     * @param startPosition
     * @param maxResults
     * @return List<TrGeneral>
     * @throws Exception
     */
    public List<TrGeneral> buscarPorEstatus(final Long estatus, final String rfce, final int startPosition,
	    final int maxResults,final Date fechaInicial,final Date fechaFinal) throws Exception {
	return dao.find(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.eq("twCfdi.estatus", estatus));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));
		c.setFirstResult(startPosition);
		c.setMaxResults(maxResults);

	    }
	});
    }

    /**
     * Metodo para contar los registros de la entidad TrGeneral por fechaInicial,fechaFinal y rfce
     * @param fechaInicial
     * @param fechaFinal
     * @param rfce
     * @return int
     * @throws Exception
     */
    public int buscarPorFechaTimbradoCountEmisor(final Date fechaInicial, final Date fechaFinal, final String rfce)
	    throws Exception {
	return dao.countComprobante(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));

	    }
	});
    }

    /**
     * Metodo para contar los registros de la entidad TrGeneral por rfce,claveConfirmacion
     * @param rfce
     * @param claveConfirmacion
     * @return int
     * @throws Exception
     */
    public int buscarPorClaveConfirmacionCountEmisor(final String rfce, final String claveConfirmacion,final Date fechaInicial,final Date fechaFinal)
	    throws Exception {
	return dao.countComprobante(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twCfdi.claveConfirmacion", claveConfirmacion));
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));

	    }
	});
    }

    /**
     * Metodo que busca registros de la entidad TrGeneral
     * @param fechaInicial
     * @param fechaFinal
     * @param rfce
     * @param startPosition
     * @param maxResults
     * @return List<TrGeneral>
     * @throws Exception
     */
    public List<TrGeneral> buscarPorFechaTimbradoEmisor(final Date fechaInicial, final Date fechaFinal,
	    final String rfce, final int startPosition, final int maxResults) throws Exception {
	return dao.find(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));
		c.setFirstResult(startPosition);
		c.setMaxResults(maxResults);
	    }
	});
    }

    /**
     * Metodo que busca los registros de la entidad TrGeneral
     * @param rfce
     * @param claveConfirmacion
     * @param startPosition
     * @param maxResults
     * @return List<TrGeneral>
     * @throws Exception
     */
    public List<TrGeneral> buscarPorEmisorClaveConfirmacion(final String rfce, final String claveConfirmacion,
	    final int startPosition, final int maxResults,final Date fechaInicial,final Date fechaFinal) throws Exception {
	return dao.find(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twUsuario.rfce", rfce));
		c.add(Restrictions.eq("twCfdi.claveConfirmacion", claveConfirmacion));
		c.add(Restrictions.between("twCfdi.fechaCreacion", fechaInicial, fechaFinal));
		c.addOrder(Order.asc("twCfdi.fechaCreacion"));
		c.setFirstResult(startPosition);
		c.setMaxResults(maxResults);
	    }
	});
    }

    /**
     * Metodo para buscar una entidad unica de TrGeneral por id 
     * @param id
     * @return TrGeneral
     * @throws Exception
     */
    public TrGeneral buscarCfdiPorId(final Long id) throws Exception {
	return dao.findUnique(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria c) {
		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
		c.add(Restrictions.eq("twCfdi.id", id));
	    }
	});

    }
    
    /**
     * Metodo para buscar una entidad unica de TrGeneral por id
     * @param id
     * @return TrGeneral
     * @throws Exception
     */
    public TrGeneral buscarCfdiPorCancelacionId(final Long id) throws Exception {
    	return dao.findUnique(TrGeneral.class, new CriteriaEstrategia() {

    	    @Override
    	    public void estrategia(Criteria c) {
    		c.createAlias("tcContribuyente", "tcContribuyente", Criteria.LEFT_JOIN);
    		c.createAlias("tcReceptor", "tcReceptor", Criteria.LEFT_JOIN);
    		c.createAlias("twCfdi", "twCfdi", Criteria.LEFT_JOIN);
    		c.createAlias("twUsuario", "twUsuario", Criteria.LEFT_JOIN);
    		c.add(Restrictions.eq("twCfdi.id", id));
    	    }
    	});

        }

    /**
     * Metodo que guarda cualquier entidad que se encuentre mapeada
     * @param entity
     * @throws Exception
     */
    public <E> void saveOrUpdateGeneral(E entity) throws Exception {
	dao.saveOrupdate(entity);
    }

    /**
     * Metodo para contar los registros de  la entidad TrGeneral por idContribuyente,ppd
     * @param idContribuyente
     * @param ppd
     * @return
     * @throws Exception
     */
    public Integer findTrGeneralPaginationCount(final Long idContribuyente, final String ppd) throws Exception {
	Long count = (long) dao.countComprobante(TrGeneral.class, new CriteriaEstrategia() {
	    @Override
	    public void estrategia(Criteria criteria) {
		criteria.createAlias("tcContribuyente", "contribuyente");

		criteria.createAlias("twCfdi", "cfdi");
		criteria.add(Restrictions.eq("cfdi.metodoPago", ppd));
		criteria.add(Restrictions.isNull("cfdi.twCfdi"));
		criteria.add(Restrictions.eq("contribuyente.id", idContribuyente));
	    }
	});
	return count.intValue();
    }

    /**
     * Metodo para buscar los registros en bloques de la entidad TrGeneral por idContribuyente, ppd 
     * @param startPosition
     * @param maxResults
     * @param idContribuyente
     * @param ppd
     * @return
     * @throws Exception
     */
    public List<TrGeneral> findTrGeneralPadre(final int startPosition, final int maxResults, final Long idContribuyente,
	    final String ppd) throws Exception {
	return dao.find(TrGeneral.class, new CriteriaEstrategia() {

	    @Override
	    public void estrategia(Criteria criteria) {
		criteria.createAlias("tcContribuyente", "contribuyente");
		//criteria.createAlias("contribuyente.tcDomicilio", "domicilio");
		criteria.createAlias("tcReceptor", "receptor");
		//criteria.createAlias("receptor.tcDomicilio", "domicilioReceptor");
		criteria.createAlias("twCfdi", "cfdi");
		criteria.add(Restrictions.eq("cfdi.metodoPago", ppd));
		criteria.add(Restrictions.isNull("cfdi.twCfdi"));
		criteria.add(Restrictions.eq("contribuyente.id", idContribuyente));
		criteria.setFirstResult(startPosition);
		criteria.setMaxResults(maxResults);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Order.desc("id"));

	    }
	});
    }

}
