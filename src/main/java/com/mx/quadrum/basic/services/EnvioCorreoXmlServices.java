package com.mx.quadrum.basic.services;

import java.io.Serializable;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.cfdi.utilerias.BytesArchivo;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

@Service
public class EnvioCorreoXmlServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1640808389305459911L;

	private static final Logger LOGGER = Logger.getLogger(EnvioCorreoXmlServices.class);
	@Autowired
	private MailService mailService;
	@Autowired
	private EmisorServices emisorServices;
	@Autowired
	private AesServices aesServices;
	@Autowired
	private TcReceptorServices tcReceptorServices;

	/**
	 * Metodo para enviar xml por correo
	 * 
	 * @param direccionXml
	 * @param correos
	 */
	public void enviarXmlPorCorreo(final TwCfdi twCfdi, final String direccionXml, final String uuid,
			final String... correos) {
		try {

			for (String correo : correos) {
				mailService.enviarZip(correo, null, "Representaci\u00f3n impresa",
						buscarNombreCfdis(uuid, ParametroEnum.DIRRECCION_APP.getValue()), uuid + ".zip",
						crearZipEnvio(direccionXml, uuid, twCfdi));
			}
		} catch (Exception e) {
			LOGGER.error(e);
			LOGGER.error("error en metodo enviarXmlPorCorreo--> " + UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
	}

	/**
	 * Metodo para enviar correo de xml cancelado
	 * 
	 * @param direccionXml
	 * @param uuid
	 * @param correo
	 */
	public void enviarXmlCanceladoPorCorreo(final String direccionXml, final String uuid, final String correo) {
		try {
			Path path = Paths.get(direccionXml);
			List<BytesArchivo> listabyte = new ArrayList<>();
			BytesArchivo byteXMl = new BytesArchivo();
			byteXMl.setNombreArchivo(uuid + ".xml");
			byteXMl.setBytesArchivo(Files.readAllBytes(path));
			listabyte.add(byteXMl);
			mailService.enviarZip(correo, null, "Acuse de cancelaci\u00f3n",
					buscarNombreCancelado(uuid, ParametroEnum.DIRRECCION_APP.getValue()), uuid + ".zip",
					UtileriasCfdi.crearZipListaBytesArchivo(listabyte));
		} catch (Exception e) {
			LOGGER.error("error en metodo enviarXmlCanceladoPorCorreo--> "
					+ UtileriasCfdi.imprimeStackError(e.getStackTrace()));
		}
	}

	/**
	 * Metodo para crear un archvio zip
	 * 
	 * @param direccionXml
	 * @param uuid
	 * @return retorna un flujo de bytes
	 * @throws Exception
	 */
	private byte[] crearZipEnvio(final String direccionXml, String uuid, TwCfdi twCfdi) throws Exception {
		List<BytesArchivo> listabyte = new ArrayList<>();
		Path path = Paths.get(direccionXml);
		BytesArchivo byteXMl = new BytesArchivo();
		byteXMl.setNombreArchivo(uuid);
		byteXMl.setBytesArchivo(Files.readAllBytes(path));
		listabyte.add(byteXMl);
		byte[] arrayFiles = null;

		arrayFiles = Utilerias.crearPdf(ParametroEnum.JASPER_V4.getValue(), direccionXml, twCfdi.getTotal());
		BytesArchivo bytepdf = new BytesArchivo();
		bytepdf.setNombreArchivo(uuid + ".pdf");
		bytepdf.setBytesArchivo(arrayFiles);
		listabyte.add(bytepdf);
		return UtileriasCfdi.crearZipListaBytesArchivo(listabyte);
	}

	/**
	 * Metodo que carga el cuerpo o contenido que se envia al correo electronico
	 * 
	 * @param nombre
	 * @return retorna el cuero del correo a enviar
	 * @throws UnknownHostException
	 */
	public String buscarNombreCfdis(String nombre, String url) {
		return "Estimado Cliente,"
				+ "\n\nA continuaci\u00f3n \"Centro de validaci\u00f3n digital S.A. de C.V.\" le hace entrega de los siguientes CFDIs: "
				+ "\n " + nombre
				+ "\n\n Adjunto a este mensaje se anexan los comprobantes con las siguientes caracteriticas:"
				+ "\n\t Representaci\u00f3n impresa: (archivo con extensi\u00f3n .pdf)"
				+ "\n\t Archivo Fuente: (archivo con extensi\u00f3n .xml)" + "\n\t " + "\n\n Saludos Cordiales" + "\n\n"
				+ "\n (01) 4421612565, (01) 5512091825, 01(55) 90007456, (01) 5512091825, 01(722)5075558, 01(722)5075559 y 01(722)2122754"
				+ "\n " + ParametroEnum.CORREO_SOPORTE.getValue() + "" + "\n\n Para facturar entre a: " + url
				+ "\n\n Este correo fue generado de forma autom\u00e1tica; por favor, no emita una respuesta al mismo.";
	}

	/**
	 * Metodo que carga el cuerpo o contenido que se envia al correo electronico
	 * 
	 * @param nombre
	 * @return retorna el cuerpo del correo de cancelacion
	 * @throws UnknownHostException
	 */
	public String buscarNombreCancelado(String nombre, String url) {
		return "Estimado Cliente,"
				+ "\n\nA continuaci\u00f3n \"Centro de validaci\u00f3n digital S.A. de C.V.\" le hace entrega de su CFDI cancelado: "
				+ "\n " + nombre
				+ "\n\n Adjunto a este mensaje se anexa el comprobante con las siguientes caracteriticas:"
				+ "\n\t Archivo Fuente: (archivo con extensi\u00f3n .xml)" + "\n\t " + "\n\n Saludos Cordiales" + "\n\n"
				+ "\n (01) 4421612565, (01) 5512091825, 01(55) 90007456, 01(722)5075558, 01(722)5075559 y 01(722)2122754"
				+ "\n " + ParametroEnum.CORREO_SOPORTE.getValue() + "" + "\n\n Para facturar entre a: " + url
				+ "\n\n Este correo fue generado de forma autom\u00e1tica; por favor, no emita una respuesta al mismo.";
	}
}
