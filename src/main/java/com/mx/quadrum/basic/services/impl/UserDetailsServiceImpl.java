package com.mx.quadrum.basic.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.dao.IUsuarioDao;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.entity.UsuarioPrincipal;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.util.ParametroEnum;
import com.mx.quadrum.basic.util.SessionApp;
import com.mx.quadrum.basic.util.Utilerias;


/**
 * 
 * Implementacion de Servicio para autentificar el usuario
 * 
 * @author Pedro Romero Martinez
 * @version 1.0.0
 * @see org.springframework.security.core.userdetails.UserDetailsService.
 * 
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserDetailsServiceImpl implements UserDetailsService {

	/**
	 * Log de la aplicaci�n.
	 */
	final static Logger logger = Logger.getLogger(UserDetailsServiceImpl.class);

	/**
	 * Acceso a la base de datos.
	 */
	@Autowired
	private IUsuarioDao dao;

	@Autowired
	private PistasServices pistasServices;

	/**
	 * Servicio de encripci�n.
	 */

	@Autowired
	private AesServices aesServices;
	/**
	 * Servicio de Historial
	 */

	/**
	 * Messages de Spring security.
	 */

	@Autowired
	private MessageSource messages;

	/**
	 * M�todo para autentificar al usuario.
	 * 
	 * @param username login del usuario que intenta accesar.
	 * @return UserDetails usuario que esta firmarmado.
	 * @throws UsernameNotFoundException error de acceso.
	 * @throws DataAccessException       error de acceso.
	 */
	// @Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		TwUsuario userEntity = null;
		SessionApp sessionApp = new SessionApp();
		sessionApp.setRfc(username);
		aesServices.encriptaCadena(username);
		logger.debug("sessionApp--> " + sessionApp.getRfc());
		boolean banSegundo = Boolean.FALSE;
		try {
			logger.debug("Va a buscar---> ");
			userEntity = dao.getFindUsuarioLogin(aesServices.encriptaCadena(username));
			if (userEntity != null) {
				logger.debug("userEntity.getSesion()---> " + userEntity.getSesion());
				int intento = 0;
				if (userEntity.getSesion() != null && !userEntity.getSesion().isEmpty()) {
					intento = Integer.valueOf(userEntity.getSesion());
				}

				dao.getUpdateIntentos(aesServices.encriptaCadena(username), intento);
				logger.debug("encontro---> " + userEntity.getCorreo() + " intento--->" + intento);
				int segundos = 0;
				if (userEntity.getFechaIntentos() != null)
					segundos = calculaSegundo(userEntity.getFechaIntentos());
				logger.debug("segundos---> " + segundos + " intento--->" + intento);
				if (intento >= 3 && segundos < Integer.valueOf(ParametroEnum.TIEMPO_BLOQ_USR.getValue())) {
					banSegundo = Boolean.TRUE;
					if (userEntity.getTcPerfil().getId() == 1) {
						Utilerias.eventosAdministrador("USERS", "INTENTO " + intento + " PARA ACCESO AL SISTEMA",
								userEntity, "ERROR", pistasServices, null);
					}
					throw new LockedException(messages.getMessage("AbstractSecurityInterceptor.intentos",
							new Object[] { username, Integer.valueOf(ParametroEnum.TIEMPO_BLOQ_USR.getValue()) },
							new Locale("es")));
				}
				if (segundos >= Integer.valueOf(ParametroEnum.TIEMPO_BLOQ_USR.getValue())) {
					dao.getUpdateIntentos(aesServices.encriptaCadena(username), 0);
				}
			}
		} catch (Exception e) {
			try {
				if (userEntity.getTcPerfil().getId() == 1) {
					Utilerias.eventosAdministrador("USERS", "INTENTO FALLIDO PARA ACCESO AL SISTEMA", userEntity,
							"SELECT", pistasServices, null);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			if (banSegundo == Boolean.TRUE) {
				throw new LockedException(messages.getMessage("AbstractSecurityInterceptor.intentos",
						new Object[] { username, Integer.valueOf(ParametroEnum.TIEMPO_BLOQ_USR.getValue()) },
						new Locale("es")));

			} else {
				throw new LockedException(messages.getMessage("AbstractSecurityInterceptor.authenticationNotFound",
						new Object[] { username }, new Locale("es")));
			}
		}
		if (userEntity == null) {
			throw new LockedException(messages.getMessage("AbstractSecurityInterceptor.authenticationNotFound",
					new Object[] { username }, new Locale("es")));
		}
		return buildUserFromUserEntity(userEntity);
	}

	private int calculaSegundo(Date ultimaconexion) {
		Date ahorita = new Date();
		long diff = ahorita.getTime() - ultimaconexion.getTime();
		long segundos = diff / 1000;
		long minutos = segundos / 60;
		long horas = minutos / 60;
		long dias = horas / 24;
		logger.debug("Segundo()->" + minutos);
		return (int) minutos;
	}

	/**
	 * M�todo para agregar al usuario al objeto
	 * org.springframework.security.core.userdetails.User de spring.
	 * 
	 * @param userEntity objeto que esta frimado en la session de spring.
	 * @return User objeto de contexto de spring.
	 */
	User buildUserFromUserEntity(TwUsuario userEntity) {
		String username = userEntity.getUsername();
		String password = userEntity.getPassword();
		boolean enabled = userEntity.isEnabled();
		boolean accountNonExpired = userEntity.isAccountNonExpired();
		boolean credentialsNonExpired = userEntity.isCredentialsNonExpired();
		boolean accountNonLocked = userEntity.isAccountNonLocked();
		List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
		grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		UsuarioPrincipal user = new UsuarioPrincipal(username, password, enabled, accountNonExpired,
				credentialsNonExpired, accountNonLocked, grantedAuths);
		user.setUsuario(userEntity);

		return user;
	}

}
