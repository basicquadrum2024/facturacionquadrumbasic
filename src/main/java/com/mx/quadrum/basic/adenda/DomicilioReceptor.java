package com.mx.quadrum.basic.adenda;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Clase que contiene los datos de domicilio receptor para generar xml
 * @author Manu
 *
 */
public class DomicilioReceptor {

    private String email;
    private String calle;
    private String numeroExterior;
    private String numeroInterior;
    private String colonia;
    private String localidad;
    private String referencia;
    private String municipio;
    private String estado;
    private String pais;
    private String codigoPostal;

    @XmlAttribute(name = "correo")
    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    @XmlAttribute(name = "calle")
    public String getCalle() {
	return calle;
    }

    public void setCalle(String calle) {
	this.calle = calle;
    }

    @XmlAttribute(name = "numeroExterior")
    public String getNumeroExterior() {
	return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
	this.numeroExterior = numeroExterior;
    }

    @XmlAttribute(name = "numeroInterior")
    public String getNumeroInterior() {
	return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
	this.numeroInterior = numeroInterior;
    }

    @XmlAttribute(name = "colonia")
    public String getColonia() {
	return colonia;
    }

    public void setColonia(String colonia) {
	this.colonia = colonia;
    }

    @XmlAttribute(name = "localidad")
    public String getLocalidad() {
	return localidad;
    }

    public void setLocalidad(String localidad) {
	this.localidad = localidad;
    }

    @XmlAttribute(name = "referencia")
    public String getReferencia() {
	return referencia;
    }

    public void setReferencia(String referencia) {
	this.referencia = referencia;
    }

    @XmlAttribute(name = "municipio")
    public String getMunicipio() {
	return municipio;
    }

    public void setMunicipio(String municipio) {
	this.municipio = municipio;
    }

    @XmlAttribute(name = "estado")
    public String getEstado() {
	return estado;
    }

    public void setEstado(String estado) {
	this.estado = estado;
    }

    @XmlAttribute(name = "pais")
    public String getPais() {
	return pais;
    }

    public void setPais(String pais) {
	this.pais = pais;
    }

    @XmlAttribute(name = "codigoPostal")
    public String getCodigoPostal() {
	return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
	this.codigoPostal = codigoPostal;
    }
}
