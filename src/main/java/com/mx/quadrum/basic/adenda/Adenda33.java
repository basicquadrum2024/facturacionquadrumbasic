package com.mx.quadrum.basic.adenda;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase @XmlRootElement para crear adenda en la version 3.3 de CFDI
 * @author 
 *
 */
@XmlRootElement(name = "Addenda")
@XmlAccessorType(XmlAccessType.FIELD)
public class Adenda33 {

    @JsonProperty("DomicilioEmisor")
    @XmlElement(name = "DomicilioEmisor")
    private DomicilioEmisor domicilioEmisor;

    @JsonProperty("DomicilioReceptor")
    @XmlElement(name = "DomicilioReceptor")
    private DomicilioReceptor domicilioReceptor;

    public DomicilioEmisor getDomicilioEmisor() {
	if (null == domicilioEmisor)
	    domicilioEmisor = new DomicilioEmisor();
	return domicilioEmisor;
    }

    public void setDomicilioEmisor(DomicilioEmisor domicilioEmisor) {
	this.domicilioEmisor = domicilioEmisor;
    }

    @XmlAttribute
    private String folio;

    @XmlAttribute
    private String serie;

    @XmlAttribute
    private String nombreComprobante;

    public DomicilioReceptor getDomicilioReceptor() {
	if (null == domicilioReceptor)
	    domicilioReceptor = new DomicilioReceptor();
	return domicilioReceptor;
    }

    public void setDomicilioReceptor(DomicilioReceptor domicilioReceptor) {
	this.domicilioReceptor = domicilioReceptor;
    }

    public String getFolio() {
	return folio;
    }

    public void setFolio(String folio) {
	this.folio = folio;
    }

    public String getSerie() {
	return serie;
    }

    public void setSerie(String serie) {
	this.serie = serie;
    }

    public String getNombreComprobante() {
	return nombreComprobante;
    }

    public void setNombreComprobante(String nombreComprobante) {
	this.nombreComprobante = nombreComprobante;
    }

}
