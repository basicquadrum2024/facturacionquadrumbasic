package com.mx.quadrum.basic.adenda;

import javax.xml.bind.annotation.XmlAttribute;
/**
 * Clase que contine los datos de domicilio Emisor para generar xml
 * @author Manu
 *
 */
public class DomicilioEmisor {
    private String calle;
    private String colonia;
    private String municipio;
    private String localidad;
    private String numeroInterior;
    private String numeroExterior;
    private String estado;
    private String referencia;
    private String pais;
    private String codigoPostal;

    @XmlAttribute(name = "calle")
    public String getCalle() {
	return calle;
    }

    public void setCalle(String calle) {
	this.calle = calle;
    }

    @XmlAttribute(name = "colonia")
    public String getColonia() {
	return colonia;
    }

    public void setColonia(String colonia) {
	this.colonia = colonia;
    }

    @XmlAttribute(name = "municipio")
    public String getMunicipio() {
	return municipio;
    }

    public void setMunicipio(String municipio) {
	this.municipio = municipio;
    }

    @XmlAttribute(name = "localidad")
    public String getLocalidad() {
	return localidad;
    }

    public void setLocalidad(String localidad) {
	this.localidad = localidad;
    }

    @XmlAttribute(name = "numeroInterior")
    public String getNumeroInterior() {
	return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
	this.numeroInterior = numeroInterior;
    }

    @XmlAttribute(name = "numeroExterior")
    public String getNumeroExterior() {
	return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
	this.numeroExterior = numeroExterior;
    }

    @XmlAttribute(name = "estado")
    public String getEstado() {
	return estado;
    }

    public void setEstado(String estado) {
	this.estado = estado;
    }

    @XmlAttribute(name = "referencia")
    public String getReferencia() {
	return referencia;
    }

    public void setReferencia(String referencia) {
	this.referencia = referencia;
    }

    @XmlAttribute(name = "pais")
    public String getPais() {
	return pais;
    }

    public void setPais(String pais) {
	this.pais = pais;
    }

    @XmlAttribute(name = "codigoPostal")
    public String getCodigoPostal() {
	return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
	this.codigoPostal = codigoPostal;
    }

}
