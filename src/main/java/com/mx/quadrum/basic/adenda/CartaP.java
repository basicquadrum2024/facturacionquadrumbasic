package com.mx.quadrum.basic.adenda;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.mx.quadrum.cfdi.v32.schema.Comprobante.Addenda;

/**
 * Clase @XmlRootElement para crear nodo CartaPorte 
 * @author 
 *
 */
@XmlRootElement(name = "CartaPorte")
@XmlAccessorType(XmlAccessType.FIELD)
public class CartaP extends Addenda {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlAttribute(name = "origen")
	private String origen;

	@XmlAttribute(name = "destino")
	private String destino;
	@XmlAttribute(name = "cuotayTonelada")
	private String cuotayTonelada;
	@XmlAttribute(name = "fraccion")
	private String fraccion;
	@XmlAttribute(name = "clase")
	private String clase;
	@XmlAttribute(name = "condicionesPago")
	private String condicionesPago;
	@XmlAttribute(name = "peso")
	private String peso;
	@XmlAttribute(name = "metros")
	private String metros;
	@XmlAttribute(name = "pesoEstimado")
	private String pesoEstimado;
	@XmlAttribute(name = "valorDeclarado")
	private String valorDeclarado;
	@XmlAttribute(name = "cantidad")
	private String cantidad;
	@XmlAttribute(name = "tipoUnidad")
	private String tipoUnidad;
	@XmlAttribute(name = "numeroEconomico")
	private String numeroEconomico;
	@XmlAttribute(name = "empresa")
	private String empresa;
	@XmlAttribute(name = "propietario")
	private String propietario;
	@XmlAttribute(name = "operador")
	private String operador;
	@XmlAttribute(name = "contieneRemitente")
	private String contieneRemitente;

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getCuotayTonelada() {
		return cuotayTonelada;
	}

	public void setCuotayTonelada(String cuotayTonelada) {
		this.cuotayTonelada = cuotayTonelada;
	}

	public String getFraccion() {
		return fraccion;
	}

	public void setFraccion(String fraccion) {
		this.fraccion = fraccion;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getCondicionesPago() {
		return condicionesPago;
	}

	public void setCondicionesPago(String condicionesPago) {
		this.condicionesPago = condicionesPago;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public String getMetros() {
		return metros;
	}

	public void setMetros(String metros) {
		this.metros = metros;
	}

	public String getPesoEstimado() {
		return pesoEstimado;
	}

	public void setPesoEstimado(String pesoEstimado) {
		this.pesoEstimado = pesoEstimado;
	}

	public String getValorDeclarado() {
		return valorDeclarado;
	}

	public void setValorDeclarado(String valorDeclarado) {
		this.valorDeclarado = valorDeclarado;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getTipoUnidad() {
		return tipoUnidad;
	}

	public void setTipoUnidad(String tipoUnidad) {
		this.tipoUnidad = tipoUnidad;
	}

	public String getNumeroEconomico() {
		return numeroEconomico;
	}

	public void setNumeroEconomico(String numeroEconomico) {
		this.numeroEconomico = numeroEconomico;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public String getContieneRemitente() {
		return contieneRemitente;
	}

	public void setContieneRemitente(String contieneRemitente) {
		this.contieneRemitente = contieneRemitente;
	}

}
