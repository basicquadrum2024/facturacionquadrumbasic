package com.mx.quadrum.basic.documentos;

public class FormatoContratoServiciosBasicoPersonaFisica {

	public static String crearPdfPersonaFisica(String nombrepersonafisica, String ineife, String folioine, String edad,
			String domiciliofiscal, String rfc, String telefono, String correo, String personacontacto,String fechaconformidad,String cadenaOriginal,int dia,String mes,int anio,String firmaelectronica,String selloDigitalContrato) {

		return "<!DOCTYPE html>\n" + 
				"<html lang=\"es\">\n" + 
				"\n" + 
				"<head>\n" + 
				"    <meta charset=\"utf-8\">\n" + 
				"    <title>Example 1</title>\n" + 
				"    <link rel=\"stylesheet\" href=\"bootstrap.css\" media=\"all\" />\n" + 
				"</head>\n" + 
				"<style type=\"text/css\">\n" + 
				"    .estiloletrajustificada {\n" + 
				"        font-size: 12px !important;\n" + 
				"        text-align: justify !important;\n" + 
				"        /* font-weight: bold !important;*/\n" + 
				"    }\n" + 
				"    \n" + 
				"    .estiloletracentado {\n" + 
				"        text-align: center !important;\n" + 
				"        font-size: 12px !important;\n" + 
				"    }\n" + 
				"    \n" + 
				"    .style1 {\n" + 
				"        width: 10% !important;\n" + 
				"        border-top: 1px solid #8c8b8b !important;\n" + 
				"    }"
				+ ".overflow-visible {\n" + 
				"  white-space: initial;\n" + 
				"}\n" + 
				"</style>\n" + 
				"\n" + 
				"<body>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        <strong>CONTRATO DE PRESTACI\u00d3N DE SERVICIOS DE CERTIFICACI\u00d3N DE COMPROBANTES FISCALES DIGITALES POR INTERNET Y SERVICIOS ADICIONALES COMPLEMENTARIOS, QUE CELEBRAN POR UNA PARTE LA SOCIEDAD DENOMINADA CENTRO DE VALIDACI\u00d3N DIGITAL CVDSA S.A. DE C.V., EN LO SUCESIVO EL PROVEEDOR Y POR LA OTRA ----- "+nombrepersonafisica+" ----- EN LO SUCESIVO EL CONSUMIDOR, A QUIENES EN SU CONJUNTO SE LES DENOMINAR\u00c1 LAS PARTES AL TENOR DE LAS SIGUIENTES DECLARACIONES Y CL\u00c1USULAS.</strong>\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletracentado\">\n" + 
				"        D E C L A R A C I O N E S\n" + 
				"    </p>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        I.- DEL PROVEEDOR\n" + 
				"    </p>\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            I.1 Que es una persona moral mexicana, seg\u00fan consta en la escritura p\u00fablica n\u00famero 10,485 de fecha ocho de abril de dos mil once, pasada ante la fe del Licenciado Alfonso Martin Le\u00f3n Orantes, Notario P\u00fablico n\u00famero 238 en el Distrito Federal e inscrita en el Registro P\u00fablico de Comercio de la Ciudad de M�xico bajo el folio mercantil 451848 de fecha veinticuatro de junio de dos mil once, con Registro Federal de Contribuyentes (RFC) CVD110412TF6, representado en este acto por el La Lic. Laura Ivete P\u00e9rez Serafin, quien acredita su personalidad mediante el testimonio Notarial n\u00famero 58,895 pasado ante la fe del Licenciado Antonio Velarde Violante Notario P\u00fablico n\u00famero 164 de la Ciudad de M�xico, en fecha 05 de agosto de 2019, inscrito en el Registro P\u00fablico de Comercio de esta Ciudad, bajo el folio mercantil 451848-1, la cual otorga facultades suficientes para suscribir el presente instrumento.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            I.2 La Lic. Laura Ivete P\u00e9rez Serafin, en su calidad de representante legal, tiene todas las facultades para celebrar el presente contrato las cuales no le han sido revocadas, modificadas o disminuidas en forma alguna.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            I.3 Que para los fines y efectos legales del presente contrato se\u00f1ala como domicilio el ubicado en Calle Montecito n\u00famero 38 piso 25 oficina 22, Colonia N\u00e1poles Delegaci\u00f3n Benito Ju\u00e1rez, Ciudad de M�xico CP. 03810, con n\u00famero de tel�fono (55) 9000-7454 y correo electr\u00f3nico liperez@quadrum.com.mx.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            I.4 Que cuenta con la capacidad legal, infraestructura, servicios, recursos materiales y humanos necesarios debidamente capacitados, para dar cabal cumplimiento a las obligaciones derivadas del presente Contrato.\n" + 
				"\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            I.5 Que mediante oficio n\u00famero 700-03-00-00-00-2018-1186 de fecha 05 de octubre de 2018 le fue renovada por parte del Servicio de Administraci\u00f3n Tributaria (SAT) la autorizaci\u00f3n para operar como proveedor de certificaci\u00f3n de comprobantes fiscales digitales por internet por los ejercicios fiscales 2019 y 2020, por lo cual con fundamento en el art\u00edculo 29 del C\u00f3digo Fiscal de la Federaci\u00f3n (CFF) est\u00e1 facultada para validar que los comprobantes fiscales digitales por internet (CFDI) cumplan con los requisitos establecidos en el art\u00edculo 29-A del CFF; asignar el folio del CFDI; e incorporar el sello digital del SAT.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            I.6 Que cuenta con el Certificado de Sello Digital del SAT vigente, mismo que le fue otorgado con motivo de la autorizaci\u00f3n se\u00f1alada en el inciso anterior.\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        II.- DEL CONSUMIDOR:\n" + 
				"    </p>\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            II.1 Que es una persona f\u00edsica, de nacionalidad mexicana, ----- "+edad+" ----- a\u00f1os, que se encuentra en pleno uso de sus facultades legales para suscribir el presente contrato, quien se identifica ----- "+ineife+" -----, con n\u00famero de folio ----- "+folioine+" -----.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            II.2 Que para los fines y efectos legales del presente contrato se\u00f1ala como domicilio el ubicado en Calle ----- "+domiciliofiscal+" -----, con n\u00famero de tel�fono ----- "+telefono+" ----- y correo electr\u00f3nico ----- "+correo+" -----\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            1. II.3 Que su registro federal de contribuyentes es ----- "+rfc+" -----.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            II.4 Que manifiesta su voluntad para obligarse en los t�rminos y condiciones del presente contrato.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            II.5 Que proporcionar\u00e1 la informaci\u00f3n requerida por el PROVEEDOR de forma completa, veraz y actualizada, para la realizaci\u00f3n de las actividades que deriven del objeto de este contrato.\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        III.- DE LAS PARTES:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            III.1 Previamente a la suscripci\u00f3n de este instrumento han revisado y obtenido todas y cada una de las autorizaciones de car\u00e1cter legal y administrativo, adem\u00e1s de contar con las facultades y capacidad legal suficientes para tales efectos, las cuales no les han sido modificados, restringidas o revocadas en forma alguna, a la fecha de celebraci\u00f3n del presente Contrato.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            III.2 Reconocen en forma rec\u00edproca la personalidad con la que se ostentan y comparecen a la suscripci\u00f3n de este instrumento y admiten como propias, en lo que corresponda, todas y cada una de las declaraciones anteriores, por lo que est\u00e1n de acuerdo en obligarse en los t�rminos y condiciones que se estipulan en las cl\u00e1usulas del presente instrumento.\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletracentado\">\n" + 
				"        C L \u00c1 U S U L A S\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        1.- OBJETO.\n" + 
				"    </p>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        1.1 El presente instrumento tiene por objeto regular la relaci\u00f3n entre las PARTES derivada de la prestaci\u00f3n de los servicios de CFDI y servicios adicionales.\n" + 
				"    </p>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        1.2 Que para efectos del presente contrato se entender\u00e1 por:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             CFDI: Comprobante fiscal digital por internet, que certifica un proveedor a un consumidor y que re\u00fane los requisitos del art\u00edculo 29 y 29-A del C\u00f3digo Fiscal de la Federaci\u00f3n y dem\u00e1s disposiciones aplicables.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             CFF: C\u00f3digo Fiscal de la Federaci\u00f3n.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             CCF: C\u00f3digo Civil Federal.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             CC: C\u00f3digo de Comercio.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             CONSUMIDOR: Persona f\u00edsica o moral que adquiere y/o contrata los servicios b\u00e1sicos, los servicios adicionales o complementarios, respecto a la certificaci\u00f3n de comprobantes fiscales digitales por internet (CFDI).\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             e-firma: El certificado de firma electr\u00f3nica que establece el art\u00edculo 17-D del C\u00f3digo Fiscal de la Federaci\u00f3n, misma que se define como el mensaje de datos que se ampara por un certificado vigente que confirma el v\u00ednculo entre un firmante y los datos de creaci\u00f3n de la misma.\n" + 
				"\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             PROFECO: Procuradur\u00eda Federal del Consumidor.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             PROVEEDOR: Persona moral que cuenta con autorizaci\u00f3n por parte del SAT para operar como proveedor de certificaci\u00f3n de CFDI.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             SAT: Servicio de Administraci\u00f3n Tributaria.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"             SERVICIO ADICIONAL O COMPLEMENTARIO: Servicios adicionales o complementarios distintos al servicio b\u00e1sico que se detallar\u00e1n en la cl\u00e1usula 2 del presente contrato.\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        1.3 El SERVICIO B\u00c1SICO consistir\u00e1 en lo siguiente:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            a) Validar el cumplimiento de los requisitos establecidos en el art\u00edculo 29-A del CFF.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            b) Asignar el folio del CFDI.\n" + 
				"        </p>\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            c) Incorporar el sello digital del SAT.\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        Adicionalmente a lo anterior deber\u00e1 resguardar por un per\u00edodo de tres meses posteriores al de su certificaci\u00f3n los CFDI a los que se hayan aplicado las acciones contenidas en los incisos que anteceden.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        El SERVICIO B\u00c1SICO tambi�n incluir\u00e1 la recepci\u00f3n por parte del PROVEEDOR, de los CFDI a ser certificados, as\u00ed como el env\u00edo de estos, una vez certificados al CONSUMIDOR a trav�s de correo electr\u00f3nico que para tal efecto el CONSUMIDOR, designe para tal efecto.\n" + 
				"    </p>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        El SERVICIO B\u00c1SICO definido es distinto al servicio de certificaci\u00f3n gratuita de CFDI, que se realiza a trav�s de la aplicaci\u00f3n inform\u00e1tica gratuita con la que deben contar todos los proveedores de certificaci\u00f3n de comprobantes fiscales digitales por internet.\n" + 
				"\n" + 
				"    </p>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        2.-CARGOS POR EL USO DEL SERVICIO.\n" + 
				"        <br> 3.1 Las partes acuerdan sobre el costo total del servicio(s) contratado(s) estar\u00e1 especificado en un anexo o documento adjunto, mismo que formar\u00e1 parte integral del presente contrato, donde constar\u00e1 el volumen de CFDI,\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        3.- ACCESO AL SERVICIO.\n" + 
				"        <br> 3.1 El PROVEEDOR deber\u00e1 de prestar al CONSUMIDOR todas las herramientas, medios, as\u00ed como asesor\u00eda o apoyo t�cnico, a fin de satisfacer los servicios se\u00f1alados en las CL\u00c1USULAS 1 y 2 del presente instrumento seg\u00fan el tipo de servicio que las partes convengan.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        3.2 En caso de que el PROVEEDOR establezca el uso de claves y contrase\u00f1as para el uso u operaci\u00f3n de los servicios contratados, el CONSUMIDOR deber\u00e1 adoptar las medidas adecuadas para garantizar que las mismas no sean reveladas a ning\u00fan tercero, con la finalidad de preservar en todo momento la seguridad de las transacciones realizadas, debiendo dar aviso oportuno a la direcci\u00f3n de correo electr\u00f3nico del PROVEEDOR, en caso de robo o extrav\u00edo para la reposici\u00f3n de la misma.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        3.3 el PROVEEDOR deber\u00e1 establecer y poner a disposici\u00f3n del CONSUMIDOR uno o m\u00e1s canales seguros para la transferencia electr\u00f3nica de la informaci\u00f3n objeto del presente contrato, de forma que se proteja dicha informaci\u00f3n durante su transferencia hacia y desde los equipos del PROVEEDOR.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        3.4 Para mejoras y mantenimiento continuo a la infraestructura y a la aplicaci\u00f3n, el PROVEEDOR podr\u00e1 planear y ejecutar espacios de tiempo para realizar tareas de mantenimiento del servicio, denominados “ventanas de tiempo de mantenimiento trimestrales, de m\u00e1ximo veinticuatro (24) horas, lo anterior ser\u00e1 notificado al CONSUMIDOR a trav�s de la p\u00e1gina de internet del PROVEEDOR y a trav�s del medio de contacto establecido al efecto en la cl\u00e1usula 14 del presente instrumento, con una anticipaci\u00f3n de por lo menos 7 d\u00edas naturales, durante dichas ventanas de tiempo el servicio estar\u00e1 suspendido.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        4.- OBLIGACIONES.\n" + 
				"        <br> Las PARTES tendr\u00e1n las siguientes obligaciones derivadas de la prestaci\u00f3n de los servicios se\u00f1alados en el presente instrumento:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        a) Certificar los CFDI generados por el CONSUMIDOR, incluyendo en su caso los complementos a los mismos que al efecto requiera el CONSUMIDOR.\n" + 
				"        <br> b) Proporcionar el servicio de acuerdo a las CL\u00c1USULAS pactadas en el presente contrato, as\u00ed como certificar los CFDI de manera directa sin que exista de por medio un tercero realizando cualquier parte del proceso de certificaci\u00f3n de CFDI que opere en calidad de socio comercial, distribuidor o cualquier otra figura an\u00e1loga.\n" + 
				"        <br> c) Informar y respetar los precios y tarifas, conforme a las cuales se hubiera ofrecido, obligado o convenido con el CONSUMIDOR, durante la vigencia del presente contrato.\n" + 
				"        <br> d) Publicar en su p\u00e1gina de internet el documento que se\u00f1ale los est\u00e1ndares de niveles de servicios requeridos, que el SAT le establece como m\u00ednimos para otorgar el servicio de certificaci\u00f3n.\n" + 
				"        <br> e) Asegurar como nivel de servicio que durante la vigencia del contrato, la aplicaci\u00f3n inform\u00e1tica a trav�s de la cual se presten los servicios contratados, se encuentre disponible y operable para el CONSUMIDOR las veinticuatro (24) horas de los trescientos sesenta y cinco (365) d\u00edas del a\u00f1o, con un servicio de por lo menos el 99.3 por ciento, en donde el 0.7 por ciento representa el per\u00edodo de tiempo m\u00e1ximo en que la aplicaci\u00f3n podr\u00eda no estar disponible por causas no planeadas e imputables al PROVEEDOR.\n" + 
				"        <br> f) Proporcionar el servicio de acuerdo a lo contratado, ofrecido o publicitado, cumpliendo con los est\u00e1ndares de niveles de servicios requeridos, tanto el establecido en el inciso b) de la presente cl\u00e1usula como en la cl\u00e1usula 2 del presente instrumento, o los que el SAT establezca como condici\u00f3n o resultado de la autorizaci\u00f3n que otorga para operar como proveedor de CFDI conforme a las disposiciones fiscales vigentes; en este \u00faltimo caso, el PROVEEDOR publicar\u00e1 en su p\u00e1gina de internet los est\u00e1ndares o caracter\u00edsticas del servicio establecido por el SAT.\n" + 
				"        <br> g) Abstenerse de realizar cualquier acci\u00f3n que cause da\u00f1o o vulnere la seguridad de los sistemas inform\u00e1ticos del CONSUMIDOR.\n" + 
				"        <br> h) Resarcir al CONSUMIDOR en su caso, el da\u00f1o o da\u00f1os causados y los perjuicios, por el incumplimiento en los est\u00e1ndares o niveles de servicio pactados o por impericia, de acuerdo a las cl\u00e1usulas del presente contrato.\n" + 
				"        <br> i) Certificar los CFDI que reciba el CONSUMIDOR en un periodo no mayor a una (01) hora despu�s de su recepci\u00f3n.\n" + 
				"        <br> j) Conservar la informaci\u00f3n y documentaci\u00f3n necesaria que se requiera conforme a las disposiciones fiscales, derivada de la prestaci\u00f3n del servicio se\u00f1alado en el presente instrumento.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        4.2 Del CONSUMIDOR\n" + 
				"        <br> a) Pagar por los servicios contratados, en tiempo y forma pactados.\n" + 
				"        <br> b) Proporcionar al PROVEEDOR la informaci\u00f3n necesaria y veraz para que �ste pueda cumplir con su encomienda.\n" + 
				"        <br> c) Autorizar al PROVEEDOR para que remita al SAT todos los CFDI que le certifique.\n" + 
				"        <br> d) Abstenerse de realizar cualquier acci\u00f3n que cause da\u00f1o o vulnere la seguridad de los sistemas inform\u00e1ticos del PROVEEDOR.\n" + 
				"        <br> e) Actuar con la debida prudencia y diligencia para asegurarse de que en el caso de que se establezca el uso de claves y contrase\u00f1as para el uso y operaci\u00f3n de los servicios contratados, estas permanezcan seguras y en conocimientos exclusivo del CONSUMIDOR, procediendo a cancelarlas de inmediato y dar aviso al PROVEEDOR a trav�s del medio establecido en la cl\u00e1usula 14, en el caso de que la seguridad o confidencialidad de �stas se encuentre en riesgo.\n" + 
				"        <br>\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        El incumplimiento de las obligaciones de las partes podr\u00e1 dar lugar a la rescisi\u00f3n del contrato y en su caso al pago de la pena convencional establecida en la cl\u00e1usula 11 del presente instrumento.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        5.- ALMACENAMIENTO Y RESGUARDO DE INFORMACI\u00d3N.\n" + 
				"        <br> 5.1 El PROVEEDOR se obliga a almacenar los CFDI, conservando los documentos por lo menos tres meses posteriores a la certificaci\u00f3n, para permitir al CONSUMIDOR su consulta o impresi\u00f3n de acuerdo a las disposiciones fiscales vigentes; como servicio adicional o complementario podr\u00e1 proporcionarse almacenamiento por un periodo adicional contado a partir del vencimiento del per\u00edodo m\u00ednimo de tres meses ya mencionado con el costo que las partes establezcan.\n" + 
				"        <br> 5.2 El PROVEEDOR manifiesta expresamente que podr\u00e1 almacenar los datos personales, los CFDI que certifique o la dem\u00e1s informaci\u00f3n que le proporcione el CONSUMIDOR para efectos de la ejecuci\u00f3n del presente contrato, en equipos, sistemas, servicios o servidores ubicados fuera del territorio nacional.\n" + 
				"        <br> El pa\u00eds en d\u00f3nde se podr\u00e1 almacenar la informaci\u00f3n del contribuyente ser\u00e1 en Estados Unidos de Norteam�rica.\n" + 
				"        <br> El PROVEEDOR de igual forma, deber\u00e1 de informar al CONSUMIDOR si recibe solicitudes de autoridad distinta a la mexicana para entregar su informaci\u00f3n, no pudiendo entregar dicha informaci\u00f3n sin haber recabado previamente autorizaci\u00f3n expresa y por escrito del CONSUMIDOR, a trav�s de alguno de los medios establecidos en la cl\u00e1usula 14.\n" + 
				"        <br>\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        6.- CANCELACI\u00d3N.\n" + 
				"        <br> 6.1 En caso de que el CONSUMIDOR, no est� conforme con el servicio, podr\u00e1 cancelar el contrato, sin responsabilidad alguna dentro de un periodo de cinco d\u00edas naturales siguientes a partir de la fecha de la firma del presente contrato, el PROVEEDOR realizar\u00e1 la devoluci\u00f3n del pago en un plazo no menor a 3 ni mayor a 8 d\u00edas naturales, mediante cheque nominativo para abono en cuenta del CONSUMIDOR y/o transferencia electr\u00f3nica de fondos. Para tal efecto el CONSUMIDOR al momento de solicitar la cancelaci\u00f3n indicar\u00e1 la CLABE interbancaria y el nombre del banco, plaza y sucursal correspondiente.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        En el supuesto de cancelaci\u00f3n del servicio, cuando el CONSUMIDOR haya hecho uso de los servicios contratados en el presente contrato, tendr\u00e1 que pagar los mismos y el PROVEEDOR devolver\u00e1 las cantidades restantes entregadas por adelantado.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        6.2 El contrato tambi�n ser\u00e1 cancelado en el caso de que el SAT revoque la autorizaci\u00f3n para operar como proveedor de CFDI, una vez publicada la revocaci\u00f3n de la autorizaci\u00f3n del PROVEEDOR en la p\u00e1gina electr\u00f3nica del SAT en internet (www.sat.gob.mx), se deber\u00e1 hacer del conocimiento del CONSUMIDOR, a trav�s de:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            a) La publicaci\u00f3n de un aviso en su p\u00e1gina de internet, y;\n" + 
				"            <br> b) El env\u00edo de un aviso mediante mensaje de datos dirigido a la direcci\u00f3n de correo electr\u00f3nico establecida al efecto de la cl\u00e1usula 14 del presente contrato, o en su caso mediante el medio que para este caso en especial se disponga en la citada cl\u00e1usula 14.\n" + 
				"\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        La publicaci\u00f3n y el env\u00edo de los avisos mencionados tendr\u00e1n como prop\u00f3sito informar oportunamente al CONSUMIDOR a fin de que pueda contratar un nuevo proveedor.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        6.3 A partir de la fecha de publicaci\u00f3n de la revocaci\u00f3n de la autorizaci\u00f3n del PROVEEDOR en la p\u00e1gina de internet del SAT, dicho PROVEEDOR continuar\u00e1 prestando el servicio contratado por el CONSUMIDOR durante un per\u00edodo de transici\u00f3n que ser\u00e1 de noventa d\u00edas contados a partir de la fecha de dicha publicaci\u00f3n.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        6.4 En caso de incumplimiento por parte del PROVEEDOR con las obligaciones se\u00f1aladas en esta cl\u00e1usula el CONSUMIDOR afectado podr\u00e1 hacer efectiva la pena convencional referida en la cl\u00e1usula 11 del presente contrato.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        7.- RESCISI\u00d3N.\n" + 
				"        <br> 7.1 En caso de incumplimiento del contrato, cualquiera de las partes podr\u00e1 solicitar la rescisi\u00f3n del mismo, por lo cual la parte que incumpla pagar\u00e1 la pena convencional establecida en la cl\u00e1usula 11 del presente contrato.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        7.2 En caso de que el PROVEEDOR tome acuerdo de disoluci\u00f3n, entre en proceso de liquidaci\u00f3n, concurso mercantil o extinci\u00f3n de la sociedad, se rescindir\u00e1 el contrato y el PROVEEDOR deber\u00e1 hacer esto del conocimiento del CONSUMIDOR, en un plazo no mayor de 24 horas contadas a partir de la toma del Acuerdo o de que surta efectos el acto jur\u00eddico que corresponda.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            a) Publicar un aviso en su p\u00e1gina de internet, y;\n" + 
				"            <br> b) Enviar un aviso mediante mensaje de datos dirigido a la direcci\u00f3n de correo electr\u00f3nico establecida al efecto en la cl\u00e1usula 14 del presente contrato o en su caso mediante el medio que para este caso en especial se disponga en la citada cl\u00e1usula 14.\n" + 
				"\n" + 
				"        </p>\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        7.3 Lo anterior para que el CONSUMIDOR pueda tomar las acciones que considere convenientes como es el contratar un nuevo proveedor; durante el periodo de transici\u00f3n que se desarrolle la disoluci\u00f3n, liquidaci\u00f3n o concurso mercantil, el PROVEEDOR deber\u00e1 seguir prestando el servicio contratado al CONSUMIDOR.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        7.4 En caso de incumplimiento por parte del PROVEEDOR con las obligaciones se\u00f1aladas en esta cl\u00e1usula el consumidor afectado podr\u00e1 hacer efectiva la pena convencional referida en la cl\u00e1usula 11 del presente contrato.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        8.- TERMINACI\u00d3N.\n" + 
				"        <br> 8.1 Las partes podr\u00e1n dar por terminado el presente contrato en cualquier momento, d\u00e1ndose aviso entre ellas por escrito, debiendo el PROVEEDOR dar de baja del servicio al CONSUMIDOR, eliminando de sus sistemas cualquier informaci\u00f3n con la que cuente, sin menoscabo de aqu�lla que deba conservar en virtud de las disposiciones fiscales vigentes relativas a la autorizaci\u00f3n como PROVEEDOR de certificaci\u00f3n CFDI. En virtud de lo anterior, el CONSUMIDOR reconoce y acepta, que una vez concluido el periodo de conservaci\u00f3n obligatorio se da por terminado el contrato y por ende ser\u00e1n canceladas las claves y contrase\u00f1as generadas al CONSUMIDOR para el uso y operaci\u00f3n del servicio.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"\n" + 
				"        8.2 El presente contrato terminar\u00e1 al vencimiento de su vigencia si este no es renovado, o cuando concluya el servicio pactado y por ende haya acabado su objeto.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        9.- RESPONSABILIDAD.\n" + 
				"        <br> 9.1 En caso de que el PROVEEDOR con motivo de la prestaci\u00f3n de los servicios contratados, por error, omisi\u00f3n o impericia cause da\u00f1o al CONSUMIDOR, se compromete a coadyuvar con este, tramitando las aclaraciones pertinentes y/o necesarias ante el SAT, que correspondan derivadas de dichos actos ante el Sistema de Administraci\u00f3n Tributaria, en un plazo no menor de dos (02) d\u00edas h\u00e1biles y no mayor de ocho (08) d\u00edas h\u00e1biles, as\u00ed como a resarcir el da\u00f1o causado al CONSUMIDOR. Lo anterior, sin perjuicio de las sanciones que en su caso le sean aplicables al propio PROVEEDOR derivado de las posibles inconsistencias o incumplimientos imputables al mismo.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        El PROVEEDOR no ser\u00e1 responsable de aquellos errores o fallas en los servicios cuando los mismos hayan sido ocasionados por causas imputables al CONSUMIDOR.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        El CONSUMIDOR manifiesta y acepta que para la contrataci\u00f3n de los servicios objeto del presente contrato, cuenta con los requerimientos t�cnicos que en esta materia se establecen en las disposiciones legales, reglamentarias y administrativas fiscales o en cualquier otra \u00edndole aplicable, mismos que de manera enunciativa m\u00e1s no limitativa incluyen el contar con certificado de e-firma vigente y con uno o m\u00e1s Certificados de Sellos Digital vigentes emitidos por el SAT.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        EL PROVEEDOR ser\u00e1 responsable del resguardo de los Certificados de Sellos Digital (CSD) vigentes emitidos por el SAT, del uso de la documentaci\u00f3n y de la validaci\u00f3n del comprobante que se emita por la prestaci\u00f3n de servicio.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        10.- PENA CONVENCIONAL.\n" + 
				"        <br> 10.1 Las partes convienen que en caso de incumplimiento de cualquiera de sus obligaciones y de conformidad con lo se\u00f1alado por el art\u00edculo 88 del C\u00f3digo de Comercio (CC) y art\u00edculo 73-TER, p\u00e1rrafo primero fracci\u00f3n IX del Reglamento de la Ley Federal de Protecci\u00f3n al Consumidor, la parte que incumpla pagar\u00e1 el treinta por ciento (30%) del valor del contrato; en caso de incumplimiento del PROVEEDOR, este deber\u00e1 devolver al CONSUMIDOR las cantidades que le haya entregado por adelantado, m\u00e1s la pena convencional, en un plazo no menor a cinco (05) ni mayor a diez (10) d\u00edas h\u00e1biles, la pena convencional pactada previamente mediante cheque nominativo para abono en cuenta del PROVEEDOR y/o transferencia electr\u00f3nica. Para tal efecto el PROVEEDOR indicar\u00e1 la CLABE interbancaria y el nombre del banco, plaza y sucursal.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        De igual forma, cuando el incumplimiento sea de parte del CONSUMIDOR, este deber\u00e1 de pagar al PROVEEDOR, en un plazo no menor a cinco (05) ni mayor a diez (10) d\u00edas h\u00e1biles, la pena convencional pactada previamente mediante cheque nominativo para abono en cuenta del PROVEEDOR y/o transferencia electr\u00f3nica. Para tal efecto el PROVEEDOR indicar\u00e1 la CLABE interbancaria y el nombre del banco, plaza y sucursal.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        11.- VIGENCIA.\n" + 
				"        <br> 11.1 El presente contrato tendr\u00e1 una vigencia de 12 meses. En caso de no establecerse vigencia determinada, el contrato estar\u00e1 vigente hasta que el PROVEEDOR concluya con los servicios contratados debiendo informar mediante mensaje de datos enviado a la direcci\u00f3n de correo electr\u00f3nico de contacto o al medio que al efecto se determine en la cl\u00e1usula 14 del presente instrumento al CONSUMIDOR que el servicio est\u00e1 por terminar, con una anticipaci\u00f3n de 90 d\u00edas naturales a la concusi\u00f3n del contrato.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        12.- PROTECCI\u00d3N DE DATOS PERSONALES.\n" + 
				"        <br> 12.1 Ambas partes convienen en que el contenido del presente contrato, la informaci\u00f3n proporcionada por el CONSUMIDOR para su ejecuci\u00f3n, la de los clientes del CONSUMIDOR, y la de cualquier tercero involucrado en las operaciones consignadas en los CFDI que se operen en ejecuci\u00f3n del objeto del presente contrato, tienen el car\u00e1cter de confidencial y por ende no pueden ser utilizadas para fines distintos al objeto del presente contrato ni divulgadas a terceros, con la salvedad de la informaci\u00f3n que las partes est�n obligadas a entregar al SAT o a tener a disposici\u00f3n de dicha autoridad fiscal en virtud de las disposiciones fiscales aplicables, incluyendo las disposiciones aplicables al PROVEEDOR como resultado o requisito de la autorizaci\u00f3n para operar como tal que le ha sido otorgada por el SAT, por lo que el PROVEEDOR se obliga expresamente a mantener absoluta reserva sobre la misma, y que \u00fanicamente podr\u00e1 ser revelada por mandamiento de autoridad competente de conformidad con lo se\u00f1alado por el art\u00edculo 1, 23, 24 y 25 de la Ley General de Transparencia y Acceso a la Informaci\u00f3n P\u00fablica y por la Ley de Protecci\u00f3n de Datos en Posesi\u00f3n de Particulares.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        12.2 La confidencialidad de la informaci\u00f3n a que se refiere la presente cl\u00e1usula incluye expresamente la prohibici\u00f3n para el PROVEEDOR de ceder o transmitir a terceros incluso con fines mercadot�cnicos o publicitarios los datos e informaci\u00f3n a que se refiere esta cl\u00e1usula, o de enviar publicidad sobre los bienes y servicios, salvo que conste la autorizaci\u00f3n expresa y por escrito del CONSUMIDOR.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        12.3 El contenido de la presente cl\u00e1usula se regular\u00e1, en lo conducente, por el C\u00f3digo Fiscal de la Federaci\u00f3n, la Resoluci\u00f3n Miscel\u00e1nea Fiscal vigente, las condiciones de otorgamiento de la autorizaci\u00f3n para operar como proveedor de certificaci\u00f3n de comprobantes fiscales digitales por internet y la Ley Federal de Protecci\u00f3n de Datos Personales en Posesi\u00f3n de los Particulares.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        13.- CONFIDENCIALIDAD. Las partes, expresamente reconocen que, como consecuencia de la celebraci\u00f3n del presente contrato, tendr\u00e1n acceso a informaci\u00f3n confidencial.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"\n" + 
				"        13.1 En virtud de lo anterior, las partes deber\u00e1n mantener, y proveer\u00e1n que sus funcionarios y empleados mantengan como confidencial toda la informaci\u00f3n que pueda conocer en relaci\u00f3n con la operaci\u00f3n del CLIENTE, as\u00ed como sus bienes, negocios, operaciones, prospectos o estrategias, incluyendo el presente contrato.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        El PROVEEDOR, bajo su m\u00e1s estricta responsabilidad, deber\u00e1 suscribir con cualquier tercero, diverso a este contrato, acuerdos de confidencialidad que permitan la no divulgaci\u00f3n de la informaci\u00f3n a la que tiene acceso en el cumplimiento del objeto del presente contrato, en los mismos t�rminos del acuerdo que �l tiene suscrito con EL CLIENTE, y deber\u00e1:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"            a) Tomar todas las precauciones para garantizar que la informaci\u00f3n confidencial no sea divulgada a ninguna persona f\u00edsica o moral no autorizada conforme a este convenio.\n" + 
				"            <br> b) En caso de que EL PROVEEDOR considere necesario divulgar o comunicar parte o la totalidad de la informaci\u00f3n confidencial a un tercero con el prop\u00f3sito menciona en el inciso a) anterior, EL PROVEEDOR deber\u00e1 notificar este hecho a EL CLIENTE, dentro de los tres (3) d\u00edas naturales siguientes, para que con anterioridad a que sea proporcionada la Informaci\u00f3n Confidencial, EL PROVEEDOR y el tercero en cuesti\u00f3n celebren un convenio de confidencialidad, respecto de la informaci\u00f3n que ser\u00e1 proporcionada.\n" + 
				"            <br> c) A devolver a EL CLIENTE la Informaci\u00f3n Confidencial, o en su caso si as\u00ed lo indica EL CLIENTE y en su presencia, si as\u00ed lo requiere, destruir todo documento que contenga la Informaci\u00f3n Confidencial. Asimismo, EL PROVEEDOR si as\u00ed lo requiere EL CLIENTE, expedir un recibo donde conste que toda la Informaci\u00f3n Confidencial ha sido devuelta o destruida, en el entendido de que el PROVEEDOR continuara obligado por el presente convenio, hasta por un t�rmino de cinco (5) a\u00f1os contado a partir del t�rmino de la vigencia del convenio de confidencialidad que se celebra por documento a parte del presente instrumento.\n" + 
				"            <br> d) La informaci\u00f3n confidencial tendr\u00e1 para todos los efectos legales el car\u00e1cter de secreto industrial, y por lo tanto se estar\u00e1 a lo dispuesto en el art\u00edculo 82 de la Ley de Propiedad Industrial vigente en los Estados Unidos Mexicanos.\n" + 
				"            <br> e) Todo lo anterior, no ser\u00e1 aplicable en casos de que la informaci\u00f3n que sea por su naturaleza disponible para el p\u00fablico, o bien, sea requerida de ser divulgada como resultado de cualquier ley o disposici\u00f3n de una autoridad gubernamental competente.\n" + 
				"            <br>\n" + 
				"\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        14.- MEDIOS DE CONTACTO Y NOTIFICACIONES.\n" + 
				"        <br> 14.1 Todas las notificaciones, requerimientos, autorizaciones, renuncias, avisos, cambio de domicilio y otras comunicaciones que deban realizarse conforme a este contrato, deber\u00e1n hacerse mediante mensajes de datos enviados a trav�s de medios electr\u00f3nicos, a las direcciones de correo electr\u00f3nico se\u00f1aladas en esta cl\u00e1usula, consider\u00e1ndolas como debidamente enviadas al momento en que el mensaje de datos salga del servidor de correo electr\u00f3nico del emisor y como debidamente recibidas por el destinatario en el momento que el mensaje de datos ingrese en el servidor de correo electr\u00f3nico del destinatario. Por lo que ambas partes se obligan a verificar con la debida oportunidad si han recibido alguna comunicaci\u00f3n de su contraparte.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        14.2 Las partes acuerdan que cuando se requiera que el mensaje de datos sea firmado de manera electr\u00f3nica, o que se acuse de recibo mediante mensaje de datos firmado electr\u00f3nicamente, esto se se\u00f1alar\u00e1 expresamente en cada caso y se utilizar\u00e1 siempre la e-firma que emite el Servicio de Administraci\u00f3n Tributaria al tenor de lo dispuesto por el art\u00edculo 17-D del C\u00f3digo Fiscal de la Federaci\u00f3n, as\u00ed como los servicios de validaci\u00f3n que presta dicha Autoridad Fiscal con acceso al p\u00fablico en general.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        14.3 Las direcciones para recibir las notificaciones, requerimientos, autorizaciones, renuncias, avisos, cambio de domicilio y otras comunicaciones ser\u00e1n:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        Por medios electr\u00f3nicos (Se pueden se\u00f1alar distintas direcciones electr\u00f3nicas clasific\u00e1ndolas por tema o asunto a tratar):\n" + 
				"    </p>\n" + 
				"    <div class=\"container\">\n" + 
				"        <ul class=\"estiloletrajustificada\">\n" + 
				"            <li>Del PROVEEDOR: liperez@quadrum.com.mx.</li>\n" + 
				"            <li>Del CONSUMIDOR ----- "+correo+" -----</li>\n" + 
				"\n" + 
				"        </ul>\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        De igual manera las PARTES acuerdan que dicha comunicaci\u00f3n se haga por escrito en papel con firma aut\u00f3grafa de la parte, su representante o apoderado legal, y se notifique en los siguientes domicilios f\u00edsicos:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        <strong>EL PROVEEDOR</strong>\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <table class=\"table table-bordered\">\n" + 
				"        <tbody>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Domicilio:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Calle Montecito #38 Piso 25 Oficina 22 Colonia N\u00e1poles CP. 03810 Ciudad de M�xico\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Delegaci\u00f3n/Municipio:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Delegaci\u00f3n Benito Ju\u00e1rez\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Tel�fono:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        (01) 5512091825\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Correo electr\u00f3nico:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        liperez@quadrum.com.mx\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Atenci\u00f3n:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Laura Ivete P\u00e9rez Serafin\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"        </tbody>\n" + 
				"    </table>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        <strong>EL CONSUMIDOR</strong>\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <table class=\"table table-bordered\">\n" + 
				"        <tbody>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Domicilio:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        "+domiciliofiscal+" \n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Delegaci\u00f3n/Municipio:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        "+domiciliofiscal+"\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Tel�fono:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        "+telefono+"\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Correo electr\u00f3nico:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        "+telefono+"\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"            <tr>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        Atenci\u00f3n:\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"                <th>\n" + 
				"                    <p class=\"estiloletrajustificada\">\n" + 
				"                        "+personacontacto+"\n" + 
				"                    </p>\n" + 
				"                </th>\n" + 
				"            </tr>\n" + 
				"        </tbody>\n" + 
				"    </table>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        15.- DUDAS, ACLARACIONES, QUEJAS O RECLAMACIONES.\n" + 
				"        <br> 15.1 El PROVEEDOR est\u00e1 obligado a atender las consultas, dudas, aclaraciones, quejas, o reclamaciones, formuladas por parte del CONSUMIDOR. El PROVEEDOR cuenta con el n\u00famero telef\u00f3nico (55) 9000-1234 y con la direcci\u00f3n de correo electr\u00f3nico se\u00f1alada en la cl\u00e1usula 14 del presente instrumento, en los cuales, el CONSUMIDOR deber\u00e1 levantar el reporte respectivo. Dicho servicio se prestar\u00e1 de forma gratuita al CONSUMIDOR y estar\u00e1 disponible las veinticuatro horas del d\u00eda, los trescientos sesenta y cinco (365) d\u00edas del a\u00f1o.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"\n" + 
				"        15.2 El PROVEEDOR deber\u00e1 atender las consultas, reclamaciones, o quejas formuladas por el CONSUMIDOR cumpliendo, al menos, los siguientes niveles de servicio:\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <ul>\n" + 
				"            <li>En la v\u00eda telef\u00f3nica:</li>\n" + 
				"\n" + 
				"        </ul>\n" + 
				"\n" + 
				"        <table class=\"table table-bordered\">\n" + 
				"            <tbody>\n" + 
				"                <tr>\n" + 
				"                    <th>\n" + 
				"                        <p class=\"estiloletrajustificada\">\n" + 
				"                            Consultas:\n" + 
				"                        </p>\n" + 
				"                    </th>\n" + 
				"\n" + 
				"                    <th>\n" + 
				"                        <p class=\"estiloletrajustificada\">\n" + 
				"                            De manera inmediata o en un m\u00e1ximo de 24 horas.\n" + 
				"                        </p>\n" + 
				"                    </th>\n" + 
				"                </tr>\n" + 
				"                <tr>\n" + 
				"                    <th>\n" + 
				"                        <p class=\"estiloletrajustificada\">\n" + 
				"                            Quejas o reclamaciones:\n" + 
				"                        </p>\n" + 
				"                    </th>\n" + 
				"                    <th>\n" + 
				"                        <p class=\"estiloletrajustificada\">\n" + 
				"                            De manera inmediata o en un m\u00e1ximo de 72 horas h\u00e1biles.\n" + 
				"                        </p>\n" + 
				"                    </th>\n" + 
				"                </tr>\n" + 
				"\n" + 
				"            </tbody>\n" + 
				"        </table>\n" + 
				"\n" + 
				"        <ul>\n" + 
				"            <li>V\u00eda correo electr\u00f3nico:</li>\n" + 
				"\n" + 
				"        </ul>\n" + 
				"\n" + 
				"        <table class=\"table table-bordered\">\n" + 
				"            <tbody>\n" + 
				"                <tr>\n" + 
				"                    <th>\n" + 
				"                        <p class=\"estiloletrajustificada\">\n" + 
				"                            Consultas:\n" + 
				"                        </p>\n" + 
				"                    </th>\n" + 
				"\n" + 
				"                    <th>\n" + 
				"                        <p class=\"estiloletrajustificada\">\n" + 
				"                            Dentro de un plazo m\u00e1ximo de 24 horas.\n" + 
				"                        </p>\n" + 
				"                    </th>\n" + 
				"                </tr>\n" + 
				"            </tbody>\n" + 
				"        </table>\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"\n" + 
				"        16.- CASO FORTUITO O FUERZA MAYOR.\n" + 
				"        <br> 16.1 Ninguna de las partes ser\u00e1 responsable de cualquier retraso o incumplimiento de este contrato, que resulte por caso fortuito o fuerza mayor.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        Se entiende por caso fortuito o fuerza mayor, el acontecimiento proveniente de la naturaleza o del hombre, caracterizado por ser imprevisible, inevitable, irresistible, insuperable, ajeno a la voluntad de las partes y que imposibilita el cumplimiento de todas o alguna de las obligaciones previstas en el presente contrato.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        La falta de previsi\u00f3n, o por alguna negligencia o impericia t�cnica del PROVEEDOR, que le impida el cabal cumplimiento de las obligaciones del presente contrato, no se considerar\u00e1 caso fortuito o fuerza mayor.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        17.- JURISDICCI\u00d3N Y COMPETENCIA.\n" + 
				"        <br> 17.1 Para todo lo relativo a la interpretaci\u00f3n, aplicaci\u00f3n y cumplimiento del contrato, las partes acuerdan someterse en la v\u00eda administrativa a la Procuradur\u00eda Federal del Consumidor (PROFECO), y en caso de subsistir diferencias, a la jurisdicci\u00f3n de los tribunales competentes del lugar donde se celebre este contrato o en su caso de los tribunales federales competentes.\n" + 
				"\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        Las partes acuerdan que conocen el contenido y alcance de todas y cada una de las declaraciones y cl\u00e1usulas del contrato y que los datos contenidos en las mismas son verdaderos y vigentes.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        17.2 El presente contrato podr\u00e1 celebrarse de manera electr\u00f3nica y en dicho caso se har\u00e1 uso de la e-firma que emite el SAT para la firma del documento electr\u00f3nico o mensaje de datos.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        Para efectos de la validaci\u00f3n de los certificados de la e-firma, se har\u00e1 uso de los servicios de validaci\u00f3n que pone a disposici\u00f3n el SAT en su p\u00e1gina de internet.\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        Una vez le\u00eddo y hecha la explicaci\u00f3n de su alcance legal y contenido, este contrato fue suscrito por duplicado en la Ciudad de M�xico y en la fecha "+fechaconformidad+", entreg\u00e1ndose un tanto a cada una de las PARTES.\n" + 
				"    </p>\n" +
				"\n" + 
				"<p class=\"estiloletrajustificada\">\n" + 
						"        <strong>Sello Digital del Contrato:</strong>\n" + 
						"    </p>\n" +
						"<p class=\"estiloletrajustificada\">"+selloDigitalContrato
						+ "</p>"+
				
				
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        <strong>Cadena original del contrato con el contribuyente:</strong>\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletracentado\">" +cadenaOriginal+ 
				"    </p>\n"
				+ "<p class=\"estiloletrajustificada\">\n" + 
				"<strong>Firma electr\u00f3nica del contrato:</strong></p>"+
				
				"<p class=\"estiloletrajustificada\">"+firmaelectronica+"</p>"+
				"</body>\n" + 
				"\n" + 
				"</html>";
	}
}
