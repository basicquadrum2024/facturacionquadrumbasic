package com.mx.quadrum.basic.documentos;

public class ManifiestoPersonaMoral {

	public static String crearmanifiestopersonamoral(int dia,String mes,int anio,String razonsocial,String rfc,String nombrerepresentantelegaapoderadolegal,String cadenaOriginal,String firmaelectronica,String selloDigitalManifiesto) {
		
		return "<!DOCTYPE html>\n" + 
				"<html lang=\"es\">\n" + 
				"\n" + 
				"<head>\n" + 
				"    <meta charset=\"utf-8\">\n" + 
				"    <title>Example 1</title>\n" + 
				"    <link rel=\"stylesheet\" href=\"bootstrap.css\" media=\"all\" />\n" + 
				"</head>\n" + 
				"<style type=\"text/css\">\n" + 
				"    .estiloletrajustificada {\n" + 
				"        font-size: 12px !important;\n" + 
				"        text-align: justify !important;\n" + 
				"    }\n" + 
				"    \n" + 
				"    .estiloletracentado {\n" + 
				"        text-align: center !important;\n" + 
				"        font-size: 12px !important;\n" + 
				"    }\n" + 
				"    \n" + 
				"    .style1 {\n" + 
				"        width: 10% !important;\n" + 
				"        border-top: 1px solid #8c8b8b !important;\n" + 
				"    }\n" + 
				"</style>\n" + 
				"\n" + 
				"<body>\n" + 
				"\n" + 
				"         <p class=\"estiloletracentado\">\n" + 
				"              <strong>MANIFIESTO</strong><br>\n" + 
				"\n" + 
				"Ciudad de M�xico, a -"+dia+"-- del mes -----"+mes+"----- de -"+anio+"--.\n" + 
				"\n" + 
				"         </p>\n" + 
				"\n" + 
				"         <p class=\"estiloletrajustificada\">\n" + 
				"             Por medio del presente -------------"+razonsocial+"------------------------------------ con Registro Federal de Contribuyentes (RFC)  -----------"+rfc+"----------, manifiesto mi conformidad y autorizaci\u00f3n para que la empresa Centro de Validaci\u00f3n Digital CVDSA S.A. de C.V., Proveedor Autorizado de Certificaci\u00f3n de CFDI con n\u00famero de autorizaci\u00f3n 69,901, proceda a entregar al Servicio de Administraci\u00f3n Tributaria, copia de los comprobantes fiscales que me haya certificado, de acuerdo a lo establecido en la regla 2.7.2.7 de la Resoluci\u00f3n Miscel\u00e1nea Fiscal para 2019 publicada en el Diario Oficial de la Federaci\u00f3n el 29 de Abril de 2019.<br><br>\n" + 
				"Al mismo tiempo acepto que Centro de Validaci\u00f3n Digital CVDSA S.A. de C.V., sea quien me proporcione a mi nombre o al de mi representada, el *Servicio de Certificaci\u00f3n para todos los Comprobantes Fiscales Digitales por Internet (CFDI) que emita ---------------"+razonsocial+"------------------.<br><br>\n" + 
				"Centro de Validaci\u00f3n Digital CVDSA S. A. de C.V. ser\u00e1 responsable del resguardo de los Certificados del Sello Digital (CSD) emitidos por el SAT que se encuentren vigentes, del uso de la documentaci\u00f3n y de la validaci\u00f3n del comprobante que se emita por la prestaci\u00f3n de servicio.\n" + 
				"<br><br>\n" + 
				"*Servicio de Certificaci\u00f3n: se refiere a validaci\u00f3n y timbrado de Comprobantes Fiscales Digitales por Internet (CFDI)\n" + 
				"\n" + 

" <p class=\"estiloletrajustificada\">\n" + 
"        <strong>Sello Digital del Manifiesto:</strong>\n" + 
"    </p>\n" +
"<p class=\"estiloletrajustificada\">"+selloDigitalManifiesto
+ "</p>"+
"\n" + 
"     <p class=\"estiloletrajustificada\">\n" + 
"        <strong>Cadena original del manifiesto con el contribuyente:</strong>\n" + 
"    </p>\n" + 
"\n" + 
"<p class=\"estiloletracentado\">" +cadenaOriginal+ 
"</p>\n" + 
" <p class=\"estiloletrajustificada\">\n" + 
"             <strong>Firma electr\u00f3nica del manifiesto:</strong>"+ 
"         </p> "+

"<p class=\"estiloletrajustificada\">"+firmaelectronica+"</p>"+
				"</body>\n" + 
				"\n" + 
				"</html>";
	}
}
