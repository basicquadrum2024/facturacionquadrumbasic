package com.mx.quadrum.basic.documentos;

public class ConvenioConfidencialidadPersonaFisica {
	
	
	
	public static String avisopersonaFisica(String nombrepersonafisica, String ineife, String folioine, String edad,
			String domiciliofiscal, String rfc, String telefono, String correo, String personacontacto,String fechaconformidad,String sello,String cargopersonafisica,int dia,String mes,int anio,String firmaelectronica,String selloDigitalConvenio) {
		
		return "<!DOCTYPE html>\n" + 
				"<html lang=\"es\">\n" + 
				"\n" + 
				"<head>\n" + 
				"    <meta charset=\"utf-8\">\n" + 
				"    <title>Example 1</title>\n" + 
				"    <link rel=\"stylesheet\" href=\"bootstrap.css\" media=\"all\" />\n" + 
				"</head>\n" + 
				"<style type=\"text/css\">\n" + 
				"    .estiloletrajustificada {\n" + 
				"        font-size: 12px !important;\n" + 
				"        text-align: justify !important;\n" + 
				"        /* font-weight: bold !important;*/\n" + 
				"    }\n" + 
				"    \n" + 
				"    .estiloletracentado {\n" + 
				"        text-align: center !important;\n" + 
				"        font-size: 12px !important;\n" + 
				"    }\n" + 
				"    \n" + 
				"    .style1 {\n" + 
				"        width: 10% !important;\n" + 
				"        border-top: 1px solid #8c8b8b !important;\n" + 
				"    }\n" + 
				"</style>\n" + 
				"\n" + 
				"<body>\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"        <strong>CONVENIO DE CONFIDENCIALIDAD, QUE CELEBRAN POR UNA PARTE ----- "+nombrepersonafisica+" -----, A QUIEN EN LO SUCESIVO SE LE DENOMINARA EL CLIENTE,  Y POR LA OTRA PARTE, CENTRO DE VALIDACION DIGITAL CVDSA S. A. DE C. V., A QUIEN EN LO SUCESIVO SE LE DENOMINAR\u00c1 QUADRUM, REPRESENTADO EN ESTE ACTO POR LA LIC. LAURA IVETE P\u00c9REZ SERAFIN, EN SU CAR\u00c1CTER DE REPRESENTANTE LEGAL, AL TENOR DE LAS DECLARACIONES Y CL\u00c1USULAS SIGUIENTES:</strong>\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletracentado\">\n" + 
				"        D E C L A R A C I O N E S\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"I. Declara EL CLIENTE, por conducto de su Representante o Apoderado legal, que:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"a)  Que es una persona f\u00edsica, de nacionalidad mexicana, ----- "+edad+" -----, que se encuentra en pleno uso de sus facultades legales para suscribir el presente contrato, quien se identifica ----- "+ineife+" -----, con n\u00famero de folio ----- "+folioine+" -----.\n" + 
				"<br><br>\n" + 
				"b)  Que para los fines y efectos legales del presente contrato se\u00f1ala como domicilio el ubicado en Calle ----- "+domiciliofiscal+" -----, con n\u00famero de tel�fono ----- "+telefono+"----- y correo electr\u00f3nico ----- "+correo+" -----<br><br>\n" + 
				"c)  Que su registro federal de contribuyentes es ----- "+rfc+" -----.\n" + 
				"<br><br>\n" + 
				"d)  Que proporcionar\u00e1 la informaci\u00f3n requerida por QUADRUM de forma completa, veraz y actualizada, para la realizaci\u00f3n de las actividades que deriven del objeto de este convenio.\n" + 
				"\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"    <p class=\"estiloletrajustificada\">\n" + 
				"II. Declara QUADRUM, por conducto de su Apoderada Legal, que:\n" + 
				"    </p>\n" + 
				"\n" + 
				"    <div class=\"container\">\n" + 
				"        <p class=\"estiloletrajustificada\">\n" + 
				"a)  Es una sociedad an\u00f3nima legalmente constituida conforme a las leyes mexicanas, lo que acredita con la escritura p\u00fablica n\u00famero 10,845, de fecha 8 de abril de 2011, otorgada ante la fe del Lic. Alfonso Mart\u00edn Le\u00f3n Orantes, titular de la Notar\u00eda P\u00fablica n\u00famero 238 del Distrito Federal, hoy Ciudad de M�xico, inscrita en el Registro P\u00fablico de la Propiedad y del Comercio del Distrito Federal hoy Ciudad de M�xico, bajo el folio mercantil electr\u00f3nico n\u00famero 451858-1, con fecha 24 de junio de 2011.\n" + 
				"<br><br>\n" + 
				"b)  Mediante escritura p\u00fablica n\u00famero 18,081 de fecha 15 de diciembre de 2015, otorgada ante la fe del Lic. Jorge Franco Mart\u00ednez, Titular de la Notar\u00eda P\u00fablica n\u00famero 81 del Distrito Federal hoy Ciudad de M�xico, inscrita en el Registro P\u00fablico de la Propiedad y del Comercio del Distrito Federal hoy Ciudad de M�xico, bajo el folio mercantil electr\u00f3nico n\u00famero 451858-1, con fecha 3 de mayo de 2016, se hizo constar entre otros, la venta de acciones.\n" + 
				"<br><br>\n" + 
				"c)  Mediante escritura p\u00fablica n\u00famero 127,741 de fecha 22 de diciembre de 2017, otorgada ante la fe del Lic. Jos� Eugenio Casta\u00f1eda Escobedo, Titular de la Notar\u00eda P\u00fablica n\u00famero 211 de la Ciudad de M�xico, inscrita en el Registro P\u00fablico de la Propiedad y del Comercio del Distrito Federal hoy Ciudad de M�xico, bajo el folio mercantil electr\u00f3nico n\u00famero 451858-1, con fecha 22 de febrero de 2018, se hizo constar entre otros, la modificaci\u00f3n a la forma de r�gimen de administraci\u00f3n de la sociedad y la designaci\u00f3n de miembros del consejo de administraci\u00f3n.\n" + 
				"<br><br>\n" + 
				"d)  Mediante escritura p\u00fablica n\u00famero 132,324 de fecha 6 de agosto de 2018, otorgada ante la fe del Lic. Jos� Eugenio Casta\u00f1eda Escobedo, Titular de la Notar\u00eda P\u00fablica n\u00famero 211 de la Ciudad de M�xico, inscrita en el Registro P\u00fablico de la Propiedad y del Comercio del Distrito Federal hoy Ciudad de M�xico, bajo el folio mercantil electr\u00f3nico n\u00famero 451858-1, con fecha 17 de agosto de 2018, se hizo constar entre otros, la ratificaci\u00f3n del capital social.\n" + 
				"<br><br>\n" + 
				"e)  La Lic. Laura Ivete P\u00e9rez Serafin, quien acredita su personalidad mediante el testimonio Notarial n\u00famero 58,895 pasado ante la fe del Licenciado Antonio Velarde Violante Notario P\u00fablico n\u00famero 164 de la Ciudad de M�xico, en fecha 05 de agosto de 2019, inscrito en el Registro P\u00fablico de Comercio de esta Ciudad, bajo el folio mercantil 451848-1, la cual otorga facultades suficientes para suscribir el presente instrumento.\n" + 
				"<br><br>\n" + 
				"f)  Tiene por objeto, entre otros, desarrollar, implementar y desarrollar sistemas inform\u00e1ticos, software y hardware en general, desarrollo de software, aplicaci\u00f3n de programas, lenguajes operativos. \n" + 
				"<br><br>\n" + 
				"g)  Tiene capacidad jur\u00eddica para celebrar este Contrato y re\u00fane las condiciones t�cnicas, materiales, humanas y econ\u00f3micas necesarias para obligarse a la prestaci\u00f3n de los servicios objeto del mismo. \n" + 
				"<br><br>\n" + 
				"h)  Conoce el contenido y alcances de los servicios objeto del presente instrumento. \n" + 
				"<br><br>\n" + 
				"i)  Cuenta con clave del Registro Federal de Contribuyentes n\u00famero CVD110412TF6.\n" + 
				"<br><br>\n" + 
				"j)  Se\u00f1ala como domicilio para los fines y efectos legales de este Contrato el ubicado en Calle Montecito n\u00famero 38 piso 25-22, Colonia N\u00e1poles, C\u00f3digo Postal 03810, Alcald\u00eda Benito Ju\u00e1rez, Ciudad de M�xico.\n" + 
				"\n" + 
				"        </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"       <p class=\"estiloletrajustificada\">\n" + 
				"              III.  Declaran conjuntamente las PARTES, que:\n" + 
				"         </p>\n" + 
				"\n" + 
				"         <div class=\"container\">\n" + 
				"         <p class=\"estiloletrajustificada\">\n" + 
				"              a)    Previamente a la suscripci\u00f3n de este instrumento han revisado y obtenido todas y cada una de las autorizaciones de car\u00e1cter legal y administrativo, adem\u00e1s de contar con las facultades y capacidad legal suficientes para tales efectos, las cuales no les han sido modificados, restringidas o revocadas en forma alguna, a la fecha de celebraci\u00f3n del presente Contrato.\n" + 
				"<br><br>\n" + 
				"b)  Reconocen en forma rec\u00edproca la personalidad con la que se ostentan y comparecen a la suscripci\u00f3n de este instrumento y admiten como propias, en lo que corresponda, todas y cada una de las declaraciones anteriores, por lo que est\u00e1n de acuerdo en obligarse en los t�rminos y condiciones que se estipulan en las cl\u00e1usulas del presente instrumento.\n" + 
				"<br><br>\n" + 
				"c)  Las partes acuerdan que para efectos del presente Convenio, el t�rmino INFORMACI\u00d3N CONFIDENCIAL significa aquella informaci\u00f3n escrita, oral, gr\u00e1fica o contenida en medios electromagn�ticos o en cualquier otro medio conocido o por conocer propiedad de LA FIDUCIARIA, incluyendo de manera enunciativa mas no limitativa, informaci\u00f3n comercial, financiera, t�cnica, industrial, de mercado, de negocios, informaci\u00f3n relativa a nombres de clientes o socios potenciales, ofertas de negocios, dise\u00f1os, marcas, know how, invenciones (patentables o no), f\u00f3rmulas, dibujos o datos t�cnicos, nombres, papeles, figuras, estudios, sistemas de producci\u00f3n, c\u00f3digo fuente, logaritmos, software, bases de datos, an\u00e1lisis, ideas, creaciones, presupuestos, reportes, planes, proyecciones, documentos de trabajo, compilaciones, comparaciones, estudios u otros documentos preparados, por cada una de las partes que contengan o reflejen dicha informaci\u00f3n y cualquier otra informaci\u00f3n a la que cada una de ellas o sus representantes tengan o pudieran tener acceso en virtud de la celebraci\u00f3n y ejecuci\u00f3n de este Convenio, siempre y cuando se encuentre fijada en un soporte material, y se indique expresamente que dicha informaci\u00f3n tiene el car\u00e1cter de confidencial.\n" + 
				"<br><br>\n" + 
				"d)  Las partes acuerdan que para efectos del presente Convenio, el t�rmino INFORMACI\u00d3N CONFIDENCIAL significa aquella informaci\u00f3n escrita, oral, gr\u00e1fica o contenida en medios electromagn�ticos o en cualquier otro medio conocido o por conocer propiedad de EL CLIENTE, incluyendo de manera enunciativa mas no limitativa, informaci\u00f3n comercial, financiera, t�cnica, industrial, de mercado, de negocios, informaci\u00f3n relativa a nombres de clientes o socios potenciales, ofertas de negocios, dise\u00f1os, marcas, know how, invenciones (patentables o no), f\u00f3rmulas, dibujos o datos t�cnicos, nombres, papeles, figuras, estudios, sistemas de producci\u00f3n, c\u00f3digo fuente, logaritmos, software, bases de datos, an\u00e1lisis, ideas, creaciones, presupuestos, reportes, planes, proyecciones, documentos de trabajo, compilaciones, comparaciones, estudios u otros documentos preparados, por cada una de las partes que contengan o reflejen dicha informaci\u00f3n y cualquier otra informaci\u00f3n a la que cada una de ellas o sus representantes tengan o pudieran tener acceso en virtud de la celebraci\u00f3n y ejecuci\u00f3n de este Convenio,  siempre y cuando se encuentre fijada en un soporte material, y se indique expresamente que dicha informaci\u00f3n tiene el car\u00e1cter de confidencial. \n" + 
				"<br><br>\n" + 
				"e)  Las partes acuerdan que no ser\u00e1 considerada como INFORMACI\u00d3N CONFIDENCIAL toda aquella que:<br><br>\n" + 
				"  Sea de dominio p\u00fablico.<br>\n" + 
				"  Haya sido entregada previamente a EL CLIENTE o a QUADRUM por un tercero y con el cual no se haya pactado confidencialidad al respecto.<br>\n" + 
				"f)  Que garantizan a la otra parte que tienen el derecho y la autoridad para establecer el presente Convenio y que no tienen ning\u00fan impedimento que pueda inhibir su capacidad para cumplir con los t�rminos y condiciones que se les impongan mediante este instrumento, por lo que est\u00e1n conformes en sujetar su compromiso a los t�rminos y condiciones insertos en las siguientes:\n" + 
				"\n" + 
				"         </p>\n" + 
				"\n" + 
				"    </div>\n" + 
				"\n" + 
				"        <p class=\"estiloletracentado\">\n" + 
				"              C L \u00c1 U S U L A S\n" + 
				"         </p>\n" + 
				"\n" + 
				"           <p class=\"estiloletrajustificada\">\n" + 
				"              PRIMERA. OBJETO\n" + 
				"El objeto del presente Convenio es establecer los t�rminos y condiciones bajo los cuales se manejar\u00e1 la INFORMACI\u00d3N CONFIDENCIAL relacionada con los servicios de Facturaci\u00f3n en L\u00ednea para las plazas de cobro del TRAMO CARRETERO que realizar\u00e1 QUADRUM.<br><br>\n" + 
				" \n" + 
				"SEGUNDA. ALCANCES.\n" + 
				"Para dar cumplimiento al objeto del presente Convenio, as\u00ed como sus asociados, funcionarios y su personal se obligan a no revelar de ninguna forma a terceras personas, ni hacer uso inapropiado de la ConvenioConfidencialidadPersonaFisica.avisopersonaFisicaINFORMACI\u00d3N CONFIDENCIAL que reciba de EL CLIENTE, o que por cualquier causa lleguen a conocer con motivo de las actividades desarrolladas con motivo del presente Convenio, o a utilizar en ulteriores etapas de desarrollo y negociaci\u00f3n con terceros interesados en su eventual explotaci\u00f3n comercial. Asimismo, QUADRUM se obliga a darle a la INFORMACI\u00d3N CONFIDENCIAL de EL CLIENTE, el mismo trato que le dar\u00eda a la INFORMACI\u00d3N CONFIDENCIAL de su propiedad; dicho tratamiento deber\u00e1 implicar un grado razonable de cuidado para evitar la revelaci\u00f3n o uso inapropiado de la INFORMACI\u00d3N CONFIDENCIAL.\n" + 
				"<br><br>\n" + 
				"TERCERA. COMPROMISOS DE LAS PARTES.\n" + 
				"QUADRUM se obliga a no utilizar la INFORMACI\u00d3N CONFIDENCIAL de EL CLIENTE para su propio beneficio o el de sus asociados, funcionarios, empleados, subcontratistas, consultores, agentes, cesionarios o cualquier otro tercero que de manera directa o indirecta tenga acceso a dicha INFORMACI\u00d3N CONFIDENCIAL. Asimismo, se obliga a solicitar la autorizaci\u00f3n expresa de EL CLIENTE para poder transmitirla a un tercero, en el entendido de que el tercero tendr\u00e1 la obligaci\u00f3n de no divulgar la INFORMACI\u00d3N CONFIDENCIAL por ning\u00fan medio.\n" + 
				"<br><br>\n" + 
				"QUADRUM instruir\u00e1 con toda precisi\u00f3n a sus asociados, empleados, dependientes, funcionarios, consejeros, auditores, asesores, abogados o consultores (en lo sucesivo denominados indistintamente representantes) que lleguen a tener relaci\u00f3n con el objeto del presente Convenio, respecto de los t�rminos de confidencialidad dispuestos en el mismo, debiendo obtener el compromiso de confidencialidad individual de cada uno de ellos.\n" + 
				"<br><br>\n" + 
				"QUADRUM dar\u00e1 a conocer la INFORMACI\u00d3N CONFIDENCIAL a sus representantes, asociados, en la medida que �stos la requieran para realizar las funciones identificadas en el presente Convenio, por lo que las partes en este acto se obligan solidariamente a hacer que cada uno de �stos conozcan y se obliguen a cumplir con todas y cada una de las estipulaciones del presente Convenio, oblig\u00e1ndose a no revelar la INFORMACI\u00d3N CONFIDENCIAL a tercero alguno sobre el trabajo que �stos le presten a las partes. No obstante lo anterior, en caso de que EL CLIENTE o QUADRUM o sus representantes requieran revelar alguna parte de la INFORMACI\u00d3N CONFIDENCIAL conforme a la ley o debido a alguna orden judicial o administrativa, la parte obligada, antes de revelar la INFORMACI\u00d3N CONFIDENCIAL, notificar\u00e1 al titular de dicha informaci\u00f3n dentro de los 3 (tres) d\u00edas naturales siguientes a la fecha en que se le hubiere ordenado la revelaci\u00f3n de la INFORMACI\u00d3N CONFIDENCIAL respectiva, indicando las razones de dicha revelaci\u00f3n, a fin de que se puedan implementar las medidas necesarias para proteger la INFORMACI\u00d3N CONFIDENCIAL.\n" + 
				"<br><br>\n" + 
				"QUADRUM se obliga a responder de los da\u00f1os y perjuicios directos que sufra EL CLIENTE por el incumplimiento a las disposiciones del presente Convenio o por la divulgaci\u00f3n o uso inapropiado o no autorizado que QUADRUM d� a la INFORMACI\u00d3N CONFIDENCIAL, ya sea a trav�s de sus asociados, funcionarios, personal, etc., o bien, de sus contadores, abogados o cualquier tercero que act\u00fae en su representaci\u00f3n, sin perjuicio del derecho de exigir el cumplimiento forzoso de este Convenio por los medios legales correspondientes y de las responsabilidades y sanciones legales previstas en el C\u00f3digo Penal Federal, la Ley Federal del Derecho de Autor, la Ley de la Propiedad Industrial y/o aquella Ley aplicable, por lo que deber\u00e1 tomar las precauciones necesarias y apropiadas para hacer del conocimiento de las personas antes se\u00f1aladas que la informaci\u00f3n es confidencial y no debe ser divulgada o utilizada inapropiadamente.\n" + 
				"<br><br>\n" + 
				"Las partes acuerdan, que EL CLIENTE es libre de establecer Convenios de Confidencialidad y/o cualquier otro documento que requiera con terceros, independientemente del intercambio de INFORMACI\u00d3N CONFIDENCIAL y la evaluaci\u00f3n mencionada en el presente documento.\n" + 
				"<br><br>\n" + 
				"CUARTA. PROPIEDAD INTELECTUAL.\n" + 
				"QUADRUM, reconoce que los derechos de propiedad intelectual existentes, derivados o relacionados con la INFORMACI\u00d3N CONFIDENCIAL, son y seguir\u00e1n siendo de EL CLIENTE, por lo que est\u00e1 de acuerdo en usar dicha informaci\u00f3n \u00fanicamente de la manera y para los prop\u00f3sitos autorizados en este Convenio; y que este instrumento no otorga, de manera expresa o impl\u00edcita, ning\u00fan derecho intelectual, ya sea de autor o de propiedad industrial, incluyendo de manera enunciativa mas no limitativa, una licencia de uso o cesi\u00f3n de derechos respecto de la INFORMACI\u00d3N CONFIDENCIAL.\n" + 
				"<br><br>\n" + 
				"QUADRUM reconoce que los archivos que conforman el Sistema de Recepci\u00f3n de CFDI’s, as\u00ed como cualquier c\u00f3digo fuente, plataforma, software, aplicaciones, programas acad�micos, manuales, notas de los cursos, dise\u00f1o, contenido tecnol\u00f3gico y m�todo de exposici\u00f3n, que se utilizar\u00e1n con el prop\u00f3sito de dar cumplimiento al objeto del presente Convenio son propiedad de EL CLIENTE y que debe d\u00e1rsele el tratamiento de informaci\u00f3n sujeta a derechos de propiedad intelectual. Por lo tanto, QUADRUM conviene en no quitar o alterar cualquier aviso de derecho de propiedad de EL CLIENTE o de cualquier otra entidad que est�n contenidos en los citados materiales.\n" + 
				"<br><br>\n" + 
				"Por lo que EL CLIENTE se reserva en t�rminos del art\u00edculo 106 de la Ley Federal del Derecho de Autor, las facultades de autorizar o prohibir cualquier uso o modificaci\u00f3n de los archivos que conforman el Sistema de Control Presupuestal, pudiendo requerir a QUADRUM una lista detallada de las personas que tengan acceso a su c\u00f3digo fuente, incluyendo la especificaci\u00f3n de la parte del c\u00f3digo al que tienen acceso y nivel de seguridad implementado para garantizar su confidencialidad. De igual manera, QUADRUM se compromete a abstenerse de realizar cualquier otro acto que implique la descompilaci\u00f3n, los procesos para revertir la ingenier\u00eda del software o programa de c\u00f3mputo y el desdensamblaje.\n" + 
				"<br><br>\n" + 
				"QUINTA. VIGENCIA.\n" + 
				"La vigencia del presente Convenio ser\u00e1 de un a\u00f1o contados a partir de la fecha de su firma.\n" + 
				"QUADRUM se obliga durante la vigencia de este Convenio y por un plazo de 05 a\u00f1os, contados a partir de la fecha de terminaci\u00f3n del mismo, a no revelar, utilizar, divulgar, transmitir, publicar, comunicar o entregar INFORMACI\u00d3N CONFIDENCIAL de EL CLIENTE en forma total o parcial. A la terminaci\u00f3n del presente instrumento, QUADRUM se compromete a devolver o destruir toda la INFORMACI\u00d3N CONFIDENCIAL que le haya sido entregada por EL CLIENTE seg\u00fan �sta se lo indique, certificando por escrito que se ha destruido o devuelto toda la informaci\u00f3n que se encontraba en su poder.\n" + 
				"<br><br>\n" + 
				"\n" + 
				"SEXTA. RESPONSABLES.\n" + 
				"Para la entrega y recepci\u00f3n de la INFORMACI\u00d3N CONFIDENCIAL, las partes designar\u00e1n como responsables:\n" + 
				"<br>\n" + 
				"EL CLIENTE nombra como responsable al se\u00f1or ----- "+nombrepersonafisica+" -----, jefe de ----- "+cargopersonafisica+" -----.<br>\n" + 
				"QUADRUM nombra como responsable a Ing. Jos� Manuel Soberanis Su\u00e1rez, encargado del \u00e1rea de seguridad de la informaci\u00f3n.\n" + 
				"<br><br>\n" + 
				"Los responsables nombrados por cada una de las partes, ser\u00e1n los contactos institucionales por medio de los cuales ser\u00e1n presentadas todas las comunicaciones oficiales derivadas de este instrumento. Adem\u00e1s, ser\u00e1n los responsables de las actividades encomendadas por su Instituci\u00f3n.\n" + 
				"\n" + 
				"<br><br>\n" + 
				"S\u00c9PTIMA. ACUERDO DEFINITIVO.\n" + 
				"Este Convenio constituye el acuerdo total entre las partes respecto a la INFORMACI\u00d3N CONFIDENCIAL y sustituye a cualquier otro entendimiento previo, oral o escrito que haya existido entre las partes.\n" + 
				"<br><br>\n" + 
				"OCTAVA. MODIFICACIONES.\n" + 
				"El presente Convenio s\u00f3lo podr\u00e1 ser modificado y/o adicionado durante su vigencia, mediante la firma del Convenio Modificatorio respectivo, el cual ser\u00e1 suscrito por quienes cuenten con facultades suficientes para ello y formar\u00e1n parte integrante del presente instrumento, dichas modificaciones obligar\u00e1n a los signatarios a partir de la fecha de su firma.\n" + 
				"<br><br>\n" + 
				"NOVENA. RESPONSABILIDAD CIVIL.\n" + 
				"Las partes no tendr\u00e1n responsabilidad civil por da\u00f1os y perjuicios que pudieran causarse como consecuencia del caso fortuito o fuerza mayor, particularmente por paro de labores acad�mico-administrativas, en virtud de lo cual pudieran encontrarse impedidas para cumplir oportunamente con los compromisos derivados del objeto y alcance del presente Convenio. En caso de interrupci\u00f3n de actividades por esta causa, ambas partes se comprometen a reiniciar sus actividades inmediatamente despu�s de que las causas de fuerza mayor hayan desaparecido.\n" + 
				"<br><br>\n" + 
				"D\u00c9CIMA. RELACI\u00d3N LABORAL.\n" + 
				"Las partes convienen en que el personal que cada un aporte para la ejecuci\u00f3n del objeto materia del presente Convenio, se entender\u00e1 exclusivamente relacionado con aqu�lla que lo emple\u00f3, por ende, cada una de ellas asumir\u00e1 su responsabilidad por este concepto y en ning\u00fan caso ser\u00e1n consideradas como patrones solidarios o sustitutos.\n" + 
				"<br><br>\n" + 
				"D\u00c9CIMA PRIMERA. RESCISI\u00d3N.\n" + 
				"Las partes convienen que ser\u00e1 motivo de rescisi\u00f3n del presente Convenio:\n" + 
				"a.  El que alguna declaraci\u00f3n de este documento sea falsa.\n" + 
				"b.  El incumplimiento de alguna de las obligaciones consignadas en este instrumento.\n" + 
				"c.  Que QUADRUM sea declarada en concurso mercantil en cualquiera de sus etapas.\n" + 
				"d.  En general, por cualquier causa imputable a QUADRUM o a su personal que implique incumplimiento, total o parcial, a lo previsto en el presente instrumento.\n" + 
				"<br><br>\n" + 
				"D\u00c9CIMA SEGUNDA. PROCEDIMIENTO DE RESCISI\u00d3N.\n" + 
				"Si alguna de las partes considera que la contraparte ha incurrido en algunas de las causas de rescisi\u00f3n que se consignan en este instrumento, lo comunicar\u00e1 por escrito a la otra, a fin de que la misma, en un plazo de 15 d\u00edas h\u00e1biles, exponga lo que a su derecho convenga. Si despu�s de analizar las razones aducidas, la parte demandante estima que las mismas no son satisfactorias, podr\u00e1 optar por exigir el cumplimiento del Convenio, o bien la rescisi\u00f3n del mismo.\n" + 
				"<br><br>\n" + 
				"En caso de rescisi\u00f3n, las partes tomar\u00e1n las medidas necesarias para evitarse perjuicios. En caso de que QUADRUM sea declarada en concurso mercantil en cualquiera de sus etapas, la responsabilidad de mantener la confidencialidad de la informaci\u00f3n que EL CLIENTE le haya proporcionado, recaer\u00e1 sobre las personas que representan a QUADRUM en ese momento, las cuales deber\u00e1n cumplir con las obligaciones contenidas en los p\u00e1rrafos que anteceden.\n" + 
				"<br><br>\n" + 
				"D\u00c9CIMA TERCERA. TERMINACI\u00d3N ANTICIPADA.\n" + 
				"Cualquiera de las partes podr\u00e1 dar por terminado el presente instrumento con antelaci\u00f3n a su vencimiento mediante aviso por escrito a su contraparte, notific\u00e1ndola con 30 d\u00edas naturales de anticipaci\u00f3n. En tal caso, ambas partes tomar\u00e1n las medidas necesarias para evitarse perjuicios tanto a ellas como a terceros.\n" + 
				"<br><br>\n" + 
				"D\u00c9CIMA CUARTA. JURISDICCI\u00d3N.\n" + 
				"Las partes convienen que el presente instrumento es producto de la buena fe, por lo que toda controversia e interpretaci\u00f3n que se derive del mismo, respecto de su operaci\u00f3n, formalizaci\u00f3n y cumplimiento, ser\u00e1 resuelta en primera instancia por las partes.\n" + 
				"<br><br>\n" + 
				"En caso de que no llegar\u00e1n a un acuerdo, las partes se someter\u00e1n a la jurisdicci\u00f3n de los Tribunales Federales en la Ciudad de M�xico y a las disposiciones contenidas en la Ley Federal del Derecho de Autor, la Ley de Propiedad Industrial, en el C\u00f3digo Civil Federal vigente y dem\u00e1s aplicables al caso, por lo que renuncian expresamente al fuero que por raz\u00f3n de su domicilio presente o futuro pudiera corresponderles.\n" + 
				"<br><br>\n" + 
				"Le\u00eddo que fue el presente instrumento y enteradas las partes de su contenido y alcance, lo firman por duplicado, en la Ciudad de M�xico, a los --"+dia+"--d\u00edas del mes de -- "+mes+" -- de -- "+anio+"--.\n" + 
				"\n" + 
				"\n" + 
				"         </p>\n" + 
				"\n" + 
				
" <p class=\"estiloletrajustificada\">\n" + 
"        <strong>Sello Digital del Convenio:</strong>\n" + 
"    </p>\n" +
"<p class=\"estiloletrajustificada\">"+selloDigitalConvenio
+ "</p>"+
"\n" + 
"     <p class=\"estiloletrajustificada\">\n" + 
"        <strong>Cadena original del convenio con el contribuyente:</strong>\n" + 
"    </p>\n" + 
"\n" + 
"<p class=\"estiloletracentado\">"+sello+ 
"</p>\n" + 
" <p class=\"estiloletrajustificada\">\n" + 
"             <strong>Firma electr\u00f3nica del convenio:</strong>"+ 
"         </p> "+

"<p class=\"estiloletrajustificada\">"+firmaelectronica+"</p>"+
				"</body>\n" + 
				"\n" + 
				"</html>";
	}

}
