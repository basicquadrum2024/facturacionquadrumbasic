package com.mx.quadrum.basic.entity.catalogosSAT;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Aduana generated by hbm2java
 */
@Entity
@Table(name="aduana"
    ,catalog="catalogosSAT"
)
public class Aduana  implements java.io.Serializable {


     private Integer id;
     private String clave;
     private String descripcion;

    public Aduana() {
    }

    public Aduana(String clave, String descripcion) {
       this.clave = clave;
       this.descripcion = descripcion;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    
    @Column(name="clave", nullable=false, length=10)
    public String getClave() {
        return this.clave;
    }
    
    public void setClave(String clave) {
        this.clave = clave;
    }

    
    @Column(name="descripcion", nullable=false, length=450)
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }




}