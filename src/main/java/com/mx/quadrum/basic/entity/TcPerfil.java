package com.mx.quadrum.basic.entity;
// Generated 13/06/2017 01:21:08 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * TcPerfil generated by hbm2java
 */
@Entity
@Table(name="tc_perfil"
    ,catalog="facturacion_basica"
)
public class TcPerfil  implements java.io.Serializable {


     private long id;
     private String nombre;
     private String descripcion;
     private Set<TcAcciones> tcAccioneses = new HashSet<TcAcciones>(0);
     private Set<TwUsuario> twUsuarios = new HashSet<TwUsuario>(0);

    public TcPerfil() {
    }

	
    public TcPerfil(long id) {
        this.id = id;
    }
    public TcPerfil(long id, String nombre, String descripcion, Set tcAccioneses, Set twUsuarios) {
       this.id = id;
       this.nombre = nombre;
       this.descripcion = descripcion;
       this.tcAccioneses = tcAccioneses;
       this.twUsuarios = twUsuarios;
    }
   
     @Id 

    
    @Column(name="id", unique=true, nullable=false)
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }

    
    @Column(name="nombre", length=45)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    @Column(name="descripcion", length=45)
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

@ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(name="tw_perfil_acciones", catalog="facturacion_basica", joinColumns = { 
        @JoinColumn(name="tc_perfil_id", nullable=false, updatable=false) }, inverseJoinColumns = { 
        @JoinColumn(name="tc_acciones_id", nullable=false, updatable=false) })
    public Set<TcAcciones> getTcAccioneses() {
        return this.tcAccioneses;
    }
    
    public void setTcAccioneses(Set<TcAcciones> tcAccioneses) {
        this.tcAccioneses = tcAccioneses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="tcPerfil")
    public Set<TwUsuario> getTwUsuarios() {
        return this.twUsuarios;
    }
    
    public void setTwUsuarios(Set<TwUsuario> twUsuarios) {
        this.twUsuarios = twUsuarios;
    }




}


