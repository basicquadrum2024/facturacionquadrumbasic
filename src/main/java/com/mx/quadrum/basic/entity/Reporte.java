package com.mx.quadrum.basic.entity;

import java.io.Serializable;

public class Reporte implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4022462213738840073L;
	private Object[] objetos;

	public Object[] getObjetos() {
		return objetos;
	}

	public void setObjetos(Object[] objetos) {
		this.objetos = objetos;
	}

}
