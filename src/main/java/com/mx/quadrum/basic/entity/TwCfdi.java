package com.mx.quadrum.basic.entity;

// Generated 13/06/2017 01:21:08 PM by Hibernate Tools 4.3.1

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * TwCfdi generated by hbm2java
 */
@Entity
@Table(name = "tw_cfdi", catalog = "facturacion_basica")
public class TwCfdi implements java.io.Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = -5860821738517811672L;
    private Long id;
    private TwCfdi twCfdi;
    private String uuid;
    private Date fechaCreacion;
    private Date fechaCertificacion;
    private Date fechaCancelacion;
    private String direccionXml;
    private String folio;
    private String serie;
    private Long estatus;
    private String claveConfirmacion;
    private String metodoPago;
    private String monedaDr;
    private BigDecimal total;
    private int enviado;
    private String direccionAcuseRecepcion;
    private Set<TrGeneral> trGenerals = new HashSet<TrGeneral>(0);
    private Set<TwCfdi> twCfdis = new HashSet<TwCfdi>(0);

    private BigDecimal totalPagado;
    private BigDecimal restante;
    private Long totalParcialidades;

    private boolean __ng_selected__;

    public TwCfdi() {

    }

    public TwCfdi(Long id) {
	this.id = id;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tw_cfdi_id")
    public TwCfdi getTwCfdi() {
	return this.twCfdi;
    }

    public void setTwCfdi(TwCfdi twCfdi) {
	this.twCfdi = twCfdi;
    }

    @Column(name = "uuid", length = 45)
    public String getUuid() {
	return this.uuid;
    }

    public void setUuid(String uuid) {
	this.uuid = uuid;
    }

    // @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Column(name = "fecha_creacion", length = 19)
    public Date getFechaCreacion() {
	return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
	this.fechaCreacion = fechaCreacion;
    }

    // @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Column(name = "fecha_certificacion", length = 19)
    public Date getFechaCertificacion() {
	return this.fechaCertificacion;
    }

    public void setFechaCertificacion(Date fechaCertificacion) {
	this.fechaCertificacion = fechaCertificacion;
    }

    @Column(name = "direccionXml", length = 500)
    public String getDireccionXml() {
	return this.direccionXml;
    }

    public void setDireccionXml(String direccionXml) {
	this.direccionXml = direccionXml;
    }

    @Column(name = "folio", length = 10)
    public String getFolio() {
	return this.folio;
    }

    public void setFolio(String folio) {
	this.folio = folio;
    }

    @Column(name = "serie", length = 45)
    public String getSerie() {
	return this.serie;
    }

    public void setSerie(String serie) {
	this.serie = serie;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "twCfdi")
    public Set<TrGeneral> getTrGenerals() {
	return this.trGenerals;
    }

    public void setTrGenerals(Set<TrGeneral> trGenerals) {
	this.trGenerals = trGenerals;
    }

    @Column(name = "estatus")
    public Long getEstatus() {
	return estatus;
    }

    public void setEstatus(Long estatus) {
	this.estatus = estatus;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Column(name = "fecha_cancelacion")
    public Date getFechaCancelacion() {
	return fechaCancelacion;
    }

    public void setFechaCancelacion(Date fechaCancelacion) {
	this.fechaCancelacion = fechaCancelacion;
    }

    @Column(name = "clave_confirmacion")
    public String getClaveConfirmacion() {
	return claveConfirmacion;
    }

    public void setClaveConfirmacion(String claveConfirmacion) {
	this.claveConfirmacion = claveConfirmacion;
    }

    @Column(name = "metodoPago", length = 500)
    public String getMetodoPago() {
	return this.metodoPago;
    }

    public void setMetodoPago(String metodoPago) {
	this.metodoPago = metodoPago;
    }

    @Column(name = "monedaDr", length = 500)
    public String getMonedaDr() {
	return this.monedaDr;
    }

    public void setMonedaDr(String monedaDr) {
	this.monedaDr = monedaDr;
    }

    @Column(name = "total", precision = 18, scale = 6)
    public BigDecimal getTotal() {
	return this.total;
    }

    public void setTotal(BigDecimal total) {
	this.total = total;
    }
    
    @Column(name = "enviado", nullable = false)
    public int getEnviado() {
		return enviado;
	}

	public void setEnviado(int enviado) {
		this.enviado = enviado;
	}
	
	@Column(name = "direccion_acuse_recepcion")
	public String getDireccionAcuseRecepcion() {
		return direccionAcuseRecepcion;
	}

	public void setDireccionAcuseRecepcion(String direccionAcuseRecepcion) {
		this.direccionAcuseRecepcion = direccionAcuseRecepcion;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "twCfdi")
    public Set<TwCfdi> getTwCfdis() {
	return this.twCfdis;
    }

    public void setTwCfdis(Set<TwCfdi> twCfdis) {
	this.twCfdis = twCfdis;
    }

    @Transient
    public BigDecimal getTotalPagado() {
	return totalPagado;
    }

    public void setTotalPagado(BigDecimal totalPagado) {
	this.totalPagado = totalPagado;
    }

    @Transient
    public BigDecimal getRestante() {
	return restante;
    }

    public void setRestante(BigDecimal restante) {
	this.restante = restante;
    }

    @Transient
    public Long getTotalParcialidades() {
	return totalParcialidades;
    }

    public void setTotalParcialidades(Long totalParcialidades) {
	this.totalParcialidades = totalParcialidades;
    }

    @Transient
    @JsonIgnore
    public boolean is__ng_selected__() {
	return __ng_selected__;
    }

    public void set__ng_selected__(boolean __ng_selected__) {
	this.__ng_selected__ = __ng_selected__;
    }

}
