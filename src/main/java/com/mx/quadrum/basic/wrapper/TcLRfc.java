package com.mx.quadrum.basic.wrapper;

public class TcLRfc implements java.io.Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = -240137991009019328L;
	private String rfc;
	private Integer sncf;
	private Integer subcontratacion;
	private String nombre;
	private Integer codigoPostal;

	public TcLRfc() {
	}

	public TcLRfc(String rfc) {
		this.rfc = rfc;
	}

	public TcLRfc(String rfc, Integer sncf, Integer subcontratacion) {
		this.rfc = rfc;
		this.sncf = sncf;
		this.subcontratacion = subcontratacion;
	}

	public String getRfc() {
		return this.rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public Integer getSncf() {
		return this.sncf;
	}

	public void setSncf(Integer sncf) {
		this.sncf = sncf;
	}

	public Integer getSubcontratacion() {
		return this.subcontratacion;
	}

	public void setSubcontratacion(Integer subcontratacion) {
		this.subcontratacion = subcontratacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	
	
}
