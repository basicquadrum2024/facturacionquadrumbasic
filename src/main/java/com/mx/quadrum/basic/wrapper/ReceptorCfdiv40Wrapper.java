package com.mx.quadrum.basic.wrapper;

public class ReceptorCfdiv40Wrapper {

	private boolean existeLrfc = true;
	private Long id;
	private String rfc;
	private String nombre;
	private String residenciaFiscal;
	private String numRegIdTrib;
	private String usoCfdi;
	private String domicilio;
	private String regimenFiscalReceptor;
	private String domicilioFiscalReceptor;

	public boolean isExisteLrfc() {
		return existeLrfc;
	}

	public void setExisteLrfc(boolean existeLrfc) {
		this.existeLrfc = existeLrfc;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getResidenciaFiscal() {
		return residenciaFiscal;
	}

	public void setResidenciaFiscal(String residenciaFiscal) {
		this.residenciaFiscal = residenciaFiscal;
	}

	public String getNumRegIdTrib() {
		return numRegIdTrib;
	}

	public void setNumRegIdTrib(String numRegIdTrib) {
		this.numRegIdTrib = numRegIdTrib;
	}

	public String getUsoCfdi() {
		return usoCfdi;
	}

	public void setUsoCfdi(String usoCfdi) {
		this.usoCfdi = usoCfdi;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getRegimenFiscalReceptor() {
		return regimenFiscalReceptor;
	}

	public void setRegimenFiscalReceptor(String regimenFiscalReceptor) {
		this.regimenFiscalReceptor = regimenFiscalReceptor;
	}

	public String getDomicilioFiscalReceptor() {
		return domicilioFiscalReceptor;
	}

	public void setDomicilioFiscalReceptor(String domicilioFiscalReceptor) {
		this.domicilioFiscalReceptor = domicilioFiscalReceptor;
	}

	
	
}
