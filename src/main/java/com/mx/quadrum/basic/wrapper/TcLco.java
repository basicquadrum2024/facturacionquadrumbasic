package com.mx.quadrum.basic.wrapper;

import java.util.Date;

public class TcLco implements java.io.Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = -3929410815078052018L;
	private TcLcoId id;
	private Integer validezObligaciones;
	private String estatusCertificado;
	private Date fechaFinal;
	private Date fechaInicio;

	public TcLco() {
	}

	public TcLco(TcLcoId id) {
		this.id = id;
	}

	public TcLco(TcLcoId id, Integer validezObligaciones, String estatusCertificado, Date fechaFinal,
			Date fechaInicio) {
		this.id = id;
		this.validezObligaciones = validezObligaciones;
		this.estatusCertificado = estatusCertificado;
		this.fechaFinal = fechaFinal;
		this.fechaInicio = fechaInicio;
	}

	public TcLcoId getId() {
		return this.id;
	}

	public void setId(TcLcoId id) {
		this.id = id;
	}

	public Integer getValidezObligaciones() {
		return this.validezObligaciones;
	}

	public void setValidezObligaciones(Integer validezObligaciones) {
		this.validezObligaciones = validezObligaciones;
	}

	public String getEstatusCertificado() {
		return this.estatusCertificado;
	}

	public void setEstatusCertificado(String estatusCertificado) {
		this.estatusCertificado = estatusCertificado;
	}

	public Date getFechaFinal() {
		return this.fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

}
