package com.mx.quadrum.basic.wrapper;
/**
 * Clase para el control del complemento Pagos
 * @author Quadrum
 *
 */
public class PagosPremium {

    private String fechaPago;
    private String formaPago;
    private String monto;
    private String idDocumento;
    private String metodoPago;
    private String noParcialidad;
    private String ISA;
    private String ISI;
    private String moneda;
    private String importePagado;
    private String monedaDR;

    public String getFechaPago() {
	return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
	this.fechaPago = fechaPago;
    }

    public String getMonto() {
	return monto;
    }

    public void setMonto(String monto) {
	this.monto = monto;
    }

    public String getIdDocumento() {
	return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
	this.idDocumento = idDocumento;
    }

    public String getFormaPago() {
	return formaPago;
    }

    public void setFormaPago(String formaPago) {
	this.formaPago = formaPago;
    }

    public String getMetodoPago() {
	return metodoPago;
    }

    public void setMetodoPago(String metodoPago) {
	this.metodoPago = metodoPago;
    }

    public String getMoneda() {
	return moneda;
    }

    public void setMoneda(String moneda) {
	this.moneda = moneda;
    }

    public String getNoParcialidad() {
	return noParcialidad;
    }

    public void setNoParcialidad(String noParcialidad) {
	this.noParcialidad = noParcialidad;
    }

    public String getISA() {
	return ISA;
    }

    public void setISA(String ISA) {
	this.ISA = ISA;
    }

    public String getISI() {
	return ISI;
    }

    public void setISI(String ISI) {
	this.ISI = ISI;
    }

    public String getImportePagado() {
	return importePagado;
    }

    public void setImportePagado(String importePagado) {
	this.importePagado = importePagado;
    }

    public String getMonedaDR() {
	return monedaDR;
    }

    public void setMonedaDR(String monedaDR) {
	this.monedaDR = monedaDR;
    }
}