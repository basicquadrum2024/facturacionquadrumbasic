package com.mx.quadrum.basic.wrapper;

import java.util.List;
/**
 * Entidad BusquedasWrapper que sirve para el paso de parametros para la paginacion  de busquedas 
 * @author Quadrum
 *
 */

public class BusquedasWrapper {

    private String criterio;
    private int tamanioPaguina;
    private int numPaguina;
    private String valor1;
    private String valor2;
    private int totalDatos;
    private List<?> listaDatos;

    public String getCriterio() {
	return criterio;
    }

    public void setCriterio(String criterio) {
	this.criterio = criterio;
    }

    public int getTamanioPaguina() {
	return tamanioPaguina;
    }

    public void setTamanioPaguina(int tamanioPaguina) {
	this.tamanioPaguina = tamanioPaguina;
    }

    public int getNumPaguina() {
	return numPaguina;
    }

    public void setNumPaguina(int numPaguina) {
	this.numPaguina = numPaguina;
    }

    public String getValor1() {
	return valor1;
    }

    public void setValor1(String valor1) {
	this.valor1 = valor1;
    }

    public String getValor2() {
	return valor2;
    }

    public void setValor2(String valor2) {
	this.valor2 = valor2;
    }

    public int getTotalDatos() {
	return totalDatos;
    }

    public void setTotalDatos(int totalDatos) {
	this.totalDatos = totalDatos;
    }

    public List<?> getListaDatos() {
	return listaDatos;
    }

    public void setListaDatos(List<?> listaDatos) {
	this.listaDatos = listaDatos;
    }

}
