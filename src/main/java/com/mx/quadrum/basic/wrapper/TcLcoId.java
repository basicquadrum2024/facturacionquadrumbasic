package com.mx.quadrum.basic.wrapper;

public class TcLcoId implements java.io.Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = -5281411136082426933L;
	private String rfc;
	private String noCertificado;

	public TcLcoId() {
	}

	public TcLcoId(String rfc, String noCertificado) {
		this.rfc = rfc;
		this.noCertificado = noCertificado;
	}

	public String getRfc() {
		return this.rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getNoCertificado() {
		return this.noCertificado;
	}

	public void setNoCertificado(String noCertificado) {
		this.noCertificado = noCertificado;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TcLcoId))
			return false;
		TcLcoId castOther = (TcLcoId) other;

		return ((this.getRfc() == castOther.getRfc())
				|| (this.getRfc() != null && castOther.getRfc() != null && this.getRfc().equals(castOther.getRfc())))
				&& ((this.getNoCertificado() == castOther.getNoCertificado())
						|| (this.getNoCertificado() != null && castOther.getNoCertificado() != null
								&& this.getNoCertificado().equals(castOther.getNoCertificado())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getRfc() == null ? 0 : this.getRfc().hashCode());
		result = 37 * result + (getNoCertificado() == null ? 0 : this.getNoCertificado().hashCode());
		return result;
	}

}
