package com.mx.quadrum.basic.wrapper;

import java.math.BigDecimal;

import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.cfdi.complementos.pagos.v10.schema.Pagos.Pago;
/**
 * Clase para el control de datos de el complemento de pagos 
 * @author Quadrum
 *
 */
public class WrapperPagos {

    private TwCfdi cfdiPadre;
    private TcReceptor receptor;
    private TcContribuyente contribuyente;
    private Pago pago;
    private BigDecimal faltante;
    private String uuidCfdiRelacionado;
    private String serieComprobante;
    private int decimales;
   // private Impuestos impuestos;

    public TwCfdi getCfdiPadre() {
	return cfdiPadre;
    }

    public void setCfdiPadre(TwCfdi cfdiPadre) {
	this.cfdiPadre = cfdiPadre;
    }

    public TcReceptor getReceptor() {
	return receptor;
    }

    public void setReceptor(TcReceptor receptor) {
	this.receptor = receptor;
    }

    public TcContribuyente getContribuyente() {
	return contribuyente;
    }

    public void setContribuyente(TcContribuyente contribuyente) {
	this.contribuyente = contribuyente;
    }

    public Pago getPago() {
	return pago;
    }

    public void setPago(Pago pago) {
	this.pago = pago;
    }

    public boolean getFaltante() {
	return (faltante.compareTo(BigDecimal.ZERO) == 0);
    }

    public void setFaltante(BigDecimal faltante) {
	this.faltante = faltante;
    }

    public String getUuidCfdiRelacionado() {
	return uuidCfdiRelacionado;
    }

    public void setUuidCfdiRelacionado(String uuidCfdiRelacionado) {
	this.uuidCfdiRelacionado = uuidCfdiRelacionado;
    }

    public String getSerieComprobante() {
	return serieComprobante;
    }

    public void setSerieComprobante(String serieComprobante) {
	this.serieComprobante = serieComprobante;
    }

    public int getDecimales() {
	return decimales;
    }

    public void setDecimales(int decimales) {
	this.decimales = decimales;
    }

	//public Impuestos getImpuestos() {
	//	return impuestos;
	//}

	//public void setImpuestos(Impuestos impuestos) {
		//this.impuestos = impuestos;
	//}
    
    
}
