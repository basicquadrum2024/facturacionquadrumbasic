package com.mx.quadrum.basic.wrapper;

import java.math.BigDecimal;
import java.util.List;

import com.mx.quadrum.basic.entity.TcContribuyente;
import com.mx.quadrum.basic.entity.TcReceptor;
import com.mx.quadrum.basic.entity.TwCfdi;
import com.mx.quadrum.utileria2021.cfdi.schema.v40.complemento.pagos.v20.Pagos.Pago;

public class WrapperPagosV20 {

	private TwCfdi cfdiPadre;
	private TcReceptor receptor;
    private TcContribuyente contribuyente;
    private Pago pago;
    private BigDecimal faltante;
    private String uuidCfdiRelacionado;
    private String serieComprobante;
    private int decimales;
    private List<Traslado> traslados;
    private List<Retencion> retenciones;
   // private Impuestos impuestos;
    private String objetoImpDR;
	public TwCfdi getCfdiPadre() {
		return cfdiPadre;
	}
	public void setCfdiPadre(TwCfdi cfdiPadre) {
		this.cfdiPadre = cfdiPadre;
	}
	public TcReceptor getReceptor() {
		return receptor;
	}
	public void setReceptor(TcReceptor receptor) {
		this.receptor = receptor;
	}
	public TcContribuyente getContribuyente() {
		return contribuyente;
	}
	public void setContribuyente(TcContribuyente contribuyente) {
		this.contribuyente = contribuyente;
	}
	public Pago getPago() {
		return pago;
	}
	public void setPago(Pago pago) {
		this.pago = pago;
	}
	public BigDecimal getFaltante() {
		return faltante;
	}
	public void setFaltante(BigDecimal faltante) {
		this.faltante = faltante;
	}
	public String getUuidCfdiRelacionado() {
		return uuidCfdiRelacionado;
	}
	public void setUuidCfdiRelacionado(String uuidCfdiRelacionado) {
		this.uuidCfdiRelacionado = uuidCfdiRelacionado;
	}
	public String getSerieComprobante() {
		return serieComprobante;
	}
	public void setSerieComprobante(String serieComprobante) {
		this.serieComprobante = serieComprobante;
	}
	public int getDecimales() {
		return decimales;
	}
	public void setDecimales(int decimales) {
		this.decimales = decimales;
	}
//	public Impuestos getImpuestos() {
//		return impuestos;
//	}
//	public void setImpuestos(Impuestos impuestos) {
//		this.impuestos = impuestos;
//	}
//    
	public List<Traslado> getTraslados() {
		return traslados;
	}
	public void setTraslados(List<Traslado> traslados) {
		this.traslados = traslados;
	}
	public List<Retencion> getRetenciones() {
		return retenciones;
	}
	public void setRetenciones(List<Retencion> retenciones) {
		this.retenciones = retenciones;
	}
	public String getObjetoImpDR() {
		return objetoImpDR;
	}
	public void setObjetoImpDR(String objetoImpDR) {
		this.objetoImpDR = objetoImpDR;
	}
    
    
}
