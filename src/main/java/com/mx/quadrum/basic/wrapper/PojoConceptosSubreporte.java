package com.mx.quadrum.basic.wrapper;
/**
 * Clase para obtener los datos del subreporte
 * @author carlos
 *
 */

import java.math.BigDecimal;


public class PojoConceptosSubreporte {

	private BigDecimal cantidad;
	private String unidadMedida;
	private String descripcion;
	private BigDecimal precioUnitario;
	private BigDecimal importe;
	
	public PojoConceptosSubreporte(BigDecimal cantidad, String unidadMedida, String descripcion, BigDecimal precioUnitario,
			BigDecimal importe) {
		super();
		this.cantidad = cantidad;
		this.unidadMedida = unidadMedida;
		this.descripcion = descripcion;
		this.precioUnitario = precioUnitario;
		this.importe = importe;
	}
	
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	
	
	
}
