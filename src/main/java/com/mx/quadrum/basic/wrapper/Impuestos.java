package com.mx.quadrum.basic.wrapper;



public class Impuestos {
	
	private String tipoFactorTraslado;
	private String tipoFactorRetencion;
	
	public String getTipoFactorTraslado() {
		return tipoFactorTraslado;
	}
	public void setTipoFactorTraslado(String tipoFactorTraslado) {
		this.tipoFactorTraslado = tipoFactorTraslado;
	}
	public String getTipoFactorRetencion() {
		return tipoFactorRetencion;
	}
	public void setTipoFactorRetencion(String tipoFactorRetencion) {
		this.tipoFactorRetencion = tipoFactorRetencion;
	}

	
	

}
