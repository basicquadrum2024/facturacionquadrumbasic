package com.mx.quadrum.basic.wrapper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

/**
 * Clase para controlar el paginado.
 * @author Pedro Romero
 *
 */
public class Paginacion implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
	private Class clase;
	@NotNull
	private Integer page;
	@NotNull
	private Integer pageSize;
	private List<String[]> between;
	private Integer count;
	private Map<String, Object> disyuncion;
	private List<String> conjuncion;
	private List<Object> listBus;
	private List<String> order;
	private List<String> joinn;
	private String error;
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	/**
	 * @return the clase
	 */
	public Class getClase() {
		return clase;
	}
	/**
	 * @param clase the clase to set
	 */
	public void setClase(Class clase) {
		this.clase = clase;
	}
	/**
	 * @return the page
	 */
	public Integer getPage() {
		return page;
	}
	/**
	 * @param page the page to set
	 */
	public void setPage(Integer page) {
		this.page = page;
	}
	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}
	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	/**
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(Integer count) {
		this.count = count;
	}
	/**
	 * @return the disyuncion
	 */
	public Map<String, Object>  getDisyuncion() {
		return disyuncion;
	}
	/**
	 * @param disyuncion the disyuncion to set
	 */
	public void setDisyuncion(Map<String, Object>  disyuncion) {
		this.disyuncion = disyuncion;
	}
	/**
	 * @return the conjuncion
	 */
	public List<String>  getConjuncion() {
		return conjuncion;
	}
	/**
	 * @param conjuncion the conjuncion to set
	 */
	public void setConjuncion(List<String>  conjuncion) {
		this.conjuncion = conjuncion;
	}
	/**
	 * @return the listBus
	 */
	public List<Object> getListBus() {
		return listBus;
	}
	/**
	 * @param listBus the listBus to set
	 */
	public void setListBus(List<Object> listBus) {
		this.listBus = listBus;
	}
	/**
	 * @return the order
	 */
	public List<String> getOrder() {
		return order;
	}
	/**
	 * @param order the order to set
	 */
	public void setOrder(List<String> order) {
		this.order = order;
	}
	
	public List<String[]> getBetween() {
		return between;
	}
	public void setBetween(List<String[]> between) {
		this.between = between;
	}
	/**
	 * @return the joinn
	 */
	public List<String> getJoinn() {
		return joinn;
	}
	/**
	 * @param joinn the joinn to set
	 */
	public void setJoinn(List<String> joinn) {
		this.joinn = joinn;
	}
	
	
	
	
}
