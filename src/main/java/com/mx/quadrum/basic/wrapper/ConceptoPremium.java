package com.mx.quadrum.basic.wrapper;

/**
 * Entidad ConceptoPremium
 * @author quadrum
 *
 */
public class ConceptoPremium {

    private String cantidad;
    private String unidad;
    private String noIdentificacion;
    private String descripcion;
    private String valorUnitario;
    private String importe;
    private String claveProdServ;
    private String claveUnidad;
    private String descuento;

    public String getCantidad() {
	return cantidad;
    }

    public void setCantidad(String cantidad) {
	this.cantidad = cantidad;
    }

    public String getUnidad() {
	return unidad;
    }

    public void setUnidad(String unidad) {
	this.unidad = unidad;
    }

    public String getNoIdentificacion() {
	return noIdentificacion;
    }

    public void setNoIdentificacion(String noIdentificacion) {
	this.noIdentificacion = noIdentificacion;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    public String getValorUnitario() {
	return valorUnitario;
    }

    public void setValorUnitario(String valorUnitario) {
	this.valorUnitario = valorUnitario;
    }

    public String getImporte() {
	return importe;
    }

    public void setImporte(String importe) {
	this.importe = importe;
    }

    public String getClaveUnidad() {
	return claveUnidad;
    }

    public void setClaveUnidad(String claveUnidad) {
	this.claveUnidad = claveUnidad;
    }

    public String getDescuento() {
	return descuento;
    }

    public void setDescuento(String descuento) {
	this.descuento = descuento;
    }

    public String getClaveProdServ() {
	return claveProdServ;
    }

    public void setClaveProdServ(String claveProdServ) {
	this.claveProdServ = claveProdServ;
    }
}