package com.mx.quadrum.basic.wrapper;

import java.util.List;

public class WrapperCancelacion {
	
	private  List<Long> lista;
	private ObjetoCancelacion cancelacion;
	public List<Long> getLista() {
		return lista;
	}
	public void setLista(List<Long> lista) {
		this.lista = lista;
	}
	public ObjetoCancelacion getCancelacion() {
		return cancelacion;
	}
	public void setCancelacion(ObjetoCancelacion cancelacion) {
		this.cancelacion = cancelacion;
	}
	
	
	

}
