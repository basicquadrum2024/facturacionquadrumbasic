@XmlSchema(
		elementFormDefault=XmlNsForm.QUALIFIED,	
    namespace="http://www.sat.gob.mx/cfd/3",
    xmlns={
        @XmlNs(namespaceURI = "http://www.sat.gob.mx/cfd/3", prefix = "cfdi"),	
        @XmlNs(namespaceURI = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi")
    })

package com.mx.quadrum.basic.wrapper;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlNsForm;