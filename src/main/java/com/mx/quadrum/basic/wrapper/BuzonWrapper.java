package com.mx.quadrum.basic.wrapper;

import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mx.quadrum.basic.entity.TwUsuario;
/**
 * Entidad para envio de correo
 * @author quadrum
 *
 */
public class BuzonWrapper {

	private BigInteger id;
	private BigInteger estatus;
	private BigInteger tipo;
	private String asunto;
	private String mensaje;
	private Date fecha;
	private String telefono;
	private String correo;
	private String rfcUsuario;
	private String nombreUsuario;
	private String usuarioApPaterno;
	private String usuarioApMaterno;
	private BigInteger idUsuario;
	
	
	
	
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public BigInteger getEstatus() {
		return estatus;
	}
	public void setEstatus(BigInteger estatus) {
		this.estatus = estatus;
	}
	public BigInteger getTipo() {
		return tipo;
	}
	public void setTipo(BigInteger tipo) {
		this.tipo = tipo;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh-mm-ss", locale = "es_MX", timezone = "America/Mexico_City")
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getRfcUsuario() {
		return rfcUsuario;
	}
	public void setRfcUsuario(String rfcUsuario) {
		this.rfcUsuario = rfcUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getUsuarioApPaterno() {
		return usuarioApPaterno;
	}
	public void setUsuarioApPaterno(String usuarioApPaterno) {
		this.usuarioApPaterno = usuarioApPaterno;
	}
	public String getUsuarioApMaterno() {
		return usuarioApMaterno;
	}
	public void setUsuarioApMaterno(String usuarioApMaterno) {
		this.usuarioApMaterno = usuarioApMaterno;
	}
	public BigInteger getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(BigInteger idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
	

}
