package com.mx.quadrum.basic.wrapper;

import com.mx.quadrum.cfdi.v33.schema.Comprobante;

/**
 * Clase de la entidad ComprobanteWrapper que extiende de comprobante
 * @author Quadrum
 *
 */

public class ComprobanteWrapper extends Comprobante {

	private ReceptorCfdiv33Wrapper receptorCfdiv33Wrapper;

	public ReceptorCfdiv33Wrapper getReceptorCfdiv33Wrapper() {
		return receptorCfdiv33Wrapper;
	}

	public void setReceptorCfdiv33Wrapper(ReceptorCfdiv33Wrapper receptorCfdiv33Wrapper) {
		this.receptorCfdiv33Wrapper = receptorCfdiv33Wrapper;
	}

	

}
