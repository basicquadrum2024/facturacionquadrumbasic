package com.mx.quadrum.basic.wrapper;

import java.util.List;

public class ObjetoCancelacion {
	
	private  String motivoCancelacion;
	private  String uuidSustituto;
	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}
	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}
	public String getUuidSustituto() {
		return uuidSustituto;
	}
	public void setUuidSustituto(String uuidSustituto) {
		this.uuidSustituto = uuidSustituto;
	}
	
	

}
