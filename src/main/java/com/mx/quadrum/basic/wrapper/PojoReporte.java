package com.mx.quadrum.basic.wrapper;
/**
 * Clase para crear el reporte
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class PojoReporte {
	private Map<String, Object> datos = new HashMap<String, Object>();
	private String urlReporte;
	//private List<Cfdi_Comprobante> listFacuras = new ArrayList<Cfdi_Comprobante>();

	public void setDatos(Map<String, Object> datos) {
		this.datos = datos;
	}

	public Map<String, Object> getDatos() {
		return datos;
	}

	public void setUrlReporte(String urlReporte) {
		this.urlReporte = urlReporte;
	}

	public String getUrlReporte() {
		return urlReporte;
	}

//	public List<Cfdi_Comprobante> getListFacuras() {
//		return listFacuras;
//	}
//
//	public void setListFacuras(List<Cfdi_Comprobante> listFacuras) {
//		this.listFacuras = listFacuras;
//	}
//
//	public void addFactura(Cfdi_Comprobante FC) {
//		this.listFacuras.add(FC);
//	}

}
