package com.mx.quadrum.basic.wrapper;

import com.mx.quadrum.utileria2021.cfdi.schema.v40.Comprobante;

public class ComprobanteWrapperV40 extends Comprobante {
	
	ReceptorWrapperCfdiV40 receptorCfdiv33Wrapper;

	public ReceptorWrapperCfdiV40 getReceptorCfdiv33Wrapper() {
		return receptorCfdiv33Wrapper;
	}

	public void setReceptorCfdiv33Wrapper(ReceptorWrapperCfdiV40 receptorCfdiv33Wrapper) {
		this.receptorCfdiv33Wrapper = receptorCfdiv33Wrapper;
	}

	
	
	

}
