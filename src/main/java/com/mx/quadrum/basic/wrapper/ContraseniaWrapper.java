package com.mx.quadrum.basic.wrapper;

/**
 * Clase de la entidad ContraseniaWrapper que sirve para el registro de contrasenia
 * @author Quadrum
 *
 */
public class ContraseniaWrapper {
	
	private String nuevaContrasenia;
	private String contraseniaAnterior;
	private String primeraSession;
	
	public String getNuevaContrasenia() {
		return nuevaContrasenia;
	}
	public void setNuevaContrasenia(String nuevaContrasenia) {
		this.nuevaContrasenia = nuevaContrasenia;
	}
	public String getPrimeraSession() {
		return primeraSession;
	}
	public void setPrimeraSession(String primeraSession) {
		this.primeraSession = primeraSession;
	}
	public String getContraseniaAnterior() {
		return contraseniaAnterior;
	}
	public void setContraseniaAnterior(String contraseniaAnterior) {
		this.contraseniaAnterior = contraseniaAnterior;
	}
	
	

}
