package com.mx.quadrum.basic.wrapper;

import java.io.Serializable;
import java.util.Date;
/**
 * Clase para obtener los datos del cfdi timbrado
 * @author Quadrum
 *
 */
public class ResultadoTimbradoWrapper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Boolean timbrado = false;
    private String uuid;
    private String serie;
    private String folio;
    private Date fecha;
    private String rfcEmisor;
    private String lugarFile;
    private String opcion;
    private Long id;
    private Integer error;
    private String errores;

    public String getUuid() {
	return uuid;
    }

    public void setUuid(String uuid) {
	this.uuid = uuid;
    }

    public String getSerie() {
	return serie;
    }

    public void setSerie(String serie) {
	this.serie = serie;
    }

    public String getFolio() {
	return folio;
    }

    public void setFolio(String folio) {
	this.folio = folio;
    }

    public String getLugarFile() {
	return lugarFile;
    }

    public void setLugarFile(String lugarFile) {
	this.lugarFile = lugarFile;
    }

    public String getOpcion() {
	return opcion;
    }

    public void setOpcion(String opcion) {
	this.opcion = opcion;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Integer getError() {
	return error;
    }

    public void setError(Integer error) {
	this.error = error;
    }

    public Date getFecha() {
	return fecha;
    }

    public void setFecha(Date date) {
	this.fecha = date;
    }

    public String getRfcEmisor() {
	return rfcEmisor;
    }

    public void setRfcEmisor(String rfcEmisor) {
	this.rfcEmisor = rfcEmisor;
    }

    public String getErrores() {
	return errores;
    }

    public void setErrores(String errores) {
	this.errores = errores;
    }

    public Boolean getTimbrado() {
	return timbrado;
    }

    public void setTimbrado(Boolean timbrado) {
	this.timbrado = timbrado;
    }

}