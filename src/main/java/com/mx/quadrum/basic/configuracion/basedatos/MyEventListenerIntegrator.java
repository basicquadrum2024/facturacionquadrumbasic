package com.mx.quadrum.basic.configuracion.basedatos;

import javax.annotation.PostConstruct;

import org.hibernate.SessionFactory;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.quadrum.basic.aes.AesServices;

@Component
public class MyEventListenerIntegrator {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private AesServices aesServices;

	@PostConstruct
	public void registerListeners() {
		SessionFactoryImpl sfi = (SessionFactoryImpl) sessionFactory;
		EventListenerRegistry registry = sfi.getServiceRegistry().getService(EventListenerRegistry.class);
		registry.getEventListenerGroup(EventType.POST_LOAD).appendListener(new CargaEntidadesListener(aesServices));
	}
}
