package com.mx.quadrum.basic.configuracion.seguridad;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
/**
 * Clase escuchador al final de la sesi�n
 * @author Manu
 *
 */
@Component
public class SessionEndedListener implements ApplicationListener<SessionDestroyedEvent> {

    private static final Logger LOGGER = Logger.getLogger(SessionEndedListener.class);

    @Autowired
    private PistasServices pistasServices;

    @Override
    public void onApplicationEvent(SessionDestroyedEvent event) {
	for (SecurityContext securityContext : event.getSecurityContexts()) {
	    Authentication authentication = securityContext.getAuthentication();
	    if (null != authentication) {
		TwUsuario twUsuario = Utilerias.getUserSession(authentication);
		WebAuthenticationDetails authenticationDetails = (WebAuthenticationDetails) authentication.getDetails();
		try {
		    Utilerias.eventosAdministrador("USERS",
			    "El usuario ha salido del sistema por inactividad".toUpperCase(), twUsuario, "SELECT",
			    pistasServices, null);
		} catch (Exception e) {
		    LOGGER.error("Error en metodo SessionEndedListener-onApplicationEvent:"
			    + UtileriasCfdi.imprimeStackError(e));
		}
	    }

	}
    }

}