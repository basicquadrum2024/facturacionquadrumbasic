package com.mx.quadrum.basic.configuracion.seguridad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.impl.ServicioTwUsuarioImpl2024;
import com.mx.quadrum.basic.util.Utilerias;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

	private static final Logger LOGGER = Logger.getLogger(CustomAuthenticationFailureHandler.class);

	private PistasServices pistasServices;

	private ServicioTwUsuarioImpl2024 dao;

	private AesServices aesServices;

	public CustomAuthenticationFailureHandler(PistasServices pistasServices, final ServicioTwUsuarioImpl2024 dao,
			final AesServices aesServices) {
		this.pistasServices = pistasServices;
		this.dao = dao;
		this.aesServices = aesServices;
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		LOGGER.error("onAuthenticationFailure: " + UtileriasCfdi.imprimeStackError(exception));
		bitacora(request);
		request.getSession().setAttribute("msjAuthError", exception.getMessage());
		response.sendRedirect(request.getContextPath() + "/index.jsp");
	}

	private void bitacora(HttpServletRequest request) {
		try {
			TwUsuario userEntity = null;
			userEntity = dao.buscarPorRfcE(aesServices.encriptaCadena(request.getParameter("j_username")));
			if (null != userEntity)
				Utilerias.eventosAdministrador("USERS", "INTENTO FALLIDO PARA ACCESO AL SISTEMA", userEntity, "SELECT",
						pistasServices, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}