
package com.mx.quadrum.basic.configuracion.basedatos;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.entity.Foliador;

/**
 * Clase interceptor entidades, contine metodos para administrar la encriptación de objetos
 * @author Manu
 *
 */
public class InterceptorEntidades extends EmptyInterceptor {

   
    private static final long serialVersionUID = 4930009145917823156L;

    @PostConstruct
    public void inicio() {
	SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Autowired
    private AesServices aesServices;

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
	    throws CallbackException {
	validaEncriptaString(entity, state, propertyNames, types, true);
	return true;
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
	    String[] propertyNames, Type[] types) throws CallbackException {
	validaEncriptaString(entity, currentState, propertyNames, types, true);
	return true;
    }

    // @Override
    // public boolean onLoad(Object entity, Serializable id, Object[] state,
    // String[] propertyNames, Type[] types) {
    // System.out.println("Ejecuta onLoad
    // entity"+ToStringBuilder.reflectionToString(entity));
    // //validaEncriptaString(entity, state, propertyNames, types, false);
    // return true;
    // }

    /**
     * Metodo encargado de encriptar o desencriptar, la delimitacion es atraves
     * de la variable opcion
     * <ul>
     * <li>Si el valor de opcion es true entonces se encripta el atributo tipo
     * String</li>
     * <li>Si el valor de opcion es false entonces se desencripta el atributo
     * tipo String</li>
     * </ul>
     * 
     * @param entity
     * @param currentState
     * @param propertyNames
     * @param types
     * @param opcion
     */
    private void validaEncriptaString(Object entity, Object[] currentState, String[] propertyNames, Type[] types,
	    boolean opcion) {
	for (int i = 0; i < currentState.length; i++) {
	    Object objeto = (Object) currentState[i];
	    if (entity.getClass().isAssignableFrom(Foliador.class)) {
		if (!propertyNames[i].equals("serie") && objeto instanceof String) {
		    currentState[i] = aesServices.encriptaCadena((String) objeto);
		}
	    } else {
		if (objeto != null) {
		    if (objeto instanceof String && !propertyNames[i].equals("algoritmo")) {
			if (opcion) {
			    currentState[i] = aesServices.encriptaCadena((String) objeto);
			} else {
			    currentState[i] = aesServices.desEncriptaCadena((String) objeto);
			}
		    }
		}
	    }

	}
    }

}
