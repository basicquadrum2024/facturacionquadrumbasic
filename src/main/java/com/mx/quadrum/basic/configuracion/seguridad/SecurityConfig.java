package com.mx.quadrum.basic.configuracion.seguridad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import com.mx.quadrum.basic.aes.AesServices;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.services.impl.ServicioTwUsuarioImpl2024;

/**
 * Clase que contiene la configuración de seguridad
 * 
 * @author Manu
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private PistasServices pistasServices;

	@Autowired
	private ServicioTwUsuarioImpl2024 dao;

	@Autowired
	private AesServices aesServices;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/*.html")
				.access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGUER')")
				.antMatchers("/j_spring_security_check", "/index.jsp", "/view/login/login.jsp", "/*.fue")
				.access("hasRole('ROLE_ANONYMOUS')").antMatchers("/manuales/**").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/app/usuariosApp.fue").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/app/usuariosCountApp.fue").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/view/login/**").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/usuarioController/recuperarContrasenia.fue").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/usuarioController/registro.fue").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/contactoController/existeContactoRFC.fue").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/usuarioController/varificacion.fue").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/manualesController/manualUsuario.fue").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/emisorController/validarContribuyenteLco.json").access("hasRole('ROLE_ANONYMOUS')")
				.antMatchers("/usuarioController/terminos.fue").access("hasRole('ROLE_ANONYMOUS')")

				.anyRequest().authenticated().antMatchers("/view/home/**")
				.access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGUER')")
				.antMatchers("/view/controller/**")
				.access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGUER')").and().formLogin()
				.loginProcessingUrl("/j_spring_security_check").failureHandler(authenticationFailureHandler())
				.successHandler(authenticationSuccessHandler()).usernameParameter("j_username")
				.passwordParameter("j_password").and().logout().logoutUrl("/j_spring_security_logout")
				.logoutSuccessUrl("/index.jsp").deleteCookies("JSESSIONID").invalidateHttpSession(true).and()
				.exceptionHandling().accessDeniedHandler(accessDeniedHandler()).and().csrf().disable();
		http.sessionManagement().maximumSessions(1).sessionRegistry(sessionRegistry()).expiredUrl("/index.jsp").and()
				.sessionFixation().migrateSession();

	}

	@Bean
	public AuthenticationFailureHandler authenticationFailureHandler() {
		return new CustomAuthenticationFailureHandler(pistasServices, dao, aesServices);
	}

	@Bean
	public AuthenticationSuccessHandler authenticationSuccessHandler() {
		return new CustomAuthenticationSuccessHandler(pistasServices);
	}

	@Bean
	public AccessDeniedHandler accessDeniedHandler() {
		return new CustomAccessDeniedHandler();
	}

	@Bean
	public SessionRegistry sessionRegistry() {
		SessionRegistry sessionRegistry = new SessionRegistryImpl();
		return sessionRegistry;
	}

	@Bean
	public ServletListenerRegistrationBean httpSessionEventPublisher() {
		return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
	}

}
