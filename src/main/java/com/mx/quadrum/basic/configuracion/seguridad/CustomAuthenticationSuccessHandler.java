package com.mx.quadrum.basic.configuracion.seguridad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.mx.quadrum.basic.entity.TwUsuario;
import com.mx.quadrum.basic.services.PistasServices;
import com.mx.quadrum.basic.util.Utilerias;

public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private static final Logger LOGGER = Logger.getLogger(CustomAuthenticationSuccessHandler.class);

	private PistasServices pistasServices;


	public CustomAuthenticationSuccessHandler(PistasServices pistasServices) {
		this.pistasServices = pistasServices;

	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		LOGGER.info("onAuthenticationSuccess");
		bitacora(authentication);
		request.getSession().setAttribute("msjAuthError", "");
		response.sendRedirect(request.getContextPath() + "/login.html?success#/index.jsp");
	}

	private void bitacora(Authentication authentication) {
		try {
			TwUsuario usuario = Utilerias.getUserSession(authentication);
			Utilerias.eventosAdministrador("USERS", "ACCESO CORRECTO AL SISTEMA", usuario, "SELECT",
					pistasServices, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
