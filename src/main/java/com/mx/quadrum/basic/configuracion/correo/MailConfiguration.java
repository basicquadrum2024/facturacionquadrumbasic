package com.mx.quadrum.basic.configuracion.correo;

import java.util.Properties;

import javax.mail.Session;
import javax.naming.NamingException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * Clase para configuración de correo
 * @author 
 *
 */
@Configuration
public class MailConfiguration {

    @Bean
    public JavaMailSender javaMailService() {
//	JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//	mailSender.setSession(getMailSession());
    	
    
    	JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    	mailSender.setDefaultEncoding("UTF-8");
		String protocolo = "smtps";
		mailSender.setProtocol(protocolo);
		String host = "smtp.gmail.com";
		mailSender.setHost(host);
		String puertoString ="465";

		mailSender.setPort(Integer.parseInt(puertoString));
		mailSender.setUsername("sistemanotificacionp@gmail.com");
		mailSender.setPassword("waodkfivawkbelzl");

		Properties mailProperties = new Properties();
		mailProperties.put("mail.debug", "true");
		mailProperties.put("mail.smtp.port", puertoString);
		mailProperties.put("mail.smtp.host", host);
		mailProperties.put("mail.smtp.starttls.enable", "true");
		mailProperties.put("mail.smtp.debug", "true");
		mailProperties.put("mail.smtp.auth", "true");
		mailProperties.put("mail.smtp.socketFactory.port", host);
		mailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		mailProperties.put("mail.smtp.ssl.enable", "true");
		mailProperties.put("mail.smtp.starttls.enable", "true");
		mailProperties.put("mail.smtp.socketFactory.fallback", "false");
		mailSender.setJavaMailProperties(mailProperties);
    	
	return mailSender;
    }

    public Session getMailSession() {
	JndiTemplate template = new JndiTemplate();
	Session session = null;
	try {
	    session = (Session) template.lookup("java:jboss/mail/SessionEmailBasic");
	} catch (NamingException e) {
	    e.printStackTrace();
	}
	return session;
    }

}
