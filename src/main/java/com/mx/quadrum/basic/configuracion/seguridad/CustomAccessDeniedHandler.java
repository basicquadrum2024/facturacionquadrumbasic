package com.mx.quadrum.basic.configuracion.seguridad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {

	private static final Logger LOGGER = Logger.getLogger(CustomAccessDeniedHandler.class);

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		LOGGER.error(String.format("CustomAccessDeniedHandler handle:%s",
				UtileriasCfdi.imprimeStackError(accessDeniedException)));
		response.sendRedirect(request.getContextPath() + "/index.jsp");
	}

}
