package com.mx.quadrum.basic.configuracion.basedatos;

import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Clase que contiene la configuración para acceso a la base de datos patronesSAT
 * @author Manu
 *
 */
@Configuration
@EnableTransactionManagement
public class HibernateCfgPatrones {

    public final static Logger LOGGER = Logger.getLogger(HibernateCfgPatrones.class);

    @Bean(name = "sessionFactoryPatrones")
    public LocalSessionFactoryBean sessionFactory() {
	LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	sessionFactory.setDataSource(dataSource());
	sessionFactory.setPackagesToScan(new String[] { "com.mx.quadrum.basic.entity.patronesSAT" });
	sessionFactory.setHibernateProperties(hibernateProperties());
	return sessionFactory;
    }

    @Bean(name = "dataSorucePatrones")
    public DataSource dataSource() {
	JndiObjectFactoryBean dataSource = new JndiObjectFactoryBean();
	dataSource.setJndiName("java:jboss/jdbc/Patrones");
	dataSource.setResourceRef(true);
	try {
	    dataSource.afterPropertiesSet();
	} catch (IllegalArgumentException | NamingException e) {
	    e.printStackTrace();
	}

	return (DataSource) dataSource.getObject();
    }

    private Properties hibernateProperties() {
	Properties properties = new Properties();
	properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
	properties.put("hibernate.show_sql", "false");
	properties.put("hibernate.format_sql", "false");
	return properties;
    }

    @Bean(name = "hibernateTransactionManagerPatrones")
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
	HibernateTransactionManager txManager = new HibernateTransactionManager();
	txManager.setSessionFactory(sessionFactory);
	return txManager;
    }

}
