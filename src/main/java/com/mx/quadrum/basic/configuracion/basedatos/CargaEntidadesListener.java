package com.mx.quadrum.basic.configuracion.basedatos;

import java.lang.reflect.Field;

import org.hibernate.event.spi.PostLoadEvent;
import org.hibernate.event.spi.PostLoadEventListener;

import com.mx.quadrum.basic.aes.AesServices;

/**
 * Clase contiene objeto para desencriptar objetos 
 * @author 
 *
 */
public class CargaEntidadesListener implements PostLoadEventListener {

    /**
     * 
     */
    private static final long serialVersionUID = -2426218957579575604L;

    public CargaEntidadesListener(AesServices aesServices) {
	this.aesServices = aesServices;
    }

    private AesServices aesServices;

    @Override
    public void onPostLoad(PostLoadEvent event) {
	desEncriptaObjeto(event.getEntity());
    }

    /**
     * M�todo para desencriptar un objeto
     * @param objeto
     */
    private void desEncriptaObjeto(Object objeto) {
	try {
	    if (objeto != null) {
		Class<?> classObject = objeto.getClass();
		Field[] objectFields = classObject.getDeclaredFields();
		for (Field field : objectFields) {
		    field.setAccessible(true);
		    if (field.getType().toString().contains("java.lang.String")) {
			if (field.get(objeto) != null) {
			    String value = (String) field.get(objeto);
			    field.set(objeto, aesServices.desEncriptaCadena(value));
			}
		    }
		}
	    }

	} catch (Exception e) {

	}
    }
}
