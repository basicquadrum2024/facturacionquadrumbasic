package com.mx.quadrum.basic.configuracion.basedatos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mx.quadrum.basic.entity.Parametros;
//import com.mx.quadrum.basic.util.ParametroApp;
import com.mx.quadrum.basic.util.ParametroApp;
import com.mx.quadrum.basic.util.Utilerias;

/**
 * Clase que contien la configuración para el acceso a la base de datos facturacionbasica
 * @author 
 *
 */
@Configuration
@EnableTransactionManagement
@PropertySource(value = { "classpath:application.properties" })
public class HibernateCfg {

    @Autowired
    private Environment environment;

    final static Logger logger = Logger.getLogger(HibernateCfg.class);

    @Bean
    public EmptyInterceptor hibernateInterceptor() {
	return new InterceptorEntidades();
    }

    @Bean
    @Primary
    public LocalSessionFactoryBean sessionFactory() {
	LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	sessionFactory.setDataSource(dataSource());
	sessionFactory.setPackagesToScan(
		new String[] { "com.mx.quadrum.basic.entity"});
	sessionFactory.setHibernateProperties(hibernateProperties());
	sessionFactory.setEntityInterceptor(hibernateInterceptor());
	return sessionFactory;
    }

    @Bean
    @Primary
    public DataSource dataSource() {
	JndiObjectFactoryBean dataSource = new JndiObjectFactoryBean();
	dataSource.setJndiName("java:jboss/jdbc/Basic");
	dataSource.setResourceRef(true);
	try {
	    dataSource.afterPropertiesSet();
	} catch (IllegalArgumentException | NamingException e) {
	    e.printStackTrace();
	}

	return (DataSource) dataSource.getObject();
    }

    private Properties hibernateProperties() {
	Properties properties = new Properties();
	properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
	properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
	properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
	return properties;
    }

    @Bean
    @Primary
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
	HibernateTransactionManager txManager = new HibernateTransactionManager();
	txManager.setSessionFactory(sessionFactory);
	parametrosIni(sessionFactory);
	return txManager;
    }

    private void parametrosIni(SessionFactory sessionFactory) {
	Map<String, String> map = new HashMap<String, String>();
	Session session = null;
	try {
	    session = sessionFactory.openSession();
	    session.beginTransaction();
	    List<Parametros> lista = session.createCriteria(Parametros.class)
		    .add(Restrictions.ilike("nombre", Utilerias.CONTEXTO_BASICA, MatchMode.START)).list();
	    for (Parametros parametros : lista) {
		map.put(parametros.getNombre(), parametros.getValor());
	    }

	    session.flush();

	} catch (Exception e) {
	    e.printStackTrace();
	    if (session != null && session.isConnected()) {
		session.getTransaction().rollback();
	    }
	} finally {
	    if (session != null && session.isConnected()) {
		session.close();
	    }
	}

	ParametroApp pam = new ParametroApp(map);
	pam.setEmpresa(Utilerias.CONTEXTO_BASICA);
    }

}
