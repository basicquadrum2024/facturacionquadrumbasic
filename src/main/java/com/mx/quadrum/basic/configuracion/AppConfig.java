package com.mx.quadrum.basic.configuracion;

import java.io.IOException;
import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
//import com.mx.quadrum.basic.configuracion.correo.MailConfiguration;
import com.mx.quadrum.basic.configuracion.seguridad.SecurityConfig;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "com.mx.quadrum.basic.configuracion", "com.mx.quadrum.basic.configuracion.basedatos",
	"com.mx.quadrum.basic.configuracion.seguridad", "com.mx.quadrum.basic.configuracion.correo",
	"com.mx.quadrum.basic.dao", "com.mx.quadrum.basic.services", "com.mx.quadrum.basic.aes",
	"com.mx.quadrum.basic.controller", "com.mx.quadrum.basic.services.catalogos" })
@Import({ SecurityConfig.class })
public class AppConfig extends WebMvcConfigurerAdapter {

    @Bean
    public ViewResolver viewResolver() {
	InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	viewResolver.setViewClass(JstlView.class);
	viewResolver.setPrefix("/");
	viewResolver.setSuffix(".jsp");
	return viewResolver;
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
	configurer.favorPathExtension(true).ignoreAcceptHeader(true).useJaf(false)
		.defaultContentType(MediaType.TEXT_HTML).mediaType("html", MediaType.TEXT_HTML)
		.mediaType("xml", MediaType.APPLICATION_XML).mediaType("fue", MediaType.APPLICATION_JSON)
		.mediaType("json", MediaType.APPLICATION_JSON);
    }

    @Bean
    public MessageSource messageSource() {
	ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	messageSource.setBasename("messages");
	return messageSource;
    }

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }

    @Bean
    public ObjectMapper objectMapper() {
	ObjectMapper objectMapper = new ObjectMapper();
	Hibernate4Module hibernate4Module = new Hibernate4Module();
	hibernate4Module.disable(Hibernate4Module.Feature.USE_TRANSIENT_ANNOTATION);
	objectMapper.registerModule(hibernate4Module);
	return objectMapper;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
	MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
	converter.setObjectMapper(objectMapper());
	converters.add(converter);
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver getResolver() throws IOException {
	CommonsMultipartResolver resolver = new CommonsMultipartResolver();
	// Set the maximum allowed size (in bytes) for each individual file.
	resolver.setMaxUploadSize(5242880);// 5MB
	// You may also set other available properties.
	return resolver;
    }

}
