package mx.com.cfdiquadrum.ws.cancelar;

public class ApplicationProxy implements mx.com.cfdiquadrum.ws.cancelar.Application {
  private String _endpoint = null;
  private mx.com.cfdiquadrum.ws.cancelar.Application application = null;
  
  public ApplicationProxy() {
    _initApplicationProxy();
  }
  
  public ApplicationProxy(String endpoint) {
    _endpoint = endpoint;
    _initApplicationProxy();
  }
  
  private void _initApplicationProxy() {
    try {
      application = (new mx.com.cfdiquadrum.ws.cancelar.CancelarSOAPLocator()).getApplication();
      if (application != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)application)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)application)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (application != null)
      ((javax.xml.rpc.Stub)application)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public mx.com.cfdiquadrum.ws.cancelar.Application getApplication() {
    if (application == null)
      _initApplicationProxy();
    return application;
  }
  
  public views.core.soap.services.apps.Cancela cancelar_firma(byte[] xml, java.lang.String usuario, java.lang.String contrasena, java.lang.Boolean reintentar) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.cancelar_firma(xml, usuario, contrasena, reintentar);
  }
  
  public views.core.soap.services.apps.Recibo acuse(java.lang.String usuario, java.lang.String contrasena, java.lang.String rfc, java.lang.String uuid, java.lang.String tipo) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.acuse(usuario, contrasena, rfc, uuid, tipo);
  }
  
  public views.core.soap.services.apps.Cancela cancelar_externa(byte[] xml, java.lang.String usuario, java.lang.String contrasena, java.lang.String rfc, byte[] certificado, byte[] llave, java.lang.Boolean almacena) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.cancelar_externa(xml, usuario, contrasena, rfc, certificado, llave, almacena);
  }
  
  public views.core.soap.services.apps.AcusePendientes obtiene_cancelacionespendientes(java.lang.String username, java.lang.String password, java.lang.String rtaxpayer_id) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.obtiene_cancelacionespendientes(username, password, rtaxpayer_id);
  }
  
  public views.core.soap.services.apps.Cancela cancelar(views.core.soap.services.apps.UUID[] UUIDS, java.lang.String usuario, java.lang.String contrasena, java.lang.String rfc, byte[] cer, byte[] key, java.lang.Boolean reintentar) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.cancelar(UUIDS, usuario, contrasena, rfc, cer, key, reintentar);
  }
  
  public views.core.soap.services.apps.AcuseRelacionados obtiene_relaciones(java.lang.String username, java.lang.String password, java.lang.String taxpayer_id, java.lang.String rtaxpayer_id, java.lang.String uuid, byte[] cer, byte[] key) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.obtiene_relaciones(username, password, taxpayer_id, rtaxpayer_id, uuid, cer, key);
  }
  
  public views.core.soap.services.apps.AcuseEstatusSat obtiene_status_sat(java.lang.String usario, java.lang.String password, java.lang.String taxpayer_id, java.lang.String rtaxpayer_id, java.lang.String uuid, java.lang.Float total) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.obtiene_status_sat(usario, password, taxpayer_id, rtaxpayer_id, uuid, total);
  }
  
  public views.core.soap.services.apps.AcuseAceptaRechaza aceptacion_rechazo(views.core.soap.services.apps.UUIDS_AR UUIDS_AR, java.lang.String username, java.lang.String password, java.lang.String rtaxpayer_id, byte[] cer, byte[] key) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.aceptacion_rechazo(UUIDS_AR, username, password, rtaxpayer_id, cer, key);
  }
  
  public views.core.soap.services.apps.ConsultaPendiente consulta(java.lang.String usuario, java.lang.String contrasena, java.lang.String uuid) throws java.rmi.RemoteException{
    if (application == null)
      _initApplicationProxy();
    return application.consulta(usuario, contrasena, uuid);
  }
  
  
}