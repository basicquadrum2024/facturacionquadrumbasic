/**
 * Application.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.cfdiquadrum.ws.cancelar;

public interface Application extends java.rmi.Remote {
    public views.core.soap.services.apps.Cancela cancelar_firma(byte[] xml, java.lang.String usuario, java.lang.String contrasena, java.lang.Boolean reintentar) throws java.rmi.RemoteException;
    public views.core.soap.services.apps.Recibo acuse(java.lang.String usuario, java.lang.String contrasena, java.lang.String rfc, java.lang.String uuid, java.lang.String tipo) throws java.rmi.RemoteException;
    public views.core.soap.services.apps.Cancela cancelar_externa(byte[] xml, java.lang.String usuario, java.lang.String contrasena, java.lang.String rfc, byte[] certificado, byte[] llave, java.lang.Boolean almacena) throws java.rmi.RemoteException;

    /**
     * This method returns the sat receipt (response) of the cancellation
     * webservice
     */
    public views.core.soap.services.apps.AcusePendientes obtiene_cancelacionespendientes(java.lang.String username, java.lang.String password, java.lang.String rtaxpayer_id) throws java.rmi.RemoteException;
    public views.core.soap.services.apps.Cancela cancelar(views.core.soap.services.apps.UUID[] UUIDS, java.lang.String usuario, java.lang.String contrasena, java.lang.String rfc, byte[] cer, byte[] key, java.lang.Boolean reintentar) throws java.rmi.RemoteException;

    /**
     * This method returns the sat receipt (response) of the cancellation
     * webservice
     */
    public views.core.soap.services.apps.AcuseRelacionados obtiene_relaciones(java.lang.String username, java.lang.String password, java.lang.String taxpayer_id, java.lang.String rtaxpayer_id, java.lang.String uuid, byte[] cer, byte[] key) throws java.rmi.RemoteException;

    /**
     * This method returns the sat receipt (response) of the cancellation
     * webservice
     */
    public views.core.soap.services.apps.AcuseEstatusSat obtiene_status_sat(java.lang.String usario, java.lang.String password, java.lang.String taxpayer_id, java.lang.String rtaxpayer_id, java.lang.String uuid, java.lang.Float total) throws java.rmi.RemoteException;
    public views.core.soap.services.apps.AcuseAceptaRechaza aceptacion_rechazo(views.core.soap.services.apps.UUIDS_AR UUIDS_AR, java.lang.String username, java.lang.String password, java.lang.String rtaxpayer_id, byte[] cer, byte[] key) throws java.rmi.RemoteException;
    public views.core.soap.services.apps.ConsultaPendiente consulta(java.lang.String usuario, java.lang.String contrasena, java.lang.String uuid) throws java.rmi.RemoteException;
}
