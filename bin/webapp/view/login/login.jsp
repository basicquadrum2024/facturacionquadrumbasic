<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="es-mx" ng-app="QuadrumBasic.login">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="resources/img/favicon.ico">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <title>Facturacion Basic</title>
      <script src="resources/js/jquery/jquery.min.js"></script>
      <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->	
      <script src="resources/js/bootstrap/js/bootstrap.min.js"></script>
      <script src="resources/js/angular-1.3.2/angular.min.js"></script>
      <script src="resources/js/angular-1.3.2/angular-route.js"></script>
      <script src="resources/js/angular-1.3.2/angular-modal-service.js"></script>
      <script src="resources/js/angular-1.3.2/angular-animate.min.js"></script>
      <script src="resources/js/angular-1.3.2/angular-flash.js"></script>
      <script src="resources/js/login.js"></script>
      <script src="resources/js/Util.js"></script>
      <script src="resources/js/reCaptcha/angular-recaptcha.min.js"></script>
      <script src="resources/js/filtro/filtros.js"></script>
      <%@ include file="../home/direccionesCss.jsp"%>
      
      <script src="resources/js/estilosLogin.js"></script>
   
      <script type="text/javascript">
       function descarga(archivo) 
       {
       	document.location = archivo;
       }

       $(document).ready(function() {
           
       	popupLoading = $('#loading_popup').modal({
       		backdrop : 'static',
       		'show' : false
       	});
       });
         
      </script>
   </head>
   <body>
      <!-- <div id="login-button">
         <img class="login-logo" src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png" alt=""> 
      </div> -->
      <div id="barra">
         <div flash-message="7000"></div>
      </div>
      <div id="loading_popup" class="modal fade" role="dialog">
         <div class="barraload">
            Procesando ... <br> <img src="resources/img/loading.gif" />
         </div>
      </div>
      <div flash-message="15000">
      </div>
      <br>
      <br>
      <br>
      <br>
      <div class="container-fluid">
         <div class="row">
          <div class="col-sm-2">
	          <a href="http://omawww.sat.gob.mx/tramitesyservicios/Paginas/pac_quadrum.htm" target="_blank">
		          <img alt="" src="resources/img/PAcSAT.png" class="img2">
	         </a>
	      </div>
 	     </div>
      </div>
      
      <!-- MAIN CONTENT -->
      <div class="container" id="container-login">
        <!--  <h4 style="color:#fff;text-align: center; ">Centro de Validaci&oacute;n Digital CVDSA S.A de C.V. Calle
            Montecito 38 Piso 25 22 Napoles DF, CP 03810, TEL 90000927
         </h4> -->
         <!--
            <div class="text-center">
              		<img alt="Gob.mx" src="resources/img/logobasics.png"
                 		style="width: 200 px; height: 100px;"> 
               	<br/>
            </div>
            -->
         
         <div class="row" id="login-form" >
            <div class="col-md-6 col-md-offset-3">
               <c:if
                  test="${not empty sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}">
                  <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <strong>Su acceso fallo !!</strong> <br> Raz&oacute;n :
                     ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
                  </div>
               </c:if>
               <div class="row">
                 <!--  <div class="col-sm-5  col-lg-8S">
                     <label class="title-login">INICIAR SESI&Oacute;N</label>
                  </div> -->
               </div>
               <div class="capa-formulario">
                  <section class="login" >
                     <h1>INICIAR</h1>
                     <form 	class="login" 
                        role="form" 
                        id="Login" 
                        ng-submit="submit()" 
                        action="<c:url value='j_spring_security_check' />" 
                        method="post">
                        <p class="inputWithIcon">	
                           <label class="login-label" for="">RFC del emisor</label>
                           <input class="login-text" id="j_username"
                              data-toggle="tooltip" title="Ingresa el Rfc del Emisor" maxlength="13"
                              data-placement="right" name="j_username" type="text" ng-model="rfcEmisors"
                              placeholder="Escribe tu RFC.">
                           <i class="fa far fa-user fa-lg fa-fw" aria-hidden="true" style="color: white;"></i>
                        </p>
                        <span class="focus-input100"></span>
                        <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20 "
                           data-validate="Type password">
                           <p class="inputWithIcon">
                              <label class="login-label" for="">Contrase&ntilde;a</label>
                              <input class="login-password" type="text" name="maskPass" autocomplete="off" ng-model="maskPass"
                                 placeholder="Contrase&ntilde;a" ng-required="true" data-eye mask data-toggle="tooltip" title="Ingresa la contrase&ntilde;a">
                              <i class="fa fa-key" aria-hidden="true" style="color: white;"></i>
                              <input class="input100" type="text" name="j_password" autocomplete="off" hidden="true"
                                 placeholder="Contrase&ntilde;a"  data-eye  value="{{maskPass}}" required>
                           </p>
                        </div>
                        <div>
                           <input class="login-submit" type="submit" value="Acceder" ng-click="submit1();" id="aceptar" >
                        </div>	
                     </form>
                     <!--end form-->
                     <br />
                     <div align="left">
                        <div  class="pull-right">
                           <a class="login-enlace" id="open-recovery-password" >Recuperar Contrase&ntilde;a </a>
                        </div>
                        <br />
                        <div  class="pull-right">
                           <a class="login-enlace" id="open-create-user" >&iquest;Nuevo Usuario? Registrate aqu&iacute;.</a>
                        </div>
                     </div>
                  </section>
               </div>
            </div>
            <div class="row" >
	            <div class="col-sm-12 col-md-12 col-lg-12" align="center">
	               <a onclick="descarga('resources/Facturacion_basica_manual.pdf')">
	               <label class="link-login-manual"> MANUAL DE USUARIO. </label>
	               </a>
	            </div>
            </div>            
         </div>
      </div>
      <!-- END MAIN CONTENT -->
      
      
      <!-- ------------------------------------------------------------------------------------------------ --> 
      <!-- Capa de recuperaci�n de contrase�a -->
      <div id="" ng-controller="modalRestorePasswordController">
      	<jsp:include page="restore-password.jsp"></jsp:include>
      </div>
      
      <!-- Capa de registro de nuevo usuario -->
      <div id="" ng-controller="modalCuentaController">
      	<jsp:include page="add-user.jsp"></jsp:include>
      </div>
      <!-- ----------------------------------------------------------------------------------------------- --> 
      
   </body>
   <br>
   <br>
   <!-- Footer -->
<footer class="footer">

    <!-- Copyright -->
    <div >Centro de Validaci&oacute;n Digital CVDSA S.A de C.V. Calle
            Montecito 38 Piso 25 22 Napoles DF, CP 03810, TEL 90000927
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->
</html>

