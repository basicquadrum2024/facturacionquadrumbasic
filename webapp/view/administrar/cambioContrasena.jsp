<!DOCTYPE html>
<html ng-app="basic">
<head>
<title>Cambio de Contrase�a</title>
<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit"
		async defer></script>
</head>
<body ng-controller="cambioContrasenaController">
<div class="content-body-comprobante-busqueda">
	<div id="barra">
		<div flash-message="7000"></div>
	</div>
	<div class="container ">
	<h3 class="text-center" >Actualizar Contrase�a</h3>
		<!-- <p class="text-center header-restore-password"><strong>Actualizar Contrase�a</strong></p> -->
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<jsp:include page="/view/alert.jsp"></jsp:include>
			</div>
		</div>
		<p><strong>Los campos con * son obligatorios</strong></p>
		<p>La nueva contrase�a deber� complir con el siguiente patr�n:</p>
		<blockquote>
			<li>Longitud m�nima de 8 caracteres</li>
			<li>Incluir por lo menos una may�scula [A-Z]</li>
			<li>Incluir por lo menos un d�gito [0-9]</li>
			<li>Incluir por lo menos un car�cter especial [!@#$%*&(){},/=.?+ ]</li>
			<li>No incluir espacios</li>
		</blockquote>

		<div class="col-sm-6 col-md-10 container">
			<form name="frm" id="frm">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="form-group"
					ng-class="{'has-success':frm.anteriorPassword.$valid}">
					*&nbsp;&nbsp;<label class="control-label" for="anteriorPassword">Contrase�a
						Anterior:</label>
				</div>

				<div class="form-group"
					ng-class="{'has-success':frm.anteriorPassword.$valid}">
					
					<input type="text" class="form-control"
						size="60" name="anteriorPassword" ng-model="anteriorPassword"
						autofocus required mask> 
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="form-group"
					ng-class="{'has-error':frm.pw1.$invalid,'has-success':frm.pw1.$valid}">
					*&nbsp;&nbsp;<label class="control-label" for="pw1">Nueva
						Contrase�a:</label>
				</div>
               
				<div class="form-group"
					ng-class="{'has-error':frm.pw1.$invalid,'has-success':frm.pw1.$valid}">
					
					 <input type="password" class="form-control"
						validar="'password'"
						id="pw1" name="pw1" ng-model="pw1" autofocus required
						ng-minlength="8" ng-maxlength="16" maxlength="17" data-toggle="tooltip"
						data-placement="right"
						title="La contrase�a debe llevar lo siguiente: <br>- M�nino 8 Caracteres.<br>-M�ximo 15 Caracteres.<br>-Contar por lo menos una letra Mayuscula.<br>-Contar por lo menos con un Caracter Especial.<br>-Contar por lo menos con un D�gito.<br>-No tener Espacios en Blanco."
						> 
						  
						<div class="strength-meter">
                        <div class="strength-meter-fill" data-strength="{{passwordStrength}}"></div>
                    </div>
				</div>
				<div ng-show="frm.pw1.$dirty">
					<p class="help-block" ng-show="frm.pw1.$error.required">Campo obligatorio</p>
					<p class="help-block" ng-show="frm.pw1.$error.validar">Contrase�a inv�lida</p>
				</div>
				<div class="alert alert-danger" ng-show="frm.pw1.$error.minlength">La contrase�a debe tener m�nimo 8 car�cteres.</div>
				
				<div class="alert alert-danger" ng-show="frm.pw1.$error.minlength">Contrase�a debil.</div>
				
				<div class="alert alert-danger" ng-show="frm.pw1.$error.maxlength">La contrase�a sobrepasa los 16 car�cteres.</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="form-group"
					ng-class="{'has-error':frm.pw2.$invalid,'has-success':frm.pw2.$valid}">
					*&nbsp;&nbsp;<label class="control-label" for="pw2">Confirmar
						Nueva Contrase�a:</label>
				</div>

				<div class="form-group"
					ng-class="{'has-error':frm.pw2.$invalid,'has-success':frm.pw2.$valid}">
					
					 <input type="password" class="form-control" id="pw2" name="pw2"
						ng-model="pw2" autofocus required pw-check="pw1"
						data-toggle="tooltip" data-placement="right" ng-minlength="8" ng-maxlength="15" maxlength="16"
						title="Recuerda que debes Ingresar lo mismo que has escrito en Nueva Contrase�a."
						>
						<div class="strength-meter">
                        <div class="strength-meter-fill" data-strength="{{passwordStrength}}"></div>
                    </div> 
				</div>
				<div ng-show="frm.pw2.$dirty">
					<p class="help-block" ng-show="frm.pw2.$error.required" >Campo obligatorio</p>
					<p class="help-block" ng-show="frm.pw2.$error.pattern">Contrase�a inv�lida</p>
				</div>
				<div class="alert alert-danger" ng-show="frm.pw2.$error.minlength">La contrase�a debe tener m�nimo 8 caracteres.</div>
				<div class="alert alert-danger" ng-show="frm.pw2.$error.minlength">Contrase�a debil.</div>
				<div class="alert alert-danger" ng-show="frm.pw2.$error.maxlength">La contrase�a sobrepasa los 16 caracteres.</div>
			</div>
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">&nbsp;</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" align="center">
						<input id="checkpassW" type="checkbox" ng-click="cambiar()"
							ng-model="opcion" />Mostrar las contrase�as
					</div>

				</div>
				<br><br>
				<br>
				
				<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group text-center">
						<div ng-controller="cmodal">
							<a ng-click="showAvisoPrivacidad()" class="btn btn-danger btn-sm">Acuerdo
								de Nivel de Servicios
							</a>
						</div>
						<br>
							<input type="checkbox" name="selec" id="checkbox" required
								title="Acepta los T�rminos y Condiciones"/> He le�do y
							acepto las condiciones del servicio
					</div>
				</div> -->
				<div class="row">
				<div class="form-group" align="center">
			        	<div tabindex="3" theme="clean" vc-recaptcha key="'6LfFYG0UAAAAAJsylLTvKIbfdDpgiWPUIP0acdEB'" on-create="setWidgetId(widgetId)"></div>				        	
		            </div>
				</div>
				<br>
				<br>
				<br>
				<br>
				<div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<button class="btn btn-success btn-sm center-block btn-ok"
					ng-disabled="!frm.$valid" ng-click="addPost()">
					Aceptar	<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> </button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
</html>