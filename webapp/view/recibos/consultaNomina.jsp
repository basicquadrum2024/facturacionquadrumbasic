<div class="container">
	<center>
		<h1>CONSULTA DE N�MINA</h1>
	</center>
	<br>
	<br>
	<div class="col-lg-12">
		<div class="col-lg-4 form-group">
			<label for="criterio">Criterio de busqueda</label> <select
				class="form-control" ng-model="buscarRecibo">
				<option value="1">Fecha</option>
				<option value="2">Uuid</option>
				<option value="3">Todos</option>
			</select>
		</div>
		<form ng-show="buscarRecibo==1" class="form-inline" name="buscarFecha"
			role="form">
			<div class="col-lg-3 form-group">
				<label for="fechaInicio">fecha de inicio</label> <input type="text"
					ng-model="buscarFechaInicio" class="form-control"
					name="fechaBuscarComprobante" id="fechaBuscarComprobante">
			</div>
			<div class="col-lg-3 form-group">
				<label for="fechaFinal">fecha final</label> <input type="text"
					class="form-control" name="fechaBuscarComprobanteFinal"
					id="fechaBuscarComprobanteFinal" ng-model="buscarFechaFinal">
			</div>
			<div class="col-lg-2 form-group">
				<button type="submit" class="btn btn-default"
					ng-click="buscarPorFecha()">Buscar Recibo</button>
			</div>
		</form>
		<form ng-show="buscarRecibo==2" class="form-inline" role="form"
			name="buscarUuid">
			<div class="col-lg-3 form-group">
				<label for="fechaInicio">Uuid</label> <input type="text"
					class="form-control" id="Uuid" placeholder="Uuid">
			</div>

			<div class="col-lg-2 form-group">
				<button type="submit" class="btn btn-default"
					ng-click="buscarPorUuid()">Buscar Recibo</button>
			</div>
		</form>
	</div>

	<div class="row">
		<div class="form-group col-md-12 ">
			<div class="container">
				<form name="formularioTabla">
					<div class="table-responsive">
						<div class="tab-content">
							<div class='contenido-buscar table-responsive'
								style="overflow: auto;">
								<%@ include file="../gridPagination.jsp"%>
							</div>
						</div>
					</div>
					<button id="call" ng-click="limpiaTable()" style="display: none;">llamar</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		jQuery.datetimepicker.setLocale('es');
		$("#fechaBuscarComprobante").datetimepicker({

			dateFormat : 'yy-mm-dd',
			timeFormat : 'HH:mm',

			maxDate : ' +1d',

		});

		jQuery.datetimepicker.setLocale('es');
		$("#fechaBuscarComprobanteFinal").datetimepicker({

			dateFormat : 'yy-mm-dd',
			timeFormat : 'HH:mm',
			maxDate : ' +1d',

		});
	});
</script>