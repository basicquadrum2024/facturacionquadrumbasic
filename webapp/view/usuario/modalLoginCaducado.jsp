<style type="text/css">
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus
	{
	color: #FFFFFF;
	background-color: #64B5F6;
	border: 1px solid #dddddd;
	border-bottom-color: transparent;
	cursor: default;
}

.nav-tabs>li>a:hover {
	border-color: #eeeeee #eeeeee #dddddd;
	background-color: #64B5F6;
	color: #FFFFFF;
}

.nav-tabs>li.active>a {
	border-color: #eeeeee #eeeeee #dddddd;
	background-color: #64B5F6;
	color: #FFFFFF;
}
</style>
<div class="modal fade" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Cuenta Caducada.</h4>
			</div>
			
			<div class="modal-body" ng-init="cambio.model=0">
				<div ng-show="cambio.model==0">
					<div class="alert alert-warning" align="center">
						<p><strong>El sistema tiene m�s de 30 d�as sin uso por ese motivo a expirado la cuenta. <br> Favor de comunicarse con el administrador.</strong></p>
					</div>
				</div>
				<br />
				<div class="modal-footer">
					<a href="j_spring_security_logout" class="btn btn-danger btn-sm">
						<span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span> Salir
					</a>
				</div>
			</div>
			<!-- cierra modal body -->
		</div>
		<!-- cierra div content -->

	</div>
	<!-- cierra modal dialog -->
</div>
<!-- div final -->