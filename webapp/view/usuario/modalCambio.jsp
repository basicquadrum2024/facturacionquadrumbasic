<style type="text/css">
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus
	{
	color: #FFFFFF;
	background-color: #64B5F6;
	border: 1px solid #dddddd;
	border-bottom-color: transparent;
	cursor: default;
}

.nav-tabs>li>a:hover {
	border-color: #eeeeee #eeeeee #dddddd;
	background-color: #64B5F6;
	color: #FFFFFF;
}

.nav-tabs>li.active>a {
	border-color: #eeeeee #eeeeee #dddddd;
	background-color: #64B5F6;
	color: #FFFFFF;
}
</style>
<div class="modal fade" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Cambio de Contrase�a.</h4>
			</div>
			<div class="row">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<jsp:include page="/view/alert.jsp"></jsp:include>
				</div>
			</div>
			
			<div class="modal-body" ng-init="cambio.model=1">

				<!--  <div align="center">
					<ul class="nav nav-tabs">
						<li ng-show="usuario.primeraSession==0" class="active"><a
							ng-click="cambio.model=1" ng-focus="cambio.model=1"
							ng-model="cambio.model" data-toggle="tab" href="#menu1"
							class="text-warning">Cambio de contrase�a</a> <span
							ng-if="usuario.primeraSession==0">
								<div ng-init="cambio.model=1"></div>
						</span></li>
					</ul>
				</div>-->

				<div ng-show="cambio.model==1">

					<ul class="text-primary">
						<cite title="Source Title"> <small>
								<li><strong>Longitud m�nima de 8 caracteres</strong></li>
								<li><strong>Longitud m�xima de 15 caracteres</strong></li>
								<li><strong>Incluir por lo menos una
										may�scula[A-Z]</strong></li>
								<li><strong>Incluir por lo menos un d�gito [0-9]</strong></li>
								<li><strong>Incluir por lo menos un car�cter
										especial [ !@#$%*&(){},/=.?+ ]</strong></li>
								<li><strong>No incluir espacios</strong></li>
						</small>
						</cite>
					</ul>
					<br>
					<form name="frm">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label" for="anteriorPassword">Contrase�a
									Anterior:</label>
								<div class="form-group"
									ng-class="{'has-default':frm.anteriorPassword.$valid}">
									<input type="text" class="form-control" id="anteriorPassword"
										size="60" name="anteriorPassword"
										ng-model="anteriorPassword" autofocus required mask >
								</div>
							</div>

							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label" for="pw1">Nueva Contrase�a<span
									ng-if="frm.pw1.$dirty && frm.pw1.$invalid"
									style="color: #a94442">*</span> <span
									ng-if="!(frm.pw1.$dirty && frm.pw1.$invalid)">*</span>:
								</label>
								<div class="form-group"
									ng-class="{'has-error':frm.pw1.$invalid,'has-default':frm.pw1.$valid}">
									<input type="password" class="form-control"
										ng-pattern="/^((?=.*[^a-zA-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%*&(){},/=.?+])(?=.*[A-Z])(?!.*\s).{8,16})$/"
										id="pw1" name="pw1" ng-model="pw1" autofocus required
										ng-minlength="8" ng-maxlength="16" maxlength="17"
										data-toggle="tooltip" data-placement="right"
										title="La contrase�a debe llevar lo siguiente:  
										-Longitud m�nima de 8 caracteres. 
										-Longitud m�xima de 15 caracteres. 
										-Incluir por lo menos una may�scula[A-Z]. 
										-Incluir por lo menos un d�gito [0-9]. 
										-Incluir un car�cter especial [ !@#$%*&(){},/=.?+ ]. 
										-No incluir espacios.">
									<div ng-show="frm.pw1.$dirty">
										<p class="help-block" ng-show="frm.pw1.$error.required">Campo obligatorio</p>
										<p class="help-block" ng-show="frm.pw1.$error.pattern">Contrase�a inv�lida</p>
									</div>
									<div class="alert alert-danger"
										ng-show="frm.pw1.$error.minlength">La contrase�a debe tener m�nimo 8 caracteres.</div>
									<div class="alert alert-danger"
										ng-show="frm.pw1.$error.maxlength">La contrase�a sobrepasa los 16 caracteres.</div>
								</div>

							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
								<label class="control-label" for="pw2">Confirmar
									Contrase�a<span ng-if="frm.pw2.$dirty && frm.pw2.$invalid"
									style="color: #a94442">*</span> <span
									ng-if="!(frm.pw2.$dirty && frm.pw2.$invalid)">*</span>:
								</label>
								<div class="form-group"
									ng-class="{'has-error':frm.pw2.$invalid,'has-default':frm.pw2.$valid}">
									<input type="password" class="form-control" id="pw2" name="pw2"
										ng-model="pw2" autofocus required pw-check="pw1"
										ng-minlength="8" ng-maxlength="16" maxlength="17"
										data-toggle="tooltip" data-placement="right"
										title="Recuerda que debes Ingresar lo mismo que has escrito en Nueva Contrase�a.">
									<div ng-show="frm.pw2.$dirty">
										<p class="help-block" ng-show="frm.pw2.$error.required" >Campo obligatorio</p>
										<p class="help-block" ng-show="frm.pw2.$error.pattern">Contrase�a inv�lida</p>
									</div>
									<div class="alert alert-danger"
										ng-show="frm.pw2.$error.minlength">La contrase�a debe tener m�nimo 8 caracteres.</div>
									<div class="alert alert-danger"
										ng-show="frm.pw2.$error.maxlength">La contrase�a sobrepasa los 16 caracteres.</div>
								</div>

							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">&nbsp;</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" align="center">
								<input id="checkpassW" type="checkbox" ng-click="cambiar()"
									ng-model="opcion" />Mostrar las contrase�as
							</div>

						</div>
						<br>
<!-- 						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							
								
							</div>
 							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" align="center">
								<div ng-controller="cmodal">
									<a ng-click="showAvisoPrivacidad()"
										class="btn btn-danger btn-md">Acuerdo de Nivel de <br />Servicios
									</a>
								</div>
								<h6>
									<input type="checkbox" name="selec" id="checkbox" required
										title="Acepta los T�rminos y Condiciones"> He le�do y
									acepto las <br> condiciones del <br>servicio</input>
								</h6>
							</div>
						</div> -->
						<div class="row">
						<div class="form-group" align="center">
			        	<div tabindex="3" theme="clean" vc-recaptcha key="'6LfFYG0UAAAAAJsylLTvKIbfdDpgiWPUIP0acdEB'" on-create="setWidgetId(widgetId)"></div>				        	
		            </div> 
						</div>
						
						</form>
				</div>
				<!-- cierra cambio.model=1 -->
				<br />
				<div class="modal-footer">
					<a href="j_spring_security_logout" class="btn btn-danger btn-sm">
						<span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span> Salir
					</a>
					<button class="btn btn-success btn-sm" ng-disabled="!frm.$valid"
						ng-click="cambioContraseniaModal()">
						<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
						Aceptar
					</button>
				</div>
			</div>
			<!-- cierra modal body -->
		</div>
		<!-- cierra div content -->

	</div>
	<!-- cierra modal dialog -->
</div>
<!-- div final -->