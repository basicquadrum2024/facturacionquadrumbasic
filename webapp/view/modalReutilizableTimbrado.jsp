<style>
.modal-body-r {
	position: relative !important;
	overflow-y: auto !important;
	max-height: 400px !important;
	padding: 15px !important;
}


.content-body {
	color:black;
}

</style>
<div id="{{idmodalreutilizable}}" class="modal fade content-body"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="false">&times;</button>
				<h4 class="modal-title" style="color: black;">{{header}}</h4>
			</div>
			<div class="modal-body modal-body-r">
				<div align="center" ng-if="mostrarDescargaZip">
					<a href ng-click="funcionDescargarZip()">Descargar todo en un
						.zip</a>
				</div>
				<!--<div id="modalBodyR"></div>  -->
				<table class="table table-condensed">
					<thead>
						<tr>
							<th>Uuid</th>
							<th>Serie</th>
							<th>Folio</th>
							<th>Error</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{resultadoWrappers.uuid}}</td>
							<td>{{resultadoWrappers.serie}}</td>
							<td>{{resultadoWrappers.folio}}</td>
							<td>{{resultadoWrappers.errores}}</td>
							<td>
								<div ng-show="resu.error==0">
									<a title="Descargar XML"
										href="documentos/descarga.json?opcion=xml&objeto={{resultadoWrappers}}">Descargar
										XML</a>&nbsp;|&nbsp;<a title="Descargar PDF"
										href="documentos/descarga.json?opcion=pdf&objeto={{resultadoWrappers}}">Descargar
										PDF</a>
								</div>
								<div ng-show="resu.error==1">{{resu.opcion}}</div>
								<div ng-show="resu.error==2">Cancelado con �xito</div>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<h4 class="text-warning">{{footer}}</h4>
				<button type="button" class="btn btn-success btn-ok"
					data-dismiss="modal" data-ng-click="callbackbuttonright();">Aceptar</button>
			</div>
		</div>
	</div>
</div>