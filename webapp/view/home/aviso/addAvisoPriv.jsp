<div class="modal fade center-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" ng-click="close()" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: black;">Acuerdo de Nivel de Servicios.</h4>
			</div>
			<div class="modal-body" style="text-align: justify !important;" >
				T�RMINOS Y CONDICIONES DE USO DE SERVICIO De Generaci�n de
					Factura Quadrum 1.-CONDICIONES GENERALES Y SU ACEPTACI�N Quadrum
					acreditado por el Servicio de Administraci�n Tributaria (SAT) bajo
					la autorizaci�n No. 69901 como Proveedor Autorizado Certificado
					(PAC) de comprobantes Fiscales Digitales a trav�s de Internet,
					ofrece el servicio de certificaci�n al p�blico en general de los
					Estados Unidos Mexicanos (en adelante denominado Contribuyente) que
					por disposici�n oficial se encuentran obligados a remitir sus
					comprobantes a un proveedor Autorizado Certificado. Por lo que el
					usuario manifiesta que al registrarse al servicio gratuito de
					Quadrum acepta y reconoce el uso limitado y se convierte en
					suscriptor gratuito para el PAC, con lo cual acepta y reconoce que
					la aplicaci�n que usar� es totalmente gratuita y entiende que es
					una aplicaci�n limitada a un servicio b�sico de emisi�n y
					certificaci�n de comprobantes Fiscales Digitales por Internet. A su
					vez el "PAC" en todo momento podr� revocar y cancelar el uso sin
					previo aviso e informando a las autoridades del SAT cuando �ste
					detecte mal uso de la aplicaci�n o intente realizar operaciones no
					deseadas que no sean propias del servicio gratuito. Es importante
					se�alar que el servicio gratuito del PAC no incluye addendas
					comerciales ni complementos espec�ficos no requeridos por la
					autoridad y por consiguiente el "contribuyente" no podr� exigir la
					inclusi�n de dichas addendas comerciales ni de complementos
					adicionales. Adicionalmente acepta y reconoce que el PAC no ser�
					responsable por las excepciones o imposibilidades del uso del
					servicio gratuito cuando derivado de las validaciones efectuadas
					por el SAT los archivos .XML sean rechazados. El "PAC" no ser�
					responsable por la interrupci�n o suspensi�n en la prestaci�n de
					los servicios por caso fortuito, fuerza mayor o actos imputables a
					terceros, proveedores directos e indirectos que el "PAC" emplee
					para la prestaci�n del servicio, ni por fallas en los equipos o
					sistemas del SAT as� como ni de los equipos y sistemas propiedad de
					los "usuarios" que impidan el funcionamiento normal de �stos, as�
					como a las suspensiones debidas a reparaciones normales o a las
					modificaciones que sean necesarias en sus instalaciones. Cuando sea
					necesaria la suspensi�n temporal del servicio gratuito por motivos
					de mantenimiento preventivo, el "PAC" le notificar� al
					"contribuyente" con la mayor anticipaci�n posible. En todo momento
					deber� entenderse que la responsabilidad del PAC se limita al
					proporcionar el servicio de manera confiable en la medida de lo
					posible y lo exime de cualquier da�o o perjuicio que pueda
					generarse por falta de disponibilidad o problemas en el
					funcionamiento del servicio incluyendo la disponibilidad de los
					servicios prestados por el SAT, as� como del mal uso del servicio
					de cualquier �ndole que pudiera presentarse. As� mismo el
					contribuyente tampoco podr� exigir un dise�o exclusivo a la
					reproducci�n impresa o funcionalidades adicionales que ofrece el
					servicio gratuito. En todo momento el "PAC" quedar� exento de
					cualquier futura controversia, obligaci�n, adeudo fiscal, o
					cualquier otro concepto que se genere entre el "Contribuyente" y el
					"SAT", lo anterior debido a que el "usuario" es el �nico y total
					responsable de la informaci�n que ingrese al servicio gratuito,
					tanto en informaci�n del emisor, los receptores importes, impuestos
					y conceptos, as� como la entrega del resultado a sus receptores. Es
					importante se�alar que el "PAC" no guardar� copia, ni almacenar�
					informaci�n, contrase�as, archivos relacionados con la Firma
					Electr�nica Avanzada (FIEL) y/o el Certificado de Sello Digital
					(CSD) del "Contribuyente", as� mismo el "Contribuyente" acepta y
					reconoce expresamente que ser� �l quien ingresar� estos archivos
					solo en el momento que la aplicaci�n gratuita lo solicite y solo
					para la emisi�n del comprobante fiscal digital por internet CFDI.
					El resultado de la emisi�n del CFDI certificado llegar�
					directamente al correo electr�nico del "Contribuyente" de lo cual
					es una copia del .XML certificado y una representaci�n visual en
					formato "PDF". El "PAC" conservara los CFDI certificados por un
					periodo m�ximo de1 a�o, por lo que el "Contribuyente" tiene la
					obligaci�n de descargar estos de la p�gina web y as� mismo acepta y
					reconoce que el PAC podr� eliminarlos de sus registros sin
					responsabilidad alguna ante el contribuyente. Es importante se�alar
					que el contribuyente acepta y reconoce que los CFDI's emitidos y/o
					certificados mediante uso del servicio gratuito ser�n entregados
					directamente al SAT por el PAC. 2.- DEFINICIONES En lo sucesivo se
					entender� para efectos de este documento como: CFDI: Comprobante
					Fiscal Digital a trav�s de Internet CSD: Certificado de Sello
					Digital FIEL: Firma Electr�nica Usuario: Quien har� uso del
					servicio para la generaci�n en l�nea de un comprobante en l�nea
					(CFDI) Quadrum: Proveedor autorizado Certificado como prestador del
					servicio para la generaci�n en l�nea del comprobante (CFDI) SAT:
					Nombre abreviado que corresponde al Servicio de Administraci�n
					Tributaria. 3. OBJETO El PAC ofrece un servicio para la emisi�n de
					documentos CFDI gratuitos de certificaci�n para los contribuyentes
					que requieran usar el servicio de manera voluntaria y sin costo,
					por lo que las presentes condiciones generales regulan la
					prestaci�n del servicio por parte de Quadrum y la utilizaci�n del
					servicio por parte de los usuarios. Quadrum se reserva el derecho
					de modificar unilateralmente en cualquier momento y sin previo
					aviso la presentaci�n, configuraci�n y contenido del servicio, as�
					como las condiciones requeridas para utilizar el servicio. 4. USO Y
					RESTRICCIONES En el proceso de certificaci�n de CFDI, "Quadrum"
					ofrece para su aplicaci�n las siguientes caracter�sticas
					funcionales y servicios generales: a) Emisi�n de Comprobantes
					Fiscales Digitales por Internet (CFDI) b) Procesamiento del CFDI
					con funcionalidad b�sica para el procesamiento �gil y eficiente del
					comprobante uno a uno, sin addendas, ni complementos salvo el sello
					fiscal digital. c) La carga del certificado de Sello Digital (CSD)
					del contribuyente para el sellado de los comprobantes d) Permitir
					precargar los datos fiscales del contribuyente y/o usuario emisor
					e) Generaci�n del documento para su validaci�n y certificaci�n,
					cumpliendo con la especificaci�n t�cnica del anexo 20. f)
					Validaci�n y certificaci�n (timbrado) del documento g) Entrega al
					contribuyente emisor del comprobante certificado o acuse de rechazo
					h) Administrar el almacenamiento de los comprobantes i) Permitir al
					emisor del comprobante consultar, guardar o imprimir los
					comprobantes durante 1 a�o seguidos a partir de la certificaci�n
					del comprobante. j) Env�a copia del CFDI al SAT de manera inmediata
					una vez realizada la certificaci�n del comprobante. Para la
					adecuada prestaci�n de los servicios aqu� descritos Quadrum pondr�
					a disposici�n del usuario: a) Un manual y tutorial de uso b) Un
					servicio de Recepci�n y atenci�n de quejas as� como de atenci�n de
					soporte. 4.1 Soporte T�cnico: Las solicitudes de soporte se
					complementar�n con asistencia ya sea asistencia v�a telef�nica o
					por correo electr�nico para la comunicaci�n y resoluci�n de
					problemas por lo que pone a disposici�n del "Usuario" el n�mero
					telef�nico 9000-0927 y la siguiente direcci�n de correo electr�nico
					soportetecnico@quadrum.com.mx con el fin de prestar los servicios
					descritos en el presente documento en un plazo m�ximo de 8 horas.
					4.2 Mantenimiento: Consiste en un centro de atenci�n telef�nico y
					por correo electr�nico para la comunicaci�n y resoluci�n de
					problemas, consultas e incidencias. Los t�cnicos encargados est�n
					capacitados para la detecci�n y definici�n de problemas asociados
					con el uso del servicio gratuito y su resoluci�n, as� como para la
					instalaci�n, en su caso, de las sucesivas actualizaciones de los
					productos. Puede requerir el acceso remoto a los sistemas del
					Cliente para realizar su tarea. De acuerdo a las acciones descritas
					en el p�rrafo anterior el Quadrum se compromete a realizar en el
					tiempo que considere necesario y de forma las siguientes: a)
					Preventiva: Mediante el desarrollo de actualizaciones de seguridad
					y de funcionamiento del software y la cesi�n de las mismas a sus
					clientes. Evolutiva: mediante el versionado del aplicativo, tanto
					en mejoras internas y de interface del usuario, como en nuevas
					funcionalidades. b) Evolutiva: Mediante el versionado del
					aplicativo, tanto en mejoras internas y de interface del usuario,
					como en nuevas funcionalidades. c) Reactiva: Mediante un servicio
					helpdesk por diversas v�as de comunicaci�n (tel�fono, e-mail-web)
					para ayudar al usuario de la aplicaci�n gratuita a solucionar
					dificultades normales y propias del uso del aplicativo, sin que
					esto incluya la resoluci�n de aver�as del hardware y
					funcionamientos incorrectos del software propiedad del "Usuario",
					ni tampoco incluye la log�stica necesaria para las reparaciones
					f�sicas y la distribuci�n de mejoras de software. La atenci�n
					helpdesk deber� entenderse sobre consultas de configuraci�n del
					software, diferentes de las asociadas a un mal funcionamiento del
					servicio gratuito proporcionado por el PAC. 5. CONDICIONES DE
					ACCESO Y UTILIZACION DEL SERVICIO "Quadrum" ofrece un servicio para
					la emisi�n de documentos CFDI gratuito de certificaci�n a los
					usuarios que requieran usar el servicio de manera voluntaria y sin
					costo. Y para ello deber� tener los siguientes requisitos: El
					"usuario" deber� contar con un servicio de internet. El "usuario"
					deber� registrase en el portal
					https://facturacioncapufe.com.mx/Capufe 5.1 Requisitos
					t�cnicos: Para utilizar este servicio basta con poseer con un
					ordenador equipado con las caracter�sticas t�cnicas m�nimas que se
					necesitan para acceder a cualquier p�gina de internet. 5.2 Claves
					de Acceso En todo momento el usuario es el responsable �nico y
					final de mantener en secreto sus claves de acceso en la plataforma
					Quadrum. 6. RESPONSABILIDAD POR DA� OS Y PERJUICIOS El usuario
					responder� por los da�os y perjuicios de toda naturaleza que
					Quadrum pueda sufrir como consecuencia del incumplimiento de
					cualquiera de las obligaciones a las que queda por virtud de las
					Condiciones Generales o de la ley con la utilizaci�n del servicio.
					7. DURACI�N Y TERMINACI�N La prestaci�n del servicio entra en vigor
					desde la fecha de activaci�n del servicio y ser� por tiempo
					indefinido, siempre y cuando el PAC este sujeto a su autorizaci�n
					vigente y queda entendido que acepta y comprende que el acceso
					quedara bloqueado a partir de los primeros 30 d�as naturales de
					inactividad sin prorrogas, misma que no deber� ser notificado por
					escrito. El usuario puede libremente dejar de utilizar el servicio,
					cuando lo desee no obstante Quadrum en cumplimiento de la ley
					vigente deber� conservar los CFDI's certificados por un plazo
					m�nimo de tres meses. 8. POLITICA DE PRIVACIDAD El Quadrum acepta
					que toda la informaci�n que le sea proporcionada por el usuario es
					de la propiedad de �ste por lo que deber� mantenerla bajo la m�s
					estricta confidencialidad y de acuerdo a los lineamientos
					contemplados en la Ley Federal de Protecci�n de Datos Personales en
					Posesi�n de los Particulares, por lo que no se encuentra autorizado
					para reproducirla, divulgarla, publicarla, referirla, fotocopiarla,
					o de cualquier forma permitir que la conozcan terceros. Quadrum
					protege y salvaguarda sus datos personales para evitar el da�o,
					p�rdida, destrucci�n, robo, extrav�o, alteraci�n as� como el
					tratamiento no autorizado y utilizar� sus datos recabados para
					fines de identificaci�n y verificaci�n con el fin de tramitar su
					solicitud de alta en nuestra plataforma para proveer de los
					servicios y productos que usted ha solicitado, as� como
					identificarle en las relaciones comerciales que realice con
					nosotros y fines de mercadotecnia, por lo que somos responsables de
					la confidencialidad, uso y protecci�n de la informaci�n que en su
					caso nos proporciona. El usuario manifiesta su conocimiento y
					autoriza a Quadrum cumplir con todas las disposiciones establecidas
					en la Ley Federal de Protecci�n de Datos Personales en Posesi�n de
					los Particulares para la prestaci�n del servicio, considerando en
					este acto que los siguientes datos personales ser�n almacenados en
					los sistemas del "PAC" y que estos no ser�n utilizados para fines
					distintos a la prestaci�n del servicio. Para en el caso de que
					desee limitar el uso de su informaci�n personal, ejercitar sus
					derechos de acceder, rectificar y cancelar sus datos personales as�
					como de oponerse al tratamiento de los mismos o revocar el
					consentimiento que para tal fin nos haya otorgado lo podr� realizar
					a trav�s de nuestro departamento de Atenci�n al Cliente en el
					tel�fono 9000-0927 o a trav�s de nuestro correo electr�nico
					contacto@quadrum.com.mx . Dicha solicitud deber� contener por lo
					menos su nombre, domicilio completo, documentos que acrediten su
					identidad, se�alando claramente el v�nculo que tiene con la empresa
					y especificando en forma clara y precisa los datos personales de
					los que solicita su acceso, rectificaci�n, actualizaci�n o
					cancelaci�n e indicando las razones por las cuales considera que
					sus datos deban ser actualizados, rectificados o cancelados. La
					obligaci�n de confidencialidad a que se compromete Quadrum es
					absoluta, por lo cual no se encuentra sujeta a plazo o t�rmino de
					prescripci�n alguna. 9. PROPIEDAD INTELECTUAL Es voluntad de las
					partes que el usuario deber� respetar en todo momento los derechos
					de propiedad intelectual e industrial que Quadrum tenga como pueden
					ser el software o aplicativos web y en general cualquier derecho
					inherente a la propiedad intelectual y propiedad industrial
					propiedad de Quadrum a las cuales el usuario llegue a tener acceso
					por el uso del sitio web o de la aplicaci�n web. Por tal virtud el
					usuario se obliga a no vulnerar los mencionados derechos de
					propiedad intelectual o industrial de Quadrum 10. MODIFICACIONES
					Quadrum se reserva el derecho de modificar los presentes t�rminos y
					condiciones de uso en cualquier momento siempre y cuando notifique
					al usuario a trav�s de notas en los sitios dentro de los cuales se
					utilice la aplicaci�n o a trav�s de una notificaci�n o por correo
					electr�nico o correo postal. El uso de los servicios ofrecidos
					despu�s de efectuadas dichas notificaciones se tendr� por la m�s
					amplia aceptaci�n y consentimiento de los nuevos t�rminos y
					condiciones de uso. Las modificaciones que realice Quadrum en los
					t�rminos de este apartado se apegar�n en todo momento a los
					requerimientos legales que dispongan el SAT y la legislaci�n
					aplicable. 11. CESI�N Y SUBCONTRATACI�N "Quadrum" queda autorizado
					a ceder o subcontratar con terceras empresas el servicio de
					mantenimiento contratado, sin necesidad de comunicaci�n ni
					consentimiento del "Usuario" y sin que ello implique modificaci�n
					de las condiciones establecidas en los presentes t�rminos y
					condiciones. 12. GARANT�A Quadrum garantiza la prestaci�n del
					servicio y del mantenimiento en los t�rminos especificados en este
					documento de forma adecuada a cada caso y con observancia de la
					diligencia profesional y t�cnica debida. 13. CONFIDENCIALIDAD
					Cualquiera de las partes que reciba informaci�n confidencial de la
					otra, deber� mantenerla de forma reservada as� como utilizarla
					�nica y exclusivamente de acuerdo con la finalidad para la que haya
					sido revelada especialmente el "usuario" guardar� estricta
					confidencialidad con respecto a la documentaci�n e informaci�n
					impresa, verbal, audiovisual o de cualquier otra �ndole que Quadrum
					le proporcione para el cumplimiento de los presentes t�rminos y
					condiciones. 14. RECONOCIMIENTO CONTRACTUAL Los presentes t�rminos
					y condiciones constituyen la plena total y �nica manifestaci�n de
					la voluntad de las partes por lo que cualquier acuerdo, escrito,
					notificaci�n u oferta realizada previamente a estos t�rminos y
					condiciones, sea de manera escrita o verbal y que se relacione
					directa e indirectamente con el objeto del presente documento,
					queda sin efecto alguno por lo que ambas partes se reserva acci�n o
					derecho alguno que ejercer derivado de cualquiera de dichos actos
					previos a la aceptaci�n del presente documento. Tanto el usuario
					como Quadrum manifiestan que aceptan y reconocen que el uso de la
					herramienta gratuita se considera en s� como aceptaci�n de todos y
					cada uno de los t�rminos aqu� establecidos con todos los alcances
					legales que esto implica. 15. LEYES APLICABLES Y JURISDICCI�N La
					interpretaci�n y cumplimiento del contenido y alcance del presente
					documento se regir� por las disposiciones aplicables de los Estados
					Unidos Mexicanos, acordando ambas partes someterse expresamente a
					la jurisdicci�n de los Tribunales competentes en el Distrito
					Federal y en consecuencia renuncian a cualquier otro fuero que les
					pudiera corresponder en raz�n de sus domicilios particulares o
					convencionales, presentes o futuros o por cualquier otra causa que
					pudiera corresponderles.

			</div>
			<div class="modal-footer">
				<button type="button" ng-click="close()" class="btn btn-success btn-sm" data-dismiss="modal">
				Aceptar <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></button>
			</div>
		</div>
	</div>
</div>
