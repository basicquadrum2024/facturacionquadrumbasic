<script
	src="<%=request.getContextPath()%>/resources/js/jquery/jquery.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/jquery/jquery-ui.js"></script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/jquery.datetimepicker.full.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/angular.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/angular-sanitize.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/bootstrap/js/bootstrap.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/bootstrap/js/ui-bootstrap-tpls.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/angular-animate.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/angular-flash.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/angular-route.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/angular-ui-router.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/angular-modal-service.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/checklist-model.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/i18n/angular-locale_es-mx.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/angular-1.3.2/angular-confirm.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/fileinput.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/loading.js"></script>


<script
	src="<%=request.getContextPath()%>/resources/js/autocomplete/autoComplete.js"></script>
	
	<script
	src="<%=request.getContextPath()%>/resources/js/reCaptcha/angular-recaptcha.min.js"></script>
	
	<script
	src="<%=request.getContextPath()%>/resources/js/alasql.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/modalReutilizable.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/app.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/fabricas.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/servicios.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/perfilController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/modalMonitorController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/cancelacionController.js"></script>
<!-- 	controladores -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/generacionComprobanteCfdiV33Controller.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/generacionComprobanteV40Controller.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/cambioContrasenaController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/emisorController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/busquedaController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/administraTrabajadorController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/consultasTrabajadorController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/altaEmpresasController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/administracionPermisosController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/pistasController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/administracionUsuariosController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/pagosController.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/buzonController.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/administraBuzon.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/administrarAcuerdo.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/contacto.js"></script>
	
<!-- fin controladores	 -->

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/ng-grid-1.3.2.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/gridPaginationController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/metodoPagoController.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/Util.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/view/controllers/filtros.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/filtro/filtros.js"></script>
