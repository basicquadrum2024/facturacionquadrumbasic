<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="es-mx" ng-app="basic">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="resources/img/favicon.ico">
<title>Facturacion Basic</title>
<!-- CSS -->
<%@ include file="direccionesCss.jsp"%>
<script language="Javascript">
document.oncontextmenu = function(){return false}
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit"
		async defer></script>
</head>
<body>
	<%@ include file="/loading.jsp"%>
	<%@ include file="/modalValidarContinuidadTimbrado.jsp"%>
	<%@ include file="/modalValidacionLugarExpedicion.jsp"%>
	<input type="hidden" ng-model="tiempoSession"
		ng-init="tiempoSession='${pageContext.session.maxInactiveInterval}'">
	<div ng-controller="monitorController"></div>
	<!-- Contenido -->
	<main class="page"> <!-- MENU DEL SISTEMA --> <%@ include
		file="header.jsp"%>
	<div class="container">
		<div ng-view=""></div>
		<br />
	</div>
	<%@ include file="footer.jsp"%> </main>
	<!-- DIRECCIONES DE LOS CONTROLADORES DE ANGULAR -->
	<jsp:include page="direccionesJS.jsp"></jsp:include>
</body>
</html>