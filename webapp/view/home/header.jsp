<!-- Header -->
<c:set var="menuList" value="${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.usuario.tcPerfil}"></c:set>
<input type="hidden" ng-model="primeraSession"
   ng-init="primeraSession='${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.usuario.primerasession}'"> 
<input type="hidden" ng-model="rfcEmisor"
   ng-init="rfcEmisor='${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.usuario.rfce}'">
<input type="hidden" ng-model="fechaCambio"
   ng-init="fechaCambio='${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.usuario.fechacambiopassword}'">
<input type="hidden" ng-model="fechaLogin"
   ng-init="fechaLogin='${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.usuario.ultimaconexion}'">
<input type="hidden" ng-model="tcPerfil"
   ng-init="tcPerfil='${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.usuario.tcPerfil.id}'">
<div ng-controller="modalControl"></div>
<div id="barra">
   <div flash-message="7000"></div>
   <!--
   <div class="network-nav" style="height: 95px">
      <div>
         <div class="row">
            <div class="col-sm-6 col-md-6">
               <div class="navbar-brand"
                  style="color: #1E98B3; font-size: 12pt; background-color:;">
                  	 
                     <img alt="Gob.mx" src="resources/img/logoQuadrum.png"
                     style="width: 200 px; height: 80px; margin-left: 20px">
                   
               </div>
            </div>
         </div>
      </div>
      <div class="top-nav"></div>
   </div>
   -->
   <!-- Barra de navegaci�n -->
   <nav class="navbar navbar-default navBar-home bg-light">
   	<a class="navbar-brand" href="#"><span class="glyphicon glyphicon-home">
            	</span></a>
      <div class="navbar-header">
         <div class="col-md-10" style="top: 15px;">
            <c:if test="${pageContext.request.userPrincipal.name != null}">
               <label class="col-md-12 text-right">${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.usuario.rfce}</label>
            </c:if>
         </div>
      </div>
      <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            	<span class="glyphicon glyphicon-menu-hamburger">
            	</span> Men� Principal<span class="caret"></span>
           	</a>
            <ul class="dropdown-menu">
               <c:forEach items="${menuList.tcAccioneses}" var="acciones">
                  <li class="divider"></li>
                  <li>
                  	<a href="${acciones.accion}">
                  		<span class="glyphicon glyphicon-chevron-right"></span> ${acciones.nombre}</a>
  				  </li>
               </c:forEach>
            </ul>
         <li>
            <a id="salir" href="<c:url value="/j_spring_security_logout"/>
               ">
                  <span class="glyphicon glyphicon-log-in" style="margin-right: 10px;"> Salir </span>
            </a>
         </li>
      </ul>
   </nav>
</div>

