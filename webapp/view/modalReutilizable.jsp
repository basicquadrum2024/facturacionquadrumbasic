<style>
.content-body {
	color:black;
}
</style>
<div id="{{idmodalreutilizablec}}" class="modal fade content-body"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color:  black;">{{header}}</h4>
			</div>
			<div class="modal-body">
				<!--<div id="modalBodyR"></div>  -->
				<table class="table table-condensed">
					<thead>
						<tr>
							<th>uuid</th>
							<th>serie</th>
							<th>folio</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>				
						<tr ng-repeat="resu in resultadoWrappers">
							<td>{{resu.uuid}}</td>
							<td>{{resu.serie}}</td>
							<td>{{resu.folio}}</td>
						
							<td>
							<div ng-show="resu.error==0"><a title="Descargar XML"
								href="documentos/descargaTimbrado.json?opcion=xml&objeto={{resu}}">Descargar
									XML</a>&nbsp;|&nbsp;<a title="Descargar PDF"
								href="documentos/descargaTimbrado.json?opcion=pdf&objeto={{resu}}">Descargar
									PDF</a></div>
							<div ng-show="resu.error==2">{{resu.opcion}}</div></td>
							
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<h4 class="text-warning">{{footer}}</h4>
				<button type="button" class="btn btn-success btn-ok" data-dismiss="modal"
					data-ng-click="callbackbuttonright();">Aceptar</button>
			</div>
		</div>
	</div>
</div>