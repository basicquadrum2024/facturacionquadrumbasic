<div class="content-body-buzon">

<h1 class="text-center">Contacto</h1>
<br>
<div class="row">
<span class="glyphicon glyphicon-map-marker">  Conocenos</span>
<br>
<p>Centro de Validaci&oacute;n Digital CVDSA S.A de C.V. calle Montecito 38 Piso 25 22 Napoles DF, CP 03810, Tel. (55) 9000 0927
</p>
</div>
<div class="row">
<span class="glyphicon glyphicon-envelope">  Env&iacute;anos un email</span>
<br>
<p>soportetecnico@quadrum.com.mx</p>
</div>

<div class="row"> 
<span class="glyphicon glyphicon-earphone">  N&uacute;meros de atenci&oacute;n a cliente</span>
<br>
<p>(01) 442 161 2565, (01) 551 209 1825</p>
</div>
<div class="row">
<div>
<a href="https://www.facebook.com/grup0quadrum?ref=ts&amp;fref=ts" target="_blank" >
    <span class="fa fa-facebook">  Siguenos en Facebook</span> 
  </a>
</div>
</div>

</div>