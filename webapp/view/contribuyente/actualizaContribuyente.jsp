<div class="content-body-actualiza">
	<form id="form" name="formEstatus" class="form-horizontal">
		<h3>
			<strong>Actualizar Datos de Contribuyente: </strong>{{rfcEmisor}}
		</h3>

		<hr class="red">
	</form>
	<div class='table-responsive' style="overflow: auto;"></div>
	<form id="formContribuyente" name="formContribuyente" role="form"
		enctype="multipart/form-data;charset=UTF-8">
		<input type="hidden" ng-model="contribuyente.id" /> <input
			type="hidden" ng-model="contribuyente.domiciliofiscal.id" />

		<div class="row">
			<div class="col-xs-6">
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.rfc.$invalid}">
					<label class="control-label" for="rfc"> *RFC <span
						ng-if="formContribuyente.rfc.$dirty && formContribuyente.rfc.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.rfc.$invalid && !formContribuyente.rfc.$pristine}">
						<input class="form-control" type="text" id="rfc" name="rfc"
							placeholder="RFC" ng-model="contribuyente.rfc" autocomplete="off"
							maxlength="13" autocomplete="off" required
							ng-disabled="contribuyente.id!=null" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.rfc.$error.required">RFC es
							requerido</p>
					</div>
					<!-- validaRfc();validarRfcEmisor(); -->
				</div>
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.razonSocial.$invalid}">
					<label class="control-label" for="razonSocial">* Raz�n
						Social:</label>
					<div>
						<input class="form-control" type="text" id="razonSocial"
							name="razonSocial" placeholder="Razon Social"
							ng-model="contribuyente.nombre" autocomplete="off"
							ng-maxlength="254" paste-trimed validar="'razonSocial'"
							capitalize required>
						<p ng-show="formContribuyente.razonSocial.$error.required"
							class="help-block">Raz�n Social es requerido.</p>
						<p ng-show="formContribuyente.razonSocial.$error.validar"
							class="help-block">Caracter inv�lido.</p>
						<div class="help-block"
							ng-show="formContribuyente.razonSocial.$error.maxlength">La
							raz�n social sobre pasa los 254 caracteres.</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.regimen.$invalid}">
					<label class="control-label" for="regimen">* R�gimen
						Fiscal:</label>
					<div>
						<select id="regimen" name="regimen" required
							ng-model="contribuyente.regimenFiscal" class="form-control"
							ng-options="regimen.clave as regimen.clave+' '+regimen.descripcion for regimen in listaRegimen ">
						</select>
						<p ng-show="formContribuyente.regimen.$error.required"
							class="help-block">R�gimen Fiscal es requerido.</p>
					</div>
				</div>
			</div>
		</div>
		<div ng-init="mostrarCer">
			<h4>
				<strong>Certificados:</strong>
			</h4>
			<input type="checkbox" name="certificado" ng-true-value="true"
				ng-false-value="false" ng-model="actualizarCer"> Actualizar
			certificados
			<hr class="red">
			<div class="row" ng-show="actualizarCer">
				<div class="col-xs-6">
					<div class="form-group"
						ng-class="{ 'has-error' : formContribuyente.cve.$invalid}">
						<label class="control-label" for="cve">* Clave:</label>
						<div>
							<input class="form-control" type="password" id="cve" name="cve"
								placeholder="Clave" ng-model="contribuyente.clave"
								autocomplete="off" ng-maxlength="300"
								ng-required="actualizarCer" maxlength="301">
							<p ng-show="formContribuyente.cve.$error.required"
								class="help-block">Clave es requerido.</p>
							<div class="help-block"
								ng-show="formContribuyente.cve.$error.maxlength">La clave
								sobre pasa los 300 caracteres</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6"></div>
				<div class="col-xs-6">
					<div class="form-group">
						<label class="control-label" for="cer">* Seleccione
							Archivo Cer:</label>
						<div>
							<input id="fileCer" name="fileCer" type="file" class="file"
								autocomplete="off" valida-cer="fileCer"
								ng-model="contribuyente.llaveCer" accept="cer" valid-file
								data-allowed-file-extensions='["cer"]'
								ng-required="actualizarCer">
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group">
						<label class="control-label" for="key">* Seleccione
							Archivo Key:</label>
						<!-- <div> -->

						<div>
							<input id="fileKey" name="fileKey" type="file" class="file"
								valida-archivo="fileKey" ng-model="contribuyente.llaveKey"
								accept="key" valid-file data-allowed-file-extensions='["key"]'
								ng-required="actualizarCer">
						</div>
					</div>
				</div>
			</div>
		</div>
		<h4>
			<strong>Direcci�n del Contribuyente:</strong>
		</h4>
		<hr class="red">
		<div class="row">

			<div class="col-xs-6">
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.calle.$invalid}">
					<label class="control-label" for="direccion">* Domicilio:</label>
					<div>
						<input class="form-control" type="text" id="domicilio"
							name="domicilio" placeholder="Domicilio" autocomplete="off"
							required="required" ng-model="contribuyente.domicilio"
							ng-maxlength="1000" maxlength="1000" paste-trimed capitalize>
						<p ng-show="formContribuyente.domicilio.$error.required"
							class="help-block">La direccion es requerida.</p>
						<div class="help-block"
							ng-show="formContribuyente.domicilio.$error.maxlength">La
							direccion sobre pasa los 1000 caracteres</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.cp.$invalid}">
					<label class="control-label" for="cp">*C�digo Postal(Lugar
						de expedici�n):</label>
					<div>
						<input class="form-control" type="text" id="cp" name="cp"
							placeholder="C�digo Postal" required="required"
							ng-model="contribuyente.lugarExpedicion" autocomplete="off"
							ng-blur="validarLugarExpedicion()" ng-maxlength="5" maxlength="6"
							validar="'codigoPostal'" paste-trimed capitalize>
						<p ng-show="formContribuyente.cp.$error.required"
							class="help-block">C�digo Postal es requerido.</p>
						<div class="help-block"
							ng-show="formContribuyente.cp.$error.maxlength">El c�digo
							postal sobre pasa los 5 caracteres</div>
						<div ng-show="formContribuyente.cp.$dirty">
							<p class="help-block"
								ng-show="formContribuyente.cp.$error.validar">C�digo postal
								inv�lido</p>
						</div>
					</div>

				</div>
			</div>
			<!-- 
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.lugarExpedicion.$invalid}">
					<label class="control-label" for="lugarExpedicion">*C�digo Postal(Lugar de expedici�n):</label>
					<div>
						<input class="form-control" type="text" id="lugarExpedicion" name="lugarExpedicion"
							placeholder="Lugar de Expedici�n" autocomplete="off" required="required"
							ng-model="contribuyente.lugarExpedicion" 
							ng-maxlength="5" maxlength="5" validar="'lugarExpedicion'"
							paste-trimed capitalize>
						<p ng-show="formContribuyente.lugarExpedicion.$error.required"
							class="help-block">El lugar de expedici�n es requerido</p>
						<div class="help-block" ng-show="formContribuyente.lugarExpedicion.$error.maxlength">El lugar de expedici�n es requerida</div>
						
					</div>
				</div>
				-->
			<!--  
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.colonia.$invalid}">
					<label class="control-label" for="colonia"> Colonia:</label>
					<div>
						<input class="form-control" type="text" id="colonia"
							name="colonia" placeholder="Colonia" ng-maxlength="300" maxlength="301"
							ng-model="contribuyente.tcDomicilio.colonia" autocomplete="off"
							paste-trimed capitalize>
						<div class="help-block" ng-show="formContribuyente.colonia.$error.maxlength">La colonia 
							sobre pasa los 300 caracteres</div>
					</div>
				</div>
-->
			<!--  	<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.municipio.$invalid}">
					<label class="control-label" for="municipio">* Municipio o
						Delegaci�n:</label>
					<div>
						<input class="form-control" type="text" id="municipio"
							name="municipio" placeholder="Municipio o Delegaci�n"
							ng-model="contribuyente.tcDomicilio.municipio" autocomplete="off" 
							maxlength="301" ng-maxlength="300"
							paste-trimed capitalize required>
							<p ng-show="formContribuyente.municipio.$error.required"
							class="help-block">Municipio es requerido.</p>
							<div class="help-block" ng-show="formContribuyente.municipio.$error.maxlength">El
								municipio sobre pasa los 300 caracteres</div>
					</div>
				</div>
-->
			<!--  -->
			<!--  
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.localidad.$invalid}">
					<label class="control-label" for="localidad"> Localidad:</label>
					<div>
						<input class="form-control" type="text" id="localidad"
							name="localidad" placeholder="Localidad" maxlength="301" ng-maxlength="300"
							ng-model="contribuyente.tcDomicilio.localidad" autocomplete="off" ng-maxlength="150"
							paste-trimed capitalize >
						<div class="help-block" ng-show="formContribuyente.localidad.$error.maxlength">La localidad
							sobre pasa los 300 caracteres</div>
					</div>
				</div>
-->
			<!--  
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.referencia.$invalid}">
					<label class="control-label" for="referecia"> Referencia:</label>
					<div>
						<input class="form-control" type="text" id="referencia"
							name="referencia" placeholder="Referencia" maxlength="301" ng-maxlength="300"
							ng-model="contribuyente.tcDomicilio.referencia" autocomplete="off" ng-maxlength="150"
							paste-trimed capitalize>
						<div class="help-block" ng-show="formContribuyente.referencia.$error.maxlength">La referencia
							sobre pasa los 300 caracteres</div>
					</div>
				</div>
				-->


			<div class="col-xs-6">
				<!--  
				<div class="col-xs-6">
					<div class="form-group"
						ng-class="{ 'has-error' : formContribuyente.numeroExt.$invalid}">
						<label class="control-label" for="numeroExt"> No. Exterior:</label>
						<div>
							<input class="form-control" type="text" id="numeroExt"
								name="numeroExt" placeholder="No. Exterior" maxlength="301" ng-maxlength="300"
								ng-model="contribuyente.tcDomicilio.noExterior" autocomplete="off" ng-maxlength="150"
								paste-trimed capitalize>
						<div class="help-block" ng-show="formContribuyente.numeroExt.$error.maxlength">El no. Ext
							sobre pasa los 300 caracteres</div>
						</div>
					</div>
				</div>
				-->
				<!--  
				<div class="col-xs-6">
					<div class="form-group"
						ng-class="{ 'has-error' : formContribuyente.numeroInt.$invalid}">
						<label class="control-label" for="numeroInt"> No.
							Interior:</label>
						<div>
							<input class="form-control" type="text" id="numeroInt"
								name="numeroInt" placeholder="No. Interior" maxlength="301" ng-maxlength="300"
								ng-model="contribuyente.tcDomicilio.noInterior" autocomplete="off" ng-maxlength="150"
								paste-trimed capitalize>
							<div class="help-block" ng-show="formContribuyente.numeroInt.$error.maxlength">El no. Int
								sobre pasa los 300 caracteres</div>
						</div>
					</div>
				</div>
				-->
				<!-- 
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.estado.$invalid}">
					<label class="control-label" for="estado">* Estado:</label>
					<div>
						<input class="form-control" type="text" id="estado" name="estado"
							placeholder="Estado" required="required"
							ng-model="contribuyente.tcDomicilio.estado" autocomplete="off" ng-maxlength="300" maxlength="301"
							paste-trimed capitalize>
						<p ng-show="formContribuyente.estado.$error.required"
							class="help-block">Estado es requerido.</p>
						<div class="help-block" ng-show="formContribuyente.estado.$error.maxlength">El Estado
								sobre pasa los 300 caracteres</div>
					</div>
				</div>
				-->
				<!-- 
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.pais.$invalid}">
					<label class="control-label" for="pais">* Pa�s:</label>
					<div>
						<input class="form-control" type="text" id="pais" name="pais"
							placeholder="Pa�s" required="required"
							ng-model="contribuyente.tcDomicilio.pais" autocomplete="off" ng-maxlength="300" maxlength="301"
							paste-trimed capitalize>
						<p ng-show="formContribuyente.pais.$error.required"
							class="help-block">Pa�s es requerido.</p>
						<div class="help-block" ng-show="formContribuyente.pais.$error.maxlength">El Pa�s
								sobre pasa los 300 caracteres</div>
					</div>
				</div>
-->
				<!-- 
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.cp.$invalid}">
					<label class="control-label" for="cp">* C�digo Postal:</label>
					<div>
						<input class="form-control" type="text" id="cp" name="cp"
							placeholder="C�digo Postal" required="required"
							ng-model="contribuyente.tcDomicilio.codigoPostal" autocomplete="off" ng-maxlength="5" maxlength="6"
							validar="'codigoPostal'" paste-trimed capitalize>
						<p ng-show="formContribuyente.cp.$error.required"
							class="help-block">C�digo Postal es requerido.</p>
						<div class="help-block" ng-show="formContribuyente.cp.$error.maxlength">El c�digo postal
								sobre pasa los 5 caracteres</div>
						<div ng-show="formContribuyente.cp.$dirty">
							<p class="help-block" ng-show="formContribuyente.cp.$error.validar">C�digo postal inv�lido</p>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.numeroEscrituraPublica.$invalid}">
					<label class="control-label" for="numeroEscrituraPublica">
						*N�MERO DE LA ESCRITURA P�BLICA (ACTA CONSTITUTIVA) <span
						ng-if="formContribuyente.numeroEscrituraPublica.$dirty && formContribuyente.numeroEscrituraPublica.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.numeroEscrituraPublica.$invalid && !formContribuyente.numeroEscrituraPublica.$pristine}">
						<input class="form-control" type="text"
							id="numeroEscrituraPublica" name="numeroEscrituraPublica"
							placeholder="N�mero Escritura Publica"
							ng-model="contribuyente.numeroEscrituraPublica"
							autocomplete="off" maxlength="500" autocomplete="off"
							ng-required="requeridoMoral" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.numeroEscrituraPublica.$error.required">campo
							requerido</p>
					</div>

				</div>
			</div>
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.fechaActa.$invalid}">
					<label class="control-label" for="fechaActa">
						*FECHA DE LA ESCRITURA P�BLICA (ACTA CONSTITUTIVA) <span
						ng-if="formContribuyente.fechaActa.$dirty && formContribuyente.fechaActa.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.fechaActa.$invalid && !formContribuyente.fechaActa.$pristine}">
						<input id="fechaActa" name="fechaActa"
						class="form-control" type="text" ng-model="fechaEscrituraPublica" ng-required="requeridoMoral"
						data-tooltip="La fecha debe tener  1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora hh:mm, Ejemplo: 2016/06/07 00:00">
						<p class="help-block"
							ng-show="formContribuyente.fechaActa.$error.required">campo
							requerido</p>
					</div>
					<!-- validaRfc();validarRfcEmisor(); -->
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.nombreNotario.$invalid}">
					<label class="control-label" for="nombreNotario"> * NOMBRE
						DEL NOTARIO <span
						ng-if="formContribuyente.nombreNotario.$dirty && formContribuyente.nombreNotario.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.nombreNotario.$invalid && !formContribuyente.nombreNotario.$pristine}">
						<input class="form-control" type="text" id="nombreNotario"
							name="nombreNotario" placeholder="Nombre del Notario"
							ng-model="contribuyente.nombreNotario" autocomplete="off"
							maxlength="500" autocomplete="off" ng-required="requeridoMoral"
							capitalize paste-trimed data-placement="top"
							style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.nombreNotario.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.numeroNotario.$invalid}">
					<label class="control-label" for="numeroNotario"> *N�MERO
						DEL NOTARIO P�BLICO <span
						ng-if="formContribuyente.numeroNotario.$dirty && formContribuyente.numeroNotario.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.numeroNotario.$invalid && !formContribuyente.numeroNotario.$pristine}">
						<input class="form-control" type="text" id="numeroNotario"
							name="numeroNotario" placeholder="N�mero del Notario"
							ng-model="contribuyente.numeroNotario" autocomplete="off"
							maxlength="500" autocomplete="off" ng-required="requeridoMoral"
							capitalize paste-trimed data-placement="top"
							style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.numeroNotario.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.entidadFederativaNotario.$invalid}">
					<label class="control-label" for="entidadFederativaNotario">
						*ENTIDAD FEDERATIVA DEL NOTARIO P�BLICO <span
						ng-if="formContribuyente.entidadFederativaNotario.$dirty && formContribuyente.entidadFederativaNotario.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.entidadFederativaNotario.$invalid && !formContribuyente.entidadFederativaNotario.$pristine}">
						<input class="form-control" type="text"
							id="entidadFederativaNotario" name="entidadFederativaNotario"
							placeholder="Entidad del notario"
							ng-model="contribuyente.entidadFederativaNotario"
							autocomplete="off" maxlength="500" autocomplete="off"
							ng-required="requeridoMoral" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.entidadFederativaNotario.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.entidadFederativaConstruyoEmpresa.$invalid}">
					<label class="control-label"
						for="entidadFederativaConstruyoEmpresa"> *ENTIDAD
						FEDERATIVA DONDE SE CONSTITUYE LA EMPRESA <span
						ng-if="formContribuyente.entidadFederativaConstruyoEmpresa.$dirty && formContribuyente.entidadFederativaConstruyoEmpresa.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.entidadFederativaConstruyoEmpresa.$invalid && !formContribuyente.entidadFederativaConstruyoEmpresa.$pristine}">
						<input class="form-control" type="text"
							id="entidadFederativaConstruyoEmpresa"
							name="entidadFederativaConstruyoEmpresa"
							placeholder="Entidad Federativa donde se construyo la empresa"
							ng-model="contribuyente.entidadFederativaConstruyoEmpresa"
							autocomplete="off" maxlength="500" autocomplete="off"
							ng-required="requeridoMoral" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.entidadFederativaConstruyoEmpresa.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
		</div>
	<div class="row">
						<div class="col-xs-6">
							<div ng-show="tipoRfc=='M'" class="form-group"
								ng-class="{ 'has-error' : formContribuyente.folioMercantil.$invalid}">
								<label class="control-label" for="folioMercantil">
									*FOLIO MERCANTIL DONDE QUED� INSCRITA (ENTIDAD DE REGISTRO) <span
									ng-if="formContribuyente.folioMercantil.$dirty && formContribuyente.folioMercantil.$invalid"
									style="color: #a94442"></span>:
								</label>

								<div
									ng-class="{ 'has-error' : formContribuyente.folioMercantil.$invalid && !formContribuyente.folioMercantil.$pristine}">
									<input class="form-control" type="text" id="folioMercantil"
										name="folioMercantil" placeholder="Folio Mercantil"
										ng-model="contribuyente.folioMercantil" autocomplete="off"
										maxlength="100" autocomplete="off"
										ng-required="requeridoMoral" capitalize paste-trimed
										data-placement="top" style="color: black;">
									<p class="help-block"
										ng-show="formContribuyente.folioMercantil.$error.required">campo
										requerido</p>
								</div>
							</div>
						</div>
							<div class="col-xs-6">
							<div ng-show="tipoRfc=='M'" class="form-group"
								ng-class="{ 'has-error' : formContribuyente.fechaFolioMercantil.$invalid}">
								<label class="control-label" for="fechaFolioMercantil"> * FECHA DONDE QUEDA INSCRITO EL FOLIO MERCANTIL <span
									ng-if="formContribuyente.fechaFolioMercantil.$dirty && formContribuyente.fechaFolioMercantil.$invalid"
									style="color: #a94442"></span>:
								</label>

								<div
									ng-class="{ 'has-error' : formContribuyente.fechaFolioMercantil.$invalid && !formContribuyente.fechaFolioMercantil.$pristine}">
									<input id="fechaFolioMercantil" name="fechaFolioMercantil" class="form-control"
										type="text" ng-model="fechaFolioMer" ng-required="requeridoMoral"
										data-tooltip="La fecha debe tener  1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora hh:mm, Ejemplo: 2016/06/07 00:00">
									<p class="help-block"
										ng-show="formContribuyente.fechaFolioMercantil.$error.required">campo
										requerido</p>
								</div>
								<!-- validaRfc();validarRfcEmisor(); -->
							</div>
						</div>
						
					</div>
					<div class="row">
					<div class="col-xs-6">
							<div ng-show="tipoRfc=='M'" class="form-group"
								ng-class="{ 'has-error' : formContribuyente.nombreRepresentanteLegal.$invalid}">
								<label class="control-label" for="nombreRepresentanteLegal">
									*NOMBRE DEL REPRESENTANTE LEGAL/APODERADO LEGAL <span
									ng-if="formContribuyente.nombreRepresentanteLegal.$dirty && formContribuyente.nombreRepresentanteLegal.$invalid"
									style="color: #a94442"></span>:
								</label>

								<div
									ng-class="{ 'has-error' : formContribuyente.nombreRepresentanteLegal.$invalid && !formContribuyente.nombreRepresentanteLegal.$pristine}">
									<input class="form-control" type="text" id="nombreNotario"
										name="nombreRepresentanteLegal"
										placeholder="Nombre del representante legal"
										ng-model="contribuyente.nombreRepresentanteLegal"
										autocomplete="off" maxlength="100" autocomplete="off"
										ng-required="requeridoMoral" capitalize paste-trimed
										data-placement="top" style="color: black;">
									<p class="help-block"
										ng-show="formContribuyente.nombreRepresentanteLegal.$error.required">campo
										requerido</p>
								</div>
							</div>
						</div>
					</div>
		<div class="row">
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.rfcRepresentanteLegalApoderado.$invalid}">
					<label class="control-label" for="numeroNotario"> *RFC DEL
						REPRESENTANTE LEGAL O APODERADO <span
						ng-if="formContribuyente.rfcRepresentanteLegalApoderado.$dirty && formContribuyente.rfcRepresentanteLegalApoderado.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.rfcRepresentanteLegalApoderado.$invalid && !formContribuyente.rfcRepresentanteLegalApoderado.$pristine}">
						<input class="form-control" type="text"
							id="rfcRepresentanteLegalApoderado"
							name="rfcRepresentanteLegalApoderado"
							placeholder="RFC representante legal"
							ng-model="contribuyente.rfcRepresentanteLegalApoderado"
							autocomplete="off" maxlength="100" autocomplete="off"
							ng-required="requeridoMoral" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.rfcRepresentanteLegalApoderado.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.numeroEscrituraPublicaFacultades.$invalid}">
					<label class="control-label" for="numeroEscrituraPublicaFacultades">
						*N�MERO DE LA ESCRITURA P�BLICA (DONDE SE OBSERVA LAS FACULTADES
						DEL REPRESENTANTE LEGAL/APODERADO LEGAL <span
						ng-if="formContribuyente.numeroEscrituraPublicaFacultades.$dirty && formContribuyente.numeroEscrituraPublicaFacultades.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.numeroEscrituraPublicaFacultades.$invalid && !formContribuyente.numeroEscrituraPublicaFacultades.$pristine}">
						<input class="form-control" type="text"
							id="numeroEscrituraPublicaFacultades"
							name="numeroEscrituraPublicaFacultades"
							placeholder="N�mero de Escritura Publica Facultades"
							ng-model="contribuyente.numeroEscrituraPublicaFacultades"
							autocomplete="off" maxlength="100" autocomplete="off"
							ng-required="requeridoMoral" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.numeroEscrituraPublicaFacultades.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.nombreNotarioTestimonio.$invalid}">
					<label class="control-label" for="nombreNotarioTestimonio">
						*NOMBRE DEL NOTARIO DEL TESTIMONIO DONDE SE OBSERVA LAS FACULTADES
						DEL REPRESENTANTE LEGAL/APODERADO LEGAL <span
						ng-if="formContribuyente.nombreNotarioTestimonio.$dirty && formContribuyente.nombreNotarioTestimonio.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.nombreNotarioTestimonio.$invalid && !formContribuyente.nombreNotarioTestimonio.$pristine}">
						<input class="form-control" type="text"
							id="nombreNotarioTestimonio" name="nombreNotarioTestimonio"
							placeholder="Nombre del Notario del testimonio"
							ng-model="contribuyente.nombreNotarioTestimonio"
							autocomplete="off" maxlength="100" autocomplete="off"
							ng-required="requeridoMoral" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.nombreNotarioTestimonio.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='M'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.numeroNotarioTestimonio.$invalid}">
					<label class="control-label" for="numeroNotarioTestimonio">
						*N�MERO DEL NOTARIO P�BLICO DONDE SE OBSERVA LAS FACULTADES DEL
						REPRESENTANTE <span
						ng-if="formContribuyente.numeroNotarioTestimonio.$dirty && formContribuyente.numeroNotarioTestimonio.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.numeroNotarioTestimonio.$invalid && !formContribuyente.numeroNotarioTestimonio.$pristine}">
						<input class="form-control" type="text"
							id="numeroNotarioTestimonio" name="numeroNotarioTestimonio"
							placeholder="N�mero del notario p�blico donde se observa las facultades"
							ng-model="contribuyente.numeroNotarioTestimonio"
							autocomplete="off" maxlength="100" autocomplete="off"
							ng-required="requeridoMoral" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.numeroNotarioTestimonio.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='F'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.ineIfe.$invalid}">
					<label class="control-label" for="ineIfe"> *INE/IFE <span
						ng-if="formContribuyente.ineIfe.$dirty && formContribuyente.ineIfe.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.ineIfe.$invalid && !formContribuyente.ineIfe.$pristine}">
						<input class="form-control" type="text" id="ineIfe" name="ineIfe"
							placeholder="INE" ng-model="contribuyente.ineIfe"
							autocomplete="off" maxlength="100" autocomplete="off"
							ng-required="requeridoFisico" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.ineIfe.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='F'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.folioIne.$invalid}">
					<label class="control-label" for="folioIne"> *FOLIO INE/IFE
						<span
						ng-if="formContribuyente.folioIne.$dirty && formContribuyente.folioIne.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.folioIne.$invalid && !formContribuyente.folioIne.$pristine}">
						<input class="form-control" type="text" id="folioIne"
							name="folioIne" placeholder="Folio Ine"
							ng-model="contribuyente.folioIne" autocomplete="off"
							maxlength="100" autocomplete="off" ng-required="requeridoFisico"
							capitalize paste-trimed data-placement="top"
							style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.folioIne.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6">
				<div ng-show="tipoRfc=='F'" class="form-group"
					ng-class="{ 'has-error' : formContribuyente.edad.$invalid}">
					<label class="control-label" for="edad"> *EDAD <span
						ng-if="formContribuyente.edad.$dirty && formContribuyente.edad.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.edad.$invalid && !formContribuyente.edad.$pristine}">
						<input class="form-control" type="text" id="edad" name="edad"
							placeholder="EDAD" ng-model="contribuyente.edad"
							autocomplete="off" maxlength="100" autocomplete="off"
							ng-required="requeridoFisico" capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.edad.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.telefono.$invalid}">
					<label class="control-label" for="telefono"> *Tel�fono<span
						ng-if="formContribuyente.telefono.$dirty && formContribuyente.telefono.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.telefono.$invalid && !formContribuyente.telefono.$pristine}">
						<input class="form-control" type="text" id="telefono"
							name="telefono" placeholder="Tel�fono"
							ng-model="contribuyente.telefono" autocomplete="off"
							maxlength="15" required capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.telefono.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.correo.$invalid}">
					<label class="control-label" for="correo"> *CORREO
						ELECTRONICO: <span
						ng-if="formContribuyente.correo.$dirty && formContribuyente.correo.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.correo.$invalid && !formContribuyente.correo.$pristine}">
						<input class="form-control" type="text" id="correo" name="correo"
							placeholder="Correo Electronico" ng-model="contribuyente.correo"
							autocomplete="off" maxlength="100" autocomplete="off" required
							capitalize paste-trimed data-placement="top"
							style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.correo.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group"
					ng-class="{ 'has-error' : formContribuyente.nombrePersonaContacto.$invalid}">
					<label class="control-label" for="telefono"> *NOMBRE DE
						PERSONA DE CONTACTO<span
						ng-if="formContribuyente.nombrePersonaContacto.$dirty && formContribuyente.nombrePersonaContacto.$invalid"
						style="color: #a94442"></span>:
					</label>

					<div
						ng-class="{ 'has-error' : formContribuyente.nombrePersonaContacto.$invalid && !formContribuyente.nombrePersonaContacto.$pristine}">
						<input class="form-control" type="text" id="nombrePersonaContacto"
							name="nombrePersonaContacto"
							placeholder="Nombre Persona Contacto"
							ng-model="contribuyente.nombrePersonaContacto" autocomplete="off"
							maxlength="100" required capitalize paste-trimed
							data-placement="top" style="color: black;">
						<p class="help-block"
							ng-show="formContribuyente.nombrePersonaContacto.$error.required">campo
							requerido</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
					<div class="col-xs-6">
							<div class="form-group" ng-show="tipoRfc=='M'"
								ng-class="{ 'has-error' : formContribuyente.cargoPersonaContacto.$invalid}">
								<label class="control-label" for="cargoPersonaContacto"> *CARGO DE LA PERSONA DE CONTACTO<span
									ng-if="formContribuyente.cargoPersonaContacto.$dirty && formContribuyente.cargoPersonaContacto.$invalid"
									style="color: #a94442"></span>:
								</label>

								<div
									ng-class="{ 'has-error' : formContribuyente.cargoPersonaContacto.$invalid && !formContribuyente.cargoPersonaContacto.$pristine}">
									<input class="form-control" type="text"
										id="cargoPersonaContacto" name="cargoPersonaContacto"
										placeholder="Cargo Persona Contacto"
										ng-model="contribuyente.cargoPersonaContacto"
										autocomplete="off" maxlength="100" ng-required="requeridoMoral" capitalize
										paste-trimed data-placement="top" style="color: black;">
									<p class="help-block"
										ng-show="formContribuyente.cargoPersonaContacto.$error.required">campo
										requerido</p>
								</div>
							</div>
						</div>
					
					</div>
<div class="row">
					<h6>
						<strong>Certificados de tipo FIEL para poder realizar el firmado del contrato </strong>
					</h6>
					<div class="col-xs-6">
					<div class="form-group"
								ng-class="{ 'has-error' : formContribuyente.cveFiel.$invalid}">
								<label class="control-label" for="cveFiel">* Clave Fiel:</label>
								<div>
									<input class="form-control" type="password" id="cveFiel" name="cveFiel"
										placeholder="Clave Fiel" ng-model="contribuyente.claveFiel"
										autocomplete="off" ng-maxlength="300" maxlength="301"
										required="required">
									<p ng-show="formContribuyente.cveFiel.$error.required"
										class="help-block">Clave es requerido.</p>
									<div class="help-block"
										ng-show="formContribuyente.cveFiel.$error.maxlength">La
										clave sobre pasa los 300 caracteres</div>
								</div>
							</div>
					</div>
							<div class="col-xs-6"></div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="control-label" for="fileCerFiel">* Seleccione
									Archivo FIEL Cer:</label> <input id="fileCerFiel" name="fileCerFiel" type="file"
									class="file" 
									ng-model="fielCer" accept=".cer" valid-file>
							
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="control-label" for="fileKeyFiel">* Seleccione
									Archivo FIEL Key:</label> <input id="fileKeyFiel" name="fileKeyFiel" type="file"
									class="file" 
									ng-model="fielKey" accept=".key" >

								
							</div>
						</div>
					</div>
					<div class="row">
					<div class="col-xs-4">
					<button type="button" class="btn btn-primary btn-lg btn-block" ng-click="descargaContrato()" ng-disabled="formContribuyente.$invalid">Contrato</button>
					</div>
					<div class="col-xs-4">
					<button type="button" class="btn btn-primary btn-lg btn-block"  ng-click="descargaConvenio()" ng-disabled="formContribuyente.$invalid">Convenio</button>
					</div>
					<div class="col-xs-4">
					<button type="button" class="btn btn-primary btn-lg btn-block"  ng-click="descargaManifiesto()" ng-disabled="formContribuyente.$invalid">Manifiesto</button>
					</div>
					</div>
					
	</form>
	<script type="text/javascript">
			$.datetimepicker.setLocale('es');
			$('#fechaActa').datetimepicker({
				format : 'Y-m-d H:i'
			});
			$('#fechaFolioMercantil').datetimepicker({
				format : 'Y-m-d H:i'
			});
		
		</script>
	<div class="modal-footer">
		<div class="row">
			<div class="form-actions floatRight">
				<button type="button" ng-click="limpiar()"
					class="btn btn-warning btn-sm btn-warning"
					ng-disabled="formContribuyente.$pristine">Limpiar</button>
				<input type="submit"
					value="{{!contribuyente.id ? 'Guardar' : 'Actualizar'}}"
					class="btn btn-success btn-sm btn-ok"
					ng-click="actualizarCer ? actualizar() : actualizarSinCer()"
					ng-disabled="formContribuyente.$invalid">
			</div>
		</div>
	</div>

</div>