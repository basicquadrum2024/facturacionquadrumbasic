<style>
.content-body {
	color:black;
}
</style>
<div id="{{idModalDos}}" class="modal fade content-body">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{header}}</h4>
			</div>
			<div class="modal-body">
				<div align="center">
					<label class="control-label">Datos del Emisor</label>
				</div>
				<h6>{{datosEmisor}}</h6>
				<div class="form-group">
					<div align="center">
						<label class="control-label">Datos del Receptor</label>
					</div>
					<div>
						<table class="table table-condensed table-striped table-hover">
							<tr>
								<td><strong><label>R.F.C:</label></strong></td>
								<td>{{receptor.rfc}}</td>
								<td><strong><label>Municipio o Delegaci�n:</label></strong></td>
								<td>{{receptor.domicilioReceptor.municipio}}</td>
							</tr>
							<tr>
								<td><strong><label>Nombre o raz�n Social:</label></strong></td>
								<td>{{receptor.razonSocial}}</td>
								<td><strong><label>C�digo Postal:</label></strong></td>
								<td>{{receptor.domicilioReceptor.cp}}</td>
							</tr>
							<tr>
								<td><strong><label>Calle:</label></strong></td>
								<td>{{receptor.domicilioReceptor.calle}}</td>
								<td><strong><label>Referencia:</label></strong></td>
								<td>{{receptor.domicilioReceptor.referencia}}</td>
							</tr>
							<tr>
								<td><strong><label>No. Exterior:</label></strong></td>
								<td>{{receptor.domicilioReceptor.numeroExt}}</td>
								<td><strong><label>Estado:</label></strong></td>
								<td>{{receptor.domicilioReceptor.estado}}</td>
							</tr>
							<tr>
								<td><strong><label>No. Interior:</label></strong></td>
								<td>{{receptor.domicilioReceptor.numeroInt}}</td>
								<td><strong><label>Pa�s:</label></strong></td>
								<td>{{receptor.domicilioReceptor.pais}}</td>
							</tr>
							<tr>
								<td><strong><label>Localidad:</label></strong></td>
								<td>{{receptor.domicilioReceptor.localidad}}</td>
								<td><strong><label></label></strong></td>
								<td></td>
							</tr>
							<tr>
								<td><strong><label>Colonia:</label></strong></td>
								<td>{{receptor.domicilioReceptor.colonia}}</td>
								<td><strong><label></label></strong></td>
								<td></td>
							</tr>

						</table>
					</div>
				</div>

				<table class="table table-striped table-responsive">
					<thead>
						<tr>
							<th>Cantidad</th>
							<th>Valor unitario</th>
							<th>Descripci�n</th>
							
						</tr>
					</thead>
					<tbody>
						<tr >
							<td>1</td>
							<td>NA</td>
							<td>{{parcialidades.numeroParcialidadPagar}} Parcialidades de {{parcialidades.totalParcialidadesPagar}}</td>
							
						</tr>
					</tbody>
				</table>
							
						<table class="table table-condensed table-striped table-hover">
							<tr>
								
							</tr>
							<tr>
								
								<td><strong><label>IVA Traslado:</label></strong></td>
								<td>{{parcialidades.comprobante.impuesto}}</td>
							</tr>
							<tr>
								
								<td><strong><label>Subtotal:</label></strong></td>
								<td>{{parcialidades.comprobante.subtotal}}</td>
							</tr>
							<tr>
								<td><strong><label>Total:</label></strong></td>
								<td>{{formap}}</td>
								
							</tr>
							
							
							

						</table>
					
			</div>
			<div class="modal-footer">
				<h4 class="text-warning"></h4>
				<button type="button" class="btn btn-success btn-ok"
					data-ng-click="callbackbuttonright();" data-dismiss="modal">Aceptar</button>
				<button type="button" class="btn btn-danger btn-warning" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>