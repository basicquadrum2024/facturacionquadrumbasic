<div class="content-body-buzon">

	<h1 class="text-center">Buz&oacute;n de sugerencias y quejas</h1>

	<form id="formBuzon" name="formBuzon" ng-submit="enviar(buzon)">
		<br>
		<div class="row">
			<h6>Los campos con * son requeridos</h6>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<label class="control-label"> *Tipo de comunicaci&oacute;n:
				</label>
			</div>
		</div>
		<div class="row">
			<div class="form-group">

				<div class="col-xs-12 col-sm-6 col-md-6">
					<label class="radio-inline"><input type="radio" value="1"
						ng-model="buzon.tipo">Sugerencia</label>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<label class="radio-inline"><input type="radio" value="2"
						ng-model="buzon.tipo">Queja</label>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="form-group">
					<label class="control-label" for="asunto"> *Asunto:</label>
					<div>
						<input class="form-control" type="text" id="asunto" name="asunto"
							placeholder="Asunto" ng-model="buzon.asunto" maxlength="20"
							autocomplete="off" required data-placement="top"
							style="color: black;">
						<p class="help-block" ng-show="formBuzon.asunto.$error.required">Asunto
							es requerido</p>
						<p class="help-block" ng-show="formBuzon.asunto.$error.maxlength">N&uacute;mero
							de caracteres excedido, m&aacute;ximo 20</p>
					</div>

				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="form-group">
					<label class="control-label" for="mensaje">*Mensaje:</label>
					<textarea id="mensaje" name="mensaje" class="form-control" rows="5"
						required ng-model="buzon.mensaje" maxlength="200"></textarea>
					<p class="help-block" ng-show="formBuzon.mensaje.$error.required">Mensaje
						es requerido</p>
					<p class="help-block" ng-show="formBuzon.asunto.$error.maxlength">N&uacute;mero
						de caracteres excedido, m&aacute;ximo 200</p>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4>Indique sus datos si desea que nos comuniquemos con usted</h4>
			</div>
		</div>
		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="form-group">
					<label class="control-label" for="tel"> Tel&eacute;fono:</label>
					<div>
						<input class="form-control" type="text" id="tel" name="tel"
							placeholder="Tel&eacute;fono" ng-model="buzon.telefono"
							maxlength="10" autocomplete="off" data-placement="top"
							style="color: black;" pattern="[0-9]{10}"
							title="Ingresa un n&uacute;mero de tel&eacute;fono a 10 d&iacute;gitos">
							<p class="help-block" ng-show="formBuzon.tel.$error.pattern">Ingresa un n&uacute;mero de tel&eacute;fono a 10 d&iacute;gitos</p>
					</div>

				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="form-group">
					<label class="control-label" for="correo"> Email:</label>
					<div>
						<input class="form-control" type="text" id="correo" name="correo"
							placeholder="Correo" ng-model="buzon.correo" autocomplete="off"
							data-placement="top" style="color: black;"
							pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"
							title="Ingresa un correo valido">
							<p class="help-block" ng-show="formBuzon.correo.$error.pattern">Ingresa un correo valido</p>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="text-center">
			<input type="submit" class="btn btn-primary btn-ok"  value="Enviar">
				<!-- <button id="enviar" type="button" class="btn btn-primary btn-ok"
					ng-click="enviar(buzon)" ng-disabled="formBuzon.$invalid">
					Enviar</button> -->
			</div>
		</div>

	</form>
</div>