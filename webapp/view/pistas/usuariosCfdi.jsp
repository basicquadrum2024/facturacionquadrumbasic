<div class="container">
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-11">
		<div class="panel with-nav-tabs panel-default">
			<div class="panel-heading">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab20default" data-toggle="tab"
						ng-click="limpiar(0)">Desbloqueo de Cuenta Inactiva por
							m&#225;s de 30 d�as</a></li>
					<li><a href="#tab21default" data-toggle="tab"
						ng-click="limpiar(0)">Desbloqueo de Inicio de Sesi&#243;n</a></li>
					<li><a href="#tab22default" data-toggle="tab"
						ng-click="limpiar(0)">Datos de Contacto del Contribuyente</a></li>
					<li><a href="#tab23default" data-toggle="tab"
						ng-click="clientesRegistrados(0)">Contribuyentes Registrados</a></li>
				</ul>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane fade in active" id="tab20default"><%@ include
							file="inactividad30.jsp"%></div>

					<div class="tab-pane fade" id="tab21default"><%@ include
							file="desbloqueo.jsp"%></div>

					<div class="tab-pane fade" id="tab22default"><%@ include
							file="datosContribuyente.jsp"%></div>

					<div class="tab-pane fade" id="tab23default"><%@ include
							file="contribuyentesRegistrados.jsp"%></div>
				</div>
			</div>
		</div>
	</div>
</div>
