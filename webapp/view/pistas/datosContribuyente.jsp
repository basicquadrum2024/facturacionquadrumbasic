<form id="formDatosContribuyente">
	* Datos requeridos
	<div class="form-group row">
		<label class="col-sm-2 col-form-label">RFC del Emisor </label>
		<div class="col-sm-10">
			<input required="required" ng-model="usuario.rfce" capitalize
				class="form-control" ng-blur="obtenerContacto('consulta')" />
		</div>
		<p ng-show="formDatosContribuyente.rfc.$error.required"
			class="help-block">Campo requerido.</p>
	</div>

	<div class="form-group row">
		<label for="inputtext" class="col-sm-2 col-form-label">Nombre</label>
		<div class="col-sm-10">
			<input required="required" class="form-control"
				ng-model="usuario.nombre" />
		</div>
	</div>

	<div class="form-group row">
		<label for="inputtext" class="col-sm-2 col-form-label">Apellido
			Paterno </label>
		<div class="col-sm-10">
			<input type="text" class="form-control" ng-model="usuario.apPaterno">
		</div>
	</div>

	<div class="form-group row">
		<label for="inputtext" class="col-sm-2 col-form-label">Apellido
			Materno </label>
		<div class="col-sm-10">
			<input type="text" class="form-control" ng-model="usuario.apMaterno">
		</div>
	</div>

	<div class="form-group row">
		<label for="inputtext" class="col-sm-2 col-form-label">Tel�fono
		</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" ng-model="usuario.telFijo">
		</div>
	</div>

	<div class="form-group row">
		<label for="inputtext" class="col-sm-2 col-form-label">Celular
		</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" ng-model="usuario.telCel">
		</div>
	</div>

	<div class="form-group row">
		<label for="inputtext" class="col-sm-2 col-form-label">Correo
			electr�nico </label>
		<div class="col-sm-10">
			<input type="text" class="form-control" ng-model="usuario.correo">
		</div>
	</div>

</form>