<div class="container content-body">
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-12">
		<div class="panel with-nav-tabs panel-default">
			<div class="panel-heading">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1default" data-toggle="tab"
						ng-click="limpiar(1);">Monitoreo y Transacciones en la
							aplicaci&#243;n eQuadrumCFDI</a></li>
					<li><a href="#tab2default" data-toggle="tab"
						ng-click="limpiar(1);">Transacciones en la aplicaci&#243;n
							Cliente CFDI-Basica</a></li>
					<li><a href="#tab3default" data-toggle="tab"
						ng-click="limpiar(0);">Usuarios CFDI-Basica</a></li>
				</ul>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane fade in active" id="tab1default"><%@ include
							file="pistaCliente.jsp"%></div>
					<div class="tab-pane fade" id="tab2default"><%@ include
							file="aplicaionQuadrum.jsp"%></div>
					<div class="tab-pane fade" id="tab3default"><%@ include
							file="usuariosCfdi.jsp"%></div>
				</div>
			</div>
		</div>
		<div class="table-responsive" ng-show="mostrar == 1">
			<div class="tab-content">
				<div class='contenido-buscar table-responsive'
					style="overflow: auto;">
					<%@ include file="gridPagination.jsp"%>
				</div>
			</div>
		</div>
	</div>
</div>
