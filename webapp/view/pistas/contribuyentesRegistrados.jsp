<form class="form-inline" name="contactos">
	<div class="container">
		<button class="btn btn-second glyphicon-download"
			id="descargaPistasClientes" type="submit"
			ng-click="descargaClientesRegistrados('descargaClientesRegistrados.json')">Descargar
			Excel</button>
	</div>

	<div class="table-responsive">
		<table class="table">
			<tr>
				<th>RFC</th>
				<th>RAZON SOCIAL</th>
				<th>REGIMEN FISCAL</th>
				<th>CALLE</th>
				<th>NO. INTERIOR</th>
				<th>NO EXTERIOR</th>
				<th>COLONIA</th>
				<th>MUNICIPIO</th>
				<th>LOCALIDAD</th>
				<th>REFERENCIA</th>
				<th>ESTADO</th>
				<th>PAIS</th>
				<th>CODIGO POSTAL</th>
			</tr>
			<tbody>
				<tr ng-repeat="emisor in listaEmisores" ng-class-odd="odd"
					ng-class-even="even">
					<td>{{emisor.rfc}}</td>
					<td>{{emisor.nombre}}</td>
					<td>{{emisor.regimenFiscal}}</td>
					<td>{{emisor.tcDomicilio.calle}}</td>
					<td>{{emisor.tcDomicilio.noInterior}}</td>
					<td>{{emisor.tcDomicilio.noExterior}}</td>
					<td>{{emisor.tcDomicilio.colonia}}</td>
					<td>{{emisor.tcDomicilio.municipio}}</td>
					<td>{{emisor.tcDomicilio.localidad}}</td>
					<td>{{emisor.tcDomicilio.referencia}}</td>
					<td>{{emisor.tcDomicilio.estado}}</td>
					<td>{{emisor.tcDomicilio.pais}}</td>
					<td>{{emisor.tcDomicilio.codigoPostal}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</form>