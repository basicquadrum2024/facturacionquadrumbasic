<style>
	*
	{
		color:black !important;
	}
</style>
<form class="form-inline" name="formularioFechaBuscar">
	<div class="form-group">
		<label class="mr-sm-2 control-label contenido-buscar"> Fecha
			Inicio: </label> <input class="form-control" type="text" id="fechahi"
			name="fechahi" placeholder="Fecha de inicio" required="required"
			ng-model="fechaInicio"
			validar="'fechaInicio'"
			data-tooltip="La fecha debe tener  1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora hh:mm, Ejemplo: 2016/06/07 00:00"
			autocomplete="off">

		<div ng-show="formularioFechaBuscar.fechahi.$error.validar "
			class="alert alert-danger">Fecha de Inicio incorrecta la fecha
			debe tener 1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora
			hh:mm</div>
		<p ng-show="formularioFechaBuscar.fechahi.$error.required"
			class="help-block">Fecha de Inicio es requerida.</p>
	</div>
	<div class="form-group">
		<label class="mr-sm-2 control-label contenido-buscar"> Fecha
			Final: </label> <input class="form-control" type="text" id="fechahf"
			name="fechahf" placeholder="Fecha de Fin" required="required"
			ng-model="fechaFinal"
			validar="'fechaInicio'"
			data-tooltip="La fecha debe tener  1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora hh:mm, Ejemplo: 2016/06/07 00:00"
			autocomplete="off">
		<div ng-show="formularioFechaBuscar.fechahf.$error.validar "
			class="alert alert-danger">Fecha de Fin incorrecta la fecha
			debe tener 1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora
			hh:mm</div>
		<p ng-show="formularioFechaBuscar.fechahf.$error.required"
			class="help-block">Fecha de Fin es requerida.</p>
	</div>

	<div class="form-group">
		<button class="btn btn-second glyphicon-download"
			style="visibility: hidden;" id="descargaPistasClientes" type="submit"
			ng-disabled="formularioFechaBuscar.$invalid"
			ng-click="descargaPistasClientes('descargaPistasAdmin.json')">Descargar
			Excel</button>

		<button class="btn btn-primary btn-ok" type="submit"
			ng-disabled="formularioFechaBuscar.$invalid" ng-click="buscaPistasAdmin()">Buscar</button>
	</div>
</form>
<script>
    $(function( ) {
        jQuery.datetimepicker.setLocale('es');
        $("#fechahi").datetimepicker({

            dateFormat : 'yy-mm-dd',
            timeFormat : 'HH:mm',

            maxDate : ' +1d',

        });

        jQuery.datetimepicker.setLocale('es');
        $("#fechahf").datetimepicker({

            dateFormat : 'yy-mm-dd',
            timeFormat : 'HH:mm',
            maxDate : ' +1d',

        });
    });
</script>