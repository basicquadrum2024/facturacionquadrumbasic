<form class="form-inline" name="formularioAdmin">
	<div class="row">
		<div class="col-md-3">
			<div>
				<label class="control-label contenido-buscar" for="fechahi">
					Fecha Inicio: </label>
				<div>
					<input class="form-control" type="text" id="adminFechaInicio"
						name="adminFechaInicio" placeholder="Fecha de inicio"
						required="required" ng-model="fechaInicio"
						validar="'fechaInicio'"
						data-tooltip="La fecha debe tener  1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora hh:mm, Ejemplo: 2016/06/07 00:00"
						autocomplete="off">
					<div ng-show="formularioAdmin.fechahi.$error.validar "
						class="alert alert-danger">Fecha de Inicio incorrecta la
						fecha debe tener 1.- Formato yyyy/mm/dd 2.- Un espacio en blanco
						3.- hora hh:mm</div>
					<p ng-show="formularioAdmin.adminFechaInicio.$error.required"
						class="help-block">Fecha de Inicio es requerida.</p>
				</div>

				<div>
					<label class="control-label contenido-buscar" for="rfcBuscar">
						Fecha Final: </label>
					<div>
						<input class="form-control" type="text" id="adminFechaFin"
							name="adminFechaFin" placeholder="Fecha de Fin"
							required="required" ng-model="fechaFinal"
							validar="'fechaInicio'"
							data-tooltip="La fecha debe tener  1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora hh:mm, Ejemplo: 2016/06/07 00:00"
							autocomplete="off">
						<div ng-show="formularioAdmin.fechahf.$error.validar "
							class="alert alert-danger">Fecha de Fin incorrecta la fecha
							debe tener 1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.-
							hora hh:mm</div>
						<p ng-show="formularioAdmin.adminFechaFin.$error.required"
							class="help-block">Fecha de Fin es requerida.</p>
						<div></div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6 ">
			<div class="form-group">
				<label class="form-check-label"> Acciones: <input
					class="form-check-input" ng-model="accion" type="radio"
					name="inlineRadioOptions" required="required" id="inlineRadio1"
					value="TODOS"> TODOS
				</label>
			</div>
			<div class="form-group">
				<label class="form-check-label"> <input
					class="form-check-input" ng-model="accion" type="radio"
					name="inlineRadioOptions" required="required" id="inlineRadio1"
					value="SELECT"> SELECT
				</label>
			</div>
			<div class="form-group">
				<label class="form-check-label"> <input
					class="form-check-input" ng-model="accion" type="radio"
					name="inlineRadioOptions" required="required" id="inlineRadio1"
					value="INSERT"> INSERT
				</label>
			</div>
			<div class="form-group">
				<label class="form-check-label"> <input
					class="form-check-input" ng-model="accion" type="radio"
					name="inlineRadioOptions" required="required" id="inlineRadio1"
					value="DELETE"> DELETE
				</label>
			</div>
			<div class="form-group">
				<label class="form-check-label"> <input
					class="form-check-input" ng-model="accion" type="radio"
					name="inlineRadioOptions" required="required" id="inlineRadio1"
					value="ERROR"> ERROR
				</label>
			</div>
		</div>
		<div class="col-md-3 ">
			<div class="form-group">
				<label class="control-label contenido-buscar"> Entidad: </label> <select
					required="required" ng-model="entidad" class="form-control"
					ng-options="o as o for o in tablaEntidad"></select>
			</div>
			<div class="form-group">
				<button class="btn btn-primary btn-ok" type="submit"
					ng-disabled="formularioAdmin.$invalid"
					ng-click="buscaPistasAdminSAT()">Buscar</button>

				<button class="btn btn-second glyphicon-download btn-ok"
					style="visibility: hidden;" id="descargaPistasAdmin" type="submit"
					ng-disabled="formularioAdmin.$invalid"
					ng-click="descargaPistasAdmin('descargaPistasCliente.json')">Descargar
					Excel</button>
			</div>
		</div>
	</div>
</form>
<script>
    $(function( ) {
        jQuery.datetimepicker.setLocale('es');
        $("#adminFechaInicio").datetimepicker({

            dateFormat : 'yy-mm-dd',
            timeFormat : 'HH:mm',

            maxDate : ' +1d',

        });

        jQuery.datetimepicker.setLocale('es');
        $("#adminFechaFin").datetimepicker({

            dateFormat : 'yy-mm-dd',
            timeFormat : 'HH:mm',
            maxDate : ' +1d',

        });
    });
</script>
