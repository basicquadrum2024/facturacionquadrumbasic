<form class="form-inline" name="rfmInactividad">
	
	<label class="mr-sm-2" for="inlineFormCustomSelect">* RFC de
		Emisor: </label> <input id="rfc" name="rfc" type="text" class="form-control"
		placeholder="RFC de Contacto" required="required" capitalize
		ng-model="usuario.rfce" ng-blur="obtenerContacto('bloqueo')">

	<p ng-show="rfmInactividad.rfc.$error.required" class="help-block">Campo
		requerido.</p>
		
	<div ng-show="usuario.nombre != '' ">	
		<span ng-if="usuario.sesion < 3 " class="animate-if">
		  Usuario {{usuario.rfce}} esta Activo.
		</span>
		
		<span ng-if="usuario.sesion > 3 " class="animate-if">
		  Usuario {{usuario.rfce}} esta Inactivo.
		</span>
	</div>

	<button type="submit" class="btn btn-primary" id="activarContacto" ng-show="usuario.sesion > 3"
		style="visibility: hidden;" ng-click="activaCuenta()">Activar</button>
</form>