<style>
.content-body {
	color:black;
}

</style>
<div class="content-body-comprobante-busqueda content-body">
<h3 class="text-center" style="color:white;">{{mensaje}}</h3>
<!-- <ol class="breadcrumb header">
	<li class="active">{{mensaje}}</li>
</ol> -->
<br>
<div class="row">
<div class="form-group col-md-4">
		<label for="criterio" style="color:white; text-align: left;"  >Elige un criterio de b�squeda:</label>
		<select name="criterio" id="criterio"
			ng-options="option.value for option in listaCriterios track by option.id"
			ng-model="criterio" class="form-control" ng-change="cambio()"></select>
	</div>
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
	<br>
	<button id="consulta" type="button" class="btn btn-primary btn-ok" ng-click="buscarCriterios()">
					Buscar <span class="glyphicon glyphicon-search " aria-hidden="true"></span>
					</button>
	
		<!-- <input id="consulta" type="button" class="btn btn-primary btn-ok"
			ng-click="buscarCriterios()" value="Buscar"> -->
	</div>
</div>

<br>
<div >
	<div ng-show="criterio.id==1">

		<form id="formFiscal" role="form"
			name="formFiscal">
			<div class="row  col-sm-12 col-md-4 col-lg-4">
			<div class="form-group">
				
					<label for="folio" style="color:white;">* Folio
						Fiscal:</label>
				
					<input id="fiscal" name="folio" class="form-control" type="text"
						maxlength="41" ng-maxlength="40" ng-model="uuid" required
						paste-trimed capitalize size="40">
					<p class="help-block" ng-show="formFiscal.folio.$error.required">Campo
						obligatorio</p>
					<div class="help-block" ng-show="formFiscal.folio.$error.maxlength">El
						Folio Fiscal sobre pasa los 40 caracteres</div>
				
			</div>
			</div>
			<div style="display: none;">{{formFiscalValido=formFiscal.$valid}}</div>
		</form>
	</div>

	<div ng-show="criterio.id==2">

		<form id="formReceptor"  role="form"
			name="formReceptor">
			<div class="row  col-sm-12 col-md-4 col-lg-4">
			<div class="form-group">
					<label for="rfcReceptor"  style="color:white; text-align: right;">*
						R.F.C de receptor:</label>
				
					<input id="rfcReceptor" name="rfcReceptor" class="form-control"
						maxlength="13" type="text" ng-model="rfcReceptor"
						ng-blur="buscarReceptorRfc()" required paste-trimed capitalize>
					<p class="help-block"
						ng-show="formReceptor.rfcReceptor.$error.required">Campo
						obligatorio</p>
				</div>
			</div>
			<div style="display: none;">{{formReceptorValido=formReceptor.$valid}}</div>
		</form>
	</div>

	<div ng-show="criterio.id==3">

		<form  role="form" name="formEstatus">
		<div class="row  col-sm-12 col-md-4 col-lg-4">
			<div class="form-group">
				
					<label for="fiscal"  for="activo" style="color:white; text-align: right;">*
						Estatus:</label>
				<br>
					<label style="color:white; text-align: right;"> <input name="activo" type="radio"
						ng-model="estatus" value="1" required /> Activos
					</label ><br /> <label style="color:white; text-align: right;"> <input name="cancelado" type="radio"
						ng-model="estatus" value="0" required /> Cancelados
					</label>

				</div>
			</div>
			<div style="display: none;">{{formEstatusValido=formEstatus.$valid}}</div>
		</form>
	</div>

	<div ng-show="criterio.id==4">

		<form id="formFechas"  role="form"
			name="formFechas">
			<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">
				
					<label for="fechaInicio" class="control-label" style="color:white; text-align: right;">* Fecha
						Inicio:</label> <input id="fechaInicio" name="fechaInicio"
						class="form-control" type="text" ng-model="fechaInicio" required
						data-tooltip="La fecha debe tener  1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora hh:mm, Ejemplo: 2016/06/07 00:00">
				</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">
				
					<label for="fechaFin" class="control-label" style="color:white; text-align: right;">* Fecha Final:</label>
					<input id="fechaFin" name="fechaFin" class="form-control"
						type="text" ng-model="fechaFin" required
						data-tooltip="La fecha debe tener  1.- Formato yyyy/mm/dd 2.- Un espacio en blanco 3.- hora hh:mm, Ejemplo: 2016/06/07 00:00">
				</div>
			</div>
			</div>
			<div style="display: none;">{{formFechasValido=formFechas.$valid}}</div>
		</form>

		<script type="text/javascript">
			$.datetimepicker.setLocale('es');
			$('#fechaInicio').datetimepicker({
				format : 'Y-m-d H:i'
			});
			$('#fechaFin').datetimepicker({
				format : 'Y-m-d H:i'
			});
		</script>
	</div>

	<div ng-show="criterio.id==5">
		<form  role="form" name="formConfirmacion">
		<div class="row col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<div class="form-group">
				
					<label for="confirmacion" 
						for="activo" style="color:white; text-align: right;">* Clave confirmaci�n</label>
				

				
					<input id="confirmacion" name="confirmacion" class="form-control"
						type="text" ng-model="confirmacion" required
						validar="'confirmacion'" size="20">
					<div
					ng-show="formConfirmacion.confirmacion.$dirty && formConfirmacion.confirmacion.$invalid">
					<p class="help-block"
						ng-show="formConfirmacion.confirmacion.$error.required">Campo
						obligatorio</p>
					<p class="help-block"
						ng-show="formConfirmacion.confirmacion.$error.validar">El
						campo confirmaci�n debe de contener 5 caracteres.</p>
				</div>
				</div>
			</div>
			<div style="display: none;">{{formConfirmacionValido=formConfirmacion.$valid}}</div>
		</form>
	</div>
</div>


<div modal-reutilizablecancelacion id-modal-reutilizablec="idModalT"
	modal-body="bodyModalT" modal-footer="footerModalT"
	modal-header="headerModalT" resultado-wrappers="resultadoWrappers"></div>


<br>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"
		ng-show="listaEnvio.length>0">
		<a href class="btn btn-success btn-ok" ng-click="enviarE()">Enviar xml a
			mi correo</a>
	</div>
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"
		ng-show="listaEnvio.length>0">
		<a href class="btn btn-success btn-ok" ng-click="agregarEmails()">Enviar
			xml a varios correos Electr�nico</a>
	</div>
	

</div>

<br>

<div class="row">


	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"
		ng-show="listaCancelar.length>0">
		<label for="criterioCancelacion" style="color:white;">Motivo cancelacion:</label>
		<select name="criterioCancelacion" id="criterioCancelacion"
			ng-options='option.clave as (option.clave +"-"+ option.descripcion) for option in listaCriteriosCancelacion'
			
			ng-model="cancelacion.motivoCancelacion" class="form-control" ></select>
		</div>
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"
		ng-show="cancelacion.motivoCancelacion=='01'">
		<label for="folio" style="color:white;">*  Folio Fiscal que lo sustuir�:</label>
	   <input id="folioSustituto" name="folioSustituto" class="form-control" type="text"
						maxlength="41" ng-maxlength="40" ng-model="cancelacion.uuidSustituto" required
						paste-trimed capitalize size="40">
		</div>
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"
		ng-show="listaCancelar.length>0">
		<br>
		<a href class="btn btn-success btn-ok" ng-click="cancelar()">Cancelar
			comprobante</a>
	</div>
	<br>
</div>
<br>
<div class="row">
	<div class="gridStyle" ng-grid="gridOptions"></div>
</div>

<div id="modalCorreo" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" ng-click="close()"
					data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: black;">Envio por correo</h4>
			</div>
			<div class="modal-body">
				<p>El proceso para el envio de correos es el siguiente:</p>
				<ul>
					<li>1. Captura alg�n correo electr�nico en la casilla Correo
						electr�nico al terminar <strong> presiona Enter para agregar</strong>.</li>
					<li>2. Si deseas agregar m�s correos repite el paso 1.</li>
					<li>3. Dar click en el bot�n Enviar.</li>
				</ul>
				<form name="formu" class="form-horizontal" role="form">
					<div class="form-group">
						<label for="correo" class="col-sm-5 control-label"> Correo
							Electr�nico:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : formu.correo.$invalid && !formu.correo.$pristine}">
							<input type="text" class="form-control input-sm" id="correo"
								name="correo" placeholder="Correo electr�nico"
								autocomplete="off" ng-model="correo" paste-trimed
								validar="'correo'"
								ng-keypress="addCorreos($event)" style="color: black !important;" title="Para agregar correo presiona enter">
							<div ng-show="formu.correo.$dirty && formu.correo.$invalid">
								<p class="help-block"
									text-danger" ng-show="formu.correo.$error.validar">Correo
									inv�lido</p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<ul>
							<li ng-repeat="correo in Correos">{{correo}} <a href
								ng-click="deleteCorreo(correo)" style="color: red;">&nbsp;
									Eliminar</a></li>
						</ul>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<h5 ng-hide="Correos.length==0">Listo ya puedes enviar los
					archivos a los destino(s) que has ingresado click en el bot�n
					Enviar</h5>
				<button type="button" class="btn btn-danger btn-warning" data-dismiss="modal"
					ng-click="cancel()">Cerrar</button>
				<button type="button" class="btn btn-success btn-ok"
					ng-hide="Correos.length==0" ng-click="envioporEmail()"
					data-dismiss="modal">Enviar</button>
			</div>
		</div>
	</div>
</div>
</div>