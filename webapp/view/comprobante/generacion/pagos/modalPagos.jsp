<style type="text/css">
.modal {
	top: -100px;
}

.modal-body {
	margin: 5px 5px 5px 5px !important;
	text-align: left !important;
	max-height: calc(100vh - 212px);
	overflow-y: auto;
}
.content-body {
	color:black;
}
</style>

<div id="modalPagos" class="modal fade content-body" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
			{{title}}
			</div>
			<div class="modal-body">
				<form id="formPagos" name="formPagos" role="form">
					<div class="panel panel-primary">
						<div class="panel-heading">Datos del CFDI Origen / Documento Relacionado</div>
						<div class="panel-body">
							<div class="row">
								<div class="form-group col-lg-3">
									<label>RFC Receptor:</label>
									<p>{{cfdiReceptor.rfc}}</p>
								</div>
								<div class="form-group col-lg-3">
									<label>Folio Fiscal:</label>
									<p>{{cfdiPadre.uuid}}</p>
								</div>
								<div class="form-group col-lg-1">
									<label>Folio:</label>
									<p>{{cfdiPadre.folio}}</p>
								</div>
								<div class="form-group col-lg-2">
									<label>Serie:</label>
									<p>{{cfdiPadre.serie}}</p>
								</div>
								<div class="form-group col-lg-3">
									<label>Total General:</label>
									<p>{{cfdiPadre.total}}</p>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-lg-offset-2">
									<div class="form-group col-lg-2">
										<label>Total Pagado:</label>
										<p>{{cfdiPadre.totalPagado == null ? 0 : cfdiPadre.totalPagado}}</p>
									</div>
									<div class="form-group col-lg-2">
										<label>Total Restante:</label>
										<p>{{(cfdiPadre.total - cfdiPadre.totalPagado)}}</p>
									</div>
									<div class="form-group col-lg-2">
										<label>No. Parcialidades Realizadas:</label>
										<p>{{cfdiPadre.totalParcialidades}}</p>
									</div>
									<div class="form-group col-lg-2">
										<label>No. Parcialidad Actual:</label>
										<p>{{(cfdiPadre.totalParcialidades + 1)}}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-primary" ng-show="listaCfdiCancelados">
						<div class="panel-heading">CFDI Relacionado / CFDI's Cancelados Anteriormente</div>
						<div class="panel-body">
							<div class="row">
								<table class="table table-striped table-responsive table-bordered">
									<thead>
										<tr>
											<th>Seleccionar</th>
											<th>Folio Fiscal</th>
											<th>Total</th>
											<th>Serie</th>
											<th>Folio</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="cfdiCancelado in listaCfdiCancelados">
											<td ><input ng-model="uuidCfdiRelacionado.uuid" type="radio" name="radioSeleccionado" value="{{cfdiCancelado.uuid}}" /></td>
											<td >{{cfdiCancelado.uuid}}</td>
											<td scope="row">{{cfdiCancelado.total}}</td>
											<td scope="row">{{cfdiCancelado.serie}}</td>
											<td scope="row">{{cfdiCancelado.folio}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">Datos Generales De Recepci�n De Pagos</div>
						<div class="panel-body">
							<div class="row">
								<div class="form-group col-lg-4">
									<label class="control-label" for="formaPago">
										<span ng-class="{ 'error-asterisco' : formPagos.formaPago.$invalid && !formPagos.formaPago.$pristine}">*</span>  Forma de pago:</label>
									<div ng-class="{ 'has-error' : formPagos.formaPago.$invalid && !formPagos.formaPago.$pristine}">
										<select id="formaPago" name="formaPago" ng-model="pago.formaDePagoP" class="form-control" required ng-blur="validacionesFormaPago()"
											ng-options='formaPago.clave as (formaPago.clave +" "+formaPago.descripcion) for formaPago in listaFormaPago'>
											<option value="">-- Seleccione --</option>
											</select>
										<div ng-show="formPagos.formaPago.$dirty && formPagos.formaPago.$invalid">
											<p class="help-block" ng-show="formPagos.formaPago.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div>
								
								<div class="form-group col-lg-4">
									<label class="control-label" for="moneda">
										<span ng-class="{ 'error-asterisco' : formPagos.moneda.$invalid && !formPagos.moneda.$pristine}">*</span> Moneda:</label>
									<div ng-class="{ 'has-error' : formPagos.moneda.$invalid && !formPagos.moneda.$pristine}">
										<select id="moneda" name="moneda" ng-model="entidadMoneda" autofocus ng-blur="obtenerNumDecimales()" class="form-control" required
											ng-options='moneda as (moneda.clave+" "+moneda.descripcion) for moneda in listaMonedas'>
										</select>
										<div ng-show="formPagos.moneda.$dirty && formPagos.moneda.$invalid">
											<p class="help-block" ng-show="formPagos.moneda.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div>
								
								<div class="form-group col-lg-4">
									<label class="control-label" for="monto">
									<span ng-class="{ 'error-asterisco' : formPagos.monto.$invalid && !formPagos.monto.$pristine}">*</span> Monto:</label>
									<div ng-class="{ 'has-error' : formPagos.monto.$invalid && !formPagos.monto.$pristine}"><!-- ^{{expresionRegularMonto}}$ -->
										<input type="text" class="form-control" id="monto" name="monto" placeholder="Monto" required monto="numDecimales"
											ng-model="pago.monto" autocomplete="off" menor-que-total style="color: black;">
										<div ng-show="formPagos.monto.$invalid && !formPagos.monto.$pristine">
											<p class="help-block" ng-show="formPagos.monto.$error.monto">De tener opcionalmente 18 digitos y es requerido el n�mero de decimales que soporta la moneda: {{numDecimales}}</p>
											<p class="help-block" ng-show="formPagos.monto.$error.required">Campo requerido</p>
											<p class="help-block" ng-show="formPagos.monto.$error.menorquetotal">Debe ser menor o igual al total restante.</p>
										</div>
									</div>
								</div>
								
							</div>
							<div class="row">
								<!-- <div class="form-group col-lg-4">
									<label class="control-label" for="serie">
										<span ng-class="{ 'error-asterisco' : formPagos.serie.$invalid && !formPagos.serie.$pristine}">*</span> Serie:</label>
									<div ng-class="{ 'has-error' : formPagos.serie.$invalid && !formPagos.serie.$pristine}">
										<select id="serie" name="serie" ng-model="serie" autofocus class="form-control" required
											ng-options='serie.nombreSerie as serie.nombreSerie for serie in listaSeries'>
										</select>
										<div ng-show="formPagos.serie.$dirty && formPagos.serie.$invalid">
											<p class="help-block" ng-show="formPagos.serie.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div> -->
								
								<div class="form-group col-lg-4" ng-if="entidadMoneda.clave != 'MXN'">
									<label class="control-label" for="tipoCambio">
										<span ng-class="{ 'error-asterisco' : formPagos.tipoCambio.$invalid && !formPagos.tipoCambio.$pristine}">*
										</span>Tipo de cambio:</label>
									<div ng-class="{ 'has-error' : formPagos.tipoCambio.$invalid && !formPagos.tipoCambio.$pristine}">
										<input id="tipoCambio" name="tipoCambio" ng-model="pago.tipoCambioP" class="form-control" required validar="'tipoCambio'">
										<div ng-show="!formPagos.tipoCambio.$pristine && formPagos.tipoCambio.$invalid">
											<p class="help-block" ng-show="formPagos.tipoCambio.$error.required">Campo obligatorio</p>
											<p class="help-block" ng-show="formPagos.tipoCambio.$error.validar">Car�cter invalido.</p>
										</div>
									</div>
								</div>
							
								<div class="form-group col-lg-4">
									<label class="control-label" for="numOperacion">
										<span ng-class="{ 'error-asterisco' : formPagos.numOperacion.$invalid && !formPagos.numOperacion.$pristine}">
										</span>Num. Operaci�n:</label>
									<div ng-class="{ 'has-error' : formPagos.numOperacion.$invalid && !formPagos.numOperacion.$pristine}">
										<input id="numOperacion" name="numOperacion" ng-model="pago.numOperacion" class="form-control"  validar="'numOperacion'">
										<div ng-show="formPagos.numOperacion.$dirty && formPagos.numOperacion.$invalid">
											<p class="help-block" ng-show="formPagos.numOperacion.$error.required">Campo obligatorio</p>
											<p class="help-block" ng-show="formPagos.numOperacion.$error.validar">Car�cter invalido.</p>
										</div>
									</div>
								</div>
							
							</div>
							<hr>
							
							<div class="row">
								<div class="form-group col-lg-4" ng-if="rfcEmisorCtaOrd">
									<label class="control-label" for="rfcEmisorCtaOrd">
										<span ng-class="{ 'error-asterisco' : formPagos.rfcEmisorCtaOrd.$invalid && !formPagos.rfcEmisorCtaOrd.$pristine}">
										</span>Rfc Emisor Cta. Ordenante:</label>
									<div ng-class="{ 'has-error' : formPagos.rfcEmisorCtaOrd.$invalid && !formPagos.rfcEmisorCtaOrd.$pristine}">
										<input id="rfcEmisorCtaOrd" name="rfcEmisorCtaOrd" ng-model="pago.rfcEmisorCtaOrd" class="form-control"  validar="'rfcEmisorCtaOrd'">
										<div ng-show="formPagos.rfcEmisorCtaOrd.$dirty && formPagos.rfcEmisorCtaOrd.$invalid">
											<p class="help-block" ng-show="formPagos.rfcEmisorCtaOrd.$error.required">Campo obligatorio</p>
											<p class="help-block" ng-show="formPagos.rfcEmisorCtaOrd.$error.validar">Car�cter invalido.</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="pago.rfcEmisorCtaOrd === 'XEXX010101000'">
									<label class="control-label" for="nomBancoOrdExt">
										<span ng-class="{ 'error-asterisco' : formPagos.nomBancoOrdExt.$invalid && !formPagos.nomBancoOrdExt.$pristine}">*
										</span> Nombre Del Banco Ordenante:</label>
									<div ng-class="{ 'has-error' : formPagos.nomBancoOrdExt.$invalid && !formPagos.nomBancoOrdExt.$pristine}">
										<input id="nomBancoOrdExt" name="nomBancoOrdExt" ng-model="pago.nomBancoOrdExt" class="form-control" required>
										<div ng-show="formPagos.nomBancoOrdExt.$dirty && formPagos.nomBancoOrdExt.$invalid">
											<p class="help-block" ng-show="formPagos.nomBancoOrdExt.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="ctaOrdenante">
									<label class="control-label" for="ctaOrdenante">
										<span ng-class="{ 'error-asterisco' : formPagos.ctaOrdenante.$invalid && !formPagos.ctaOrdenante.$pristine}">
										</span>Cuenta Ordenante:</label><!-- ^{{patronCtaOrd}}$ -->
									<div ng-class="{ 'has-error' : formPagos.ctaOrdenante.$invalid && !formPagos.ctaOrdenante.$pristine}"><!-- [0-9]{11}|[0-9]{18} -->
										<input id="ctaOrdenante" name="ctaOrdenante" ng-model="pago.ctaOrdenante" class="form-control"  pattern="^{{patronCtaOrd}}$">
										<div ng-show="formPagos.ctaOrdenante.$dirty && formPagos.ctaOrdenante.$invalid">
											<p class="help-block" ng-show="formPagos.ctaOrdenante.$error.required">Campo obligatorio</p>
											<p class="help-block" ng-show="formPagos.ctaOrdenante.$error.pattern">Patr�n inv�lido '{{patronCtaOrd}}'.</p>
										</div>
									</div>
								</div>
							</div>
							
							<hr>
							
							<div class="row">
								<div class="form-group col-lg-4" ng-if="rfcEmisorCtaBen">
									<label class="control-label" for="rfcEmisorCtaBen">
										<span ng-class="{ 'error-asterisco' : formPagos.rfcEmisorCtaBen.$invalid && !formPagos.rfcEmisorCtaBen.$pristine}">
										</span>Rfc Emisor Cuenta Beneficiario:</label>
									<div ng-class="{ 'has-error' : formPagos.rfcEmisorCtaBen.$invalid && !formPagos.rfcEmisorCtaBen.$pristine}">
										<input id="rfcEmisorCtaBen" name="rfcEmisorCtaBen" ng-model="pago.rfcEmisorCtaBen" class="form-control">
										<div ng-show="formPagos.rfcEmisorCtaBen.$dirty && formPagos.rfcEmisorCtaBen.$invalid">
											<p class="help-block" ng-show="formPagos.rfcEmisorCtaBen.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="ctaBeneficiario">
									<label class="control-label" for="ctaBeneficiario">
										<span ng-class="{ 'error-asterisco' : formPagos.ctaBeneficiario.$invalid && !formPagos.ctaBeneficiario.$pristine}">
										</span>Cuenta Beneficiario:</label>
									<div ng-class="{ 'has-error' : formPagos.ctaBeneficiario.$invalid && !formPagos.ctaBeneficiario.$pristine}">
										<input id="ctaBeneficiario" name="ctaBeneficiario" ng-model="pago.ctaBeneficiario" class="form-control"  pattern="^{{patronCtaBen}}$">
										<div ng-show="formPagos.ctaBeneficiario.$dirty && formPagos.ctaBeneficiario.$invalid">
											<p class="help-block" ng-show="formPagos.ctaBeneficiario.$error.required">Campo obligatorio</p>
											<p class="help-block" ng-show="formPagos.ctaBeneficiario.$error.pattern">Patr�n inv�lido '{{patronCtaBen}}'.</p>
										</div>
									</div>
								</div>
							</div>
							
							<hr>
							
							<div class="row">
								<div class="form-group col-lg-4" ng-if="tipoCadPago">
									<label class="control-label" for="tipoCadPago">
										<span ng-class="{ 'error-asterisco' : formPagos.tipoCadPago.$invalid && !formPagos.tipoCadPago.$pristine}">
										</span>Tipo De Cadena De Pago:</label>
									<div ng-class="{ 'has-error' : formPagos.tipoCadPago.$invalid && !formPagos.tipoCadPago.$pristine}">
										<select id="tipoCadPago" name="tipoCadPago" ng-model="pago.tipoCadPago" class="form-control" 
											ng-options='tipoCadPago.clave as (tipoCadPago.clave +" "+tipoCadPago.descripcion) for tipoCadPago in listaTipoCadPago'>
											<option value="">-- Seleccione --</option>
											</select>
										<div ng-show="formPagos.tipoCadPago.$dirty && formPagos.tipoCadPago.$invalid">
											<p class="help-block" ng-show="formPagos.tipoCadPago.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="tipoCadPago">
									<label class="control-label" for="certPago">
										<span ng-class="{ 'error-asterisco' : formPagos.certPago.$invalid && !formPagos.certPago.$pristine}">*
										</span> Certificado Que Corresponde Al Pago:</label>
									<div ng-class="{ 'has-error' : formPagos.certPago.$invalid && !formPagos.certPago.$pristine}">
										<input id="certPago" name="certPago" ng-model="pago.certPago" class="form-control"  >
										<div ng-show="formPagos.certPago.$dirty && formPagos.certPago.$invalid">
											<p class="help-block" ng-show="formPagos.certPago.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="tipoCadPago">
									<label class="control-label" for="cadPago">
										<span ng-class="{ 'error-asterisco' : formPagos.cadPago.$invalid && !formPagos.cadPago.$pristine}">*
										</span> Cadina Original Del Comprobante De Pago:</label>
									<div ng-class="{ 'has-error' : formPagos.cadPago.$invalid && !formPagos.cadPago.$pristine}">
										<input id="cadPago" name="cadPago" ng-model="pago.cadPago" class="form-control">
										<div ng-show="formPagos.cadPago.$dirty && formPagos.cadPago.$invalid">
											<p class="help-block" ng-show="formPagos.cadPago.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="tipoCadPago">
									<label class="control-label" for="selloPago">
										<span ng-class="{ 'error-asterisco' : formPagos.selloPago.$invalid && !formPagos.selloPago.$pristine}">*
										</span> Sello Digital Que Se Asocie Al Pago.:</label>
									<div ng-class="{ 'has-error' : formPagos.selloPago.$invalid && !formPagos.selloPago.$pristine}">
										<input id="selloPago" name="selloPago" ng-model="pago.selloPago" class="form-control">
										<div ng-show="formPagos.selloPago.$dirty && formPagos.selloPago.$invalid">
											<p class="help-block" ng-show="formPagos.selloPago.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
							
				</form>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" ng-disabled="formPagos.$invalid"
					ng-click="generaComplementoPagos(pago)" class="btn btn-success btn-ok">
					Generar <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"/>
				</button>
				<button type="button" class="btn btn-danger btn-warning" ng-click="cerrarModalPagos()">
					Cancelar <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"/>
				</button>
			</div>
		</div>
	</div>
</div>