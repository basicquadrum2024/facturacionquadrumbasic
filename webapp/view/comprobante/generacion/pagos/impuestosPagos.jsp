<style>
.content-body {
	color:black;
}
</style>
<div class="row content-body">
	<br>
	<jsp:include page="ModalTrasladosDR.jsp"></jsp:include>
	<div class="col-md-4">
		<button class="btn btn-ok" data-toggle="tooltip"
			data-placement="left" title="Agregar Traslados"
			ng-click="nuevoTrasladoPagos()"
			
			<span class="glyphicon glyphicon-plus"></span> Nuevo Traslado
		</button>
	</div>

	<div class="table-responsive">
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						
						<th>Impuesto DR</th>
						<th>TipoFactor DR</th>
						<th>TasaOCuota DR</th>
					
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
				 
					<tr ng-repeat="traslado in traslados">
					 
						
						<td>{{traslado.impuesto}}</td>
						<td>{{traslado.tipoFactor}}</td>
						<td>{{traslado.tasaOCuota}}</td>
						
						<td><a href="" 
							class="btn btn-success btn-lg btn-xs" ng-click="editarTraslado(traslado)"data-toggle="tooltip"
							data-placement="left" title="Editar traslado"> <span
								class="glyphicon glyphicon-edit"></span>
						</a> <a href=""
							class="btn-danger btn-xs red-tooltip" ng-click="borrarTraslado(traslado)" data-toggle="tooltip"
							data-placement="right" title="Eliminar traslado"> <span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<br>
	<jsp:include page="ModalRetencionesDR.jsp"></jsp:include>
	<div class="col-md-4">
		<button class="btn btn-ok" data-toggle="tooltip"
			data-placement="left" title="Agregar Retenciones"
			ng-click="nuevaRetencionPagos()"
			>
			<span class="glyphicon glyphicon-plus"></span> Nueva retención
		</button>
	</div>
	<div class="table-responsive">
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						
						<th>Impuesto DR</th>
						<th>TipoFactor DR</th>
						<th>TasaOCuota DR</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
				  
					<tr ng-repeat="retencion in retenciones">
					
						
						<td>{{retencion.impuesto}}</td>
						<td>{{retencion.tipoFactor}}</td>
						<td>{{retencion.tasaOCuota}}</td>	
											
						
						<td><a href=""
							class="btn btn-success btn-lg btn-xs"  ng-click="editarRetencion(retencion)" data-toggle="tooltip"
							data-placement="left" title="Editar retencion"> <span
								class="glyphicon glyphicon-edit"></span>
						</a> <a href="" 
							class="btn-danger btn-xs red-tooltip" ng-click="borrarRetencion(retencion)" data-toggle="tooltip"
							data-placement="right" title="Eliminar retencion"> <span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>