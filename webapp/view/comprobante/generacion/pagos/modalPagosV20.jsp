<style type="text/css">
.modal {
	top: -100px;
}

.modal-body {
	margin: 5px 5px 5px 5px !important;
	text-align: left !important;
	max-height: calc(100vh - 212px);
	overflow-y: auto;
}

.content-body {
	color: black;
}
</style>

<div id="modalPagos2" class="modal fade content-body" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">{{title}}</div>
			<div class="modal-body">
				<form id="formPagos2" name="formPagos2" role="form">
					<div class="panel panel-primary">
						<div class="panel-heading">Datos del CFDI Origen / Documento
							Relacionado versi�n 2.0</div>
						<div class="panel-body">
							<div class="row">
								<div class="form-group col-lg-3">
									<label>RFC Receptor:</label>
									<p>{{cfdiReceptor.rfc}}</p>
								</div>
								<div class="form-group col-lg-3">
									<label>Folio Fiscal:</label>
									<p>{{cfdiPadre.uuid}}</p>
								</div>
								<div class="form-group col-lg-1">
									<label>Folio:</label>
									<p>{{cfdiPadre.folio}}</p>
								</div>
								<div class="form-group col-lg-2">
									<label>Serie:</label>
									<p>{{cfdiPadre.serie}}</p>
								</div>
								<div class="form-group col-lg-3">
									<label>Total General:</label>
									<p>{{cfdiPadre.total}}</p>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-lg-offset-2">
									<div class="form-group col-lg-2">
										<label>Total Pagado:</label>
										<p>{{cfdiPadre.totalPagado == null ? 0 :
											cfdiPadre.totalPagado}}</p>
									</div>
									<div class="form-group col-lg-2">
										<label>Total Restante:</label>
										<p>{{(cfdiPadre.total - cfdiPadre.totalPagado)}}</p>
									</div>
									<div class="form-group col-lg-2">
										<label>No. Parcialidades Realizadas:</label>
										<p>{{cfdiPadre.totalParcialidades}}</p>
									</div>
									<div class="form-group col-lg-2">
										<label>No. Parcialidad Actual:</label>
										<p>{{(cfdiPadre.totalParcialidades + 1)}}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-primary" ng-show="listaCfdiCancelados">
						<div class="panel-heading">CFDI Relacionado / CFDI's
							Cancelados Anteriormente</div>
						<div class="panel-body">
							<div class="row">
								<table
									class="table table-striped table-responsive table-bordered">
									<thead>
										<tr>
											<th>Seleccionar</th>
											<th>Folio Fiscal</th>
											<th>Total</th>
											<th>Serie</th>
											<th>Folio</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="cfdiCancelado in listaCfdiCancelados">
											<td><input ng-model="uuidCfdiRelacionado.uuid"
												type="radio" name="radioSeleccionado"
												value="{{cfdiCancelado.uuid}}" /></td>
											<td>{{cfdiCancelado.uuid}}</td>
											<td scope="row">{{cfdiCancelado.total}}</td>
											<td scope="row">{{cfdiCancelado.serie}}</td>
											<td scope="row">{{cfdiCancelado.folio}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">Datos Generales De Recepci�n De
							Pagos</div>
						<div class="panel-body">
							<div class="row">
								<div class="form-group col-lg-4">
									<label class="control-label" for="fechaPago"> <span
										ng-class="{ 'error-asterisco' : formPagos2.fechaPago.$invalid && !formPagos2.fechaPago.$pristine}">*</span>
										Fecha de pago:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.fechaPago.$invalid && !formPagos2.fechaPago.$pristine}">
										<input class="form-control" type="text" id="fechaPago"
											name="fechaPago" ng-model="pago.fechaPago" required>
										<small class="form-text text-muted">Fecha de pago de
											la transacci�n.</small>
										<div
											ng-show="formPagos2.fechaPago.$dirty && formPagos2.fechaPago.$invalid">
											<p class="help-block"
												ng-show="formPagos2.fechaPago.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4">
									<label class="control-label" for="formaPago"> <span
										ng-class="{ 'error-asterisco' : formPagos2.formaPago.$invalid && !formPagos2.formaPago.$pristine}">*</span>
										Forma de pago:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.formaPago.$invalid && !formPagos2.formaPago.$pristine}">
										<select id="formaPago" name="formaPago"
											ng-model="pago.formaDePagoP" class="form-control" required
											ng-blur="validacionesFormaPago()"
											ng-options='formaPago.clave as (formaPago.clave +" "+formaPago.descripcion) for formaPago in listaFormaPago'>
											<option value="">-- Seleccione --</option>
										</select>
										<div
											ng-show="formPagos2.formaPago.$dirty && formPagos2.formaPago.$invalid">
											<p class="help-block"
												ng-show="formPagos2.formaPago.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>

								<div class="form-group col-lg-4">
									<label class="control-label" for="moneda"> <span
										ng-class="{ 'error-asterisco' : formPagos2.moneda.$invalid && !formPagos2.moneda.$pristine}">*</span>
										Moneda:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.moneda.$invalid && !formPagos2.moneda.$pristine}">
										<select id="moneda" name="moneda" ng-model="entidadMoneda"
											autofocus ng-blur="obtenerNumDecimales()"
											class="form-control" required
											ng-options='moneda as (moneda.clave+" "+moneda.descripcion) for moneda in listaMonedas'>
										</select>
										<div
											ng-show="formPagos2.moneda.$dirty && formPagos2.moneda.$invalid">
											<p class="help-block"
												ng-show="formPagos2.moneda.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>

								<div class="form-group col-lg-4">
									<label class="control-label" for="monto"> <span
										ng-class="{ 'error-asterisco' : formPagos2.monto.$invalid && !formPagos2.monto.$pristine}">*</span>
										Monto:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.monto.$invalid && !formPagos2.monto.$pristine}">
										<!-- ^{{expresionRegularMonto}}$ -->
										<input type="text" class="form-control" id="monto"
											name="monto" placeholder="Monto" required
											monto="numDecimales" ng-model="pago.monto" autocomplete="off"
											menor-que-total style="color: black;">
										<div
											ng-show="formPagos2.monto.$invalid && !formPagos2.monto.$pristine">
											<p class="help-block" ng-show="formPagos2.monto.$error.monto">De
												tener opcionalmente 18 digitos y es requerido el n�mero de
												decimales que soporta la moneda: {{numDecimales}}</p>
											<p class="help-block"
												ng-show="formPagos2.monto.$error.required">Campo
												requerido</p>
											<p class="help-block"
												ng-show="formPagos2.monto.$error.menorquetotal">Debe ser
												menor o igual al total restante.</p>
										</div>
									</div>
								</div>

							</div>
							<div class="row">
								<!-- <div class="form-group col-lg-4">
									<label class="control-label" for="serie">
										<span ng-class="{ 'error-asterisco' : formPagos2.serie.$invalid && !formPagos2.serie.$pristine}">*</span> Serie:</label>
									<div ng-class="{ 'has-error' : formPagos2.serie.$invalid && !formPagos2.serie.$pristine}">
										<select id="serie" name="serie" ng-model="serie" autofocus class="form-control" required
											ng-options='serie.nombreSerie as serie.nombreSerie for serie in listaSeries'>
										</select>
										<div ng-show="formPagos2.serie.$dirty && formPagos2.serie.$invalid">
											<p class="help-block" ng-show="formPagos2.serie.$error.required">Campo obligatorio</p>
										</div>
									</div>
								</div> -->

								<div class="form-group col-lg-4"
									ng-if="entidadMoneda.clave != 'MXN'">
									<label class="control-label" for="tipoCambio"> <span
										ng-class="{ 'error-asterisco' : formPagos2.tipoCambio.$invalid && !formPagos2.tipoCambio.$pristine}">*
									</span>Tipo de cambio:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.tipoCambio.$invalid && !formPagos2.tipoCambio.$pristine}">
										<input id="tipoCambio" name="tipoCambio"
											ng-model="pago.tipoCambioP" class="form-control" required
											validar="'tipoCambio'">
										<div
											ng-show="!formPagos2.tipoCambio.$pristine && formPagos2.tipoCambio.$invalid">
											<p class="help-block"
												ng-show="formPagos2.tipoCambio.$error.required">Campo
												obligatorio</p>
											<p class="help-block"
												ng-show="formPagos2.tipoCambio.$error.validar">Car�cter
												invalido.</p>
										</div>
									</div>
								</div>

								<div class="form-group col-lg-4">
									<label class="control-label" for="numOperacion"> <span
										ng-class="{ 'error-asterisco' : formPagos2.numOperacion.$invalid && !formPagos2.numOperacion.$pristine}">
									</span>Num. Operaci�n:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.numOperacion.$invalid && !formPagos2.numOperacion.$pristine}">
										<input id="numOperacion" name="numOperacion"
											ng-model="pago.numOperacion" class="form-control"
											validar="'numOperacion'">
										<div
											ng-show="formPagos2.numOperacion.$dirty && formPagos2.numOperacion.$invalid">
											<p class="help-block"
												ng-show="formPagos2.numOperacion.$error.required">Campo
												obligatorio</p>
											<p class="help-block"
												ng-show="formPagos2.numOperacion.$error.validar">Car�cter
												invalido.</p>
										</div>
									</div>
								</div>
								
								<div class="form-group col-lg-3">
									<label for="objetoImp">
									<span
										ng-class="{ 'error-asterisco' : formPagos2.objetoImp.$invalid && !formPagos2.objetoImp.$pristine}">*</span>
									
									Objeto Impuesto:</label>
									<div ng-class="{ 'has-error' : formPagos2.objetoImp.$invalid && !formPagos2.objetoImp.$pristine}">
										<select id="objetoImp" name="objetoImp" ng-model="objetoImpDR"
											class="form-control"
											ng-options='obImp.clave as  (obImp.clave +"-" + obImp.descripcion) for obImp in listaObjetoImpuesto'
											ng-required="true">
										</select>
										<small
											class="form-text text-muted">Objeto Impuesto.</small>
										<div
											ng-show="formPagos2.objetoImp.$dirty && formPagos2.objetoImp.$invalid">
											<p class="help-block"
												ng-show="formPagos2.objetoImp.$error.required">Campo
												obligatorio</p>
											
										</div>

									</div>

								</div>


							</div>
							<hr>

							<div class="row">
								<div class="form-group col-lg-4" ng-if="rfcEmisorCtaOrd">
									<label class="control-label" for="rfcEmisorCtaOrd"> <span
										ng-class="{ 'error-asterisco' : formPagos2.rfcEmisorCtaOrd.$invalid && !formPagos2.rfcEmisorCtaOrd.$pristine}">
									</span>Rfc Emisor Cta. Ordenante:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.rfcEmisorCtaOrd.$invalid && !formPagos2.rfcEmisorCtaOrd.$pristine}">
										<input id="rfcEmisorCtaOrd" name="rfcEmisorCtaOrd"
											ng-model="pago.rfcEmisorCtaOrd" class="form-control"
											validar="'rfcEmisorCtaOrd'">
										<div
											ng-show="formPagos2.rfcEmisorCtaOrd.$dirty && formPagos2.rfcEmisorCtaOrd.$invalid">
											<p class="help-block"
												ng-show="formPagos2.rfcEmisorCtaOrd.$error.required">Campo
												obligatorio</p>
											<p class="help-block"
												ng-show="formPagos2.rfcEmisorCtaOrd.$error.validar">Car�cter
												invalido.</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4"
									ng-if="pago.rfcEmisorCtaOrd === 'XEXX010101000'">
									<label class="control-label" for="nomBancoOrdExt"> <span
										ng-class="{ 'error-asterisco' : formPagos2.nomBancoOrdExt.$invalid && !formPagos2.nomBancoOrdExt.$pristine}">*
									</span> Nombre Del Banco Ordenante:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.nomBancoOrdExt.$invalid && !formPagos2.nomBancoOrdExt.$pristine}">
										<input id="nomBancoOrdExt" name="nomBancoOrdExt"
											ng-model="pago.nomBancoOrdExt" class="form-control" required>
										<div
											ng-show="formPagos2.nomBancoOrdExt.$dirty && formPagos2.nomBancoOrdExt.$invalid">
											<p class="help-block"
												ng-show="formPagos2.nomBancoOrdExt.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="ctaOrdenante">
									<label class="control-label" for="ctaOrdenante"> <span
										ng-class="{ 'error-asterisco' : formPagos2.ctaOrdenante.$invalid && !formPagos2.ctaOrdenante.$pristine}">
									</span>Cuenta Ordenante:
									</label>
									<!-- ^{{patronCtaOrd}}$ -->
									<div
										ng-class="{ 'has-error' : formPagos2.ctaOrdenante.$invalid && !formPagos2.ctaOrdenante.$pristine}">
										<!-- [0-9]{11}|[0-9]{18} -->
										<input id="ctaOrdenante" name="ctaOrdenante"
											ng-model="pago.ctaOrdenante" class="form-control"
											pattern="^{{patronCtaOrd}}$">
										<div
											ng-show="formPagos2.ctaOrdenante.$dirty && formPagos2.ctaOrdenante.$invalid">
											<p class="help-block"
												ng-show="formPagos2.ctaOrdenante.$error.required">Campo
												obligatorio</p>
											<p class="help-block"
												ng-show="formPagos2.ctaOrdenante.$error.pattern">Patr�n
												inv�lido '{{patronCtaOrd}}'.</p>
										</div>
									</div>
								</div>
							</div>

							<hr>

							<div class="row">
								<div class="form-group col-lg-4" ng-if="rfcEmisorCtaBen">
									<label class="control-label" for="rfcEmisorCtaBen"> <span
										ng-class="{ 'error-asterisco' : formPagos2.rfcEmisorCtaBen.$invalid && !formPagos2.rfcEmisorCtaBen.$pristine}">
									</span>Rfc Emisor Cuenta Beneficiario:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.rfcEmisorCtaBen.$invalid && !formPagos2.rfcEmisorCtaBen.$pristine}">
										<input id="rfcEmisorCtaBen" name="rfcEmisorCtaBen"
											ng-model="pago.rfcEmisorCtaBen" class="form-control">
										<div
											ng-show="formPagos2.rfcEmisorCtaBen.$dirty && formPagos2.rfcEmisorCtaBen.$invalid">
											<p class="help-block"
												ng-show="formPagos2.rfcEmisorCtaBen.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="ctaBeneficiario">
									<label class="control-label" for="ctaBeneficiario"> <span
										ng-class="{ 'error-asterisco' : formPagos2.ctaBeneficiario.$invalid && !formPagos2.ctaBeneficiario.$pristine}">
									</span>Cuenta Beneficiario:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.ctaBeneficiario.$invalid && !formPagos2.ctaBeneficiario.$pristine}">
										<input id="ctaBeneficiario" name="ctaBeneficiario"
											ng-model="pago.ctaBeneficiario" class="form-control"
											pattern="^{{patronCtaBen}}$">
										<div
											ng-show="formPagos2.ctaBeneficiario.$dirty && formPagos2.ctaBeneficiario.$invalid">
											<p class="help-block"
												ng-show="formPagos2.ctaBeneficiario.$error.required">Campo
												obligatorio</p>
											<p class="help-block"
												ng-show="formPagos2.ctaBeneficiario.$error.pattern">Patr�n
												inv�lido '{{patronCtaBen}}'.</p>
										</div>
									</div>
								</div>
							</div>

							<hr>

							<div class="row">
								<div class="form-group col-lg-4" ng-if="tipoCadPago">
									<label class="control-label" for="tipoCadPago"> <span
										ng-class="{ 'error-asterisco' : formPagos2.tipoCadPago.$invalid && !formPagos2.tipoCadPago.$pristine}">
									</span>Tipo De Cadena De Pago:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.tipoCadPago.$invalid && !formPagos2.tipoCadPago.$pristine}">
										<select id="tipoCadPago" name="tipoCadPago"
											ng-model="pago.tipoCadPago" class="form-control"
											ng-options='tipoCadPago.clave as (tipoCadPago.clave +" "+tipoCadPago.descripcion) for tipoCadPago in listaTipoCadPago'>
											<option value="">-- Seleccione --</option>
										</select>
										<div
											ng-show="formPagos2.tipoCadPago.$dirty && formPagos2.tipoCadPago.$invalid">
											<p class="help-block"
												ng-show="formPagos2.tipoCadPago.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="tipoCadPago">
									<label class="control-label" for="certPago"> <span
										ng-class="{ 'error-asterisco' : formPagos2.certPago.$invalid && !formPagos2.certPago.$pristine}">*
									</span> Certificado Que Corresponde Al Pago:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.certPago.$invalid && !formPagos2.certPago.$pristine}">
										<input id="certPago" name="certPago" ng-model="pago.certPago"
											class="form-control">
										<div
											ng-show="formPagos2.certPago.$dirty && formPagos2.certPago.$invalid">
											<p class="help-block"
												ng-show="formPagos2.certPago.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="tipoCadPago">
									<label class="control-label" for="cadPago"> <span
										ng-class="{ 'error-asterisco' : formPagos2.cadPago.$invalid && !formPagos2.cadPago.$pristine}">*
									</span> Cadina Original Del Comprobante De Pago:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.cadPago.$invalid && !formPagos2.cadPago.$pristine}">
										<input id="cadPago" name="cadPago" ng-model="pago.cadPago"
											class="form-control">
										<div
											ng-show="formPagos2.cadPago.$dirty && formPagos2.cadPago.$invalid">
											<p class="help-block"
												ng-show="formPagos2.cadPago.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>
								<div class="form-group col-lg-4" ng-if="tipoCadPago">
									<label class="control-label" for="selloPago"> <span
										ng-class="{ 'error-asterisco' : formPagos2.selloPago.$invalid && !formPagos2.selloPago.$pristine}">*
									</span> Sello Digital Que Se Asocie Al Pago.:
									</label>
									<div
										ng-class="{ 'has-error' : formPagos2.selloPago.$invalid && !formPagos2.selloPago.$pristine}">
										<input id="selloPago" name="selloPago"
											ng-model="pago.selloPago" class="form-control">
										<div
											ng-show="formPagos2.selloPago.$dirty && formPagos2.selloPago.$invalid">
											<p class="help-block"
												ng-show="formPagos2.selloPago.$error.required">Campo
												obligatorio</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>








				
				</form>
                 <div ng-show="objetoImpDR=='02'">
				<h3>Impuestos</h3>
				<jsp:include page="impuestosPagos.jsp"></jsp:include>
                </div>






			</div>
			<div class="modal-footer">
				<button type="button" ng-disabled="formPagos2.$invalid"
					ng-click="generaComplementoPagosV20(pago)"
					class="btn btn-success btn-ok">
					Generar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true" />
				</button>
				<button type="button" class="btn btn-danger btn-warning"
					ng-click="cerrarModalPagos2()">
					Cancelar <span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true" />
				</button>
			</div>
		</div>
	</div>
</div>