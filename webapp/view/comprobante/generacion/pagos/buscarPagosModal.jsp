<style>
.content-body {
	color:black;
}

</style>
<div class="alert alert-warning content-body" ng-if="!buscarPago" role="alert" style="background: white;">
	<span class="glyphicon glyphicon-exclamation-sign" style="color: orange !important;"></span><strong style="color: orange !important;"> Atenci�n!</strong> Debe seleccionar un <strong>Comprobante de pagos</strong> ra�z o padre.
</div>
<div class="panel panel-primary">
	<div class="panel-heading">Datos del CFDI Origen / Documento Relacionado</div>
	<div class="panel-body" style="color: black;">
		<div class="row">
			<div class="form-group col-lg-3">
				<label>RFC Receptor:</label>
				<p>{{cfdiPadre.receptor.rfc}}</p>
			</div>
			<div class="form-group col-lg-4">
				<label>Folio Fiscal:</label>
				<p>{{cfdiPadre.uuid}}</p>
			</div>
			<div class="form-group col-lg-1">
				<label>Folio:</label>
				<p>{{cfdiPadre.folio}}</p>
			</div>
			<div class="form-group col-lg-2">
				<label>Serie:</label>
				<p>{{cfdiPadre.serie}}</p>
			</div>
			<div class="form-group col-lg-2">
				<label>Total General:</label>
				<p>{{cfdiPadre.total}}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-lg-offset-3">
				<div class="form-group col-lg-2">
					<label>Total Pagado:</label>
					<p>{{cfdiPadre.totalPagado == null ? 0 : cfdiPadre.totalPagado}}</p>
				</div>
				<div class="form-group col-lg-2">
					<label>Total Restante:</label>
					<p>{{(cfdiPadre.total - cfdiPadre.totalPagado)}}</p>
				</div>
				<div class="form-group col-lg-2">
					<label>No. Parcialidades Realizadas:</label>
					<p>{{cfdiPadre.totalParcialidades}}</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="panel panel-primary">
	<div class="panel-heading">B�squeda de CFDI's</div>
	<div class="panel-body" style="color: black;">
		<form id="formBPagos" name="formBPagos" role="form">
		<div class="row">
		<div class="form-group col-lg-2">
					<button type="button" ng-click="buscarPagos()" ng-disabled="((!buscarPago) || (formBPagos.$invalid))" class="btn btn-success btn-ok">
					Buscar <span class="glyphicon glyphicon-search " aria-hidden="true"></span>
					</button>
				</div>
				</div>
			<div class="row">
				
				<div class="form-group col-lg-4">
					<label class="control-label" for="uuid">
						<span ng-class="{ 'error-asterisco' : formBPagos.uuid.$invalid && !formBPagos.uuid.$pristine}">Folio Fiscal</span></label>
					<div ng-class="{ 'has-error' : formBPagos.uuid.$invalid && !formBPagos.uuid.$pristine}">
						<input type="text" ng-model="twCfdi.uuid" validar="'uuid'" class="form-control color-black" id="uuid" name="uuid" aria-describedby="uuid" placeholder="Escriba el Folio Fiscal">
						<small class="form-text text-muted"> Identificador Universalmente �nico o Folio Fiscal.</small>
						<div ng-show="formBPagos.uuid.$invalid && !formBPagos.uuid.$pristine">
							<p class="help-block" ng-show="formBPagos.uuid.$error.validar">Folio fiscal no v�lido.</p>
						</div>
					</div>
				</div>
				<div class="form-group col-lg-4">
					<label class="control-label" for="fechaInicial">
						<span ng-class="{ 'error-asterisco' : formBPagos.fechaInicial.$invalid && !formBPagos.fechaInicial.$pristine}">Fecha</span></label>
					<div ng-class="{ 'has-error' : formBPagos.fechaInicial.$invalid && !formBPagos.fechaInicial.$pristine}">
						<input type="text" ng-model="twCfdi.fechaCreacion" ng-pattern="/^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$/" 
							class="form-control color-black" id="fechaInicial" name="fechaInicial" aria-describedby="fecha" placeholder="Fecha inicial">
						<small class="form-text text-muted"> Fecha de timbrado YYYY-MM-DD.</small>
						<div ng-show="formBPagos.fechaInicial.$invalid && !formBPagos.fechaInicial.$pristine">
							<p class="help-block " ng-show="formBPagos.fechaInicial.$error.pattern">Fecha no v�lida.</p>
						</div>
					</div>
				</div>
				<div class="form-group col-lg-2">
					<label class="control-label" for="serie">
						<span ng-class="{ 'error-asterisco' : formBPagos.serie.$invalid && !formBPagos.serie.$pristine}">Serie</span></label>
					<div ng-class="{ 'has-error' : formBPagos.serie.$invalid && !formBPagos.serie.$pristine}">
						<input type="text" ng-model="twCfdi.serie" validar="'serie'" class="form-control color-black" id="serie" name="serie" aria-describedby="serie" placeholder="Serie">
						<small class="form-text text-muted color-black"> Serie del Comprobante.</small>
						<div ng-show="formBPagos.serie.$invalid && !formBPagos.serie.$pristine">
							<p class="help-block" ng-show="formBPagos.serie.$error.validar">Serie no v�lida.</p>
						</div>
					</div>
				</div>
				<div class="form-group col-lg-2">
					<label class="control-label" for="estatus">
						<span ng-class="{ 'error-asterisco' : formBPagos.estatus.$invalid && !formBPagos.estatus.$pristine}">Estatus</span></label>
					<div ng-class="{ 'has-error' : formBPagos.estatus.$invalid && !formBPagos.estatus.$pristine}">
						<select class="form-control" id="estatus" name="estatus" ng-model="twCfdi.estatus">
							<option value="">-Seleccione-</option>
							<option value="1">Activo</option>
							<option value="0">Cancelado</option>
						</select>
						<small class="form-text text-muted"> Activo / Cancelado</small>
						<div ng-show="formBPagos.estatus.$invalid && !formBPagos.estatus.$pristine">
							<p class="help-block" ng-show="formBPagos.estatus.$error.validar">Estatus no v�lido.</p>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
