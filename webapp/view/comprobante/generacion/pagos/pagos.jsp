<div class="container content-body-pagos" style="color: black;">
	<%-- <div class="modal fade" id="modalFolios" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog " role="document">
			<div class="modal-content">
				<div class="modal-header">
	        		<h4 class="modal-title" id="mySmallModalLabel"><strong>Atención</strong></h4>
     			</div>
		      	<div class="modal-body">
		        	<h4 ng-if="foliosAdquiridos > 0 && foliosAdquiridos <= 10">Quedan pocos folios disponibles.!</h4>
		        	<h4 ng-if="foliosAdquiridos <= 0">Se ha quedado sin folios disponibles.!</h4>
		        	<strong>Folios : {{foliosAdquiridos}}</strong>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
		      	</div>
			</div>
		</div>
	</div> --%>
	
	<jsp:include page="modalPagos.jsp"></jsp:include>
	<jsp:include page="modalPagosV20.jsp"></jsp:include>
	<jsp:include page="../modalReutilizable.jsp"></jsp:include>
	<div class="">
		<%-- <div class="alert alert-warning" ng-if="foliosAdquiridos <= 10" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign"></span><strong> Atencion!</strong> Folios Disponibles <strong>{{foliosAdquiridos}}</strong> 
		</div> --%>
		<ul class="nav nav-tabs">
			<li id="menuPagos" class="active" ng-click="toggle()"><a data-toggle="tab" href="#pagos">Registrar Pagos</a></li>
			<li><a id="menuBuscar" ng-disabled="buscarPago" data-toggle="tab" href="#buscar">Busqueda de pagos</a></li>
		</ul>
		<div class="tab-content">
			<div id="pagos" class="tab-pane fade in active">
				<h3></h3>
				<div class="row">
					<div class="form-group col-lg-12 header">
						<ol class="breadcrumb">
							<li class="active"><h4><span class="glyphicon glyphicon-list-alt"></span> Recibo electrónico de pagos</h4></li>
						</ol>
						<!-- <div class="w3-container w3-pale-blue w3-leftbar w3-border-blue">
							<h4><span class="glyphicon glyphicon-list-alt"></span> Recibo electrónico de pagos</h4>
						</div> -->
						<hr>
						<div class="table table-striped" data-ng-grid="gridOptions" style="height: 200px;"></div>
					</div>
				</div>
			</div>
			<div id="buscar" class="tab-pane fade">
				<h3></h3>
				<jsp:include page="buscarPagosModal.jsp"></jsp:include>
				<div class="row">
					<div class="form-group col-lg-12">
				</div></div>
			</div>
		</div>
	</div>
	<div class="container" ng-if="buscarPago">
		<div class="table table-striped" data-ng-grid="gridOptionsBusqueda" style="height: 200px;"></div>
	</div>
</div>