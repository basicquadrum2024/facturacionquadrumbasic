<div class="row">
	<!-- <pre>{{comprobanteController.formInformacionComprobante | json}}</pre> -->


	<form id="comprobanteController.formInformacionComprobante"
		name="comprobanteController.formInformacionComprobante"
		class="form-horizontal" role="form">
		<!-- se cambia de posicion tipo de comprobante para realizar validaciones -->
		<div class="form-group">
			<label for="tipoDeComprobante" class="col-sm-4 control-label"><span
				ng-class="{ 'error-asterisco' : comprobanteController.formInformacionComprobante.tipoDeComprobante.$invalid && !comprobanteController.formInformacionComprobante.tipoDeComprobante.$pristine}">*
			</span> Tipo de comprobante:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.tipoDeComprobante.$invalid && !comprobanteController.formInformacionComprobante.tipoDeComprobante.$pristine}">
				<select id="tipoDeComprobante" name="tipoDeComprobante"
					ng-model="comprobante.tipoDeComprobante" class="form-control"
					ng-change="muestraCfdiRelacionado(comprobante.tipoDeComprobante); validarCamposRequeridos(comprobante.tipoDeComprobante)"
					ng-options='tipoDeComprobante.clave as (tipoDeComprobante.clave+"-"+tipoDeComprobante.descripcion) for tipoDeComprobante in listaTiposComprobante'
					required>
					<option value="">-- Seleccione --</option>
				</select>
				<div
					ng-show="comprobanteController.formInformacionComprobante.tipoDeComprobante.$dirty && comprobanteController.formInformacionComprobante.tipoDeComprobante.$invalid">
					<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.tipoDeComprobante.$error.required">Campo
						obligatorio</p>
				</div>
			</div>
		</div>
		<!--  -->
		
		
		
		
		<div class="form-group">
			<label for="metodoPago" class="col-sm-4 control-label">* Método
				Pago:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.metodoPago.$invalid && !comprobanteController.formInformacionComprobante.metodoPago.$pristine}">
				<!-- <input id="metodoPago" type="text" name="metodoPago"
					placeholder="Metodo Pago" class="form-control"
					ng-model="comprobante.metodoPago" /> -->
				<select id="metodoPago" name="metodoPago" ng-required="requeridoyActivoMetodo" ng-disabled="!requeridoyActivoMetodo"  ng-change="validarFormaPago(comprobante.metodoPago)"
					ng-model="comprobante.metodoPago" class="form-control"
					ng-options='metodoPago.clave as (metodoPago.clave+"-"+metodoPago.descripcion) for metodoPago in listaMetodosPago'>
					<option value="">-- Seleccione --</option>
				</select>
				<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.metodoPago.$error.required">Campo
						obligatorio</p>
			</div>
		</div>
		<div class="form-group">
			<label for="formaPago" class="col-sm-4 control-label">* Forma
				Pago :</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.formaPago.$invalid && !comprobanteController.formInformacionComprobante.formaPago.$pristine}">
				<!-- <input id="formaPago" type="text" name="formaPago"
					placeholder="Forma Pago" class="form-control"
					ng-model="comprobante.formaPago" /> -->

				<select id="formaPago" name="formaPago" ng-required="requeridoFormaPago" ng-disabled="bloqueaFormaPago"
					ng-model="comprobante.formaPago" class="form-control"
					ng-options='(formaPago.clave) as (formaPago.clave +"-" + formaPago.descripcion) for formaPago in listaFormasPago'>
					<option value="">-- Seleccione --</option>
				</select>
				<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.formaPago.$error.required">Campo
						obligatorio</p>
			</div>
		</div>
		<div class="form-group">
			<label for="condicionesDePago" class="col-sm-4 control-label">Condiciones
				de Pago :</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.condicionesDePago.$invalid && !comprobanteController.formInformacionComprobante.condicionesDePago.$pristine}">
				<input id="condicionesDePago" type="text" name="condicionesDePago"
					placeholder="Condiciones de Pago" class="form-control"
					ng-model="comprobante.condicionesDePago" />
			</div>
		</div>
		<div class="form-group">
			<label for="descuento" class="col-sm-4 control-label">Descuento:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.descuento.$invalid && !comprobanteController.formInformacionComprobante.descuento.$pristine}">
				<input id="descuento" type="number" name="descuento"
					placeholder="Descuento" class="form-control"
					ng-model="comprobante.descuento" ng-disabled="true" />

			</div>
		</div>

		<div class="form-group">
			<label for="moneda" class="col-sm-4 control-label"><span
				ng-class="{ 'error-asterisco' : comprobanteController.formInformacionComprobante.moneda.$invalid && !comprobanteController.formInformacionComprobante.moneda.$pristine}">*
			</span> Moneda:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.moneda.$invalid && !comprobanteController.formInformacionComprobante.moneda.$pristine}">
				<!-- <input id="moneda" type="text" name="moneda" placeholder="Moneda"
					class="form-control" required ng-model="comprobante.moneda" /> -->

				<select id="moneda" name="moneda" ng-model="comprobante.moneda"
					class="form-control"
					ng-options='moneda.clave as (moneda.clave+"-"+moneda.descripcion) for moneda in listaMonedas'
					ng-change="validaCampoMoneda(comprobante.moneda)">
					<option value="">-- Seleccione --</option>
				</select>
				<div
					ng-show="comprobanteController.formInformacionComprobante.moneda.$dirty && comprobanteController.formInformacionComprobante.moneda.$invalid">
					<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.moneda.$error.required">Campo
						obligatorio</p>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="tipoCambio" class="col-sm-4 control-label">Tipo
				Cambio:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.tipoCambio.$invalid && !comprobanteController.formInformacionComprobante.tipoCambio.$pristine}">
				<input id="tipoCambio" type="number" name="tipoCambio"
					placeholder="Tipo Cambio" class="form-control"
					validar="'tipoCambio'"
					ng-model="comprobante.tipoCambio" ng-disabled="bloqueaTipoCambio" />
				<div
					ng-show="comprobanteController.formInformacionComprobante.tipoCambio.$dirty && comprobanteController.formInformacionComprobante.tipoCambio.$invalid">
					<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.tipoCambio.$error.required">Campo
						obligatorio</p>
					<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.tipoCambio.$error.validar">Patrón
						no válido</p>
				</div>

			</div>

		</div>
		
		<div class="form-group" ng-show="comprobante.total>valorMaximo">
			<label for="confirmacion" class="col-sm-4 control-label">Confirmación:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.confirmacion.$invalid && !comprobanteController.formInformacionComprobante.confirmacion.$pristine}">
				<input id="confirmacion" type="text" name="confirmacion"
					placeholder="Confirmación" class="form-control"
					ng-model="comprobante.confirmacion" validar="'confirmacion'" ng-blur="validarNumeroConfirmacion(comprobante.confirmacion)"/>
				<div
					ng-show="comprobanteController.formInformacionComprobante.confirmacion.$dirty && comprobanteController.formInformacionComprobante.confirmacion.$invalid">
					<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.confirmacion.$error.required">Campo
						obligatorio</p>
					<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.confirmacion.$error.validar">El
						campo confirmación debe de contener 5 caracteres.</p>
					<p class="help-block"
						ng-show="comprobanteController.formInformacionComprobante.confirmacion.$error.utilizado">La
						clave de confirmación ya ha sido utilizada.</p>
				</div>
			</div>
		</div>

		<div class="form-group" >
			<label class="col-sm-4 control-label">Cfdi relacionado <input name="checkboxRelacion"
				id="checkboxRelacion" type="checkbox" ng-model="cdfdirelacionado"
				ng-click="llenaLsitaa(cdfdirelacionado)"></label><br />
		</div>
		
		<div>
			<div class="form-group" ng-show="cdfdirelacionado">
				<label for="tipoRelacion" class="col-sm-4 control-label">Tipo
					de relación:</label>
				<div class="col-sm-7"
					ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.tipoRelacion.$invalid && !comprobanteController.formInformacionComprobante.tipoRelacion.$pristine}">

					<select id="tipoRelacion" name="tipoRelacion"
						ng-model="relacionado.tipoRelacion" class="form-control"
						ng-options='tipoDeRelacion.clave as (tipoDeRelacion.clave+"-"+tipoDeRelacion.descripcion) for tipoDeRelacion in listaTipoRelacion'
						ng-required="cdfdirelacionado"></select>
					<div
						ng-show="comprobanteController.formInformacionComprobante.tipoRelacion.$dirty && comprobanteController.formInformacionComprobante.tipoRelacion.$invalid">
						<p class="help-block"
							ng-show="comprobanteController.formInformacionComprobante.tipoRelacion.$error.required">Campo
							obligatorio</p>
					</div>
				</div>
			</div>

			<div class="form-group" ng-show="cdfdirelacionado">
				<label for="uuidRelacion" class="col-sm-4 control-label">Uuid
					Relacionado:</label>
				<div class="col-sm-7"
					ng-class="{ 'has-error' : comprobanteController.formInformacionComprobante.uuidRelacion.$invalid && !comprobanteController.formInformacionComprobante.uuidRelacion.$pristine}">
					<input id="uuidRelacion" type="text" name="uuidRelacion"
						placeholder="UUID relacionado" class="form-control"
						ng-model="relacionado.uuid"
						validar="'uuid'"
						ng-required="cdfdirelacionado" />
					<div
						ng-show="comprobanteController.formInformacionComprobante.uuidRelacion.$dirty && comprobanteController.formInformacionComprobante.uuidRelacion.$invalid">
						<p class="help-block"
							ng-show="comprobanteController.formInformacionComprobante.uuidRelacion.$error.required">Campo
							obligatorio</p>
						<p class="help-block"
							ng-show="comprobanteController.formInformacionComprobante.uuidRelacion.$error.validar">Estructura no válida del Folio Fiscal</p>


					</div>
				</div>
			</div>
		</div>

	</form>
</div>