<div class="row">
	<br>
	<jsp:include page="modalInformacionAduanera.jsp"></jsp:include>
	<div class="col-md-4">
		<button class="btn btn-success btn-ok" data-toggle="tooltip"
			data-placement="left" title="Agregar Informaci�n Aduanera"
			ng-click="nuevaInformacionAduanera()" ng-disabled="comprobanteController.formConcepto.$invalid">
			<span class="glyphicon glyphicon-plus"></span> Nueva Informaci�n Aduanera
		</button>
	</div>

	<div class="table-responsive">
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>N�mero Pedimento</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="informacionAduanera in concepto.informacionAduanera">
						<td>{{informacionAduanera.numeroPedimento}}</td>
						<td><a href="" ng-click="editarinformacionAduanera(informacionAduanera)"
							class="btn btn-success btn-lg btn-xs" data-toggle="tooltip"
							data-placement="left" title="Editar informacionAduanera"> <span
								class="glyphicon glyphicon-edit"></span>
						</a> <a href="" ng-click="borrarinformacionAduanera(informacionAduanera)"
							class="btn-danger btn-xs red-tooltip" data-toggle="tooltip"
							data-placement="right" title="Eliminar informacionAduanera"> <span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>