<style>
.content-body {
	color:black;
}
</style>
<div id="modalNuevaRetencion" class="modal fade center-modal content-body ">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				Agregar Retención
			</div>
			<div class="modal-body">
				<p>Los campos con * son requeridos.</p>
				<form id="comprobanteController.formRetencion"
					name="comprobanteController.formRetencion" class="form-horizontal"
					role="form">

					<div class="form-group">
						<label for="base" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobanteController.formRetencion.base.$invalid && !comprobanteController.formRetencion.base.$pristine}">*
						</span> Base:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formRetencion.base.$invalid && !comprobanteController.formRetencion.base.$pristine}">
							<input id="base" type="number" name="base" placeholder="Base"
								class="form-control" required ng-model="retencion.base" ng-disabled="true"/>
							<div
								ng-show="comprobanteController.formRetencion.base.$dirty && comprobanteController.formRetencion.base.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formRetencion.base.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="impuesto" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobanteController.formRetencion.impuesto.$invalid && !comprobanteController.formRetencion.impuesto.$pristine}">*
						</span> Impuesto:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formRetencion.impuesto.$invalid && !comprobanteController.formRetencion.impuesto.$pristine}">							
								<select id="impuesto" name="impuesto"
								ng-model="retencion.impuesto" class="form-control"
								ng-options="impuesto.clave as impuesto.descripcion for impuesto in listaImpuestosRetenidos"
								required ng-change="buscarTasaOCuotaRetenciones(retencion.impuesto,retencion.tipoFactor); buscarTipoFactor(retencion.impuesto, 'R')">
								
								
							</select>
							<div
								ng-show="comprobanteController.formRetencion.impuesto.$dirty && comprobanteController.formRetencion.impuesto.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formRetencion.impuesto.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="tipoFactor" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobanteController.formRetencion.tipoFactor.$invalid && !comprobanteController.formRetencion.tipoFactor.$pristine}">*
						</span> Tipo Factor:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formRetencion.tipoFactor.$invalid && !comprobanteController.formRetencion.tipoFactor.$pristine}">															
							<select id="tipoFactor" name="tipoFactor"
								ng-model="retencion.tipoFactor" class="form-control"
								ng-options="tipoFactor as tipoFactor for tipoFactor in listatiposFactor"
								required ng-change="buscarTasaOCuotaRetenciones(retencion.impuesto,retencion.tipoFactor)">
							</select>	
							<div
								ng-show="comprobanteController.formRetencion.tipoFactor.$dirty && comprobanteController.formRetencion.tipoFactor.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formRetencion.tipoFactor.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>

					<!-- <div class="form-group">
						<label for="tasaOCuota" class="col-sm-4 control-label"> <span
							ng-class="{ 'error-asterisco' : comprobanteController.formRetencion.tasaOCuota.$invalid && !comprobanteController.formRetencion.tasaOCuota.$pristine}">*
						</span> Tasa o Cuota:
						</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formRetencion.tasaOCuota.$invalid && !comprobanteController.formRetencion.tasaOCuota.$pristine}">															
							<select id="tasaOCuota" name="tasaOCuota"
								ng-model="retencion.tasaOCuota" class="form-control"
								ng-options="(tasaOCuota.valorMaximo | uppercase) as (tasaOCuota.valorMaximo | uppercase) for tasaOCuota in listaTasaOCuotaRetenciones"
								required ng-change="calcularImporteRetenido()">
							</select>	
							<div
								ng-show="comprobanteController.formRetencion.tasaOCuota.$dirty && comprobanteController.formRetencion.tasaOCuota.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formRetencion.tasaOCuota.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div> -->

					<div class="form-group"  ng-show="listaTasaOCuotaRetenciones[0].rangoFijo=='Fijo'">
						<label for="tasaOCuota" class="col-sm-4 control-label"> <span
							ng-class="{ 'error-asterisco' : comprobanteController.formRetencion.tasaOCuota.$invalid && !comprobanteController.formRetencion.tasaOCuota.$pristine}">*
						</span> Tasa o Cuota:
						</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formRetencion.tasaOCuota.$invalid && !comprobanteController.formRetencion.tasaOCuota.$pristine}">															
							<select id="tasaOCuota" name="tasaOCuota"
								ng-model="retencion.tasaOCuota" class="form-control"
								ng-options="(tasaOCuota.valorMaximo) as (tasaOCuota.valorMaximo) for tasaOCuota in listaTasaOCuotaRetenciones"
								required ng-change="calcularImporteRetenido()">
							</select>	
							<div
								ng-show="comprobanteController.formRetencion.tasaOCuota.$dirty && comprobanteController.formRetencion.tasaOCuota.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formRetencion.tasaOCuota.$error.required">Campo obligatorio</p>
							</div>
						</div>
					</div>
					
					<div class="form-group"  ng-show="listaTasaOCuotaRetenciones[0].rangoFijo=='Rango'">
						<label for="tasaOCuota" class="col-sm-4 control-label"> <span
							ng-class="{ 'error-asterisco' : comprobanteController.formRetencion.tasaOCuota.$invalid && !comprobanteController.formRetencion.tasaOCuota.$pristine}">*
						</span> Tasa o Cuota:
						</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formRetencion.tasaOCuota.$invalid && !comprobanteController.formRetencion.tasaOCuota.$pristine}">															
							<input type="range" step="0.000001" name="range" ng-model="retencion.tasaOCuota" min="{{listaTasaOCuotaRetenciones[0].valorMinimo}}" 
							ng-change="calcularImporteRetenido()" max="{{listaTasaOCuotaRetenciones[0].valorMaximo}}">
							<input id="tasaOCuota" name="tasaOCuota" type="text"
								ng-model="retencion.tasaOCuota" class="form-control" required readonly >
							<div
								ng-show="comprobanteController.formRetencion.tasaOCuota.$dirty && comprobanteController.formRetencion.tasaOCuota.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formRetencion.tasaOCuota.$error.required">Campo obligatorio</p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="importe" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobanteController.formRetencion.importe.$invalid && !comprobanteController.formRetencion.importe.$pristine}">*
						</span> Importe:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formRetencion.importe.$invalid && !comprobanteController.formRetencion.importe.$pristine}">
							<input id="importe" type="number" name="importe"
								placeholder="Importe" class="form-control"
								ng-model="retencion.importe" required ng-disabled="true"/>

							<div
								ng-show="comprobanteController.formRetencion.importe.$dirty && comprobanteController.formRetencion.importe.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formRetencion.importe.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" ng-click="guardarRetencion(retencion)"
					class="btn btn-success btn-ok">
					Aceptar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
				<button type="button" class="btn btn-danger btn-warning" ng-click="cerrarModaRetenciones()">
					Cancelar <span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span>
				</button>
			</div>
		</div>
	</div>
</div>