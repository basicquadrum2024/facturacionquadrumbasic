<style>
.content-body {
	color:black;
}
</style>

<div id="modalNuevoTrasaldo" class="modal fade center-modal content-body">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">				
				Agregar Traslado
			</div>
			<div class="modal-body">
				<p>Los campos con * son requeridos.</p>
				<form id="comprobanteController.formTraslado"
					name="comprobanteController.formTraslado" class="form-horizontal"
					role="form">

					<div class="form-group">
						<label for="base" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobanteController.formTraslado.base.$invalid && !comprobanteController.formTraslado.base.$pristine}">*
						</span> Base:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formTraslado.base.$invalid && !comprobanteController.formTraslado.base.$pristine}">
							<input id="base" type="number" name="base" placeholder="Base"
								class="form-control" required ng-model="traslado.base"
								ng-disabled="true" />
							<div
								ng-show="comprobanteController.formTraslado.base.$dirty && comprobanteController.formTraslado.base.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formTraslado.base.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="impuesto" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobanteController.formTraslado.impuesto.$invalid && !comprobanteController.formTraslado.impuesto.$pristine}">*
						</span> Impuesto:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formTraslado.impuesto.$invalid && !comprobanteController.formTraslado.impuesto.$pristine}">							
							<select id="impuesto" name="impuesto"
								ng-model="traslado.impuesto" class="form-control"
								ng-options="impuesto.clave as impuesto.descripcion for impuesto in listaImpuestosTrasladados"
								required
								ng-change="buscarTasaOCuotaTraslados(traslado.impuesto,traslado.tipoFactor); buscarTipoFactor(traslado.impuesto, 'T')">
							</select>
							<div
								ng-show="comprobanteController.formTraslado.impuesto.$dirty && comprobanteController.formTraslado.impuesto.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formTraslado.impuesto.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="tipoFactor" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobanteController.formTraslado.tipoFactor.$invalid && !comprobanteController.formTraslado.tipoFactor.$pristine}">*
						</span> Tipo Factor:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formTraslado.tipoFactor.$invalid && !comprobanteController.formTraslado.tipoFactor.$pristine}">							
							<select id="tipoFactor" name="tipoFactor"
								ng-model="traslado.tipoFactor" class="form-control"
								ng-options="tipoFactor as tipoFactor for tipoFactor in listatiposFactor"
								required
								ng-change="buscarTasaOCuotaTraslados(traslado.impuesto,traslado.tipoFactor)">
							</select>
							<div
								ng-show="comprobanteController.formTraslado.tipoFactor.$dirty && comprobanteController.formTraslado.tipoFactor.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formTraslado.tipoFactor.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>

					<!-- <div class="form-group">
						<label for="tasaOCuota" class="col-sm-4 control-label">
							Tasa o Cuota:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formTraslado.tasaOCuota.$invalid && !comprobanteController.formTraslado.tasaOCuota.$pristine}">
							<select id="tasaOCuota" name="tasaOCuota"
								ng-model="traslado.tasaOCuota" class="form-control"
								ng-options="(tasaOCuota.valorMaximo) as (tasaOCuota.valorMaximo) for tasaOCuota in listaTasaOCuotaTrasalados"
								ng-change="calcularImporteTraslado()" ng-disabled="traslado.tipoFactor == 'EXENTO'">
							</select>
						</div>
					</div> -->
					<div class="form-group" ng-show="listaTasaOCuotaTrasalados[0].rangoFijo=='Fijo'">
						<label for="tasaOCuota" class="col-sm-4 control-label"><span ng-show="traslado.tipoFactor != 'Exento'"
							ng-class="{ 'error-asterisco' : comprobanteController.formTraslado.tasaOCuota.$invalid && !comprobanteController.formTraslado.tasaOCuota.$pristine}">*
						</span>
							Tasa o Cuota:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formTraslado.tasaOCuota.$invalid && !comprobanteController.formTraslado.tasaOCuota.$pristine}">
							<select id="tasaOCuota" name="tasaOCuota" ng-required="traslado.tipoFactor != 'Exento'"
								ng-model="traslado.tasaOCuota" class="form-control"
								ng-options="(tasaOCuota.valorMaximo) as (tasaOCuota.valorMaximo) for tasaOCuota in listaTasaOCuotaTrasalados"
								ng-disabled="traslado.tipoFactor == 'Exento'"
								ng-change="calcularImporteTraslado()">
							</select>
							<div
								ng-show="comprobanteController.formTraslado.tasaOCuota.$dirty && comprobanteController.formTraslado.tasaOCuota.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formTraslado.tasaOCuota.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>
					<div class="form-group" ng-show="listaTasaOCuotaTrasalados[0].rangoFijo=='Rango'">
						<label for="tasaOCuota" class="col-sm-4 control-label"><span ng-show="traslado.tipoFactor != 'Exento'"
							ng-class="{ 'error-asterisco' : comprobanteController.formTraslado.tasaOCuota.$invalid && !comprobanteController.formTraslado.tasaOCuota.$pristine}">*
						</span>
							Tasa o Cuota:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formTraslado.tasaOCuota.$invalid && !comprobanteController.formTraslado.tasaOCuota.$pristine}">
							<input type="range" step="0.000001" name="range" ng-model="traslado.tasaOCuota" min="{{listaTasaOCuotaTrasalados[0].valorMinimo}}" 
							ng-change="calcularImporteTraslado()" max="{{listaTasaOCuotaTrasalados[0].valorMaximo}}">
							<input type="text" id="tasaOCuota" name="tasaOCuota" ng-required="traslado.tipoFactor != 'Exento'"
								ng-model="traslado.tasaOCuota" class="form-control" readonly
								ng-disabled="traslado.tipoFactor == 'Exento'" >
								
								<div
								ng-show="comprobanteController.formTraslado.tasaOCuota.$dirty && comprobanteController.formTraslado.tasaOCuota.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formTraslado.tasaOCuota.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="importe" class="col-sm-4 control-label">Importe:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formTraslado.importe.$invalid && !comprobanteController.formTraslado.importe.$pristine}">
							<input id="importe" type="number" name="importe"
								placeholder="Importe" class="form-control"
								ng-model="traslado.importe" ng-disabled="true" />
						</div>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" ng-click="guardarTraslado(traslado)"
					class="btn btn-success btn-ok">
					Aceptar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
				<button type="button" class="btn btn-danger btn-warning"
					ng-click="cerrarModalTraslados()">
					Cancelar <span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span>

				</button>
			</div>
		</div>
	</div>
</div>