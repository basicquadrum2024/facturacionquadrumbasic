<style>
.content-body {
	color:black;
}
</style>
<div class="row content-body">
	<br>
	<jsp:include page="modalTraslados.jsp"></jsp:include>
	<div class="col-md-4">
		<button class="btn btn-ok" data-toggle="tooltip"
			data-placement="left" title="Agregar Traslados"
			ng-click="nuevoTraslado()"
			ng-disabled="comprobanteController.formConcepto.$invalid">
			<span class="glyphicon glyphicon-plus"></span> Nuevo Traslado
		</button>
	</div>

	<div class="table-responsive">
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Base</th>
						<th>Impuesto</th>
						<th>TipoFactor</th>
						<th>TasaOCuota</th>
						<th>Importe</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="traslado in concepto.impuestos.traslados.traslado">
						<td>{{traslado.base}}</td>
						<td>{{traslado.impuesto}}</td>
						<td>{{traslado.tipoFactor}}</td>
						<td>{{traslado.tasaOCuota}}</td>
						<td>{{traslado.importe | currency : "$" : objdecimales.decimalesOperaciones}}</td>
						<td><a href="" ng-click="editarTraslado(traslado)"
							class="btn btn-success btn-lg btn-xs" data-toggle="tooltip"
							data-placement="left" title="Editar traslado"> <span
								class="glyphicon glyphicon-edit"></span>
						</a> <a href="" ng-click="borrarTraslado(traslado)"
							class="btn-danger btn-xs red-tooltip" data-toggle="tooltip"
							data-placement="right" title="Eliminar traslado"> <span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<br>
	<jsp:include page="modalRetenciones.jsp"></jsp:include>
	<div class="col-md-4">
		<button class="btn btn-ok" data-toggle="tooltip"
			data-placement="left" title="Agregar Retenciones"
			ng-click="nuevaRetencion()"
			ng-disabled="comprobanteController.formConcepto.$invalid">
			<span class="glyphicon glyphicon-plus"></span> Nueva retención
		</button>
	</div>
	<div class="table-responsive">
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Base</th>
						<th>Impuesto</th>
						<th>TipoFactor</th>
						<th>TasaOCuota</th>
						<th>Importe</th>						
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="retencion in concepto.impuestos.retenciones.retencion">
						<td>{{retencion.base}}</td>
						<td>{{retencion.impuesto}}</td>
						<td>{{retencion.tipoFactor}}</td>
						<td>{{retencion.tasaOCuota}}</td>						
						<td>{{retencion.importe |currency : "$" : objdecimales.decimalesOperaciones}}</td>
						<td><a href="" ng-click="editarRetencion(retencion)"
							class="btn btn-success btn-lg btn-xs" data-toggle="tooltip"
							data-placement="left" title="Editar retencion"> <span
								class="glyphicon glyphicon-edit"></span>
						</a> <a href="" ng-click="borrarRetencion(retencion)"
							class="btn-danger btn-xs red-tooltip" data-toggle="tooltip"
							data-placement="right" title="Eliminar retencion"> <span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>