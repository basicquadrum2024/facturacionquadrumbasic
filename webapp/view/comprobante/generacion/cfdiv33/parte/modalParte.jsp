<style>
.content-body {
	color:black;
}
</style>
<div id="modalNuevaParte" class="modal fade center-modal content-body">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">Agregar Parte</div>
			<div class="modal-body">
				<p>Los campos con * son requeridos.</p>
				<form id="comprobanteController.formParte"
					name="comprobanteController.formParte" role="form">
					<div class="row">

						<div class="form-group  col-xs-12 col-md-6">
							<label for="pClaveProdServ"><span
								ng-class="{'error-asterisco' : comprobanteController.formParte.pClaveProdServ.$invalid && !comprobanteController.formParte.pClaveProdServ.$pristine}">*
							</span>Clave producto servicio:</label>
							
									<div ng-class="{ 'has-error' : comprobanteController.formParte.pClaveProdServ.$invalid && !comprobanteController.formParte.pClaveProdServ.$pristine}">
								
								<auto-complete

								 place-holder="Clave del producto o servicio"
									modelo-ng-model="parte.claveProdServ" class="autocomplete"
									lista-objetos="listaClavesProdServ"
									propiedad-mostrar='["clave","descripcion"]'
									funcion-busqueda="buscarClaveProdServ"
									propiedad-elegir='clave' nombre-elemento="claveProdServ"
									nombre-catalogo="c_ClaveProdServ"
									modelo-ng-required="true"
									tooltip="Escribe la clave del producto o servicio se realizar� la b�squeda autom�tica"
									></auto-complete>
								
								<!--  	<input
									type="text" ng-model="parte.claveProdServ" class="form-control"
									id="pClaveProdServ" name="pClaveProdServ" required>-->
									</div>
									
								<div
									ng-show="comprobanteController.formParte.pClaveProdServ.$dirty && comprobanteController.formParte.pClaveProdServ.$invalid">
									<p class="help-block"
										ng-show="comprobanteController.formParte.pClaveProdServ.$error.required">Campo
										obligatorio</p>
								</div>
						</div>


						<div class="form-group col-xs-12 col-md-6">
							<label for="pNoIdentificacion">No. identificaci�n:</label> 
							<input validar="'noIdentificacion'"
								type="text" ng-model="parte.noIdentificacion"
								class="form-control" id="pNoIdentificacion"
								name="pNoIdentificacion">
								<p ng-show="comprobanteController.formParte.pNoIdentificacion.$error.validar"
							class="help-block">Caracter inv�lido.</p>
						</div>
					</div>

					<div class="row">

						<div class="form-group col-xs-12 col-md-6" ng-class="{ 'has-error' : comprobanteController.formParte.pCantidad.$invalid}">
							<label for="pCantidad"><span
								ng-class="{ 'error-asterisco' : comprobanteController.formParte.pCantidad.$invalid && !comprobanteController.formParte.pCantidad.$pristine}">*
							</span>Cantidad:</label> 
							<div ng-class="{ 'has-error' : comprobanteController.formParte.pCantidad.$invalid && !comprobanteController.formParte.pCantidad.$pristine}">
							<input
								type="number" class="form-control" ng-model="parte.cantidad" ng-blur="calcularParteImporte()"
								id="pCantidad" name="pCantidad" required validar="'cantidad'">
								</div>
							<div
								ng-show="comprobanteController.formParte.pCantidad.$dirty && comprobanteController.formParte.pCantidad.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formParte.pCantidad.$error.required">Campo
									obligatorio</p>
									<p ng-show="comprobanteController.formParte.pCantidad.$error.validar"
							class="help-block">Caracter inv�lido.</p>
									
							</div>
						</div>

						<div class="form-group col-xs-12 col-md-6">
							<label for="pUnidad">Unidad:</label> 
							<input type="text" validar="'unidad'"
								class="form-control" ng-model="parte.unidad" id="pUnidad"
								name="pUnidad">
								
								<p ng-show="comprobanteController.formParte.pUnidad.$error.validar"
							class="help-block">Caracter inv�lido.</p>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 col-md-6">
							<label for="pDescripcion"><span
								ng-class="{ 'error-asterisco' : comprobanteController.formParte.pDescripcion.$invalid && !comprobanteController.formParte.pDescripcion.$pristine}">*
							</span>Descripci�n:</label> 
							<div ng-class="{ 'has-error' : comprobanteController.formParte.pDescripcion.$invalid && !comprobanteController.formParte.pDescripcion.$pristine}">
							<input type="text" class="form-control" validar="'descripcion'"
								ng-model="parte.descripcion" id="pDescripcion"
								name="pDescripcion" required>
								<p ng-show="comprobanteController.formParte.pDescripcion.$error.validar"
							class="help-block">Caracter inv�lido.</p>
								</div>
							<div
								ng-show="comprobanteController.formParte.pDescripcion.$dirty && comprobanteController.formParte.pDescripcion.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formParte.pDescripcion.$error.required">Campo
									obligatorio</p>
							</div>

						</div>
						<div class="form-group col-xs-12 col-md-6" ng-class="{ 'has-error' : comprobanteController.formParte.pValorUnitario.$invalid}">
							<label for="pValorUnitario">Valor unitario:</label> <input
								type="number" class="form-control" ng-model="parte.valorUnitario"
								id="pValorUnitario" name="pValorUnitario" ng-blur="calcularParteImporte()" 
								validar="'valorUnitario'">
						<div>
						</div>
						<p ng-show="comprobanteController.formParte.pValorUnitario.$error.validar"
							class="help-block">Caracter inv�lido.</p>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 col-md-6">
							<label for="pImporte">Importe:</label> <input type="number"
								class="form-control" ng-model="parte.importe" id="pImporte"
								name="pImporte" ng-disabled="true" >
						</div>
					</div>
				</form>
				<h3>Informaci�n Aduanera</h3>
				<jsp:include page="../parte/parteInformacionAduanera.jsp"></jsp:include>
			</div>
			<div class="modal-footer">
				<button type="button" ng-click="guardarParte(parte)"
					class="btn btn-success btn-ok">
					Aceptar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
				<button type="button" class="btn btn-danger btn-warning"
					ng-click="cerrarModalParte()">
					Cancelar <span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span>

				</button>
			</div>
		</div>
	</div>
</div>

