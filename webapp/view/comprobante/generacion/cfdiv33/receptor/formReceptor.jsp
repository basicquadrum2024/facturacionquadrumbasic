<div>
	<form id="comprobanteController.formularioReceptor"
		name="comprobanteController.formularioReceptor">
		<input type="hidden" value="receptor.id">
		<input type="hidden" value="receptor.idDomicilio">
		<div class="row">
			<div class="form-group col-lg-4">
				<label for="rfc" class="control-label"><span
					ng-class="{ 'error-asterisco' : comprobanteController.formularioReceptor.rfc.$invalid && !comprobanteController.formularioReceptor.rfc.$pristine}">*</span>
					RFC Receptor: </label>
				<div ng-class="{ 'has-error' : comprobanteController.formularioReceptor.rfc.$invalid && !comprobanteController.formularioReceptor.rfc.$pristine}">
					<input class="form-control" type="text" id="rfc" name="rfc"
						placeholder="Rfc" ng-model="receptor.rfc" maxlength="13"
						autocomplete="off" ng-blur="buscarReceptorPorRfc(receptor.rfc)"
						paste-trimed required capitalize
						data-tooltip="Ingresar el Rfc si el Rfc ya habia sido capturado en tu cuenta el formulario se cargara en automatico, de lo contrario favor de llenar el formulario">
					<div ng-show="comprobanteController.formularioReceptor.rfc.$dirty && comprobanteController.formularioReceptor.rfc.$invalid">
						<p class="help-block" ng-show="comprobanteController.formularioReceptor.rfc.$error.required">Campo
							Rfc obligatorio</p>
					</div>
				</div>
			</div>

			<div class="form-group col-lg-4">
				<label class="control-label" for="nombre">Nombre o Raz�n
					Social:</label>
				<div>
					<input type="text" class="form-control" id="nombre" name="nombre" ng-maxlength="254"
						placeholder="Nombre o Raz�n Social" capitalize paste-trimed 
						validar="'razonSocial'"
						data-placement="top" ng-model="receptor.nombre" autocomplete="off">
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.nombre.$error.validar">Caracter inv�lido.</div>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.nombre.$error.maxlength">La raz�n social 
							sobre pasa los 254 caracteres.</div>
				</div>
			</div>
			<div class="form-group col-lg-4">
				<label for="residenciaFiscal" class="control-label">Residencia
					Fiscal: </label>
				<div>
					<select id="residenciaFiscal" name="residenciaFiscal" ng-disabled="!extranjero"
						ng-model="receptor.residenciaFiscal" class="form-control"
						ng-options="residenciaFiscal.clave as residenciaFiscal.descripcion for residenciaFiscal in listaresidenciaFiscal">
					</select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group col-lg-4">
				<label class="control-label" for="numRegIdTrib">N�mero Registro Identidad Fiscal:</label>
				<div>
					<input type="text" class="form-control" id="numRegIdTrib" name="numRegIdTrib" maxlength="301" ng-maxlength="300"
						placeholder="N�mero Registro Identidad Fiscal" ng-model="receptor.numRegIdTrib" capitalize paste-trimed
						autocomplete="off" ng-disabled="!extranjero">
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.numRegIdTrib.$error.maxlength">El Registro de Identidad Fiscal
							sobre pasa los 300 caracteres</div>
				</div>
			</div>

			<div class="form-group col-lg-4">
				<label for="usoCFDI" class="control-label"><span
					ng-class="{ 'error-asterisco' : comprobanteController.formularioReceptor.usoCFDI.$invalid && !comprobanteController.formularioReceptor.usoCFDI.$pristine}">*</span>
					Uso CFDI: </label>
				<div
					ng-class="{ 'has-error' : comprobanteController.formularioReceptor.usoCFDI.$invalid && !comprobanteController.formularioReceptor.usoCFDI.$pristine}">
					<select id="usoCFDI" name="usoCFDI" ng-model="receptor.usoCFDI"
						class="form-control"
						ng-options="usoCFDI.clave as usoCFDI.descripcion for usoCFDI in listausoCFDI"
						required>
					</select>
					<div ng-show="comprobanteController.formularioReceptor.usoCFDI.$dirty && comprobanteController.formularioReceptor.usoCFDI.$invalid">
						<p class="help-block"
							ng-show="comprobanteController.formularioReceptor.usoCFDI.$error.required">Campo
							Uso CFDI obligatorio</p>
					</div>
				</div>
			</div>
    
			<div class="form-group col-lg-4">
				<label class="control-label" for="domicilio">Direccion:</label>
				<div>
					<input type="text" class="form-control" id="domicilio" name="domicilio" maxlength="1000" ng-maxlength="1000"
						placeholder="Direcci�n" ng-model="receptor.domicilio" autocomplete="off" capitalize>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.domicilio.$error.maxlength">El domicilio
							sobre pasa los 1000 caracteres</div>
				</div>
			</div>
			
		</div>
		
		<div class="row">
		<!-- 
			<div class="form-group col-lg-4">
				<label class="control-label" for="noExterior">No. Exterior:</label>
				<div>
					<input type="text" class="form-control" id="noExterior" name="noExterior"
						placeholder="No. Exterior" ng-model="receptor.noExterior" maxlength="301" ng-maxlength="300"
						autocomplete="off" capitalize>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.noExterior.$error.maxlength">El no. Ext
							sobre pasa los 300 caracteres</div>
				</div>
			</div>
-->
<!-- 
			<div class="form-group col-lg-4">
				<label class="control-label" for="noInterior">No. Interior:</label>
				<div>
					<input type="text" class="form-control" id="noInterior" name="noInterior"
						placeholder="No. Interior" ng-model="receptor.noInterior" maxlength="301" ng-maxlength="300"
						autocomplete="off" capitalize>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.noInterior.$error.maxlength">El no. Int
							sobre pasa los 300 caracteres</div>
				</div>
			</div>
            -->
            <!-- 
			<div class="form-group col-lg-4">
				<label class="control-label" for="colonia">Colonia:</label>
				<div>
					<input type="text" class="form-control" id="colonia" name="colonia"
						placeholder="Colonia" ng-model="receptor.colonia" maxlength="301" ng-maxlength="300"
						autocomplete="off" capitalize>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.colonia.$error.maxlength">La colonia
							sobre pasa los 300 caracteres</div>
				</div>
			</div>
			-->
		</div>
		<div class="row">
		<!--
			<div class="form-group col-lg-4">
				<label class="control-label" for="localidad">Localidad:</label>
				<div>
					<input type="text" class="form-control" id="localidad" name="localidad"
						placeholder="Localidad" ng-model="receptor.localidad" maxlength="301" ng-maxlength="300"
						autocomplete="off" capitalize>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.localidad.$error.maxlength">La localidad
							sobre pasa los 300 caracteres</div>
				</div>
			</div>
 -->
 <!--  
			<div class="form-group col-lg-4">
				<label class="control-label" for="referencia">Referencia:</label>
				<div>
					<input type="text" class="form-control" id="referencia" name="referencia"
						placeholder="Referencia" ng-model="receptor.referencia" maxlength="301" ng-maxlength="300"
						autocomplete="off" capitalize>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.referencia.$error.maxlength">La referencia
							sobre pasa los 300 caracteres</div>
				</div>
			</div>
-->
<!-- 
			<div class="form-group col-lg-4">
				<label class="control-label" for="municipio">Municipio:</label>
				<div
					ng-class="{ 'has-error' : comprobanteController.formularioReceptor.municipio.$invalid && !comprobanteController.formularioReceptor.municipio.$pristine}">
					<input type="text" class="form-control" id="municipio" name="municipio"
						name="Municipio" placeholder="Municipio" maxlength="301" ng-maxlength="300"
						ng-model="receptor.municipio" autocomplete="off" capitalize>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.municipio.$error.maxlength">El municipio
							sobre pasa los 300 caracteres</div>
				</div>
			</div>
			-->
			
		</div>
		<div class="row">
		<!-- 
			<div class="form-group col-lg-4">
				<label class="control-label" for="estado">Estado:</label>
				<div>
					<select id="estado" name="estado" ng-model="receptor.estado"
						class="form-control" ng-options="estado as estado for estado in listaEstados">
					</select>
				</div>
			</div>
-->
<!--  
			<div class="form-group col-lg-4">
				<label for="pais" class="control-label">Pa�s:</label>
				<div>
					<input type="text" class="form-control input-sm" id="pais" name="pais"
						name="pais" placeholder="Pa�s" capitalize ng-model="receptor.pais"
						value="M�xico" paste-trimed ng-maxlength="300" maxlength="301" autocomplete="off" capitalize>
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.pais.$error.maxlength">El Pa�s
							sobre pasa los 300 caracteres</div>
				</div>
			</div>
-->
<!--  
			<div class="form-group col-lg-4">
				<label class="control-label" for="codigoPostal">C�digo
					postal:</label>
				<div>
					<input type="text" class="form-control" id="codigoPostal" name="codigoPostal"
						name="codigoPostal" placeholder="C�digo postal" maxlength="6" ng-maxlength="5"
						validar="'codigoPostal'" ng-model="receptor.codigoPostal" autocomplete="off" capitalize>
					
					<div class="help-block" ng-show="comprobanteController.formularioReceptor.codigoPostal.$error.maxlength">El c�digo postal
					 sobre pasa los 5 caracteres</div>
					 <div ng-show="comprobanteController.formularioReceptor.codigoPostal.$dirty">
						<p class="help-block" ng-show="comprobanteController.formularioReceptor.codigoPostal.$error.validar">C�digo postal inv�lido</p>
					</div>
				</div>
			</div>
			-->
			<div formularioR></div>
		</div>
	</form>
</div>