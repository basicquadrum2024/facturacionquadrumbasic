<div id="modalSuperaLimite" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" ng-click="close()"
					data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Alerta de facturaci�n</h4>
			</div>
			<div class="modal-body">
				<p>El proceso para facturar  m�s de " {{comprobante.total | currency }} " millones es el siguiente:</p>
				<ul>
					<li>1. Mandar correo a la siguiente dirreci�n : soporte@quadrum.mx indicando que le asigne un c�digo de confirmaci�n</li>
					<li>2. Una vez asigando el c�digo de confirmaci�n, ingresar el c�digo en el campo de confirmaci�n</li>
					<li>3. Dar click en el bot�n facturar.</li>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"
					ng-click="cancel()">Aceptar</button>
			</div>
		</div>
	</div>
</div>