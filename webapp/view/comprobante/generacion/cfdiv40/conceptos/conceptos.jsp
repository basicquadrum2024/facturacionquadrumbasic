<div>
	<jsp:include page="modalConcepto.jsp"></jsp:include>
	<!-- <pre>{{listaConceptos | json}}</pre> -->
	
		<br>
		<div class="row">
		<div class="col-md-4">
		<label class="control-label">N�mero de decimales para realizar calculos: </label>
		 <select id="decimales" name="decimales" ng-model="objdecimales.decimalesOperaciones"
              class="form-control"  ng-options="valor for valor in listaOpcionDecimales">
            </select>
		</div>
		</div>
		<div class="row">
		<br>
		<div class="col-md-4">
			<button class="btn btn-success btn-ok" data-toggle="tooltip"
				data-placement="left" title="Nuevo Concepto"
				ng-click="nuevoConcepto()"
				ng-disabled="receptor == undefined && receptor==null">
				<span class="glyphicon glyphicon-plus"></span> Nuevo Concepto
			</button>
		</div>
	</div>
	<br>
	<div class="table-responsive">
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Cantidad</th>
						<th>Unidad</th>
						<th>N�mero de Identificaci�n</th>
						<th>Descripci�n</th>
						<th>Valor Unitario</th>
						<th>Importe</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="concepto in comprobante.conceptos.concepto">
						<td>{{concepto.cantidad}}</td>
						<td>{{concepto.unidad}}</td>
						<td>{{concepto.noIdentificacion}}</td>
						<td>{{concepto.descripcion}}</td>
						<td>{{concepto.valorUnitario |  currency : "$" : objdecimales.decimalesOperaciones}}</td>
						<td>{{concepto.importe |  currency : "$" : objdecimales.decimalesOperaciones}}</td>
						<td><a href="" ng-click="editarConcepto(concepto)"
							class="btn btn-success btn-lg btn-xs" data-toggle="tooltip"
							data-placement="left" title="Editar Concepto"> <span
								class="glyphicon glyphicon-edit"></span>
						</a> <a href="" ng-click="borrarConcepto(concepto)"
							class="btn-danger btn-xs red-tooltip" data-toggle="tooltip"
							data-placement="right" title="Eliminar concepto"> <span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<h4>Importe sobre el comprobante</h4>
		</div>
	</div>

	<div class="div-table-content">
		<h3>Traslados</h3>
		<table>
			<tr>
				<td><h5>Total traslados:</h5></td>
				<td><h5>
						<strong>{{comprobante.impuestos.totalImpuestosTrasladados
							|   currency : "$" : objdecimales.decimalesOperaciones}}</strong>
					</h5></td>
			</tr>
		</table>
		<h3>Retenciones</h3>
		<table>
			<tr>
				<td><h5>Total retenciones:</h5></td>
				<td><h5>
						<strong>{{comprobante.impuestos.totalImpuestosRetenidos |
							  currency : "$" : objdecimales.decimalesOperaciones}}</strong>
					</h5></td>
			</tr>
		</table>
		<h3>Descuentos</h3>
		<table>
			<tr>
				<td><h5>Total descuentos:</h5></td>
				<td><h5>
						<strong>{{comprobante.descuento |   currency : "$" :objdecimales.decimalesOperaciones}}</strong>
					</h5></td>
			</tr>
		</table>
		<h3>Importes</h3>
		<table>
			<tr>
				<td><h5>Subtotal:</h5></td>
				<td><h5>
						<strong>{{comprobante.subTotal |  currency : "$" : objdecimales.decimalesOperaciones}}</strong>
					</h5></td>
			</tr>
			<tr>
				<td><h5>Total:</h5></td>
				<td><h5>
						<strong>{{comprobante.total |  currency : "$" : objdecimales.decimalesOperaciones}}</strong>
					</h5></td>
			</tr>
		</table>
	</div>
</div>