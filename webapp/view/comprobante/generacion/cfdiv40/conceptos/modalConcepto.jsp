<style type="text/css">
.modal {
	top: -100px;
}

.modal-body {
	color:black !important;
	margin: 5px 5px 5px 5px !important;
	text-align: left !important;
	max-height: calc(100vh - 212px);
	overflow-y: auto;
}
</style>

<div id="modalConcepto" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header" style="color: black !important;">
				<!-- <button type="button" class="close" ng-click="close()"
					data-dismiss="modal" aria-hidden="true">&times;</button> -->
				Agregar un concepto
			</div>
			<div class="modal-body">
				<p>Los campos con * son requeridos.</p>
				<!-- <pre>{{concepto | json}}</pre> -->
				<form id="comprobantev4Controller.formConcepto"
					name="comprobantev4Controller.formConcepto" role="form" style="color: black !important;">
					<div class="row">
						<div class="form-group col-lg-4">
							<label for="cantidad"><span
								ng-class="{ 'error-asterisco' : comprobantev4Controller.formConcepto.cantidad.$invalid && !comprobantev4Controller.formConcepto.cantidad.$pristine}">*
							</span> Cantidad:</label>
							<div
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.cantidad.$invalid && !comprobantev4Controller.formConcepto.cantidad.$pristine}">
								<input id="cantidad" type="number" name="cantidad"
									placeholder="Cantidad" class="form-control" required
									ng-model="concepto.cantidad" ng-blur="calcular()" validar="'cantidad'" />
								<div
									ng-show="comprobantev4Controller.formConcepto.cantidad.$dirty && comprobantev4Controller.formConcepto.cantidad.$invalid">
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.cantidad.$error.required">Campo
										obligatorio</p>
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.cantidad.$error.number">Dato
										inv�lido</p>
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.cantidad.$error.pattern">N�mero
										inv�lido</p>
										<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.cantidad.$error.validar">Cantidad inv�lida tiene mas de 6 decimales</p>
								</div>
							</div>
						</div>

						<div class="form-group col-lg-4">
							<label for="unidad">Unidad:</label>
							<div
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.unidad.$invalid && !comprobantev4Controller.formConcepto.unidad.$pristine}">
								<input style="color: black !important;" id="unidad" type="text" name="unidad"
									placeholder="Unidad" class="form-control"
									ng-model="concepto.unidad" validar="'unidad'" style="color: black !important;" />
									<div ng-show="comprobantev4Controller.formConcepto.unidad.$dirty">
										<p class="help-block"
											ng-show="comprobantev4Controller.formConcepto.unidad.$error.validar">Valor unidad inv�lido pasa los 20 caracteres</p>
									</div>
									
							</div>
						</div>

						<div class="form-group col-lg-4">
							<label for="claveUnidad"><span
								ng-class="{ 'error-asterisco' : comprobantev4Controller.formConcepto.claveUnidad.$invalid && !comprobantev4Controller.formConcepto.claveUnidad.$pristine}">*
							</span>Clave Unidad:</label>
							<div 
							style="color: black !important;"
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.claveUnidad.$invalid && !comprobantev4Controller.formConcepto.claveUnidad.$pristine}">
								<!-- <input id="claveUnidad" type="text" name="claveUnidad"
									placeholder="Clave Unidad" class="form-control" required
									ng-model="concepto.claveUnidad" /> -->
								
								
								<auto-complete 
									class="autocomplete"
									place-holder="Clave unidad"
									modelo-ng-model="concepto.claveUnidad" 
									lista-objetos="listaClavesUnidad"
									propiedad-mostrar='["clave","nombre"]'
									funcion-busqueda="buscarClavesUnidadLikeNombre"
									propiedad-elegir='clave' nombre-elemento="claveUnidad"
									nombre-catalogo="c_ClaveUnidad"
									modelo-ng-required="true" tooltip="Escribe el nombre de la clave unidad"></auto-complete>
							
								<div
									ng-show="comprobantev4Controller.formConcepto.claveUnidad.$dirty && comprobantev4Controller.formConcepto.claveUnidad.$invalid">
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.claveUnidad.$error.required">Campo
										obligatorio</p>
								</div>
							</div>
						</div>

					</div> <!-- r1 -->
					<div class="row">
						<div class="form-group col-lg-4">
							<label for="noIdentificacion">N�mero
								Identificaci�n:</label>
							<div>
								<input id="noIdentificacion" type="text" name="noIdentificacion"
									placeholder="N�mero Identificaci�n" class="form-control"
									ng-model="concepto.noIdentificacion" style="color: black !important;" />
							</div>
						</div>

						<div class="form-group col-lg-4">
							<label for="descripcion"><span
								ng-class="{ 'error-asterisco' : comprobantev4Controller.formConcepto.descripcion.$invalid && !comprobantev4Controller.formConcepto.descripcion.$pristine}">*
							</span> Descripci�n:</label>
							<div
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.descripcion.$invalid && !comprobantev4Controller.formConcepto.descripcion.$pristine}">

								<select id="descripcion" name="descripcion" style="color: black !important;"
									ng-model="concepto.descripcion" class="form-control"
									ng-options="descripcion.descripcion as descripcion.descripcion for descripcion in listaDescripciones"
									required
									ng-change="cambioNuevaDescripcion(concepto.descripcion)">
								</select>
								<div
									ng-show="comprobantev4Controller.formConcepto.descripcion.$dirty && comprobantev4Controller.formConcepto.descripcion.$invalid">
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.descripcion.$error.required">Campo
										obligatorio</p>
								</div>
							</div>
						</div>

						<div class="form-group col-lg-4" ng-show="nuevaDescripcion">
							<label for="nuevaDescripcion"><span
								ng-class="{ 'error-asterisco' : comprobantev4Controller.formConcepto.nuevaDescripcion.$invalid && !comprobantev4Controller.formConcepto.nuevaDescripcion.$pristine}">*
							</span> Nueva Descripci�n:</label>
							<div
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.nuevaDescripcion.$invalid && !comprobantev4Controller.formConcepto.nuevaDescripcion.$pristine}">
								<input id="nuevaDescripcion" type="text" name="nuevaDescripcion"
									placeholder=" Nueva Descripci�n" class="form-control" ng-maxlength="1000"
									validar="'descripcion'"
									ng-required="nuevaDescripcion"
									ng-model="concepto.nuevaDescripcion" style="color: black !important;"/>
								<div
									ng-show="comprobantev4Controller.formConcepto.nuevaDescripcion.$dirty && comprobantev4Controller.formConcepto.nuevaDescripcion.$invalid">
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.nuevaDescripcion.$error.required">Campo
										obligatorio</p>
									<p class="help-block" ng-show="comprobantev4Controller.formConcepto.nuevaDescripcion.$error.validar">Caracter inv�lido.</p>
								</div>
							</div>
						</div>


					</div> <!-- r2 -->
					<div class="row">
						<div class="form-group col-lg-4">
							<label for="claveProdServ"><span
								ng-class="{ 'error-asterisco' : comprobantev4Controller.formConcepto.claveProdServ.$invalid && !comprobantev4Controller.formConcepto.claveProdServ.$pristine}">*
							</span> Clave Prod Serv:</label>
							<div  style="color: black !important;"
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.claveProdServ.$invalid && !comprobantev4Controller.formConcepto.claveProdServ.$pristine}">
								<!-- <input id="claveProdServ" type="text" name="claveProdServ"
									placeholder="Clave Prod Serv" class="form-control" required
									ng-model="concepto.claveProdServ" /> -->

								<auto-complete place-holder="Clave del producto o servicio"
									modelo-ng-model="concepto.claveProdServ" class="autocomplete"
									lista-objetos="listaClavesProdServ"
									propiedad-mostrar='["clave","descripcion"]'
									funcion-busqueda="buscarClaveProdServ"
									propiedad-elegir='clave' nombre-elemento="claveProdServ"
									nombre-catalogo="c_ClaveProdServ"
									modelo-ng-required="true" tooltip="Escribe la clave del producto o servicio se realizar� la b�squeda autom�tica"></auto-complete>
								<div
									ng-show="comprobantev4Controller.formConcepto.claveProdServ.$dirty && comprobantev4Controller.formConcepto.claveProdServ.$invalid">
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.claveProdServ.$error.required">Campo
										obligatorio</p>
								</div>
							</div>
						</div>

						<div class="form-group col-lg-4">
							<label for="valorUnitario">
								<span
								ng-class="{ 'error-asterisco' : comprobantev4Controller.formConcepto.valorUnitario.$invalid && !comprobantev4Controller.formConcepto.valorUnitario.$pristine}">*
							</span> Valor Unitario:
							</label>
							<div
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.valorUnitario.$invalid && !comprobantev4Controller.formConcepto.valorUnitario.$pristine}">
								<input id="valorUnitario" type="number" name="valorUnitario"
									placeholder="Valor Unitario" class="form-control" required
									ng-model="concepto.valorUnitario"
									validar="'t_importe'" ng-blur="calcular()"  style="color: black !important;"/>
								<div
									ng-show="comprobantev4Controller.formConcepto.valorUnitario.$dirty && comprobantev4Controller.formConcepto.valorUnitario.$invalid">
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.valorUnitario.$error.required">Campo
										obligatorio</p>
								</div>
								<div ng-show="comprobantev4Controller.formConcepto.valorUnitario.$dirty">
										<p class="help-block"
											ng-show="comprobantev4Controller.formConcepto.valorUnitario.$error.validar">Valor unitario inv�lido</p>
									</div>
							</div>
						</div>

						<div class="form-group col-lg-4">
							<label for="descuento">Descuento:</label>
							<div
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.descuento.$invalid && !comprobantev4Controller.formConcepto.descuento.$pristine}">
								<input id="descuento" type="number" name="descuento"
									placeholder="Descuento" class="form-control"
									ng-model="concepto.descuento" style="color: black !important;"ng-pattern="/^[1-9]{1,18}(.[1-9]{1,6})?$/"/>
							</div>
							<div ng-show="comprobantev4Controller.formConcepto.descuento.$dirty">
										<p class="help-block"
											ng-show="comprobantev4Controller.formConcepto.descuento.$error.pattern">Valor unitario inv�lido</p>
									</div>
						</div>

					</div>
					<div class="row">
						<div class="form-group col-lg-4">
							<label for="importe">Importe:</label>
							<div
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.importe.$invalid && !comprobantev4Controller.formConcepto.importe.$pristine}">
								<input id="importe" type="text" name="importe"
									placeholder="Importe" class="form-control"
									ng-model="concepto.importe" ng-disabled="true" style="color: black !important;"/>
							</div>
						</div>

						<div class="form-group col-lg-4">
							<label for="cuentaPredial.numero">Cuenta
								Predial:</label>
							<div
								ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.cuentaPredial.numero.$invalid && !comprobantev4Controller.formConcepto.cuentaPredial.numero.$pristine}">
								<input id="cuentaPredial.numero" type="text"
									name="cuentaPredial.numero" placeholder="Cuenta Predial"
									class="form-control" ng-model="cuentaPredial.numero" style="color: black !important;" />
							</div>
						</div>
						
						<div class="form-group col-lg-4">
							<label for="objetoImp">
							<span
								ng-class="{ 'error-asterisco' : comprobantev4Controller.formConcepto.objetoImp.$invalid && !comprobantev4Controller.formConcepto.objetoImp.$pristine}">*
							</span>
							Objeto Impuesto:</label>
							<div ng-class="{ 'has-error' : comprobantev4Controller.formConcepto.objetoImp.$invalid && !comprobantev4Controller.formConcepto.objetoImp.$pristine}">
							<select   id="objetoImp" name="objetoImp" ng-model="concepto.objetoImp"
						class="form-control"
						ng-options='obImp.clave as  (obImp.clave +"-" + obImp.descripcion) for obImp in listaObjetoImpuesto'
						required>
					</select>
					<div
									ng-show="comprobantev4Controller.formConcepto.objetoImp.$dirty && comprobantev4Controller.formConcepto.objetoImp.$invalid">
									<p class="help-block"
										ng-show="comprobantev4Controller.formConcepto.objetoImp.$error.required">Campo
										obligatorio</p>
								</div>
								
							</div>
						</div>
					</div>
				</form>
				<h3>Impuestos</h3>
				<jsp:include page="../impuestos/impuestos.jsp"></jsp:include>

				<h3>Informaci�n Aduanera</h3>
				<jsp:include page="../informacionAduanera/informacionAduanera.jsp"></jsp:include>
				
				<h3>Parte</h3>
				<jsp:include page="../parte/parte.jsp"></jsp:include>
				
				<h3>Cuenta Terceros</h3>
				<jsp:include page="../cuentaTerceros/cuentaTerceros.jsp"></jsp:include>
				
			</div>
			<div class="modal-footer">
				<button type="button" ng-click="guardarConcepto(concepto)"
					class="btn btn-success btn-ok">
					Aceptar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
				<button type="button" class="btn btn-danger btn-warning"
					ng-click="cerraModalAgregarConcetos()">
					Cancelar <span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span>

				</button>
			</div>
		</div>
	</div>
</div>