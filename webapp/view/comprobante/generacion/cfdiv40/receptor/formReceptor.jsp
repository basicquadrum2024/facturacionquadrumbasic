<div>
	<form id="comprobantev4Controller.formularioReceptor"
		name="comprobantev4Controller.formularioReceptor">
		<input type="hidden" value="receptor.id"> <input type="hidden"
			value="receptor.idDomicilio">
		<div class="row">
			<div class="form-group col-lg-4">
				<label for="rfc" class="control-label"><span
					ng-class="{ 'error-asterisco' : comprobantev4Controller.formularioReceptor.rfc.$invalid && !comprobantev4Controller.formularioReceptor.rfc.$pristine}">*</span>
					RFC Receptor: </label>
				<div ng-class="{ 'has-error' : comprobantev4Controller.formularioReceptor.rfc.$invalid && !comprobantev4Controller.formularioReceptor.rfc.$pristine}">
					<input class="form-control" type="text" id="rfc" name="rfc"
						placeholder="Rfc" ng-model="receptor.rfc" maxlength="13"
						autocomplete="off" ng-blur="buscarReceptorPorRfc(receptor.rfc)"
						paste-trimed required capitalize
						data-tooltip="Ingresar el Rfc si el Rfc ya habia sido capturado en tu cuenta el formulario se cargara en automatico, de lo contrario favor de llenar el formulario">
					<div
						ng-show="comprobantev4Controller.formularioReceptor.rfc.$dirty && comprobantev4Controller.formularioReceptor.rfc.$invalid">
						<p class="help-block"
							ng-show="comprobantev4Controller.formularioReceptor.rfc.$error.required">Campo
							Rfc obligatorio</p>
					</div>
				</div>
			</div>

			<div class="form-group col-lg-4">
				<label class="control-label" for="nombre">
				<span ng-class="{ 'error-asterisco' : comprobantev4Controller.formularioReceptor.nombre.$invalid && !comprobantev4Controller.formularioReceptor.nombre.$pristine}">*</span>
				Nombre o Raz�n Social:</label>
				<div ng-class="{ 'has-error' : comprobantev4Controller.formularioReceptor.nombre.$invalid && !comprobantev4Controller.formularioReceptor.nombre.$pristine}">
					<input type="text" class="form-control" id="nombre" name="nombre"
						ng-maxlength="254" placeholder="Nombre o Raz�n Social" capitalize
						paste-trimed required data-placement="top"
						ng-model="receptor.nombre" autocomplete="off">
					<div
						ng-show="comprobantev4Controller.formularioReceptor.nombre.$dirty && comprobantev4Controller.formularioReceptor.nombre.$invalid">
						<p class="help-block"
							ng-show="comprobantev4Controller.formularioReceptor.nombre.$error.required">Campo
							obligatorio</p>
					</div>
				</div>
			</div>
			<div class="form-group col-lg-4">
				<label class="control-label" for="nombre">
				<span ng-class="{ 'error-asterisco' : comprobantev4Controller.formularioReceptor.domicilioFiscalReceptor.$invalid && !comprobantev4Controller.formularioReceptor.domicilioFiscalReceptor.$pristine}">*</span>
				Domicilio Fiscal:</label>
				<div ng-class="{ 'has-error' : comprobantev4Controller.formularioReceptor.domicilioFiscalReceptor.$invalid && !comprobantev4Controller.formularioReceptor.domicilioFiscalReceptor.$pristine}">
					<input type="text" class="form-control"
						id="domicilioFiscalReceptor" name="domicilioFiscalReceptor"
						placeholder="Domicilio Fiscal" capitalize paste-trimed required
						data-placement="top" ng-model="receptor.domicilioFiscalReceptor">
					<div
						ng-show="comprobantev4Controller.formularioReceptor.domicilioFiscalReceptor.$dirty && comprobantev4Controller.formularioReceptor.domicilioFiscalReceptor.$invalid">
						<p class="help-block"
							ng-show="comprobantev4Controller.formularioReceptor.domicilioFiscalReceptor.$error.required">Campo
							obligatorio</p>
					</div>

				</div>
			</div>

		</div>

		<div class="row">
			<div class="form-group col-lg-4">
				<label for="residenciaFiscal" class="control-label">Residencia
					Fiscal: </label>
				<div>
					<select id="residenciaFiscal" name="residenciaFiscal"
						ng-disabled="!extranjero" ng-model="receptor.residenciaFiscal"
						class="form-control"
						ng-options="residenciaFiscal.clave as residenciaFiscal.descripcion for residenciaFiscal in listaresidenciaFiscal">
					</select>
				</div>
			</div>
			<div class="form-group col-lg-4">
				<label class="control-label" for="numRegIdTrib">N�mero
					Registro Identidad Fiscal:</label>
				<div>
					<input type="text" class="form-control" id="numRegIdTrib"
						name="numRegIdTrib" maxlength="301" ng-maxlength="300"
						placeholder="N�mero Registro Identidad Fiscal"
						ng-model="receptor.numRegIdTrib" capitalize paste-trimed
						autocomplete="off" ng-disabled="!extranjero">
					<div class="help-block"
						ng-show="comprobantev4Controller.formularioReceptor.numRegIdTrib.$error.maxlength">El
						Registro de Identidad Fiscal sobre pasa los 300 caracteres</div>
				</div>
			</div>

			<div class="form-group col-lg-4">
				<label class="control-label" for="regimenFiscal">
				<span ng-class="{ 'error-asterisco' : comprobantev4Controller.formularioReceptor.regimenFiscal.$invalid && !comprobantev4Controller.formularioReceptor.regimenFiscal.$pristine}">*</span>
				R�gimen Fiscal:</label>
				<div ng-class="{ 'has-error' : comprobantev4Controller.formularioReceptor.regimenFiscal.$invalid && !comprobantev4Controller.formularioReceptor.regimenFiscal.$pristine}">
					<select id="regimenFiscal" name="regimenFiscal"
						ng-model="receptor.regimenFiscalReceptor" class="form-control" required
						ng-options='regimen.id as (regimen.id +"-" + regimen.descripcion) for regimen in listaRegimenFiscal'
						required ng-change="buscarUsosCfdiPorRfc();">
					</select>
					<div
						ng-show="comprobantev4Controller.formularioReceptor.regimenFiscal.$dirty && comprobantev4Controller.formularioReceptor.regimenFiscal.$invalid">
						<p class="help-block"
							ng-show="comprobantev4Controller.formularioReceptor.regimenFiscal.$error.required">Campo
							obligatorio</p>
					</div>

				</div>
			</div>

			<div class="form-group col-lg-4">
				<label for="usoCfdi" class="control-label"><span
					ng-class="{ 'error-asterisco' : comprobantev4Controller.formularioReceptor.usoCfdi.$invalid && !comprobantev4Controller.formularioReceptor.usoCfdi.$pristine}">*</span>
					Uso CFDI: </label>
				<div
					ng-class="{ 'has-error' : comprobantev4Controller.formularioReceptor.usoCfdi.$invalid && !comprobantev4Controller.formularioReceptor.usoCfdi.$pristine}">
					<select id="usoCfdi" name="usoCfdi" ng-model="receptor.usoCfdi"
						class="form-control"
						ng-options='usoCfdi.id as (usoCfdi.id +"-" + usoCfdi.descripcion) for usoCfdi in listausoCFDI'
						required>
					</select>
					<div
						ng-show="comprobantev4Controller.formularioReceptor.usoCfdi.$dirty && comprobantev4Controller.formularioReceptor.usoCfdi.$invalid">
						<p class="help-block"
							ng-show="comprobantev4Controller.formularioReceptor.usoCfdi.$error.required">Campo
							Uso CFDI obligatorio</p>
					</div>
				</div>
			</div>



		</div>



		<div class="row">

			<div formularioR></div>
		</div>
	</form>
</div>