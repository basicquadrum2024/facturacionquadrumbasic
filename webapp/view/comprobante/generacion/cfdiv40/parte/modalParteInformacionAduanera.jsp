<style>
.content-body {
	color:black;
}
</style>
<div id="modalparteinformacionAduanera" class="modal fade center-modal content-body">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <button type="button" class="close" ng-click="close()"
					data-dismiss="modal" aria-hidden="true">&times;</button> -->
				Agregar InformacionAduanera
			</div>
			<div class="modal-body">
				<p>Los campos con * son requeridos.</p>
				<form id="formparteinformacionAduanera" name="comprobanteController.formparteInformacionAduanera"
					class="form-horizontal" role="form">
<br>
<br>
					<div class="form-group">
						<label for="numeroPedimento" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobanteController.formparteInformacionAduanera.numeroPedimento.$invalid && !comprobanteController.formparteInformacionAduanera.numeroPedimento.$pristine}">*
						</span> numeroPedimento:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobanteController.formparteInformacionAduanera.numeroPedimento.$invalid && !comprobanteController.formparteInformacionAduanera.numeroPedimento.$pristine}">
							
							<input id="numeroPedimento" type="text" name="numeroPedimento"
								placeholder="Numero Pedimento" class="form-control" required
								ng-model="informacionAduanera.numeroPedimento" data-tooltip="Ejemplo n�mero de pedimento, 56--45--4778--7854968 (Los - representan espacios)"
								validar="'numeroPedimento'"
								/>
							<div
								ng-show="comprobanteController.formparteInformacionAduanera.numeroPedimento.$dirty && comprobanteController.formparteInformacionAduanera.numeroPedimento.$invalid">
								<p class="help-block"
									ng-show="comprobanteController.formparteInformacionAduanera.numeroPedimento.$error.required">Campo
									obligatorio</p>
									<p class="help-block"
						ng-show="comprobanteController.formparteInformacionAduanera.numeroPedimento.$error.validar">Patr�n
						no v�lido</p>
									
							</div>
						</div>
					</div>					
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" ng-click="guardarParteInformacionAduanera(informacionAduanera)"
					class="btn btn-success btn-ok">
					Aceptar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
				<button type="button" class="btn btn-danger btn-warning" ng-click="cerrarModalParteInformacionAduanera()">
					Cancelar <span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span>

				</button>
			</div>
		</div>
	</div>
</div>