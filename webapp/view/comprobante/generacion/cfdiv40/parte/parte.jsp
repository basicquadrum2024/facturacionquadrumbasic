<div class="row">
<br>
<jsp:include page="modalParte.jsp"></jsp:include>
	<div class="col-md-4">
		<button class="btn btn-success btn-ok" data-toggle=""
			data-placement="left" title="Agregar Parte"
			ng-click="nuevaParte()" ng-disabled="comprobanteController.formConcepto.$invalid">
			<span class="glyphicon glyphicon-plus"></span> Nueva Parte
		</button>
	</div>
	
	<div class="table-responsive">
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>ClaveProdServ</th>
						<th>Cantidad</th>
						<th>Unidad</th>
						<th>Descripci&oacuten</th>
						<th>Valor Unitario</th>
						<th>Importe</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="parte in concepto.parte">
						<td>{{parte.claveProdServ}}</td>
						<td>{{parte.cantidad}}</td>
						<td>{{parte.unidad}}</td>
						<td>{{parte.descripcion}}</td>
						<td>{{parte.valorUnitario | currency}}</td>
						<td>{{parte.importe | currency}}</td>
						<td><a href="" ng-click="editarParte(parte)"
							class="btn btn-success btn-lg btn-xs" data-toggle="tooltip"
							data-placement="left" title="Editar Parte"> <span
								class="glyphicon glyphicon-edit"></span>
						</a> <a href="" ng-click="borrarParte(parte)"
							class="btn-danger btn-xs red-tooltip" data-toggle="tooltip"
							data-placement="right" title="Eliminar Parte"> <span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

</div>