<div class="row">
	<!-- <pre>{{comprobanteController.formInformacionComprobante | json}}</pre> -->


	<form id="comprobantev4Controller.formInformacionComprobante"
		name="comprobantev4Controller.formInformacionComprobante"
		class="form-horizontal" role="form">
		<!-- se cambia de posicion tipo de comprobante para realizar validaciones -->
		<div class="form-group">
			<label for="tipoDeComprobante" class="col-sm-4 control-label"><span
				ng-class="{ 'error-asterisco' : comprobantev4Controller.formInformacionComprobante.tipoDeComprobante.$invalid && !comprobantev4Controller.formInformacionComprobante.tipoDeComprobante.$pristine}">*
			</span> Tipo de comprobante:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.tipoDeComprobante.$invalid && !comprobantev4Controller.formInformacionComprobante.tipoDeComprobante.$pristine}">
				<select id="tipoDeComprobante" name="tipoDeComprobante"
					ng-model="comprobante.tipoDeComprobante" class="form-control"
					ng-change="muestraCfdiRelacionado(comprobante.tipoDeComprobante); validarCamposRequeridos(comprobante.tipoDeComprobante)"
					ng-options='tipoDeComprobante.clave as (tipoDeComprobante.clave+"-"+tipoDeComprobante.descripcion) for tipoDeComprobante in listaTiposComprobante'
					required>
					<option value="">-- Seleccione --</option>
				</select>
				<div
					ng-show="comprobantev4Controller.formInformacionComprobante.tipoDeComprobante.$dirty && comprobantev4Controller.formInformacionComprobante.tipoDeComprobante.$invalid">
					<p class="help-block"
						ng-show="comprobantev4Controller.formInformacionComprobante.tipoDeComprobante.$error.required">Campo
						obligatorio</p>
				</div>
			</div>
		</div>
		<!--  -->




		<div class="form-group">
			<label for="metodoPago" class="col-sm-4 control-label">*
				M�todo Pago:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.metodoPago.$invalid && !comprobantev4Controller.formInformacionComprobante.metodoPago.$pristine}">
				<!-- <input id="metodoPago" type="text" name="metodoPago"
					placeholder="Metodo Pago" class="form-control"
					ng-model="comprobante.metodoPago" /> -->
				<select id="metodoPago" name="metodoPago"
					ng-required="requeridoyActivoMetodo"
					ng-disabled="!requeridoyActivoMetodo"
					ng-change="validarFormaPago(comprobante.metodoPago)"
					ng-model="comprobante.metodoPago" class="form-control"
					ng-options='metodoPago.clave as (metodoPago.clave+"-"+metodoPago.descripcion) for metodoPago in listaMetodosPago'>
					<option value="">-- Seleccione --</option>
				</select>
				<p class="help-block"
					ng-show="comprobantev4Controller.formInformacionComprobante.metodoPago.$error.required">Campo
					obligatorio</p>
			</div>
		</div>
		<div class="form-group">
			<label for="formaPago" class="col-sm-4 control-label">* Forma
				Pago :</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.formaPago.$invalid && !comprobantev4Controller.formInformacionComprobante.formaPago.$pristine}">
				<!-- <input id="formaPago" type="text" name="formaPago"
					placeholder="Forma Pago" class="form-control"
					ng-model="comprobante.formaPago" /> -->

				<select id="formaPago" name="formaPago"
					ng-required="requeridoFormaPago" ng-disabled="bloqueaFormaPago"
					ng-model="comprobante.formaPago" class="form-control"
					ng-options='(formaPago.clave) as (formaPago.clave +"-" + formaPago.descripcion) for formaPago in listaFormasPago'>
					<option value="">-- Seleccione --</option>
				</select>
				<p class="help-block"
					ng-show="comprobantev4Controller.formInformacionComprobante.formaPago.$error.required">Campo
					obligatorio</p>
			</div>
		</div>
		<div class="form-group">
			<label for="exportacion" class="col-sm-4 control-label">*
				Exportacion :</label>
			<div class="col-sm-7">
				<select id="exportacion" name="exportacion"
					ng-model="comprobante.exportacion" class="form-control"
					ng-options='exp.clave as (exp.clave +"-" + exp.descripcion)  for exp in listaExportacion'
					required>
					<option value="">-- Seleccione --</option>
				</select>
				<p class="help-block"
					ng-show="comprobantev4Controller.formInformacionComprobante.exportacion.$error.required">Campo
					obligatorio</p>

			</div>
		</div>
		<div class="form-group">
			<label for="condicionesDePago" class="col-sm-4 control-label">Condiciones
				de Pago :</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.condicionesDePago.$invalid && !comprobantev4Controller.formInformacionComprobante.condicionesDePago.$pristine}">
				<input id="condicionesDePago" type="text" name="condicionesDePago"
					placeholder="Condiciones de Pago" class="form-control"
					ng-model="comprobante.condicionesDePago" />
			</div>
		</div>
		<div class="form-group">
			<label for="descuento" class="col-sm-4 control-label">Descuento:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.descuento.$invalid && !comprobantev4Controller.formInformacionComprobante.descuento.$pristine}">
				<input id="descuento" type="number" name="descuento"
					placeholder="Descuento" class="form-control"
					ng-model="comprobante.descuento" ng-disabled="true" />

			</div>
		</div>

		<div class="form-group">
			<label for="moneda" class="col-sm-4 control-label"><span
				ng-class="{ 'error-asterisco' : comprobantev4Controller.formInformacionComprobante.moneda.$invalid && !comprobantev4Controller.formInformacionComprobante.moneda.$pristine}">*
			</span> Moneda:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.moneda.$invalid && !comprobantev4Controller.formInformacionComprobante.moneda.$pristine}">
				<!-- <input id="moneda" type="text" name="moneda" placeholder="Moneda"
					class="form-control" required ng-model="comprobante.moneda" /> -->

				<select id="moneda" name="moneda" ng-model="comprobante.moneda"
					class="form-control"
					ng-options='moneda.clave as (moneda.clave+"-"+moneda.descripcion) for moneda in listaMonedas'
					ng-change="validaCampoMoneda(comprobante.moneda)">
					<option value="">-- Seleccione --</option>
				</select>
				<div
					ng-show="comprobantev4Controller.formInformacionComprobante.moneda.$dirty && comprobantev4Controller.formInformacionComprobante.moneda.$invalid">
					<p class="help-block"
						ng-show="comprobantev4Controller.formInformacionComprobante.moneda.$error.required">Campo
						obligatorio</p>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="tipoCambio" class="col-sm-4 control-label">Tipo
				Cambio:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.tipoCambio.$invalid && !comprobantev4Controller.formInformacionComprobante.tipoCambio.$pristine}">
				<input id="tipoCambio" type="number" name="tipoCambio"
					placeholder="Tipo Cambio" class="form-control"
					validar="'tipoCambio'" ng-model="comprobante.tipoCambio"
					ng-disabled="bloqueaTipoCambio" />
				<div
					ng-show="comprobantev4Controller.formInformacionComprobante.tipoCambio.$dirty && comprobantev4Controller.formInformacionComprobante.tipoCambio.$invalid">
					<p class="help-block"
						ng-show="comprobantev4Controller.formInformacionComprobante.tipoCambio.$error.required">Campo
						obligatorio</p>
					<p class="help-block"
						ng-show="comprobantev4Controller.formInformacionComprobante.tipoCambio.$error.validar">Patr�n
						no v�lido</p>
				</div>

			</div>

		</div>

		<div class="form-group" ng-show="comprobante.total>valorMaximo">
			<label for="confirmacion" class="col-sm-4 control-label">Confirmaci�n:</label>
			<div class="col-sm-7"
				ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.confirmacion.$invalid && !comprobantev4Controller.formInformacionComprobante.confirmacion.$pristine}">
				<input id="confirmacion" type="text" name="confirmacion"
					placeholder="Confirmaci�n" class="form-control"
					ng-model="comprobante.confirmacion" validar="'confirmacion'"
					ng-blur="validarNumeroConfirmacion(comprobante.confirmacion)" />
				<div
					ng-show="comprobantev4Controller.formInformacionComprobante.confirmacion.$dirty && comprobantev4Controller.formInformacionComprobante.confirmacion.$invalid">
					<p class="help-block"
						ng-show="comprobantev4Controller.formInformacionComprobante.confirmacion.$error.required">Campo
						obligatorio</p>
					<p class="help-block"
						ng-show="comprobantev4Controller.formInformacionComprobante.confirmacion.$error.validar">El
						campo confirmaci�n debe de contener 5 caracteres.</p>
					<p class="help-block"
						ng-show="comprobantev4Controller.formInformacionComprobante.confirmacion.$error.utilizado">La
						clave de confirmaci�n ya ha sido utilizada.</p>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label">Cfdi relacionado <input
				name="checkboxRelacion" id="checkboxRelacion" type="checkbox"
				ng-model="cdfdirelacionado" ng-click="llenaLsitaa(cdfdirelacionado)"></label><br />
		</div>

		<div>
			<div class="form-group" ng-show="cdfdirelacionado">
				<label for="tipoRelacion" class="col-sm-4 control-label">Tipo
					de relaci�n:</label>
				<div class="col-sm-7"
					ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.tipoRelacion.$invalid && !comprobantev4Controller.formInformacionComprobante.tipoRelacion.$pristine}">

					<select id="tipoRelacion" name="tipoRelacion"
						ng-model="relacionado.tipoRelacion" class="form-control"
						ng-options='tipoDeRelacion.clave as (tipoDeRelacion.clave+"-"+tipoDeRelacion.descripcion) for tipoDeRelacion in listaTipoRelacion'
						ng-required="cdfdirelacionado"></select>
					<div
						ng-show="comprobantev4Controller.formInformacionComprobante.tipoRelacion.$dirty && comprobantev4Controller.formInformacionComprobante.tipoRelacion.$invalid">
						<p class="help-block"
							ng-show="comprobantev4Controller.formInformacionComprobante.tipoRelacion.$error.required">Campo
							obligatorio</p>
					</div>
				</div>
			</div>

			<div class="form-group" ng-show="cdfdirelacionado">
				<label for="uuidRelacion" class="col-sm-4 control-label">Uuid
					Relacionado:</label>
				<div class="col-sm-7"
					ng-class="{ 'has-error' : comprobantev4Controller.formInformacionComprobante.uuidRelacion.$invalid && !comprobantev4Controller.formInformacionComprobante.uuidRelacion.$pristine}">
					<input id="uuidRelacion" type="text" name="uuidRelacion"
						placeholder="UUID relacionado" class="form-control"
						ng-model="relacionado.uuid" validar="'uuid'"
						ng-required="cdfdirelacionado" />
					<div
						ng-show="comprobantev4Controller.formInformacionComprobante.uuidRelacion.$dirty && comprobantev4Controller.formInformacionComprobante.uuidRelacion.$invalid">
						<p class="help-block"
							ng-show="comprobantev4Controller.formInformacionComprobante.uuidRelacion.$error.required">Campo
							obligatorio</p>
						<p class="help-block"
							ng-show="comprobantev4Controller.formInformacionComprobante.uuidRelacion.$error.validar">Estructura
							no v�lida del Folio Fiscal</p>


					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="cfdiGlobal" class="col-sm-4 control-label">Cfdi
					Global: <input name="checkboxGlobal" id="checkboxGlobal"
					type="checkbox" ng-model="cfdiGlobal">
				</label><br /> </label>
			</div>
			<div class="form-group" ng-show="cfdiGlobal">
				<label for="periodicidad" class="col-sm-4 control-label">Periodicidad:</label>

				<div class="col-sm-7">
					<input id="periodicidad" type="text" name="periodicidad"
						placeholder="Periodicidad" class="form-control"
						ng-model="comprobante.periodicidad" />
				</div>
			</div>

			<div class="form-group" ng-show="cfdiGlobal">
				<label for="meses" class="col-sm-4 control-label">Meses:</label>
				<div class="col-sm-7">
					<input id="periodicidad" type="text" name="meses"
						placeholder="meses" class="form-control"
						ng-model="comprobante.meses" />
				</div>
			</div>

			<div class="form-group" ng-show="cfdiGlobal">

				<label for="meses" class="col-sm-4 control-label">A�o:</label>
				<div class="col-sm-7">
					<input id="anio" type="text" name="anio" placeholder="anio"
						class="form-control" ng-model="comprobante.anio" />
				</div>
			</div>


		</div>

	</form>
</div>