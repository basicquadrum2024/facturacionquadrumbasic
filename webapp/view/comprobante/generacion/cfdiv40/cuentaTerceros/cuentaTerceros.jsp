<div class="row">
	<br>
	<jsp:include page="modalTerceros.jsp"></jsp:include>
	<div class="col-md-4">
		<button class="btn btn-success btn-ok" data-toggle="tooltip"
			data-placement="left" title="Agregar cuenta terceros"
			ng-click="nuevaIformacionTerceros()" ng-disabled="comprobantev4Controller.formConcepto.$invalid">
			<span class="glyphicon glyphicon-plus"></span> Nueva Información Cuenta Terceros
		</button>
	</div>

	<div class="table-responsive">
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Rfc cuenta terceros</th>
						<th>Nombre a cuenta terceros</th>
						<th>Regimen Fiscal a cuenta terceros</th>
						<th>Domicilio a cuenta terceros</th>
						
					</tr>
					
					
				</thead>
				<tbody>
					<tr >
						<th>{{concepto.cuentaTerceros.rfcACuentaTerceros}}</th>
						<th>{{concepto.cuentaTerceros.nombreCuentaTerceros}}</th>
						<th>{{concepto.cuentaTerceros.regimenFiscalTerceros}}</th>
						<th>{{concepto.cuentaTerceros.domicilioFiscalCuentaTerceros}}</th>
						<td><a href="" ng-click="borrarCuentaTerceros()"
							class="btn-danger btn-xs red-tooltip" data-toggle="tooltip"
							data-placement="right" title="Eliminar cuenta terceros"> <span
								class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>