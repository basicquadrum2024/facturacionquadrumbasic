<style>
.content-body {
	color:black;
}
</style>

<div id="modalCuentaTerceros" class="modal fade center-modal content-body">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">				
				Agregar Datos Cuenta Terceros
			</div>
			<div class="modal-body">
				<p>Los campos con * son requeridos.</p>
				<form id="comprobantev4Controller.formTerceros"
					name="comprobantev4Controller.formTerceros" class="form-horizontal"
					role="form">

					<div class="form-group">
						<label for="rfcACuentaTerceros" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobantev4Controller.formTerceros.rfcACuentaTerceros.$invalid && !comprobantev4Controller.formTerceros.rfcACuentaTerceros.$pristine}">*
						</span> RFC a Cuenta Terceros:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobantev4Controller.formTerceros.rfcACuentaTerceros.$invalid && !comprobantev4Controller.formTerceros.rfcACuentaTerceros.$pristine}">
							<input id="base" type="text" name="rfcACuentaTerceros" placeholder="rfc A CuentaTerceros"
								class="form-control" required ng-model="cuentaTerceros.rfcACuentaTerceros"
								 />
							<div
								ng-show="comprobantev4Controller.formTerceros.rfcACuentaTerceros.$dirty && comprobantev4Controller.formTerceros.rfcACuentaTerceros.$invalid">
								<p class="help-block"
									ng-show="comprobantev4Controller.formTerceros.rfcACuentaTerceros.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="impuesto" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobantev4Controller.formTerceros.nombreCuentaTerceros.$invalid && !comprobantev4Controller.formTerceros.nombreCuentaTerceros.$pristine}">*
						</span> Nombre a Cuenta Terceros:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobantev4Controller.formTerceros.nombreCuentaTerceros.$invalid && !comprobantev4Controller.formTerceros.nombreCuentaTerceros.$pristine}">							
							<input id="nombreCuentaTerceros" type="text" name="nombreCuentaTerceros" placeholder="Nombre a cuenta terceros"
								class="form-control" required ng-model="cuentaTerceros.nombreCuentaTerceros"
								 />
							<div
								ng-show="comprobantev4Controller.formCuentaTerceros.nombreCuentaTerceros.$dirty && comprobantev4Controller.formCuentaTerceros.nombreCuentaTerceros.$invalid">
								<p class="help-block"
									ng-show="comprobantev4Controller.formCuentaTerceros.nombreCuentaTerceros.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="impuesto" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobantev4Controller.formTerceros.regimenFiscalTerceros.$invalid && !comprobantev4Controller.formTerceros.regimenFiscalTerceros.$pristine}">*
						</span> Regimen Fiscal a cuenta terceros:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobantev4Controller.formTerceros.regimenFiscalTerceros.$invalid && !comprobantev4Controller.formTerceros.regimenFiscalTerceros.$pristine}">							
							<input id="regimenFiscalTerceros" type="text" name="regimenFiscalTerceros" placeholder="Regimen Fiscal a cuenta terceros"
								class="form-control" required ng-model="cuentaTerceros.regimenFiscalTerceros"
								 />
							<div
								ng-show="comprobantev4Controller.formCuentaTerceros.regimenFiscalTerceros.$dirty && comprobantev4Controller.formCuentaTerceros.regimenFiscalTerceros.$invalid">
								<p class="help-block"
									ng-show="comprobantev4Controller.formCuentaTerceros.regimenFiscalTerceros.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>
<div class="form-group">
						<label for="impuesto" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobantev4Controller.formTerceros.domicilioFiscalCuentaTerceros.$invalid && !comprobantev4Controller.formTerceros.domicilioFiscalCuentaTerceros.$pristine}">*
						</span> Domicilio Fiscal a Cuenta terceros:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobantev4Controller.formTerceros.domicilioFiscalCuentaTerceros.$invalid && !comprobantev4Controller.formTerceros.domicilioFiscalCuentaTerceros.$pristine}">							
							<input id="domicilioFiscalCuentaTerceros" type="text" name="domicilioFiscalCuentaTerceros" placeholder="Domicilio Fiscal cuenta terceros"
								class="form-control" required ng-model="cuentaTerceros.domicilioFiscalCuentaTerceros"
								 />
							<div
								ng-show="comprobantev4Controller.formCuentaTerceros.domicilioFiscalCuentaTerceros.$dirty && comprobantev4Controller.formCuentaTerceros.domicilioFiscalCuentaTerceros.$invalid">
								<p class="help-block"
									ng-show="comprobantev4Controller.formCuentaTerceros.domicilioFiscalCuentaTerceros.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>
                  </form>
			</div>
			<div class="modal-footer">
				<button type="button" ng-click="guardarCuentaTerceros(cuentaTerceros)"
					class="btn btn-success btn-ok">
					Aceptar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
				<button type="button" class="btn btn-danger btn-warning"
					ng-click="cerralModalCuentaTerceros()">
					Cancelar <span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span>

				</button>
			</div>
		</div>
	</div>
</div>