<div ng-controller="generacionComprobanteV40Controller as comprobantev4Controller" >
	<div>
	<div><h1>Genera Comprobante versión 4.0</h1></div>
	<div class="btn-group header-comprobante">
	<button class="btn" ng-class="{'btn-primary':esPasoActual(0)}"
				ng-click="asignarPasoActual(0)" ng-disabled="true">Paso 1. Capturar tus datos
				fiscales</button> &nbsp; &nbsp; &nbsp;
			<button class="btn" ng-class="{'btn-primary':esPasoActual(1)}"
				ng-click="asignarPasoActual(1)" ng-disabled="true">Paso 2. Capturar conceptos</button> &nbsp; &nbsp; &nbsp;
			<button class="btn" ng-class="{'btn-primary':esPasoActual(2)}"
				ng-click="asignarPasoActual(2)" ng-disabled="true">Paso 3. Confirmación de
				datos</button>
	
	</div>
	<div ng-switch="obtenerPasoActual()" class="slide-frame">
			<div ng-switch-when="uno" class="wave">
				<fieldset>
					<h1>Capturar datos fiscales</h1>
					<hr/>
					<jsp:include page="receptor/formReceptor.jsp"></jsp:include>
				</fieldset>
			</div>
			<div ng-switch-when="dos" class="wave">
				<fieldset>
					<h1>Capturar Conceptos</h1>
					<jsp:include page="conceptos/conceptos.jsp"></jsp:include>
				</fieldset>
			</div>
			<div ng-switch-when="tres" class="wave">
				<fieldset>
					<h1>Información del comprobante</h1>
					<jsp:include
						page="informacionComprobante/informacionComprobante.jsp"></jsp:include>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			<div align="left">
				<a class="btn btn-default btn-warning" ng-click="manejarAnterior()"
					ng-show="!esPrimerPaso()"><span
					class="glyphicon glyphicon-chevron-left "></span> Anterior</a>
			</div>

		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
		<div>
		 
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			<div align="right">
				<a class="btn btn-primary btn-sm btn-ok" ng-click="manejarSiguiente()"
					ng-hide="esUltimoPaso()">{{obtenerSiguienteEtiqueta()}} <span
					class="glyphicon glyphicon-chevron-right"></span>
				</a>
            
				<button ng-show="esUltimoPaso()" type="button"
					ng-disabled="comprobantev4Controller.formInformacionComprobante.$invalid"
					ng-click="generarComprobante()" class="btn btn-success btn-ok">
					Facturar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
			</div>
		</div>
	</div>
	
	<%@include file="../modalReutilizable.jsp"%>	

</div>