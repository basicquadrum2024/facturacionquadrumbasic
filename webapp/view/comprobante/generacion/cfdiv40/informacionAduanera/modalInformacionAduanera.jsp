<div id="modalinformacionAduanera" class="modal fade center-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <button type="button" class="close" ng-click="close()"
					data-dismiss="modal" aria-hidden="true">&times;</button> -->
				Agregar Informaci�n Aduanera
			</div>
			<div class="modal-body">
				<p>Los campos con * son requeridos.</p>
				<form id="forminformacionAduanera" name="comprobantev4Controller.formInformacionAduanera"
					class="form-horizontal" role="form">
<br>
<br>
					<div class="form-group">
						<label for="numeroPedimento" class="col-sm-4 control-label"><span
							ng-class="{ 'error-asterisco' : comprobantev4Controller.formInformacionAduanera.numeroPedimento.$invalid && !comprobantev4Controller.formInformacionAduanera.numeroPedimento.$pristine}">*
						</span> N�mero Pedimento:</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : comprobantev4Controller.formInformacionAduanera.numeroPedimento.$invalid && !comprobantev4Controller.formInformacionAduanera.numeroPedimento.$pristine}">
							
							<input id="numeroPedimento" type="text" name="numeroPedimento"
								placeholder="N�mero Pedimento" class="form-control" required
								ng-model="informacionAduanera.numeroPedimento" data-tooltip="Ejemplo n�mero de pedimento, 56--45--4778--7854968 (Los - representan espacios)"
								validar="'numeroPedimento'"
								/>
							<div
								ng-show="comprobantev4Controller.formInformacionAduanera.numeroPedimento.$dirty && comprobantev4Controller.formInformacionAduanera.numeroPedimento.$invalid">
								<p class="help-block"
									ng-show="comprobantev4Controller.formInformacionAduanera.numeroPedimento.$error.required">Campo
									obligatorio</p>
									<p class="help-block"
						ng-show="comprobantev4Controller.formInformacionAduanera.numeroPedimento.$error.validar">Patr�n
						no v�lido</p>
									
							</div>
						</div>
					</div>					
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" ng-click="guardarInformacionAduanera(informacionAduanera)"
					class="btn btn-success btn-ok">
					Aceptar <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
				<button type="button" class="btn btn-danger btn-warning" ng-click="cerrarModalinformacionAduanera()">
					Cancelar <span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span>

				</button>
			</div>
		</div>
	</div>
</div>