<style>
.content-body {
	color:black;
}
</style>

<div id="modalReutilizable" class="modal fade content-body">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="color: black;">{{title}}</h4>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <div class="modal-body">
        <div class="row" ng-if="resultado.timbrado">
          <div ng-if="resultado.error === 1"><!-- Timbrado -->
            <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok-sign"></span><strong> Exito!</strong> {{resultado.errores}}
          </div>
          <div class="alert alert-info" role="alert">
          <!--  
            <h4 class="alert-heading"><span class="glyphicon glyphicon-list"></span><strong> Detalles:</strong></h4>
            -->
            <hr>
            <strong>Folio Fiscal : </strong>{{resultado.uuid}}
            <hr>
            <div class="row">
              <div class="col-lg-6"><strong>Folio : </strong>{{resultado.folio}}</div>
              <div class="col-lg-6"><strong>Serie : </strong>{{resultado.serie}}</div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-lg-offset-1">
              <div class="card text-center">
              	<a href="documentos/descarga.json?opcion=xml&id={{resultado.id}}">
                <img class="card-img-top" src="resources/img/file_xml.png" alt="xml">
                </a>
			    <div class="card-body">
			      <h4 class="card-title">Descargar archivo XML</h4>
			      <h6 class="card-subtitle mb-2 text-muted">Click para descargar el archivo xml y guardarlo en su computadora.</h6>
			     <!--  <a href="documentos/descarga.json?opcion=xml&id={{resultado.id}}" class="btn btn-primary">Descargar</a> --><!--  -->
			    </div>
			  </div>
            </div>
            <div class="col-lg-4 col-lg-offset-1">
              <div class="card text-center">
              	<a href="documentos/descarga.json?opcion=pdf&id={{resultado.id}}">
                <img class="card-img-top" src="resources/img/file_pdf.png" alt="pdf">
                </a>
			    <div class="card-body">
			      <h4 class="card-title">Descargar archivo PDF</h4>
			      <h6 class="card-subtitle mb-2 text-muted">Click para descargar la representación impresa y guardarla en su computadora.</h6>
			      <!-- <a href="documentos/descarga.json?opcion=pdf&id={{resultado.id}}" class="btn btn-primary">Descargar</a> -->
			    </div>
			  </div>
            </div>
          </div>
          </div>
          <div ng-if="resultado.error === 2"><!-- Cancelado -->
            
            <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok-sign"></span><strong> Mensaje!</strong> {{resultado.opcion}}
          </div>
          <div class="alert alert-info" role="alert">
            <h4 class="alert-heading"><span class="glyphicon glyphicon-list"></span><strong> Detalles:</strong></h4>
            <hr>
            <strong>Folio fiscal de la factura: </strong>{{resultado.uuid}}
            
            <div class="row" ng-show="resultado.id != null">
            <div class="col-lg-4 col-lg-offset-4">
              <div class="card text-center">
                <img class="card-img-top" src="resources/img/file_xml.png" alt="xml">
			    <div class="card-body">
			      <h4 class="card-title">Descargar acuse de cancelación XML</h4>
			      <h6 class="card-subtitle mb-2 text-muted">Click para descargar el archivo xml y guardarlo en su computadora.</h6>
			      <a href="documentos/descarga.json?opcion=xml&id={{resultado.id}}" class="btn btn-primary">Descargar</a><!--  -->
			    </div>
			  </div>
            </div>
            </div>
            
          </div>
          </div>  
        </div>
        <div class="row" ng-if="!resultado.timbrado"><!-- Error -->
          
          
            <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-ok-sign"></span><strong> Error!</strong> {{resultado.uuid}}{{resultado.opcion}}
            
            
            
          </div>
         
        </div>
        <!-- <pre>{{$scope.resultadoWrappers}}</pre> -->
      </div>
      <div class="modal-footer">
      	<button id="aceptarTimbrado" type="button" class="btn btn-success btn-ok" data-dismiss="modal" ng-click="reinicioValores(resultado.timbrado)"> Aceptar <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></button>
      </div>
    </div>
  </div>
</div>