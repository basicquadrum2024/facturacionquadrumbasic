<style>
.content-body {
	color:black;
}
</style>
<div class="modal fade center-modal content-body">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" ng-click="close()"
					data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: white;">Alta de Conceptos</h4>
			</div>
			<div class="modal-body"style="text-align: justify !important;">
			<div class="form-group">
			<form name="formModalCon" >
						<div class="col-md-4">
			<div class="form-group"
				ng-class="{ 'has-error' : formModalCon.descripcion.$invalid}">
				<label class="control-label" for="descripcion">*
					Descripción:</label>
				<div>
					<input class="form-control" type="text" id="descripcion"
						name="descripcion" placeholder="Descripción"
						ng-model="conceptos.descripcion" ng-maxlength="1000" paste-trimed
						capitalize required="required">
					<p ng-show="formModalCon.descripcion.$error.required"
						class="help-block">Descripcion es requerido.</p>
				</div>
				</div>
				<div class="form-group">
				 <button ng-disabled="!formModalCon.$valid" ng-click="guardarConcepto();" data-dismiss="modal" class="btn btn-primary">Guardar</button>
				 </div>
			</div>
		</form>
					</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>