<div id="modalPrincipal" class="modal fade center-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" ng-click="close()"
					data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: white;">{{mensaje}}</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form name="frmUsers">
						<!--  <div class="col-md-4">
						<label class="control-label" for="rfc">RFC <span
							ng-if="frmuser.rfc.$dirty && frmuser.rfc.$invalid"
							style="color: #a94442">*</span> <span
							ng-if="!(frmuser.rfc.$dirty && frmuser.rfc.$invalid)">*</span>
							:
						</label>
					 		<div ng-class="{ 'has-error' : frmuser.rfc.$invalid && !frmuser.rfc.$pristine}">
								<input class="form-control" type="text" id="rfc" name="rfc"
									placeholder="RFC" ng-model="user.rfc" maxlength="13" autocomplete="off"
									ng-disabled="user.id!=null" capitalize  paste-trimed
									data-placement="top" required uppercase>
							</div> 
						</div>-->
						
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="correo">Email <span
									ng-if="frmUsers.correo.$dirty && frmUsers.correo.$invalid"
									style="color: #a94442">*</span> <span
									ng-if="!(frmUsers.correo.$dirty && frmUsers.correo.$invalid)">*</span>
									:
								</label>
								<div ng-class="{ 'has-error' : frmUsers.correo.$invalid && !frmUsers.correo.$pristine}">
									<input class="form-control" type="email" id="correo"
										name="correo" placeholder="Correo" ng-model="user.correo"
										autocomplete="off" ng-maxlength="45" maxlength="46"
										validar="'correo'"
										data-tooltip="Ingresa el correo" data-placement="top" ng-blur="validaUCorreo()" required paste-trimed>
									<div ng-show="frmUsers.correo.$dirty && frmUsers.correo.$invalid">
										<p class="help-block" ng-show="frmUsers.correo.$error.required">Campo obligatorio</p>
									</div>
									<div ng-show="frmUsers.correo.$dirty">
										<p class="help-block" text-danger ng-show="frmUsers.correo.$error.validar">Correo inválido</p>
									</div>
									<div class="alert alert-danger"
										ng-show="frmUsers.correo.$error.maxlength">El correo electrónico sobrepasa los 45 carácteres.</div>
								</div>
							</div>
						</div>
						
						<!--  <div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="perfil">Perfil <span
									ng-if="frmUsers.perfil.$dirty && frmUsers.perfil.$invalid"
									style="color: #a94442">*</span> <span
									ng-if="!(frmUsers.perfil.$dirty && frmUsers.perfil.$invalid)">*</span>
									:</label>
								<div ng-class="{ 'has-error' : frmUsers.perfil.$invalid && !frmUsers.perfil.$pristine}">
									<select class="form-control" id="perfil" name="perfil"
										ng-model="user.tc_perfil_id.id" required
										ng-options="perfil.id as perfil.nombre for perfil in perfils">
										<option value="">--Selecciona--</option>
									</select>
									<div ng-show="frmUsers.perfil.$dirty && frmUsers.perfil.$invalid">
										<p class="help-block" ng-show="frmUsers.perfil.$error.required">Campo
											obligatorio</p>
									</div>	
								</div>
							</div>
						</div>-->
						
					</form>

				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-sm" ng-click="close()" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove-circle"
						aria-hidden="true"></span> Cancelar 
				</button>
				<button type="button" ng-disabled="frmUsers.$invalid"
					ng-click="saveUser()" class="btn btn-success btn-sm" data-dismiss="modal">
					<span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span> {{!user.id ? 'Guardar' : 'Actualizar'}}
				</button>
			</div>
		</div>
	</div>
</div>