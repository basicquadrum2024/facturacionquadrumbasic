<div class="modal center-modal-restore">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title">P�gina caducada</h4>
      </div>
      <div class="modal-body">
        <p>Por favor hacer click en el bot�n aceptar para volver al inicio de sesi�n.</p>		
      </div>
      <div class="modal-footer">
        <button type="button" onclick="window.location = '<%=request.getContextPath()%>/index.jsp';" class="btn btn-primary" data-dismiss="modal">Aceptar
        	<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>		
        </button>       
      </div>
    </div>
  </div>
</div>