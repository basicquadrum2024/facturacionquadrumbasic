<style>
#scroll {
	border: 1px solid;
	height: 1000px;
	width: 700px;
	overflow-y: scroll;
	overflow-x: hidden;
}
</style>
<div class="form-create-account content" id="preguntas-sat">
	<div>
		<div>
			<div class="modal-header">
				<button id="close-window-preguntasSAT" type="button" class="close"
					style="color: white;" data-dismiss="modal" aria-hidden="true" ng-click="cancel()" >&times;</button>
				<h4 class="modal-title">PREGUNTAS HOMOLOGADAS</h4>

			</div>
			<div id="scroll">
				<div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
					<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
						<jsp:include page="/view/alert.jsp"></jsp:include>
					</div>
				</div>
				<table class="table table-striped">
					<thead>

					</thead>
					<tbody>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">1. �Se deber� cancelar el CFDI
								cuando el receptor dar� un uso diferente al se�alado en el campo
								UsoCFDI?</td>

						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No, en caso de que
								se registre una clave distinta al uso del CFDI que le dar� el
								receptor del comprobante, no ser� motivo de cancelaci�n o
								sustituci�n, y no afectar� para su deducci�n o acreditamiento de
								impuestos.<a style="color: blue;">Fundamento Legal:</a> Anexo 20 Gu�a de llenado de los
								comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT .</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">2. �En el CFDI versi�n 4.0 se
								podr�n registrar cantidades en negativo?</td>
						</tr>

						<tr>
							<th  style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No, en la versi�n 4.0 del CFDI no aplica el uso de
								n�meros negativos para ning�n dato.<a style="color: blue;"> Fundamento Legal:</a> Anexo 20
								versi�n 4.0 vigente.</td>

						</tr>

						<tr>
							<th scope="row"></th>
							<td style="color: blue;">3. �C�mo se deben reflejar los
								impuestos retenidos y trasladados en el CFDI versi�n 4.0?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En la versi�n 4.0 del CFDI se expresar�n los
								impuestos trasladados y retenidos aplicables por cada concepto
								registrado en el comprobante, debi�ndose detallar lo siguiente:
								.Base para el c�lculo del impuesto. .Impuesto (Tipo de impuesto
								ISR, IVA, IEPS). .Tipo factor (Tasa, cuota o exento). .Tasa o
								cuota (Valor de la tasa o cuota que corresponda al impuesto).
								.Importe (Monto del impuesto). Se debe incluir a nivel
								comprobante el resumen de los impuestos trasladados por Tipo de
								impuesto, Tipo factor, Tasa o cuota e Importe. Se debe incluir a
								nivel comprobante el resumen de los impuestos retenidos por
								Impuesto e Importe. Asimismo, se debe registrar en su caso, el
								Total de los Impuestos Trasladados y/o Retenidos. <a style="color: blue;">Fundamento
								Legal:</a> Art�culo 29-A, fracci�n VII, inciso a), primer y segundo
								p�rrafo del C�digo Fiscal de la Federaci�n, Anexo 20 versi�n 4.0
								vigente y Anexo 20 Gu�a de llenado de los comprobantes fiscales
								digitales por internet versi�n 4.0, publicada en el Portal del
								SAT .</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">4. �En qu� caso los campos
								condicionales del CFDI son de uso obligatorio?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Los campos
								condicionales deber�n informarse ser�n obligatorios- siempre que
								se registre informaci�n en alg�n otro campo que como resultado
								de las reglas de validaci�n contenidas en el est�ndar t�cnico y
								precisadas en la Gu�a de llenado, obligue en consecuencia a que
								se registre informaci�n en dichos campos condicionales.
								<a style="color: blue;">Fundamento</a> Legal: Anexo 20 versi�n 4.0 vigente y Anexo 20 Gu�a
								de llenado de los comprobantes fiscales digitales por internet
								versi�n 4.0, publicada en el Portal del SAT.</td>

						</tr>

						<tr>
							<th scope="row"></th>
							<td style="color: blue;">5. �C�mo se deben clasificar los
								productos y servicios de acuerdo con el cat�logo publicado por
								el SAT (c_ClaveProdSev)?</td>
						</tr>

						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. La clasificaci�n
								del cat�logo se integra de acuerdo con las caracter�sticas
								comunes de los productos y servicios, y si est�n
								interrelacionados, la cual se estructura de la siguiente manera:
								.Divisi�n: Se identifica por el primero y segundo d�gito de la
								clave. .Grupo: Se identifica por el tercero y cuarto d�gito de
								la clave. .Clase: Se identifica por el quinto y sexto d�gito de
								la clave. .Producto: Se identifica por el s�ptimo y octavo
								d�gito de la clave. Un ejemplo es la clave 10101502:


								<table class="table table-dark">
									<thead>

									</thead>
									<tbody>
										<tr>

											<td style="color: blue;">Divisi�n</td>
											<td style="color: blue;">Grupo</td>
											<td style="color: blue;">Clase</td>
											<td style="color: blue;">Mercanc�a o Producto</td>
											<td style="color: blue;">Descripci�n</td>
										</tr>
										<tr>

											<td style="color: blue;">D�gitos verificadores para
												fines de an�lisis</td>
											<td style="color: blue;">D�gitos agrupadores para
												categor�as de productos relacionados entre s�</td>
											<td style="color: blue;">D�gitos para identificar a los
												productos que comparten caracter�sticas comunes</td>
											<td style="color: blue;">Grupo de productos o servicios
												sustitutivos</td>
											<td style="color: blue;">Definici�n del producto o
												servicio</td>
										</tr>
										<tr>

											<td style="color: blue;">10</td>
											<td style="color: blue;">10</td>
											<td style="color: blue;">15</td>
											<td style="color: blue;">02</td>
											<td style="color: blue;">Perros</td>
										</tr>
									</tbody>
								</table> Se debe registrar una clave que permita clasificar los
								conceptos del comprobante, los cuales se deber�n asociar a nivel
								Clase, es decir, cuando los �ltimos dos d�gitos tengan el valor
								cero "0", no obstante, se podr�n asociar a nivel Producto,
								siempre y cuando la clave est� registrada en el cat�logo.

								<a style="color: blue;">Fundamento Legal:</a> Anexo 20 versi�n 4.0 vigente y Anexo 20 Gu�a
								de llenado de los comprobantes fiscales digitales por internet
								versi�n 4.0, publicada en el Portal del SAT.
							</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">6. �En los CFDI por anticipos se
								debe desglosar el IVA?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. S�, se debe
								desglosar el IVA en las facturas que amparen anticipos cuando el
								bien o producto a adquirir grave IVA. Fundamento Legal:
								Art�culos 1 y 1-B de la LIVA y Anexo 20 Gu�a de llenado de los
								comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">7. El cliente se equivoc� y pag� de
								m�s o indebidamente, �Se tiene que emitir una factura?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Si el cliente pag�
								de m�s o indebidamente y la cantidad que est� en demas�a no se
								va a considerar como un anticipo, se deber� devolver al cliente
								el importe pagado de m�s. En el caso, de que la cantidad pagada
								de m�s o indebidamente se tome como un anticipo, se deber�
								emitir el CFDI de conformidad con lo establecido en el Ap�ndice
								6 Procedimiento para la emisi�n de los CFDI en el caso de
								anticipos recibidos.<a style="color: blue;"> Fundamento Legal:</a> Art�culo 29 del C�digo
								Fiscal de la Federaci�n y Anexo 20 Gu�a de llenado de los
								comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">8. �C�mo se deben incluir los
								impuestos locales en el CFDI versi�n 4.0?</td>
						</tr>

						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Los impuestos
								locales se deben registrar en el "Complemento Impuestos
								Locales", publicado en el Portal del SAT. <a style="color: blue;">Fundamento Legal:</a>
								Regla 2.7.1.8. de la Resoluci�n Miscel�nea Fiscal vigente y
								Ap�ndice 1 Notas Generales del Anexo 20 Gu�a de llenado de los
								comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>

						<tr>
							<th scope="row"></th>
							<td style="color: blue;">9. �El contribuyente receptor del
								CFDI tiene la obligaci�n de validar a detalle las claves de
								producto/servicio de todas las facturas que reciba?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No existe una
								obligaci�n de revisarlas a detalle; la recomendaci�n es que se
								verifiquen que los datos asentados sean correctos y coincidan al
								menos en t�rminos generales con el bien o servicio de que se
								trate y la descripci�n que del mismo se asiente en el propio
								comprobante. <a style="color: blue;">Fundamento Legal:</a> Anexo 20 versi�n 4.0 vigente y
								Anexo 20 Gu�a de llenado de los comprobantes fiscales digitales
								por internet versi�n 4.0, publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">10. Si mis clientes no han
								actualizado su domicilio a Ciudad de M�xico �Yo deb� cambiarlo a
								efecto de registrar el mismo en el CFDI?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Para la versi�n
								4.0 del CFDI se eliminan los campos del domicilio del emisor y
								receptor. En el lugar de expedici�n en la versi�n 4.0 del CFDI,
								se debe registrar el c�digo postal del lugar de expedici�n del
								comprobante (de la matriz o de la sucursal). El c�digo postal
								debe corresponder con una clave incluida en el cat�logo
								c_CodigoPostal publicado en el Portal del SAT. <a style="color: blue;">Fundamento Legal:</a>
								Art�culo 29-A, fracci�n III del C�digo Fiscal de la Federaci�n y
								Anexo 20 Gu�a de llenado de los comprobantes fiscales digitales
								por internet versi�n 4.0, publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">11. �Cu�l es el m�todo de pago que
								se debe registrar en el CFDI por el valor total de la operaci�n
								en el caso de pago en parcialidades o pago diferido?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe registrar
								la clave PPD (Pago en parcialidades o diferido) del cat�logo
								c_MetodoPago publicado en el Portal del SAT. <a style="color: blue;">Fundamento Legal:</a>
								Anexo 20 Gu�a de llenado de los comprobantes fiscales digitales
								por internet versi�n 4.0, publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">12. �Qu� sucede si clasifico de
								manera err�nea en el CFDI la clave de los productos o servicios?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En caso de que se
								asigne "err�neamente" la clave del producto o servicio se debe
								reexpedir la factura para corregirlo. Para clasificar los
								productos y servicios que se facturan, debe consultar el
								Ap�ndice 3 del Anexo 20 Gu�a de llenado de los comprobantes
								fiscales digitales por Internet versi�n 4.0, publicada en el
								Portal del SAT, y puede utilizarse la herramienta de
								clasificaci�n publicada en el mismo Portal. <a style="color: blue;">Fundamento Legal:</a>
								Ap�ndice 3 Clasificaci�n de Productos y Servicios del Anexo 20
								Gu�a de llenado de los comprobantes fiscales digitales por
								internet versi�n 4.0, publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">13. �C�mo se deben registrar en los
								CFDI los conceptos exentos de impuestos?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En el
								Nodo:Traslados se debe expresar la informaci�n detallada del
								impuesto, de la siguiente forma: Base para el c�lculo del
								impuesto. Impuesto (Tipo de impuesto ISR, IVA, IEPS). Tipo
								factor (exento). No se deben registrar los atributos TasaOCuota
								e Importe. <a style="color: blue;">Fundamento Legal:</a> Anexo 20 versi�n 4.0 vigente.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">14. En el caso de que un CFDI haya
								sido pagado con diversas formas de pago �Qu� forma de pago debe
								registrarse en el comprobante?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En el caso de
								aplicar m�s de una forma de pago en una transacci�n, los
								contribuyentes deben incluir en este campo, la clave de forma de
								pago con la que se liquida la mayor cantidad del pago. En caso
								de que se reciban distintas formas de pago con el mismo importe,
								el contribuyente debe registrar a su consideraci�n, una de las
								formas de pago con las que se recibi� el pago de la
								contraprestaci�n.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">15. Si me realizan un dep�sito para
								garantizar el pago de las rentas en el caso de un contrato de
								arrendamiento inmobiliario, �Se debe facturar como un anticipo
								dicho dep�sito?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Si la operaci�n de
								que se trata se refiere a la entrega de una cantidad por
								concepto de garant�a o dep�sito, es decir, la entrega de una
								cantidad que garantiza la realizaci�n o cumplimiento de alguna
								condici�n, como sucede en el caso del dep�sito que en ocasiones
								se realiza por el arrendatario al arrendador para garantizar el
								pago de las rentas en el caso de un contrato de arrendamiento
								inmobiliario, no estamos ante el caso de un anticipo.<a style="color: blue;"> Fundamento
								Legal:</a> Anexo 20 Gu�a de llenado de los comprobantes fiscales
								digitales por internet versi�n 4.0, publicada en el Portal del
								SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">16. �En qu� casos se deber� emitir
								un CFDI por un anticipo?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Estaremos ante el
								caso de una operaci�n en d�nde existe el pago de un anticipo,
								cuando: .No se conoce o no se ha determinado el bien o servicio
								que se va a adquirir o el precio del mismo. .No se conoce o no
								se ha determinado ni el bien o servicio que se va a adquirir ni
								el precio del mismo.<a style="color: blue;"> Fundamento Legal:</a> Anexo 20 Gu�a de llenado
								de los comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">17. Si tengo varias sucursales,
								pero mis sistemas de facturaci�n se encuentran en la matriz �Qu�
								c�digo postal debo registrar en el campo lugar de expedici�n en
								el CFDI?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En el caso de que
								se emita un comprobante fiscal en una sucursal, en dicho
								comprobante se debe registrar el c�digo postal de �sta,
								independientemente de que los sistemas de facturaci�n de la
								empresa se encuentren en un domicilio distinto al de la
								sucursal.<a style="color: blue;"> Fundamento Legal:</a> Art�culo 29-A, fracciones I y III
								del C�digo Fiscal de la Federaci�n, Anexo 20 Gu�a de llenado de
								los comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">18. �En qu� apartado del CFDI se
								pueden expresar las penalizaciones o incumplimientos en el caso
								de contratos de obras p�blicas?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se podr�n incluir
								en el nodo "Addenda" <a style="color: blue;">Fundamento Legal:</a> Anexo 20 Gu�a de llenado
								de los comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">19. �Qu� clave de unidad de medida
								se debe utilizar para facturar servicios?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. La clave de unidad
								de medida depender� del tipo de servicio y del giro del
								proveedor, y �sta se identifica utilizando el cat�logo
								c_ClaveUnidad publicado en el Portal del SAT, puede haber m�s de
								una unidad de medida aplicable a un servicio o producto, como se
								puede apreciar en los siguientes ejemplos: Un servicio de
								transporte terrestre puede estar clasificado por distancia
								(KMT), por peso transportado (KGM), por pasajero/asiento (IE -
								persona), o por viaje (E54). Un servicio de hospedaje puede
								estar medido por habitaciones (ROM), tiempo transcurrido (DAY),
								personas (IE). Los servicios administrativos y profesionales se
								pueden dar por tiempo (HUR - hora, DAY, etc.), por actividades
								(ACT), por grupos atendidos (10), por tiempo- hombre (3C - mes
								hombre). <a style="color: blue;">Fundamento Legal:</a> Cat�logos del CFDI versi�n 4.0,
								publicado en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">20. Si a mi cliente le otorgo un
								descuento sobre el total de una factura despu�s de haberla
								emitido �Qu� tipo de CFDI debo emitir?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe de emitir
								un CFDI de egresos. Si el descuento lo aplican cuando se realiza
								la venta o prestaci�n del servicio, en el CFDI que se emita se
								puede aplicar el descuento a nivel concepto. <a style="color: blue;">Fundamento Legal:</a>
								Art�culo 29, pen�ltimo p�rrafo del C�digo Fiscal de la
								Federaci�n y 25, primer p�rrafo, fracci�n I de la Ley del
								Impuesto sobre la Renta.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">21. �Qu� tipo de cambio podr�n
								utilizar los integrantes del sistema financiero en la emisi�n
								del CFDI?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Podr�n utilizar en
								tipo de cambio FIX, del �ltimo d�a del mes, de la fecha de
								emisi�n o del d�a del corte del CFDI para operaciones en d�lares
								de los EUA, y en el caso de monedas distintas, el que
								corresponda conforme a la tabla de Equivalencias la �ltima que
								haya sido publicada por BANXICO.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">22. �Cu�l es la clave de forma de
								pago que deben utilizar los integrantes del sistema financiero
								para la emisi�n de los CFDI?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. La clave que deben
								utilizar es la "03" Transferencia electr�nica de fondos,
								contenida en el cat�logo c_FormaPago del Anexo 20. <a style="color: blue;">Fundamento
								legal:</a> Anexo 20 versi�n 4.0 vigente.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">23. En el campo "LugarExpedicion"
								del CFDI, �Qu� c�digo postal deben de registrar los integrantes
								del sistema financiero?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Deben registrar el
								c�digo postal del domicilio fiscal de la instituci�n financiera.

								<a style="color: blue;">Fundamento Legal: </a>Art�culo 29-A, fracciones I y III del C�digo
								Fiscal de la Federaci�n y Anexo 20 Gu�a de llenado de los
								comprobantes fiscales digitales por Internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">24. �Cu�l es la clave de unidad que
								deben utilizar por los servicios que prestan los integrantes del
								sistema financiero para la emisi�n de los CFDI?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. La clave de unidad
								que pueden utilizar es la "E48" (Unidad de servicio), contenida
								en el cat�logo c_ClaveUnidad del Anexo 20. <a style="color: blue;">Fundamento legal:</a>
								Anexo 20 versi�n 4.0 vigente.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">25. �Cu�l es la clave de productos
								o servicios, que deben utilizar para clasificar los servicios
								que prestan los integrantes del sistema financiero en la emisi�n
								de los CFDI?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. La clave de
								productos o servicios que pueden utilizar es la "84121500"
								(Instituciones bancarias), contenida en el cat�logo
								c_ClaveProdServ del Anexo 20. Lo anterior, sin menoscabo de que
								los integrantes del sistema financiero puedan, por la naturaleza
								del servicio prestado, clasificar �ste de manera particular.

								<a style="color: blue;">Fundamento legal: </a>Anexo 20 versi�n 4.0 vigente y Art�culo 7 de
								la Ley del lmpuesto sobre la Renta.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">26. Para efectos de la emisi�n de
								CFDI a que se refiere la regla 2.7.1.20., en los casos en los
								cuales �ste deba emitirse por conceptos totalmente en ceros, los
								integrantes del sistema financiero en la generaci�n de los
								mismos, podr�n considerar lo siguiente:</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Los integrantes
								del sistema financiero, podr�n ingresar un cargo con valor de un
								centavo o la cantidad que en su caso determinen por concepto de"
								Servicios de Facturaci�n ", con la clave de productos o
								servicios "84121500"" (Instituciones bancarias) y con clave de
								unidad "E48"" (Unidad de servicio), incluyendo en el mismo
								concepto un descuento por el mismo monto.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">27. �Las facturas se pueden pagar
								con bienes o servicios?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No, no existe en
								el cat�logo la forma de pago en especie o servicios, derivado de
								que la persona que pretende pagar con bienes est� realizando la
								enajenaci�n de un bien, por lo tanto, debe emitir un CFDI de
								ingresos por ese bien que est� enajenando, por otra parte, si la
								persona que pretende pagar lo realiza con la prestaci�n de un
								servicio, debe emitir un CFDI por dicho servicio. Tanto en el
								caso de la enajenaci�n de bienes, como en la prestaci�n de
								servicios se considera que el cliente y el proveedor son el
								mismo contribuyente, por lo tanto, se puede aplicar la forma de
								pago "17" Compensaci�n. <a style="color: blue;">Fundamento legal:</a> Art�culo 14 del C�digo
								Fiscal de la Federaci�n.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">28. �Podr�a el SAT imponer una
								sanci�n por considerar que la clave de producto y servicio que
								se registre en una factura es err�nea?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. La identificaci�n
								de la clave de producto y servicio corresponde al contribuyente
								emisor de la factura, esto en raz�n de que �l es quien conoce la
								naturaleza y las caracter�sticas del producto o servicio que
								ampara el comprobante. Si se registra una clave de producto o
								servicio que se asemeje o tenga relaci�n en sus caracter�sticas
								al producto que se va a facturar, esto por no haber encontrado
								una clave espec�fica, dicha situaci�n no ser� motivo de sanci�n
								por parte de la autoridad salvo en casos de notoria y evidente
								incongruencia entre lo facturado y la clave de producto y
								servicio registrada, por ejemplo: Registro la clave de producto
								y servicio 82101500 que corresponde a "Publicidad impresa", pero
								en realidad soy una enfermera que me dedico a cuidar personas
								enfermas en sus domicilios y debiera asentar como servicio
								proporcionado el de "Servicios de enfermer�a" con la clave
								85101601. No obstante lo anterior, se considera que al tratarse
								de un tema nuevo, los primeros meses de vigencia del mismo ser�n
								parte de un proceso de aprendizaje colectivo, durante el cual
								los proveedores de bienes y servicios y sus clientes ir�n
								estableciendo acuerdos y usos en sus respectivos sectores o
								ramas de la econom�a en relaci�n a la clasificaci�n de los
								bienes o servicios que intercambian, esta situaci�n har� que no
								sea necesario durante estos primeros meses corregir alguna
								clasificaci�n que se haya realizado y que con el transcurso de
								este tiempo de aprendizaje com�n resulte variar o considerarse
								equivoca.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">29. �Qu� c�digo postal se debe
								registrar en el CFDI cuando �ste no se encuentre en el Cat�logo
								de c�digo postal del Anexo 20?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En caso de que
								dentro del cat�logo c_CodigoPostal, no se encuentre contenida
								informaci�n del c�digo postal, se debe registrar la clave del
								c�digo postal m�s cercano del lugar de expedici�n del
								comprobante fiscal. <a style="color: blue;">Fundamento Legal:</a> Art�culo 29-A del C�digo
								Fiscal de la Federaci�n y Anexo 20 Gu�a de llenado de los
								comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">30. �Se puede registrar en un CFDI
								de tipo ingreso versi�n 4.0 el n�mero de cuenta, la clave de
								rastreo SPEI o los �ltimos n�meros de una tarjeta de d�bito,
								cr�dito, de servicios o monedero electr�nico?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En un comprobante
								fiscal de ingresos estos datos no son requisitos fiscales que
								deban incluirse, por lo tanto, no existen campos espec�ficos
								para ello. Si a pesar de no ser un requisito fiscal las partes
								quieren incluirlos en el comprobante pueden hacerlo agreg�ndolos
								en una addenda, que es un elemento que se agrega a la factura
								para poder poner informaci�n distinta a la fiscal o distinta a
								la requerida por las disposiciones fiscales. <a style="color: blue;">Fundamento Legal:</a>
								Anexo 20 Gu�a de llenado de los comprobantes fiscales digitales
								por Internet.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">31. �Qu� forma de pago se debe
								registrar en el CFDI de egresos cuando �ste se emita por una
								devoluci�n, descuento o bonificaci�n relacionado al CFDI de
								ingresos correspondiente, siempre que �ste �ltimo no haya sido
								pagado total o parcialmente?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se puede registrar
								la clave "15" (Condonaci�n) del cat�logo c_FormaPago publicado
								en el Portal del SAT. <a style="color: blue;">Fundamento Legal:</a> Anexo 20 Gu�a de llenado
								de los comprobantes fiscales digitales por Internet y Anexo 20
								Gu�a de llenado de los comprobantes fiscales digitales por
								internet versi�n 4.0, publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">32. Si otorgu� una bonificaci�n
								mediante una tarjeta de regalo, �Qu� forma de pago debo
								registrar en el CFDI de egreso que ampara dicha bonificaci�n? y,
								�Que forma de pago se debe registrar en una factura de ingreso
								cuando se reciba como medio de pago la tarjeta de regalo?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe registrar
								en ambos casos la clave "01" (Efectivo) del cat�logo c_FormaPago
								publicado en el Portal del SAT. <a style="color: blue;">Fundamento Legal:</a> Anexo 20 Gu�a
								de llenado de los comprobantes fiscales digitales por Internet
								versi�n 4.0.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">33. �Qu� es un validador de factura
								electr�nica?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Son aplicaciones
								inform�ticas o sistemas que confirman el cumplimiento de la
								estructura y especificaci�n t�cnica de un comprobante fiscal y
								en algunos casos requisitos o elementos comerciales, definidos
								por el receptor de un comprobante.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">34. �El SAT reconoce o autoriza
								alg�n servicio, herramienta o sistema de validaci�n de facturas
								electr�nicas ofrecido por terceros ajenos al propio SAT?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No, la �nica
								validaci�n de facturas electr�nicas con <a style="color: blue;">fundamento legal</a> y
								reconocimiento fiscal es la que realizan los Proveedores
								autorizados de certificaci�n de CFDI (PAC), de esta forma cuando
								el PAC asigna a este comprobante el sello digital del SAT, es
								decir lo "timbra", se est� validando el comprobante por el
								propio SAT a trav�s del PAC, por lo cual los contribuyentes que
								hagan uso del mismo s�lo requieren verificar que el comprobante
								esta efectivamente sellado digitalmente por el SAT, esto a
								trav�s de alguna de las herramientas que ofrece el propio SAT,
								si efectivamente esta "timbrado" por el SAT, el citado
								comprobante es v�lido y no requiere de mayor validaci�n
								tecnol�gica. <a style="color: blue;">Fundamento legal:</a> Art�culo 29, fracciones IV y VI
								del CFF, reglas 2.7.1.4 y 2.7.2.5 de la RMF vigente.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">35. El sistema de c�mputo con el
								que genero mis CFDI, registra importes con m�s de dos decimales
								por cada partida de la factura y con dos decimales en la parte
								de totales. �Es v�lido generar comprobantes con diferente n�mero
								de decimales en las partidas y en los totales?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. S� es correcto;
								por cada concepto de la factura se puede utilizar de cero hasta
								seis decimales como m�ximo y en los totales se debe redondear al
								final del c�lculo el resultado al n�mero de decimales que
								soporta la moneda. Ejemplo: Se emite una factura con moneda
								mexicana (MXN) con las siguientes partidas donde base y tasa o
								cuota tienen 4 decimales.
								<table class="table table-dark">
									<thead>

									</thead>
									<tbody>
										<tr>

											<td style="color: blue;">Conceptos</td>
											<td style="color: blue;">Base</td>
											<td style="color: blue;">Tasa O cuota</td>
											<td style="color: blue;">Monto del Impuesto</td>

										</tr>
										<tr>

											<td style="color: blue;">Servicio 1</td>
											<td style="color: blue;">1.1100</td>
											<td style="color: blue;">0.1600</td>
											<td style="color: blue;">0.1776</td>

										</tr>
										<tr>

											<td style="color: blue;">Servicio 2</td>
											<td style="color: blue;">1.1100</td>
											<td style="color: blue;">0.1600</td>
											<td style="color: blue;">0.1776</td>

										</tr>
										<tr>

											<td style="color: blue;">Servicio 3</td>
											<td style="color: blue;">1.1100</td>
											<td style="color: blue;">0.1600</td>
											<td style="color: blue;">0.1776</td>

										</tr>
										<tr>

											<td style="color: blue;">Servicio 4</td>
											<td style="color: blue;">1.1100</td>
											<td style="color: blue;">0.1600</td>
											<td style="color: blue;">0.1776</td>

										</tr>
										<tr>

											<td style="color: blue;">Totales</td>
											<td style="color: blue;">4.4400</td>
											<td style="color: blue;"></td>
											<td style="color: blue;">0.7104</td>

										</tr>
									</tbody>
								</table> El monto del impuesto se calcula tambi�n con 4 decimales; al
								obtener el total del impuesto sumando las 4 partidas se obtiene
								0.7104; al redondear al final del c�lculo el resultado al n�mero
								de decimales que soporta la moneda, en este caso son dos
								decimales, se llega a 0.71. De esta manera los comprobantes son
								validados y timbrados sin problema por el PAC. <a style="color: blue;">Fundamento Legal:</a>
								Anexo 20 versi�n 4.0 vigente.
							</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">36. El sistema de c�mputo con el
								que genero mis CFDI, "completa" a los seis decimales permitidos
								campos como: Cantidad, Valor Unitario, Importe (monto del
								impuesto), Descuento, Base; �es necesario rellenar de ceros a la
								derecha en la parte fraccionaria, para completar los seis
								decimales? �es v�lido omitir los ceros no significativos?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. La validaci�n del
								PAC para cada uno de los campos a reportar en el CFDI debe de
								cumplir con que el n�mero de decimales reportados sea menor o
								igual al n�mero de decimales especificados en el "Estandar del
								Anexo 20 y sus complementos". Esto permite rellenar de ceros a
								la derecha en la parte fraccionaria para completar los 6
								decimales y tambi�n permite el omitir los ceros no
								significativos; ambos criterios son aceptados. Ejemplo:
								<table class="table table-dark">
									<thead>

									</thead>
									<tbody>
										<tr>

											<td style="color: blue;">Valor en el sistema de la
												empresa</td>
											<td style="color: blue;">Valor v�lido en el CFDI</td>


										</tr>
										<tr>

											<td style="color: blue;">$0.19</td>
											<td style="color: blue;">$0.19</td>
											<td style="color: blue;">$0.190000</td>


										</tr>
										<tr>

											<td style="color: blue;">$1.4567</td>
											<td style="color: blue;">$1.4567</td>
											<td style="color: blue;">$1.456700</td>


										</tr>
										<tr>

											<td style="color: blue;">$3.4567288</td>
											<td style="color: blue;">$3.456729</td>
											<td style="color: blue;">$3.456729</td>


										</tr>


									</tbody>
								</table>Es importante ser consistentes al realizar las operaciones
								aritm�ticas con el mismo n�mero de decimales que hayan definido
								para los campos del tipo importe. <a style="color: blue;">Fundamento Legal:</a> Anexo 20
								versi�n 4.0 vigente .
							</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">37. �En general cuales son las
								recomendaciones que sugiere el SAT, para evitar un rechazo en la
								factura con respecto al tema de decimales?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Para evitar un
								rechazo de la factura se sugiere: .Para los c�lculos considerar
								el m�ximo n�mero de decimales que permita el sistema que
								utilizan las empresas para generar su factura (hasta seis
								decimales como m�ximo). .Los campos que permiten hasta seis
								decimales son los del tipo t_Importe, por ejemplo: Cantidad,
								Valor Unitario, Importe (resultado de multiplicar cantidad por
								Valor Unitario), Descuento, Base, Importe a nivel de impuestos.
								.Ser consistentes al realizar las operaciones aritm�ticas con el
								mismo n�mero de decimales que hayan definido para los campos del
								tipo importe del punto anterior. .Redondear al final del c�lculo
								y no antes, el resultado al n�mero de decimales que soporta la
								moneda. .En el caso del Importe de los Conceptos, el redondeo
								aplicar� en el campo SubTotal del comprobante. .En el caso de
								los Descuentos de los Conceptos, el redondeo aplicar� en el
								campo Descuento del comprobante. .En el caso de los Impuestos de
								los Conceptos, el redondeo aplicar� en el resumen de Impuestos,
								en los campos Importe de los nodos Retenciones y Traslados
								(donde deben agruparse por impuesto, TipoFactor y TasaOCuota).
								<a style="color: blue;">Fundamento Legal:</a> Anexo 20 versi�n 4.0 vigente.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">38. El sistema de c�mputo con el
								que genero mis CFDI, en el campo de TasaOCuota utiliza 2
								decimales �es v�lido utilizar s�lo 2 decimales o se deben de
								reportar seis decimales?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Lo correcto es que
								el valor registrado debe corresponder a un valor, fijo o de
								rango respectivamente, del cat�logo c_TasaOCuota. Se deben usar
								seis decimales de conformidad con dicho cat�logo. <a style="color: blue;">Fundamento
								Legal:</a> Anexo 20 versi�n 4.0 vigente.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">39. En las validaciones para
								determinar el rango de los campos num�ricos con l�mites �es
								correcto que el l�mite inferior sea igual al l�mite superior?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No es correcto; si
								el l�mite inferior es igual al superior; seguramente se est�
								aplicando mal el c�lculo. Ejemplo:
								<table class="table table-dark">
									<thead>

									</thead>
									<tbody>
										<tr>

											<td style="color: blue;">Conceptos</td>
											<td style="color: blue;">Base</td>
											<td style="color: blue;">Tasa</td>
											<td style="color: blue;">Monto impuesto</td>
											<td style="color: blue;">L�mite inferior</td>
											<td style="color: blue;">L�mite superior</td>

										</tr>
										<tr>

											<td style="color: blue;">Servicio 1</td>
											<td style="color: blue;">1,480,791.59</td>
											<td style="color: blue;">0.16</td>
											<td style="color: blue;">236,926.65</td>
											<td style="color: blue;">236,926.65</td>
											<td style="color: blue;">236,926.66</td>

										</tr>
										<tr>

											<td style="color: blue;">Servicio 2</td>
											<td style="color: blue;">1,454,021.93</td>
											<td style="color: blue;">0.16</td>
											<td style="color: blue;">232,643.50</td>
											<td style="color: blue;">232,643.50</td>
											<td style="color: blue;">232,643.51</td>


										</tr>
										<tr>

											<td style="color: blue;">Servicio 3</td>
											<td style="color: blue;">1,343,559.33</td>
											<td style="color: blue;">0.16</td>
											<td style="color: blue;">214,969.49</td>
											<td style="color: blue;">214,969.49</td>
											<td style="color: blue;">214,969.49</td>


										</tr>


									</tbody>
								</table> En el ejemplo anterior en el l�mite superior se realiz� un
								redondeo aritm�tico, siendo que se debe redondear hacia arriba.

								En resumen, lo correcto es:El resultado de calcular el l�mite
								inferior truncarlo con el m�ximo n�mero de decimales que permita
								el sistema (hasta seis decimales como m�ximo). El resultado de
								calcular el l�mite superior redondearlo hacia arriba con el
								m�ximo n�mero de decimales que permita el sistema (hasta 6
								decimales como m�ximo). Ejemplo: .moneda MXN, decimales 2,
								importe 924.224956 .Truncado del importe a 2 decimales: 924.22
								.Redondeado del importe hacia arriba: 924.23 <a style="color: blue;">Fundamento Legal:</a>
								Anexo 20 versi�n 4.0 vigente.
							</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">40. Cuando se deba emitir un CFDI
								que sustituye a otro CFDI, �Qu� debo hacer?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe actuar en
								este orden: 1. Primero se debe cancelar el CFDI que se va a
								sustituir, y 2. Se debe emitir el nuevo CFDI en el cual se debe
								registrar en el campo TipoRelacion la clave "04" (Sustituci�n de
								los CFDI previos) y en el campo UUID del Nodo CFDIRelacionado se
								debe registrar el folio fiscal del comprobante que se va a
								sustituir. <a style="color: blue;">Fundamento Legal:</a> Anexo 20 Gu�a de llenado de los
								comprobantes fiscales digitales por internet versi�n 4.0,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: red;">Preguntas Frecuentes sobre el recibo
								de n�mina.</td>
						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">1. Para el CFDI de n�mina versi�n
								1.2 �Qu� m�todo de pago se debe utilizar?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En el caso de los
								comprobantes fiscales digitales por Internet que se emitan por
								concepto de n�mina bajo la versi�n 1.2 del complemento, se
								deber� se�alar la clave "PUE" (Pago en una sola exhibici�n).

								<a style="color: blue;">Fundamento Legal: </a>Gu�a de llenado del complemento de n�mina,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">2. En los casos donde el trabajador
								est� de incapacidad y el patr�n no le realiza pago debido a que
								no tiene obligaci�n de hacerlo, es decir "incapacidad sin goce
								de sueldos", �Se debe expedir el CFDI de n�mina?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No; cuando no
								exista un pago al trabajador no se debe emitir el CFDI con el
								complemento de n�mina, esto ya que la obligaci�n de emitir el
								comprobante se genera por el hecho de hacer el pago de sueldos y
								salarios, entendi�ndose por este las percepciones, comisiones,
								prestaciones en especie y cualquiera otra cantidad o prestaci�n
								que se entregue al empleado por su trabajo.<a style="color: blue;"> Fundamento legal:</a>
								Art�culos 99 fracci�n III de la Ley del Impuesto Sobre la Renta,
								132 fracci�n VII y 804 primer p�rrafo fracci�n II de la Ley
								Federal del Trabajo.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">3. �En el CFDI de n�mina versi�n
								1.2 se podr�n registrar cantidades en negativo?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No, en la versi�n
								del complemento de n�mina 1.2 no aplica el uso de n�meros
								negativos para ning�n dato, por lo que deber�n analizar cada uno
								de los casos en los que conforme a su operaci�n actual reportan
								montos negativos para determinar c�mo debe informarse en el
								nuevo complemento utilizando los nodos de Percepciones,
								Deducciones u Otros Pagos. <a style="color: blue;">Fundamento Legal:</a> Gu�a de llenado del
								complemento de n�mina, publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">4. �Los campos condicionales del
								CFDI son de uso obligatorio?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Los campos
								condicionales deber�n informarse siempre que aplique el supuesto
								conforme al CFDI de N�mina que se est� expidiendo, as� como las
								obligaciones del patr�n y la informaci�n proporcionada en alg�n
								otro campo. Por ejemplo, el dato CURP en el Nodo Emisor, el cual
								corresponde a la CURP del patr�n (emisor), deber� informarse
								cuando se trate de una persona f�sica. En el caso de personas
								morales, toda vez que estas no cuentan con CURP, no se deber�
								informar. Para mayor referencia sobre el registro y detalle de
								cada uno de los campos del CFDI de n�mina, se recomienda
								verificar la Gu�a de llenado publicada en el Portal.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">5. �C�mo se reflejar�n en el CFDI
								de n�mina versi�n 1.2 las correcciones por percepciones pagadas
								en exceso?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En el caso de que
								se emita un CFDI de n�mina que tenga errores consistentes en
								reflejar percepciones pagadas en exceso, se puede realizar su
								correcci�n de cualquiera de las siguientes formas: I. Cancelando
								el CFDI emitido con errores y expidiendo uno nuevo con los datos
								correctos. II. Reflejando como deducci�n el descuento de las
								percepciones en exceso, esto en el siguiente CFDI de n�mina que
								se expida, siempre que sea en el mismo ejercicio fiscal. A
								efecto de que se especifique claramente las deducciones gravadas
								y exentas se deber�n utilizar las claves correspondientes
								incluidas en el cat�logo c_TipoDeduccion. <a style="color: blue;">Fundamento Legal:</a>
								Art�culos 94 y 95 de la Ley del Impuesto sobre la Renta y Gu�a
								de llenado del complemento de n�mina, publicada en el Portal del
								SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">6. �C�mo se registran en el CFDI de
								n�mina versi�n 1.2 los pr�stamos otorgados a los empleados?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se reportar�an en
								la secci�n de OtrosPagos con la clave 999 Pagos distintos a los
								listados y que no deben considerarse como ingreso por sueldos,
								salarios o ingresos asimilados. <a style="color: blue;">Fundamento Legal:</a> Gu�a de
								llenado del complemento de n�mina, publicada en el Portal del
								SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">7. Si la clave de percepci�n
								asignada en la contabilidad del empleador a las percepciones es
								mayor a 15 caracteres �C�mo debe indicarse?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Si la clave
								contiene m�s s�lo se indicar�n los primeros 15. <a style="color: blue;">Fundamento
								Legal:</a> Gu�a de llenado del complemento de n�mina, publicada en
								el Portal del SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">8. �Los nodos "HorasExtra" e
								"Incapacidades" son de uso obligatorio?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">Son nodos
								condicionales: .El nodo "HorasExtra" se deber� informar si se
								incluye en percepciones la clave 019 "Horas Extra". .El nodo
								"Incapacidades" se deber� informar si se incluye en percepciones
								la clave 014 "Subsidios por Incapacidad" o bien en deducciones
								la clave 006 "Descuento por incapacidad". <a style="color: blue;">Fundamento Legal:</a> Gu�a
								de llenado del complemento de n�mina, publicada en el Portal del
								SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">9. �A qu� se refiere
								"Incapacidades"?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Si se registr�
								como una deducci�n con la clave 006, el importe monetario
								corresponde al descuento que por motivo de la incapacidad la
								empresa realiza. Si se registra como una percepci�n corresponde
								al pago que realiza la empresa por lo que corresponde respecto a
								la incapacidad. <a style="color: blue;">Fundamento Legal:</a> Art�culo 58 de la Ley del
								Seguro Social y Gu�a de llenado del complemento de n�mina,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">10. Si el n�mero de cuenta es de
								una longitud diferente a las especificadas como longitudes
								v�lidas para un n�mero de cuenta en la versi�n 1.2 del
								Complemento de N�mina (10, 11, 16 o 18 d�gitos) �C�mo debe
								procederse?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Siempre que se
								confirme que es un n�mero de cuenta v�lido, se deber� rellenar
								con ceros a la izquierda hasta completar la longitud m�s cercana
								a la longitud real del n�mero de cuenta. Por ejemplo, si se
								tiene un n�mero de cuenta de 12 d�gitos, deber� rellenar con
								ceros a la izquierda hasta completar 16 d�gitos.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">11. Se cuenta con algunos empleados
								que se les paga la n�mina a dos cuentas bancarias, �C�mo se debe
								registrar el campo de CuentaBancaria en el CFDI de n�mina?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En este caso, se
								deber� incluir la cuenta bancaria del empleado donde se deposita
								la mayor cantidad del pago de n�mina. <a style="color: blue;">Fundamento Legal:</a> Gu�a de
								llenado del complemento de n�mina, publicada en el Portal del
								SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">12. Soy patr�n y cumplo con otorgar
								seguridad social a mis trabajadores, pero esta se otorga
								mediante prestadores de servicios privados debido a que tenemos
								convenio de subrogaci�n con el IMSS �Qu� dato debo asentar en el
								campo RegistroPatronal del CFDI de n�mina?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Debe asentar
								precisamente su clave de Registro Patronal con independencia de
								que exista un convenio se subrogaci�n con el IMSS, el patr�n
								debe contar con dicho registro y es precisamente �ste el dato a
								asentar en este campo. Si tiene alguna duda sobre c�mo se
								obtiene el registro patronal se sugiere acercarse a las �reas de
								atenci�n del IMSS.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">13. Soy una instituci�n p�blica y
								l�gicamente tenemos trabajadores asalariados, cumplimos con la
								obligaci�n de otorgar seguridad social a nuestros trabajadores,
								pero esta se otorga mediante prestadores de servicios privados
								debido a que tenemos convenio de subrogaci�n con el Instituto
								Mexicano de Seguridad Social, �Qu� dato debo asentar en el campo
								RegistroPatronal del CFDI de n�mina?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe asentar la
								clave de ramo-pagadur�a o aquella que por la afiliaci�n le
								asigne el instituto de seguridad social que conforme a ley
								corresponda (federal, Estatal o municipal), si tiene duda acerca
								de cu�l es el dato, se sugiere verificar con el Instituto de
								seguridad social correspondiente. <a style="color: blue;">Fundamento Legal:</a> Gu�a de
								llenado del complemento de n�mina, publicada en el Portal del
								SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">14. En un mismo periodo de pago, un
								trabajador tiene percepciones de subsidios por incapacidad que
								se registran como TipoPerccepcion con la clave "014" y se le
								aplican descuentos por incapacidad que se registran como
								TipoDeduccion con la clave "006", �C�mo debe registrarse la
								informaci�n (percepci�n y deducci�n) en el Nodo Incapacidades?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Es posible que se
								reporte en 2 CFDI o bien en 1 CFDI con 2 complementos de n�mina.

								En un complemento de n�mina o CFDI independiente se podr�
								incluir s�lo la informaci�n de la percepci�n "014" Subsidios por
								incapacidad con el correspondiente nodo de Incapacidades y en un
								segundo complemento o CFDI incluir las dem�s percepciones u
								otros pagos, as� como la Deducci�n "006" Descuento por
								incapacidad con el correspondiente nodo de Incapacidades, a
								efecto de cumplir con las validaciones.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">15. �C�mo se deben reportar en el
								CFDI de n�mina el reintegro, devoluci�n o las cantidades
								descontadas al trabajador por concepto de vi�ticos entregados
								que no fueron utilizados?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En el caso de
								descuento v�a n�mina por concepto de vi�ticos, se deber�
								reflejar en el apartado de deducciones con la clave
								TipoDeduccion 004 Otros. Los reintegros o devoluciones de
								vi�ticos que realice el trabajador directamente al patr�n
								(ejemplo dep�sito, efectivo, transferencia) no se reportar�n a
								trav�s del CFDI de n�mina. <a style="color: blue;">Fundamento legal:</a> Art�culos 28,
								fracci�n V y 93, fracci�n XVII de la Ley del Impuesto sobre la
								Renta.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">16. �Qu� debo registrar en el campo
								LugarExpedicion, cuando el C�digo Postal no exista en el
								cat�logo c_CodigoPostal publicado en el Portal del SAT?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. El cat�logo
								c_CodigoPostal integra los c�digos postales registrados en
								SEPOMEX y en los domicilios fiscales registrados ante el RFC,
								por lo que, en caso de no encontrarse el c�digo postal, se
								deber� registrar el c�digo m�s cercano al domicilio de que se
								trate, en tanto se actualiza el cat�logo. El cat�logo se
								actualizar� de manera mensual.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">17. Si genero un comprobante de
								n�mina de un trabajador asimilado a salario �Es correcto
								ingresar en la factura, en el campo "descripci�n" del nodo
								Conceptos del comprobante de n�mina el valor "Pago de n�mina",
								como lo se�ala la Gu�a de llenado y est�ndar del comprobante,
								aunque se trate de un asimilado?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. S�, es correcto el
								ingreso del valor "Pago de n�mina" en el campo descripci�n del
								nodo Conceptos del CFDI de n�mina en la factura, toda vez que
								aun cuando se trata de un comprobante de un asimilado a salario,
								la informaci�n espec�fica que denota si el comprobante
								corresponde a un asimilado a sueldos o a un asalariado, se
								precisa dentro del complemento de n�mina en los campos "Tipo
								contrato y "Tipo r�gimen", por lo que no hay lugar a error de
								interpretaci�n acerca de si se trata de un asalariado o un
								asimilado.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">18. Si soy un contribuyente que
								presto servicios de subcontrataci�n laboral, �Me encuentro
								obligado a ingresar la informaci�n que se solicita en el nodo
								subcontrataci�n del complemento de n�mina?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. El uso del nodo
								subcontrataci�n resulta actualmente como opcional, esto mientras
								no exista alguna disposici�n legal que establezca para alg�n
								contribuyente la obligaci�n de su uso. El est�ndar del
								comprobante se clasifica como "condicional" esto significa que
								mientras no exista la referida disposici�n legal que lo haga
								obligatorio, entonces pr�cticamente su uso es opcional.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">19. Para el caso del fondo de
								ahorro de los trabajadores, con el objeto de no duplicar el
								registro de los ingresos del trabajador, �C�mo debe registrarse
								en el Complemento de sueldos, salarios e ingresos asimilados?,
								�C�mo un ingreso en cada pago y una deducci�n por aportaci�n
								patronal al fondo de ahorro? �O se registra hasta que se cobra
								el monto ahorrado y los intereses?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Al ser las
								aportaciones patronales al fondo de ahorro una prestaci�n
								derivada de la relaci�n laboral, son ingresos por la prestaci�n
								de un servicio personal subordinado; de esta forma deben
								registrarse en el cat�logo de percepciones del Complemento en
								cada pago de salarios que se realice, al mismo tiempo que debe
								registrarse en el cat�logo de deducciones del Complemento el
								descuento correspondiente para realizar el dep�sito al fondo. Al
								momento de percibir el monto ahorrado y los intereses, el
								pagador de �stos debe expedir un CFDI por este concepto, es
								decir por intereses, ya que se trata de estos y no de un sueldo,
								salario o ingreso asimilado a estos. <a style="color: blue;">Fundamento Legal:</a> Art�culos
								27, fracci�n XI, 93, fracci�n XI y 94 de la Ley del Impuesto
								sobre la Renta.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">20. �En qu� momento y c�mo se
								deber�n reportar los gastos m�dicos mayores y el seguro de vida,
								se reportan c�mo concepto de percepci�n del empleado?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Las primas que
								amparen estos seguros (por ambos conceptos) que sean otorgadas
								al trabajador por cuenta del patr�n, al ser prestaciones
								derivadas de la relaci�n laboral, se reportan en el Complemento,
								las primas a cargo del patr�n primero como percepci�n y luego
								como deducci�n por pago de prima a cargo del patr�n. Cuando se
								realice el siniestro y esto de origen al pago de la cantidad
								asegurada por parte de la empresa asegurada, dichas cantidades
								no tienen el car�cter de sueldos y salarios, y por ende no
								requieren ser incluidas en el Complemento. <a style="color: blue;">Fundamento Legal:</a>
								Art�culos 27, fracci�n XI, 93, fracci�n XXI y 94 de la Ley del
								Impuesto sobre la Renta.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">21. En el caso de la deducci�n por
								pago de prima de gastos m�dicos mayores, �Se puede incluir una
								sola vez al a�o en un recibo de n�mina?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. La deducci�n por
								pago de prima se debe reflejar en el CFDI conforme se vaya
								devengando.<a style="color: blue;"> Fundamento Legal:</a> Art�culos 27, fracci�n XI, 93,
								fracci�n XXI y 94 de la Ley del Impuesto sobre la Renta.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff" scope="row"></th>
							<td style="color: blue;background-color:#ffffff">22. Cuando se deba emitir un CFDI
								que sustituye a otro CFDI, �Qu� debo hacer?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">Se debe actuar en
								este orden: 1. Primero se debe cancelar el CFDI que se va a
								sustituir, y 2. Se debe emitir el nuevo CFDI en el cual se debe
								registrar en el campo TipoRelacion la clave "04" (Sustituci�n de
								los CFDI previos) y en el campo UUID del Nodo CFDIRelacionado se
								debe registrar el folio fiscal del comprobante que se va a
								sustituir . <a style="color: blue;">Fundamento Legal:</a> Gu�a de llenado del complemento de
								n�mina, publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th style="background-color:#ffffff"  scope="row"></th>
							<td style="background-color:#ffffff;color: red;">Preguntas Frecuentes sobre el
								complemento de pago.</td>
						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">1. Cuando haya compensaci�n de
								pagos entre contribuyentes �Qu� clave se debe registrar como
								forma de pago en el CFDI con complemento para recepci�n de
								pagos, tambi�n denominado "recibo electr�nico de pago"?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En el campo forma
								de pago se debe registrar la clave "17 Compensaci�n" del
								cat�logo c_FormaPago del Anexo 20. <a style="color: blue;">Fundamento Legal:</a> Gu�a de
								llenado del comprobante al que se le incorpore el complemento
								para recepci�n de pagos, publicada en el portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">2. Cuando un s�lo pago recibido es
								para liquidar varias facturas, �Se tiene que emitir un CFDI con
								complemento para recepci�n de pagos tambi�n denominado "recibo
								electr�nico de pago" por cada comprobante que se liquida?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No, se expedir� un
								s�lo CFDI con complemento para recepci�n de pagos y en �l se
								incluir�n las referencias a todas las facturas que se liquidan.
								Para ello deber�n incluir en el campo identificador del
								documento, cada uno de los folios (UUID) que identifican a cada
								una de las facturas. <a style="color: blue;">Fundamento Legal:</a> Gu�a de llenado del
								comprobante al que se le incorpore el complemento para recepci�n
								de pagos, publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">3. �Se puede cancelar un CFDI con
								complemento para recepci�n de pagos tambi�n denominado "recibo
								electr�nico de pago", si la clave en el RFC del receptor no es
								correcta?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. S�, se puede
								cancelar siempre que se sustituya por otro con los datos
								correctos.<a style="color: blue;"> Fundamento Legal:</a> Gu�a de llenado del comprobante al
								que se le incorpore el complemento para recepci�n de pagos,
								publicada en el portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">4. �Qu� plazo se tiene para emitir
								el CFDI con complemento para recepci�n de pagos tambi�n
								denominado "recibo electr�nico de pago"?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe emitir el
								CFDI con complemento para recepci�n de pagos a m�s tardar al
								d�cimo d�a natural del mes siguiente al que se recibi� el pago.
								Dado que el dato es un insumo para la determinaci�n del IVA, se
								consider� un plazo similar al establecido para la declaraci�n de
								dicho impuesto.<a style="color: blue;"> Fundamento Legal:</a> Gu�a de llenado del
								comprobante al que se le incorpore el complemento para recepci�n
								de pagos, publicada en el portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">5. En la factura que se elabora
								conforme al est�ndar del Anexo 20 versi�n 4.0, en el campo tipo
								de comprobante, �Qu� clave se debe registrar al emitir un CFDI
								con complemento para recepci�n de pagos tambi�n denominado
								"recibo electr�nico de pago"?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe registrar
								la clave tipo de comprobante "P" (Pago), conforme al cat�logo
								c_TipoDeComprobante. Al hacer esto, la mayor�a de los
								aplicativos deshabilitar�n los campos que no hay que llenar y
								habilitar�n los campos que se deben llenar. Tal es el caso del
								servicio de facturaci�n del SAT.<a style="color: blue;"> Fundamento Legal:</a> Gu�a de
								llenado del comprobante al que se le incorpore el complemento
								para recepci�n de pagos, publicada en el Portal del SAT.</td>

						</tr>

						<tr>
							<th scope="row"></th>
							<td style="color: blue;">6. �Es obligatorio el campo de "Uso
								CFDI" en el CFDI con complemento para recepci�n de pagos,
								tambi�n denominado "recibo electr�nico de pago"?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. S�, es
								obligatorio. Cuando se emita un CFDI con complemento para
								recepci�n de pagos, en el campo Uso de CFDI se debe registrar la
								clave "P01" Por definir. En raz�n, de que el Uso del CFDI quedo
								registrado en la factura emitida por el monto total de la
								operaci�n. <a style="color: blue;">Fundamento Legal:</a> Gu�a de llenado del comprobante al
								que se le incorpore el complemento para recepci�n de pagos,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">7. En un CFDI con complemento para
								recepci�n de pagos tambi�n denominado "recibo electr�nico de
								pago", cuando el pago se realice en pesos mexicanos, �Qu� tipo
								de cambio se debe registrar en el campo TipoCambioP?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. El campo se puede
								omitir o se puede registrar el valor "1" sin decimales.

								<a style="color: blue;">Fundamento Legal:</a> Gu�a de llenado del comprobante al que se le
								incorpore el complemento para recepci�n de pagos, publicada en
								el Portal del SAT.</td>

						</tr>

						<tr>
							<th scope="row"></th>
							<td style="color: blue;">8. �Se debe desglosar el IVA en el
								CFDI con complemento para recepci�n de pagos tambi�n denominado
								"recibo electr�nico de pago", como lo indica la fracci�n VII,
								inciso b), del Art�culo 29-A del C�digo Fiscal de la Federaci�n?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No se desglosan
								los impuestos trasladados y retenidos en el CFDI con complemento
								para recepci�n de pagos, ya que la regla 2.7.1.35., establece
								que el monto del pago se aplicar� proporcionalmente a los
								conceptos integrados en la factura emitida por el valor total de
								la operaci�n.<a style="color: blue;"> Fundamento Legal:</a> Regla 2.7.1.35. de la Resoluci�n
								Miscel�nea Fiscal y Gu�a de llenado del comprobante al que se le
								incorpore el complemento para recepci�n de pagos, publicadas en
								el portal del SAT.</td>

						</tr>

						<tr>
							<th scope="row"></th>
							<td style="color: blue;">9. En la factura que se emita por
								la recepci�n de 10 pagos, �Cu�ntos complementos en el CFDI con
								complemento para recepci�n de pagos tambi�n denominado "recibo
								electr�nico de pago" se incluir�n?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. S�lo se debe
								emitir un CFDI con complemento para recepci�n de pagos generando
								10 apartados "Pago" para relacionar los 10 pagos recibidos y
								generar los apartados de "Documento relacionado", que se
								requieran para relacionar los UIDD de las facturas que se
								vinculan con dichos pagos, siempre y cuando se trate de un mismo
								receptor. <a style="color: blue;">Fundamento Legal:</a> Gu�a de llenado del comprobante al
								que se le incorpore el complemento para recepci�n de pagos,
								publicada en el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">10. Si recibo un pago con
								transferencia electr�nica de fondos, �Debo registrar
								obligatoriamente en el CFDI con Complemento para recepci�n de
								pagos tambi�n denominado "recibo electr�nico de pago" la
								informaci�n en los campos TipoCadPago, CertPago, CadPago y
								SelloPago?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Si se recibe un
								pago con transferencia electr�nica de fondos se puede o no
								registrar informaci�n en el campo TipoCadPago. Si se decide
								registrar informaci�n en el campo TipoCadPago, es obligatorio
								registrar tambi�n la informaci�n de los campos CertPago, CadPago
								y SelloPago. Si no se registra informaci�n en el campo
								TipoCadPago no se debe registrar informaci�n en los campos
								CertPago, CadPago y SelloPago.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">11. �Se puede emitir un CFDI con
								complemento para recepci�n de pagos tambi�n denominado recibo
								electr�nico de pago, con una fecha de pago a futuro?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. No, el comprobante
								se debe emitir cuando efectivamente se reciba el pago. La fecha
								de pago deber� ser igual o anterior a la fecha de emisi�n del
								recibo de pago. No deber�n emitirse documentos de pago a partir
								de una promesa de pago.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">12. �Qu� fecha se debe registrar en
								el campo FechaPago en el CFDI con complemento para recepci�n de
								pagos tambi�n denominado recibo electr�nico de pago, cu�ndo se
								reciba como pago un cheque de un banco distinto, considerando
								que se acredita el dep�sito hasta el d�a siguiente?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe registrar
								la fecha en la que se recibe el cheque, aunque aparezca el
								dep�sito al d�a siguiente.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">13. �Qu� fecha se debe registrar en
								el campo "Fecha" del CFDI al que se le incorpora el complemento
								para recepci�n de pagos tambi�n denominado "Recibo electr�nico
								de pago", y qu� fecha se debe registrar en el campo "FechaPago"
								del complemento para recepci�n de pagos?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. En el campo
								"Fecha" se debe registrar la fecha y hora de expedici�n del CFDI
								y en el campo "FechaPago" se debe registrar la fecha y hora en
								la que se est� recibiendo el pago de la contraprestaci�n.

								<a style="color: blue;">Fundamento Legal:</a> Gu�a de llenado del comprobante al que se le
								incorpore el complemento para recepci�n de pagos, publicada en
								el Portal del SAT.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">14. �En una operaci�n en
								parcialidades o con pago diferido, si realizo el pago debo
								obtener el Recibo electr�nico de pago correspondiente para poder
								acreditar los impuestos trasladados, y en su caso deducir?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. S�, es requisito
								necesario para poder realizar el acreditamiento o en su caso
								deducci�n el contar con el Recibo electr�nico de pago.

								<a style="color: blue;">Fundamento Legal:</a> Art�culos 29, primer p�rrafo, 29-A,
								antepen�ltimo p�rrafo del CFF, 27, fracci�n III y 147, fracci�n
								IV de la LISR, 5, fracci�n II de la LIVA y 4, fracciones III y V
								de la LIEPS.</td>

						</tr>
						<tr>
							<th scope="row"></th>
							<td style="color: blue;">15. Cuando se deba emitir un CFDI
								que sustituye a otro CFDI, �Qu� debo hacer?</td>
						</tr>
						<tr>
							<th style="background-color: #92a8d1;" scope="row"></th>
							<td style="background-color: #92a8d1;">R. Se debe actuar en
								este orden: 1.- Primero se debe cancelar el CFDI que se va a
								sustituir, y 2.- Se debe emitir el nuevo CFDI en el cual se debe
								registrar en el campo TipoRelacion la clave "04" (Sustituci�n de
								los CFDI previos) y en el campo UUID del Nodo CFDIRelacionado se
								debe registrar el folio fiscal del comprobante que se va a
								sustituir. <a style="color: blue;">Fundamento Legal:</a> Gu�a de llenado del comprobante al
								que se le incorpore el complemento para recepci�n de pagos,
								publicada en el Portal del SAT.</td>

						</tr>
					</tbody>
				</table>





			</div>
			<div class="modal-footer">
				<div class="col-md-12" align="right">
				<!--
					<button type="button" class="btn-danger" ng-click="cancel()"
						data-dismiss="modal" id="close-window-preguntasSAT">
						Cerrar <span class="glyphicon glyphicon-remove-circle"
							aria-hidden="true"></span>
					</button>
					-->

				</div>
			</div>
		</div>
	</div>
</div>