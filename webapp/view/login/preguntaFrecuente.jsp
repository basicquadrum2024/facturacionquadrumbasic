
<div class="preguntas-frecuentes" id="pregunta-frecuente">
	<div class="modal-header">
		<button id="close-window-cre" type="button" class="close"
			style="color: white;" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h2 class="modal-title">Preguntas frecuentes Quadrum Basic</h2>
	</div>
	<div class="modal-body">

		<div id="accordion">
			<div class="card">
				<div class="card-header" id="headingOne">
					<h2 class="mb-0">

						<button class="btn btn-link collapsed" data-toggle="collapse"
							data-target="#collapseOne" aria-expanded="true"
							aria-controls="collapseOne"
							onClick="muestra_oculta('collapseOne');">�Que necesito
							para poder facturar en la plataforma gratuita de QUADRUM?</button>
					</h2>
				</div>
				<div id="collapseOne" class="collapse" aria-labelledby="headingOne"
					data-parent="#accordion">
					<div class="card-body">R: Estar inscrito en el SAT como
						persona f�sica o moral, contar con su Certificado de Sello Digital
						vigente tramitado ante el SAT y registrarse en la plataforma
						b�sica de Quadrum.</div>
				</div>
			</div>



			<div class="card">
				<div class="card-header" id="headingTwo">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="#collapseTwo"
							aria-expanded="false" aria-controls="collapseTwo"
							onClick="muestra_oculta('collapseTwo');">Quiero
							registrar una empresa, �porque me pide nombre y apellidos?</button>
					</h2>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
					data-parent="#accordion">
					<div class="card-body">R: Para crear una cuenta, se solicita
						informaci�n de contacto, la informaci�n fiscal para facturar se
						agrega posterior al registro.</div>
				</div>
			</div>


			<div class="card">
				<div class="card-header" id="headingThree">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="#collapseThree"
							aria-expanded="false" aria-controls="collapseThree"
							onClick="muestra_oculta('collapseThree');">�Por qu� no
							me permite el acceso con la contrase�a que me env�an?</button>
					</h2>
				</div>
				<div id="collapseThree" class="collapse"
					aria-labelledby="headingThree" data-parent="#accordion">
					<div class="card-body">R: Se debe de ingresar la contrase�a
						completa y sin espacios</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="headingThree">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="collapseFour"
							aria-expanded="false" aria-controls="collapseFour"
							onClick="muestra_oculta('collapseFour');">�Por qu� no me
							permite cargar mis sellos CSD?</button>
					</h2>
				</div>
				<div id="collapseFour" class="collapse"
					aria-labelledby="headingFour" data-parent="#accordion">
					<div class="card-body">Es necesario verificar que sean los
						archivos con extensi�n .cer y .key correspondientes a los sellos
						CSD y no a los de la FIEL que tienen la misma extensi�n.</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="headingFive">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="#collapseFive"
							aria-expanded="false" aria-controls="collapseFive"
							onClick="muestra_oculta('#collapseFive');">Despu�s de
							tramitar o renovar mis sellos �Cu�nto tiempo debo esperar para
							poder comenzar a facturar?</button>
					</h2>
				</div>
				<div id="#collapseFive" class="collapse"
					aria-labelledby="headingFive" data-parent="#accordion">
					<div class="card-body">Una vez que se obtienen los sellos
						CSD, es necesario esperar un aproximado de 24 a 48 hrs que es el
						tiempo que tarda en SAT para actualizarse la LCO, si intenta
						facturar la plataforma le enviara un error.</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="headingSix">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="#collapseSix"
							aria-expanded="false" aria-controls="collapseSix"
							onClick="muestra_oculta('#collapseSix');">�Puedo
							personalizar el PDF de mis facturas?</button>
					</h2>
				</div>
				<div id="#collapseSix" class="collapse" aria-labelledby="headingSix"
					data-parent="#accordion">
					<div class="card-body">No, la plataforma solo cuenta con una
						platilla gen�rica para todos los usuarios de la misma.</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="headingSeven">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="#collapseSeven"
							aria-expanded="false" aria-controls="collapseSeven"
							onClick="muestra_oculta('#collapseSeven');">Mi empresa
							cambio de direcci�n fiscal �Como la puedo actualizar en el
							sistema?</button>
					</h2>
				</div>
				<div id="#collapseSeven" class="collapse"
					aria-labelledby="headingSeven" data-parent="#accordion">
					<div class="card-body">En men� principal tiene la opci�n de
						Actualizar Contribuyente, aqu� le permite actualizar sus datos
						Fiscales o realizar cambios a la informaci�n del emisor.</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="headingEight">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="#collapseEight"
							aria-expanded="false" aria-controls="collapseEight"
							onClick="muestra_oculta('#collapseEight');">�C�mo
							realizo la b�squeda de una factura?</button>
					</h2>
				</div>
				<div id="#collapseEight" class="collapse"
					aria-labelledby="headingEight" data-parent="#accordion">
					<div class="card-body">En el men� principal est� la opci�n
						B�squeda de Facturas y puede seleccionar alguno de los criterios
						de b�squeda, por el cual desea rastrear la factura.</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="headingNine">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="#collapseNine"
							aria-expanded="false" aria-controls="collapseNine"
							onClick="muestra_oculta('#collapseNine');">Cancele una
							factura por error, �Es posible que se pueda activar nuevamente o
							que puedo hacer en estos casos?</button>
					</h2>
				</div>
				<div id="#collapseNine" class="collapse"
					aria-labelledby="headingNine" data-parent="#accordion">
					<div class="card-body">Ya no es posible la activaci�n de la
						misma, se tiene que generar nuevamente el comprobante.</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="headingTen">
					<h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button"
							data-toggle="collapse" data-target="#collapseTen"
							aria-expanded="false" aria-controls="collapseTen"
							onClick="muestra_oculta('#collapseTen');">�C�mo genero
							una factura de pagos?</button>
					</h2>
				</div>
				<div id="#collapseTen" class="collapse" aria-labelledby="headingTen"
					data-parent="#accordion">
					<div class="card-body">En el men� principal se encuentra la
						opci�n factura de pagos, donde se enlistan las facturas generadas
						con PPD- Pago en parcialidades o diferido, donde podemos generar
						nuevo recibo o consultar recibo de pago seleccionado.</div>
				</div>
			</div>

		</div>
	</div>
	<div class="modal-footer">
		<div class="col-md-12" align="right">
			<button type="button" class="btn-danger" ng-click="cancel()"
				data-dismiss="modal" id="close-window-pregunta-frecuente">
				Regresar <span class="glyphicon glyphicon-remove-circle"
					aria-hidden="true"></span>
			</button>
		</div>

	</div>


</div>
