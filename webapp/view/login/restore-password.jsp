
<div class="restore-pass" id="restore-password">
         <div>
            <div>
               <div class="modal-header">
                  <button id="close-window" type="button" class="close" style="color:white;"  data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">RESTAURAR CONTRASE&Ntilde;A</h4>
                 
               </div>
               <div >
                  <div>
                     <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                     <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <jsp:include page="/view/alert.jsp"></jsp:include>
                     </div>
                  </div>
                  <p><strong><br>Por favor ingresa el RFC de Emisor que se encuentra
                     registrado con nosotros, te enviaremos la contraseņa a tu correo.</strong>
                  </p>
                  <form name="formu" class="form-horizontal" role="form">
                     <div class="row form-group">	
                        <label for="rfc" class="col-sm-2 label-rfc">RFC: <span
                           ng-if="formu.rfc.$dirty && formu.rfc.$invalid"
                           style="color: #a94442">*</span> <span
                           ng-if="!(formu.rfc.$dirty && formu.rfc.$invalid)">*</span>
                        
                        </label>
                        <div class="col-sm-9" ng-class="{ 'has-error' : formu.rfc.$invalid && !formu.rfc.$pristine}">
                           <input type="text" class="input-rfc" id="rfc" name="rfc" placeholder="Rfc de Emisor"
                              ng-model="usuario.rfce" ng-blur="validaRfc(usuario.rfce)" autocomplete="off" maxlength="16"
                              capitalize paste-trimed required />
                           <div ng-show="formu.rfc.$dirty && formu.rfc.$invalid">
                              <p class="help-block" ng-show="formu.rfc.$error.required">Campo obligatorio</p>
                           </div>
                        </div>
                     </div>
                     <div class="row" >
                        <div class="col-sm-12">
                           <div class="form-group" align="center">
                              <div tabindex="3" theme="clean" vc-recaptcha key="'6LfFYG0UAAAAAJsylLTvKIbfdDpgiWPUIP0acdEB'" on-create="setWidgetId(widgetId)"></div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="modal-footer">
                  <div class="col-md-12" align="right">
                     <button type="button" class="btn-danger" ng-click="cancel()" data-dismiss="modal" id="close-window-recovery-password">
                     	Cancelar <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                     </button>
                     <button type="button" class="btn-accept" ng-disabled="formu.$invalid" ng-click="recuperar()" data-dismiss="modal">
                     	Recuperar contraseņa <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>

