
<div class="preguntas-frecuentes" id="tutorial-usuario">
	<div class="modal-header">
		<button id="close-window-cre" type="button" class="close"
			style="color: white;" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h2 class="modal-title">Tutorial Quadrum Basic</h2>
	</div>
	<div class="modal-body">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="560" height="315"
							src="https://www.youtube.com/embed/EM8yhAgVUZ8"
							title="YouTube video player" frameborder="0"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
							allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="560" height="315"
							src="https://www.youtube.com/embed/wlecHr3s-vk"
							title="YouTube video player" frameborder="0"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
							allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="560" height="315"
							src="https://www.youtube.com/embed/MoLMAHCBR4U"
							title="YouTube video player" frameborder="0"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
							allowfullscreen></iframe>

					</div>
				</div>

				<div class="col-lg-4">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="560" height="315"
							src="https://www.youtube.com/embed/ktwhuVvtbwk"
							title="YouTube video player" frameborder="0"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
							allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe width="560" height="315"
							src="https://www.youtube.com/embed/PUAbdOfutfE"
							title="YouTube video player" frameborder="0"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
							allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<div class="col-md-12" align="right">
			<button id="close-tutorial-usuario" type="button" class="btn-danger"
				ng-click="cancel()">
				Regresar <span class="glyphicon glyphicon-remove-circle"
					aria-hidden="true"></span>
			</button>
		</div>

	</div>


</div>
