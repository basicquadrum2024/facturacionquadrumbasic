<script
	src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit"
	async defer>
	
</script>


<div class="form-create-account content" id="crear-cuenta">
	<div>
		<div>
			<div class="modal-header">
				<button id="close-window-cre" type="button" class="close"
					style="color: white;" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">CREAR UNA CUENTA</h4>
			</div>
			<div class="modal-body">
				<form name="forma" class="form-horizontal" role="form" method="post">
					<div class="form-group">
						<div align="center" style="color: white;">* Campos
							obligatorios.</div>
					</div>
					<div class="form-group">
						<label for="rfcTemporal" class="col-sm-5 control-label">Rfc
							<span
							ng-if="forma.rfcTemporal.$dirty && forma.rfcTemporal.$invalid"
							style="color: #a94442">*</span> <span
							ng-if="!(forma.rfcTemporal.$dirty && forma.rfcTemporal.$invalid)">*</span>
							:
						</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : forma.rfcTemporal.$invalid && !forma.rfcTemporal.$pristine}">
							<input type="text" class="input-add" id="rfcTemporal"
								name="rfcTemporal" placeholder="Rfc de Emisor" maxlength="13"
								ng-minlength="12" ng-model="usuario.rfce"
								ng-blur="validaRfc(usuario.rfce)" capitalize paste-trimed
								required>
							<div
								ng-show="forma.rfcTemporal.$dirty && forma.rfcTemporal.$invalid">
								<p class="help-block"
									ng-show="forma.rfcTemporal.$error.required">Campo
									obligatorio</p>
							</div>
							<div class="help-block"
								ng-show="forma.rfcTemporal.$error.minlength">El rfc debe
								tener m�nimo 12 caracteres</div>
						</div>
					</div>
					<div class="form-group">
						<label for="nombre" class="col-sm-5 control-label">Nombre:
							<span ng-if="forma.nombre.$dirty && forma.nombre.$invalid"
							style="color: #a94442">*</span> <span
							ng-if="!(forma.nombre.$dirty && forma.nombre.$invalid)">*</span>
							:
						</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : forma.nombre.$invalid && !forma.nombre.$pristine}">
							<input type="text" class="input-add" id="nombre" name="nombre"
								placeholder="Nombre del usuario" maxlength="45"
								ng-model="usuario.nombre" autocomplete="off" capitalize
								paste-trimed required>
							<div ng-show="forma.nombre.$dirty && forma.nombre.$invalid">
								<p class="help-block" ng-show="forma.nombre.$error.required">Campo
									obligatorio</p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="apPat" class="col-sm-5 control-label">Apellido
							paterno: </label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : forma.apPat.$invalid && !forma.apPat.$pristine}">
							<input type="text" class="input-add" id="apPat" name="apPat"
								placeholder="Apellido Paterno" maxlength="45"
								ng-model="usuario.apPaterno" autocomplete="off" capitalize
								paste-trimed>
						</div>
					</div>
					<div class="form-group">
						<label for="apMat" class="col-sm-5 control-label">Apellido
							materno: </label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : forma.apMat.$invalid && !forma.apMat.$pristine}">
							<input type="text" class="input-add" id="apMat" name="apMat"
								placeholder="Apellido Materno" maxlength="45"
								ng-model="usuario.apMaterno" autocomplete="off" capitalize
								paste-trimed>
						</div>
					</div>
					<div class="form-group">
						<label for="telFijo" class="col-sm-5 control-label">Tel�fono
							1: <span ng-if="forma.telFijo.$dirty && forma.telFijo.$invalid"
							style="color: #a94442">*</span> <span
							ng-if="!(forma.telFijo.$dirty && forma.telFijo.$invalid)">*</span>
							:
						</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : forma.telFijo.$invalid && !forma.telFijo.$pristine}">
							<input type="text" class="input-add" id="telFijo"
								ng-pattern="/^[0-9]{10}$/" name="telFijo"
								placeholder="Tel�fono fijo" maxlength="13" ng-minlength="10"
								ng-model="usuario.telFijo" autocomplete="off" capitalize
								paste-trimed required>
							<div ng-show="forma.telFijo.$dirty && forma.telFijo.$invalid">
								<p class="help-block" ng-show="forma.telFijo.$error.required">Campo
									obligatorio</p>
							</div>
							<div ng-show="forma.telFijo.$dirty">
								<p class="help-block" ng-show="forma.telFijo.$error.pattern">Tel�fono
									inv�lido</p>
							</div>
							<div class="help-block" ng-show="forma.telFijo.$error.minlength">El
								Tel�fono tener m�nimo 10 caracteres</div>
						</div>
					</div>
					<div class="form-group">
						<label for="telCel" class="col-sm-5 control-label">Tel�fono
							2: </label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : forma.telCel.$invalid && !forma.telCel.$pristine}">
							<input type="text" class="input-add" id="telCel"
								ng-pattern="/^[0-9]{10}$/" name="telCel"
								placeholder="Tel�fono celular" maxlength="13" ng-minlength="10"
								ng-model="usuario.telCel" autocomplete="off" capitalize
								paste-trimed>
							<div ng-show="forma.telCel.$dirty">
								<p class="help-block" ng-show="forma.telCel.$error.pattern">Celular
									inv�lido</p>
							</div>
							<div class="help-block" ng-show="forma.telCel.$error.minlength">El
								Celular debe tener m�nimo 10 caracteres</div>
						</div>
					</div>
					<div class="form-group">
						<label for="correouno" class="col-sm-5 control-label">
							Correo electr�nico <span
							ng-if="forma.correouno.$dirty && forma.correouno.$invalid"
							style="color: #a94442">*</span> <span
							ng-if="!(forma.correouno.$dirty && forma.correouno.$invalid)">*</span>
							:
						</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : forma.correouno.$invalid && !forma.correouno.$pristine}">
							<input type="text" class="input-add" id="correouno"
								name="correouno" placeholder="Correo electr�nico"
								ng-maxlength="45" ng-model="usuario.correo" maxlength="46"
								ng-pattern="/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/"
								autocomplete="off" paste-trimed required />
							<div ng-show="forma.correouno.$dirty && forma.correouno.$invalid">
								<p class="help-block" ng-show="forma.correouno.$error.required">Campo
									obligatorio</p>
								<p class="help-block"
									text-danger" ng-show="forma.correouno.$error.pattern">Correo
									inv�lido</p>
							</div>
							<div class="help-block"
								ng-show="forma.correouno.$error.maxlength">El correo sobre
								pasa los 45 caracteres</div>
						</div>
					</div>
					<div class="form-group col-md-12" align="right">
						<button type="button" class="btn-accept-"
							ng-disabled="forma.correouno.$invalid" ng-click="enviarcodigo()">Enviar
							c&oacute;digo</button>
					</div>
					<div class="form-group">
						<label for="codigo" class="col-sm-5 control-label">
							C&oacute;digo de verificaci&oacute;n de correo electr&oacute;nico
							<span ng-if="forma.codigo.$dirty && forma.codigo.$invalid"
							style="color: #a94442">*</span> <span
							ng-if="!(forma.codigo.$dirty && forma.codigo.$invalid)">*</span>
							:
						</label>
						<div class="col-sm-7"
							ng-class="{ 'has-error' : forma.codigo.$invalid && !forma.codigo.$pristine}">
							<input placeholder="C&oacute;digo de verificaci&oacute;n"
								class="input-add" type="text" ng-model="codigo" id="codigo"
								name="codigo" required>
						</div>
					</div>

					<div class="form-group">

						<div align="center">
							<button type="button" class="btn-accept-"
								ng-click="descargaAvisos()">T�RMINOS Y CONDICIONES DE
								USO DE SERVICIO</button>

						</div>

					</div>

					<div class="form-group">
						<div align="center">
							<h4 class="span3 checkbox">
								<input type="checkbox" ng-model="terminos"
									title="He le�do y acepto las condiciones del servicio.">He
								le�do y acepto las condiciones del servicio.
							</h4>
						</div>
					</div>


					<div class="row">
						<div class="col-sm-12">
							<div class="form-group" align="center">
								<div tabindex="3" theme="clean" vc-recaptcha
									key="'6LfFYG0UAAAAAJsylLTvKIbfdDpgiWPUIP0acdEB'"
									on-create="setWidgetId(widgetId)"></div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="col-md-12" align="right">
					<button id="close-window-add-user" type="button" class="btn-danger"
						ng-click="cancel()">
						Cancelar <span class="glyphicon glyphicon-remove-circle"
							aria-hidden="true"></span>
					</button>
					<button type="button" class="btn-accept"
						ng-disabled="forma.$invalid" ng-click="registrar()">
						Registrar <span class="glyphicon glyphicon-ok-circle"
							aria-hidden="true"></span>
					</button>
				</div>
			</div>
		</div>
	</div>

</div>