<jsp:include
						page="modalModificaBuzon.jsp"></jsp:include>
<div class="content-body-bandeja-buzon">
<h1 class="text-center" style="color: white;">Bandeja buz&oacute;n de sugerencias y quejas</h1>
	
	<br>
	<br>
	<form name="formBuzon" id="formBuzon" ng-submit="buscar()" style="color: white !important;">
		<div class="row">
			<div class="form-group col-md-4"
				ng-class="{ 'has-error' : formBuzon.fecha1.$invalid}">
				<label class="control-label" for="fecha1"> * Fecha inicial:</label>
				<div>
					<input class="form-control" type="text" id="fecha1"
						name="fecha1"
						ng-model="wrapper.valor1" required autocomplete="off">
				</div>
				<p
					ng-show="formBuzon.fecha1.$error.required"
					class="help-block">Campo requerido.</p>
			</div>
			
			<div class="form-group col-md-4"
				ng-class="{ 'has-error' : formBuzon.fecha2.$invalid}">
				<label class="control-label" for="fecha2"> * Fecha final:</label>
				<div>
					<input class="form-control" type="text" id="fecha2"
						name="fecha2"
						ng-model="wrapper.valor2" required autocomplete="off">
				</div>
				<p
					ng-show="formBuzon.fecha1.$error.required"
					class="help-block">Campo requerido.</p>
			</div>
			
			<div  class="form-group col-md-4">
			<br>
			<input class="btn btn-primary btn-ok" type="submit" value="Buscar">
			</div>
			
			</div>
			<div class="row" >
			
			<div class="form-group col-md-6">
			<br>
			<button type="button" ng-disabled="!formBuzon.$valid" class="btn btn-primary btn-ok"
						ng-click="exportarExcel()">Descargar Excel</button>
			</div>
		</div>
		
		</form>
		
		<br>
		<br>
	<div class="table table-striped table-responsive table-bordered" data-ng-grid="gridOptions"
	style="height: 450px;"></div>
	
	<script type="text/javascript">
			$.datetimepicker.setLocale('es');
			$('#fecha1').datetimepicker({
				format : 'Y-m-d'
			});
			$('#fecha2').datetimepicker({
				format : 'Y-m-d'
			});
		</script>
</div>