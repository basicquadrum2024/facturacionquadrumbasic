<style>
.content-body {
	color:black;
}
</style>
<div class="modal fade content-body" data-backdrop="static" id="modalModificar" name="modalModificar">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title" style="color: black;">Mensaje de confirmaci&oacute;n</h4>
			</div>
			
			<div class="modal-body">
				<p>�Este mensaje fue revisado? </p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-ok" ng-click="modificar()"
					data-dismiss="modal">
					Aceptar<span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
			</div>

		</div>
	</div>
</div>