/**
 * Controlador para el modulo de administracion de usuario
 */
angular.module('basic').controller('adminController',['$scope','$http','globalMenssages','utileriasFactory','globalDownloadFile','$filter','$confirm','validacionesFactory','Flash',function($scope,$http,globalMenssages,utileriasFactory,globalDownloadFile,$filter,$confirm,validacionesFactory,Flash){		
	
	$scope.user={};
	
	$scope.filterOptions = {
	        filterText: "",
	        useExternalFilter: true
	};  
	$scope.totalServerItems = 0;
	$scope.pagingOptions = {
	        pageSizes: [25, 50, 100],
	        pageSize: 25,
	        currentPage: 1
	};	
	/**
	 * Funcion para la paginacion
	 */
	$scope.setPagingData = function(data, page, pageSize,totalResults){
		
        if(data!= undefined && !utileriasFactory.validarObjetoVacio(data)){
        	globalMenssages.existenCoindicencias();
        }else{
        	globalMenssages.noExistenCoindicencias();
        }
	    $scope.myData = data;
        $scope.pagingOptions.totalServerItems = totalResults;	        
        if (!$scope.$$phase) {
            $scope.$apply();
        }
	};
	/**
	 * funcion para paginar los datos de la tabla
	 */
	$scope.getPagedDataAsync = function (pageSize, page, searchText) {
		var post=null;
	        setTimeout(function () {
	            var data;
	            if (searchText) {
	            	post=getPost(page,pageSize)
	                var ft = searchText.toLowerCase();
	                $http.post(post).success(function (largeLoad) {
	                	var respuesta=largeLoad.list;
	                    data = respuesta.filter(function(item) {
	                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
	                    });
	                    $scope.setPagingData(data,page,pageSize,largeLoad.totalResults);
	                    
	                }).error(function(data, status, headers, config) {
	                });  	                
	            } else {
	            	
	            	post=getPost(page,pageSize)
	                $http.post(post).success(function (largeLoad) {
	                	var respuesta=largeLoad.list;	                	
	                    $scope.setPagingData(respuesta,page,pageSize,largeLoad.totalResults);	                    
	                }).error(function(data, status, headers, config) {
	                });
	                
	            }
	        }, 100);
	};
	
	$scope.$watch('miinput',function() {
		var data;
		var ft = $scope.miinput;
		if (ft) {
			data = $scope.myData
					.filter(function(item) {
						console.warn(ft);
						return JSON.stringify(item)
								.toString()
								.indexOf(ft) != -1;
					});
			$scope.myData = data;
		} else {
			reloadTable();
		}
	}, true);
		
	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
	          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
	        }
	}, true);
	$scope.$watch('filterOptions', function (newVal, oldVal) {		
	        if (newVal !== oldVal) {
	          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
	        }
	}, true);
		/**
		 * Componente de la tabla de busquedas
		 */
	    $scope.gridOptions = {
	        data: 'myData',
	        enablePaging: true,
			showFooter: true,
			enableRowSelection: false,
	  		showSelectionCheckbox: false,
	  		displaySelectionCheckbox: false,
	  		multiSelect:false,
	        totalServerItems: 'totalServerItems',
	        pagingOptions: $scope.pagingOptions,
	        filterOptions: $scope.filterOptions,
	        columnDefs: [
			{
				 field:'rfce', 
				 displayName:'R.F.C de emisor', 
				 width: '25%', 
			},
			{
				 field:'correo', 
				 displayName:'Nombre', 
				 width: '30%', 
			},
			{
				 field:'tcPerfil.descripcion', 
				 displayName:'Perfil', 
				 width: '25%', 
			},
			{
           	 field: '', 
           	 displayName: 'Editar', 
           	 width: '10%',
           	 cellTemplate: '<div align="center"><a href="" ng-click="updateUser(row.entity)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="left" title="Actualizar Usuario"><span class="glyphicon glyphicon-edit"></span></a>'
            },
            {
          	  field: '', 
          	  displayName: 'Eliminar', 
          	  width: '10%',
          	  cellTemplate: '<div align="center"><a href="" ng-click="deleteUser(row.entity)" class="btn btn-danger btn-xs red-tooltip" data-toggle="tooltip" data-placement="right" title="Eliminar Usuario"><span class="glyphicon glyphicon-remove"></span></a>' 
            }]
	        
	    };
	    function getPost(currentPage,pageSize){
			return 'adminController/findUsuario/'+currentPage+'/'+pageSize+'/'+'.json';
		};
		
//		$http.post('adminController/perfiles.json').success(function(data){
//			$scope.perfils=data;
//		});
		
		findAll();
		/**
		 * funcion para buscar todos los registros
		 */
		function findAll(){	
			$scope.postE=getPost($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);	    		
    		$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
		};
	    /**
	     * funcion para recargar la tabla
	     */
	    function reloadTable() {
			findAll();
		};
	   /**
	    * funcion para actualizar el usuario
	    */
		$scope.updateUser=function(obj){
			$scope.user=obj;
			$scope.mensaje = "Actualizar usuario.";
			$('#modalPrincipal').modal();
			
	 	};
	 	/**
	 	 * funcion para guardar el usuario
	 	 */
	 	$scope.saveUser = function(){
				$http.post('adminController/save.json', $scope.user).success(function(data){
					 findAll();	    
				}).error(function(data){			
					globalMenssages.globalError();
				});
			
	 	}
	 	/**
	 	 * funcion para eliminiar el usuario
	 	 */
	 	$scope.deleteUser = function(obj){
			$confirm({text:'\u00bfEsta usted seguro de que esta petici\u00f3n es correcta?',
				title: 'Eliminar usuario.', cancel: 'Cancelar', ok: 'Aceptar'}).then(function() {
					$http.post('adminController/delete.json',
							{'id' : obj.id}).success(function(data) {
								setTimeout(function() {
									reloadTable();
								},1);
							})
					.error(function(data) {
						globalMenssages.getMensajeErrorTarde();
					});
					
				});
		};
}]);