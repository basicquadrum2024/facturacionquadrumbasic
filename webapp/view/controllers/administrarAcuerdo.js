/**
 * Controlador para la administracion de los Acuerdos
 */
angular.module('basic').controller(
		'administrarAcuerdo',
		[
				'$scope',
				'$http',
				'Flash',
				'$confirm',
				'globalMensajes',
				'globalMenssages',
				'utileriasFactory',
				
				
				function($scope, $http, Flash, $confirm,
						globalMensajes, globalMenssages, utileriasFactory) {

					/**
					 * seccion para mostrar los datos en la tabla
					 */
					$scope.filterOptions = {
							filterText: ""
							};
					
					$scope.pagingOptions = {
							pageSizes: [25, 50, 100],
							pageSize: 25,
							totalServerItems: 0,
							currentPage: 1
							};
					
					$scope.setPagingData = function(data, page, pageSize) {
						var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
						$scope.myData = pagedData;
						$scope.pagingOptions.totalServerItems = data.length;
						if (!$scope.$$phase) {
						$scope.$apply();
						}
						};
						
					$scope.getPagedDataAsync = function(pageSize, page) {
						
						$scope.datos={
								'inicio': ($scope.pagingOptions.currentPage -1)* $scope.pagingOptions.pageSize,
								'totalMostrar': $scope.pagingOptions.pageSize
						}
							setTimeout(function() {      
							$http.post("acuerdo/listar.json",$scope.datos).success(function(largeLoad) {
							$scope.setPagingData(largeLoad, page, pageSize);
							});
							}, 100);
							};
							
					$scope.$watch('pagingOptions', function() {
								$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
								}, true);

					 $scope.combo='<div align="center"> <select class="form-control"  ng-model="row.entity.estatusArchivos" ng-change="cambiaEstatus(row.entity)" > '+
					''+
					'<option value="1" ng-selected="row.entity.estatusArchivos==1">Firmado</option>'+
					'<option value="0" ng-selected="row.entity.estatusArchivos==0">Sin firma</option>'+
					'</select>'+	 
					 '</div>';
	 
					    $scope.gridOptions = {
						        data: 'myData',
						        enablePaging: true,
								showFooter: true,
								enableRowSelection: false,
						  		showSelectionCheckbox: false,
						  		displaySelectionCheckbox: false,
						  		multiSelect:false,
						        totalServerItems: 'totalServerItems',
						        pagingOptions: $scope.pagingOptions,
						        filterOptions: $scope.filterOptions,
						        columnDefs: [
						        	{ field: '', displayName: 'Activar', width: '8%',cellTemplate: $scope.combo },
						                     {field:'rfc', displayName:'RFC', width: '25%'},
						                     {field:'nombre', displayName:'Nombre', width: '30%'},
						                     {field:'', displayName:'Manifiesto',width: '10%',
//						                    	 cellTemplate: '<div align="center" ><button   ng-click="descargaPdf(row.entity.rutaManifiesto)" '+
//												 'class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="Pdf" >'+
//												 '<img  src="resources/img/file_pdf.png" alt="pdf" width="24" height="24"></button></div>' },
						                    	 cellTemplate: '<div align="center" ng-hide="row.entity.rutaManifiesto === null"><a href="acuerdo/pdf.json?ruta={{row.entity.rutaManifiesto}}" target="_blank"'+
												 'class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="Pdf" >'+
												 '<img  src="resources/img/file_pdf.png" alt="pdf" width="24" height="24"></a></div>' },
						                     {field:'', displayName:'Contrato',width: '10%',
							                    	 cellTemplate: '<div align="center" ng-hide="row.entity.rutaContratoServicios === null"><a href="acuerdo/pdf.json?ruta={{row.entity.rutaContratoServicios}}" target="_blank"'+
													 'class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="Pdf" >'+
													 '<img  src="resources/img/file_pdf.png" alt="pdf" width="24" height="24"></a></div>' },
						                     {field:'', displayName:'Convenio', width: '10%',
								                    	 cellTemplate: '<div align="center" ng-hide="row.entity.rutaConvenioConfidencialidad === null"><a href="acuerdo/pdf.json?ruta={{row.entity.rutaConvenioConfidencialidad}}" target="_blank"'+
														 'class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="Pdf" >'+
														 '<img  src="resources/img/file_pdf.png" alt="pdf" width="24" height="24"></a></div>'  },
														 ]
						        
						    };
					    
//					    function descarga(ruta){
//					    	$scope.w=600;
//					    	$scope.h=800;
//					    	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
//					    	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
//
//					    	    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
//					    	    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
//
//					    	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
//					    	    var top = ((height / 2) - (h / 2)) + dualScreenTop;
//					    	    var newWindow = window.open(url, "Acuerdo.pdf", 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
//
//
//					    	    if (window.focus) {
//					    	        newWindow.focus();
//					    	    }
//					    }
					    
						$scope.totalRegistros= function () {
							
							$http.post("acuerdo/totalRegistros.json").success(
									function(data) {
										$scope.pagingOptions.totalServerItems=data;
									}).error(function(data) {
								globalMenssages.globalError();
							});
						}
						
						$scope.myData={};
						
						$scope.cambiaEstatus=function(entity){
							$scope.contribuyente={
									'id':entity.id,
									'estatusArchivos':1
							}
							$http.post("acuerdo/cambiaEstatus.json",$scope.contribuyente).success(
									function(data) {
										globalMensajes.getMensajeOperacionExito();
									}).error(function(data) {
								globalMenssages.globalError();
							});
						}


//						$scope.obtenerLista = function() {
//							
//							$scope.datos={
//									'inicio': ($scope.pagingOptions.currentPage -1)* $scope.pagingOptions.pageSize,
//									'totalMostrar': $scope.pagingOptions.pageSize
//							}
//							
//							$http.post("acuerdo/listar.json",$scope.datos).success(
//									function(data) {
//										console.log(data)
//										$scope.myData=data;
//									}).error(function(data) {
//								globalMenssages.globalError();
//							});
//						}
					    
				} ]);


