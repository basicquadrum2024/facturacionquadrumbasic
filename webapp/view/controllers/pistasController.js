/**
 * controler para gestionar las pistas
 */
angular.module('basic').controller(
        'pistasController',
        function( $scope , $http , Flash , ModalService , globalMensajes ,
                globalPaginacion , helperFactory, globalDownloadFile ){
            
            var paginador = null;
            var campo = 'fechahora';
            var joinn = [];
            $scope.mostrar = 1;
            
            $scope.filterOptions = globalPaginacion.filterOptions();
            $scope.pagingOptions = globalPaginacion.pagingOptions();
            
         // columnas a mostrar defaul
            var listaColumns = [
                    {field : 'cliente',displayName : 'Usuario',width : '15%'},
                    {field : 'sentenciasql',displayName : 'Acci\u00F3n',width : '15%'},
                    {field : 'objeto',displayName : 'Entidad',width : '15%'},
                    {field : 'accion',displayName : 'Descripci\u00F3n',width : '30%'},
                    {field : 'fechahora',displayName : 'Fecha',width : '25%',cellFilter : "date:'yyyy-MM-dd HH:mm:ss a'",},
                    {field : 'clientaddress',displayName : 'Direcci\u00F3n IP',width : '25%'},
                    {field : 'clientport',displayName : 'Puerto',width : '25%'}];
            
           /**
            * metodos de ng-grip
            */
            $scope.gridOptions = globalPaginacion.gridOptions(
                    listaColumns, $scope);

            $scope.$watch('pagingOptions',function(newVal, oldVal) {
                if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
                    $scope.getPagedDataAsync = globalPaginacion.getPagedDataAsync(
                            $scope,$scope.pagingOptions.pageSize,
                            $scope.pagingOptions.currentPage,$scope.filterOptions.filterText,
                            paginador,campo,joinn);
                    }
                }, true);
            
             $scope.$watch('miinput', function () {
                 var data;
                 var ft = $scope.miinput;
                  if (ft) {
                      data = $scope.dataGrid.filter(function(item) {
                            console.warn(ft);
                            return JSON.stringify(item).toString().indexOf(ft) != -1;
                        });
                      $scope.dataGrid = data;
                     
                  }else{
                  }
              }, true);
             
             $scope.buscaPistasAdminSAT = function() {
                 paginador = {
                     'klase' : 'TcPistasAdministrador',
                     'disyuncion' : null,
                     'conjuncion' : [
                                     'accion|' + $scope.accion,
                                     'entidad|' + $scope.entidad ],
                     'between' : [
                                  [ 'fechahora',
                                          $scope.fechaInicio + ':00' ],
                                  [ 'fechahora',
                                          $scope.fechaFinal + ':59' ] ],
                     'order' : [ 'fechahora' ],
                     'joinn' : null,
                     'urlRestDefault' : 'pistasController/listaPistasCliente.json',
                     'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'
                 };
 
                 $scope.pagingOptions.currentPage = 1;
                 $scope.getPagedDataAsync = globalPaginacion
                         .getPagedDataAsync($scope,
                                 $scope.pagingOptions.pageSize,
                                 $scope.pagingOptions.currentPage,
                                 null, paginador, 'tipo', null);
                 joinn = null;
                 globalPaginacion.watchfilterOptions($scope,
                         paginador, 'tipo', joinn);
                 document.getElementById('descargaPistasAdmin').style.visibility = 'visible';
              };

             $scope.buscaPistasAdmin = function() {
                    paginador = {
                        'klase' : 'TcPistasAdministrador',
                        'disyuncion' : null,
                        'conjuncion' : null,
                        'between' : [
                                     [ 'fechahora',
                                             $scope.fechaInicio + ':00' ],
                                     [ 'fechahora',
                                             $scope.fechaFinal + ':59' ] ],
                        'order' : [ 'fechahora' ],
                        'joinn' : null,
                        'urlRestDefault' : 'pistasController/listaDePistasAdmin.json',
                        'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'
                    };
    
                    $scope.pagingOptions.currentPage = 1;
                    $scope.getPagedDataAsync = globalPaginacion
                            .getPagedDataAsync($scope,
                                    $scope.pagingOptions.pageSize,
                                    $scope.pagingOptions.currentPage,
                                    null, paginador, 'tipo', null);
                    joinn = null;
                    globalPaginacion.watchfilterOptions($scope,
                            paginador, 'tipo', joinn);
                    document.getElementById('descargaPistasClientes').style.visibility = 'visible';
                 };
                 
                 /**
                  * fin de metodo ng-grip
                  */
                 
                 /**
                  * descargar archivo .xlsx con el reporte de las pistas del cliente
                  */
                 $scope.descargaPistasClientes = function(metodo) {
                     var excel = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                     var nombre = "Pistas_Cliente.xlsx"
                     if (isDate($scope.fechaInicio)) {
                         Flash.dismiss();
                         if (isDate($scope.fechaFinal)) {
                             Flash.dismiss();
                             var pistas = {
                                     fechaInicio : $scope.fechaInicio
                                             + ':00',
                                     fechaFin : $scope.fechaFinal
                                             + ':59'
                                 };
                             globalDownloadFile.getFileDownload("pistasController/"+metodo, pistas, excel, nombre, null);
                         }else{
                             var messageExito = "<strong> Aviso !</strong> La Fecha Final "
                                 + $scope.fechaFinal
                                 + " es inv\u00e1lida verifica por favor";
                         Flash.create('info', messageExito,
                                 'customAlertError');
                         }
                     }else{
                         var messageExito = "<strong> Aviso !</strong> La Fecha Inicial "
                             + $scope.fechaInicio
                             + " es inv\u00e1lida verifica por favor";
                     Flash.create('info', messageExito,
                             'customAlertError');
                     }
                    
                };
                 
                /**
                 * descargar archivo .xlsx con el reporte de las pistas del administrador
                 */
                 $scope.descargaPistasAdmin = function(metodo) {
                     var excel = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                     var nombre = "Pistas_Administrador.xlsx"
                     if (isDate($scope.fechaInicio)) {
                         Flash.dismiss();
                         if (isDate($scope.fechaFinal)) {
                             Flash.dismiss();
                             var pistas = {
                                    fechaInicio : $scope.fechaInicio
                                             + ':00',
                                     fechaFin : $scope.fechaFinal
                                             + ':59',
                                       accion : $scope.accion,
                                       entidad: $scope.entidad,
                                 };
                             globalDownloadFile.getFileDownload("pistasController/"+metodo, pistas, excel, nombre, null);
                         }else{
                             var messageExito = "<strong> Aviso !</strong> La Fecha Final "
                                 + $scope.fechaFinal
                                 + " es inv\u00e1lida verifica por favor";
                         Flash.create('info', messageExito,'customAlertError');
                         }
                     }else{
                         var messageExito = "<strong> Aviso !</strong> La Fecha Inicial "
                             + $scope.fechaInicio
                             + " es inv\u00e1lida verifica por favor";
                         Flash.create('info', messageExito,'customAlertError');
                     }
                    
                };
                 
                /**
                 * formateador de fecha
                 */
                 function isDate(txtDate) {
                     var currVal = txtDate;
                     if (currVal == '')
                         return false;
                     var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})(\s)(([01][0-9]|2[0-3]):[0-5][0-9])$/; // Declare
                                                                                                                         // Regex

                     // var rxDatePattern =
                     // /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
                     // //Declare Regex
                     var dtArray = currVal.match(rxDatePattern); // is
                                                                 // format
                                                                 // OK?

                     if (dtArray == null)
                         return false;

                     // Checks for mm/dd/yyyy format.
                     dtMonth = dtArray[3];
                     dtDay = dtArray[5];
                     dtYear = dtArray[1];

                     if (dtMonth < 1 || dtMonth > 12)
                         return false;
                     else if (dtDay < 1 || dtDay > 31)
                         return false;
                     else if ((dtMonth == 4 || dtMonth == 6
                             || dtMonth == 9 || dtMonth == 11)
                             && dtDay == 31)
                         return false;
                     else if (dtMonth == 2) {
                         var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                         if (dtDay > 29 || (dtDay == 29 && !isleap))
                             return false;
                     }
                     return true;
                 };
                 
                 $scope.tablaEntidad=[
                                      "[ seleccionar tabla ]", "TODOS", "ADMINISTRADOR", "CFDI", "CONCEPTOS", "CONTACTO", "CONTRIBUYENTE",
                                      "DOMICILIO", "DOMICILIOFISCAL", "EVENTOS", "RECEPTOR", "USERS" ];
                 
                  $scope.limpiar = function(estatus) {
                      $scope.mostrar = estatus;
                      $scope.fechaInicio = "";
                      $scope.fechaFinal = "";
                      $scope.accion="";
                      $scope.entidad="";
                      $scope.usuario ={};
                      
                      paginador = null;
                      $scope.pagingOptions.totalServerItems = 0;
                      $scope.dataGrid = [];
                      
                      document.getElementById('descargaPistasClientes').style.visibility = 'hidden';
                      document.getElementById('descargaPistasAdmin').style.visibility = 'hidden';
                  };
                  
                  
                  //<--------------------------------SEGUNDA PARTE DE ADMINISTRADOR -------------------------------------------------->
                
                  /**
                   * metodos CRUD
                   */
                  $scope.obtenerContacto = function(bandera) {
                    $http.post("administradorCuentasController/obtenerContacto/"+$scope.usuario.rfce+"/"+bandera+".json").
                    success(function(data, status, headers, config) {
                        if(data != null && data != ""){
                            globalMensajes.getMensajeOperacionExito();
                            $scope.usuario = data;
                            
                            document.getElementById('guardaContacto').style.visibility = 'visible';
                            
                            document.getElementById('activarContacto').style.visibility = 'visible';
                        }else{
                            globalMensajes.getCoincidenciasSinExito();
                            $scope.usuario={};
                        }
                    }).error(function(data, status, headers, config) {
                    	globalMensajes.getMensajeErrorTarde();
                    });
                };
                
                $scope.guardarContacto = function() {
                    $http.post("administradorCuentasController/guardarContacto.json", $scope.usuario).
                    success(function(data, status, headers, config) {
                        globalMensajes.getMensajeOperacionExito();
                    }).error(function(data, status, headers, config) {
                        globalMensajes.getMensajeError("Error al cambiar informacion.");
                    });
                };
                
                $scope.activaCuenta = function () {
                    $http.post("administradorCuentasController/activarCuenta.json", $scope.usuario).
                    success(function(data, status, headers, config) {
                        globalMensajes.getMensajeOperacionExito();
                        $scope.usuario={};
                    }).error(function(data, status, headers, config) {
                        globalMensajes.getMensajeError("Error al Activar la Informacion.");
                        $scope.usuario={};
                    });
                };
                
                $scope.clientesRegistrados= function() {
                    $http.post("emisorController/todosEmisores.json")
                    .success(function(data, status, headers, config) {
                        if(data != null && data != ""){
                            globalMensajes.getMensajeOperacionExito();
                            $scope.listaEmisores =data;
                        }else{
                            globalMensajes.getCoincidenciasSinExito();
                        }
                    });
                }
                    
                    $scope.descargaClientesRegistrados = function(metodo) {
                        var excel = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var nombre = "Clientes_Registrados.xlsx"
                        globalDownloadFile.getFileDownload("usuarioController/"+metodo, "", excel, nombre, null);
                       
                   };
                   
                 /**
                  * FIN DE METODOS CRUD
                  */
        });
