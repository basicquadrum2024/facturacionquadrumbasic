/**
 * Controlador para el modulo de Administracion
 */
angular.module('basic').controller(
				"controlAdministrador",
				function($scope, ModalService, $http, $rootScope, $element, Flash,
						globalPaginacion, $confirm) {
					$scope.filterOptions = globalPaginacion.filterOptions();
					$scope.totalServerItems = 0;
					$scope.pagingOptions = globalPaginacion.pagingOptions();
					var paginador = null;
					var campo = 'login';
					var joinn = null;
					$scope.valor = '';
					
					// para calculo de total de paginas
					$scope.Math = window.Math;
					// texto holder de input de busqueda
					$scope.placeholderText = "Todos los criterios";
					$scope.displaySearch = 'inline';
					/**
					 * Componente para la tabla de busquedas
					 */
					var listaColumns = [
							{
								field : '',
								displayName : 'Opciones',
								width : '15%',
								cellTemplate : '<a href="" ng-click="editar(row.entity)" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Actualizar Administrador"><span class="glyphicon glyphicon-edit"></span></a><a href="" ng-click="borrar(row.entity)" class="btn btn-danger btn-xs red-tooltip" data-toggle="tooltip" data-placement="right" title="Eliminar Administrador"><span class="glyphicon glyphicon-remove"></span></a>'
							}, {
								field : 'fechaCreacion',
								displayName : 'Fecha de creaci\u00f3n',
								width : '22%',
								cellFilter : "date:'yyyy-MM-dd HH:mm:ss a'"
							}, {
								field : 'login',
								displayName : 'Usuario',
								width : '35%'
							}, {
								field : 'perfil.rol',
								displayName : 'Perfil',
								width : '10%'
							}, {
								field : 'nombre',
								displayName : 'Nombre',
								width : '18%'
							}, {
								field : 'apellidopaterno',
								displayName : 'Apellido Paterno',
								width : '10%'
							}, {
								field : 'apellidomaterno',
								displayName : 'Apellido Materno',
								width : '10%'
							}, {
								field : 'rfcEmisor',
								displayName : 'RFC Emisor',
								width : '10%'
							},
					
					];
					/**
					 * Componente para la paginacion
					 */
					$scope.gridOptions = globalPaginacion.gridOptions(listaColumns, $scope);
					$scope.$watch('pagingOptions', function(newVal, oldVal) {
						if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
							$scope.getPagedDataAsync = globalPaginacion.getPagedDataAsync(
									$scope, $scope.pagingOptions.pageSize,
									$scope.pagingOptions.currentPage,
									$scope.filterOptions.filterText, paginador, campo,
									joinn);
						}
					}, true);
					/**
					 * Componente para actualizar el componente de la tabla de busquedas
					 */
					$scope.$watch('miinput', function() {
						var data;
						var ft = $scope.miinput;
						if (ft) {
							data = $scope.dataGrid.filter(function(item) {
								return JSON.stringify(item).indexOf(ft) != -1;
							});
							$scope.dataGrid = data;
						}
						else {
							recargar();
						}
					}, true);
					/**
					 * Funcion para regargar el componente de busquedas
					 * @returns
					 */
					function recargar() {
						paginador = {
							'klase' : 'Usuario',
							'disyuncion' : null,
							'conjuncion' : null,
							'between' : null,
							'order' : [
								'idusuario'
							],
							'joinn' : joinn,
							'urlRestDefault' : 'administradorController/consultaPlazaAdministrador.json',
							'urlRestBusca' : 'administradorController/busquedaLike.json'
						
						};
						$scope.pagingOptions.currentPage = 1;
						$scope.getPagedDataAsync = globalPaginacion.getPagedDataAsync($scope,
								$scope.pagingOptions.pageSize,
								$scope.pagingOptions.currentPage, null, paginador, 'login',
								null);
						joinn = joinn;
						globalPaginacion.watchfilterOptions($scope, paginador, campo, joinn);
					}
					;
					/**
					 * Funcion para crear un nuevo registro
					 */
					$scope.nuevo = function() {
						$rootScope.admin = '';
						$rootScope.valor = false;
						$rootScope.valorcontrosena = false;
						$scope.showModalCreateAdmin();
					};
					/**
					 * funcion para mostrar un modal
					 */
					$scope.showModalCreateAdmin = function() {
						
						ModalService.showModal({
							templateUrl : 'view/admin/catalogos/administrador_modal.jsp',
							controller : "controlAdministrador",
						
						}).then(function(modal) {
							modal.element.modal();
							modal.close.then(function(result) {
							});
						});
					};
					/**
					 * funcion para borrar un registro
					 */
					$scope.borrar = function(admin) {
						var errorAd = "<strong>Error al borrar Datos !!</strong>";
						$confirm({
							text : 'Estas seguro de poner inactivo al Administrador?',
							title : 'Confirmacion de datos',
							ok : 'Aceptar',
							cancel : 'Cancelar'
						})
								.then(
										function() {
											popupLoading.modal('show');
											$http
													.post(
															'administradorController/deletePlazaAdministrador.json',
															admin.idusuario)
													.success(
															function(data, status,
																	headers,
																	config) {
																if (data == "success") {
																	popupLoading
																			.modal('hide');
																	var messageError = "<strong>Exito !</strong> Se ha puesto inactivo el Administrador";
																	Flash
																			.create(
																					'info',
																					messageError,
																					'customAlertSuccces');
																}
																else {
																	popupLoading
																			.modal('hide');
																	Flash
																			.create(
																					'danger',
																					errorAd,
																					'customAlertDanger');
																}
																
															})
													.error(
															function(data, status,
																	headers,
																	config) {
																Flash
																		.create(
																				'danger',
																				'Intentalo mas tarde',
																				'customAlertDanger');
															});
										});
					};
					/**
					 * funcion para editar un nuevo registro
					 */
					$scope.editar = function(admin) {
						$rootScope.admin = admin;
						$rootScope.valor = true;
						$rootScope.valorcontrosena = true;
						$scope.showModalCreateAdmin();
						$scope.updatePermisos(admin.rfcEmisor);
					};
					
					$scope.admin = $rootScope.admin;
					$scope.valor = $rootScope.valor;
					$scope.valorcontrosena = $rootScope.valorcontrosena;
					if ($scope.valor == '') {
						$scope.dato = 'Crear';
					}
					else {
						$scope.dato = 'Actualizar';
					}
					/**
					 * funcion para actualizar un registro
					 */
					$scope.updatePermisos = function(rfc) {
						$http.post('plazacobro/consultaEmisorPorRFC/' + rfc + '.json')
								.success(function(data) {
									$rootScope.sucursal = data;
								});
					};
					/**
					 * funcion para cambiar sucursal
					 */
					$scope.cambioSursursal = function(rfc) {
						$rootScope.admin.sucursal=null;
						$http.post('plazacobro/consultaEmisorPorRFC/' + rfc + '.json')
								.success(function(data) {
									$rootScope.sucursal = data;
								});
					};
					/**
					 * funcion para consultar el perfil
					 */
					$http.post('administradorController/consultaPlazaPerfil.json').success(
							function(data) {
								$scope.perfil = data;
							});
					/**
					 * funcion para consultar el emisor
					 */
					$http.post('plazacobro/consultaEmisor.json').success(function(data) {
						$scope.emisor = data;
						$scope.listaEmisor = {};
						for ( var i = 0; i < $scope.emisor.length; i++) {
							$scope.listaEmisor[$scope.emisor[i].rfc] = $scope.emisor[i].rfc;
						}
					});
					/**
					 * Funcion para guardar el administrador
					 */
					$scope.saveAdministrador = function() {
						popupLoading.modal('show');
						if (!$scope.admin.perfil == "") {
							
						}
						else {
							$scope.admin.perfil = null;
						}
						if (!$scope.admin.sucursal == "") {
							
						}
						else {
							$scope.admin.sucursal = null;
						}
						if (!$scope.admin.rfcEmisor == "") {
							
						}
						else {
							$scope.admin.rfcEmisor = '';
						}
						if (!$scope.admin.nombre == "") {
							
						}
						else {
							$scope.admin.nombre = '';
						}
						if (!$scope.admin.apellidopaterno == "") {
							
						}
						else {
							$scope.admin.apellidopaterno = '';
						}
						if (!$scope.admin.apellidomaterno == "") {
							
						}
						else {
							$scope.admin.apellidomaterno = '';
						}
						if (!$scope.admin.idusuario == "") {
							$scope.usuario = {
								'idusuario' : $scope.admin.idusuario,
								'perfil' : $scope.admin.perfil,
								'sucursal' : $scope.admin.sucursal,
								'rfcEmisor' : $scope.admin.rfcEmisor,
								'fechaModificacion' : new Date(),
								'primeraSession' : 2,
								'nombre' : $scope.admin.nombre,
								'apellidopaterno' : $scope.admin.apellidopaterno,
								'apellidomaterno' : $scope.admin.apellidomaterno
							
							};
							$http
									.post(
											'administradorController/updateAdministrador.json',
											$scope.usuario)
									.success(
											function(data, status, headers, config) {
												if (data == "success") {
													popupLoading.modal('hide');
													var messageError = "<strong>Exito !</strong> Aministrador actualizado.";
													Flash.create('info', messageError,
															'customAlertSuccces');
												}
												else {
													popupLoading.modal('hide');
													var messageError = "<strong>Error !</strong> Los datos no se actualizan";
													Flash.create('danger',
															messageError,
															'customAlertError');
												}
											})
									.error(
											function(data, status, headers, config) {
												popupLoading.modal('hide');
												var messageError = "<strong>Aviso !</strong> intentalo mas tarde";
												Flash.create('danger', messageError,
														'customAlertError');
											});
						}
						else {
							
							$scope.usuario = {
								'perfil' : $scope.admin.perfil,
								'sucursal' : $scope.admin.sucursal,
								'fechaCreacion' : new Date(),
								'rfcEmisor' : $scope.admin.rfcEmisor,
								'login' : $scope.admin.login,
								'password' : $scope.admin.password,
								'statusCuenta':1,
								'primeraSession' : 0,
								'nombre' : $scope.admin.nombre,
								'apellidopaterno' : $scope.admin.apellidopaterno,
								'apellidomaterno' : $scope.admin.apellidomaterno
							
							};
							$http
									.post(
											'administradorController/saveAdministrador.json',
											$scope.usuario)
									.success(
											function(data, status, headers, config) {
												if (data == "success") {
													popupLoading.modal('hide');
													var messageError = "<strong>Exito !</strong> Se guardo el Administrador";
													Flash.create('info', messageError,
															'customAlertSuccces');
												}
												if (data == "existe") {
													popupLoading.modal('hide');
													message = "<strong>Error al crear la cuenta!</strong>El correo electronico ya se encuentra registrado.";
													Flash.create('info', message,
															'customAlertError');
												}
												if (data == "error") {
													popupLoading.modal('hide');
													var messageError = "<strong>Aviso !</strong> intentalo mas tarde";
													Flash.create('danger',
															messageError,
															'customAlertError');
												}
											})
									.error(
											function(data, status, headers, config) {
												popupLoading.modal('hide');
												var messageError = "<strong>Aviso !</strong> intentalo mas tarde";
												Flash.create('danger', messageError,
														'customAlertError');
											});
						}
						
					}
                   /**
                    * Funcion para cerrar el modal
                    */
					$scope.close = function(result) {
						$element.modal('hide');
						close(result, 500);
					};
				});