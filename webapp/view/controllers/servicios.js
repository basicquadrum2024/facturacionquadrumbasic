/**
 * Servicios para obtener catalogos SAT
 */
angular.module('basic').service('catalogosServices',['$q','$http',function($q,$http){
	this.servicioBuscarClavesUnidadLikeNombre=function(nombre){
		var deferred = $q.defer();
		 $http.post('catalogosSATController/buscarClavesUnidadLikeNombre/'+nombre+'/.json').success(function(data){
			    deferred.resolve(data);
		 });
		 return deferred.promise;	
	};
	
	this.servicioBuscarClaveProdServLikeNombre=function(descripcion){
		 var deferred = $q.defer();
		 $http.post('catalogosSATController/buscarClaveProdServLikeNombre/'+descripcion+'/.json').success(function(data){
			    deferred.resolve(data);
		 });
		 return deferred.promise;
	};
	/**
	 * Servicio para buscar tasa o cuota
	 */
	this.buscarTasasOCuotas=function(impuesto,factor,tipoImpuesto){
		var deferred=$q.defer();
		$http.post('catalogosSATController/buscarTasasOCuotas/'+impuesto+"/"+factor+'/.json',tipoImpuesto).success(function(data){
		    deferred.resolve(data);
		 });
		 return deferred.promise;
	}
	
	this.servciocalculosComprobante=function(comprobante){
		 var deferred = $q.defer();
		 $http.post('comprobante/calculosComprobante.json',comprobante).success(function(data){
			    deferred.resolve(data);
		 });		 
		 return deferred.promise;
	};
	
	this.servciocalculosComprobanteV40=function(comprobante){
		 var deferred = $q.defer();
		 $http.post('comprobante_v40/calculosComprobante.json',comprobante).success(function(data){
			    deferred.resolve(data);
		 });		 
		 return deferred.promise;
	};
	
	this.buscarTiposFactor=function(tipoImpuesto){
		var deferred = $q.defer();
		 $http.post('catalogosSATController/buscarTiposFactor/'+tipoImpuesto+'/.json').success(function(data){
			    deferred.resolve(data);
		 });		 
		 return deferred.promise;
	};
	
	this.validarExistenciaClaveUnidad=function(clave){
		var deferred = $q.defer();
		 $http.post('catalogosSATController/validarExistenciaClaveUnidad/'+clave+'/.json').success(function(data){
			    deferred.resolve(data);
		 });		 
		 return deferred.promise;
	};
	
	this.validarExistenciaProvedorServicio=function(clave){
		var deferred = $q.defer();
		 $http.post('catalogosSATController/validarExistenciaProvedorServicio/'+clave+'/.json').success(function(data){
			    deferred.resolve(data);
		 });		 
		 return deferred.promise;
	};
	
	this.validarContinuidadTimbrado=function(){
		var deferred = $q.defer();
		 $http.post('comprobante/validarContinuidadTimbrado.json').success(function(data){
			    deferred.resolve(data);
		 });		 
		 return deferred.promise;
	};
	
	this.validarClaveConfirmacion=function(claveConfirmacion){
		var deferred = $q.defer();
		 $http.post('comprobante/validarClaveConfirmacion/'+claveConfirmacion+'/.json').success(function(data){
			    deferred.resolve(data);
		 });		 
		 return deferred.promise;
	};
	/**
	 * Servicio para buscar tipofactor
	 */
	this.buscarTipoFactorTipoImpuesto=function(impuesto,tipoImpuesto){
		var deferred=$q.defer();
		$http.post('catalogosSATController/buscarTiposFactorTipoImpuesto/'+impuesto+'/.json',tipoImpuesto).success(function(data){
		    deferred.resolve(data);
		 });
		 return deferred.promise;
	}
}]);