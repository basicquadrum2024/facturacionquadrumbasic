/**
 * Controlador para la busqueda de cfdis
 */
angular.module('basic').controller('busquedaController',['$scope','$http','globalMenssages','utileriasFactory','globalDownloadFile','$filter','$confirm','validacionesFactory','Flash',function($scope,$http,globalMenssages,utileriasFactory,globalDownloadFile,$filter,$confirm,validacionesFactory,Flash){		
	
	$scope.mensaje="Buscar comprobantes";	
	$scope.listaCriterios=[{id:1,value:"FOLIO FISCAL"},{id:2,value:"RFC-RECEPTOR"},{id:3,value:"ESTATUS"},{id:4,value:"FECHA"},{id:5,value:"CLAVE CONFIRMACI\u00d3N"}];
    $scope.listaCriteriosCancelacion =[{clave:"01", descripcion:"Comprobante emitido con errores con relación"}, {clave:"02", descripcion:"Comprobante emitido con errores sin relación"}, {clave:"03", descripcion:"No se llevó a cabo la operación "}, {clave:"04", descripcion:"Operación nominativa relacionada en una factura global"}];
	$scope.buscarReceptorRfc = function() {	
		if(validacionesFactory.validateRfc($scope.rfcReceptor) && $scope.rfcReceptor!=''){
		}else{
			var messageError = "<strong>ERROR !</strong> EL RFC no cumple con un patr\u00f3n v\u00e1lido, Ejemplo:<br>Persona F\u00edsica AAAA010101AAA <br> Persona MoralAAA010101AAA";
			Flash.create('danger', messageError, 'customAlertError');
		}
	};	
	
	$scope.tab = 1;
	$scope.setTab = function(newTab) {
		$scope.listaEnvio=[];
		$scope.listaCancelar=[]
		$scope.myData=[];
		$scope.tab = newTab;
	};

	$scope.isSet = function(tabNum) {
		return $scope.tab === tabNum;
	};
	
	$scope.listaEnvio=[];
	$scope.listaCancelar=[];
	$scope.Correos=[];
	
	$scope.idModalT="idmodalreutilizable";
	$scope.headerModalT="Confirmaci\u00f3n de datos.";
	/**
	 * Funcion que muestra el modal
	 */
    function muestraModal(value) {
		$scope.resultadoWrappers=value;
		$("#idmodalreutilizable").modal();
	};
	/**
	 * Funcion para cargar el modal
	 */
	$scope.cargar=function(){
		$("#idmodalreutilizable").modal();
	};
	/**
	 * Componente de paginacion
	 */
    $scope.filterOptions = {
	        filterText: "",
	        useExternalFilter: true
	};    
	$scope.totalServerItems = 0;
	$scope.pagingOptions = {
	        pageSizes: [25, 50, 100],
	        pageSize: 25,
	        currentPage: 1
	};	
	$scope.setPagingData = function(data, page, pageSize,totalResults){
		
	        if(data!= undefined && !utileriasFactory.validarObjetoVacio(data)){
	        	globalMenssages.existenCoindicencias();
	        }else{
	        	globalMenssages.noExistenCoindicencias();
	        }
		    $scope.myData = data;
	        $scope.pagingOptions.totalServerItems =totalResults;	        
	        if (!$scope.$$phase) {
	            $scope.$apply();
	        }
    };
	$scope.getPagedDataAsync = function (pageSize, page, searchText) {
		var post=null;
	        setTimeout(function () {
	            var data;	            
	            if (searchText) {
	            	post=cambioCriterio($scope.criterio.id,page,pageSize);
	                var ft = searchText.toLowerCase();
	                $http.post(post).success(function (largeLoad) {
	                	var respuesta=largeLoad.list;
	                    data = respuesta.filter(function(item) {
	                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
	                    });
	                    $scope.setPagingData(data,page,pageSize,largeLoad.totalResults);
	                    
	                }).error(function(data, status, headers, config) {
	                });  	                
	            } else {
	            	post=cambioCriterio($scope.criterio.id,page,pageSize);
	                $http.post(post).success(function (largeLoad) {
	                	var respuesta=largeLoad.list;	                	
	                    $scope.setPagingData(respuesta,page,pageSize,largeLoad.totalResults);	                    
	                }).error(function(data, status, headers, config) {
	                });
	                
	            }
	        }, 100);
	};
		/**
		 * Componente para actualizar la tabla de busquedas
		 */	
	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {			
	          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
	        }
	}, true);
	$scope.$watch('filterOptions', function (newVal, oldVal) {		
	        if (newVal !== oldVal) {
	          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
	        }
	}, true);
		
	$scope.estatusCfdi = '<div  class="ui-grid-cell-contents" ng-init="estatuCfdi=row.entity.twCfdi.estatus">'+
								'<div ng-if="estatuCfdi === 1"><span class="label label-success">Activo</span></div>'+
								'<div ng-if="estatuCfdi === 0"><span class="label label-danger">Cancelado</div></div>'+
							 '</div>';
	/**
	 * Componente de la tabla de busquedas
	 */
	    $scope.gridOptions = {
	        data: 'myData',
	        enablePaging: true,
			showFooter: true,
			enableRowSelection: false,
	  		showSelectionCheckbox: false,
	  		displaySelectionCheckbox: false,
	  		multiSelect:false,
	        totalServerItems: 'totalServerItems',
	        pagingOptions: $scope.pagingOptions,
	        filterOptions: $scope.filterOptions,
	        columnDefs: [{ field: '', displayName: 'Seleccionar', width: '8%',cellTemplate: '<div align="center"><input type="checkbox" name="radioSeleccionado" ng-click="addRemoveConceptos(row.entity.twCfdi.id)" ng-disabled="row.entity.twCfdi.estatus === 0"/></div>'},
	                     { field: '', displayName: 'Cancelar', width: '7%',cellTemplate: '<div align="center"><input type="checkbox" name="radioSeleccionado" ng-click="addCancelar(row.entity.twCfdi.id)" ng-disabled="row.entity.twCfdi.estatus === 0"/></div>'},
	                     { field: '', displayName: 'XML', width: '5%',
	                    	 cellTemplate: '<div align="center"><a href="documentos/descarga.json?opcion=xml&id={{row.entity.twCfdi.id}}" class="btn btn-default btn-xs" data-toggle="tooltip" '+
							 'data-placement="left" title="Xml"><img src="resources/img/file_xml.png" alt="xml" width="24" height="24"></a></div>'},
	                     { field: '', displayName: 'PDF', width: '5%',
//	                    	 cellTemplate: '<a class="glyphicon glyphicon-download" href="documentos/descarga.json?opcion=xml&id={{row.entity.twCfdi.id}}">&nbsp;XML</a>&nbsp;&nbsp;&nbsp;<a class="glyphicon glyphicon-download" href="documentos/descarga.json?opcion=pdf&id={{row.entity.twCfdi.id}}" ng-disabled="row.entity.twCfdi.estatus === 0">&nbsp;PDF</a>'
                    		 cellTemplate: '<div align="center" ng-show="row.entity.twCfdi.estatus === 1"><a href="documentos/descarga.json?opcion=pdf&id={{row.entity.twCfdi.id}}" '+
							 'class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="Pdf" >'+
							 '<img  src="resources/img/file_pdf.png" alt="pdf" width="24" height="24"></a></div>'
	                     },
	                     {field:'twCfdi.estatus', displayName:'Estatus', width: '7%',cellTemplate : $scope.estatusCfdi},
	                     {field:'twCfdi.uuid', displayName:'Folio fiscal', width: '26%'},
	                     {field:'tcContribuyente.rfc', displayName:'Rfc emisor',width: '11%'},
	                     {field:'tcReceptor.rfc', displayName:'Rfc receptor',width: '11%'},
	                     {field:'twCfdi.fechaCreacion', displayName:'Fecha creaci\u00f3n', width: '9%',cellFilter: "date:'yyyy-MM-dd HH:mm:ss a'"},
	                     {field:'twCfdi.serie', displayName:'Serie', width: '6%'},
	                     {field:'twCfdi.folio', displayName:'Folio',width: '5%'}]
	        
	    };
//	    cellFilter:'valorEstatus'
	    if($scope.rol=='ADMIN'){	    	
	    	$scope.gridOptions.columnDefs.push({field:'total', displayName:'Total'});
	    	$scope.gridOptions.columnDefs.push({field:'correoenvio', displayName:'Correo Envio'});
	    }
	    
	    $scope.cambio=function(){
	    	$scope.fechaInicio='';
	    	$scope.fechaFin='';
	    	$scope.estatus='';
	    	$scope.importe=0;
	    	$scope.myData=[];
	    };
	    $scope.addRemoveConceptos=function(object){	    	
	    	var index = $scope.listaEnvio.indexOf(object);    		
			if(index == -1){
				$scope.listaEnvio.push(object);	
			}else{
				$scope.listaEnvio.splice(index, 1); 
			}			
	    };
	    
	    $scope.addCancelar=function(object){	    	
	    	var index = $scope.listaCancelar.indexOf(object);    		
			if(index == -1){
				$scope.listaCancelar.push(object);	
			}else{
				$scope.listaCancelar.splice(index, 1); 
			}			
	    };
	    /**
	     * Funcion para la busqueda de criterios
	     */
	    $scope.buscarCriterios=function(){
	    	globalMenssages.dismiss();
	    	if($scope.criterio != undefined){
	    		if(validacionForm($scope.criterio.id)){
	    			$scope.listaEnvio=[];
	    			$scope.listaCancelar=[];
	    			$scope.postE=cambioCriterio($scope.criterio.id, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);	    		
		    		$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
	    		}else{
	    			globalMenssages.requeridos();
	    		}	    		
	    	}	    	
	    }
			/**
			 * Funcion para enviar por correo
			 */	
		$scope.enviarE=function(){
			$http.post("comprobante/mandarComprobantePorCorreo.json",$scope.listaEnvio).success(function(data){
				muestraModal(data);
	    		setTimeout(function() {
		 			$('#consulta').click();
		 		}, 1);
			}).error(function(data){
				globalMenssages.globalError();
			});
		};
		
	    /**
	     * funcion para la validacion de formularios
	     */
	    function validacionForm(opcion) {
	    	var respuesta=false;
	    	switch (opcion) {
			case 1:				
				respuesta=$scope.formFiscalValido;
				break;
			case 2:		
				respuesta=$scope.formReceptorValido;
				break;
			case 3:		
				respuesta=$scope.formEstatusValido;
				break;	
			case 4:		
				respuesta=$scope.formFechasValido;
				break;	

			case 5:
				respuesta=$scope.formConfirmacionValido;
				break;
			case 6:				
				respuesta=$scope.formFiscalValido32;
				break;
			case 7:		
				respuesta=$scope.formReceptorValido;
				break;
			case 8:		
				respuesta=$scope.formEstatusValido;
				break;	
			case 9:		
				respuesta=$scope.formFechasValido;
				break;	

			case 10:
				respuesta=$scope.formImporteValido;
				break;	
			default:
				break;
			}
	    	return respuesta;
		};
	    /**
	     * Funcion para le cambio de criterios de la busqueda
	     */
	    function cambioCriterio(opcion,currentPage,pageSize){
	    	var respuesta=null;
	    	switch(opcion){
	    	case 1:	    		
	    		respuesta="comprobante/buscarComprobantesPorUuid/"+currentPage+"/"+pageSize+"/"+$scope.uuid+"/.json";
	    		break;
	    	case 2:	    		
    			respuesta="comprobante/buscarComprobantesPorReceptor/"+currentPage+"/"+pageSize+"/"+$scope.rfcReceptor+"/.json";	
    		break;	
	    	case 3:	    		
    			respuesta="comprobante/buscarComprobantesPorEstatus/"+currentPage+"/"+pageSize+"/"+$scope.estatus+"/.json";	
    		break;	
	    	case 4:	
    			var fechaInicio=$('#fechaInicio').val();
	    		var fechaFin=$('#fechaFin').val();	    
    			respuesta="comprobante/buscarComprobantesPorFechas/"+currentPage+"/"+pageSize+"/"+fechaInicio+":00/"+fechaFin+":59.json";	
    		break;
	    	case 5:
	    		respuesta="comprobante/buscarComprobantesPorConfirmacion/"+currentPage+"/"+pageSize+"/"+$scope.confirmacion+"/.json";
	    		break;
	    	case 6:	    		
	    		respuesta="quadrumFree/buscarComprobantesPorUuid/"+currentPage+"/"+pageSize+"/"+$scope.uuid32+"/.json";
	    		break;
	    	case 7:	    		
    			respuesta="comprobante/buscarComprobantesPorReceptor/"+currentPage+"/"+pageSize+"/"+$scope.rfcReceptor+"/.json";	
    		break;	
	    	case 8:	    		
    			respuesta="comprobante/buscarComprobantesPorEstatus/"+currentPage+"/"+pageSize+"/"+$scope.estatus+"/.json";	
    		break;	
	    	case 9:	
    			var fechaInicio=$('#fechaInicio').val();
	    		var fechaFin=$('#fechaFin').val();	    
    			respuesta="comprobante/buscarComprobantesPorFechas/"+currentPage+"/"+pageSize+"/"+fechaInicio+":00/"+fechaFin+":59.json";	
    		break;
	    	case 10:
	    		respuesta="comprobante/buscarComprobantesPorImporte/"+currentPage+"/"+pageSize+"/"+$scope.importe+"/.json";
	    		break;	
	    	}
	    	return respuesta;
	    };
	    /**
	     * funcion para agregar correos a una lista
	     */
	    $scope.addCorreos=function(keyEvent){		
			 if (keyEvent.which === 13){	
				 if($scope.correo!=null && $scope.correo.length>0 ){
				  var index =  $scope.Correos.indexOf($scope.correo);    		
					if(index == -1){
						$scope.Correos.push($scope.correo);
					}else{
						$scope.Correos.splice(index, 1); 
						$scope.Correos.push($scope.correo);
					}
					$scope.correo=null;
				 }else{
					 var messageInfo = "<strong>ATENCION!</strong> El campo correo esta vac&iacute;o";
						Flash.create('info', messageInfo, 'customAlertError');
				 }
			 }
		};
		/**
		 * Modal para agregar correos
		 */
		  $scope.agregarEmails=function(){
				$("#modalCorreo").modal();
			};
	    /**
	     * Funcion para el envio de correos
	     */
	    $scope.envioporEmail=function(){
			var json = JSON.stringify( $scope.listaEnvio, function( key, value ) {
			    if( key === "$$hashKey" ) {
			        return undefined;
			    }
			    return value;
			});
			var listaCorreos = JSON.stringify( $scope.Correos, function( key, value ) {
			    if( key === "$$hashKey" ) {
			        return undefined;
			    }
			    return value;
			});
			var envio={
					objetos:[json,listaCorreos]
			}
			$http.post("comprobante/mandarComprobantePorCorreos.json",envio).success(function(data, status, headers, config) {
				muestraModal(data);
	    		setTimeout(function() {
		 			$('#consulta').click();
		 		}, 1);
			}).error(function(data, status, headers, config) {
				globalMenssages.globalError();
			});
		};
		/**
		 * funcion para eliminar correos de la lista
		 */
		$scope.deleteCorreo=function(correo){
			var index = $scope.Correos.indexOf(correo);		
			$scope.Correos.splice(index, 1);
			$scope.correo=null; 
		};
	/**
	 * Funcion para la cancelacion de cfdis
	 */
		$scope.cancelar=function(){
			globalMenssages.dismiss();
			if($scope.cancelacion != null){
			if($scope.cancelacion.motivoCancelacion=="02"|| $scope.cancelacion.motivoCancelacion=="03"|| $scope.cancelacion.motivoCancelacion=="04"||$scope.cancelacion.motivoCancelacion=="01" && $scope.cancelacion.uuidSustituto!=null){
			$scope.wrapperCancelacion={};
			$scope.wrapperCancelacion.lista=$scope.listaCancelar;
			$scope.wrapperCancelacion.cancelacion=$scope.cancelacion;
			$confirm({text:'Esta usted seguro de mandar a cancelar los comprobantes?',title: 'Confirmaci\u00f3n de cancelaci\u00f3n.',cancel: 'Cancelar', ok: 'Aceptar'})
	        .then(function() {
	        	$http.post("cancelar/cancelarComprobantes.json",$scope.wrapperCancelacion).success(function(data){
	        		muestraModal(data);
	        		setTimeout(function() {
			 			$('#consulta').click();
			 			$scope.cancelacion ={};
			 		}, 1);
	        	}).error(function(data){
	        		globalMenssages.globalError();
	        	});
	        
	        });
			}else{
    			globalMenssages.requeridos();
    		}	
			}else{
    			globalMenssages.requeridos();
    		}	
		}
	
}]);