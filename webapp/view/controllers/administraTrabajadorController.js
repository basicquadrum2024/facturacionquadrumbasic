/**
 * Controlador para el control de trabajador
 */
angular.module('basic').controller('trabajadorController',[
'$scope','$http','Flash','ModalService','validacionesFactory','globalMensajes','globalPaginacion','$confirm','helperFactory','globalDownloadFile','ListaCriteriosBusqueda',
						function($scope, $http,Flash, ModalService, validacionesFactory, globalMensajes,globalPaginacion,$confirm,helperFactory, globalDownloadFile,ListaCriteriosBusqueda) {
							 $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
							  $scope.buscar={};
							$scope.showFilter=false;
		$scope.filterOptions =globalPaginacion.filterOptions();
	    $scope.totalServerItems = 0;
	    $scope.pagingOptions =globalPaginacion.pagingOptions();
	    var paginador=null;
	    var campo='login';
	    var joinn=[];
	    $scope.valor='';
	    
	    $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
	    
	  //para calculo de total de paginas
		$scope.Math = window.Math;
		//texto holder de input de busqueda
		$scope.placeholderText="Todos los criterios";
		$scope.displaySearch='none';
	var listaColumns = [
	        {field : 'fecha',displayName : 'Nombre',width : '15%', cellFilter:'date:"medium"'},
			{field : 'accion',displayName : 'Apellido',width : '15%'},
			
			{field : 'descripcion',displayName : 'RFC',width : '15%'},
			{field : 'host',displayName : 'CURP',width : '15%'},
			{field : 'usuario.rfcEmisor',displayName : 'Num. Seg. Social',width : '15%'},
			{field : 'usuario.login',displayName : 'Correo',width : '15%'},
			{field : 'usuario.login',displayName : 'Eliminar',width : '15%'}
			
		
			
			
			];
	/**
	 * Componente de la paginacion de la tabla
	 */
	 $scope.gridOptions = globalPaginacion.gridOptions(listaColumns,$scope);
	    $scope.gridOptions.canSelectRows=false;
	    	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		          if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
		        	$scope.getPagedDataAsync=globalPaginacion.getPagedDataAsync($scope,$scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText,paginador,campo,joinn);
		          }
		      }, true);
	    	
	    	$scope.$watch('miinput', function () {
				 var data;
				 var ft = $scope.miinput;
		          if (ft) {
		        	  data = $scope.dataGrid.filter(function(item) {
		                    return JSON.stringify(item).indexOf(ft) != -1;
		                });
		        	  $scope.dataGrid = data;
		          }else{
		           	  setTimeout(function(){
					    	$( "#boton" ).click();
					    }, 1);
		          }
		      }, true);	
	/**
	 * Componente para la actualizacion el componente
	 */
	 $scope.$watch('miinput', function () {
		 var data;
		 var ft = $scope.miinput;
          if (ft) {
        	  data = $scope.dataGrid.filter(function(item) {
        			console.warn(ft);
                    return JSON.stringify(item).toString().indexOf(ft) != -1;
                });
        	  $scope.dataGrid = data;
          }else{
        
          }
      }, true);
	 
	 /**
	  * Funcion para la busqueda de fecha
	  */
	 //funcion de busqueda por fecha
 $scope.buscarPorFecha= function() {
		 
		 paginador = {
				
				 'klase' : 'Trabajador',
	     	 	 'disyuncion' : null,
		 		 'conjuncion' : null ,
				 'between' :null,
				 'order' : null,
				 'joinn' : null,
				 'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
				 'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'};
		 $scope.pagingOptions.currentPage = 1;
			$scope.getPagedDataAsync = globalPaginacion
					.getPagedDataAsync($scope,
							$scope.pagingOptions.pageSize,
							$scope.pagingOptions.currentPage,
							null, paginador, 'descripcion',
							null);
			joinn = null;
			globalPaginacion.watchfilterOptions($scope,
					paginador, 'descripcion', joinn);
			
			
	 
	 
	 };
	 
	
		
	           
}]);