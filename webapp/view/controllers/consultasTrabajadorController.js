/**
 * Controlador para gestionar la consulta de trabajadores
 */
angular.module('basic').controller('consultaTrabajadores',[
'$scope','$http','Flash','ModalService','validacionesFactory','globalMensajes','globalPaginacion','$confirm','helperFactory','globalDownloadFile','ListaCriteriosBusqueda',
						function($scope, $http,Flash, ModalService, validacionesFactory, globalMensajes,globalPaginacion,$confirm,helperFactory, globalDownloadFile,ListaCriteriosBusqueda) {
							 $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
							  $scope.buscar={};
							$scope.showFilter=false;
		$scope.filterOptions =globalPaginacion.filterOptions();
	    $scope.totalServerItems = 0;
	    $scope.pagingOptions =globalPaginacion.pagingOptions();
	    var paginador=null;
	    var campo='login';
	    var joinn=[];
	    $scope.valor='';
	    
	    $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
	    
	  //para calculo de total de paginas
		$scope.Math = window.Math;
		//texto holder de input de busqueda
		$scope.placeholderText="Todos los criterios";
		$scope.displaySearch='none';
	var listaColumns = [
	        {field : '',displayName : 'ID RECIBO',width : '10%', cellFilter:'date:"medium"'},
			{field : '',displayName : 'UUID',width : '15%'},
			
			{field : '',displayName : 'FECHA INICIAL PAGO',width : '10%'},
			{field : '',displayName : 'FECHA FINAL PAGO',width : '10%'},
			{field : '',displayName : 'CURP',width : '10%'},
			{field : '',displayName : 'RFC ',width : '15%'},
			{field : '',displayName : 'NUMERO DE SEGURO SOCIAL',width : '15%'},
			{field : '',displayName : 'EMPLEADO',width : '15%'}
			
		
			
			
			];
	
	 $scope.gridOptions = globalPaginacion.gridOptions(listaColumns,$scope);
	    $scope.gridOptions.canSelectRows=false;
	    	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		          if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
		        	$scope.getPagedDataAsync=globalPaginacion.getPagedDataAsync($scope,$scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText,paginador,campo,joinn);
		          }
		      }, true);
	    	
	    	$scope.$watch('miinput', function () {
				 var data;
				 var ft = $scope.miinput;
		          if (ft) {
		        	  data = $scope.dataGrid.filter(function(item) {
		                    return JSON.stringify(item).indexOf(ft) != -1;
		                });
		        	  $scope.dataGrid = data;
		          }else{
		           	  setTimeout(function(){
					    	$( "#boton" ).click();
					    }, 1);
		          }
		      }, true);	
	
	 $scope.$watch('miinput', function () {
		 var data;
		 var ft = $scope.miinput;
          if (ft) {
        	  data = $scope.dataGrid.filter(function(item) {
        			console.warn(ft);
                    return JSON.stringify(item).toString().indexOf(ft) != -1;
                });
        	  $scope.dataGrid = data;
          }else{
        
          }
      }, true);
	 
	 
	 //funcion de busqueda por fecha
 $scope.buscarPorFecha= function() {
		 
		 paginador = {
				
				 'klase' : 'Historial',
	     	 	 'disyuncion' : null,
		 		 'conjuncion' : null ,
				 'between' :[['fecha',$scope.buscar.fechaHistorial+":00"],['fechacreacion',$scope.buscar.historialFinal+":59"]],
				 'order' : null,
				 'joinn' : ['usuario'],
				 'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
				 'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'};
		 $scope.pagingOptions.currentPage = 1;
			$scope.getPagedDataAsync = globalPaginacion
					.getPagedDataAsync($scope,
							$scope.pagingOptions.pageSize,
							$scope.pagingOptions.currentPage,
							null, paginador, 'descripcion',
							null);
			joinn = null;
			globalPaginacion.watchfilterOptions($scope,
					paginador, 'descripcion', joinn);
			
			
	 
	 
	 };
	 
	
		//funcion de busqueda por UUID
		 $scope.buscarPorUuid= function() {
				 
				 paginador = {
						
						 'klase' : 'Historial',
			     	 	 'disyuncion' : null,
				 		 'conjuncion' : ['usuario.login|'+$scope.buscar.emailHistorial] ,
						 'between' :null,
						 'order' : null,
						 'joinn' : ['usuario'],
						 'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
						 'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'};
				 $scope.pagingOptions.currentPage = 1;
					$scope.getPagedDataAsync = globalPaginacion
							.getPagedDataAsync($scope,
									$scope.pagingOptions.pageSize,
									$scope.pagingOptions.currentPage,
									null, paginador, 'descripcion',
									null);
					joinn = null;
					globalPaginacion.watchfilterOptions($scope,
							paginador, 'descripcion', joinn);
			 
			 };
	           
}]);