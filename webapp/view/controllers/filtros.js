/**
 * Filtro para mostrar en estatus
 */
angular.module('basic').filter('valorEstatus', function() {
	return function(code) {
		var respuesta="Activo";
	    if (code==0){
	      respuesta= "Cancelado";
	    }
	    return respuesta;
	  };
})