/**
 * Controlador para gestionar el modulo de comprobantes con complemento de pagos 
 */
var app = angular.module('basic');

app.controller('pagosController', function($scope, $http , Flash, $confirm, validacionesFactory,
		globalMenssages, catalogosServices, fabricaFormularios, globalPaginacion, utileriasFactory, calculosFactory, globalMensajes) {

//	$scope.buscarFolio = function(){
//		
//		$http.post("contribuyenteController/datosEmisor.json").success(function (data) {
//            $scope.foliosAdquiridos = data.foliosAdquiridos;
//            if( $scope.foliosAdquiridos <= 10){
//	            $('#modalFolios').modal({
//	            	backdrop : true
//	            });
//            }
//        }).error(function(data, status, headers, config) {
//       	 	globalMenssages.error("Error al consultar información de folios!.");
//        });
//	} 
	//Scope's necesarios
	$scope.title = "Factura De Recepci\u00f3n De Pagos";
	$scope.traslados =[];
	$scope.retenciones = [];
	
	$scope.resultadoBusqueda=[];
	$scope.listaTipoFactores = [{clave:'0.000000', valor:'0.000000'},{clave:'0.160000', valor:'0.160000'},{clave:'0.080000', valor:'0.080000'}];
	
	// ...ng-grid
	$scope.filterOptions = {
	        filterText: "",
	        useExternalFilter: true
	};  
	$scope.totalServerItems = 0;
	$scope.pagingOptions = {
	        pageSizes: [25, 50, 100],
	        pageSize: 25,
	        currentPage: 1
	};	
	$scope.setPagingData = function(data, page, pageSize,totalResults){
		
	        if(data!= undefined && !utileriasFactory.validarObjetoVacio(data)){
	        	globalMenssages.existenCoindicencias();
	        }else{
	        	globalMenssages.noExistenCoindicencias();
	        }
		    $scope.myData = data;
	        $scope.pagingOptions.totalServerItems =totalResults;	
	        $scope.pagingOptions.pageSize = pageSize;	
	        
	        if (!$scope.$$phase) {
	            $scope.$apply();
	        }
    };
    
	$scope.close = function(result) {
		reloadTable();
		close(result, 500);
	};
	
	$scope.$watch('miinput',function() {
		var data;
		var ft = $scope.miinput;
		if (ft) {
			data = $scope.myData
					.filter(function(item) {
						console.warn(ft);
						return JSON.stringify(item)
								.toString()
								.indexOf(ft) != -1;
					});
			$scope.myData = data;
				} else {
					reloadTable();
				}
			}, true);
	
	$scope.getPagedDataAsync = function (pageSize, page, searchText) {
		var post=null;
	        setTimeout(function () {
	            var data;
	            if (searchText) {
	            	post=getPost(page,pageSize)
	                var ft = searchText.toLowerCase();
	                $http.post(post).success(function (largeLoad) {
	                	var respuesta=largeLoad.list;
	                    data = respuesta.filter(function(item) {
	                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
	                    });
	                    $scope.setPagingData(data,page,pageSize,largeLoad.totalResults);
	                }).error(function(data, status, headers, config) {
	                });  	                
	            } else {
	            	
	            	post=getPost(page,pageSize)
	                $http.post(post).success(function (largeLoad) {
	                	var respuesta=largeLoad.list;	
	                    $scope.setPagingData(respuesta,page,pageSize,largeLoad.totalResults);	                    
	                }).error(function(data, status, headers, config) {
	                });
	                
	            }
	        }, 100);
	};
			
	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		if(newVal !== oldVal && newVal.pageSize !== oldVal.pageSize){
			reloadTable();
		}
		if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
	          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
		}
	}, true);
	
	$scope.$watch('filterOptions', function (newVal, oldVal) {
	        if (newVal !== oldVal) {
	          $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
	        }
	}, true);
		
	var estatusClaveConfirmacion = '<div>{{COL_FIELD == null ? "N/A" : COL_FIELD}}</div>';
	
	$scope.estatusComprobante = '<div  class="ui-grid-cell-contents" ng-init="myText=row.entity.twCfdi.estatus">'+
									'<div ng-if="myText == 0"><span class="label label-danger">CANCELADO</span></div>'+
									'<div ng-if="myText == 1"><span class="label label-success">ACTIVO</span></div>'+
									'<div ng-if="myText == 2"><span class="label label-success">PAGADO</span></div>'+
								'</div>';
	$scope.estatusRestante = '<div  class="ui-grid-cell-contents" ng-init="saldoRestante=row.entity.twCfdi.restante">'+
								'<div ng-if="saldoRestante <= 0"><span class="label label-success">PAGADO</span></div>'+
								'<div ng-if="saldoRestante > 0"><span class="label label-default">{{saldoRestante}}</div></div>'+
							 '</div>';
    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
		showFooter: true,
		enableRowSelection: false,
  		showSelectionCheckbox: false,
  		displaySelectionCheckbox: false,
  		multiSelect:false,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        columnDefs: [
					{
						 field: 'twCfdi.estatus', 
						 displayName: 'Estatus', 
						 width: '8%',
						 cellTemplate: $scope.estatusComprobante
					},
					{
						 field: '', 
						 displayName: 'Pago', 
						 width: '5%',
						 cellTemplate: '<div align="center"><button href="" ng-click="obtenerVersion(row.entity)" ng-disabled="row.entity.twCfdi.restante <= 0" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="left" title="Agregar Pago"><span class="glyphicon glyphicon-plus-sign"></span></button></div>'
					},
					{
						  field: '', 
						  displayName: 'Consulta', 
						  width: '7%',
						  cellTemplate: '<div align="center"><a href="" ng-click="consultarPago(row.entity)" class="btn btn-info btn-xs red-tooltip" data-toggle="tooltip" data-placement="right" title="Consultar Pagos"><span class="glyphicon glyphicon-search"></span></a></div>' 
					},
			        {
						field : 'tcReceptor.rfc',
						displayName : 'Receptor',
						width : '11%'
					},
					{
						field : 'twCfdi.uuid',
						displayName : 'Folio Fiscal',
						width : '29%'
					},
					{
						field : 'twCfdi.total',
						displayName : 'Total',
						width : '9%'
					},
					{
						field : 'twCfdi.totalPagado',
						displayName : 'Pagado',
						width : '9%'
					},
					{
						field : 'twCfdi.restante',
						displayName : 'Restante',
						width : '9%',
						cellTemplate : $scope.estatusRestante
					},
					{
						field : 'twCfdi.folio',
						displayName : 'Folio',
						width : '6%'
					},
					{
						field : 'twCfdi.serie',
						displayName : 'Serie',
						width : '7%'
					}
					]
    };
	
	function getPost(currentPage,pageSize){
		return 'pagosController/listarComprobantesPagosPadre/'+currentPage+'/'+pageSize+'/'+'.json';
	};
	
	function findAll(){
		$scope.postE=getPost($scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize);	    		
		$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
	};
	
	function reloadTable() {
		findAll();
//		$scope.buscarFolio();
	};
	
	$scope.Math = window.Math;
	$scope.totalPage= function() {
		var totComp = Math.ceil($scope.pagingOptions.totalServerItems/$scope.pagingOptions.pageSize);
        return totComp;
     };
		   
	// ...final de ng-grid
	
     /**
      * Funcion que contruye el complemento pagos
      * @param cfdiPadre Entidad que contiene el cfdi Padre
      */
     $scope.agregarPago=function(trGeneral){
    	window.scrollTo(0, 0);
		$scope.cfdiPadre=trGeneral.twCfdi;
		$scope.cfdiReceptor = trGeneral.tcReceptor;
		$scope.cfdiContribuyente = trGeneral.tcContribuyente;
		$scope.reiniciarFormulario();
		$scope.crearNuevoPago();
		if(utileriasFactory.validarObjetoVacio($scope.listaCfdiCancelados)){
			$scope.obtenerCfdiCancelados($scope.cfdiPadre.id);
		}
		if(utileriasFactory.validarObjetoVacio($scope.listaFormaPago)){
			$scope.buscarFormaPago();
		}
		if(utileriasFactory.validarObjetoVacio($scope.listaMonedas)){
			$scope.buscarMonedas();
		}else{
			$scope.obtenerNumDecimales();
		}
		$scope.buscarTipoCadPago();
//		$scope.obtenerSeries();
		$('#modalPagos').modal(); 
     }
     
     $scope.agregarPagoV20 = function(trGeneral){
    	 window.scrollTo(0, 0);
 		$scope.cfdiPadre=trGeneral.twCfdi;
 		$scope.cfdiReceptor = trGeneral.tcReceptor;
 		$scope.cfdiContribuyente = trGeneral.tcContribuyente;
 		$scope.reiniciarFormulario();
 		$scope.crearNuevoPagoV20();
 		if(utileriasFactory.validarObjetoVacio($scope.listaCfdiCancelados)){
 			$scope.obtenerCfdiCancelados($scope.cfdiPadre.id);
 		}
 		if(utileriasFactory.validarObjetoVacio($scope.listaFormaPago)){
 			$scope.buscarFormaPago();
 		}
 		if(utileriasFactory.validarObjetoVacio($scope.listaMonedas)){
 			$scope.buscarMonedas();
 		}else{
 			$scope.obtenerNumDecimales();
 		}
 		$scope.buscarTipoCadPago();
// 		$scope.obtenerSeries();
 		$('#modalPagos2').modal(); 
 		 buscarlistaImpuestosTrasladados();
 		buscarlistaImpuestosRetenidos();
    	 
     }
     
     function buscarlistaImpuestosRetenidos() {
 		$http.post('catalogosSATController/buscarImpuestosRetenidos.json').success(function(data, status, headers, config) {
 			$scope.listaImpuestosRetenidos = data;
 		}).error(function(data, status, headers, config) {

 		});
 	};
     
     $scope.buscarTasaOCuotaRetenciones = function(impuesto, factor) {
 		$scope.retencion.importe = '';
 		catalogosServices.buscarTasasOCuotas(impuesto, factor, ["R", "TR"]).then(function(response) {
 			$scope.listaTasaOCuotaRetenciones = response;
 		});
 	};
     
     function buscarlistaImpuestosTrasladados() {
 		$http.post('catalogosSATController/buscarImpuestosTrasladados.json').success(function(data, status, headers, config) {
 			$scope.listaImpuestosTrasladados = data.splice(0, 1);
 		}).error(function(data, status, headers, config) {

 		});
 	};
 	
 	$scope.buscarTasaOCuotaTraslados = function(impuesto, factor) {
		$scope.traslado.importe = "";
		if (factor === 'Exento' || factor === 'EXENTO') {
			$scope.traslado.tasaOCuota = "";
		}
		catalogosServices.buscarTasasOCuotas(impuesto, factor, ["T", "TR"]).then(function(response) {
			$scope.listaTasaOCuotaTrasalados = response;
		});
	};
 	
 	/**
	 * Funcion para buscar el catalogo tipo factor
	 */
	$scope.buscarTipoFactor = function(impuesto, tipo) {
		catalogosServices.buscarTipoFactorTipoImpuesto(
			impuesto, [tipo, "TR"]).then(
				function(response) {
					$scope.listatiposFactor = response;
				});
	};
     
     $scope.obtenerVersion = function (trGeneral){
    	 window.scrollTo(0, 0);
 		$scope.cfdiPadre=trGeneral.twCfdi;
    	 $http.post("pagosController/buscarVersion/"+$scope.cfdiPadre.id+"/.json").success(function (data) {
             $scope.version = data;
             if($scope.version=="4.0"){
            	 buscarObjetoImpuesto();
            	 $scope.agregarPagoV20(trGeneral);
             }else{
            	 $scope.agregarPago(trGeneral);
             }
         }).error(function(data, status, headers, config) {
        	 globalMenssages.error("no se encontro la version");
         });
     }
     
     function buscarObjetoImpuesto() {
			$http.post('catalogosSATController/buscarImpustoObjeto.json').success(function(data, status, headers, config) {
				$scope.listaObjetoImpuesto = data;
			}).error(function(data, status, headers, config) {

			});

		};
     
//     /**
//      * Devuelve las series del contribuyente
//      */
//     $scope.obtenerSeries = function(){
//    	 $http.post("series/listaSeries.json").success(function (data) {
//             $scope.listaSeries = data;
//         }).error(function(data, status, headers, config) {
//        	 globalMenssages.error("No se creo correctamente el Pago!.");
//         });
//     }
     
     $scope.cerrarModalPagos= function(){
		 $('#modalPagos').modal('hide');
     }
     $scope.cerrarModalPagos2= function(){
		 $('#modalPagos2').modal('hide');
     }
     
     
     /**
      * Devuelve la Clase Pago del Complemento Pagos
      * @return pago Entidad del Complemento Pagos
      */
     $scope.crearNuevoPago = function (){
    	 $http.post("pagosController/crearComplementoPago.json").success(function (data) {
             $scope.pago = data;
         }).error(function(data, status, headers, config) {
        	 globalMenssages.error("No se creo correctamente el Pago!.");
         });
     }
     
     /**
      * Devuelve la clase de pago version 2.0
      */
     
     $scope.crearNuevoPagoV20 = function (){
	     $("#fechaPago").datetimepicker({                    
          	dateFormat : 'yyyy-MM-dd',                                 
          	timeFormat : 'hh:mm:ss',              
          	});
    
    	 $http.post("pagosController/crearComplementoPagoV20.json").success(function (data) {
             $scope.pago = data;
         }).error(function(data, status, headers, config) {
        	 globalMenssages.error("No se creo correctamente el Pago!.");
         });
     }
     
     $scope.nuevoTrasladoPagos =  function(){
     $('#modalNuevoTrasaldoDR').modal();
    	 
     }
     $scope.nuevaRetencionPagos = function(){
    	 $('#modalRetencionPagos').modal();
     }
     
     $scope.guardarTraslado = function(traslado){
    	 var preTraslado = traslado;
			var index = $scope.traslados.indexOf(preTraslado);
			if (index != -1) {
				$scope.traslados.splice(index, 1);
			}
			$scope.traslados.push(traslado);
			$('#modalNuevoTrasaldoDR').modal('hide');
    	 
     }
     $scope.editarTraslado = function(traslado) {
 		//		catalogosServices.buscarTiposFactor('T').then(function(response){
 		//			$scope.listatiposFactor=response;
 		//		});
 		$scope.traslado = traslado;
 		$('#modalNuevoTrasaldoDR').modal();
 	};

 	/**
 	 * Funcion que elimina de la lista traslado el obj indicado
 	 */
 	$scope.borrarTraslado = function(traslado) {
 		var index = $scope.traslados.indexOf(traslado);
 		$scope.traslados.splice(index, 1);
 	};
 	
 	  $scope.editarRetencion = function(retencion) {
 	 		//		catalogosServices.buscarTiposFactor('T').then(function(response){
 	 		//			$scope.listatiposFactor=response;
 	 		//		});
 	 		$scope.retencion = retencion;
 	 		$('#modalRetencionPagos').modal();
 	 	};

 	 	/**
 	 	 * Funcion que elimina de la lista traslado el obj indicado
 	 	 */
 	 	$scope.borrarRetencion = function(retencion) {
 	 		var index = $scope.retenciones.indexOf(retencion);
 	 		$scope.retenciones.splice(index, 1);
 	 	};

     $scope.cerrarModalTraslados = function(){
    	 $('#modalNuevoTrasaldoDR').modal('hide');
     }
     
     $scope.guardarRetencion = function(retencion){
    	
 			var preRetencion = retencion;
 			var index = $scope.retenciones.indexOf(preRetencion);
 			if (index != -1) {
 				$scope.retenciones.splice(index, 1);
 			}
 			$scope.retenciones.push(retencion);
 			$('#modalRetencionPagos').modal('hide');
 		
    	 
     }
     $scope.cerrarModaRetenciones =  function(){
    	 $('#modalRetencionPagos').modal('hide'); 
     }
     
     
     $scope.uuidCfdiRelacionado = {
    	        uuid: ''
    	      };
     /**
      * Obtiene Cfdi cancelados
      */
     $scope.obtenerCfdiCancelados = function (idPadre){
    	 $http.post("comprobante/buscarPagosCancelados/"+idPadre+"/.json").success(function (data) {
             $scope.listaCfdiCancelados = data;
         }).error(function(data, status, headers, config) {
        	 globalMenssages.error("No se pudieron obtener los Comprobantes cancelados!.");
         });
     }
     
     /**
      * Obtiene la forma de pago
      */
     $scope.listaFormaPago = [];
     $scope.buscarFormaPago = function (){
    	 $http.post('catalogosSATController/buscarFormasPago.json').success(function(data, status, headers, config) {
    		 angular.forEach(data, function(entidadFormaPago,index){
    			 angular.forEach(entidadFormaPago,function(valor,columna){
    				 if(columna == "clave"){
    					 if(valor != "99"){
    						 $scope.listaFormaPago.push(entidadFormaPago);
    					 }
    				 }
    			 });
    		 });
    	 }).error(function(data, status, headers, config) {
    		 globalMenssages.error("Error al obtener lista del catalogo Forma de pago!.");
    	 });
     };
     
     /**
      * Obtiene la lista de monedas
      */
     $scope.listaMonedas = [];
     $scope.buscarMonedas = function(){
    	 $http.post('catalogosSATController/buscarMonedas.json').success(function(data, status, headers, config) {
    		 angular.forEach(data, function(entidadMoneda,index){
    			 angular.forEach(entidadMoneda,function(valor,columna){
    				 if(columna == "clave"){
    					 if(valor != "XXX"){
    						 $scope.listaMonedas.push(entidadMoneda);
    					 }
    					 if(valor == "MXN"){
    						 $scope.entidadMoneda = entidadMoneda; 
    						 $scope.obtenerNumDecimales();
    					 }
    				 }
    			 });
    		 });
    	 }).error(function(data, status, headers, config) {
    		 globalMenssages.error("Error al obtener lista del catalogo Moneda!.");
    	 });
     };
     
     /**
      * Se obtiene el numero de decimales que soporta la moneda seleccionada 
      */
     $scope.obtenerNumDecimales = function(){
    	 $scope.numDecimales = $scope.entidadMoneda.decimales;
    	 $scope.pago.monedaP = $scope.entidadMoneda.clave;
    	 $scope.expresionRegularMonto = "^[0-9]{1,18}\.[0-9]{2}$";

     }
     
     /**
      * Obtiene tipo cadena de pago
      */
     $scope.buscarTipoCadPago = function(){
    	 $scope.listaTipoCadPago = [{clave : "01", descripcion: "SPEI"}];
     };
     
     /**
      * MEtodo que valida la forma de pago
      */
     $scope.validacionesFormaPago = function(){
    	 $scope.reiniciarFormulario();
    	 $scope.obtenerEntidadFormaPago();
     }
     
     /**
      * Valida RfcEmisorCtaOrd
      * @entidadFormaPago Entidad FormaPago
      * @Key index de la entidad
      */
     $scope.obtenerEntidadFormaPago = function(){
    	 angular.forEach($scope.listaFormaPago, function(entidadFormaPago, key) {
    		 angular.forEach(entidadFormaPago, function(value,key){
        		 if (key == "clave") {
        			 if(value == $scope.pago.formaDePagoP){
        				 $scope.cambiarFormulario(entidadFormaPago);
        			 }
        		 }
    		 });

          });
	 };
	
	 /**
	  * Activa/Desactiva formulario segun formaPAgo
	  */
	 $scope.cambiarFormulario = function(entidadFormaPago){
//		 $scope.rfcEmisorCtaOrd = true;
		 angular.forEach(entidadFormaPago,function(valor,columna){
			 if(columna == "rfcEmisorCuentaOrdenante"){
				 if(valor != "No"){
					 $scope.rfcEmisorCtaOrd = true;
				 }
			 }
			 if(columna == "cuentaOrdenante"){
				 if(valor != "No"){
					 $scope.ctaOrdenante = true;//patron
				 }
			 }
			 if(columna == "patronCuentaOrdenante"){
				 if($scope.ctaOrdenante){
					 $scope.patronCtaOrd = valor;
				 }
			 }
			 if(columna == "rfcEmisorCuentaBeneficiario"){
				 if(valor != "No"){
				 $scope.rfcEmisorCtaBen = true;
				 }
			 }
			 if(columna == "cuentaBeneficiario"){
				 if(valor != "No"){
				 $scope.ctaBeneficiario = true;//Patron
				 }
			 }
			 if(columna == "patronCuentaBeneficiaria"){
				 if($scope.ctaBeneficiario){
					 $scope.patronCtaBen = valor;
				 }
			 }
			 if(columna == "tipoCadenaPago"){
				 if(valor != "No"){
					 $scope.tipoCadPago = true; 
				 }
			 }

		 });
	 }
	 
	 /**
	  * Reinicia formulario
	  */
	 $scope.reiniciarFormulario = function(){
		 $scope.rfcEmisorCtaOrd = false;
		 $scope.ctaOrdenante = false;
		 $scope.rfcEmisorCtaBen = false;
		 $scope.ctaBeneficiario = false;
		 $scope.tipoCadPago = false;
	 }
	 
	 $scope.documentoRelacionado = [{}];
	 /**
	  * Termina de llenar el obj Pago y manda a timbrar 
	  */
	 $scope.generaComplementoPagos = function(pago){
		 $scope.cerrarModalPagos();
	     popupLoading.modal('show');
	     $scope.pago.monedaP = $scope.entidadMoneda.clave;
	   //Asiganmos valor por defecto si estos dos casos
		 if($scope.cfdiPadre.monedaDr == null || $scope.cfdiPadre.monedaDr == "XXX"){
			 $scope.cfdiPadre.monedaDr = "MXN"
		 }
		 
		//Checamos tipo moneda cfdi padre
		 $scope.recorreListaMonedas($scope.cfdiPadre.monedaDr);
		 
		 //Asignamos tipoCambioDR por defecto
		 if($scope.cfdiPadre.monedaDr != $scope.pago.monedaP ){
			 if($scope.cfdiPadre.monedaDr == "MXN"){
				 $scope.documentoRelacionado[0].tipoCambioDR = 1;
			 }
		 }else{
			 $scope.documentoRelacionado[0].tipoCambioDR = null;
		 }

		 pago.doctoRelacionado = $scope.documentoRelacionado;
		 if(utileriasFactory.validarObjetoVacio($scope.uuidCfdiRelacionado.uuid)){
			$scope.uuidCfdiRelacionado.uuid = null;
		 }
		 utileriasFactory.eliminarCamposVaciosObjetoJson(pago);
	    $scope.wrapperPagos = {
    		cfdiPadre : $scope.cfdiPadre,
    		receptor : $scope.cfdiReceptor,
			contribuyente : $scope.cfdiContribuyente,
			pago : pago,
			uuidCfdiRelacionado : $scope.uuidCfdiRelacionado.uuid,
			serieComprobante : $scope.serie,
			decimales : $scope.numDecimalesMonedaDr
	    }
		 $http.post("pagosController/generaComprobantePago.json",$scope.wrapperPagos).success(function (data) {
             if(data.timbrado){
            	 $scope.cerrarModalPagos();
                 popupLoading.modal('hide');
             }
             abreModalTimbradoPagos(data);
         }).error(function(data, status, headers, config) {
        	 popupLoading.modal('hide');
        	 globalMenssages.error("No se timbro el Complemento con Recepción de Pagos!.");
         });
	 }
	 
	 $scope.generaComplementoPagosV20 = function(pago){
		
		 $scope.cerrarModalPagos2();
	     popupLoading.modal('show');
	     $scope.pago.monedaP = $scope.entidadMoneda.clave;
		   //Asiganmos valor por defecto si estos dos casos
			 if($scope.cfdiPadre.monedaDr == null || $scope.cfdiPadre.monedaDr == "XXX"){
				 $scope.cfdiPadre.monedaDr = "MXN"
			 }
			 
			//Checamos tipo moneda cfdi padre
			 $scope.recorreListaMonedas($scope.cfdiPadre.monedaDr);
			 
			/* //Asignamos tipoCambioDR por defecto
			 if($scope.cfdiPadre.monedaDr != $scope.pago.monedaP ){
				 if($scope.cfdiPadre.monedaDr == "MXN"){
					 $scope.documentoRelacionado[0].tipoCambioDR = 1;
				 }
			 }else{
				 $scope.documentoRelacionado[0].tipoCambioDR = null;
			 }*/

			 pago.doctoRelacionado = [];
			 if(utileriasFactory.validarObjetoVacio($scope.uuidCfdiRelacionado.uuid)){
				$scope.uuidCfdiRelacionado.uuid = null;
			 }
			 utileriasFactory.eliminarCamposVaciosObjetoJson(pago);
	    	 var fecha = pago.fechaPago;
             var date = new Date(fecha);
             pago.fechaPago = date.toISOString();
		    $scope.wrapperPagos = {
	    		cfdiPadre : $scope.cfdiPadre,
	    		receptor : $scope.cfdiReceptor,
				contribuyente : $scope.cfdiContribuyente,
				pago : pago,
				uuidCfdiRelacionado : $scope.uuidCfdiRelacionado.uuid,
				serieComprobante : $scope.serie,
				decimales : $scope.numDecimalesMonedaDr,
				retenciones:$scope.retenciones,
		        traslados:$scope.traslados,
				objetoImpDR:$scope.objetoImpDR
				//impuestos: $scope.impuestos
		    }
		    $http.post("pagosControllerV20/generaComprobantePago.json",$scope.wrapperPagos).success(function (data) {
	             if(data.timbrado){
	            	 $scope.cerrarModalPagos2();
	            	 $scope.traslados =[];
	            	$scope.retenciones = [];
	            	$scope.traslado={};
	            	$scope.retencion={};
	                
	             }
	             abreModalTimbradoPagos(data);
                 popupLoading.modal('hide');
	         }).error(function(data, status, headers, config) {
	        	 popupLoading.modal('hide');
	        	 globalMenssages.error("No se timbro el Complemento con Recepción de Pagos!.");
	         });
		
	 }
	 
	 /**
      * Recorre la lista de monedas disponibles
      */
     $scope.recorreListaMonedas = function(monedaPadre){
    	 angular.forEach($scope.listaMonedas, function(entidadMoneda,index){
			 angular.forEach(entidadMoneda,function(valor,columna){
				 if(columna == "clave"){
					 if(valor == monedaPadre){
						 $scope.numDecimalesMonedaDr = entidadMoneda.decimales;
					 }
				 }
			 });
		 });
     }
	 
	 /**
	  * Encargado de mostrar los resultados del timbrado
	  */
	 function abreModalTimbradoPagos(data){
		 $scope.title = "CFDI para recepción de pagos";
		 $scope.resultado = data;
		 $("#modalReutilizable").modal();
	 };
	 
	 $scope.reinicioValores = function(data){
		 reloadTable();
	 }
	 
	 /**
	  * Se encarga de asignar valo a trGeneral y dirige a la busqueda
	  */
	 $scope.consultarPago = function (trGeneral){
		 $scope.calendario();
		 $scope.cfdiPadre=trGeneral.twCfdi;
		 $scope.cfdiReceptor = trGeneral.tcReceptor;
		 $scope.cfdiContribuyente = trGeneral.tcContribuyente;
		 $scope.buscarPago = true;
		 $('#menuBuscar').click();
	 };
	 
	 //validación que impide realizar busquedas si no ha seleccionado al padre
	 $scope.toggle = function(){
		 $scope.buscarPago = false;
		 $scope.cfdiPadre = [];
		 $scope.resultadoBusqueda=[];
	 }
	 
	 /**
	  * Encargado de realiar busquedas
	  */
	 $scope.twCfdi = {};
	 $scope.buscarPagos = function(){
		 $scope.listaCancelar=[]
		 if(utileriasFactory.validarObjetoVacio($scope.twCfdi)){
			 $scope.twCfdi.id = 0;
		 }
		 else{
			 utileriasFactory.eliminarCamposVaciosObjetoJson($scope.twCfdi);
		 }
		 $http.post("comprobante/buscarPagoExample/"+$scope.cfdiPadre.id+"/"+$scope.twCfdi.fechaCreacion+"/.json",$scope.twCfdi).success(function (data) {
			 $scope.resultadoBusqueda = data;
			 if(utileriasFactory.validarObjetoVacio($scope.resultadoBusqueda)){
				 globalMenssages.noExistenCoindicencias();
			 }else{
				 globalMenssages.existenCoindicencias();
			 }
		 }).error(function(data, status, headers, config) {
        	 globalMenssages.error("No se ha realizado la búsqueda!.");
         });
	 }
	 
//	ng-grid Busqueda
		$scope.filterOptionsBusqueda = {
		        filterText: "",
		        useExternalFilter: true
		};  
		$scope.totalServerItemsBusqueda = 0;
		$scope.pagingOptionsBusqueda = {
		        pageSizes: [25, 50, 100],
		        pageSize: 25,
		        currentPage: 1
		};	
		$scope.setPagingDataBusqueda = function(data, page, pageSize,totalResults){
			
		        if(data!= undefined && !utileriasFactory.validarObjetoVacio(data)){
		        	globalMenssages.existenCoindicencias();
		        }else{
		        	globalMenssages.noExistenCoindicencias();
		        }
			    $scope.resultadoBusqueda = data;
		        $scope.pagingOptionsBusqueda.totalServerItems =totalResults;	
		        $scope.pagingOptionsBusqueda.pageSize = pageSize;	
		        
		        if (!$scope.$$phase) {
		            $scope.$apply();
		        }
	    };
	    
		$scope.closeBusqueda = function(result) {
			reloadTableBusqueda();
			closeBusqueda(result, 500);
		};
		
		$scope.getPagedDataAsyncBusqueda = function (pageSize, page, searchText) {
			var post=null;
		        setTimeout(function () {
		            var data;
		            if (searchText) {
		            	post=getPostBusqueda(page,pageSize)
		                var ft = searchText.toLowerCase();
		                $http.post(post).success(function (largeLoad) {
		                	var respuesta=largeLoad.list;
		                    data = respuesta.filter(function(item) {
		                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
		                    });
		                    $scope.setPagingDataBusqueda(data,page,pageSize,largeLoad.totalResults);
		                }).error(function(data, status, headers, config) {
		                });  	                
		            } else {
		            	
		            	post=getPostBusqueda(page,pageSize)
		                $http.post(post).success(function (largeLoad) {
		                	var respuesta=largeLoad.list;	
		                    $scope.setPagingDataBusqueda(respuesta,page,pageSize,largeLoad.totalResults);	                    
		                }).error(function(data, status, headers, config) {
		                });
		                
		            }
		        }, 100);
		};
				
		$scope.$watch('pagingOptionsBusqueda', function (newVal, oldVal) {
			if(newVal !== oldVal && newVal.pageSize !== oldVal.pageSize){
				reloadTableBusqueda();
			}
			if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
		          $scope.getPagedDataAsyncBusqueda($scope.pagingOptionsBusqueda.pageSize, $scope.pagingOptionsBusqueda.currentPage, $scope.filterOptionsBusqueda.filterText);
			}
		}, true);
		
		$scope.$watch('filterOptionsBusqueda', function (newVal, oldVal) {
		        if (newVal !== oldVal) {
		          $scope.getPagedDataAsyncBusqueda($scope.pagingOptionsBusqueda.pageSize, $scope.pagingOptionsBusqueda.currentPage, $scope.filterOptionsBusqueda.filterText);
		        }
		}, true);
			
		var estatusClave = '<div>{{COL_FIELD == null ? "N/A" : COL_FIELD}}</div>';
		
		$scope.estatusComprobantePago = '<div align="center" class="ui-grid-cell-contents" ng-init="myText=row.entity.estatus">'+
										'<div ng-if="myText == 0"><span class="label label-danger">CANCELADO</span></div>'+
										'<div ng-if="myText == 1"><span class="label label-success">ACTIVO</span></div>'+
									'</div>';
	    $scope.gridOptionsBusqueda = {
	        data: 'resultadoBusqueda',
	        enablePaging: true,
			showFooter: true,
			enableRowSelection: false,
	  		showSelectionCheckbox: false,
	  		displaySelectionCheckbox: false,
	  		multiSelect:false,
	        totalServerItems: 'totalServerItemsBusqueda',
	        pagingOptions: $scope.pagingOptionsBusqueda,
	        filterOptions: $scope.filterOptionsBusqueda,
	        columnDefs: [
						{
							 field: '', 
							 displayName: 'Xml', 
							 width: '5%',
							 cellTemplate: '<div align="center"><a href="documentos/descarga.json?opcion=xml&id={{row.entity.id}}" class="btn btn-default btn-xs" data-toggle="tooltip" '+
							 'data-placement="left" title="Xml"><img src="resources/img/file_xml.png" alt="xml" width="24" height="24"></a></div>'
						},
						{
							 field: '', 
							 displayName: 'Pdf', 
							 width: '5%',
							 cellTemplate: '<div align="center"><a href="documentos/descarga.json?opcion=pdf&id={{row.entity.id}}" ng-disabled="row.entity.estatus === 0" '+
							 'class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="Pdf" >'+
							 '<img  src="resources/img/file_pdf.png" alt="pdf" width="24" height="24"></a></div>'
						},
						{
							 field: 'estatus', 
							 displayName: 'Estatus', 
							 width: '8%',
							 cellTemplate: $scope.estatusComprobantePago
						},
						{
							 field: '', 
							 displayName: 'Cancelar', 
							 width: '5%',
							 cellTemplate: '<div align="center"><a href="" ng-click="cancelarPago(row.entity.id)" ng-disabled="row.entity.estatus === 0" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left" title="Cancelar Pago"><span class="glyphicon glyphicon-remove-sign"></span></a></div>'
						},
				        
						{
							field : 'uuid',
							displayName : 'Folio Fiscal',
							width : '29%'
						},
						{
							field : 'total',
							displayName : 'Total',
							width : '9%'
						},
						{
							field : 'fechaCreacion',
							displayName : 'Creaci\u00f3n',
							width : '15%'
						},
						{
							field : 'fechaCancelacion',
							displayName : 'Cancelaci\u00f3n',
							width : '15%',
							cellTemplate : estatusClave
						},
						{
							field : 'folio',
							displayName : 'Folio',
							width : '6%'
						},
						{
							field : 'serie',
							displayName : 'Serie',
							width : '7%'
						}
						]
	    };
		
	function getPostBusqueda(currentPage,pageSize){
		return 'comprobante/listarComprobantesPagosPadre/'+currentPage+'/'+pageSize+'/'+'.json';
	};
	
	function findAllBusqueda(){
		$scope.postEBusqueda=getPostBusqueda($scope.pagingOptionsBusqueda.currentPage, $scope.pagingOptionsBusqueda.pageSize);	    		
		$scope.getPagedDataAsyncBusqueda($scope.pagingOptionsBusqueda.pageSize, $scope.pagingOptionsBusqueda.currentPage);
	};
	
	function reloadTableBusqueda() {
		findAllBusqueda();
	};
	
	$scope.MathBusqueda = window.Math;
	$scope.totalPageBusqueda= function() {
		var totComp = MathBusqueda.ceil($scope.pagingOptionsBusqueda.totalServerItems/$scope.pagingOptionsBusqueda.pageSize);
        return totComp;
	};
	// ...final de ng-grid para busqueda
	
	$scope.calendario = function(){
		$.datepicker.setDefaults($.datepicker.regional["es"]);
        $("#fechaInicial").datepicker({
            firstDay : 1,
            dateFormat : 'yy-mm-dd'
        });
	}
	
	$scope.addCancelar=function(object){	    	
    	var index = $scope.listaCancelar.indexOf(object);    		
		if(index == -1){
			$scope.listaCancelar.push(object);	
		}else{
			$scope.listaCancelar.splice(index, 1); 
		}			
    };
    
    $scope.listaCancelar=[];
	$scope.cancelarPago=function(idCfdi){
		$scope.addCancelar(idCfdi);
		$confirm({text:'Esta usted seguro de mandar a cancelar el/los comprobantes?',title: 'Confirmaci\u00f3n de cancelaci\u00f3n.',cancel: 'Cancelar', ok: 'Aceptar'})
        .then(function() {
        	$http.post("cancelar/cancelarComprobantes.json",$scope.listaCancelar).success(function(data){
        		abreModalResultadoCancelar(data[0]);
        		$scope.buscarPagos();
        	}).error(function(data){
        		globalMensajes.getMensajeError(data.mensaje);
        	});
        
        });
	}
	
	/**
	  * Encargado de mostrar los resultados del cancelado
	  */
	 function abreModalResultadoCancelar(data){
		 $scope.title = "Estatus del CFDI";
		 $scope.resultado = data;
		 $("#modalReutilizable").modal();
	 };
	

});

