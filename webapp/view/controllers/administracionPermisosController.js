/**
 * Controlador para la administracion de permisos
 */
angular.module('basic').controller('administracionPermisos',[
'$scope','$http','Flash','ModalService','validacionesFactory','globalMensajes','globalPaginacion','$confirm','helperFactory','globalDownloadFile','ListaCriteriosBusqueda',
						function($scope, $http,Flash, ModalService, validacionesFactory, globalMensajes,globalPaginacion,$confirm,helperFactory, globalDownloadFile,ListaCriteriosBusqueda) {
							 $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
							  $scope.buscar={};
							$scope.showFilter=false;
		$scope.filterOptions =globalPaginacion.filterOptions();
	    $scope.totalServerItems = 0;
	    $scope.pagingOptions =globalPaginacion.pagingOptions();
	    var paginador=null;
	    var campo='login';
	    var joinn=[];
	    $scope.valor='';
	    
	    $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
	    
	  //para calculo de total de paginas
		$scope.Math = window.Math;
		//texto holder de input de busqueda
		$scope.placeholderText="Todos los criterios";
		$scope.displaySearch='none';
	var listaColumns = [
	        {field : '',displayName : 'Usuario',width : '15%', cellFilter:'date:"medium"'},
			{field : '',displayName : 'Departamento',width : '15%'},
			{field : '',displayName : 'Puesto',width : '15%'},
			{field : '',displayName : 'Rfc',width : '15%'},
			{field : '',displayName : 'Nombre',width : '15%'},
			{field : '',displayName : 'Apellidos',width : '15%'},
			{field : '',displayName : 'Sucursal raz\u00F3n',width : '15%'},
			{field : '',displayName : 'Empresa raz\u00F3n',width : '15%'},
			];
	/**
	 * Funcion para paginar la busqueda de la tabla
	 */
	 $scope.gridOptions = globalPaginacion.gridOptions(listaColumns,$scope);
	    $scope.gridOptions.canSelectRows=false;
	    	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		          if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
		        	$scope.getPagedDataAsync=globalPaginacion.getPagedDataAsync($scope,$scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText,paginador,campo,joinn);
		          }
		      }, true);
	    	
	    	$scope.$watch('miinput', function () {
				 var data;
				 var ft = $scope.miinput;
		          if (ft) {
		        	  data = $scope.dataGrid.filter(function(item) {
		                    return JSON.stringify(item).indexOf(ft) != -1;
		                });
		        	  $scope.dataGrid = data;
		          }else{
		           	  setTimeout(function(){
					    	$( "#boton" ).click();
					    }, 1);
		          }
		      }, true);	
	
	 $scope.$watch('miinput', function () {
		 var data;
		 var ft = $scope.miinput;
          if (ft) {
        	  data = $scope.dataGrid.filter(function(item) {
        			console.warn(ft);
                    return JSON.stringify(item).toString().indexOf(ft) != -1;
                });
        	  $scope.dataGrid = data;
          }else{
        $scope.empresasRegistradas();
          }
      }, true);
	 
	 
	 /**
	  * funcion de busqueda Empresas
	  */
 $scope.empresasRegistradas= function() {
		 paginador = {
				
				 'klase' : 'empresas',
	     	 	 'disyuncion' : null,
		 		 'conjuncion' : null ,
				 'between' :null,
				 'order' : null,
				 'joinn' : null,
				 'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
				 'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'};
		 $scope.pagingOptions.currentPage = 1;
			$scope.getPagedDataAsync = globalPaginacion
					.getPagedDataAsync($scope,
							$scope.pagingOptions.pageSize,
							$scope.pagingOptions.currentPage,
							null, paginador, 'descripcion',
							null);
			joinn = null;
			globalPaginacion.watchfilterOptions($scope,
					paginador, 'descripcion', joinn);
	 
	 };
	/**
	 *  funcion para agregar usuario con permisos
	 */
	 $scope.agregarUsuarioPermisos = function() {
			helperFactory.setObjeto('');
			ModalService
					.showModal(
							{
								templateUrl : 'view/roles/addUsuarioPermisos_modal.jsp',
								controller : "modalPermisos",
							}).then(function(modal) {
						modal.element.modal();
						modal.close.then(function(result) {
						});
					});
		};
		/**
		 * Controlador para el modal de permisos
		 */
}]).controller("modalPermisos",['$scope','$http','$element','close','Flash','$filter','$q','helperFactory',
      						function($scope, $http, $element, close, Flash,
    								$filter, $q,helperFactory) {
	
}]);