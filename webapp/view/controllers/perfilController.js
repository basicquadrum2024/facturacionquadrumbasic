/**
 *  Controlador para gestionar el modulo de administrar perfiles 
 */
angular.module('basic').controller('controlPerfil',[
						'$scope',
						'$http',
						'Flash',
						'$confirm',
						'$rootScope',
						'$filter',
						'ModalService',
						'ListaCancelacion',
						'globalPaginacion',
						'StatusesConstant',
						'$element',
						function($scope, $http, Flash, $confirm, $rootScope,
								$filter, ModalService, ListaCancelacion,
								globalPaginacion, StatusesConstant, $element) {

							$scope.filterOptions = globalPaginacion
									.filterOptions();
							$scope.totalServerItems = 0;
							$scope.pagingOptions = globalPaginacion
									.pagingOptions();

							var ROL_USER;
							var paginador = null;
							var campo = 'uuid';
							var joinn = [];
							
							//para calculo de total de paginas
							$scope.Math = window.Math;
							//texto holder de input de busqueda
							$scope.placeholderText="Todos los criterios";
							$scope.displaySearch='inline';

							// columnas a mostrar defaul
							var listaColumns = [
									{field : 'tipo',displayName : 'Tipo',width : '35%'},
									{field : 'rol',displayName : 'Rol',width : '25%',enableCellEdit : true},
									{field : '',displayName : 'Opci\u00f3n',width : '20%',
										cellTemplate : '<a ng-click="editar(row.entity);buscaAciones(row.entity);" id="edit"  data-toggle="tooltip" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Actualizar Perrfil"><span class="glyphicon glyphicon-edit"></span></a>'
												+ '<a ng-click="borrar(row.entity)"  id="delete"  data-toggle="tooltip" class="btn btn-danger btn-xs red-tooltip" data-toggle="tooltip" data-placement="right" title="Eliminar Perrfil"><span class="glyphicon glyphicon-remove"></span></a>'
									} ];

							$scope.gridOptions = globalPaginacion.gridOptions(
									listaColumns, $scope);

							$scope.$watch('pagingOptions',function(newVal, oldVal) {
								if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
									$scope.getPagedDataAsync = globalPaginacion.getPagedDataAsync(
											$scope,$scope.pagingOptions.pageSize,
											$scope.pagingOptions.currentPage,$scope.filterOptions.filterText,
											paginador,campo,joinn);
									}
								}, true);
							
							 $scope.$watch('miinput', function () {
								 var data;
								 var ft = $scope.miinput;
						          if (ft) {
						        	  
						        	
						        	  data = $scope.dataGrid.filter(function(item) {
						        			console.warn(ft);
						                    return JSON.stringify(item).toString().indexOf(ft) != -1;
						                });
						        	  $scope.dataGrid = data;
						          }else{
						        	  $rootScope.iniVista();
						          }
						      }, true);

							 $rootScope.iniVista = function() {
							paginador = {
								'klase' : 'Perfil',
								'disyuncion' : null,
								'conjuncion' : null,
								'between' : null,
								'order' : [ 'tipo' ],
								'joinn' : null,
								'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
								'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'
							};

							$scope.pagingOptions.currentPage = 1;
							$scope.getPagedDataAsync = globalPaginacion
									.getPagedDataAsync($scope,
											$scope.pagingOptions.pageSize,
											$scope.pagingOptions.currentPage,
											null, paginador, 'tipo', null);
							joinn = null;
							globalPaginacion.watchfilterOptions($scope,
									paginador, 'tipo', joinn);
							 }
							
							 $scope.updatePermisos = function(rol){
								 if(rol=='USER'){
									 var messageSuccess = "<strong>Usted solo puede asignar !</strong>" +
								 		"<li> Modificaci\u00f3n de Datos (Usuarios y Contrase\u00f1a).</li>" +
								 		"<li> Facturaci\u00f3n de Tickets de Peaje.</li>" +
								 		"<li> Env\u00edo y Reenv\u00edo de Facturas (CFDI).</li>";
										Flash.create('info', messageSuccess, 'customAlertInfo');
										ROL_USER=rol;
										$scope.acciones.forEach(function(item){
											if(item.id !==2 && item.id!==5 && item.id!==6){
													if( item.select==true){
														eliminarAccion($scope.perfil, item);
														item.select=false;
													}
												}
											}); 
								 }						
							 }

							$scope.nuevoPerfil = function() {
								$http.post("perfil/nuevoPerfil.json", {})
										.success(function(data) {
											$rootScope.perfil = data;
										});
							};

							$scope.showModalCreatePerfil = function() {
								ModalService.showModal({
									templateUrl : 'view/admin/catalogos/perfilUsuarios_modal.jsp',
									controller : "controlPerfil",
									}).then(function(modal) {
										modal.element.modal();
										modal.close.then(function(result) {
											
										});
										});
							};

							/* JSON para consultar Perfil */
							function Actualizar() {
								$http({
									method : 'POST',
									url : 'perfil/findPerfil.json',
									templateUrl : 'view/admin/catalogos/perfilUsuarios.jsp',
									headers : {
												'Content-type' : 'application/json'
											}
										}).success(function(data) {
									$rootScope.perfiles = data;
								}).error(function(data) {

								});
							}

							/* Metodo para borrar Perfil */
							$scope.borrar = function(perfil) {
								$confirm({text : 'Deseas Eliminar Perfil?',
								    title : 'Eliminar Perfil',
								    ok : 'Aceptar',
								    cancel: 'Cancelar'
								}).then(function() {
									$http.post('perfil/deletePerfiles.json',
											$scope.perfil = {
												'idPerfil' : perfil.idPerfil
											}).success(
											function(data, status, headers,
													config) {
												var messageSuccess = "<strong>CORRECTO !</strong> Perfil Eliminado Correctamente.";
												Flash.create('info', messageSuccess, 'customAlertInfo');
												$rootScope.iniVista();
											}).error(
											function(data, status, headers,
													config) {
												var messageError = "<strong>ERROR !</strong> Error al Eliminae el Perfil.";
												Flash.create('info', messageError, 'customAlertDanger');
											});
							    });
							};

							/* Metodo para Editar */
							$scope.editar = function(perfil) {
								$http.post("perfil/consultarPerfiles.json", {
									'idPerfil' : perfil.idPerfil
								}).success(function(data) {
									$rootScope.perfil = data;
								});
								$scope.showModalCreatePerfil();
							};

							function getSelectIndex(id) {
								for ( var i = 0; i < $rootScope.perfiles.length; i++)
									if ($rootScope.perfiles[i].idPerfil == id)
										return i;
								return -1;
							}

							/**
							 * listar acciones
							 */
							$scope.buscaAciones = function(perfil) {
								$http.post('perfil/findAcciones.json',
										$scope.perfil = {
											'idPerfil' : perfil.idPerfil
										}).success(function(data) {
									$scope.acciones = data;
									$rootScope.acciones = $scope.acciones;
								}).error(function(data) {

								});
							};

							Array.prototype.clean = function(deleteValue) {
								for ( var i = 0; i < this.length; i++) {
									if (this[i] == deleteValue) {
										this.splice(i, 1);
										i--;
									}
								}
								return this;
							};

							$scope.realizaEjecucion = function(accion, perfil) {
								var ban=false;
								if($scope.perfil.rol && $scope.perfil.rol=='USER'){
										if(accion.id==2 || accion.id==5 || accion.id==6){
											if (accion.select == true) {
												agregarAccion(perfil, accion);
												ban=true;
											} else {
												eliminarAccion(perfil, accion);
												ban=true;
											}
									}else{
										 var messageSuccess = "<strong>Usted solo puede asignar !</strong>" +
									 		"<li> Modificaci\u00f3n de Datos (Usuarios y Contrase\u00f1a).</li>" +
									 		"<li> Facturaci\u00f3n de Tickets de Peaje.</li>" +
									 		"<li> Env\u00edo y Reenv\u00edo de Facturas (CFDI).</li>";
										Flash.create('info', messageSuccess, 'customAlertInfo');
										accion.select =false;
									}
								}
								if (accion.select == true && ban==false) {
									agregarAccion(perfil, accion);
								} else if(ban==false) {
									eliminarAccion(perfil, accion);
								}

							};

							/**
							 * agrega acciones a perfil
							 */
							function agregarAccion(perfil, accion) {
								$http.post('perfil/agregarAcciones.json', {
									perfil : perfil,
									acciones : accion
								}).success(function(data, status, headers,
										config) {
									
								}).error(function(data, status, headers,
										config) {
									
								});
							}

							/**
							 * elimina accion de perfil
							 */
							function eliminarAccion(perfil, accion) {
								$http.post('perfil/eliminarAcciones.json', {
									perfil : perfil,
									acciones : accion
								}).success(function(data, status, headers,
										config) {
									
								}).error(function(data, status, headers,
										config) {
									
								});
							}

							// GUARDAR PERFIL
							$scope.savePerfil = function() {

								$scope.copiaper = {
									'idPerfil' : $scope.perfil.idPerfil,
									'tipo' : $scope.perfil.tipo,
									'rol' : $scope.perfil.rol,
									'encriptado' : $scope.perfil.encriptado
								};

								$http.post('perfil/guardarPerfil.json',$scope.copiaper)
								.success(function(data, status, headers,config) {
									Flash.create('info',"Se ha creado correctamente",'customAlertInfo');
									location.reload();
									}).error(function(data, status, headers,config) {
										Flash.create('danger',errorSer,'customAlertdanger');
										});
							};

							// CERRAR EL MODAL
							$scope.cancelar = function(result) {
								$element.modal('hide');
								$rootScope.perfil = "";
								$rootScope.id = "";
							};

						} ]);