/**
 * Controlador para gestionar el modulo de generación de comprobantes
 */
angular.module('basic').controller('generacionComprobanteCfdiV33Controller',['$scope','$http','calculosFactory','fabricaFormularios','globalMensajes','utileriasFactory','validacionesFactory','catalogosServices',function($scope,$http,calculosFactory,fabricaFormularios,globalMensajes,utileriasFactory,validacionesFactory,catalogosServices){	
	var comprobanteController=this;
	$scope.receptor={};
	$scope.concepto={};
	$scope.cfdiRelacionado=[];
	$scope.listaRelacionados=[];
    $scope.pasos = ['uno', 'dos', 'tres'];
    $scope.paso = 0;
    
    $scope.listaOpcionDecimales=["2", "3", "4", "5", "6"];
	$scope.objdecimales={
			decimalesOperaciones:$scope.listaOpcionDecimales[0]
	};
  
    $scope.relacionado={};
   
    //validarContinuidadTimbrado();   
        /**
         * funciones para la paginacion 
         */
    $scope.esPrimerPaso = function () {
        return $scope.paso === 0;
    };

    $scope.esUltimoPaso = function () {
        return $scope.paso === ($scope.pasos.length - 1);
    };

    $scope.esPasoActual = function (paso) {
        return $scope.paso === paso;
    };

    $scope.asignarPasoActual = function (paso) {
        $scope.paso = paso;
    };

    $scope.obtenerPasoActual = function () {
        return $scope.pasos[$scope.paso];
    };

   
    $scope.obtenerSiguienteEtiqueta = function () {
        return ($scope.esUltimoPaso()) ? 'Submit' : 'Siguiente';
    };

    $scope.manejarAnterior = function () {
        $scope.paso -= ($scope.esPrimerPaso()) ? 0 : 1;
    };

    $scope.manejarSiguiente = function () {    	
    	if($scope.paso==0 && fabricaFormularios.showRequired(comprobanteController.formularioReceptor)){
    		//validarContinuidadTimbrado();
    		guardarTcReceptor($scope.receptor);
    		$scope.paso += 1;
    	}else if($scope.paso==1 && $scope.comprobante.conceptos.concepto.length>0){
    		$scope.paso += 1;
    	}else{
    		lanzarMensajes($scope.paso);
    	}
    };
    
    validarLugarexpedicion();
    function validarLugarexpedicion(){
    	$http.post('comprobante/validarLugarExpedicion.json').success(function(data, status, headers, config) {    		        	
    		if(data){
    			$('#modalValidarLugarExpedicion').modal();        	    			
    		}
    	}).error(function(data, status, headers, config) {
    		
    	});    	
    }

    /**
     * fin de las funciones de paginacion
     */
    
    /**
     * funcion para abrir modal concepto e inicializa valiables de concepto
     */
    $scope.nuevoConcepto=function(){
    	$('#modalConcepto').modal();    	
    	fabricaFormularios.reincioFormulario(comprobanteController.formConcepto);
    	$scope.nuevaDescripcion=false;
    	buscarConceptosPorContribuyente();
    	crearConceptoCfdiv33();    	
    };
    
    /**
     * Funcion para validar la moneda y desactivar el campo tipo de cambio
     */
    $scope.validaCampoMoneda = function(moneda){
    	if($scope.comprobante.moneda=="MXN"|| $scope.comprobante.moneda=="XXX" ){
    		$scope.bloqueaTipoCambio=true;
    		$scope.comprobante.tipoCambio=null;
    		$scope.comprobante.confirmacion=null;
    		$scope.monedaRequired=false;
    	}else
    		{
    		$scope.bloqueaTipoCambio=false;
    		$scope.monedaRequired=true;
    		
    		}
    };
    
    /**
     * funcion para activar o desactivar el campo cdfi relacionado
     */
    $scope.muestraCfdiRelacionado = function(objeto){
    	
    	if(objeto == "E"){
    		
    		$scope.activaCfdiRelacionado=true;
    	}else{
    		$scope.activaCfdiRelacionado=false;
    		$scope.relacionado={};
    		
    		
    	}
    };
    
    /**
     * Funcion para validar si se agregara una nueva descripcion de concepto
     */
    $scope.cambioNuevaDescripcion=function(descripcion){
    	$scope.nuevaDescripcion=false;    	
    	if(descripcion==='Agregar nueva descripci\u00f3n'){
    		$scope.nuevaDescripcion=true;    		
    	}
    };
    
    $scope.extranjero = false;
    /**
     * Funcion para buscar los datos del Receptor cuando se agrega el RFC
     */
    $scope.buscarReceptorPorRfc=function(rfcReceptor){
    	if(validacionesFactory.validateRfc($scope.receptor.rfc) && $scope.receptor.rfc!=''){
    	$http.post('tcreceptor/buscarReceptorPorRfc.json', {rfc:rfcReceptor}).success(function(data, status, headers, config) {
    		if(data.existeLrfc===true){
    			$scope.receptor=data;
        		if($scope.receptor.rfc != "XEXX010101000" ){
        			$scope.extranjero = false;
        			$scope.receptor.residenciaFiscal = null;
        			$scope.receptor.numRegIdTrib = null;
        		}else{
        			$scope.extranjero = true;
        		}
        		$http.post('catalogosSATController/buscarUsoCfdiPorTipoPersona/'+$scope.receptor.rfc+"/.json").success(function(data, status, headers, config) {
        			$scope.listausoCFDI=data;
        		}).error(function(data, status, headers, config) {
        			
        		});
    		}else{
    			$scope.receptor={};
    			globalMensajes.getMensajeError(data.rfc);
    		}
    	}).error(function(data, status, headers, config) {
    	
    	});
    	}else{    		
    		globalMensajes.getMensajeError("EL RFC no cumple con un patr\u00f3n valido, Ejemplo:<br>Persona F\u00edsica AAAA010101AAA <br> Persona MoralAAA010101AAA");
    	}
    };
    
    /**
     * Funcion para realizar los calculos matematicos 
     */
    $scope.calcular=function(){
    	console.log("decimales");
    	console.log($scope.objdecimales.decimalesOperaciones);
    	if($scope.concepto.cantidad!=null && $scope.concepto.valorUnitario!=null){
		calculosFactory.calcularCantidadPorValorUnitarioImporte($scope.concepto.cantidad,$scope.concepto.valorUnitario, $scope.objdecimales.decimalesOperaciones)
		.success(function(data, status,headers, config) {
			$scope.concepto.importe = data;
															});
		if($scope.concepto.impuestos.traslados.traslado!=null && $scope.concepto.impuestos.traslados.traslado.length>0){
			
			for (var i = 0; i < $scope.concepto.impuestos.traslados.traslado.length; i++) {
				console.log($scope.concepto.impuestos.traslados.traslado[i]);
				console.log($scope.concepto.impuestos.traslados.traslado[i].base);
				$scope.concepto.impuestos.traslados.traslado[i].base=$scope.concepto.importe;
				calculosFactory.calcularCantidadPorValorUnitarioImporte($scope.concepto.impuestos.traslados.traslado[i].base,$scope.concepto.impuestos.traslados.traslado[i].tasaOCuota, $scope.objdecimales.decimalesOperaciones)
				.success(function(data, status,headers, config) {
					$scope.concepto.impuestos.traslados.traslado[i].importe= data;
				});
				
				}
			
		}
		
		if($scope.concepto.impuestos.retenciones.retencion!=null && $scope.concepto.impuestos.retenciones.retencion.length>0){
			for (var i = 0; i < $scope.concepto.impuestos.retenciones.retencion.length; i++) {
				console.log($scope.concepto.impuestos.retenciones.retencion[i]);
				
				$scope.concepto.impuestos.retenciones.retencion[i].base=$scope.concepto.importe;
				calculosFactory.calcularCantidadPorValorUnitarioImporte($scope.concepto.impuestos.retenciones.retencion[i].base,$scope.concepto.impuestos.retenciones.retencion[i].tasaOCuota, $scope.objdecimales.decimalesOperaciones)
				.success(function(data, status,headers, config) {
					$scope.concepto.impuestos.retenciones.retencion[i].importe= data;
				});
				
				}
		}
		
    	}
	};
    
	/**
	 * Funcion para guardar la nueva descripcion de un concepto
	 */
    $scope.guardarConcepto=function(concepto){
    	
		console.log(concepto);
    	if(fabricaFormularios.showRequired(comprobanteController.formConcepto)){ 
    		catalogosServices.validarExistenciaClaveUnidad(concepto.claveUnidad).then(function(data){
    			if(data){
    				catalogosServices.validarExistenciaProvedorServicio(concepto.claveProdServ).then(function(data){
    	        		if(data){    	        			   		
    	        	    		if(!utileriasFactory.validarObjetoVacio(concepto.nuevaDescripcion)){
    	        	    			concepto.descripcion=concepto.nuevaDescripcion;
    	        	    			delete concepto['nuevaDescripcion']; 
    	        	    		}
    	        				var preconcepto=concepto;
    	        				var index = $scope.comprobante.conceptos.concepto.indexOf(preconcepto);
    	        				if(index != -1){
    	        					$scope.comprobante.conceptos.concepto.splice(index, 1);	
    	        				}			
    	        				
    	        				$scope.comprobante.conceptos.concepto.push(concepto);
    	        				recalcular($scope.comprobante);
    	        				guardarConceptoBaseDatos(concepto);
    	        				$('#modalConcepto').modal('hide');			    	        			
    	        		}else{
    	        			lanzarMensajes(3);
    	        		}
    	        	});
    			}else{
    				lanzarMensajes(2);
    			}
    		});
    	}
    };
    
    /**
     * Funcion para abre el modal de concepto para poder se editado
     */
    $scope.editarConcepto=function(concepto){
    	$http.post('conceptos/buscarConceptosPorContribuyente.json').success(function(data, status, headers, config) {    		        	
    		$scope.listaDescripciones=data;
    		$scope.listaDescripciones.push({id:null,tcContribuyente:null,descripcion:"Agregar nueva descripci\u00f3n"});
    		$scope.nuevaDescripcion=false;
        	$scope.concepto=concepto;    	
        	$('#modalConcepto').modal();        	
    	}).error(function(data, status, headers, config) {
    		
    	});    	
	};
	
	/**
	 * Funcion para eliminar el concepto de la lista 
	 */
	$scope.borrarConcepto=function(concepto){
		var index = $scope.comprobante.conceptos.concepto.indexOf(concepto);
		$scope.comprobante.conceptos.concepto.splice(index, 1);
		recalcular($scope.comprobante);
	};
	
	/**
	 * Funcion para abrir modal para agregar un impuesto traslado
	 */
	$scope.nuevoTraslado=function(){
//		catalogosServices.buscarTiposFactor('T').then(function(response){
//			$scope.listatiposFactor=response;
//		});
		$scope.listaTasaOCuotaTrasalados=[];
		crearnuevoTraslado();
		$('#modalNuevoTrasaldo').modal();
	};
	
	/**
	 * Funcion para agregar a la lista de traslados el obj traslado y ocultar el modal
	 */
	$scope.guardarTraslado=function(traslado){
		if(fabricaFormularios.showRequired(comprobanteController.formTraslado)){
			var pretraslado=traslado;
			var index = $scope.concepto.impuestos.traslados.traslado.indexOf(pretraslado);
			if(index != -1){
				$scope.concepto.impuestos.traslados.traslado.splice(index, 1);	
			}			
			$scope.concepto.impuestos.traslados.traslado.push(traslado);
			$('#modalNuevoTrasaldo').modal('hide');			
		} 
	};
	
	/**
	 * Funcion que muestra el modal de traslado para ser editado
	 */
	$scope.editarTraslado=function(traslado){
//		catalogosServices.buscarTiposFactor('T').then(function(response){
//			$scope.listatiposFactor=response;
//		});
		$scope.traslado=traslado;
		$('#modalNuevoTrasaldo').modal();		
	};
    
	/**
	 * Funcion que elimina de la lista traslado el obj indicado
	 */
	$scope.borrarTraslado=function(traslado){
		var index = $scope.concepto.impuestos.traslados.traslado.indexOf(traslado);
		$scope.concepto.impuestos.traslados.traslado.splice(index, 1);
	};
	
	/**
	 * Funcion para mostrar modal de impuestos retenidos
	 */
	$scope.nuevaRetencion=function(){
//		catalogosServices.buscarTiposFactor('R').then(function(response){
//			$scope.listatiposFactor=response;
//		});
		crearnuevaRetencion();
		$scope.listaTasaOCuotaRetenciones=[];
		$('#modalNuevaRetencion').modal();
	};
	
	/**
	 * Funcion para agregar a la lista de retenciones
	 */
	$scope.guardarRetencion=function(retencion){
		if(fabricaFormularios.showRequired(comprobanteController.formRetencion)){
			var preRetencion=retencion;
			var index = $scope.concepto.impuestos.retenciones.retencion.indexOf(preRetencion);
			if(index != -1){
				$scope.concepto.impuestos.retenciones.retencion.splice(index, 1);	
			}			
			$scope.concepto.impuestos.retenciones.retencion.push(retencion);
			$('#modalNuevaRetencion').modal('hide');			
		} 
	};
	
	/**
	 * Funcion para editar un obj retenciones
	 */
	$scope.editarRetencion=function(retencion){
//		catalogosServices.buscarTiposFactor('R').then(function(response){
//			$scope.listatiposFactor=response;
//		});
		$scope.retencion=retencion;
		$('#modalNuevaRetencion').modal();
	};
    
	/**
	 * Funcion para eliminar de la lista un obj retenciones
	 */
	$scope.borrarRetencion=function(retencion){
		var index = $scope.concepto.impuestos.retenciones.retencion.indexOf(retencion);
		$scope.concepto.impuestos.retenciones.retencion.splice(index, 1);
	};
	
	/**
	 * Funcion para mostrar el modal de informacion aduanera
	 */
	$scope.nuevaInformacionAduanera=function(){
		$scope.informacionAduanera={};
		$('#modalinformacionAduanera').modal();
	};
	
	/**
	 * funcion para editar un obj de informacion aduanera
	 */
	$scope.editarinformacionAduanera=function(informacionAduanera){
		$scope.informacionAduanera=informacionAduanera;
		$('#modalinformacionAduanera').modal();
	};
	
	/**
	 * funcion para borrar un obj de la lista informacion aduanera
	 */
	$scope.borrarinformacionAduanera=function(informacionAduanera){
		var index = $scope.concepto.informacionAduanera.indexOf(informacionAduanera);
		$scope.concepto.informacionAduanera.splice(index, 1);
	};
	
	/**
	 * Funcion para agregar a la lista de informacion aduanera
	 */
	$scope.guardarInformacionAduanera=function(informacionAduanera){
		if(fabricaFormularios.showRequired(comprobanteController.formInformacionAduanera)){
			var preinformacionAduanera=informacionAduanera;
			var index = $scope.concepto.informacionAduanera.indexOf(preinformacionAduanera);
			if(index != -1){
				$scope.concepto.informacionAduanera.splice(index, 1);	
			}			
			$scope.concepto.informacionAduanera.push(informacionAduanera);
			$('#modalinformacionAduanera').modal('hide');			
		} 
	};
	
	
	/**
	 * Funcion para mostrar modal de parte
	 */
	$scope.nuevaParte=function(){
		crearnuevaParte();
		$('#modalNuevaParte').modal();
	};
	
	/**
	 * Funcion para borrar parte de concepto
	 */
	$scope.borrarParte=function(parte){
		var index = $scope.concepto.parte.indexOf(parte);
		$scope.concepto.parte.splice(index, 1);
	};
	/**
	 * funcion para crear obj parte
	 */
	function crearnuevaParte(){
		$http.post('conceptos/crearParte.json').success(function(data, status, headers, config) {
			$scope.parte=data;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	/**
	 * Funcion que muestra el modal para agregar informacion aduanera a obj parte
	 */
	$scope.nuevaParteInformacionAduanera=function(){
		crearNuevaParteInformacionAduanera();
		$('#modalparteinformacionAduanera').modal();
	};
	
	/**
	 * Funcion  para agregar informacion aduanera a obj parte
	 */
	function crearNuevaParteInformacionAduanera(){
		$http.post('conceptos/crearParteInformacionAduanera.json').success(function(data, status, headers, config) {
			$scope.informacionAduanera=data;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	/**
	 * Funcion para editar informacion aduanera a obj parte
	 */
	$scope.editarparteinformacionAduanera=function(informacionAduanera){
		$scope.informacionAduanera=informacionAduanera;
		$('#modalparteinformacionAduanera').modal();
	};
	
	/**
	 * Funcion para borrar informacion aduanera a obj parte
	 */
	$scope.borrarparteinformacionAduanera=function(informacionAduanera){
		var index = $scope.parte.informacionAduanera.indexOf(informacionAduanera);
		$scope.parte.informacionAduanera.splice(index, 1);
	}
	
	/**
	 * Funcion para cerrar modal parteinformacionAduanera
	 */
	$scope.cerrarModalParteInformacionAduanera=function(){
		$('#modalparteinformacionAduanera').modal('hide');
	};
	
	/**
	 * Funcion para cerrar modal nuevaparte
	 */
	$scope.cerrarModalParte=function(){
		$('#modalNuevaParte').modal('hide');
	};
	
	/**
	 * Funcion que abre modal para agregar parte
	 */
	$scope.editarParte=function(parte){
		$scope.parte=parte;
		$('#modalNuevaParte').modal();
	}
	/**
	 * Funcion para calcular el importe del comprobante
	 */
	$scope.calcularParteImporte= function(){
		$scope.parte.importe=calculosFactory.calcularImporteParte($scope.parte.cantidad,$scope.parte.valorUnitario);
	};
	
	/**
	 * Funcion para agregar a lista de parte.informacionAduanera
	 */
	$scope.guardarParteInformacionAduanera=function(informacionAduanera){
		if(fabricaFormularios.showRequired(comprobanteController.formparteInformacionAduanera)){
			var preParteInfo=informacionAduanera;
			var index = $scope.parte.informacionAduanera.indexOf(preParteInfo);
			if(index != -1){
				$scope.parte.informacionAduanera.splice(index, 1);	
			}			
			$scope.parte.informacionAduanera.push(informacionAduanera);
			$('#modalparteinformacionAduanera').modal('hide');			
		} 
	};
	/**
	 * funcion para agregar parte a obj concepto
	 */
	$scope.guardarParte=function(parte){
		if(fabricaFormularios.showRequired(comprobanteController.formParte)){
			var preParte=parte;
			var index = $scope.concepto.parte.indexOf(preParte);
			if(index != -1){
				$scope.concepto.parte.splice(index, 1);	
			}			
			$scope.concepto.parte.push(parte);
			$('#modalNuevaParte').modal('hide');			
		} 
	};
			/**
			 * funcion para generar comprobante
			 */
	$scope.generarComprobante=function(comprobante){
		console.log(comprobante);
		if(comprobante.total>$scope.valorMaximo && $scope.comprobante.confirmacion==null){
			$("#modalSuperaLimite").modal();
		}else{	
			
			if(!angular.equals({}, $scope.relacionado)){
				$scope.listaRelacionados=[{uuid:$scope.relacionado.uuid}];
				comprobante.cfdiRelacionados={tipoRelacion:$scope.relacionado.tipoRelacion,cfdiRelacionado:$scope.listaRelacionados};
			}else{
				comprobante.cfdiRelacionados=null;
			}

			
		comprobante.receptorCfdiv33Wrapper=$scope.receptor;
		
		console.log(comprobante);
		//comprobante.conceptos.concepto=$scope.comprobante.conceptos.concepto;
		utileriasFactory.eliminarCamposVaciosObjetoJson(comprobante);
		console.log("despues de quitar campos vacios");
		console.log(comprobante);
		$http.post('comprobante/generarComprobante.json', comprobante).success(function(data, status, headers, config) {
			$scope.resultadoWrappers=data;
			abreModalResultados(data);
			//$("#modalResultadoTimbradoCfdi1").modal();
			if(data.timbrado){
				reinicioValores();
			}
		}).error(function(data, status, headers, config) {
			
		});
		}
	};
	
	/**
	 * funcion para calcular el importe de los impuestos traslados
	 */
	$scope.calcularImporteTraslado=function(){
		calculosFactory.calcularCantidadPorValorUnitarioImporte($scope.traslado.base,$scope.traslado.tasaOCuota, $scope.objdecimales.decimalesOperaciones)
		.success(function(data, status,headers, config) {
			$scope.traslado.importe= data;
				});
		
		
	};
	
	/**
	 * funcion para calcular el importe de los impuestos retenidos
	 */
	$scope.calcularImporteRetenido=function(){
		
			calculosFactory.calcularCantidadPorValorUnitarioImporte($scope.retencion.base,$scope.retencion.tasaOCuota, $scope.objdecimales.decimalesOperaciones)
			.success(function(data, status,headers, config) {
				$scope.retencion.importe= data;
					});
	};
	
	/**
	 * funcion para cerrar modal de agregar concepto
	 */
	$scope.cerraModalAgregarConcetos=function(){
		$('#modalConcepto').modal('hide');
	};
	
	/**
	 * Funcion para cerrar modal NuevoTrasaldo
	 */
	$scope.cerrarModalTraslados=function(){
		$('#modalNuevoTrasaldo').modal('hide');
	};
	
	/**
	 * Funcion para buscar el catalogo tipo factor
	 */
	$scope.buscarTipoFactor = function(impuesto, tipo) {
		catalogosServices.buscarTipoFactorTipoImpuesto(
				impuesto, [ tipo, "TR" ]).then(
				function(response) {
					$scope.listatiposFactor = response;
				});
	};
	
	/**
	 * funcion para cerrar modal NuevaRetencion
	 */
	$scope.cerrarModaRetenciones=function(){
		$('#modalNuevaRetencion').modal('hide');
	};
	
	/**
	 * funcion para cerrar modal informacionAduanera
	 */
	$scope.cerrarModalinformacionAduanera=function(){
		$('#modalinformacionAduanera').modal('hide');
	};
	
	/**
	 * Funcion para cambiar formato a float
	 */
	$scope.cambioFormatoDecimal=function(numero){		
		return parseFloat(Math.round(numero * 100) / 100).toFixed(2);
	};
	
	/**
	 * Funcion para buscar la clave de unidad
	 */
	$scope.buscarClavesUnidadLikeNombre=function(nombre){
		catalogosServices.servicioBuscarClavesUnidadLikeNombre(nombre).then(function(response) {			
			$scope.listaClavesUnidad = response;
		});
	};
	
	/**
	 * Funcion para mostrar la tasa o cuota correspondiente al traslado
	 */
	$scope.buscarTasaOCuotaTraslados=function(impuesto,factor){
		$scope.traslado.importe="";
		if(factor==='Exento' || factor==='EXENTO'){
			$scope.traslado.tasaOCuota="";
		}
		catalogosServices.buscarTasasOCuotas(impuesto,factor,["T","TR"]).then(function(response){
			$scope.listaTasaOCuotaTrasalados = response;
		});
	};
	
	/**
	 * Funcion para mostrar la tasa o cuota correspondiente a retenciones
	 */
	$scope.buscarTasaOCuotaRetenciones=function(impuesto,factor){
		$scope.retencion.importe='';
		catalogosServices.buscarTasasOCuotas(impuesto,factor,["R","TR"]).then(function(response){
			$scope.listaTasaOCuotaRetenciones = response;
		});
	};
	
	/**
	 * funcion para buscar claves de producto o servicio
	 */
	$scope.buscarClaveProdServ=function(descripcion){
		catalogosServices.servicioBuscarClaveProdServLikeNombre(descripcion).then(function(response) {			
			$scope.listaClavesProdServ = response;
		});
	};
	
	/**
	 * Funcion para crear obj relacionado
	 */
    $scope.llenaLsitaa=function(valor){
		 if(valor==false){			
			 $scope.relacionado={};			 		
		 }
    };
    
    /**
     * Funcion para validar el numero de conformacion insertado
     */
    $scope.validarNumeroConfirmacion=function(claveConfirmacion){
    	if(claveConfirmacion===''){
    		delete comprobanteController.formInformacionComprobante.confirmacion.$error.utilizado;
			comprobanteController.formInformacionComprobante.confirmacion.$invalid=false;
			comprobanteController.formInformacionComprobante.$invalid=false;
    	}
    	catalogosServices.validarClaveConfirmacion(claveConfirmacion).then(function(data){
    		if(data){
    			delete comprobanteController.formInformacionComprobante.confirmacion.$error.utilizado;
    			comprobanteController.formInformacionComprobante.confirmacion.$invalid=false;
    			comprobanteController.formInformacionComprobante.$invalid=false;
    		}else{
    			comprobanteController.formInformacionComprobante.$invalid=true;
    			comprobanteController.formInformacionComprobante.confirmacion.$error.utilizado=true;
            	comprobanteController.formInformacionComprobante.confirmacion.$invalid=true;
    		}
    		
    	});    	
    };
	
    /**
     * Funcion para crear un obj de tipo comprobante v3.3
     */
	function crearComprobanteCfdiv33(){
		$http.post('comprobante/crearComprobanteCfdiv33.json').success(function(data, status, headers, config) {
			$scope.comprobante=data;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	crearComprobanteCfdiv33();
	
	/**
	 * Funcion para almacenar en la base de datos un receptor
	 */
    function guardarTcReceptor(receptorWrapper){
    	$http.post("tcreceptor/guardarTcReceptor.json", receptorWrapper).success(function(data, status, headers, config) {
    		$scope.receptor=data;
    	}).error(function(data, status, headers, config) {
    		
    	});
    };
    cargarRecidencias();
    /**
     * Funcion para obtern catalogo de pais
     */
    function cargarRecidencias(){
    	$http.post('catalogosSATController/buscarTodosPaises.json').success(function(data, status, headers, config) {
			$scope.listaresidenciaFiscal=data;
		}).error(function(data, status, headers, config) {
			
		});
    };
    
    /**
     * funcion para hacer los calculos matematicos 
     */
    function recalcular(comprobante){
    	catalogosServices.servciocalculosComprobante(comprobante).then(function(response){
    		$scope.comprobante=response;
    	});
	};
	
	/**
	 * Funcion para guardar un concepto a la base de datos
	 */
	function guardarConceptoBaseDatos(concepto){
    	$http.post('conceptos/guardarConcepto.json', concepto).success(function(data, status, headers, config) {
		
    	}).error(function(data, status, headers, config) {
		
    	});
	};
	
	/**
	 * Funcion para crear un obj de tipo concepto
	 */
	function crearConceptoCfdiv33(){
		$http.post('conceptos/crearConceptoCfdiv33.json').success(function(data, status, headers, config) {
    		$scope.concepto=data;
    	}).error(function(data, status, headers, config) {
    		
    	});
	};
	
	/**
	 * Funcion para crear un obj de tipo traslado
	 */
	function crearnuevoTraslado(){
		$http.post('conceptos/crearTraslado.json').success(function(data, status, headers, config) {
			$scope.traslado=data;
			$scope.traslado.base=$scope.concepto.importe;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	/**
	 * Funcion para crear un obj de tipo retencion
	 */
	function crearnuevaRetencion(){
		$http.post('conceptos/crearRetencion.json').success(function(data, status, headers, config) {
			$scope.retencion=data;
			$scope.retencion.base=$scope.concepto.importe;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	buscarlistaImpuestosTrasladados();
	/**
	 * Funcion para obtener los impuestos correspondientes a traslado
	 */
	function buscarlistaImpuestosTrasladados(){
		$http.post('catalogosSATController/buscarImpuestosTrasladados.json').success(function(data, status, headers, config) {
			$scope.listaImpuestosTrasladados=data;
		}).error(function(data, status, headers, config) {
			
		});
	};
		
	
	buscarlistaImpuestosRetenidos();
	/**
	 * Funcion para obtener los impuestos correspondientes a retenciones
	 */
	function buscarlistaImpuestosRetenidos(){
		$http.post('catalogosSATController/buscarImpuestosRetenidos.json').success(function(data, status, headers, config) {
			$scope.listaImpuestosRetenidos=data;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	buscarFormaPago();
	/**
	 * Funcion para obtener catalogo de forma de pago
	 */
	function buscarFormaPago(){
		$http.post('catalogosSATController/buscarFormasPago.json').success(function(data, status, headers, config) {
			$scope.listaFormasPago=data;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	buscarMonedas();
	/**
	 * Funcion para obtener catalogo moneda
	 */
	function buscarMonedas(){
		$http.post('catalogosSATController/buscarMonedas.json').success(function(data, status, headers, config) {
			$scope.listaMonedas=data;
			$scope.comprobante.moneda="MXN";
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	buscarTipoComprobante();
	/**
	 * funcion para obtener catalogo tipo de comprobante
	 */
	function buscarTipoComprobante(){
		$http.post('catalogosSATController/buscarTiposComprobante.json').success(function(data, status, headers, config) {
			$scope.listaTiposComprobante=data;
			$scope.valorMaximo = $scope.listaTiposComprobante[0].valorMaximo;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	buscarMetodosPago();
	/**
	 * funcion para obtener catalogo metodo de pago
	 */
	function buscarMetodosPago(){
		$http.post('catalogosSATController/buscarMetodosPago.json').success(function(data, status, headers, config) {
			$scope.listaMetodosPago=data;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	buscarEstados();
	/**
	 * funcion para obtener catalogo estados
	 */
	function buscarEstados(){
		$http.post('catalogosSATController/buscarEstados.json').success(function(data, status, headers, config) {
			$scope.listaEstados=data;
		}).error(function(data, status, headers, config) {
			
		});
	};
	
	/**
	 * funcion para mostrar mensajes
	 */
	function lanzarMensajes(opcion){
		if(opcion===1){
			globalMensajes.getMensajeInfo("No has agregado alg\u00fan concepto !");
		}
		
		if(opcion===2){
			globalMensajes.getMensajeError("No existe la clave de unidad en los cat\u00e1logos del SAT para clave de unidad");
		}
		
		if(opcion===3){
			globalMensajes.getMensajeError("No existe la clave de proveedor de servicios en los cat\u00e1logos del SAT para clave de proveedor de servicios");
		}
	};
	inicializarValoresModalTimbrado();
	/**
	 * Muestra modal de timbrado
	 */
	function inicializarValoresModalTimbrado(){
		$scope.idmodalGeneracionCfdi="modalResultadoTimbradoCfdi1";		
		$scope.modalHeaderGeneracionCfdi="Resultados del timbrado CFDI versi\u00f3n 3.3";
		$scope.mostrarDescargaZip=false;
	};
	buscarConceptosPorContribuyente();
	/**
	 * funcion para mostrar catalogo de conceptos del contribuyente
	 */
	function buscarConceptosPorContribuyente(){
		$scope.listaDescripciones=[];
		$http.post('conceptos/buscarConceptosPorContribuyente.json').success(function(data, status, headers, config) {
    		$scope.listaDescripciones=data;
    		$scope.listaDescripciones.push({id:null,tcContribuyente:null,descripcion:"Agregar nueva descripci\u00f3n"});
    	}).error(function(data, status, headers, config) {
    		
    	});
	};
	
	/**
	 * funcion para reiniciar valores
	 */
	function reinicioValores(){
		crearComprobanteCfdiv33();
		$scope.receptor={};
		$scope.concepto={};
		$scope.relacionado={};
		document.getElementById("checkboxRelacion").disabled=true;
		$scope.paso = 0;
	};
	
	buscarTipoRelacion();
	/**
	 * funcion para obtener catalogo de tipo de relacion
	 */
	function buscarTipoRelacion(){
		$http.post('catalogosSATController/buscarTipoRelacion.json').success(function(data , status, headers, confif){
			$scope.listaTipoRelacion=data;
		}).error(function(data, status, headers, config){			
		});
	};
	
	/**
	 * Funcion que valida si el usuario puede seguir timbrando
	 */
	 function validarContinuidadTimbrado(){
	    	catalogosServices.validarContinuidadTimbrado().then(function(data){
	        	if(!data){
	        		$('#modalValidarContinuidadTimbrado').modal();
	        	}    	
	        });
	  };

	
	  /**
	   * Funcion que valida la forma de pago
	   */
	$scope.validarFormaPago= function(metodoPago){
		if(metodoPago=="PPD"){
			$scope.bloqueaFormaPago=true;
			$scope.comprobante.formaPago="99";
		}else{
			$scope.bloqueaFormaPago=false;
			$scope.comprobante.formaPago="01";
		}
	};
	$scope.requeridoyActivoMetodo=true;
	$scope.requeridoFormaPago=true;
	
	/**
	 * Funcion validar campos requeridos metodo de pago y forma de pago
	 */
	$scope.validarCamposRequeridos=function(tipo){
		if(tipo=="T"){ //los campos no deben existir
			$scope.comprobante.metodoPago=null;
			$scope.comprobante.formaPago=null;
			$scope.comprobante.impuestos=null;
			$scope.requeridoyActivoMetodo=false;
			$scope.bloqueaFormaPago=true;
			$scope.requeridoFormaPago=false;
		}else{//los campos son requeridos
			
			$scope.requeridoyActivoMetodo=true;
			$scope.bloqueaFormaPago=false;
			$scope.requeridoFormaPago=true;
		}
	};
	
	
	/**
	  * Encargado de mostrar los resultados del timbrado
	  */
	 function abreModalResultados(data){
		 $scope.title = "Resultados";
		 $scope.resultado = data;
		 
		 $("#modalReutilizable").modal();
	 };
}]);
