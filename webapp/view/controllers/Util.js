/**
 * directivas que se usan para el desarrollo
 */
var app = angular.module('components', [])
   .directive('uppercased', function() {
    return {
        require: 'ngModel',        
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(input) {
                return input ? input.toUpperCase() : "";
            });
            element.css("text-transform","uppercase");
        }
    };
}).directive('pasteTrimed', function(){
	var linkFn = function(scope, element, attrs) {        
        element.on('paste', function() {            
            setTimeout(function() {
            	element.val($.trim(element.val()));                
                scope.$apply();
            }, 5);            
        });
	};

	return {
		restrict: 'A',
		link: linkFn
	};
}).directive('capitalize', function() {
	   return {
		     require: 'ngModel',
		     link: function(scope, element, attrs, modelCtrl) {
		        var capitalize = function(inputValue) {
		           if(inputValue == undefined) inputValue = '';
		           var capitalized = inputValue.toUpperCase();
		           if(capitalized !== inputValue) {
		              modelCtrl.$setViewValue(capitalized);
		              modelCtrl.$render();
		            }         
		            return capitalized;
		         }
		         modelCtrl.$parsers.push(capitalize);
		         capitalize(scope[attrs.ngModel]);  // capitalize initial value
		     }
		   };
		})
		.directive('validaArchivo', ["$parse","Flash", function ($parse,Flash) {
		  return {
			   restrict: 'A',
			   link: function (scope, iElement, iAttrs) 
			   {
			    iElement.on("change", function(e){
			     $parse(iAttrs.validaArchivo).assign(scope, iElement[0].files[0]);
			         var term = ".key";
			         var index = iElement[0].files[0].name.indexOf(term);
			         if(index != -1){
		        	 var message = "<strong>Exito !</strong> Archivo Correcto";
			      Flash.create('info',message,'customAlertSuccces');
			      }else{
			       iElement.val(null);
			       var messageErrorTime = "<strong>Error !</strong> El archivo contiene una extencion incorrecta,requiere un archivo  con extensi\u00f3n .key valido.";
			       Flash.create('info',messageErrorTime,'customAlertError');       
			      }
			          
			    });
			    scope.$watch(iAttrs.validaArchivo, function(filekey) {
			     iElement.val(filekey);
			        });
			   }
			  };
	 }])
	 .directive('validaCer', ["$parse","Flash", function ($parse,Flash) {
		  return {
			   restrict: 'A',
			   link: function (scope, iElement, iAttrs) 
			   {
			    iElement.on("change", function(e){
			     $parse(iAttrs.validaCer).assign(scope, iElement[0].files[0]);
			         var term = ".cer";
			         var index = iElement[0].files[0].name.indexOf(term);
			         if(index != -1){
		        	 var message = "<strong>Exito !</strong> Archivo Correcto";
			      Flash.create('info',message,'customAlertSuccces');
			      }else{
			       iElement.val(null);
			       var messageErrorTime = "<strong>Error !</strong> El archivo contiene una extencion incorrecta,requiere un archivo  con extensi\u00f3n .cer valido.";
			       Flash.create('info',messageErrorTime,'customAlertError');       
			      }
			          
			    });
			    scope.$watch(iAttrs.validaCer, function(fileCer) {
			     iElement.val(fileCer);
			        });
			   }
			  };
	 }]);

/**
* Directiva que se encarga de validar todos los Patrones del SAT
* 
* @param nombreExpresion Es el nombre de la expresion regular tal cual esta en base 'nombre'
* @param expresionesRegulares Es el Map de Expresiones Regulares [Esta en $rootScope en app.js]
* @return true | false
*/
app.directive('validar', function() {
	return {
    	require: 'ngModel',
  		link: function(scope, element, attrs, ctrl) {
    	var nombreExpresion, expresionRegular;
         
     	//Asigna el nombre y el patron de la Exp. Reg.
     	scope.$watch(attrs.validar, function(value) {
        	nombreExpresion = value;
            angular.forEach(scope.expresionesRegulares, function(value, key) {
               if (key == nombreExpresion) {
                  expresionRegular = value;
               }
            });
            valida(expresionRegular);
         });

         //Se encarga de validar
         function valida(expresionRegular) {
            var INTEGER_REGEXP = new RegExp("^" + expresionRegular + "$");
            ctrl.$validators.validar = function(modelValue, viewValue) {
               if (ctrl.$isEmpty(modelValue)) {
                  return true;
               }

               if (INTEGER_REGEXP.test(viewValue)) {
                  return true;
               }

               return false;

            }
         }

      }
   };

});
/**
 * Directiva que permite validar si el numero ingresado es menor al total restante (saldo anterior)
 */
app.directive('menorQueTotal', function() {
	return {
		require: 'ngModel',
  		link: function(scope, element, attrs, ctrl) {
			var monto;
			ctrl.$validators.menorquetotal = function(modelValue, viewValue) {
				 
		        if (ctrl.$isEmpty(modelValue)) {
		          // tratamos los modelos vacíos como correctos
		          return true;
		        }
		 
		        if (viewValue) {
		            if(viewValue <= (scope.cfdiPadre.total - scope.cfdiPadre.totalPagado) ){
		              return true;
		            } else {
		              return false;
		            }
		        }
		 
		      // NIF inválido
		      return false;
		 
		      };

  		}
	};
});

/**
 * Directiva que validada el formato del monto registrado 18 digitos antes del punto
 * son opcionales, despues del punto el numero de decimales es opcional dependiendo de la moneda
 * @param numDecimales
 */
app.directive('monto', function() {
	return {
    	require: 'ngModel',
  		link: function(scope, element, attrs, ctrl) {
    	var decimales,regExp;
     	scope.$watch(attrs.monto, function(value) {
        	decimales = value;//Valor que se envia como parametro 
        	
        	if(decimales == 0){
        		regExp = /^[0-9]{1,18}\.[0-9]{0}$/;
        	}
        	if(decimales == 2){
        		regExp = /^[0-9]{1,18}\.[0-9]{2}$/;
        	}
        	if(decimales == 3){
        		regExp = /^[0-9]{1,18}\.[0-9]{3}$/;
        	}
        	if(decimales == 4){
        		regExp = /^[0-9]{1,18}\.[0-9]{4}$/;
        	}
            valida(regExp);
         });

         //Se encarga de validar
         function valida(regExp) {
            ctrl.$validators.monto = function(modelValue, viewValue) {
            	
               if (ctrl.$isEmpty(modelValue)) {
                  return true;
               }
               if (regExp.test(viewValue)) {
                  return true;
               }

               return false;

            }
         }

      }
   };

});


