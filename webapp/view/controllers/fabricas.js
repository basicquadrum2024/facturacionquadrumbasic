/**
 * Fabricas para mostrar mensajes al usuario
 */
angular.module('basic').factory('globalMenssages', ['Flash', function(Flash) {
	var globalMenssages = {};
	globalMenssages.dismiss = function() {
		Flash.dismiss();
	};

	globalMenssages.requeridos = function() {
		globalMenssages.dismiss();
		var message = '<strong>Mensaje !</strong>Todos los campos son requeridos';
		Flash.create('danger', message, 'customAlertError');
	};
	globalMenssages.existenCoindicencias = function() {
		globalMenssages.dismiss();
		var message = '<strong>Mensaje !</strong> <br> Existen coincidencias de b\u00fasqueda';
		Flash.create('success', message, 'customAlertSuccces');
	};

	globalMenssages.noExistenCoindicencias = function() {
		globalMenssages.dismiss();
		var message = '<strong>Mensaje !</strong> <br> No existen coincidencias de b\u00fasqueda';
		Flash.create('danger', message, 'customAlertError');
	};
	globalMenssages.enviadosConExito = function() {
		globalMenssages.dismiss();
		var message = '<strong>Mensaje !</strong> <br> Archivos enviados con \u00e9xito';
		Flash.create('success', message, 'customAlertSuccces');
	};
	globalMenssages.globalError = function() {
		globalMenssages.dismiss();
		var message = '<strong>Mensaje !</strong> <br> Existe un error favor de intentar m\u00e1s tarde';
		Flash.create('danger', message, 'customAlertError');
	};
	globalMenssages.exito = function(mensaje) {
		globalMenssages.dismiss();
		var message = '<strong>Mensaje !</strong> <br>' + mensaje;
		Flash.create('success', message, 'customAlertSuccces');
	};
	globalMenssages.error = function(mensaje) {
		globalMenssages.dismiss();
		var message = '<strong>Mensaje !</strong> <br>' + mensaje;
		Flash.create('danger', message, 'customAlertError');
	};
	return globalMenssages;

	/**
	 * Fabrica de utilerias que se pueden usar durante el desarrollo
	 */
}]).factory('utileriasFactory', [function() {
	var utileriasFactory = {};
	var hasOwnProperty = Object.prototype.hasOwnProperty;

	utileriasFactory.validarObjetoVacio = function(objeto) {
		if (objeto === undefined)
			return true;

		if (objeto === null)
			return true;

		if (objeto.length > 0)
			return false;

		if (objeto.length === 0)
			return true;

		if (objeto === true)
			return false;

		if (Object.keys(objeto).length > 0)
			return false;


		if (JSON.stringify(objeto) === JSON.stringify({}))
			return true;

		for (var key in objeto) {
			if (hasOwnProperty.call(objeto, key))
				return false;
		}
		if (typeof objeto === 'number')
			return false;

		if (JSON.stringify(objeto).length > 0)
			return false;
		return true;
	};

	utileriasFactory.eliminarCamposVaciosObjetoJson = function(objeto) {
		for (var propiedad in objeto) {
			let elObjeto = objeto[propiedad];
			if (typeof elObjeto === 'object') {
				if (!this.validarObjetoVacio(elObjeto)) {
					utileriasFactory.eliminarCamposVaciosObjetoJson(elObjeto);
				} else {
					delete objeto[propiedad];
				}
			} else if (typeof elObjeto === 'string') {
				if (!this.validarObjetoVacio(elObjeto)) {
					objeto[propiedad] = utileriasFactory.reemplazarCadenasAnexo20(elObjeto);
				} else {
					delete objeto[propiedad];
				}
			} else if (elObjeto instanceof Array) {
				for (var i = 0; i < elObjeto.length; ++i) {
					utileriasFactory.eliminarCamposVaciosObjetoJson(objeto[i]);
				}
			}
			if (this.validarObjetoVacio(elObjeto)) {
				delete objeto[propiedad];
			}
		}
	};

	utileriasFactory.reemplazarCadenasAnexo20 = function(cadena) {
		//cadena = cadena.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&apos;");
		//		cadena = cadena.replace("&", "&amp;");
		//		cadena = cadena.replace('"', "&quot;");
		//		cadena = cadena.replace("<", "&lt;");
		//		cadena = cadena.replace(">", "&gt;");
		//		cadena = cadena.replace("'", "&apos;");

		//		cadena = cadena.replace(/&/g, "&amp;");
		//		cadena = cadena.replace(/>/g, "&gt;");
		//		cadena = cadena.replace(/</g, "&lt;");
		//		cadena = cadena.replace(/"/g, "&quot;");
		//		cadena = cadena.replace(/'/g, "&apos;");
		return cadena;
	};

	return utileriasFactory;
	/**
	 * Fabrica para la descarga de archivos en la aplicacion
	 */
}]).factory('globalDownloadFile', ['$http', function($http) {
	var globalDownloadFile = {};
	globalDownloadFile.getFileDownload = function(method, parameters, typeFile, fileName, functionExecute) {
		$http({
			url: method,
			method: "GET",
			params: parameters,
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'arraybuffer'
		}).success(function(data, status, headers, config) {
			var a = document.createElement("a");
			document.body.appendChild(a);
			var file = new Blob([data], { type: typeFile });
			var fileURL = window.URL.createObjectURL(file);
			a.href = fileURL;
			a.download = fileName;
			a.click();

			if (functionExecute != null) {
				functionExecute();
			}
		}).error(function(data, status, headers, config) {
		});
	};

	globalDownloadFile.getFileDownloadPost = function(servicioExpuesto, datos, tipoArchivo, nombreArchivo) {
		$http({
			url: servicioExpuesto,
			method: "POST",
			data: datos,
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'arraybuffer'
		}).success(function(data, status, headers, config) {
			var a = document.createElement("a");
			document.body.appendChild(a);
			var file = new Blob([data], { type: tipoArchivo });
			var fileURL = window.URL.createObjectURL(file);
			a.href = fileURL;
			a.download = nombreArchivo;
			a.click();
		}).error(function(data, status, headers, config) {

		});
	};

	globalDownloadFile.descargaArchivosPost = function(servicioExpuesto, datos) {
		$http({
			url: servicioExpuesto,
			method: "POST",
			data: datos,
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'arraybuffer'
		}).success(function(data, status, headers, config) {
			let encabezadoContent = headers()['content-disposition'];
			let result = encabezadoContent.split(';')[1].trim().split('=')[1];
			let nombreArchivo = result.replace(/"/g, '');
			let contentType = headers()['content-type'];
			let tipo = globalDownloadFile.elegirTipo(contentType);
			var a = document.createElement("a");
			document.body.appendChild(a);
			var file = new Blob([data], { type: tipo });
			var fileURL = window.URL.createObjectURL(file);
			a.href = fileURL;
			a.download = nombreArchivo;
			a.click();
		}).error(function(data, status, headers, config) {

		});
	};

	globalDownloadFile.elegirTipo = function(contentType) {
		let respuesta = "";
		switch (contentType) {
			case 'application/vnd.ms-excel':
				respuesta = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
				break;

			case 'application/pdf':
				respuesta = contentType;
				break;

			case 'application/xml':
				respuesta = contentType;
				break;

			default:
				respuesta = "";
		}
		return respuesta;
	}
	return globalDownloadFile;
	/**
	 * Fabrica para realizar validacion de RFC 
	 */
}]).factory('validacionesFactory', [function() {
	var validacionesFactory = {};
	validacionesFactory.validateRfc = function(value) {
		var patron = /^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]{3}$/;
		return patron.test(value);
	};
	return validacionesFactory;
	/**
	 * Funciones para envio de mensajes a los usuarios
	 */
}]).factory('globalMensajes', function(Flash) {
	var globalMensajes = {};
	globalMensajes.getMensajeGlobal = function(status, mensaje) {

		if (status == 'Succces') {

			var messageError = "<strong>Éxito !</strong> " + mensaje;
		} else {
			var messageError = "<strong>Error !</strong> " + mensaje;
		}
		var flashMsn = Flash.create('info', messageError, 'customAlert' + status);
		return flashMsn;
	};

	globalMensajes.getMensajeCorrecto = function(mensaje) {
		var messageError = "<strong>Éxito !</strong> " + mensaje;
		var flashMsn = Flash.create('info', messageError, 'customAlertSuccces');
		return flashMsn;
	};

	globalMensajes.getMensajeError = function(mensaje) {
		var messageError = "<strong>ERROR !</strong> " + mensaje;
		var flashMsn = Flash.create('info', messageError, 'customAlertError');
		return flashMsn;
	};

	globalMensajes.getMensajeInfo = function(mensaje) {
		var messageError = "<strong>Informaci\u00f3n !</strong> " + mensaje;
		var flashMsn = Flash.create('info', messageError, 'customAlertSuccces');
		return flashMsn;
	};


	globalMensajes.getMensajeErrorTarde = function() {
		var messageError = "<strong>ERROR !</strong> Existe un error, por favor intentalo m\u00e1s tarde";
		var flashMsn = Flash.create('info', messageError, 'customAlertError');
		return flashMsn;
	};
	globalMensajes.getMensajeFacturaExito = function() {
		var messageError = "<strong>Éxito !</strong> Factura creada correctamente";
		var flashMsn = Flash.create('success', messageError, 'customAlertSuccces');
		return flashMsn;
	};
	globalMensajes.getCoincidenciasSinExito = function() {
		var messageError = "<strong>Aviso !</strong> No existen coincidencias";
		var flashMsn = Flash.create('danger', messageError, 'customAlertError');
		return flashMsn;
	};
	globalMensajes.getCoincidenciasconExito = function() {
		var messageError = "<strong>Éxito !</strong> Existen coincidencias";
		var flashMsn = Flash.create('success', messageError, 'customAlertSuccces');
		return flashMsn;
	};
	globalMensajes.getMensajeOperacionExito = function() {
		var messageError = "<strong>Éxito !</strong> Actividad realizada con \u00e9xito";
		var flashMsn = Flash.create('success', messageError, 'customAlertSuccces');
		return flashMsn;
	};
	return globalMensajes;
	/**
	 * fabrica para unicializar una lista de cancelacion
	 */
}).factory('ListaCancelacion', ['$http', '$filter', function($http, $filter) {
	var listaCancelacion = [];
	var regresa = {
		getListaCancelada: function() {
			return listaCancelacion;
		},
		nuevaListaCancelada: function(lista) {
			listaCancelacion.push(lista);
		}
	};
	return regresa;

    /**
     * Fabrica para gestionar la paguinacion
     */
}]).factory('globalPaginacion', ['$http', 'Flash', function($http, Flash) {

	var globalPaginacion = {};

	globalPaginacion.filterOptions = function() {
		return {
			filterText: '',
			externalFilter: 'searchText',
			useExternalFilter: true
		};
	};

	globalPaginacion.pagingOptions = function() {
		return {
			pageSizes: [25, 50, 100],
			pageSize: 25,
			currentPage: 1
		};
	};

	globalPaginacion.setPagingData = function(data, page, pageSize, ban, total, scope) {
		var pagedData;
		if (ban == 0) {
			if (data.listBus != undefined) {
				pagedData = data.listBus.slice(0, pageSize);
			} else {
				pagedData = data.listBus.slice(0, 0);
			}
			if (data.listBus.length == 0) {
				var message = '<strong>Mensaje !</strong> <br> No existen coincidencias de b\u00fasqueda';
				Flash.create('danger', message, 'customAlertError');
			}
		}
		if (ban == 1) {
			if (data != undefined) {
				pagedData = data.slice(0, pageSize);
			} else {
				pagedData = data.slice(0, 0);
			}
		}
		scope.dataGrid = pagedData;
		scope.pagingOptions.totalServerItems = total;

	};

	globalPaginacion.getPagedDataAsync = function($scope, pageSize, page, searchText, paginador, campo, join) {
		var paginacion = {};
		setTimeout(function() {

			if (searchText) {
				var ft = searchText;
				paginacion = {
					'clase': 'com.mx.quadrum.basic.entity.' + paginador.klase,
					'page': page,
					'pageSize': pageSize,
					'count': 0,
					'disyuncion': null,
					'conjuncion': [campo + '|' + ft],
					'between': null,
					'listBus': null,
					'order': paginador.order,
					'joinn': join
				};
				$http.post(paginador.urlRestBusca, paginacion).success(function(largeLoad) {
					if (largeLoad.error != null) {
						var messageError = "<strong>Aviso !</strong> " + largeLoad.error;
						var flashMsn = Flash.create('info', messageError, 'customAlertError');
						largeLoad.listBus = [];
						globalPaginacion.setPagingData($scope.data, page, pageSize, 1, largeLoad.count, $scope);
					} else {
						$scope.data = largeLoad.listBus.filter(function(item) {
							return JSON.stringify(item).indexOf(ft) != -1;
						});
						globalPaginacion.setPagingData($scope.data, page, pageSize, 1, largeLoad.count, $scope);
					}
				}).error(function(data, status) {
					console.error(data);
				});
			} else {
				paginacion = {
					'clase': 'com.mx.quadrum.basic.entity.' + paginador.klase,
					'page': page,
					'pageSize': pageSize,
					'count': 0,
					'listBus': null,
					'disyuncion': paginador.disyuncion,
					'conjuncion': paginador.conjuncion,
					'between': paginador.between,
					'order': paginador.order,
					'joinn': paginador.joinn
				};
				$http.post(paginador.urlRestDefault, paginacion).success(function(largeLoad) {
					if (largeLoad.error != null) {
						var messageError = "<strong>Aviso !</strong> " + largeLoad.error;
						var flashMsn = Flash.create('info', messageError, 'customAlertError');
						largeLoad.listBus = [];
						globalPaginacion.setPagingData(largeLoad, page, pageSize, 0, largeLoad.count, $scope);

					} else {
						globalPaginacion.setPagingData(largeLoad, page, pageSize, 0, largeLoad.count, $scope);
					}
				}).error(function(data, status) {
					console.error(data);
				});
			}
		}, 50);
	};


	globalPaginacion.watchfilterOptions = function($scope, paginador, campo, join) {
		$scope.$watch('filterOptions', function(newVal, oldVal) {
			if (newVal !== oldVal) {
				$scope.pagingOptions.currentPage = 1;
				$scope.getPagedDataAsync = globalPaginacion.getPagedDataAsync($scope, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText, paginador, campo, join);
			}
		}, true);
	};

	var mySelections = [];

	globalPaginacion.gridOptions = function(listaColumns, $scope) {
		return {
			data: 'dataGrid',
			enablePaging: true,
			showFooter: true,
			enableRowSelection: false,
			showSelectionCheckbox: false,
			displaySelectionCheckbox: false,
			multiSelect: false,
			showFilter: $scope.showFilter,
			totalServerItems: 'totalServerItems',
			pagingOptions: $scope.pagingOptions,
			filterOptions: $scope.filterOptions,
			columnDefs: listaColumns,
			afterSelectionChange: globalPaginacion.rowSelectedFunc,
			i18n: "es"
		};
	};

	globalPaginacion.rowSelectedFunc = function() {
		return mySelections;
	};


	globalPaginacion.editar = function() {
		//    	alert("editar");
	};
	globalPaginacion.borrar = function() {
		//    	alert("borrar");
	};
	return globalPaginacion;
	/**
	 * Fabricas para gestionar el receptor
	 */
}]).factory('receptorFactory', ['$http', 'ModalService', function($http, ModalService) {
	var receptorFactory = {};
	receptorFactory.buscarRfcLikeReceptor = function(rfcEmisor) {
		return $http.post("receptor/findLikeReceptor.json", { 'rfc': rfcEmisor });
	};
	receptorFactory.agregarReceptor = function() {
		return ModalService.showModal({
			templateUrl: 'view/admin/emergencia/emergeciaReceptor_modal.jsp',
			controller: "modalReceptor",
		}).then(function(modal) {
			modal.element.modal();
			modal.close.then(function(result) {
			});
		});
	};

	receptorFactory.editarReceptor = function() {
		return receptorFactory.agregarReceptor();
	};

	receptorFactory.receptor = function() {
		return {
			object: {}
		};
	};
	receptorFactory.receptores = function() {
		return {
			objects: []
		};
	};
	receptorFactory.getDatosReceptor = function(receptor) {
		var datos = "";
		if (receptor.rfc != null && receptor.rfc != "") {
			datos = "R.F.C: " + receptor.rfc;
		}
		if (receptor.nombre != null && receptor.nombre != "") {
			datos = datos + ", Nombre o Raz\u00f3n Social: " + receptor.nombre
		}
		return datos;
	};
	return receptorFactory;
	/**
	 * Frabrica con la lista para los criterios de busqueda
	 */
}]).factory("ListaCriteriosBusqueda", function() {
	var utileriasFactory = {};
	var data = {};
	utileriasFactory.getListaCriteriaBusqueda = function() {
		return data = {
			model: null,
			availableOptions: [

				{ id: '1', name: 'FOLIO FISCAL' },
				{ id: '2', name: 'RFC-RECEPTOR' },
				{ id: '3', name: 'ESTATUS' },
				{ id: '4', name: 'FECHA' },
				{ id: '5', name: 'FORMA DE PAGO' }
			]
		};
	}
	return utileriasFactory;
	/**
	 * Fabrica con metodos para el desarrollo
	 */
}).factory("helperFactory", function() {
	var calulos = {};
	var objects = [];
	var lista = [];
	var objeto = {};
	var objeto2 = {};
	var object = {
		getLista: function() {
			return objects;
		},
		addLista: function(value) {
			objects.push(value);
		},
		setLista: function(value) {
			objects = value;
		},
		addRemoveObjectCheckbox: function(object) {
			var index = lista.indexOf(object);
			if (index == -1) {
				lista.push(object);
			} else {
				lista.splice(index, 1);
			}
		},
		getListaCheckbox: function() {
			return lista;
		},
		clearListaCheckbox: function() {
			lista = [];
		},
		recalcula: function(arr) {
			var subtotal = 0;
			var iva = 0;
			var total = 0;
			for (x = 0; x < arr.length; x++) {
				subtotal = subtotal + Number(arr[x].importe);
				iva = iva + arr[x].impuestoIva;
				total = total + arr[x].total;
			}
			calulos.subtotal = subtotal;
			calulos.iva = iva;
			calulos.total = total;
		},
		getcalculos: function() {
			return calulos;
		},
		setObjeto: function(valor) {
			objeto = valor;
		},
		getObjeto: function() {
			return objeto;
		},
		setEmpyObtejo: function() {
			objeto = {};
		},
		setObjeto2: function(valor) {
			objeto2 = valor;
		},
		getObjeto2: function() {
			return objeto2;
		},
		setEmpyObtejo2: function() {
			objeto2 = {};
		}
	}
	return object;
    /**
     * Fabrica para realizar calculos matematicos
     */
}).factory('calculosFactory', ['$http', 'utileriasFactory', function($http, utileriasFactory) {
	var calculosFactory = {};
	var lista = [];
	var calculos = {};

	calculosFactory.calcularCantidadPorValorUnitario = function(cantidad, valorUnitario) {
		var numero = cantidad * valorUnitario;
		return calculosFactory.truncarDecimales(numero, 6);
	};

	calculosFactory.calcularCantidadPorValorUnitarioImporte = function(cantidad, valorUnitario, decimales) {
		return $http.post("comprobante_v40/calcularImporte/" + cantidad + "/" + valorUnitario + "/" + decimales + "/.json");
	};

	calculosFactory.calcularImporteImpuestos = function(base, tasaOCuota) {
		var numero = base * tasaOCuota;
		return calculosFactory.truncarDecimales(numero, 6);
	};


	calculosFactory.calcularImporteParte = function(cantidad, valorunitario) {
		var numero = cantidad * valorunitario;
		return calculosFactory.truncarDecimales(numero, 6);
	};

	calculosFactory.recalcular = function(listaConceptos) {
		var subtotal = 0;
		var total = 0;
		calculos.totalIvaTrasladado = 0;
		calculos.totalIvaRetenido = 0;
		calculos.totalDescuentos = 0;
		for (contador = 0; contador < listaConceptos.length; contador++) {
			var concepto = listaConceptos[contador];
			var cantidad = Number(concepto.cantidad);
			var valorUnitario = Number(concepto.valorUnitario);
			var importe = Number(concepto.importe);
			var descuento = Number(concepto.descuento);
			subtotal = subtotal + importe;
			calculos.totalDescuentos = calculos.totalDescuentos + descuento;
			if (!utileriasFactory.validarObjetoVacio(concepto.impuestos.traslados.traslado)) {
				calculosFactory.sumarTraslados(concepto.impuestos.traslados.traslado);
			}
			if (!utileriasFactory.validarObjetoVacio(concepto.impuestos.retenciones.retencion)) {
				calculosFactory.sumarRetenciones(concepto.impuestos.retenciones.retencion);
			}
		}
		calculos.subtotal = calculosFactory.truncarDecimales(subtotal, 2);
		calculos.total = calculos.subtotal + calculos.totalDescuentos + calculos.totalIvaTrasladado - calculos.totalIvaRetenido;
	};

	calculosFactory.sumarTraslados = function(traslados) {
		var importeTotal = 0;
		for (contador = 0; contador < traslados.length; contador++) {
			importeTotal = importeTotal + traslados[contador].importe;
		}
		calculos.totalIvaTrasladado = calculosFactory.truncarDecimales(importeTotal, 2);
	};

	calculosFactory.sumarRetenciones = function(retenciones) {
		var importeTotal = 0;
		for (contador = 0; contador < retenciones.length; contador++) {
			importeTotal = importeTotal + retenciones[contador].importe;
		}
		calculos.totalIvaRetenido = calculosFactory.truncarDecimales(importeTotal, 2);
	};

	calculosFactory.truncarDecimales = function(numero, digitos) {
		var multiplicador = Math.pow(10, digitos),
			numeroAjustado = numero * multiplicador,
			numeroTruncado = Math[numeroAjustado < 0 ? 'ceil' : 'floor'](numeroAjustado);

		return numeroTruncado / multiplicador;
	};


	calculosFactory.getcalculos = function() {
		return calculos;
	};
	return calculosFactory;
	/**
	 * Frabrica para gestionar formularios
	 */
}]).factory('fabricaFormularios', [function() {
	var formsFactory = {};
	formsFactory.showRequired = function(form) {
		var response = false;
		if (form.$valid) {
			response = true;
		} else {
			if (form != null && form != undefined) {
				var requireds = form.$error.required;
				if (!requireds) {
					requireds = form.$$success.required;
				}
				for (var int = 0; int < requireds.length; int++) {
					var field = requireds[int];
					field.$dirty = true;
					field.$pristine = false;
				}
			}
		}
		return response;
	};
	formsFactory.reincioFormulario = function(form) {
		form.$setPristine();
	};
	return formsFactory;

}]).factory('myHttpInterceptor', ['$q', function($q) {
	var canceller = $q.defer();
	return {
		'request': function(config) {
			config.timeout = canceller.promise;
			$('#loading_popup').show();
			return config;
		},
		'response': function(response) {
			$('#loading_popup').hide();
			return response;
		},
		'responseError': function(rejection) {
			$('#loading_popup').hide();
			if (rejection.status === 401) {
				canceller.resolve('Unauthorized');
				return location.href = 'index.jsp';
			}
			if (rejection.status === 403) {
				canceller.resolve('Forbidden');
				return location.href = 'index.jsp';
			}
			return $q.reject(rejection);
		}

	};
}
]).config(['$httpProvider', function($httpProvider) {
	$httpProvider.interceptors.push('myHttpInterceptor');
}])