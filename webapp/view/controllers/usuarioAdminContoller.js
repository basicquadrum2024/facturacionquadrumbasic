/**
 * Controlador para gestionar usuarios
 */
angular.module('basic').controller('controlUsuarioAdministradores',[
             '$scope','$http','Flash','$confirm','$rootScope','$filter','ModalService',
	         'ListaCancelacion','globalPaginacion','StatusesConstant','$element','$confirm',
	                                                     
	         function($scope, $http, Flash, $confirm, $rootScope,$filter, 
	         ModalService, ListaCancelacion,globalPaginacion, StatusesConstant, $element,$confirm) {
            	 
            	 $scope.filterOptions = globalPaginacion.filterOptions();
           	  $scope.totalServerItems = 0;
           	  $scope.pagingOptions = globalPaginacion.pagingOptions();

           	  var paginador = null;
           	  var campo = 'login';
           	  var joinn = [];
           	  
           	//para calculo de total de paginas
				$scope.Math = window.Math;
				//texto holder de input de busqueda
				$scope.placeholderText="Todos los criterios";
				$scope.displaySearch='inline';
           	  
           	  var listaColumns = [
  							  {field : 'login',displayName : 'Usuario (Correo Electr\u00f3nico)',width : '25%'},
   							  {field : 'nombre',displayName : 'Nombre',width : '15%'},
   							  {field : 'apellidopaterno',displayName : 'Apellido Paterno',width : '15%'},
   							  {field : 'apellidomaterno',displayName : 'Apellido Materno',width : '15%'},
    						  {field : 'perfil.tipo',displayName : 'Perfil de Usuario',width : '15%'},
   							  {field : '',displayName : 'Cambiar perfil',width : '14%',
							cellTemplate : '<a ng-click="editar(row.entity)" id="edit"  data-toggle="tooltip"'+
								'class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Actualizar Usuario">'+
								'<span class="glyphicon glyphicon-edit""></span></a>'+
   							  			   '<a ng-click="resetPassword(row.entity)" id="resetPassword" data-toggle="tooltip"'+
   							  	'class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="left" title="Cambiar Password">'+
   							  	'<span class="glyphicon glyphicon-refresh""></span></a>'} 
	   							  ];
           	  
           	  $scope.gridOptions = globalPaginacion.gridOptions(listaColumns, $scope);

           	  $scope.$watch('pagingOptions',function(newVal, oldVal) {
           		  if (newVal !== oldVal&& newVal.currentPage !== oldVal.currentPage) {
           			  $scope.getPagedDataAsync = globalPaginacion.getPagedDataAsync(
           					  $scope,$scope.pagingOptions.pageSize,$scope.pagingOptions.currentPage,
           					  $scope.filterOptions.filterText,paginador,campo,joinn);
           			  }
           		  }, true);
           	  
           	 $scope.$watch('miinput', function () {
				 var data;
				 var ft = $scope.miinput;
		          if (ft) {
		        	  data = $scope.dataGrid.filter(function(item) {
		                    return JSON.stringify(item).indexOf(ft) != -1;
		                });
		        	  $scope.dataGrid = data;
		          }else{
		        	$scope.iniVista();
		          }
		      }, true);
           	$scope.iniVista = function() {
           		
           	  paginador = {
           			  'klase' : 'Usuario',
						  'disyuncion' : null,
						  'conjuncion' : null,
						  'between' : null,
						  'order' : [ 'nombre' ],
						  'joinn' : ['perfil'],
						  'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
						  'urlRestBusca' : 'paginacion/getPaginationBuscaLike.json'
							  };
           	  
           	  $scope.pagingOptions.currentPage = 1;
           	  $scope.getPagedDataAsync = globalPaginacion.getPagedDataAsync(
           			  $scope,$scope.pagingOptions.pageSize,$scope.pagingOptions.currentPage,
									null, paginador, 'login', null);
           	  
           	  joinn = ['perfil'];
           	  globalPaginacion.watchfilterOptions($scope,paginador, 'login', joinn);	 
           	} 
	
			$scope.buscar = "";
			$scope.showModalCreateusuarios = function(id) {
			ModalService.showModal({
				templateUrl : 'view/admin/catalogos/usuarioAdministradores_modal.jsp',
				controller : "controlUsuarioAdministradores",
					}).then(function(modal) {
						modal.element.modal();
						modal.close.then(function(result) {
						$scope.message = "You said " + result;
						});
					});
				};
			
		// DATOS DE CONSULTA ADMINISTRADOR

				$scope.checkkey = function (event) {
						$http.post('usuarioAdmin/consultaUsuarioAdmin.json', 
								$scope.usu = {
										'login' : event
								},{headers: {'Content-type': 'application/json'}})
								.then(function(result) {
								$rootScope.usuarios= result.data;
								if(result.data == "" || result.data == null){
									$('#resultados').html("<div class='alert alert-danger'><strong>No se ha encontrado Registros</strong></div>");
								}else{
									$('#resultados').html(""); 
								}
							    return result.data;
						});				
			};

			// CERRAR MODAL
			$scope.cancelar = function(){
				$('#Mymodal').modal('hide');
				$element.modal('hide');
				$rootScope.usuarioo ="";
				$rootScope.usuarios ="";
				$rootScope.buscar = "";
				$scope.buscar = "";
		    };
    
		    $scope.close = function (){
		    	$rootScope.usuarioo ="";
		    	$('#Mymodal').modal('hide');
		    	$element.modal('hide');
		    	$rootScope.usuarios ="";
		    	$scope.buscar = "";
		    };

	
			// DATOS DE LA CONSULTA DE PERFIL
			$http({
				method : 'POST',
				url : 'usuarioAdmin/consultaPerfil.json',
				headers: {
				'Content-type': 'application/json'
		    }
			}).then(function(result) {
				
			    $rootScope.listPerfil = result.data;
			    return result.data;
			});
	
	
			// COMPARACION JSON
			$scope.valorinicial = function (){
		
				for(var i=0; i< $rootScope.perfil.length; i++){
					if ($rootScope.perfil[i].idPerfil == $scope.usuarios[$rootScope.idusuario].perfil.idPerfil){
						
						return i;
					}
				}
			};
	


			$scope.editar = function (usuario){
				$rootScope.usuarioo = usuario;
				 $scope.showModalCreateusuarios();	
			};
			
			$scope.resetPassword = function (usuario){
				$rootScope.usuarioo = usuario;
				$confirm({text:'Estas seguro que quieres cambiar el password a "Temporal001" ?',title: 'Cambio de Password', ok: 'Aceptar', cancel: 'Cancelar'})
		        .then(function() {
		        	popupLoading.modal('show');
		        	$http.post('usuarioAdmin/resetPassword/'+usuario.idusuario+'.json').success(function(data, status, headers,config) {
								if (data == "success") {
									popupLoading.modal('hide');
								} else {
									var messageError = "<strong>Error !</strong> Intentalo mas tarde";
									Flash.create('info',messageError,'customAlertError');
									popupLoading.modal('hide');
								}

							}).error(function(data, status, headers,config) {
								var messageError = "<strong>Error !</strong> Intentalo mas tarde";
								Flash.create('info',messageError,'customAlertError');
								popupLoading.modal('hide');
							});
		        });
			};
			
			$scope.update = function(){
		     	$rootScope.usuarios.perfil = $scope.per;
		    };
	
		    $scope.saveAdministrador = function() {
		    	var status = $('#status').val();
		    	if($scope.usuarioo.idusuario == null || $scope.usuarioo.idusuario == "" ){
		    		$scope.fecha = new Date();
		    		
		    	}else{
		    		$scope.fecha = $scope.usuarioo.fechaCreacion;
		    	}
		    	
		    	$scope.administrador = {
						'idusuario' : $scope.usuarioo.idusuario,
		    			'nombre' : $scope.usuarioo.nombre,
		    			'fechaCreacion' : $scope.fecha,
						'login' : $scope.usuarioo.login,
						'password' : $scope.usuarioo.password,
						'perfil' : $scope.usuarioo.perfil,
						'apellidopaterno' : $scope.usuarioo.apellidopaterno,
						'apellidomaterno' : $scope.usuarioo.apellidomaterno,
						'rfcEmisor' : $scope.usuarioo.rfcEmisor,
						'statusCuenta' : $scope.usuarioo.statusCuenta,
						'statusSession' : $scope.usuarioo.statusSession,
						'session' : $scope.usuarioo.session,
						'fechaModificacion' : new Date(),
						'primeraSession' : 2,
						'telefono' : $scope.usuarioo.telefono
					
				};
				$http.post('usuarioAdmin/guardaUsuarioAdmin.json',$scope.administrador)
				.success(function(data, status, headers, config) {
					var saveUser = "<strong>Datos Guardado Corretamente !!</strong>";
					Flash.create('info', saveUser , 'customAlertInfo');								
					}).error(function(data, status, headers, config) {
						var errorUser = "<strong>Error al Guardar los Datos !!</strong>";
						Flash.create('danger', errorUser , 'customAlertDanger');	
						});
		    	};
	
	
	
		}]);