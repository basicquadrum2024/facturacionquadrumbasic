/**
 * Controlador  para la administracion de administrador Buzon
 */
angular
		.module('basic')
		.controller(
				'administraBuzon',
				[
						'$scope',
						'$http',
						'$captcha',
						'Flash',
						'$confirm',
						'globalMensajes', 'globalMenssages','utileriasFactory',
						function($scope, $http, $captcha, Flash, $confirm,
								globalMensajes, globalMenssages, utileriasFactory) {
$scope.mensaje="Hola";
$scope.wrapper={};		

/**
 * Funcion crea formatos de fecha
 */
$scope.crearFechas=function(){
	$.datepicker.setDefaults($.datepicker.regional["es"]);
    
    $("#fecha1").datepicker({                    
    	dateFormat : 'yy-mm-dd',                                 
    	});
    
    $("#fecha2").datepicker({                    
    	dateFormat : 'yy-mm-dd',                                 
    	});
	};
	
	/**
	 * funcion para hacer la busqueda
	 */
	$scope.buscar=function(){
		 reloadTable();
	    };
	
/**
 * Componente para paginar la tabla de busquedas
 */
//...ng-grip
$scope.filterOptions = {
	filterText : "",
	useExternalFilter : true
};
$scope.totalServerItems = 0;
$scope.pagingOptions = {
	pageSizes : [ 25, 50, 100 ],
	pageSize : 25,
	currentPage : 1
};
$scope.setPagingData = function(data, page,
		pageSize, totalResults) {
	if (data != undefined
			&& !utileriasFactory.validarObjetoVacio(data)) {
		
	} else {
		globalMenssages.noExistenCoindicencias();
	}
	$scope.myData = data;
	$scope.pagingOptions.totalServerItems = totalResults;
	$scope.pagingOptions.pageSize = pageSize;

	if (!$scope.$$phase) {
		$scope.$apply();
	}
};

$scope.close = function(result) {
	reloadTable();
	close(result, 500);
};

$scope.$watch('miinput', function() {
	var data;
	var ft = $scope.miinput;
	if (ft) {
		data = $scope.myData.filter(function(item) {
			console.warn(ft);
			return JSON.stringify(item).toString()
					.indexOf(ft) != -1;
		});
		$scope.myData = data;
	} else {
		
		//reloadTable();
	}
}, true);

$scope.getPagedDataAsync = function(pageSize, page,
		searchText) {
	
	var post = null;
	setTimeout(
			function() {
				var data;
				if (searchText) {
					post = getPost(page, pageSize)
					var ft = searchText
							.toLowerCase();
					$http
							.post(post, $scope.wrapper)
							.success(
									function(
											largeLoad) {
										var respuesta = largeLoad.list;
										data = respuesta
												.filter(function(
														item) {
													return JSON
															.stringify(
																	item)
															.toLowerCase()
															.indexOf(
																	ft) != -1;
												});
										
										$scope
												.setPagingData(
														data,
														page,
														pageSize,
														largeLoad.totalResults);

									})
							.error(
									function(
											data,
											status,
											headers,
											config) {
									});
				} else {

					post = getPost(page, pageSize)
					$http
							.post(post, $scope.wrapper )
							.success(
									function(
											largeLoad) {
										var respuesta = largeLoad.list;
										$scope
												.setPagingData(
														respuesta,
														page,
														pageSize,
														largeLoad.totalResults);
									})
							.error(
									function(
											data,
											status,
											headers,
											config) {
									});

				}
			}, 100);
};

$scope
		.$watch(
				'pagingOptions',
				function(newVal, oldVal) {
					if (newVal !== oldVal
							&& newVal.pageSize !== oldVal.pageSize) {
						reloadTable();
					}
					if (newVal !== oldVal
							&& newVal.currentPage !== oldVal.currentPage) {
						$scope
								.getPagedDataAsync(
										$scope.pagingOptions.pageSize,
										$scope.pagingOptions.currentPage,
										$scope.filterOptions.filterText);
					}
				}, true);

$scope.$watch('filterOptions', function(newVal,
		oldVal) {
	if (newVal !== oldVal) {
		$scope.getPagedDataAsync(
				$scope.pagingOptions.pageSize,
				$scope.pagingOptions.currentPage,
				$scope.filterOptions.filterText);
	}
}, true);

$scope.revisarMensaje=function(buzon){
	$scope.buzonModifica=
	{       id:buzon.id, 

     tipo:buzon.tipo,
     asunto:buzon.asunto,
     mensaje:buzon.mensaje,
     telefono: buzon.telefono,
     correo:buzon.correo,
     estatus:buzon.estatus,
     fecha:buzon.fecha,
			
			twUsuario:{id:buzon.idUsuario}
	};
	$('#modalModificar').modal();    
}

/**
 * funcion para modificar buzon
 */
$scope.modificar=function(){
	
	$http
	.post(
			"buzonController/modificarBuzon.json",
			$scope.buzonModifica)
	.success(
			function(data, status, headers,
					config) {
				$('#modalModificar').modal('hide');
				reloadTable();
				globalMensajes
						.getMensajeCorrecto("La informaci&oacute;n fue enviada con exito, gracias por actualizar la informaci&oacute;n");
			})
	.error(
			function(data, status, headers,
					config) {
				globalMensajes
						.getMensajeError("Ocurrio un error al enviar los datos.");
			});
}
/**
 * Funcion para el tipo de comunicacion
 */
$scope.tipoComunicacion = '<div align="center" class="ui-grid-cell-contents" ng-init="myText=row.entity.tipo">'+
'<div ng-if="myText == 1"><span >SUGERENCIA</span></div>'+
'<div ng-if="myText == 2"><span >QUEJA</span></div>'+
'</div>';

$scope.cambiarEstatus='<div align="center" class="ui-grid-cell-contents" ng-init="myText=row.entity.estatus">'+
	
	'<div ng-if="myText == 0"><a href="" ng-click="revisarMensaje(row.entity)" data-toggle="tooltip" data-placement="left" title="Revizar"><span class="glyphicon glyphicon-warning-sign"></span></a></div>'+
	'<div ng-if="myText == 1"><span class="label label-success">REVISADO</span></div>'+
'</div>';
/**
 * funcion para paginar las busquedas
 */
$scope.gridOptions = {
	data : 'myData',
	enablePaging : true,
	showFooter : true,
	enableRowSelection : false,
	showSelectionCheckbox : false,
	displaySelectionCheckbox : false,
	multiSelect : false,
	totalServerItems : 'totalServerItems',
	pagingOptions : $scope.pagingOptions,
	filterOptions : $scope.filterOptions,
	columnDefs : [
			
			{
				field : 'estatus',
				displayName : 'Estatus',
				width : '10%',
			    cellTemplate:$scope.cambiarEstatus
			},
			{
				field : 'tipo',
				displayName : 'Tipo',
				width : '10%',
		        cellTemplate:$scope.tipoComunicacion
			},
			{
				field : 'asunto',
				displayName : 'Asunto',
				width : '15%'
			}, 
			{
				field : 'mensaje',
				displayName : 'Mensaje',
				width : '20%'
			}, 
			{
				field : 'fecha',
				displayName : 'Fecha ',
				width : '15%',
			   cellFilter:'date:\"dd MMM yyyy\"'
			} 
			, 
			{
				field : 'telefono',
				displayName : 'Teléfono',
				width : '15%'
			} , 
			{
				field : 'correo',
				displayName : 'Correo',
				width : '15%'
			} , 
			{
				field : 'rfcUsuario',
				displayName : 'RFC',
				width : '15%'
			} , 
			{
				field : 'nombreUsuario',
				displayName : 'Nombre',
				width : '15%'
			} 
			
			
	]
};
/**
 * Funcion para la busqueda del buzon
 */
function getPost(currentPage, pageSize) {
	$scope.wrapper.numPaguina=currentPage;
	 $scope.wrapper.tamanioPaguina=pageSize;
		return 'buzonController/bandejaBuzon.json';
};


/**
 * Funcion para mostrar todo
 */
function findAll() {
	$scope.postE = getPost(
			$scope.pagingOptions.currentPage,
			$scope.pagingOptions.pageSize);
	$scope.getPagedDataAsync(
			$scope.pagingOptions.pageSize,
			$scope.pagingOptions.currentPage);
}
;
/**
 * Funcion para recargar la tabla
 */
function reloadTable() {
	findAll();
}
;
$scope.Math = window.Math;
$scope.totalPage = function() {
	var totComp = Math
			.ceil($scope.pagingOptions.totalServerItems
					/ $scope.pagingOptions.pageSize);
	return totComp;
};
// ...final de ng-grip
//reloadTable();	



/**
 * fucion para generar excel
 */
$scope.exportarExcel=function(){
	 $http.post('buzonController/obtenerReporteExcel.json', $scope.wrapper).success(function(data) {
         excel(data);
 }).error(function(data){
	 globalMenssages
		.error("Ocurrio un error al realizar la descarga.");
});
};

function excel(data){
    var arregloDatos=[];
    for (var int = 0; int < data.length; int++) {
        var elemento = data[int];
        var objeto={
        		'ESTATUS':elemento.estatus == 1 ? "REVISADO" : "NO REVISADO",
        		'TIPO':elemento.tipo == 1 ? "SUGERENCIA" : "QUEJA",
        		'ASUNTO':elemento.asunto == null ? "" : elemento.asunto,
        		'MENSAJE':elemento.mensaje == null ? "" : elemento.mensaje,	
        		'FECHA':elemento.fecha,
        		'TELEFONO':elemento.telefono == null ? "" : elemento.telefono,
        		'CORREO':elemento.correo == null ? "" : elemento.correo,
        		'RFC': elemento.rfcUsuario == null ? "" : elemento.rfcUsuario,
        		'NOMBRE': elemento.nombreUsuario == null ? "" : elemento.nombreUsuario 
                };
        arregloDatos.push(objeto);
        }
    
    var mystyle = {	
			 headers:true,
			 column: {style:{Font:{Bold:"1", Color: "#FE2E2E"}}}		
    };						
    var f = new Date();
    var fechahoy = "_"+f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear()
    alasql('SELECT * INTO XLSXML("ReporteBuzon'+fechahoy+'.xls",?) FROM ?',[mystyle,arregloDatos]);
 }


						} ]);