/**
 * Controlador para el cambio de contrasenia
 */
angular.module('basic').controller('cambioContrasenaController',
	['$scope','$http','$captcha','Flash','$confirm','$zxcvbn',
		function($scope, $http,
			 $captcha,Flash,$confirm,$zxcvbn) {
	   // getUsuario();
	    /**
	     * funcion para obtener el usuario
	     */
	    function getUsuario() {
		$http.post('contrasenaController/obtenerUsuario.json'
		    ).success(function(data, status, headers, config) {
			 $scope.usuario = data;
		    }).error(function(data, status, headers, config) {

		    });
	    };
	    /**
	     * funcion para las validaciones del formulario de cambio de contrasenia
	     */
	    $scope.addPost = function() {
			
			//if($scope.anteriorPassword == $scope.usuario.password){
				//validar patron
				//if (($scope.pw1 == $scope.pw2) && ($scope.usuario.password != $scope.pw2) && ($scope.usuario.passwordAnterior != $scope.pw2)  ) {
					//if( $('#checkbox').prop('checked') ) {
						window.scrollTo(0, 0);
						var obj={
								nuevaContrasenia:$scope.pw2,
								contraseniaAnterior: $scope.anteriorPassword,
								primeraSession:1
						};
						$http.post('contrasenaController/cambioContrasenia.json', obj)
						.success(function(data, status, headers, config) {
							console.log("success");
							console.log(data);
							if(data.mensaje!=undefined){
								if(data.mensaje==="exito"){
									var message =data.mensaje;	  
									$scope.mostrar=true;
					                $scope.tipoError="success";
					      			$scope.valor="\u00c9xito!";
					      			$scope.mesage = "La contraseña se cambio con \u00c9xito";
					      			document.getElementById("frm").reset();	
								}else{
								$scope.mostrar=true;
								$scope.tipoError="danger";
				      			$scope.valor="Error!";
				      			$scope.mesage = data.mensaje;
								}
				      			
							}else{
								var message =data.mensaje;	  
								$scope.mostrar=true;
				                $scope.tipoError="success";
				      			$scope.valor="\u00c9xito!";
				      			$scope.mesage = "La contraseña se cambio con \u00c9xito";
				      			document.getElementById("frm").reset();	
							}
													      			
						    }).error(function(data, status, headers, config) {
						    console.log("error");
						    console.log(data);
						    	window.scrollTo(0, 0);
						    	 var message = "Al actualizar la nueva contrase\u00f1a.";
						    	 $scope.mostrar=true;
				                 $scope.tipoError="danger";
				      			 $scope.valor="Error!";
				      			 $scope.mesage = message;
				      			 $scope.resultado = '';
				                 $scope.frm.pw1.$dirty = false;
				                 $scope.frm.pw2.$dirty = false;
				                 
				                // document.getElementById("checkbox").checked = false;
						    });
						
						
//					}else{
//						window.scrollTo(0, 0);
//						   $scope.mostrar=true;
//		                   $scope.tipoError="danger";
//		      			   $scope.valor="Error!";
//		      			   $scope.mesage="Aceptar los t\u00e9rminos y condiciones";
//		                   $scope.resultado = '';
//		                   $scope.frm.pw1.$dirty = false;
//		                   $scope.frm.pw2.$dirty = false;
//		                 
//		                   document.getElementById("checkbox").checked = false;
//						
//					}
//				} else if ($scope.usuario.password == $scope.pw2){
//					window.scrollTo(0, 0);
//					$scope.mostrar=true;
//	                $scope.tipoError="danger";
//	   			   	$scope.valor="Error!";
//	   			   	$scope.mesage="La nueva contrase\u00f1a no puede ser igual a la antigua";
//                    $scope.resultado = '';
//                    $scope.frm.pw1.$dirty = false;
//                    $scope.frm.pw2.$dirty = false;
//                   
//                  //  document.getElementById("checkbox").checked = false;
//				}
//			}else {
//				window.scrollTo(0, 0);
//				var message = "Contrase\u00F1a anterior no v\u00E1lida";	  
//				$scope.mostrar=true;
//                $scope.tipoError="danger";
//      			$scope.valor="Error!";
//      			$scope.mesage = message;
//      			$scope.anteriorPassword="";
//			}
			
			console.log("entra aqui")
			 var pwd = $scope.pw1;
	    	 $scope.passwordStrength = pw1 ? ((regExp.test(pw1)) && $zxcvbn.score(pw1) || 0) : null;

	};
	/**
	 * funcion para el cambio de contrasenia
	 */
	$scope.cambiar= function (){
		console.log("metodo cambiar cambiar");
	    var checkbox = document.getElementById('checkpassW');
	    var pass1 = document.getElementById('pw1');
	    var pass2 = document.getElementById('pw2');
	    if($scope.opcion==true){
		pass1.type = "text";
		pass2.type = "text";
	    }else{
		pass1.type = "password";
		pass2.type = "password";
	    }
	};
	
	/**
	 * Validar Contraseña
	 */
	$scope.$watch("[pw1,pw2]", function(asd,ass) {
    	
    	 var pwd = $scope.pw1;
    	 $scope.passwordStrength = pwd ? ((regExp.test(pwd)) && $zxcvbn.score(pwd) || 0) : null;
     });
}]);