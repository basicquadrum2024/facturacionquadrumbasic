/**
 * Controlador para gestionar componente ng-grip
 */ 
angular.module('basic').controller('controllerPag', function($scope, $http,globalPaginacion) {		  
		  
	 	 // default
	      $scope.filterOptions =globalPaginacion.filterOptions();
	      $scope.totalServerItems = 0;
	      $scope.pagingOptions =globalPaginacion.pagingOptions();
	      
	      //datos a buscar al sumbmit
	      $scope.paginador={
	    		  'klase':'Usuario',
	    		  'disyuncion':null,
	    		  'conjuncion':['login|promero@quadrum.com.mx'],
	    		  'between':null,
	    		  'order':['apellidopaterno','nombre'],
	    		  'joinn':['sucursal'],
	    		  'urlRestDefault':'paginacion/getPaginationObjetBusca.json',
	    		  'urlRestBusca':'paginacion/getPaginationObjetBusca.json'
	      			
	      };
	      //busqueda Inicial al submitir
	      $scope.getPagedDataAsync=globalPaginacion.getPagedDataAsync($scope,$scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage,null, $scope.paginador,'login',null);
	    //busqueda por pagina deafult
	      $scope.paginador.joinn=['sucursal'];
	      globalPaginacion.watchpagingOptions($scope,$scope.paginador,'login',$scope.paginador.joinn);
	      //busqueda por filtro defaul
	      globalPaginacion.watchfilterOptions($scope,$scope.paginador,'login',$scope.paginador.joinn);

	      //globalPaginacion.watchmiinput($scope);
	     
	      //columnas a mostrar defaul
	      var listaColumns=[
		                       { field: 'login', displayName: 'Correo', width: '15%' },
		                       { field: 'nombre', displayName: 'Nombre', width: '25%',enableCellEdit: true },
		                       { field: 'fechaCreacion', displayName: 'fecha creaci\u00f3n', width: '15%',cellFilter: "date:'yyyy-MM-dd HH:mm:ss a'" },   
		                       { field: 'apellidopaterno', displayName: 'Apellido Paterno', width: '10%' },
		                       { field: 'apellidomaterno', displayName: 'Apellido Materno', width: '10%' },
		                       { field: 'sucursal.nombre', displayName: 'sucursal', width: '10%' },
		                       { field:'', displayName:'opci\u00f3n', width: '20%' , cellTemplate: '<a ng-click="editar()" id="edit"  data-toggle="tooltip"><i class="glyphicon glyphicon-edit" ></i></a><a ng-click="borrar()"  id="delete"  data-toggle="tooltip"><i class="glyphicon glyphicon-remove"></i></a>'}
		                   ];
	      $scope.gridOptions = globalPaginacion.gridOptions(listaColumns,$scope);
	      //accion de editar
	      $scope.editar=function(){
	    	  globalPaginacion.editar();
	      };
	      // accionde eliminar
	      $scope.borrar=function(){
	    	  globalPaginacion.borrar();
	      };
	    	  
	  });