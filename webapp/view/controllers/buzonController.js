/**
 * Controlador para el modulo de buzon
 */
angular
		.module('basic')
		.controller(
				'buzonController',
				[
						'$scope',
						'$http',
						'$captcha',
						'Flash',
						'$confirm',
						'globalMensajes',
						function($scope, $http, $captcha, Flash, $confirm,
								globalMensajes) {

							function iniciaValores() {
								$scope.buzon = {};
								$scope.buzon.tipo = 1;
							}
							;
							iniciaValores();
                            /**
                             * funcion para enviar buzon
                             */
							$scope.enviar = function(buzon) {

								$http
										.post(
												"buzonController/guardarEnBuzon.json",
												buzon)
										.success(
												function(data, status, headers,
														config) {
													iniciaValores();
													globalMensajes
															.getMensajeCorrecto("La informaci&oacute;n fue enviada con exito, gracias por comunicarte con nosotros");
												})
										.error(
												function(data, status, headers,
														config) {
													globalMensajes
															.getMensajeError("Ocurrio un error al enviar los datos.");
												});

							}

						} ]);
