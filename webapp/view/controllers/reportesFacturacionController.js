/**
 * Controlador para gestionar los reportes 
 */
angular.module("capufe")
.controller("controlReportesFacturacion", ['$scope','$http','ModalService','Flash','globalDownloadFile',function($scope, $http,ModalService,Flash,globalDownloadFile) {

	$http.post("sucursalController/findByemisor.json").success(function(data){
		   $scope.emisores=data;

 });
	
	/**
	 * descarga archiv pdf
	 */
	$scope.fileDownloadFactDiaria= function(metodo,caso){
		var pdf ='application/pdf';
		var facturacion={fechaInicio:$scope.fechaInicio+':00',fechaFin:$scope.fechaFin+':59',rfcEmisor:$scope.rfc};
		globalDownloadFile.getFileDownload(metodo,facturacion,pdf);
	};
	
}]);