angular.module("basic", ['ngRoute', 'angularModalService','flash','ngAnimate','components','angular-confirm','checklist-model','ngGrid','modalCnfReutilizable'])
//Home controller
/***********************************************************************************************/
.controller("modalControl", function($scope, $http, ModalService) {
    if($scope.primeraSession==0 || $scope.rfcEmisor=="" || $scope.rfcEmisor==null){
    	ultimaConexion();
    	modalCambio();
    }else{
    	caducaPassword();
    	caducaLogin();
    	ultimaConexion();
    }
    
    /**
     * funcion para validar la caducidad de la contraseña
     */
    function caducaPassword(){
    	var fechaActual = obtenerFecha();
    	var fechaCambio = $scope.fechaCambio;
    	var fechaFinal = obtenerFechaFinal(fechaCambio);    	
	    if(fechaActual > fechaFinal){
//	    	alert("Fecha actual=> "+fechaActual+ "\n" 
//	    			+"Fecha cambio=> "+fechaCambio+"\n"
//	    			+"Fecha cambio + un mes=> "+fechaFinal);
	    	modalPassCaducada();  		
	    }
    }
    /**
     * funcion para validar la sesion 
     */
    function caducaLogin(){
    	var fechaActual = obtenerFecha();
    	var fechaUlLogin = $scope.fechaLogin;
    	var fechaUlFinal = obtenerFechaFinal(fechaUlLogin);
    	if(fechaActual > fechaUlFinal){
//    		alert("Fecha actual=> "+fechaActual+ "\n" 
//    				+"Fecha login=> "+fechaUlLogin+"\n"
//    				+"Fecha login + un mes=> "+fechaUlFinal);
    		modalLoginCaducado();
    	}
    }

    /**
     * funcion para obtener la fecha de la ultima conexion
     */
    function ultimaConexion(){
    	$http.post('contrasenaController/ultimoLogin.json').success(function(data, status, headers, config) {
	    	
	    }).error(function(data, status, headers, config) {s

	    });	
    }
    
    /**
     * funcion para obtener la fecha actual
     */
	function obtenerFecha(){
	    var f = new Date();
	    var fechActual = f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate() ;
	    return fechActual;
	}
	 
	/**
	 * funcion para obtener la fecha final
	 */
	function obtenerFechaFinal(fechaCambio){
		var f = new Date(fechaCambio),
		 	dia = f.getDate(),
	        mes = f.getMonth() + 1,
	        anio = f.getFullYear(),
	        addTime = 30 * 86400; //Tiempo de 30 días en segundos
        	f.setSeconds(addTime); //Añado el tiempo
	    return f;
	}
	
	/**
	 * Funcion para mostrar el modal de cambio de contraseña
	 */
  function modalCambio() {
	ModalService.showModal({
		templateUrl : 'view/usuario/modalCambio.jsp',
		controller : "modalPassword",				
	}).then(function(modal) {
		modal.element.modal();
		modal.close.then(function(result) {
		  
		});
	});
  };
  
  /**
   * Funcion para mostrar modal de contraseña caducada
   */
  function modalPassCaducada() {
		ModalService.showModal({
			templateUrl : 'view/usuario/modalPassCaducada.jsp',
			controller : "modalPassword",				
		}).then(function(modal) {
			modal.element.modal();
			modal.close.then(function(result) {
			  
			});
		});
  };
  
  /**
   * Funcion para mostrar el fin de la sesion
   */
  function modalLoginCaducado(){
	  ModalService.showModal({
			templateUrl : 'view/usuario/modalLoginCaducado.jsp',
			controller : "modalPassword",				
		}).then(function(modal) {
			modal.element.modal();
			modal.close.then(function(result) {
			  
			});
		});
  };

})
/***********************************************************************************************/
/**
 * Controlador para gestion modulo de contraseña
 */
.controller("modalPassword",function($scope, close, $element, $http, Flash,$captcha){

	var contador = 0;
	$scope.mostrar=false;
	$scope.opcion=false;
    $scope.lispost = {};
    $scope.$watch('minlength', function () {
    	  if($scope.minlength==undefined){
        	   $scope.minlength=false;
        	   
        	   $scope.mostrar=$scope.minlength;
           }else{
               $scope.mostrar=$scope.minlength;
               $scope.tipoError="danger";
               $scope.valor="Aviso!";
               $scope.mesage="La contrase\u00f1a debe tener longitud m\u00ednima de 8 caracteres, longitud m\u00e1xima de 15 car\u00e1cteres";
           }
	 }, true);
    
    $scope.$watch('pattern', function () {
	  	  if($scope.pattern==undefined){
	      	   $scope.pattern=false;
	      	 
	      	   $scope.mostrar=$scope.pattern;
	         }else{
	             $scope.mostrar=$scope.pattern;
	             $scope.tipoError="danger";
				   $scope.valor="Aviso!";
				   $scope.mesage="La contrase\u00f1a no cumple con el patr\u00f3n requerido.";
	         }
	}, true);
    
    $scope.$watch('pwmatch', function () {
      if($scope.pwmatch==undefined){
	  $scope.pwmatch=false;
	  $scope.mostrar=$scope.pwmatch;
      	 if($scope.pwmatch==false){
      	   contador++;
      	  
      	   if(contador==2){
      		 contador=1;
      		   $scope.mostrar=true;
               $scope.tipoError="success";
               $scope.valor="Exito!";
               $scope.mesage="Las contrase\u00f1as coinciden";  
      	   }
         }
	  }else{
             $scope.mostrar=$scope.pwmatch;
             $scope.tipoError="danger";
			 $scope.valor="Aviso!";
			 $scope.mesage="Las contrase\u00f1as no coinciden";
      }
	 }, true);
    
    
	 $http.post('contrasenaController/obtenerUsuario.json').success(function(data, status, headers, config) {
		 $scope.usuario = data;
	 	}).error(function(data, status, headers, config) {
	 });
	 
	$scope.addPost = function(result) {
		
		popupLoading.modal('show');
		if($scope.anteriorPassword == $scope.usuario.password){

			if (($scope.pw1 == $scope.pw2) && ($scope.usuario.password != $scope.pw2)) {
				
				if( $('#checkbox').prop('checked') ) {
					
					var obj={
							nuevaContrasenia:$scope.pw2,
							primeraSession:1
					};
					$http.post('contrasenaController/cambioContrasenia.json', obj).success(function(data, status, headers, config) {
						
					    	if(data=="success"){
								var message = "<strong>\u00c9xito!</strong> <br>" +
									"Se ha cambiado el password correctamente.";	  
								Flash.create('info', message, 'customAlertSuccces');	
								popupLoading.modal('hide');
					    	}
					    	
					    }).error(function(data, status, headers, config) {
					    	var message = "<strong>Error!</strong> <br>" +
								"Al actualizar la nueva contraseña.";
					    	Flash.create('info', message, 'customAlertError');
							popupLoading.modal('hide');
					    });	
					close(result, 500); // close, but give 500ms for bootstrap to animate
				
				}else{
					   $scope.mostrar=true;
	                   $scope.tipoError="danger";
	      			   $scope.valor="Error!";
	      			   $scope.mesage="Aceptar los t\u00e9rminos y condiciones"; 
					
				}
			} else if ($scope.usuario.password == $scope.pw2){
				$scope.mostrar=true;
                $scope.tipoError="danger";
   			   	$scope.valor="Error!";
   			   	$scope.mesage="La nueva contrase\u00f1a no puede ser igual a la antigua"; 	
			}
			
		}else {
			$scope.mostrar=true;
            $scope.tipoError="danger";
			   	$scope.valor="Error!";
			   	$scope.mesage="Contrase\u00F1a anterior no v\u00E1lida"; 	
      			$scope.anteriorPassword="";
			}

	};
	
	$scope.cambiar= function (){
	    var checkbox = document.getElementById('checkpassW');
	    var pass1 = document.getElementById('pw1');
	    var pass2 = document.getElementById('pw2');
	    if($scope.opcion==true){
		pass1.type = "text";
		pass2.type = "text";
	    }else{
		pass1.type = "password";
		pass2.type = "password";
	    }
	};
	
	$scope.past = function() {
		$timeout(function() {
			$scope.pw2 = "";
			}, 0);
		}
})
/***********************************************************************************************/
.directive('captcha',function() {
	return {
		restrict : 'E',
		template : '<br><br> <span ng-model="field1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{field1}}</span> '
				+ '<span ng-model="operator">{{operator}}</span> '
				+ '<span ng-model="field2">{{field2}}</span> = ',
		scope : {
			field1 : "@", // variables de alcance($scope) o
			// por valor
			field2 : "@", // variables de alcance($scope) o
			// por valor
			operator : "@" // variables de alcance($scope) o
		// por valor
		},
	}
})
/***********************************************************************************************/
.factory('$captcha',['$rootScope',function($rootScope) {
			return {
				// obtenemos los numeros y el operador para la
				// operación actual
				getOperation : function() {
					// array de operadores
					$rootScope.operators = [ "+", "-", "*", "/" ];
					// obtenemos un operador aleatorio
					$rootScope.operator = $rootScope.operators[Math
							.floor(Math.random()
									* $rootScope.operators.length)];
					// numero aleatorio entre 25 y 6
					$field1 = Math.floor(Math.random()
							* (25 - 6) + 6);
					// si el operador es una división
					if ($rootScope.operator == "/") {
						// obtenemos los posibles divisores del
						// numero obtenido
						$num = this.getDivisors($field1);
						// field2 es un numero aleatorio de los
						// posibles divisores que nos
						// ha proporcionado $num
						$field2 = $num[Math.floor(Math.random()
								* $num.length)];
					} else {
						// en otro caso, obtenemos un numero
						// aleatorio
						$field2 = Math
								.floor((Math.random() * 5) + 1);
						// comprobamos que fiel1 sea mayor que
						// field2
						while ($field1 < $field2) {
							$field1 = Math
									.floor((Math.random() * 15));
							$field2 = Math
									.floor((Math.random() * 5) + 1);
						}
					}
					// asignamos field1 y field2
					$rootScope.field1 = $field1;
					$rootScope.field2 = $field2;
				},

				// obtiene los posibles numeros divisores del
				// que hemos pasado como parametro,
				// si es 14 devuelve un array como este [1, 2,
				// 7, 14] etc
				// fuente:
				// http://nayuki.eigenstate.org/res/calculate-divisors-javascript.js
				getDivisors : function(n) {
					if (n < 1)
						n = 0;

					var small = [];
					var large = [];
					var end = Math.floor(Math.sqrt(n));
					for (var i = 1; i <= end; i++) {
						if (n % i == 0) {
							small.push(i);
							if (i * i != n)
								large.push(n / i);
						}
					}
					large.reverse();
					return small.concat(large);
				},

				// retornamos el resultado de la operación
				// realizada
				// podriamos haber hecho un switch, pero sirve
				// perfectamente
				getResult : function(n1, n2, operator) {
					if (operator == "*") {
						return (n1) * (n2);
					} else if (operator == "+") {
						return (n1) + (n2);
					} else if (operator == "/") {
						return (n1) / (n2);
					} else {
						return (n1) - (n2);
					}
				},

				// resultado es el input que el usuario pone en
				// el formulario
				// para responder a la operación
				checkResult : function(resultado) {
					// si la respuesta a la operacion es
					// correcta
					if (parseInt(resultado) == this.getResult(
							parseInt($rootScope.field1),
							parseInt($rootScope.field2),
							$rootScope.operator)) {
						return true;
					}
					// en otro caso cambiamos la operacion
					else {
						this.getOperation();
					}
				}
			}
		} ]).run(function($captcha) {
$captcha.getOperation();
})
/***********************************************************************************************/
.controller('cmodal', function($scope, ModalService) {
	$scope.showAvisoPrivacidad = function() {
		ModalService.showModal({
			templateUrl : 'view/home/aviso/addAvisoPriv.jsp',
			controller : "modalController"
		}).then(function(modal) {
			modal.element.modal();
			modal.close.then(function(result) {
				$scope.message = "You said " + result;
			});
		});
	};
}).controller('modalController', function($scope, close) {

	$scope.close = function(result) {
		// close(result, 500); // close, but give 500ms for bootstrap to
		// animate
	};

})
/***********************************************************************************************/
.directive('pwCheck',['Flash' ,function(Flash) {
    return {
    	require : 'ngModel',link : function(scope, elem, attrs, ctrl) {
    	    var firstPassword = '#' + attrs.pwCheck;
    	    elem.add(firstPassword).on('keyup',function() {
    		scope.$apply(function() {
    		    // console.info(elem.val() ===
    		    // $(firstPassword).val());
    		    ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
    		    if(elem.val() === $(firstPassword).val()){
    			
    		    }else{
//    								    var messageError = "<strong>ERROR !</strong> No coinciden las contrase\u00f1as";
//    								      Flash.create('success', messageError, 'customAlertError'); 
    			}
    		    });
    		});
    	    }
        }
        }
    ]);