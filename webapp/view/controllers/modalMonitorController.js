/**
 * Controlafor para gestionar el tiempo en sesion
 */
angular.module('basic').controller('monitorController',function($scope,ModalService){
	var timpoInactivo = 0;
	var tiempomaximoSession=$scope.tiempoSession;
	$(document).ready(function () {
	    var idleInterval = setInterval(incrementarTiempo, 1000); // 1 minute
	    $(this).mousemove(function (e) {
	    	timpoInactivo = 0;
	    });
	
	    $(this).keypress(function (e) {
	    	timpoInactivo = 0;
	    });
	});
	
	function incrementarTiempo() {
		timpoInactivo = timpoInactivo + 1;
	    if (timpoInactivo == tiempomaximoSession) { // 10 minutes                    
	    	cargarModal();
	    }
	}
	
	function cargarModal(){
		ModalService.showModal({
			templateUrl : 'view/login/modalMonitor.jsp',
			controller : "modalmonitorController"
		}).then(function(modal) {			
			modal.element.modal();
			modal.close.then(function(result) {
			});
		});
	}
	
}).controller('modalmonitorController',function($scope, close, $element, $http, Flash) {
	$scope.recargarPagina = function(result) {						
		close(result, 500);
		
	};
	$scope.cancel = function(result) {		
		close(result, 500);
	};

});