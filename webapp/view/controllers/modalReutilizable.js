/**
 * Directivas para gestionar el modal reutilizable
 */
angular.module('modalCnfReutilizable',[]).directive('modalDinamic', [function () {
    return {
        restrict: 'EA',
        scope: {
            title: '=modalTitle',
            header: '=modalHeader',
            body: '=modalBody',
            footer: '=modalFooter',
            callbackbuttonleft: '&ngClickLeftButton',
            callbackbuttonright: '&ngClickRightButton',
            idModal: '=idModal',
            receptor:'=modalReceptor',
            conceptosList:'=modalConceptos',
            datosEmisor:'=modalEmisorData'
        },
        templateUrl: 'view/principal/modalConfirmacionFacturacion.jsp',
        transclude: true,
        controller: function ($scope) {        	
        },
    };
}])
.directive('modalReutilizable', function () {
    return {
        restrict: 'EA',
        scope: {
            title: '=modalTitle',
            header: '=modalHeader',
            body: '=modalBody',
            footer: '=modalFooter',
            callbackbuttonleft: '&ngClickLeftButton',
            callbackbuttonright: '&ngClickRightButton',
            idmodalreutilizable: '=idModalReutilizable',
            resultadoWrappers:'=resultadoWrappers',
            mostrarDescargaZip:'=mostrarDescargaZip',
            funcionDescargarZip:'&funcionDescargarZip'
        },
        templateUrl: 'view/modalReutilizableTimbrado.jsp',
        transclude: true,
        controller: function ($scope) {
        	$('#modalBodyR').append($scope.body);
        },
    };
}).directive('modalReutilizablecancelacion', function () {
    return {
        restrict: 'EA',
        scope: {
            title: '=modalTitle',
            header: '=modalHeader',
            body: '=modalBody',
            footer: '=modalFooter',
            callbackbuttonleft: '&ngClickLeftButton',
            callbackbuttonright: '&ngClickRightButton',
            idmodalreutilizablec: '=idModalReutilizablec',
            resultadoWrappers:'=resultadoWrappers'
        },
        templateUrl: 'view/modalReutilizable.jsp',
        transclude: true,
        controller: function ($scope) {        	
        },
    };
});