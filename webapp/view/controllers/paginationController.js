/**
 * Controler para gestionar paguinacion de componente ng-grip
 */ 
angular.module('basic').controller('controllerPag', function($scope, $http) {
		  
	      $scope.filterOptions = {
	          filterText: "",
	          useExternalFilter: true
	      };
	      $scope.totalServerItems = 0;
	      $scope.pagingOptions = {
	          pageSizes: [25,50,100],
	          pageSize: 25,
	          currentPage: 1
	      };  
	      
	      
	      $scope.setPagingData = function(data, page, pageSize,ban,total){	
	          //var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
	    	  var pagedData;
	    	  if(ban==0){
		    	  if(data.listBus!=undefined){
		    		  pagedData = data.listBus.slice(0, pageSize);
		    	  }
	    	  }
	    	  if(ban==1){
	    		  if(data!=undefined){
	    			  pagedData = data.slice(0, pageSize);
	    		  }
	    	  }
	          $scope.dataGrid = pagedData;
	          $scope.pagingOptions.totalServerItems = total;
	          if (!$scope.$$phase) {
	              $scope.$apply();
	          }
	      };
	      
	      $scope.maxPages = function(){
	    	    data=Math.round($scope.pagingOptions.totalServerItems/$scope.pagingOptions.pageSize);
	    	    return data;
	    	};
	      $scope.getPagedDataAsync = function (pageSize, page, searchText) {
	          setTimeout(function () {
	              var data;
	              if (searchText) {
	                  //var ft = searchText.toLowerCase();
	            	  var ft = searchText;
	                  $scope.paginacion = {
					            'clase':'com.mx.quadrum.basic.entity.'+'Usuario',
								'page' : page,
								'pageSize' : pageSize,
								'count' : 0,
								'conjuncion':{'nombre':ft},
								'between':[['fechaCreacion','2015-06-10'],['fechaCreacion','2015-11-11']],
								'order':['fechaCreacion','nombre'] 
							};
	                  $http.post('paginacion/getPaginationObjetBusca.json',$scope.paginacion).success(function (largeLoad) {
	                	  if(largeLoad.error!=null){
	            			  alert(largeLoad.error);
	            		  }else{
	                      data = largeLoad.listBus.filter(function(item) {
	                          //return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
	                    	  return JSON.stringify(item).indexOf(ft) != -1;
	                      });
	                      $scope.setPagingData(data,page,pageSize,1,largeLoad.count);
	            		  }
	                  });            
	              } else {
	            	  $scope.paginacion = {
					            'clase':'com.mx.quadrum.basic.entity.'+'Usuario',
								'page' : page,
								'pageSize' : pageSize,
								'count' : 0,
								'order':['fechaCreacion','nombre'] 
							};
//	            	  $http.post('paginacion/getPaginationData/'+pageSize+"/"+page+".json").success(function (largeLoad) {
	            		  $http.post('paginacion/getPaginationObjetBusca.json',$scope.paginacion).success(function (largeLoad) {
	            		  if(largeLoad.error!=null){
	            			  alert(largeLoad.error);
	            		  }else{
	                      $scope.setPagingData(largeLoad,page,pageSize,0,largeLoad.count);
	            		  }
	                  });
	              }
	          }, 1000);
	      };
	  	
	      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
	  	
	      $scope.$watch('pagingOptions', function (newVal, oldVal) {
	          if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
	            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
	          }
	      }, true);
	      $scope.$watch('filterOptions', function (newVal, oldVal) {
	          if (newVal !== oldVal) {
	        	  $scope.pagingOptions.currentPage=1;
	            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
	          }
	      }, true);
	      
	    //cada vez que escribo en el input
	      $scope.$watch('miinput', function () {
	          if ($scope.miinput !== "") {
	              $scope.pagingOptions.currentPage=1;
	              $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.miinput);
	          }else{
	              $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
	          }
	      }, true);
	     
	      $scope.editar = function(){
	    	  alert("editar");
	      };
	      
	      $scope.borrar = function(){
	    	  alert("borrar");
	      };
	      
	      $scope.gridOptions = {
	          data: 'dataGrid',
	          enablePaging: true,
	          showFooter: true,
		      enableCellEdit: true,
		      multiSelect: false,
		      pinSelectionCheckbox: false,
		      selectWithCheckboxOnly: false,
		      showSelectionCheckbox: false,
	          totalServerItems:'totalServerItems',
	          pagingOptions: $scope.pagingOptions,
	          filterOptions: $scope.filterOptions,
	          columnDefs: [
	                       { field: 'login', displayName: 'Correo', width: '15%' },
	                       { field: 'nombre', displayName: 'Nombre', width: '25%',enableCellEdit: true},
	                       { field: 'fechaCreacion', displayName: 'fecha creaci\u00f3n', width: '15%',cellFilter: "date:'yyyy-MM-dd HH:mm:ss a'" },   
	                       { field: 'apellidopaterno', displayName: 'Apellido Paterno', width: '10%' },
	                       { field: 'apellidomaterno', displayName: 'Apellido Materno', width: '10%' },
	                       { field:'', displayName:'opci\u00f3n', width: '20%' , cellTemplate: '<a ng-click="editar()" id="edit"  data-toggle="tooltip"><i class="glyphicon glyphicon-edit" ></i></a><a ng-click="borrar()"  id="delete"  data-toggle="tooltip"><i class="glyphicon glyphicon-remove"></i></a>'}
	                   ]
	      };
	  });