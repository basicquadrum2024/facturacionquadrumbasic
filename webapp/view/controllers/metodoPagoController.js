/**
 * Controlador para gestionar modulo de metodo de pago
 */
angular.module("basic")
.controller("controlMetodoPago", ['$scope','$http','ModalService','Flash','globalPaginacion','helperFactory','$confirm',function($scope, $http,ModalService,Flash,globalPaginacion,helperFactory,$confirm) {
	$scope.filterOptions =globalPaginacion.filterOptions();
    $scope.totalServerItems = 0;
    $scope.pagingOptions =globalPaginacion.pagingOptions();
    $scope.metodoPago={};
    $scope.metodoPago=helperFactory.getObjeto();
    
    var paginador=null;
    var campo=null;
    var joinn=[];
    $scope.valor='';

    /**
     * Funciones del componente ng-grip
     */
	  //para calculo de total de paginas
	$scope.Math = window.Math;
	//texto holder de input de busqueda
	$scope.placeholderText="Todos los criterios";
	$scope.displaySearch='inline';
    
    var listaColumns=[
                      { field: 'clave', displayName: 'Clave', width: '10%'},
                      { field: 'concepto', displayName: 'Concepto', width: '35%'},   
                      { field: '', displayName: 'Opciones', width: '15%',cellTemplate:'<a href="" ng-click="editar(row.entity)" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="Actualizar Plaza de Cobro"><span class="glyphicon glyphicon-edit"></span></a><a href="" ng-click="borrar(row.entity)" class="btn btn-danger btn-xs red-tooltip" data-toggle="tooltip" data-placement="right" title="Eliminar Plaza de Cobro"><span class="glyphicon glyphicon-remove"></span></a>' },
                     
                      ];
    $scope.gridOptions = globalPaginacion.gridOptions(listaColumns,$scope);
    	$scope.$watch('pagingOptions', function (newVal, oldVal) {
	          if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
	        	$scope.getPagedDataAsync=globalPaginacion.getPagedDataAsync($scope,$scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText,paginador,campo,joinn);
	          }
	      }, true);
    	
    	 $scope.$watch('miinput', function () {
			 var data;
			 var ft = $scope.miinput;
	          if (ft) {
	        	  data = $scope.dataGrid.filter(function(item) {
	                    return JSON.stringify(item).indexOf(ft) != -1;
	                });
	        	  $scope.dataGrid = data;
	          }else{
	        	  recargar();
	          }
	      }, true);
    	 
    	 function recargar(){
				paginador={
		    	  		  'klase':'MetodoPago',
		    	  		  'disyuncion':null,
		    	  		  'conjuncion':null,
		    	  		  'between':null,
		    	  		  'order':null,
		    	  		  'joinn':null,
		    	  		  'urlRestDefault':'paginacion/getPaginationObjetBusca.json',
		    	  		  'urlRestBusca':'paginacion/getPaginationObjetBusca.json'
		    	    			
		    	    };
		    	$scope.pagingOptions.currentPage=1;
				$scope.getPagedDataAsync=globalPaginacion.getPagedDataAsync($scope,$scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage,null, paginador,'login',null);
				joinn=null;
				globalPaginacion.watchfilterOptions($scope,paginador,campo,joinn);
			}
    	 $scope.inicio=function(){
    		 recargar();
    	 }
    	 /**
    	  * fin funciones de ng-grip
    	  */
    	 
    	 /**
    	  * funcion para mostrar modal EditarMetodoPago
    	  */
    	 $scope.muestramodal=function(){
				ModalService.showModal({
					templateUrl : 'view/admin/catalogos/metodoPago_modal.jsp',
					controller : "controlEditarMetodoPago"
					}).then(function(modal) {
					modal.element.modal();
					modal.close.then(function(result) {
					});
				});
				
			}
    	 
    	 /**
    	  * funciones CRUD 
    	  */
    	 $scope.nuevo=function(){
    		 helperFactory.setEmpyObtejo();
				$scope.muestramodal();
			};
			
		 $scope.editar=function(obj){
			 helperFactory.setObjeto(obj);
			 $scope.muestramodal();
		 };	
		 
		 $scope.borrar = function(obj) {
				$confirm({
					text : 'Deseas Eliminar el metodo de pago?',
					title : 'Eliminar Meotod de pago',
					ok : 'Aceptar',
					cancel : 'Cancelar'
				}).then(function() {
					$http.post('metodoPago/borrarMetodoPago.json',obj).success(function(data,status,headers,config) {
						if(data=='succes'){
						var messageSuccess = "<strong>CORRECTO !</strong> Metodo de pago Eliminado Correctamente.";
						Flash.create('info',messageSuccess,'customAlertInfo');
						 setTimeout(function() {
					 			$('#call').click();
					 		}, 50);
						}else{
							var messageExito = "<strong> Exito !</strong> No puede crear metodo de pago";
						    Flash.create('info',messageExito,'customAlertError');
						}
					}).error(function(data,status,headers,config) {
						var messageError = "<strong>ERROR !</strong> Error al Metodo de pago.";
						Flash.create('info',messageError,'customAlertDanger');
						});
					});
			};
    	/**
    	 * fin de funciones CRUD
    	 */
	
			/**
			 * Controlador para gestionar la edicion de metodo de pago
			 */
}]).controller('controlEditarMetodoPago',				
		function($scope, $element, close, $http,Flash,globalPaginacion,helperFactory) {
	$scope.metodoPago=helperFactory.getObjeto();
	
	$scope.close = function(result) {
		$element.modal('hide');
		close(result, 500);
	};
	$scope.guardar=function(){
	$http.post('metodoPago/guardarMetodoPago.json',$scope.metodoPago).success(function(data){
		if(data=='succes'){
			var messageExito = "<strong> Exito !</strong> Metodo de pago creado";
		    Flash.create('info',messageExito,'customAlertSuccces');
		    setTimeout(function() {
	 			$('#call').click();
	 		}, 50);
		}else{
			var messageExito = "<strong> Exito !</strong> No puede crear metodo de pago";
		    Flash.create('info',messageExito,'customAlertError');
		}
	});
	};
	});