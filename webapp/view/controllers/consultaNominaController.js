/**
 * Controlador para gestionar las consultas de los comprobantes
 */
angular.module('basic').controller('consultaRecibos',[
'$scope','$http','Flash','ModalService','validacionesFactory','globalMensajes','globalPaginacion','$confirm','helperFactory','globalDownloadFile','ListaCriteriosBusqueda',
						function($scope, $http,Flash, ModalService, validacionesFactory, globalMensajes,globalPaginacion,$confirm,helperFactory, globalDownloadFile,ListaCriteriosBusqueda) {
							 $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
							  $scope.buscar={};
							$scope.showFilter=false;
		$scope.filterOptions =globalPaginacion.filterOptions();
	    $scope.totalServerItems = 0;
	    $scope.pagingOptions =globalPaginacion.pagingOptions();
	    var paginador=null;
	    var campo='login';
	    var joinn=[];
	    $scope.valor='';
	    
	    $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
	    
	  //para calculo de total de paginas
		$scope.Math = window.Math;
		//texto holder de input de busqueda
		$scope.placeholderText="Todos los criterios";
		$scope.displaySearch='none';
	var listaColumns = [
	        {field : 'fecha',displayName : 'FECHA INICIAL PAGO',width : '15%', cellFilter:'date:"medium"'},
			{field : 'accion',displayName : 'FECHA FINAL PAGO',width : '15%'},
			
			{field : 'descripcion',displayName : 'ENVIAR POR CORREO',width : '15%'},
			{field : 'host',displayName : 'UUID',width : '15%'},
			{field : 'usuario.rfcEmisor',displayName : 'FECHA PAGO',width : '15%'},
			{field : 'usuario.login',displayName : 'DESCARGAR PDF',width : '15%'},
			{field : 'usuario.login',displayName : 'DESCARGAR XML',width : '15%'}
			
		
			
			
			];
	
	/**
	 * funciones del componante ng-grip
	 */
	 $scope.gridOptions = globalPaginacion.gridOptions(listaColumns,$scope);
	    $scope.gridOptions.canSelectRows=false;
	    	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		          if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
		        	$scope.getPagedDataAsync=globalPaginacion.getPagedDataAsync($scope,$scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText,paginador,campo,joinn);
		          }
		      }, true);
	    	
	    	$scope.$watch('miinput', function () {
				 var data;
				 var ft = $scope.miinput;
		          if (ft) {
		        	  data = $scope.dataGrid.filter(function(item) {
		                    return JSON.stringify(item).indexOf(ft) != -1;
		                });
		        	  $scope.dataGrid = data;
		          }else{
		           	  setTimeout(function(){
					    	$( "#boton" ).click();
					    }, 1);
		          }
		      }, true);	
	
	 $scope.$watch('miinput', function () {
		 var data;
		 var ft = $scope.miinput;
          if (ft) {
        	  data = $scope.dataGrid.filter(function(item) {
        			console.warn(ft);
                    return JSON.stringify(item).toString().indexOf(ft) != -1;
                });
        	  $scope.dataGrid = data;
          }else{
        
          }
      }, true);
	 
	 /**
	  * fin de funciones del componente ng-grip
	  */
	 //funcion de busqueda por fecha
 $scope.buscarPorFecha= function() {
		 
		 paginador = {
				
				 'klase' : 'Historial',
	     	 	 'disyuncion' : null,
		 		 'conjuncion' : null ,
				 'between' :[['fecha',$scope.buscar.fechaHistorial+":00"],['fechacreacion',$scope.buscar.historialFinal+":59"]],
				 'order' : null,
				 'joinn' : ['usuario'],
				 'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
				 'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'};
		 $scope.pagingOptions.currentPage = 1;
			$scope.getPagedDataAsync = globalPaginacion
					.getPagedDataAsync($scope,
							$scope.pagingOptions.pageSize,
							$scope.pagingOptions.currentPage,
							null, paginador, 'descripcion',
							null);
			joinn = null;
			globalPaginacion.watchfilterOptions($scope,
					paginador, 'descripcion', joinn);
			
			
	 
	 
	 };
	 
	
		//funcion de busqueda por UUID
		 $scope.buscarPorUuid= function() {
				 
				 paginador = {
						
						 'klase' : 'Historial',
			     	 	 'disyuncion' : null,
				 		 'conjuncion' : ['usuario.login|'+$scope.buscar.emailHistorial] ,
						 'between' :null,
						 'order' : null,
						 'joinn' : ['usuario'],
						 'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
						 'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'};
				 $scope.pagingOptions.currentPage = 1;
					$scope.getPagedDataAsync = globalPaginacion
							.getPagedDataAsync($scope,
									$scope.pagingOptions.pageSize,
									$scope.pagingOptions.currentPage,
									null, paginador, 'descripcion',
									null);
					joinn = null;
					globalPaginacion.watchfilterOptions($scope,
							paginador, 'descripcion', joinn);
					
					
			 
			 
			 };
	 

	           
}]);