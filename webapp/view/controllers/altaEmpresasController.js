/**
 * Componente para el modulo de Empreasas
 */
angular.module('basic').controller('altaEmpresasController',[
'$scope','$http','Flash','ModalService','validacionesFactory','globalMensajes','globalPaginacion','$confirm','helperFactory','globalDownloadFile','ListaCriteriosBusqueda',
						function($scope, $http,Flash, ModalService, validacionesFactory, globalMensajes,globalPaginacion,$confirm,helperFactory, globalDownloadFile,ListaCriteriosBusqueda) {
							 $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
							  $scope.buscar={};
							$scope.showFilter=false;
		$scope.filterOptions =globalPaginacion.filterOptions();
	    $scope.totalServerItems = 0;
	    $scope.pagingOptions =globalPaginacion.pagingOptions();
	    var paginador=null;
	    var campo='login';
	    var joinn=[];
	    $scope.valor='';
	    
	    $scope.data = ListaCriteriosBusqueda.getListaCriteriaBusqueda();
	    
	  //para calculo de total de paginas
		$scope.Math = window.Math;
		//texto holder de input de busqueda
		$scope.placeholderText="Todos los criterios";
		$scope.displaySearch='none';
	var listaColumns = [
	        {field : '',displayName : 'Rfc',width : '15%', cellFilter:'date:"medium"'},
			{field : '',displayName : 'Raz\u00F3n Social',width : '15%'},
			{field : '',displayName : 'Regimen Patronal',width : '15%'},
			{field : '',displayName : 'Regimen Fiscal',width : '15%'},
			{field : '',displayName : 'Tipo Persona',width : '15%'}
			];
	/**
	 * Componente para la paginacion de la tabla de busquedas
	 */
	 $scope.gridOptions = globalPaginacion.gridOptions(listaColumns,$scope);
	    $scope.gridOptions.canSelectRows=false;
	    	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		          if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
		        	$scope.getPagedDataAsync=globalPaginacion.getPagedDataAsync($scope,$scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText,paginador,campo,joinn);
		          }
		      }, true);
	    	
	    	$scope.$watch('miinput', function () {
				 var data;
				 var ft = $scope.miinput;
		          if (ft) {
		        	  data = $scope.dataGrid.filter(function(item) {
		                    return JSON.stringify(item).indexOf(ft) != -1;
		                });
		        	  $scope.dataGrid = data;
		          }else{
		           	  setTimeout(function(){
					    	$( "#boton" ).click();
					    }, 1);
		          }
		      }, true);	
	/**
	 * componente la actualizacion del componente
	 */
	 $scope.$watch('miinput', function () {
		 var data;
		 var ft = $scope.miinput;
          if (ft) {
        	  data = $scope.dataGrid.filter(function(item) {
        			console.warn(ft);
                    return JSON.stringify(item).toString().indexOf(ft) != -1;
                });
        	  $scope.dataGrid = data;
          }else{
        $scope.empresasRegistradas();
          }
      }, true);
	 
	 
	/**
	 * funcion para la busqueda de empresas
	 */
 $scope.empresasRegistradas= function() {
		 paginador = {
				
				 'klase' : 'empresas',
	     	 	 'disyuncion' : null,
		 		 'conjuncion' : null ,
				 'between' :null,
				 'order' : null,
				 'joinn' : null,
				 'urlRestDefault' : 'paginacion/getPaginationObjetBusca.json',
				 'urlRestBusca' : 'paginacion/getPaginationObjetBusca.json'};
		 $scope.pagingOptions.currentPage = 1;
			$scope.getPagedDataAsync = globalPaginacion
					.getPagedDataAsync($scope,
							$scope.pagingOptions.pageSize,
							$scope.pagingOptions.currentPage,
							null, paginador, 'descripcion',
							null);
			joinn = null;
			globalPaginacion.watchfilterOptions($scope,
					paginador, 'descripcion', joinn);
	 
	 };
	 /**
	  * funcion para mandar a traer el modal para agregar una nueva empresa
	  */
	 $scope.agregarEmpresa = function() {
			helperFactory.setObjeto('');
			ModalService
					.showModal(
							{
								templateUrl : 'view/empresas/addEmpresas.jsp',
								controller : "modalEmpresa",
							}).then(function(modal) {
						modal.element.modal();
						modal.close.then(function(result) {
						});
					});
		};
		/**
		 * Controlador para el modal Empresa
		 */
}]).controller("modalEmpresa",['$scope','$http','$element','close','Flash','$filter','$q','helperFactory',
      						function($scope, $http, $element, close, Flash,
    								$filter, $q,helperFactory) {
	
}]);