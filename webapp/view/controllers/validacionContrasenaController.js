/**
 * Controlador para gestionar el cambio de contraseña
 */
angular.module('basic').controller('cambioContrasenaControl',['$scope','$window','usuarioFactory',function($scope,$window,usuarioFactory) {
	
	validarCambioContrasena();	
	function validarCambioContrasena(){
		var valor=$scope.primeraSession;		
		if(valor==0){
			usuarioFactory.getPrimeraSession($scope.usuarioSession).success(function(data, status, headers, config) {
				if (data != null) {
					 $scope.primeraSession = data;
				} else {

				}
			}).error(function(data, status, headers, config) {

			});
		$window.location.href = '#/cambio';
		$scope.mostrarMenu=false;
		}else{
			$scope.mostrarMenu=true;			
		}					
	};
}]);
