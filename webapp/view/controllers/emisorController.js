/**
 * Controlador para gestionar el modulo de emisor
 */
angular.module('basic').controller('emisorController' ,['$scope' ,'Flash', '$http' ,'$captcha' ,'Flash' ,'$confirm' ,'validacionesFactory','globalMensajes','globalDownloadFile',
		    function($scope, Flash , $http , $captcha , Flash , $confirm, validacionesFactory,globalMensajes,globalDownloadFile) {
			getUsuario();
			$scope.tipoRfc="";
			$scope.requeridoMoral=false;
			$scope.requeridoFisico=false;
			$scope.contribuyente={};
			
			$scope.descargoContrato=false;
	    	$scope.descargoConvenio=false;
	    	$scope.descargoManifiesto=false;
			
			/**
			 * Funcion para validar que exista lugar de expedicion o código postal dentro del catálogo c_CodigoPostal del SAT
			 * 
			 */
			$scope.validarLugarExpedicion = function(){
				var cpTemp=$scope.contribuyente.lugarExpedicion;
				$http.post("catalogosSATController/buscaLugarExpedicion.json",$scope.contribuyente )
				.success(function(data, status, headers,config) {
					if(data==false){
						$scope.contribuyente.lugarExpedicion="";
						globalMensajes.getMensajeError('El lugar de expedici\u00F3n '+cpTemp+' no existe en el catalogo "c_CodigoPostal" del SAT.');
						}
					}).error(function(data, status, header,config) {
					});
			}

			/**
			 * Funcion para obtener un objeto de emisor
			 */
			function getUsuario() {
			$http.post('emisorController/datosEmisor.json'
			    ).success(
			    function(data, status, headers, config) {
				if (data != "") {
				 $scope.contribuyente = data;
				 $scope.regimen($scope.contribuyente.rfc);
				 verificarRfcEmisorA($scope.contribuyente.rfc);
				 if( $scope.contribuyente.fechaEscrituraPublica!=null){
					$scope.fechaEscrituraPublica=$scope.contribuyente.fechaEscrituraPublica;
				 }
				 if($scope.contribuyente.fechaInscritoFolioMercantil!=null){
					 $scope.fechaFolioMer= $scope.contribuyente.fechaInscritoFolioMercantil;
				 }
				 if($scope.actualizarCer == true){
					 $scope.contribuyente.llaveKey = null;
					 $scope.contribuyente.llaveCer = null;
				 }
				}else{
					 $scope.regimen($scope.contribuyente.rfc);
					 verificarRfcEmisor($scope.contribuyente.rfc);
				}
				
			    }).error(
			    function(data, status, headers, config) {

			    });
			
		    };
			     /**
					 * Funcion para limpiar elementos del obj contribuyente
					 */
				$scope.limpiar=function(){
					$scope.contribuyente.regimenFiscal="";
					$scope.contribuyente.nombre="";
					$scope.contribuyente.clave="";
					$scope.contribuyente.tcDomicilio.calle="";
					$scope.contribuyente.tcDomicilio.colonia="";
					$scope.contribuyente.tcDomicilio.municipio="";
					$scope.contribuyente.tcDomicilio.localidad="";
					$scope.contribuyente.tcDomicilio.referencia="";
					$scope.contribuyente.tcDomicilio.noExterior="";
					$scope.contribuyente.tcDomicilio.noInterior="";
					$scope.contribuyente.tcDomicilio.estado="";
					$scope.contribuyente.tcDomicilio.pais="";
					$scope.contribuyente.tcDomicilio.codigoPostal="";
				}
				
				var validarCertificados =function(tipoCertificado,idArchivoKey,idArchivoCer){
					return new Promise(function(resolve, reject){
						if(document.getElementById(idArchivoKey).files.length==0){
							globalMensajes.getMensajeError("Debe de elegir un archivo.key de tipo "+tipoCertificado);
							return;
						}
							
						if(document.getElementById(idArchivoCer).files.length==0){
							globalMensajes.getMensajeError("Debe de elegir un archivo.cer de tipo "+tipoCertificado);
							return;
						}
						
						var validacionFiel= new Promise(function(resolve, reject){
							var formDataFiel = new FormData();
					    	formDataFiel.append("fileKeyFiel", document.getElementById(idArchivoKey).files[0]);
					    	formDataFiel.append("fileCerFiel", document.getElementById(idArchivoCer).files[0]);
					    	$scope.contribuyente.tipoCertificado=tipoCertificado;
					    	formDataFiel.append("objeto", angular.toJson($scope.contribuyente));
							$http.post('validacion_fiel/validacion.json',formDataFiel,{transformRequest: function(data, headersGetterFunction) {
								return data;
							},headers: {'Content-type': undefined}  
								}).success(function(data, status, headers, config) {
									resolve(data);
								}).error(function() {
									reject("Ocurrio un error al validar archivos tipo "+tipoCertificado);
									globalMensajes.getMensajeError("Ocurrio un error al validar archivos tipo "+tipoCertificado);
								});
						
						});
						validacionFiel.then(function (data) {
							if(data === 'valida'){
								resolve(true);
							}else{
								reject(data);
							}
					    }).catch(function (error) { 
					    	reject(error);
					    }); 
					});
				}
				
				/**
				 * Funcion para actualizar el emisor/contribuyente con
				 * certificados
				 */
			    $scope.actualizar = function() {
			    	validarCertificados("CSD","fileKey","fileCer").then(function(value) {
			    		validarCertificados("FIEL","fileKeyFiel","fileCerFiel").then(function(value) {
			    			if($scope.descargoContrato === true && $scope.descargoConvenio === true && $scope.descargoManifiesto === true){
			    				$confirm({text:'\u00bfEst\u00e1 usted seguro de que esta informaci\u00f3n es correcta?, se le hace la aclaraci\u00f3n sobre los certificados de tipo FIEL, Los certificados de tipo FIEL ser\u00e1n utilizados unicamente para realizar el firmado electr\u00f3nico del contrato ,convenio y manifiesto, estos certificados de tipo FIEL no ser\u00e1n guardados dentro del sistema, solamente se guardar\u00e1n los certificados de tipo CSD para emitir los CFDI.',
			    					title: 'Resgistrar datos contribuyente', cancel: 'No estoy seguro', ok: 'Si estoy seguro'}).then(function() {
			    						$scope.contribuyente.fechaEscrituraPublica=new Date($scope.fechaEscrituraPublica).getTime();
						                   $scope.contribuyente.fechaInscritoFolioMercantil=new Date($scope.fechaFolioMer).getTime();
										    var formData = new FormData();
										    formData.append("fileKey", document.getElementById("fileKey").files[0]);
										    formData.append("fileCer", document.getElementById("fileCer").files[0]);
										    formData.append("fileKeyFiel", document.getElementById("fileKeyFiel").files[0]);
										    formData.append("fileCerFiel", document.getElementById("fileCerFiel").files[0]);
											formData.append("objeto", angular.toJson($scope.contribuyente));
										
											$http.post('emisorController/actualizaEmisor.json',formData,{transformRequest: function(data, headersGetterFunction) {
												return data;
											},headers: {'Content-type': undefined}  
												}).success(function(data) {
												    globalMensajes.getMensajeGlobal(data[0],data[1]);
												   $scope.rfcEmisor=data[2];
												    if(data[1]=="El emisor fue agregado correctamente"){
												    	 setTimeout(function () {
												    	        location.reload();
												    	      }, 300);									    
												    }
												}).error(function() {
													globalMensajes.getMensajeError("Ocurrio un error al actalizar los datos.");
												});
			    					});
			    			}else{
			    				globalMensajes.getMensajeInfo("Estimado usuario es necesario que descargue el contrato, convenio y el manifiesto para su posterior revisi\u00f3n");
			    			}
			    		}).catch(function(reason) {
			    			globalMensajes.getMensajeError(reason);
			    		})
			    	}).catch(function(reason) {
			    		globalMensajes.getMensajeError(reason);
			    	});
			    };
			    /**
			     * Metodo para validar los archivos fiel
			     */
			    var validacionExistenciaFiel=function(tipo){
			    	return new Promise(function(resolve, reject) {
				    	if(document.getElementById("fileKeyFiel").files.length==0){							
							reject("Debe de elegir un archivo.key de tipo FIEL para poder descargar el "+tipo);
						}
							
						if(document.getElementById("fileCerFiel").files.length==0){							
							reject("Debe de elegir un archivo.cer de tipo FIEL para poder descargar el "+tipo);
						}
						resolve(true);
				    });
			    };
			    
			    /**
			     * Funcion para descargar el contrato del contribuyente
			     */
			    $scope.descargaContrato=function(){
			    	validacionExistenciaFiel("contrato").then(function(value){
				    	$scope.contribuyente.fechaEscrituraPublica=new Date($scope.fechaEscrituraPublica).getTime();
	                    $scope.contribuyente.fechaInscritoFolioMercantil=new Date($scope.fechaFolioMer).getTime();
	                    $scope.descargoContrato=true;
						globalDownloadFile.descargaArchivosPost('documentos/visualizarcontrato.json',$scope.contribuyente);
			    	}).catch(function(reason) {
			    		globalMensajes.getMensajeError(reason);
			    	});
				};
				/**
				 * Funcion para descargar el convenio del contribuyente
				 */
				$scope.descargaConvenio=function(){
					validacionExistenciaFiel("convenio").then(function(value){
						$scope.descargoConvenio=true;
				    	$scope.contribuyente.fechaEscrituraPublica=new Date($scope.fechaEscrituraPublica).getTime();
	                    $scope.contribuyente.fechaInscritoFolioMercantil=new Date($scope.fechaFolioMer).getTime();
						globalDownloadFile.descargaArchivosPost('documentos/vizualizarConvenio.json',$scope.contribuyente);
			    	}).catch(function(reason) {
			    		globalMensajes.getMensajeError(reason);
			    	});
				};
				/**
				 * Funcion para descargar el manifiesto del contribuyente
				 */
				$scope.descargaManifiesto=function(){
					validacionExistenciaFiel("manifiesto").then(function(value){
						$scope.descargoManifiesto=true;
						globalDownloadFile.descargaArchivosPost('documentos/vizualizaManifiesto.json',$scope.contribuyente);
			    	}).catch(function(reason) {
			    		globalMensajes.getMensajeError(reason);
			    	});
				};
			    
			    /**
				 * Funcion para actualizar el emisor/contribuyente sin
				 * certificados
				 */
			    $scope.actualizarSinCer = function() {
			    
			    	$scope.contribuyente.fechaEscrituraPublica=new Date($scope.fechaEscrituraPublica).getTime();
	                   $scope.contribuyente.fechaInscritoFolioMercantil=new Date($scope.fechaFolioMer).getTime();
				    var formData = new FormData();
					formData.append("objeto", angular.toJson($scope.contribuyente));
				
					$http.post('emisorController/actualizaEmisorSinCer.json',formData,{transformRequest: function(data, headersGetterFunction) {
						return data;
					},headers: {'Content-type': undefined}  
						}).success(function(data) {
						    globalMensajes.getMensajeGlobal(data[0],data[1]);
						   $scope.rfcEmisor=data[2];
						    if(data[1]=="El emisor fue agregado correctamente"){
						    	 setTimeout(function () {
						    	        location.reload();
						    	      }, 300);
						    
						    }
						}).error(function() {
							globalMensajes.getMensajeError("Ocurrio un error al actualizar los datos.");
						});
			    };
			    
			    /**
				 * Funcion para obtener una lista de regimen fiscal que
				 * corresponda al RFC
				 */
			    $scope.regimen = function(rfc){
					$http.post("catalogosSATController/buscaRegimenFiscal/"+rfc+".json")
					.success(function(data, status, headers,config) {
						$scope.listaRegimen = data;
						}).error(function(data, status, header,config) {
							
						});
			    }
			    
			      function  verificarRfcEmisor(rfc){
			    	$http.post("emisorController/validarLongitudRfc/"+rfc+".json")
					.success(function(data, status, headers,config) {
						$scope.tipoRfc=data;
						
						if($scope.tipoRfc=="M"){
							$scope.requeridoMoral=true;	
							$http.post("usuarioController/buscarUsuarioPorRfc/"+rfc+".json")
							.success(function(data, status, headers,config) {
								$scope.contribuyente.telefono = data.telFijo;
								$scope.contribuyente.nombre = data.nombre;
								$scope.contribuyente.correo = data.correo
								
								}).error(function(data, status, header,config) {
										
									
								});	
						}
						if($scope.tipoRfc=="F"){
							$scope.requeridoFisico=true;
							$http.post("usuarioController/buscarUsuarioPorRfc/"+rfc+".json")
							.success(function(data, status, headers,config) {
								$scope.contribuyente.telefono = data.telFijo;
								$scope.contribuyente.nombre = data.nombre;
								$scope.contribuyente.correo = data.correo
								
								}).error(function(data, status, header,config) {
										
									
								});	
						}
						}).error(function(data, status, header,config) {
							
						});	
			    }
			      function  verificarRfcEmisorA(rfc){
				    	$http.post("emisorController/validarLongitudRfc/"+rfc+".json")
						.success(function(data, status, headers,config) {
							$scope.tipoRfc=data;
							
							if($scope.tipoRfc=="M"){
								$scope.requeridoMoral=true;	
								
							}
							if($scope.tipoRfc=="F"){
								$scope.requeridoFisico=true;
							
							}
							}).error(function(data, status, header,config) {
								
							});	
				    }
			      $scope.valida = function(rfc){
			       verificarRfcEmisor(rfc);
			      }
			     
			      $scope.validaModalActualizar = function(rfc){
				       verificarRfcEmisorA(rfc);
				      }
		    } ]);