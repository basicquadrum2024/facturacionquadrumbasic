<style>
.content-body {
	color:black;
}
</style>
<div class="modal fade content-body" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title" style="color: black;">Factura creada con exito</h4>
			</div>
			<div class="row">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<jsp:include page="/view/alert.jsp"></jsp:include>
				</div>
			</div>
			<div class="modal-body">
				<table class="table table-condensed">
					<thead>
						<tr>
							<th>uuid</th>
							<th>serie</th>
							<th>folio</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>				
						<tr ng-repeat="resu in resultadoWrappers">
							<td>{{resu.uuid}}</td>
							<td>{{resu.serie}}</td>
							<td>{{resu.folio}}</td>
							<td>
							<div ng-show="resu.error==0"><a
								href="documentos/descarga.json?opcion=xml&objeto={{resu}}">Descargar
									XML</a>&nbsp;|&nbsp;<a
								href="documentos/descarga.json?opcion=pdf&objeto={{resu}}">Descargar
									PDF</a></div>
							<div ng-show="resu.error==1">Xml no encontrado ante el SAT, favor de intentarlo en 20 minutos</div>							
							<div ng-show="resu.error==2">Cancelado con �xito</div></td>
							
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-ok" ng-click="cambio()"
					data-dismiss="modal">
					Aceptar<span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
			</div>

		</div>
	</div>
</div>