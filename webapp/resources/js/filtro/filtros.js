var app = angular.module('custom-password',[]);

app.directive("mask", ["$compile", function($compile) {
	return {
        require : "ngModel",
        restrict : "A",
        link : function(scope, element, attributes, controller) {
            var maskedInputElement;

            var maskValue = function(value) {
                // reemplaza el valor con *
                return (value || "").replace(/[\S]/g, "\u2022");
            };

            // para enmascarar se clona el input
            var createMaskedInputElement = function() {
                if (! maskedInputElement || ! maskedInputElement.length) {
                    maskedInputElement = element.clone(true);
                    maskedInputElement.attr("type", "password");
                    maskedInputElement.removeAttr("name");
                    maskedInputElement.removeAttr("id");
                    maskedInputElement.removeAttr("mask");
                    maskedInputElement.bind("blur", function() {
                        element.removeClass("ng-hide");
                        maskedInputElement.remove();
                        maskedInputElement = null;
                    });
                    $compile(maskedInputElement)(scope);
                    element.after(maskedInputElement);
                }
            };

            // se oculta al perder focus
            element.bind("focus", function() {
                createMaskedInputElement();
                element.addClass("ng-hide");
                maskedInputElement[0].focus();
            });

            // se agrega valor a la vista
            controller.$formatters.push(function(value) {
                return maskValue(value);
            });

        }
	};
}]);

var app2 = angular.module('seg-password',[])
app2.factory('$zxcvbn', function() {
    return {
        score: function() {
            var compute = zxcvbn.apply(null, arguments);
            return compute && compute.score;
        }
    };
})