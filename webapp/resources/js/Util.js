angular.module('components', [])
   .directive('uppercased', function() {
    return {
        require: 'ngModel',        
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(input) {
                return input ? input.toUpperCase() : "";
            });
            element.css("text-transform","uppercase");
        }
    };
}).directive('pasteTrimed', function(){
	var linkFn = function(scope, element, attrs) {        
        element.on('paste', function() {            
            setTimeout(function() {
            	element.val($.trim(element.val()));                
                scope.$apply();
            }, 5);            
        });
	};

	return {
		restrict: 'A',
		link: linkFn
	};
}).directive('myAlert', function($modal,$log) {
    return {
        restrict: 'E',
        scope: {
          mode: '@',
          boldTextTitle: '@',
          textAlert : '@'
        },
        link: function(scope, elm, attrs) {
        
       scope.data= {
                mode:scope.mode || 'info',
                boldTextTitle:scope.boldTextTitle || 'title',
                textAlert:scope.textAlert || 'text'
              }
        
       var ModalInstanceCtrl = function ($scope, $modalInstance, data) {
          
           console.log(data);
          
          $scope.data = data;
             $scope.close = function(){
             $modalInstance.close($scope.data);
          };
        };
        
        elm.parent().bind("click", function(e){
           scope.open();
       });
        
     scope.open = function () {
        console.log("open")
        var modalInstance = $modal.open({
          templateUrl: 'view/modal/myModalContent.html',
          controller: ModalInstanceCtrl,
          backdrop: true,
          keyboard: true,
          backdropClick: true,
          size: 'lg',
          resolve: {
            data: function () {
              return scope.data;
            }
          }
        });
    
    
        modalInstance.result.then(function (selectedItem) {
          scope.selected = selectedItem;
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
    }
  }
  }; 
})
.directive('capitalize', function() {
	   return {
		     require: 'ngModel',
		     link: function(scope, element, attrs, modelCtrl) {
		        var capitalize = function(inputValue) {
		           if(inputValue == undefined) inputValue = '';
		           var capitalized = inputValue.toUpperCase();
		           if(capitalized !== inputValue) {
		              modelCtrl.$setViewValue(capitalized);
		              modelCtrl.$render();
		            }         
		            return capitalized;
		         }
		         modelCtrl.$parsers.push(capitalize);
		         capitalize(scope[attrs.ngModel]);  // capitalize initial value
		     }
		   };
});
