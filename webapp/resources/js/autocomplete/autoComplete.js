angular.module('modulo-autocomplete',[]).directive('autoComplete',[function(){
	return {
		restrict:'AE',
		scope:{
			listaObjetos:'=listaObjetos',
			placeHolder:'@placeHolder',
			clase:'@clase',
			propiedadMostrar:'=propiedadMostrar',
			propiedadElegir:'@propiedadElegir',
			funcionBusqueda:'=funcionBusqueda',
			modeloNgModel:'=modeloNgModel',
			nombreElemento:'@nombreElemento',
			modeloNgRequired:'@modeloNgRequired',
			tooltip:'@tooltip',
			nombreCatalogo:'@nombreCatalogo'
		},
		template:'<input style="color: black !important;" type="text" placeholder="{{placeHolder}}" data-tooltip="{{tooltip}}"' +
							'name="{{nombreElemento}}"'+
							'ng-required="{{modeloNgRequired}}"'+
							'ng-class="clase"' +
							'ng-model="modeloNgModel"' +
							'ng-keyup="metodoOnKeyUp(modeloNgModel)"/>'+							
							'<div ng-show="mostrarLista">' +
								'<ul class="list-group ul-separar">' +
									'<li class="list-group-item li-separar"' +										
										'ng-repeat="option in listaObjetos"' +
										'ng-mouseenter="hover = true"' +
										'ng-mouseleave="hover = false"'+
										'ng-click="seleccionarOpcion(option)"' +
										'ng-class="{active: hover}">' +
										'<span>{{option | filtroConcatenaPropiedades : propiedadMostrar}}</span>' +
									'</li>' +
								'</ul>' +
							'</div>'+
							'<div ng-show="listaObjetos.length==0">'+
						       '<p class="help-block">La {{nombreElemento}} no se encuentra dentro del catalogo {{nombreCatalogo}} del SAT.</p>'+
					         '</div>',
		controller:['$scope',function($scope){
			$scope.modelo='';
			$scope.mostrarLista=false;
			$scope.metodoOnKeyUp=function(modelo){
				$scope.mostrarLista=true;
				$scope.funcionBusqueda(modelo);
			};

			$scope.seleccionarOpcion=function(option){
				if($scope.propiedadElegir!=null && $scope.propiedadElegir!=undefined){
					$scope.modeloNgModel=option[$scope.propiedadElegir];
					$scope.mostrarLista=false;				   
				}else{
					console.log("Propiedad a obtener del objeto no existe o no ha sido declarada")
				}			
			};
			
//			$scope.$watchCollection('listaObjetos', function (newVal, oldVal) { 				
//			});
		}],
		link:function(scope, element, attrs){
			element.on('keydown', function() {            
            //setTimeout(function() {
            	//console.log(scope);
            	//element.val(element.value.toUpperCase());         
                //scope.$apply();
            //}, 5);            
         });
		}
	};

}]).filter("filtroConcatenaPropiedades", function() {

  return function(objeto, listaPropiedades) {
  	var cadenaRespuesta="";
  	for (var i = 0;i<=listaPropiedades.length; i++) {
  		var propiedad=listaPropiedades[i];  		
  		if(objeto[propiedad] !=undefined){
  				cadenaRespuesta=cadenaRespuesta+objeto[propiedad]+" - ";   			 		
  		}
  	}
  	cadenaRespuesta=cadenaRespuesta.trim();
  	var cadenaFinal=cadenaRespuesta.substring(0, cadenaRespuesta.length-1);
    return cadenaFinal;

  };
});;
