var app = angular.module('QuadrumBasic.login', ['ngRoute', 'angularModalService', 'flash', 'ngAnimate', 'components',
	'vcRecaptcha', 'custom-password', 'ngSanitize']);

// Declared route

app.config(['$routeProvider', function($routeProvider) {

	$routeProvider.when('/login', {
		templateUrl: 'login.jsp',
		controller: 'loginCtrl'
	});

}]);
app.controller('notificacionesController', ['$scope', '$http', function(scope, http) {
	scope.mostrarNotificacion = false;
	scope.mensajeNotificacion = [];

	http.post('https://www.facturacionquadrum.com.mx/Notificaciones/notificacion/consultarNotificacionQuadrumApps.json', { "key": "basica", "value": 1 }).success(function(data) {
		if (!angular.equals(data, '')) {
			scope.mostrarNotificacion = true;
			scope.mensajeNotificacion = data;
		}

	}).error(function(error) {
		console.log(error);
	});

}]);

app.controller('manualesController', ['$scope', '$http', function(scope, http) {

	scope.descargaManuales = function() {
		scope.ruta = '/data/facturacion/basica/manuales/Facturacion_basica_manual.pdf';
		descargaArchivo(scope.ruta, 'Facturacion_basica_manual.pdf');

	};

	scope.descargaAvisos = function() {
		scope.ruta = '/data/facturacion/basica/manuales/Terminos_y_Condiciones_facturacionquadrum.pdf';
		descargaArchivo(scope.ruta, 'Terminos_y_Condiciones_facturacionquadrum.pdf');
	};

	function descargaArchivo(datos, nombreArchivo) {
		http({
			url: 'usuarioController/manuales.fue?ruta=' + datos,
			method: "POST",
			// data:'ruta='+scope.ruta,
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'arraybuffer'
		}).success(function(data, status, headers, config) {
			var a = document.createElement("a");
			document.body.appendChild(a);
			var file = new Blob([data], { type: '.pdf' });
			var fileURL = window.URL.createObjectURL(file);
			a.href = fileURL;
			a.download = nombreArchivo;
			a.click();
		}).error(function(data, status, headers, config) {

		});
	}


}]);
// Home controller
/***********************************************************************************************/
app.controller('restorePassword', function($scope, ModalService, Flash) {
	$scope.showModalRestorePassword = function() {
		popupLoading.modal('show');
		ModalService.showModal({
			templateUrl: 'view/login/restore-password.jsp',
			controller: "modalRestorePasswordController"
		}).then(function(modal) {
			popupLoading.modal('hide');
			$scope.usuario = {};
			modal.element.modal();
			modal.close.then(function(result) {
			});
		});
	};

});

app.controller('modalRestorePasswordController', function($scope, $element, $http, Flash, $captcha) {

	$scope.usuario = {};
	$scope.validaRfc = function(rfc) {
		var patron = /^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]{3}$/;
		if (patron.test(rfc)) {

		} else {
			var messageError = "<strong>Error!</strong> EL RFC no cumple con un patr\u00f3n v\u00e1lido. Ejemplo:<br>" +
				"Persona F\u00edsica AAAA010101AAA. <br> Persona Moral AAA010101AAA.";
			Flash.create('danger', messageError, 'customAlertError');
			$scope.usuario.rfce = "";
		}
	};

	$scope.recuperar = function(result) {
		popupLoading.modal('show');
		$http.post('usuarioController/recuperarContrasenia.fue', $scope.usuario).success(function(data, status, headers, config) {
			if (data == "success") {
				var message = "<strong>\u00c9xito!</strong> <br>" +
					"Se ha enviado a su correo electr\u00f3nico un password favor de revisar.";
				Flash.create('info', message, 'customAlertSuccces');
				popupLoading.modal('hide');
				$('#restore-password').hide();
				$('#crear-cuenta').hide();
				$('#login-form').show(1000);
			} else {
				var message = "<strong>Error!</strong> <br>" +
					"El rfc que usted captur\u00f3 no se encuentra dentro de nuestros registros.";
				Flash.create('info', message, 'customAlertError');
				popupLoading.modal('hide');
			}

		}).error(function(data, status, headers, config) {
			var message = "<strong>Error al recuperar contrase\u00f1a!</strong>";
			Flash.create('info', message, 'customAlertError');
			popupLoading.modal('hide');
		});
		//	close(result, 500); // close, but give 500ms for bootstrap to animate

	};

	$scope.cancel = function(result) {
		$element.modal('hide');
		//close(result, 500);
	};

});
app.directive('captcha', function() {
	return {
		restrict: 'E',
		template: '<br><br> <span ng-model="field1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{field1}}</span> '
			+ '<span ng-model="operator">{{operator}}</span> '
			+ '<span ng-model="field2">{{field2}}</span> = ',
		scope: {
			field1: "@",
			field2: "@",
			operator: "@"
		},
	}
});

app.factory('utileriasFactory', [function() {
	var utileriasFactory = {};
	var hasOwnProperty = Object.prototype.hasOwnProperty;

	utileriasFactory.validarObjetoVacio = function(objeto) {
		if (objeto === undefined)
			return true;

		if (objeto === null)
			return true;

		if (objeto.length > 0)
			return false;

		if (objeto.length === 0)
			return true;

		if (objeto === true)
			return false;

		if (Object.keys(objeto).length > 0)
			return false;


		if (JSON.stringify(objeto) === JSON.stringify({}))
			return true;

		for (var key in objeto) {
			if (hasOwnProperty.call(objeto, key))
				return false;
		}
		if (typeof objeto === 'number')
			return false;

		if (JSON.stringify(objeto).length > 0)
			return false;
		return true;
	};

	return utileriasFactory;
	/**
	 * Fabrica para la descarga de archivos en la aplicacion
	 */
}]);
app.factory('$captcha', ['$rootScope', function($rootScope) {
	return {
		// obtenemos los numeros y el operador para la
		// operación actual
		getOperation: function() {
			// array de operadores
			$rootScope.operators = ["+", "-", "*", "/"];
			// obtenemos un operador aleatorio
			$rootScope.operator = $rootScope.operators[Math
				.floor(Math.random()
					* $rootScope.operators.length)];
			// numero aleatorio entre 25 y 6
			$field1 = Math.floor(Math.random() * (25 - 6) + 6);
			// si el operador es una división
			if ($rootScope.operator == "/") {
				// obtenemos los posibles divisores del
				// numero obtenido
				$num = this.getDivisors($field1);
				// field2 es un numero aleatorio de los
				// posibles divisores que nos
				// ha proporcionado $num
				$field2 = $num[Math.floor(Math.random() * $num.length)];
			} else {
				// en otro caso, obtenemos un numero
				// aleatorio
				$field2 = Math
					.floor((Math.random() * 5) + 1);
				// comprobamos que fiel1 sea mayor que
				// field2
				while ($field1 < $field2) {
					$field1 = Math
						.floor((Math.random() * 15));
					$field2 = Math
						.floor((Math.random() * 5) + 1);
				}
			}
			// asignamos field1 y field2
			$rootScope.field1 = $field1;
			$rootScope.field2 = $field2;
		},

		// obtiene los posibles numeros divisores del
		// que hemos pasado como parametro,
		// si es 14 devuelve un array como este [1, 2,
		// 7, 14] etc
		// fuente:
		// http://nayuki.eigenstate.org/res/calculate-divisors-javascript.js
		getDivisors: function(n) {
			if (n < 1)
				n = 0;

			var small = [];
			var large = [];
			var end = Math.floor(Math.sqrt(n));
			for (var i = 1; i <= end; i++) {
				if (n % i == 0) {
					small.push(i);
					if (i * i != n)
						large.push(n / i);
				}
			}
			large.reverse();
			return small.concat(large);
		},
		//
		// retornamos el resultado de la operación
		// realizada
		// podriamos haber hecho un switch, pero sirve
		// perfectamente
		getResult: function(n1, n2, operator) {
			if (operator == "*") {
				return (n1) * (n2);
			} else if (operator == "+") {
				return (n1) + (n2);
			} else if (operator == "/") {
				return (n1) / (n2);
			} else {
				return (n1) - (n2);
			}
		},

		// resultado es el input que el usuario pone en
		// el formulario
		// para responder a la operación
		checkResult: function(resultado) {
			// si la respuesta a la operacion es
			// correcta
			if (parseInt(resultado) == this.getResult(
				parseInt($rootScope.field1),
				parseInt($rootScope.field2),
				$rootScope.operator)) {
				return true;
			}
			// en otro caso cambiamos la operacion
			else {
				this.getOperation();
			}
		}
	}
}]).run(function($captcha) {
	$captcha.getOperation();
});

/***********************************************************************************************/
app.controller('preguntaFrecuente', function($scope, ModalService, Flash) {
	$scope.showModalpreguntaFrecuente = function() {
		popupLoading.modal('show');
		ModalService.showModal({
			templateUrl: 'view/login/preguntaFrecuente.jsp',
			controller: "modalpreguntaFrecuente"
		}).then(function(modal) {
			popupLoading.modal('hide');
			modal.element.modal();
			modal.close.then(function(result) {
			});
		});
	};

});
/***********************************************************************************************/
app.controller('createAccount', function($scope, ModalService, Flash, globalDownloadFile) {

	$scope.submit1 = function() {
		popupLoading.modal('show');
		popupLoading.modal('hide');
	};


	$scope.showModalCreateUser = function() {
		popupLoading.modal('show');
		ModalService.showModal({
			templateUrl: 'view/login/add-user.jsp',
			controller: "modalCuentaController"
		}).then(function(modal) {
			popupLoading.modal('hide');
			$scope.usuario = {};
			modal.element.modal();
			modal.close.then(function(result) {
			});
		});
	};

});

app.controller('modalCuentaController', function($scope, $element, $http, Flash, $timeout, utileriasFactory) {

	$scope.terminos = false;

	var promesaValidacionLco = function(rfc) {
		return new Promise(function(resolve, reject) {
			$http.post('emisorController/validarContribuyenteLco.json', { rfc: rfc }).success(function(data, status, headers, config) {
				if (data === true)
					resolve(data);
				else
					reject("El rfc " + rfc + " no se encuentra en la LCO o Lista de Contribuyentes Obligados");
			}).error(function(data, status, headers, config) {
				reject("El rfc " + rfc + " no se encuentra en la LCO o Lista de Contribuyentes Obligados");
			});
		});
	};
	$scope.usuario = {};
	$scope.validaRfc = function(rfc) {
		var patron = /^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]{3}$/;
		if (patron.test(rfc)) {
			promesaValidacionLco(rfc).then(function(data) {

			}).catch(function(reason) {
				$scope.usuario.rfce = "";
				var message = "<strong>" + reason + "</strong>";
				Flash.create('info', message, 'customAlertError');
			});
		} else {
			var messageError = "<strong>Error!</strong> EL RFC no cumple con un patr\u00f3n v\u00e1lido. Ejemplo:<br>" +
				"Persona F\u00edsica AAAA010101AAA. <br> Persona Moral AAA010101AAA.";
			Flash.create('danger', messageError, 'customAlertError');
			$scope.usuario.rfce = "";
		}
	};


	$scope.registrar = function(result) {

		//utileriasFactory.validarObjetoVacio($scope.terminos);


		try {
			if ($scope.terminos === false)
				throw new Error("Acepto las condiciones del servicio");
			if ($scope.codigo == $scope.codigoGenerado) {



				popupLoading.modal('show');

				$http.post('usuarioController/registro.fue', $scope.usuario).success(function(data, status, headers, config) {

					if (data == "success") {
						var message = "<strong>Cuenta creda con \u00e9xito!</strong><br>" +
							"Se ha enviado a su correo electr\u00f3nico un password temporal.";
						Flash.create('info', message, 'customAlertSuccces');
						popupLoading.modal('hide');
						$('#restore-password').hide();
						$('#crear-cuenta').hide();
						$('#login-form').show(1000);
						//$('#modalCrearCuenta').modal('hide');
						//$('.modal-backdrop').remove();
					} else {
						var message = "<strong>Error al crear la cuenta!</strong><br>" +
							"El correo electr\u00f3nico y/o rfc ya se encuentra registrado.";
						Flash.create('info', message, 'customAlertError');
						popupLoading.modal('hide');
						//$('#modalCrearCuenta').modal('hide');
						//$('.modal-backdrop').remove();

					}

				}).error(function(data, status, headers, config) {
					var message = "<strong>Error al crear la cuenta!</strong>";
					Flash.create('info', message, 'customAlertError');
					popupLoading.modal('hide');
					//$('#modalCrearCuenta').modal('hide');
					//$('.modal-backdrop').remove();
				});

				//close(result, 500); // close, but give 500ms for bootstrap to// animate
			} else {

				var message = "<strong>Error!</strong><br>" +
					"El c\u00F3digo ingresado es incorrecto.";
				Flash.create('danger', message, 'customAlertError');

			}

		}
		catch (err) {
			var message = `<strong>Error!</strong><br>${err.message}`;
			Flash.create('danger', message, 'customAlertError');

		}



	};

	$scope.cancel = function(result) {
		//$element.modal('hide');
		//close(result, 500);
	};

	$scope.past = function() {
		$timeout(function() {
			$scope.cemail = " ";
		}, 0);
	}

	$scope.enviarcodigo = function() {
		$scope.codigoGenerado = aleatorio();
		var codigoWrapper = {
			correo: $scope.usuario.correo,
			codigo: $scope.codigoGenerado
		};

		$http.post('usuarioController/varificacion.fue', codigoWrapper).success(function(data, status, headers, config) {

			if (data == "success") {
				var message = "<strong>\u00C9xito!</strong><br>" +
					"Se ha enviado a su correo electr\u00f3nico un c\u00F3digo de verificaci\u00F3n.";
				Flash.create('info', message, 'customAlertSuccces');
			} else {
				var message = "<strong>Error al enviar c\u00F3digo!</strong><br>" +
					"Intente de nuevo o consulte al adminitrador.";
				Flash.create('info', message, 'customAlertError');

			}

		}).error(function(data, status, headers, config) {
			var message = "<strong>Error al enviar c\u00F3digo!</strong>";
			Flash.create('info', message, 'customAlertError');

		});

		document.getElementById("correouno").disabled = true;

	}

	function aleatorio() {
		return Math.round(Math.random() * (9999 - 1000) + parseInt(1000));
	}

	$scope.descargaAvisos = function() {
		descargaArchivoTermino('Terminos_y_Condiciones_facturacionquadrum.pdf');
	};
	
	function descargaArchivoTermino(nombreArchivo) {
		$http({
			url: 'usuarioController/terminos.fue',
			method: "POST",
			// data:'ruta='+scope.ruta,
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'arraybuffer'
		}).success(function(data, status, headers, config) {
			var a = document.createElement("a");
			document.body.appendChild(a);
			var file = new Blob([data], { type: '.pdf' });
			var fileURL = window.URL.createObjectURL(file);
			a.href = fileURL;
			a.download = nombreArchivo;
			a.click();
		}).error(function(data, status, headers, config) {

		});
	}

	function descargaArchivo(datos, nombreArchivo) {
		$http({
			url: 'usuarioController/manuales.fue?ruta=' + datos,
			method: "POST",
			// data:'ruta='+scope.ruta,
			headers: {
				'Content-type': 'application/json'
			},
			responseType: 'arraybuffer'
		}).success(function(data, status, headers, config) {
			var a = document.createElement("a");
			document.body.appendChild(a);
			var file = new Blob([data], { type: '.pdf' });
			var fileURL = window.URL.createObjectURL(file);
			a.href = fileURL;
			a.download = nombreArchivo;
			a.click();
		}).error(function(data, status, headers, config) {

		});
	}

});
/***********************************************************************************************/
app.controller('masInformacion', function($scope, ModalService, Flash) {

	$scope.showModalMasInformacion = function() {
		ModalService.showModal({
			templateUrl: 'view/login/masInformacion.jsp',
			controller: "modalController"
		}).then(function(modal) {
			modal.element.modal();
			modal.close.then(function(result) {
			});
		});
	};

	$scope.showModalGuiaRapida = function() {
		ModalService.showModal({
			templateUrl: 'view/login/guiaRapida.jsp',
			controller: "masInformacion"
		}).then(function(modal) {
			popupLoading.modal('hide');
			modal.element.modal();
			modal.close.then(function(result) {
			});
		})
	};

});


/***********************************************************************************************/
app.directive(
	'pwCheck',
	['Flash', function(Flash) {
		return {
			require: 'ngModel',
			link: function(scope, elem, attrs, ctrl) {
				var firstPassword = '#' + attrs.pwCheck;
				elem.add(firstPassword).on(
					'keyup',
					function() {
						scope.$apply(function() {
							ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
							if (elem.val() === $(firstPassword).val()) {
								var message = "<strong>\u00c9xito!</strong> <br>" +
									"Los correos coinciden.";
								Flash.create('success', message, 'customAlertSuccces');
							} else {
								var messageError = "<strong>Aviso!</strong> Los correos no coinciden.<br>" +
									"Por favor escriba los datos correctamente.";
								Flash.create('success', messageError, 'customAlertError');
							}
						});
					});
			}
		}


	}]);

/**
Se integra directiva para sanitizar los input para evitar la inyección de código malicioso*/
app.directive('input', function($sanitize) {
	return {
		restrict: 'E',
		require: '?ngModel',
		link: function(scope, element, attrs, ngModel) {
			if (ngModel !== undefined) {
				ngModel.$parsers.push(function(value) {
					return $sanitize(value);
				});
			}
		}
	};
});