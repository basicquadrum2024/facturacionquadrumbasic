<div id="modalValidarContinuidadTimbrado" class="modal fade"
	data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" style="color: black;">L�mite de timbrado excedido</h4>
			</div>
			<div class="modal-body">
				<h3>Le informamos que ha excedido su limite de timbrado, para
					la aplicaci�n b�sica usted solo puede generar 3 facturas mensuales, 
					si requiere m�s facturas comun�quese al �rea de atenci�n a clientes</h3>
			</div>
			<div class="modal-footer">
				<a class="btn btn-success btn-ok" href="j_spring_security_logout">Aceptar</a>
			</div>
		</div>
	</div>
</div>