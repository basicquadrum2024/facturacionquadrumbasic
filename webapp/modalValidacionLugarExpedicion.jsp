<div id="modalValidarLugarExpedicion" class="modal fade"
	data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" style="color: black;">Informaci�n</h4>
			</div>
			<div class="modal-body">
				<h3>Estimado usuario, es nesesario que actualize sus datos en la secci�n de "Actualizar contribuyente" para poder crear una factura</h3>
			</div>
			<div class="modal-footer">
				<a class="btn btn-success btn-ok" href="j_spring_security_logout">Aceptar</a>
			</div>
		</div>
	</div>
</div>